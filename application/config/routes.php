<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['home'] = 'teachers/home';
$route['home/(:any)/(:any)'] = 'teachers/home/$1/$2';
$route['logout'] = 'login/logout';
$route['login'] = 'login/index';
$route['admin'] = 'admin/index';
$route['all_classes'] = 'admin/classes';
$route['results/edit/(:any)'] = 'admin/edit_student_results/$1';
$route['results/edit/(:any)/(:any)'] = 'admin/edit_student_results/$1/$2';
$route['results/edit/(:any)/(:any)/(:any)'] = 'admin/edit_student_results/$1/$2/$3';
/*$route['all_subjects'] = 'admin/subjects';
$route['all_subjects/(:any)'] = 'admin/subjects/$1';*/
$route['school/matokeo'] = 'admin/matokeo';
$route['school/matokeo/(:any)'] = 'admin/matokeo/$1';
$route['school/matokeo/(:any)/(:any)'] = 'admin/matokeo/$1/$2';
$route['myclass/assign_monitor/(:any)'] = 'students/assign_monitor/$1';
//$route['school_attendance_report/(:any)'] = 'teachers/get_daily_attendance_report_summary/$1';
$route['school_attendance_report/(:any)'] = 'teachers/school_attendance_report';
$route['students/transfer/IN'] = 'students/transferred/transferred_in';
$route['students/transfer/OUT'] = 'students/transferred/transferred_out';
$route['discipline/all_students'] = 'students/get_students_discipline_records';
//$route['dormitory/(:any)/roll_call'] = 'dormitory/load_roll_call/$1';
$route['students/results/(:any)/(:any)'] = 'students/get_results_by_class_and_subject/$1/$2';
$route['teacher/assigned_classes/(:any)'] = 'teachers/get_teachers_classes/$1';
$route['teacher/assigned_classes/(:any)/(:any)'] = 'teachers/get_teachers_classes/$1/$2';
$route['teacher/assigned_classes/(:any)/(:any)/(:any)'] = 'teachers/get_teachers_classes/$1/$2/$3';
$route['teacher/myclasses/(:any)/(:any)'] = 'teachers/get_students_by_class_stream_and_subject/$1/$2';
$route['teacher/myclasses/(:any)/(:any)/(:any)'] = 'teachers/get_students_by_class_stream_and_subject/$1/$2/$3';
$route['teacher/myclasses/(:any)/(:any)/(:any)/(:any)'] = 'teachers/get_students_by_class_stream_and_subject/$1/$2/$3/$4';
$route['mysubjects/results/(:any)'] = 'students/retrieve_mkeka/$1';
$route['mysubjects/add_score/(:any)'] = 'teachers/add_score/$1';
//$route['scoresheet/(:any)/(:any)'] = 'students/get_students_by_class_and_subject/$1/$2';
//$route['scoresheet/(:any)/(:any)/(:any)'] = 'students/get_students_by_class_and_subject/$1/$2/$3';
//$route['scoresheet/(:any)/(:any)/(:any)/(:any)'] = 'students/get_students_by_class_and_subject/$1/$2/$3/$4';
//$route['(:any)/(:any)/results'] = 'students/student_results/$1/$2';


//===================USED THIS==================================
//$route['teacher/mytimetable/(:any)'] = 'classes/view_timetable_by_teacher/$1';
$route['teacher/mytimetable/(:any)'] = 'teachers/individual_timetable/$1';
$route['subject_teaching_logs/(:any)'] = 'subjects/subject_teaching_logs/$1';
$route['mylogs/(:any)'] = 'subjects/get_log_by_teacher_id/$1';





$route['myclass/students/(:any)'] = 'students/get_students_by_class_stream/$1';
$route['myclass/students/(:any)/(:any)'] = 'students/get_students_by_class_stream/$1/$2';
$route['myclass/students/(:any)/(:any)/(:any)'] = 'students/get_students_by_class_stream/$1/$2/$3';
$route['myclass/subject_teachers/(:any)'] = 'subjects/get_subject_teacher_by_class/$1';
$route['myclass/subject_teachers/(:any)/(:any)'] = 'subjects/get_subject_teacher_by_class/$1/$2';
$route['myclass/subject_teachers/(:any)/(:any)/(:any)'] = 'subjects/get_subject_teacher_by_class/$1/$2/$3';
$route['discipline/add_record'] = 'students/load_new_discipline';
//$route['students/registered'] = 'students/index';
//$route['students/class/(:any)'] = 'students/get_select_students_by_class/$1';
$route['dormitory/view_student/(:any)'] = 'Dormitory/view_students/$1';

$route['departments'] = 'departments';
//$route['departments/index'] = 'departments';
$route['class'] = 'classes/class_form';
$route['classes'] = 'classes';
$route['periods'] = 'periods';
$route['students'] = 'students';
$route['subjects'] = 'subjects';
$route['assign_subject'] = 'subjects/teaching_assignment';
$route['teachers'] = 'teachers';
$route['person/add_score'] = 'persons/add_scores';
$route['person/view/(:any)'] = 'persons/view_by_name/$1';
$route['person/view_all'] = 'persons/view_all_persons';
$route['person/new'] = 'persons/add_new_person';
$route['home/score/(:any)'] = 'students/scoresheet/$1';
//$route['home/9'] = 'students/view';
//$route['home/10'] = 'students/view_other';
//$route['home'] = 'home';
$route['agent/view'] = 'agents/view_agents';
$route['sponsor/view'] = 'sponsors/get_sponsors';
$route['department/view'] = 'departments/view_departments';
$route['department/new'] = 'departments/add_new_dept';
$route['league/new_league'] = 'leagues/create_league';
$route['sponsor/create_sponsor'] = 'sponsors/create_sponsor';
$route['players/(:any)'] = 'players/viewByPlayerType/$1';
$route['players'] = '';
$route['teams/create'] = 'teams/create';
$route['teams/(:any)'] = 'teams/view/$1';
$route['teams'] = 'teams';
//$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';

$route['default_controller'] = 'Login';
//$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;
