<?php
        defined('BASEPATH') OR exit('No direct script access allowed');

        $config = array(
                'add_student' => array(
                        array(
                                'field' => 'admission_no',
                                'label' => 'Admission No',
                                'rules' => 'trim|required|numeric|exact_length[7]|is_unique[student.admission_no]',
                                'errors' => array(
                                        'is_unique' => 'The student\'s admission_no already exists.',
                                ),
                        ),
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'required'
                        ),
                       
                   
                      array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'dob',
                                'label' => 'Date Of Birth',
                                'rules' => 'trim|required'
                        ),


 			array(
                                'field' => 'doa',
                                'label' => 'Date Of Admission',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'disabled',
                                'label' => 'Disabled',
                                'rules' => 'trim|required|in_list[disabled,No]'
                        ),
                        array(
                                'field' => 'disability',
                                'label' => 'Disability',
                                'rules' => 'trim|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'tribe_id',
                                'label' => 'Tribe',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'religion_id',
                                'label' => 'Religion',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'admission_date',
                                'label' => 'Admission Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'transferred',
                                'label' => 'Transferred',
                                'rules' => 'trim|required|in_list[transferred_in,selected]'
                        ),
                        array(
                                'field' => 'former_school',
                                'label' => 'Former School',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'dorm_id',
                                'label' => 'Dormitory',
                                'rules' => 'trim|required|numeric|min_length[1]',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Class',
                                'rules' => 'trim|required|alpha_dash',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        ),
                        array(
                                'field' => 'g_firstname',
                                'label' => 'Guardian Firstname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'g_middlename',
                                'label' => 'Guardian MiddleName',
                              
                        ),
                        array(
                                'field' => 'g_lastname',
                                'label' => 'Guardian Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'occupation',
                                'label' => 'Occupation',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'rel_type',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                               
                        ),
                        array(
                                'field' => 'phone_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][0-9][0-9]{6}$/]|is_unique[guardian.phone_no]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        ),
                        array(
                                'field' => 'p_address',
                                'label' => 'Guardian Address',
                                'rules' => 'trim|required|numeric|min_length[1]|max_length[9]'
                        ),
                        array(
                                'field' => 'p_region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        )
                ),
                'edit_student' => array(
                        array(
                                'field' => 'admission_no',
                                'label' => 'Admission No',
                                'rules' => 'trim|required|numeric|exact_length[7]'
                        ),
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'required'
                        ),
                        
 			
			array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'dob',
                                'label' => 'Date Of Birth',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'disabled',
                                'label' => 'Disabled',
                                'rules' => 'trim|required|in_list[disabled,No]'
                        ),
                        array(
                                'field' => 'disability',
                                'label' => 'Disability',
                                'rules' => 'trim|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'tribe_id',
                                'label' => 'Tribe',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'religion_id',
                                'label' => 'Religion',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'admission_date',
                                'label' => 'Admission Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'transferred',
                                'label' => 'Transferred',
                                'rules' => 'trim|required|in_list[transferred_in,selected]'
                        ),
                        array(
                                'field' => 'former_school',
                                'label' => 'Former School',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'g_firstname',
                                'label' => 'Guardian Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'g_middlename',
                                'label' => 'Guardian MiddleName',
                                'rules' => 'trim|alpha'
                        ),
                        array(
                                'field' => 'g_lastname',
                                'label' => 'Guardian Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'occupation',
                                'label' => 'Occupation',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'rel_type',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                                'rules' => 'trim|valid_email'
                        ),
                        array(
                                'field' => 'phone_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]',
                                'errors' => array(
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        ),
                        array(
                                'field' => 'p_address',
                                'label' => 'Guardian Address',
                                'rules' => 'trim|required|numeric|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'p_region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        )
                ),
                'add-edit_dormitory' => array(
                        array(
                                'field' => 'dorm_name',
                                'label' => 'Dormitory Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'dorm_location',
                                'label' => 'Dormitory Location',
                                'rules' => 'trim|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'dorm_capacity',
                                'label' => 'Dormitory Capacity',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        )
                ),
                'add-edit_asset' => array(
                        array(
                                'field' => 'asset_name',
                                'label' => 'Asset Name',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'asset_no',
                                'label' => 'Asset No',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'status',
                                'label' => 'Status',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|required'
                        )
                ),
                'add_student_to_dorm' => array(
                        array(
                                'field' => 'admission_no',
                                'label' => 'Student Name',
                                'rules' => 'trim|required'
                        )
                ),
                'shift_student_to_dorm' => array(
                        array(
                                'field' => 'dorm_id',
                                'label' => 'Dormitory Name',
                                'rules' => 'trim|required'
                        )
                ),
                'assign_dorm_master' => array(
                       /*array(
                                'field' => 'start_date',
                                'label' => 'Date',
                                'rules' => 'trim|required'
                        ),*/
                        array(
                                'field' => 't_name',
                                'label' => 'Teacher Name',
                                'rules' => 'trim|required'
                        )
                ),
                'assign_hod' => array(
                        array(
                                'field' => 'staff_id',
                                'label' => 'Staff Name',
                                'rules' => 'trim|required'
                        )
                ),
                'add-edit_department' => array(
                        array(
                                'field' => 'dept_name',
                                'label' => 'Department Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'dept_loc',
                                'label' => 'Department Location',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )
                ),
                'announcement' => array(
                        array(
                                'field' => 'group_id',
                                'label' => 'Group Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'heading',
                                'label' => 'Announcement Heading',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'msg',
                                'label' => 'Announcement',
                                'rules' => 'trim|required'
                        )
                ),
                'assign_tod' => array(
                        array(
                                'field' => 't_id',
                                'label' => 'Teacher Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'start_date',
                                'label' => 'Start Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        )
                ),
                'add-edit-topic' => array(
                        array(
                                'field' => 'topic_name',
                                'label' => 'Topic Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )
                ),
                'add-edit-subtopic' => array(
                        array(
                                'field' => 'subtopic_name',
                                'label' => 'SubTopic Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )
                ),
                'add-edit_ta' => array(
                        array(
                                'field' => 'subject_id',
                                'label' => 'Subject Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'teacher_id',
                                'label' => 'Teacher Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'class_id',
                                'label' => 'Class Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'stream',
                                'label' => 'Stream',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'start_date',
                                'label' => 'Start Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        )
                ),
                'change_password' => array(
                        array(
                                'field' => 'current_password',
                                'label' => 'Current Password',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'new_password',
                                'label' => 'Password',
                                'rules' => 'trim|required|min_length[8]'
                        ),
                        array(
                                'field' => 'confirm_new_password',
                                'label' => 'Password Confirmation',
                                'rules' => 'trim|required|matches[new_password]'
                        )
                ),
                'update_profile' => array(
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'middlename',
                                'label' => 'MiddleName',
                                'rules' => 'trim|alpha'
                        ),
                        array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'dob',
                                'label' => 'Date Of Birth',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'marital_status',
                                'label' => 'Marital Status',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                                'rules' => 'trim|valid_email'
                        ),
                        array(
                                'field' => 'phone_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]',
                                'errors' => array(
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        )
                ),
                'add-edit_qualification' => array(
                        array(
                                'field' => 'certification',
                                'label' => 'Certificattion',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'university',
                                'label' => 'University',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'year',
                                'label' => 'Year',
                                'rules' => 'trim|required|numeric|exact_length[4]'
                        )
                ),
                'add_score' => array(
                        array(
                                'field' => 'score[]',
                                'label' => 'All Fields',
                                'rules' => 'trim'
                        ),
                        array(
                                'field' => 'edate',
                                'label' => 'Exam Date',
                                'rules' => 'trim|required'
                        )
                ),
                'update_results' => array(
                        array(
                                'field' => 'score',
                                'label' => 'Marks Field',
                                'rules' => 'trim|required'
                        )
                ),
                'add_discipline' => array(
                        array(
                                'field' => 'admission_no',
                                'label' => 'Student Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'decision',
                                'label' => 'Decision',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'date',
                                'label' => 'Date',
                                'rules' => 'trim|required'
                        )
                ),
                'add-edit_division' => array(
                        array(
                                'field' => 'min_points',
                                'label' => 'Minimum Points',
                                'rules' => 'trim|required|numeric'
                        ),
                        array(
                                'field' => 'max_points',
                                'label' => 'Maximum Points',
                                'rules' => 'trim|required|numeric'
                        ),
                        array(
                                'field' => 'level',
                                'label' => 'Class Level',
                                'rules' => 'trim|required|in_list[A,O]'
                        ),
                        array(
                                'field' => 'division',
                                'label' => 'Division',
                                'rules' => 'trim|required|in_list[I,II,III,IV,0]'
                        )
                ),
                'add_cd' => array(
                        array(
                                'field' => 'nos',
                                'label' => 'Number of Subjects',
                                'rules' => 'trim|required|is_natural_no_zero'
                        )
                ),
                'add-edit_o_level_grade_system' => array(
                        array(
                                'field' => 'start_mark',
                                'label' => 'Start Mark',
                                'rules' => 'trim|required|decimal'
                        ),
                        array(
                                'field' => 'end_mark',
                                'label' => 'End Mark',
                                'rules' => 'trim|required|decimal'
                        ),
                        array(
                                'field' => 'grade',
                                'label' => 'Grade',
                                'rules' => 'trim|required|in_list[A,B+,B,C,D,F]'
                        ),
                        array(
                                'field' => 'unit',
                                'label' => 'Units',
                                'rules' => 'trim|required|numeric|exact_length[1]'
                        ),
                        array(
                                'field' => 'remarks',
                                'label' => 'Remarks',
                                'rules' => 'trim|required|in_list[Excellent,Very Good,Good,Fair,Pass,Fail]'
                        )                        
                ),
                'add-edit_a_level_grade_system' => array(
                        array(
                                'field' => 'start_mark',
                                'label' => 'Start Mark',
                                'rules' => 'trim|required|decimal'
                        ),
                        array(
                                'field' => 'end_mark',
                                'label' => 'End Mark',
                                'rules' => 'trim|required|decimal'
                        ),
                        array(
                                'field' => 'grade',
                                'label' => 'Grade',
                                'rules' => 'trim|required|in_list[A,B+,B,C,D,E,S,F]'
                        ),
                        array(
                                'field' => 'points',
                                'label' => 'Points',
                                'rules' => 'trim|required|numeric|exact_length[1]'
                        ),
                        array(
                                'field' => 'remarks',
                                'label' => 'Remarks',
                                'rules' => 'trim|required|in_list[Excellent,Very Good,Good,Fair,Pass,Satisfactory,Fail]'
                        )                        
                ),
                'add-edit_academic_year' => array(
                        array(
                                'field' => 'start_date',
                                'label' => 'Start Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'level',
                                'label' => 'Class Level',
                                'rules' => 'trim|required|in_list[A\'Level,O\'Level]',
                                'errors' => array(
                                        'required' => 'The Class Level field is required. Set to only appear in January (O"Level) or July (A"Level )',
                                ),
                        ),
                        array(
                                'field' => 'year',
                                'label' => 'Academic Year',
                                'rules' => 'trim|required',
                                'errors' => array(
                                        'required' => 'The Academic Year field is required. Set to only appear in January (O"Level) or July (A"Level )',
                                ),
                        )                       
                ),
                'add-edit_term' => array(
                        array(
                                'field' => 'begin_date',
                                'label' => 'Begin Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'term_name',
                                'label' => 'Term Name',
                                'rules' => 'trim|required|in_list[first_term,second_term]'
                        )                       
                ),
                'add-edit_class' => array(
                        array(
                                'field' => 'class_id',
                                'label' => 'Class ID',
                                'rules' => 'trim|required|in_list[C01,C02,C03,C04,C05,C06]'
                        ),
                        array(
                                'field' => 'class_name',
                                'label' => 'Class Name',
                                'rules' => 'trim|required|in_list[Form One,Form Two,Form Three,Form Four,Form Five,Form Six]'
                        ),
                        array(
                                'field' => 'level',
                                'label' => 'Class Level',
                                'rules' => 'trim|required|in_list[A\'Level,O\'Level]'
                        )                       
                ),
                'add-edit_stream' => array(
                        array(
                                'field' => 'stream',
                                'label' => 'Stream Name',
                                'rules' => 'trim|required|in_list[A,B,C,C1,C2,C3,D,E,E1,E2,E3,M,M1,M2,M3,PCM,PCB,EGM,PGM,CBG]'
                        )                      
                ),
                'add-edit_subject' => array(
                        array(
                                'field' => 'subject_id',
                                'label' => 'Subject ID',
                                'rules' => 'trim|required|regex_match[/SUB[0-9][0-9]$/]',
                                'errors' => array(
                                        'regex_match' => 'The subject_id should contain the word SUB followed by exactly two digits. Eg: SUB77',
                                ),
                        ),
                        array(
                                'field' => 'subject_name',
                                'label' => 'Subject Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'department_id',
                                'label' => 'Department Name',
                                'rules' => 'trim|required|numeric'
                        ),
                        array(
                                'field' => 'subject_category',
                                'label' => 'Subject Category',
                                'rules' => 'trim|required|in_list[T,N]'
                        ),
                        array(
                                'field' => 'subject_choice',
                                'label' => 'Subject Choice',
                                'rules' => 'trim|required|in_list[optional,compulsory]'
                        ),
                        array(
                                'field' => 'studied_by',
                                'label' => 'Class Level',
                                'rules' => 'trim|required|in_list[A,O,BOTH]'
                        ),
			array(
				'field' => 'subject_type',
				'label' => 'Subject Type',
				'rules' => 'trim|required|in_list[Core,Core_With_Penalty,Supplement]'
			)
                ),
                'add_staff' => array(
                        array(
                                'field' => 'staff_id',
                                'label' => 'Staff ID',
                                'rules' => 'trim|required|regex_match[/STAFF[0-9][0-9][0-9][0-9]/]|is_unique[staff.staff_id]',
                                'errors' => array(
                                        'is_unique' => 'This staff\'s ID already exists.',
                                        'regex_match' => 'The staff ID should contain the word STAFF followed by exactly four digits. E.g: STAFF0345',
                                ),
                        ),
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'middlename',
                                'label' => 'MiddleName',
                                'rules' => 'trim|alpha'
                        ),
                        array(
                                'field' => 'marital_status',
                                'label' => 'Marital Status',
                                'rules' => 'trim|required|in_list[Single,Married,Divorced,Widowed]'
                        ),
                        array(
                                'field' => 'staff_type',
                                'label' => 'Staff Type',
                                'rules' => 'trim|required|in_list[T,S,L,A]',
                                'errors' => array(
                                        'in_list' => 'The staff type field should be either of Teacher, Secretary, Librarian or Accountant',
                                ),
                        ),
                        /*array(
                                'field' => 'subject_id[]',
                                'label' => 'Subjects',
                                'rules' => 'trim|regex_match[/SUB[0-9][0-9]/]',
                                'errors' => array(
                                        'regex_match' => 'The subject_id should contain the word SUB followed by exactly two digits. Eg: SUB77',
                                ),
                        ),*/
                        array(
                                'field' => 'r_box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'r_region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'r_firstname',
                                'label' => 'NOK Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'r_lastname',
                                'label' => 'NOK Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'r_gender',
                                'label' => 'NOK Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'r_relationship',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|in_list[Husband,Wife,Brother,Sister,Other]'
                        ),
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                                'rules' => 'trim|valid_email|is_unique[staff.email]'
                        ),
                        array(
                                'field' => 'phone_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]|is_unique[staff.phone_no]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        ),
                        array(
                                'field' => 'r_phone',
                                'label' => 'NOK Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]|is_unique[staff_nok.phone_no]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        )
                ),
                'edit_staff' => array(
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'middlename',
                                'label' => 'MiddleName',
                                'rules' => 'trim|alpha'
                        ),
                        array(
                                'field' => 'marital_status',
                                'label' => 'Marital Status',
                                'rules' => 'trim|required|in_list[Single,Married,Divorced,Widowed]'
                        ),
                        /*array(
                                'field' => 'staff_type',
                                'label' => 'Staff Type',
                                'rules' => 'trim|required|in_list[T,S,L,A]',
                                'errors' => array(
                                        'in_list' => 'The staff type field should be either of Teacher, Secretary, Librarian or Accountant',
                                ),
                        ),
                        array(
                                'field' => 'subject_id[]',
                                'label' => 'Subjects',
                                'rules' => 'trim|regex_match[/SUB[0-9][0-9]/]',
                                'errors' => array(
                                        'regex_match' => 'The subject_id should contain the word SUB followed by exactly two digits. Eg: SUB77',
                                ),
                        ),*/
                        array(
                                'field' => 'r_box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'r_firstname',
                                'label' => 'NOK Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'r_lastname',
                                'label' => 'NOK Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'r_gender',
                                'label' => 'NOK Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'r_relationship',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|in_list[Husband,Wife,Brother,Sister,Other]'
                        ),
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                                'rules' => 'trim|valid_email'
                        ),
                        array(
                                'field' => 'phone_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        ),
                        array(
                                'field' => 'r_phone',
                                'label' => 'NOK Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        )
                ),
                'add_admin_group' => array(
                        array(
                                'field' => 'group_name',
                                'label' => 'Group Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'password',
                                'label' => 'Password',
                                'rules' => 'trim|required|min_length[8]'
                        )
                ),
                'add_staff_to_group' => array(
                        array(
                                'field' => 'staff_id[]',
                                'label' => 'Staff Names',
                                'rules' => 'trim'
                        )
                ),
                'edit_admin_group' => array(
                        array(
                                'field' => 'permission_id[]',
                                'label' => 'Permissions',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'group_name',
                                'label' => 'Group Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )
                ),
                'add-edit_staff_group' => array(
                        array(
                                'field' => 'group_name',
                                'label' => 'Group Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )
                ),
                'add_or_remove_staff_to_role' => array(
                        array(
                                'field' => 'staff_id[]',
                                'label' => 'Staff Name',
                                'rules' => 'trim'
                        )
                ),
                'add_class_stream' => array(
                        array(
                                'field' => 'class_id',
                                'label' => 'Class Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'stream_id_name',
                                'label' => 'Stream Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Combination',
                                'rules' => 'trim|in_list[C05PCM,C05PGM,C05EGM,C05PCB,C05CBG,C05ECA,C05HKL,C05HGE,C05HGK,C05HGL,C06PCM,C06PGM,C06EGM,C06PCB,C06CBG,C06ECA,C06HKL,C06HGE,C06HGK,C06HGL,C01A,C01B,C01C,C01D,C02C,C02C1,C02C2,C02E,C02E1,C02E2,C02M,C02M1,C02M2,C03C,C03C1,C03C2,C03E,C03E1,C03E2,C03M,C03M1,C03M2,C04C,C04C1,C04C2,C04E,C04E1,C04E2,C04M,C04M1,C04M2]',
                                'errors' => array(
                                        'in_list' => 'Invalid %s of Class Name and Stream',
                                ),
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'capacity',
                                'label' => 'Capacity',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[2]'
                        ),
                        array(
                                'field' => 'nos',
                                'label' => 'Number Of Subjects',
                                'rules' => 'trim|required|is_natural_no_zero'
                        )                      
                ),
                'edit_class_stream' => array(                        
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'capacity',
                                'label' => 'Capacity',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[2]'
                        ),
                        array(
                                'field' => 'nos',
                                'label' => 'Number Of Subjects',
                                'rules' => 'trim|required|is_natural_no_zero'
                        )                   
                ),
                'assign_subject_to_class_stream' => array(
                        array(
                                'field' => 'subject_id[]',
                                'label' => 'Subjects',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'class_id',
                                'label' => 'Class Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'stream_id',
                                'label' => 'Stream',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Combination',
                                'rules' => 'trim|in_list[C05PCM,C05PGM,C05EGM,C05PCB,C05CBG,C05ECA,C05HKL,C05HGE,C05HGK,C05HGL,C06PCM,C06PGM,C06EGM,C06PCB,C06CBG,C06ECA,C06HKL,C06HGE,C06HGK,C06HGL,C01A,C01B,C01C,C01D,C02C,C02C1,C02C2,C02E,C02E1,C02E2,C02M,C02M1,C02M2,C03C,C03C1,C03C2,C03E,C03E1,C03E2,C03M,C03M1,C03M2,C04C,C04C1,C04C2,C04E,C04E1,C04E2,C04M,C04M1,C04M2]',
                                'errors' => array(
                                        'in_list' => 'Invalid %s of Class Name and Stream',
                                ),
                        )
                ),
                'update_assign_subject_to_class_stream' => array(
                        array(
                                'field' => 'subject_id[]',
                                'label' => 'Subjects',
                                'rules' => 'trim|required'
                        )
                ),
                'add_tod_routine' => array(
                        array(
                                'field' => 'date',
                                'label' => 'Date',
                                'rules' => 'trim|required|is_unique[tod_daily_routine_tbl.date_]',
                                'errors' => array(
                                        'is_unique' => "The Report of this date you are trying to enter already exist",
                                ),
                        ),
                        array(
                                'field' => 'wakeup',
                                'label' => 'Wake Up Time',
                                'rules' => 'trim|required|regex_match[/0[0-9]:[0-5][0-9]:[0-5][0-9]/]',
                                'errors' => array(
                                        'regex_match' => "Invalid Time Format: Time should be in 0H:MM:SS format",
                                ),
                        ),
                        array(
                                'field' => 'parade',
                                'label' => 'Parade Time',
                                'rules' => 'trim|required|regex_match[/[01][0-9]:[0-5][0-9]:[0-5][0-9]/]',
                                'errors' => array(
                                        'regex_match' => "Invalid Time Format: Time should be in HH:MM:SS format",
                                ),
                        ),
                        array(
                                'field' => 'class',
                                'label' => 'Start Classes',
                                'rules' => 'trim|required|regex_match[/[01][0-9]:[0-5][0-9]:[0-5][0-9]/]',
                                'errors' => array(
                                        'regex_match' => "Invalid Time Format: Time should be in HH:MM:SS format",
                                ),
                        ),
                        array(
                                'field' => 'break',
                                'label' => 'Breakfast',
                                'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]'
                        ),
                        array(
                                'field' => 'lunch',
                                'label' => 'Lunch',
                                'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]'
                        ),
                        array(
                                'field' => 'dinner',
                                'label' => 'Dinner',
                                'rules' => 'trim|required|alpha_numeric_spaces|max_length[100]'
                        ),
                        array(
                                'field' => 'events',
                                'label' => 'Events',
                                'rules' => 'trim'
                        ),
                        array(
                                'field' => 'security',
                                'label' => 'Security',
                                'rules' => 'trim'
                        )
                ),
                'filter_timetable' => array(
                        array(
                                'field' => 'class_name',
                                'label' => 'Class Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'stream',
                                'label' => 'Stream',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'csid',
                                'label' => 'Combination',
                                'rules' => 'trim|in_list[C05PCM,C05PGM,C05EGM,C05PCB,C05CBG,C05ECA,C05HKL,C05HGE,C05HGK,C05HGL,C06PCM,C06PGM,C06EGM,C06PCB,C06CBG,C06ECA,C06HKL,C06HGE,C06HGK,C06HGL,C01A,C01B,C01C,C01D,C02C,C02C1,C02C2,C02E,C02E1,C02E2,C02M,C02M1,C02M2,C03C,C03C1,C03C2,C03E,C03E1,C03E2,C03M,C03M1,C03M2,C04C,C04C1,C04C2,C04E,C04E1,C04E2,C04M,C04M1,C04M2]',
                                'errors' => array(
                                        'in_list' => 'Invalid %s of Class Name and Stream',
                                ),
                        )
                ),
                'edit_exam_type' => array(
                        array(
                                'field' => 'exam_name',
                                'label' => 'Exam Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'start_date',
                                'label' => 'Start Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        )
                ),
                'assign_leader' => array(
                        array(
                                'field' => 'admission_no',
                                'label' => 'Student Name',
                                'rules' => 'trim|required'
                        )                      
                ),
                'add_student_attendance' => array(
                        array(
                                'field' => 'admission_no[]',
                                'label' => 'Names',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'date',
                                'label' => 'Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Class Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'attendance[]',
                                'label' => 'Attendance',
                                'rules' => 'trim|required'
                        )
                ),
                'add_staff_attendance' => array(
                        array(
                                'field' => 'staff_id[]',
                                'label' => 'Names',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'date',
                                'label' => 'Date',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'attendance[]',
                                'label' => 'Attendance',
                                'rules' => 'trim|required'
                        )
                ),
                'add_class_journal' => array(
                        array(
                                'field' => 'subject_id[]',
                                'label' => 'Subject Name',
                                'rules' => 'trim'
                        ),
                        array(
                                'field' => 'subtopic[]',
                                'label' => 'SubTopic',
                                'rules' => 'trim'
                        ),
                        array(
                                'field' => 'absentees[]',
                                'label' => 'Number of Absentees',
                                'rules' => 'trim|required|numeric'
                        ),
                        array(
                                'field' => 'status[]',
                                'label' => 'Status',
                                'rules' => 'trim|required|in_list[taught,untaught,none]'
                        )
                ),
                'add-edit_timetable' => array(
                        array(
                                'field' => 'monday_subject[]',
                                'label' => 'Monday Field(s)',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'tuesday_subject[]',
                                'label' => 'Tuesday Field(s)',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'wednesday_subject[]',
                                'label' => 'Wednesday Field(s)',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'thursday_subject[]',
                                'label' => 'Thursday Field(s)',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'friday_subject[]',
                                'label' => 'Friday Field(s)',
                                'rules' => 'trim|required'
                        )
                ),
                'add-edit_new_role' => array(
                        array(
                                'field' => 'role_name',
                                'label' => 'Role Name',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim'
                        )                    
                ),
                'manage_role' => array(
                        array(
                                'field' => 'permission[]',
                                'label' => 'Permission',
                                'rules' => 'trim'
                        )                   
                ),
                'add_log' => array(
                        array(
                                'field' => 'topic_id',
                                'label' => 'Topic Name',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'start_date',
                                'label' => 'Begin Date',
                                'rules' => 'trim|required'
                        )                  
                ),
                'add-edit_period' => array(
                        array(
                                'field' => 'period_no',
                                'label' => 'Period No',
                                'rules' => 'trim|required|numeric|is_natural_no_zero'
                        ),
                        array(
                                'field' => 'start_time',
                                'label' => 'Start Time',
                                'rules' => 'trim|required|regex_match[/[012][0-9]:[0-5][0-9]/]',
                                'errors' => array(
                                        'regex_match' => "Invalid Time Format: Time should be in HH:MM format",
                                ),
                        ),
                        array(
                                'field' => 'end_time',
                                'label' => 'End Time',
                                'rules' => 'trim|required|regex_match[/[012][0-9]:[0-5][0-9]/]',
                                'errors' => array(
                                        'regex_match' => "Invalid Time Format: Time should be in HH:MM format",
                                ),
                        )                
                ),
                'add-edit_break' => array(
                        array(
                                'field' => 'after_period_no',
                                'label' => 'Period No',
                                'rules' => 'trim|required|numeric|is_natural_no_zero'
                        ),
                        array(
                                'field' => 'description',
                                'label' => 'Description',
                                'rules' => 'trim|required'
                        )                
                ),
                'edit_attendance' => array(
                        array(
                                'field' => 'att_id',
                                'label' => 'Attendance Type',
                                'rules' => 'trim|required'
                        )             
                ),
                'add_subject_teacher' => array(
                        array(
                                'field' => 'subject_id[]',
                                'label' => 'Subject Name',
                                'rules' => 'trim'
                        )
                ),'add_new_selected' => array(
                        array(
                                'field' => 'exam_no',
                                'label' => 'Exam No',
                                'rules' => 'trim|required|is_unique[selected_students.examination_no]',
                                'errors' => array(
                                        'is_unique' => 'The examination_no already exists.',
                                ),
                        ),
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'dob',
                                'label' => 'Date Of Birth',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'form',
                                'label' => 'Form',
                                'rules' => "trim|required|in_list[I-O'level,V-A'level]",
                                'errors' => array(
                                        'is_unique' => 'The Form should be either Form One or Form Five.',
                                ),
                        ),
                        array(
                                'field' => 'nationality',
                                'label' => 'Nationality',
                                'rules' => 'trim|required'
                        )/*,
                        array(
                                'field' => 'tribe_id',
                                'label' => 'Tribe',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'religion_id',
                                'label' => 'Religion',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        )*/,
                        array(
                                'field' => 'box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'former_school',
                                'label' => 'Former School',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'district',
                                'label' => 'District',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'ward',
                                'label' => 'Ward',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )/*,
                        array(
                                'field' => 'dorm_id',
                                'label' => 'Dormitory',
                                'rules' => 'trim|required|numeric|min_length[1]',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Class',
                                'rules' => 'trim|required|alpha_dash',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        )*/,
                        array(
                                'field' => 'g_firstname',
                                'label' => 'Guardian Firstname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'g_lastname',
                                'label' => 'Guardian Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'orphan',
                                'label' => 'Orphan',
                                'rules' => 'trim|required|in_list[yes,no]'
                        )/*,
                        array(
                                'field' => 'occupation',
                                'label' => 'Occupation',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'rel_type',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|alpha'
                        )*/,
                        array(
                                'field' => 'email',
                                'label' => 'Email',

                        ),
                        array(
                                'field' => 'tel_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]|is_unique[selected_students.tel_no]',
                                'errors' => array(
                                        'is_unique' => 'This phone_no is already used by another person.',
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        )/*,
                        array(
                                'field' => 'p_address',
                                'label' => 'Guardian Address',
                                'rules' => 'trim|required|numeric|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'p_region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        )*/
                ),'edit_selected_student' => array(
                        array(
                                'field' => 'exam_no',
                                'label' => 'Exam No',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'firstname',
                                'label' => 'Firstname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'lastname',
                                'label' => 'Lastname',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'dob',
                                'label' => 'Date Of Birth',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'gender',
                                'label' => 'Gender',
                                'rules' => 'trim|required|in_list[Male,Female]'
                        ),
                        array(
                                'field' => 'form',
                                'label' => 'Form',
                                'rules' => "trim|required|in_list[I-O'level,V-A'level]"
                        ),
                        array(
                                'field' => 'nationality',
                                'label' => 'Nationality',
                                'rules' => 'trim|required'
                        )/*,
                        array(
                                'field' => 'tribe_id',
                                'label' => 'Tribe',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        ),
                        array(
                                'field' => 'religion_id',
                                'label' => 'Religion',
                                'rules' => 'trim|required|numeric|min_length[1]'
                        )*/,
                        array(
                                'field' => 'box',
                                'label' => 'Box',
                                'rules' => 'trim|required|is_natural_no_zero|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        ),
                        array(
                                'field' => 'former_school',
                                'label' => 'Former School',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'district',
                                'label' => 'District',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        ),
                        array(
                                'field' => 'ward',
                                'label' => 'Ward',
                                'rules' => 'trim|required|alpha_numeric_spaces'
                        )/*,
                        array(
                                'field' => 'dorm_id',
                                'label' => 'Dormitory',
                                'rules' => 'trim|required|numeric|min_length[1]',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        ),
                        array(
                                'field' => 'class_stream_id',
                                'label' => 'Class',
                                'rules' => 'trim|required|alpha_dash',
                                'errors' => array(
                                        'required' => 'A student must be assigned a %s.',
                                ),
                        )*/,
                        array(
                                'field' => 'g_firstname',
                                'label' => 'Guardian Firstname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'g_lastname',
                                'label' => 'Guardian Lastname',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'orphan',
                                'label' => 'Orphan',
                                'rules' => 'trim|required|in_list[yes,no]'
                        )/*,
                        array(
                                'field' => 'occupation',
                                'label' => 'Occupation',
                                'rules' => 'trim|required|alpha'
                        ),
                        array(
                                'field' => 'rel_type',
                                'label' => 'Relationship',
                                'rules' => 'trim|required|alpha'
                        )*/,
                        array(
                                'field' => 'email',
                                'label' => 'Email',
                                'rules' => 'trim|valid_email'
                        ),
                        array(
                                'field' => 'tel_no',
                                'label' => 'Phone No',
                                'rules' => 'trim|required|regex_match[/\+255[67][1-9][1-9][0-9]{6}$/]',
                                'errors' => array(
                                        'regex_match' => 'Invalid format, the phone_no should be in the following format. E.g: +255755310473',
                                ),
                        )/*,
                        array(
                                'field' => 'p_address',
                                'label' => 'Guardian Address',
                                'rules' => 'trim|required|numeric|min_length[1]|max_length[5]'
                        ),
                        array(
                                'field' => 'p_region',
                                'label' => 'Region',
                                'rules' => 'trim|required|alpha_dash'
                        )*/
                ),
                'add_topic_end_date' => array(
                        array(
                                'field' => 'comment',
                                'label' => 'Comment',
                                'rules' => 'trim|required'
                        ),
                        array(
                                'field' => 'end_date',
                                'label' => 'End Date',
                                'rules' => 'trim|required'
                        )
                )
        );
?>
