<?php
	class Admin_model extends MY_Model{

		public function __construct(){
			$this->load->database();
		}

		public function update_password($id, $password_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('admin', $password_record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE PASSWORD', 'ADMIN');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_current_password($id, $hashed_current_password){
			$q = $this->db->get_where('admin', array('id' => $id, 'password' => $hashed_current_password));
			return $q->num_rows();
		}

		public function select_audit_student_results($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('student_names', $value);
				$this->db->or_like('subject_name', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('exam_name', $value);
				$this->db->or_like('year', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_student_results_view');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'AUDIT STUDENT RESULTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_audit_student_results($value){
			if(!empty($value)){
	            $this->db->like('student_names', $value);
				$this->db->or_like('subject_name', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('exam_name', $value);
				$this->db->or_like('year', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_student_results_view');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}


		public function select_audit_data_student_attendance($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('att_desc', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_student_attendance_view');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        //Activity Log
	         $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'AUDIT STUDENT ATTENDANCE');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_audit_student_attendance_data($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('att_desc', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_student_attendance_view');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_audit_data_staff_attendance($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('att_desc', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_staff_attendance_view');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'AUDIT STAFF ATTENDANCE');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_audit_staff_attendance_data($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('added_changed_by', $value);
				$this->db->or_like('ip_address', $value);
				$this->db->or_like('change_time', $value);
				$this->db->or_like('change_type', $value);
				$this->db->or_like('att_desc', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('audit_staff_attendance_view');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function get_continous_results($term_id, $etype_id, $class_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$rs = $this->db->query('SELECT * FROM student_subject_assessment WHERE etype_id='.$this->db->escape($etype_id).' AND term_id='.$this->db->escape($term_id).' AND class_stream_id IN(SELECT class_stream_id FROM class_stream WHERE class_id='.$this->db->escape($class_id).')');

	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CONTINOUS RESULTS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function get_students_for_continous_results($term_id, $etype_id, $class_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student.admission_no LIKE '%$value%')");
	        }


			$this->db->select('(SELECT COUNT(*) FROM student_subject_assessment WHERE etype_id='.$this->db->escape($etype_id).' AND term_id='.$this->db->escape($term_id).' AND class_stream_id IN(SELECT class_stream_id FROM class_stream WHERE class_id='.$this->db->escape($class_id).') AND admission_no=student.admission_no) AS number_of_subjects, student_subject_assessment.*, student.firstname, student.lastname, student_subject_assessment.admission_no');
			$this->db->from("student");
			$this->db->join("student_subject_assessment", "student_subject_assessment.admission_no = student.admission_no");
			$this->db->where('etype_id='.$this->db->escape($etype_id).' AND term_id='.$this->db->escape($term_id).'');
			$this->db->where('student_subject_assessment.class_stream_id IN(SELECT class_stream_id FROM class_stream WHERE class_id='.$this->db->escape($class_id).')');
			$this->db->group_by('admission_no');
			$this->db->limit($limit, $offset);

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function edit_student_results($admission_no, $etype_id, $term_id){
			$this->db->select('student_subject_assessment.*, subject_name, student.firstname, student.lastname');
			$this->db->from('student_subject_assessment');
			$this->db->join('student', 'student.admission_no = student_subject_assessment.admission_no');
			$this->db->join('subject', 'student_subject_assessment.subject_id = subject.subject_id');
			$this->db->where('student_subject_assessment.admission_no', $admission_no);
			$this->db->where('etype_id', $etype_id);
			$this->db->where('term_id', $term_id);

			$rs = $this->db->get();
			return $rs->result_array();		

			return $this->feedbackMsg();	
		}

		public function update_student_results($admission_no, $etype_id, $term_id, $subject_id, $student_marks, $csid, $cid){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			/*$this->db->query('UPDATE student_subject_assessment SET marks='.$this->db->escape($student_marks['marks']).' WHERE subject_id='.$this->db->escape($subject_id).' AND term_id='.$this->db->escape($term_id).' AND admission_no='.$this->db->escape($admission_no).' AND etype_id='.$this->db->escape($etype_id).'');*/

			$this->CI_mySQL('student_subject_assessment', $student_marks, array('subject_id' => $subject_id, 'term_id' => $term_id, 'admission_no' => $admission_no, 'etype_id' => $etype_id), 'UPDATE');

			//$this->db->query('CALL calculate_subject_rank_procedure('.$this->db->escape($subject_id).', '.$this->db->escape($term_id).', '.$this->db->escape($csid).','.$this->db->escape($cid).')');
			$this->CI_getProcedureError();

			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'RESULTS');

			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function count_all_academic_year_for_results($value){
			if(!empty($value)){
	            $this->db->like('term_name', $value);
				$this->db->or_like('year', $value);
				$this->db->or_like('class_level', $value);
	        }

	        $this->db->select('academic_year.*, term_id, term_name');
	        $this->db->from('academic_year');
	        $this->db->join('term', 'term.aid = academic_year.id');
	        $this->db->where('id IN(SELECT aid FROM term WHERE term_id IN(SELECT term_id FROM student_subject_assessment))');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_years_for_results($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('term_name', $value);
				$this->db->or_like('year', $value);
				$this->db->or_like('class_level', $value);
	        }

	        $this->db->select('academic_year.*, term_id, term_name');
	        $this->db->from('academic_year');
	        $this->db->join('term', 'term.aid = academic_year.id');
	        $this->db->where('id IN(SELECT aid FROM term WHERE term_id IN(SELECT term_id FROM student_subject_assessment))');
	        $this->db->order_by('year', 'ASC');
	        $this->db->limit($limit, $offset);	        

	        $rs = $this->db->get();

	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'MATOKEO');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_all_students($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();

			//return $this->db->count_all('active_students_view');
		}

		public function fetch_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get();
	        return $rs->result_array();			

	        $rs = $this->db->get();
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'REGISTERED STUDENTS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_department($value){
			if(!empty($value)){
	            $this->db->like('dept_name', $value);
				$this->db->or_like('dept_type', $value);
				$this->db->or_like('firstname', $value);
				$this->db->or_like('lastname', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('department_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_departments($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('dept_name', $value);
				$this->db->or_like('dept_type', $value);
				$this->db->or_like('firstname', $value);
				$this->db->or_like('lastname', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('department_view');
	        $this->db->order_by('dept_id','ASC');
	        $this->db->limit($limit, $offset);	 

	        $rs = $this->db->get();
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DEPARTMENTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function insert_new_department($department_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('department', $department_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DEPARTMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_department_by_id($dept_id){
			$this->db->select('*');
			$this->db->from('department');
			$this->db->where('dept_id', $dept_id);

			$q = $this->db->get();
			return $q->row_array();
		}

		
		//END DEPARTMENT

		//START DORMITORY
		public function count_dormitory($value){
			if(!empty($value)){
	            $this->db->like('dorm_name', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('dormitory');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_dormitories($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('dorm_name', $value);
	        }

	        $this->db->select('dorm_id, dorm_name, location, capacity, admission_no, concat(firstname, " ", lastname) AS names');
	        $this->db->from('dormitory');
	        $this->db->join('teacher', 'dormitory.teacher_id = teacher.staff_id', 'left');	
			$this->db->join('staff', 'staff.staff_id = teacher.staff_id', 'left');
	        $this->db->order_by('dorm_id','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DORMITORIES');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function insert_new_dormitory($dormitory_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dormitory', $dormitory_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_dormitory_by_id($dorm_id){
			$this->db->select('*');
			$this->db->from('dormitory');

			$this->db->where('dorm_id', $dorm_id);

			$q = $this->db->get();
			return $q->row_array();
		}

		public function update_dormitory($dorm_id, $dormitory_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dormitory', $dormitory_record, array('dorm_id' => $dorm_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_dormitory($dorm_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dormitory', NULL, array('dorm_id' => $dorm_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
		//END DORMITORY

		public function count_class_level(){
			return $this->db->count_all('class_level');
		}

		public function fetch_class_level($limit, $offset){
			$this->db->limit($limit, $offset);
			$q = $this->db->get('class_level');

			//Activity Log
			//$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'class_level');

			if($q->num_rows() > 0){
				return $q->result_array();
			}
			else{
				return $q->result_array();
			}
		}


		public function select_class_level_by_id($class_id){
			$this->db->select('*');
			$this->db->from('class_level');
			$this->db->where('class_id', $class_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_class($class_id, $class_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_level', $class_record, array('class_id' => $class_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CLASS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_new_class($class_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_level', $class_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'CLASS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_class($class_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_level', NULL, array('class_id' => $class_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'CLASS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//========================SUBJECTS======================
		public function count_subjects($value){
			if(!empty($value)){
	            $this->db->like('subject_name', $value);
				$this->db->or_like('subject_category', $value);
				$this->db->or_like('subject_choice', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('subject');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_subjects($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('subject_name', $value);
				$this->db->or_like('subject_category', $value);
				$this->db->or_like('subject_choice', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('subject_department_view');
	        $this->db->order_by('subject_name','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SUBJECTS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function fetch_subjects_for_use(){
	        $this->db->select('*');
	        $this->db->from('subject_department_view');
	        $this->db->order_by('subject_name','ASC');

	        $rs = $this->db->get();
			return $rs->result_array();
		}


		public function select_subject_by_id($subject_id){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('subject_id', $subject_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_subject($subject_id, $subject_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('subject', $subject_record, array('subject_id' => $subject_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_new_subject($subject_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('subject', $subject_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_subject($subject_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('subject', NULL, array('subject_id' => $subject_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
			/*$this->db->where('subject_id', $subject_id);
			$this->db->delete('subject');*/			
		}

		public function insert_new_stream($stream_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('stream', $stream_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_stream_by_id($stream_id){
			$this->db->select('*');
			$this->db->from('stream');
			$this->db->where('stream_id', $stream_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function count_streams($value){
			if(!empty($value)){
	            $this->db->like('stream_name', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('stream');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_stream($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			
			if(!empty($value)){
	            $this->db->like('stream_name', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('stream');
	        $this->db->order_by('stream_name','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	         //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STREAM');       
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function update_stream($stream_id, $stream_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('stream_id', $stream_id);
			$this->db->update('stream', $stream_record);
			$this->CI_mySQL('stream', $stream_record, array('stream_id' => $stream_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_stream($stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('stream', NULL, array('stream_id' => $stream_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_new_class_stream($class_stream_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream', $class_stream_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'CLASS_STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_class_stream($class_stream_id, $class_stream_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream', $class_stream_record, array('class_stream_id' => $class_stream_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CLASS_STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_class_stream_by_id($class_stream_id){
			$this->db->select('class_stream.*, class_level.class_name, level');
			$this->db->from('class_stream');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}


		public function delete_class_stream($class_stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream', NULL, array('class_stream_id' => $class_stream_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'CLASS_STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//CLASS STREAM SUBJECT
		public function count_class_stream_subjects($value){
			if(!empty($value)){
	            $this->db->like('stream', $value);
				$this->db->or_like('class_name', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('class_stream_subject_display_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_class_stream_subjects($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('stream', $value);
				$this->db->or_like('class_name', $value);
	        }

	        //class_stream_subject_display_view is for displaying class_stream_subjects

	        $this->db->select('*');
	        $this->db->from('class_stream_subject_display_view');
	        $this->db->order_by('class_id','ASC');
	        $this->db->order_by('stream','ASC');
	        $this->db->limit($limit, $offset);
	        
	        $rs = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS_STREAM_SUBJECT');
	        $this->db->trans_complete();

	        return $rs->result_array();
		}

		public function insert_assign_subject($assign_subject_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream_subject', $assign_subject_record, NULL, 'INSERT_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'CLASS_STREAM_SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function select_class_stream_subjects_by_class_stream_id($class_stream_id){
			//!!!!!!!!!!!!!The function is for returning the array of all subject per class_stream to be used to echo in the select element for subjects in edit_class_stream_subjects
			$this->db->select('*');
			$this->db->from('class_stream_subject_view');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();			
			return $query->result_array();
		}

		public function select_class_stream_subject_by_class_stream_id($class_stream_id){
			//!!!!!!!This function is for obtaiining the single value for edit_class_stream_subjects to be used in 
			//class_name and stream select elements
			//NOTE: They differ by an S in 'lass_stream_subject(s)'
			$this->db->select('*');
			$this->db->from('class_stream_subject_view');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_assigned_subject($class_stream_id, $assign_subject_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream_subject', $assign_subject_record, array('class_stream_id' => $class_stream_id), 'UPDATE_BATCH');
			$this->db->query('CALL after_update_class_stream_subject_procedure("'.$class_stream_id.'")');
			$this->CI_getProcedureError();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CLASS_STREAM_SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function delete_class_stream_subject($class_stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_stream_subject', NULL, array('class_stream_id' => $class_stream_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'CLASS_STREAM_SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_subject_teacher_by_teacher_id($teacher_id){
			$this->db->select('*');
			$this->db->from('subject_teacher_view');
			$this->db->where('staff_id', $teacher_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_user_role_by_id($teacher_id){
			$this->db->select('*');
			$this->db->from('subject_teacher_view');
			$this->db->where('staff_id', $teacher_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function reset_all_passwords(){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');	

			$this->db->query("UPDATE staff SET password=(SELECT password FROM password_reset_tbl)");
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'RESET_ALL_PASSWORDS', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
			//$this->session->set_flashdata('success_message', 'All passwords have been reset to default password');
		}

		public function reset_password($record, $staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE staff SET password=(SELECT password FROM password_reset_tbl) WHERE staff_id='.$this->db->escape($staff_id).'');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'RESET_PASSWORD', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
			//$this->session->set_flashdata('success_message', 'Password successfully reset');
		}

		public function insert_remove_deactivation_reason($inactive_record, $staff_id){
			$this->db->trans_start();
			if($this->input->post('reason')){
				$this->db->insert('inactive_staff', $inactive_record);
			}
			else{
				$this->db->where('staff_id', $staff_id);
			        $this->db->delete('inactive_staff');
			}
			$this->db->trans_complete();
			
			if($this->db->trans_status() === TRUE){
				return TRUE;
			}
			else{
				$this->db->trans_rollback();
		    	return FALSE;
		    }
		}

		public function delete_staff($staff_id, $data){
			/*$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');			
			$this->db->where('staff_id', $staff_id);
			$this->db->update('staff', $data);
			$this->db->trans_complete();*/

			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('staff', $data, array('staff_id' => $staff_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'staff');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		//START EXAMS
		public function select_exam_types($etid = FALSE){
			$this->db->select('*');
			$this->db->from('exam_type');

			if($etid === FALSE){
				$q = $this->db->get();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'EXAM_TYPES');
				return $q->result_array();
			}

			$this->db->where('id', $etid);
			$q = $this->db->get();			
			return $q->row_array();
		}

		public function insert_new_exam_type(/*$exam_type_record,*/ $exam_name){
			//return $this->db->insert('exam_type', $exam_type_record);
			//return $this->db->query('CALL set_academic_year_on_exam_type_procedure("'.$exam_name.'")');
		}

		public function update_exam_type($etid, $exam_name, $start_date, $end_date){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'EXAM_TYPE');

			$this->db->query('UPDATE exam_type SET start_date='.$this->db->escape($start_date).', end_date='.$this->db->escape($end_date).', exam_name='.$this->db->escape($exam_name).' WHERE id='.$this->db->escape($etid).'');
			//$this->session->set_flashdata('success_message', 'Exam type successfully updated');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_exam_type($etid){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('exam_type', NULL, array('etid' => $etid), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'EXAM_TYPE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_o_grade($grade = FALSE){
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('o_level_grade_system_tbl');

			if($grade === FALSE){
				$q = $this->db->get();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'O_LEVEL_GRADE_SYSTEM');
				return $q->result_array();
			}

			$this->db->where('grade', $grade);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function select_o_grade_record_by_id($id){
			$this->db->select('*');
			$this->db->from('o_level_grade_system_tbl');

			$this->db->where('id', $id);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function insert_new_o_grade($o_grade_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('o_level_grade_system_tbl', $o_grade_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'O_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_o_grade($id, $o_grade_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('o_level_grade_system_tbl', $o_grade_record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'O_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_o_grade($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('o_level_grade_system_tbl', NULL, array('id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'O_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_a_grade($grade = FALSE){
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('a_level_grade_system_tbl');

			if($grade === FALSE){
				$q = $this->db->get();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'A_LEVEL_GRADE_SYSTEM');
				return $q->result_array();
			}

			$this->db->where('grade', $grade);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function select_a_grade_record_by_id($id){
			$this->db->select('*');
			$this->db->from('a_level_grade_system_tbl');

			$this->db->where('id', $id);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function insert_new_a_grade($a_grade_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('a_level_grade_system_tbl', $a_grade_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'A_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_a_grade($id, $a_grade_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('a_level_grade_system_tbl', $a_grade_record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'A_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_a_grade($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('a_level_grade_system_tbl', NULL, array('id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'A_LEVEL_GRADE_SYSTEM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_division(){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('division');
			$this->db->where('id !=', '1');

			$q = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DIVISION');
			$this->db->trans_complete();

			return $q->result_array();
		}

		public function select_computing_division($level = FALSE){
			$this->db->select('*');
			$this->db->from('compute_division');

			if($level === FALSE){
				$q = $this->db->get();			
				return $q->result_array();
			}

			$this->db->where('level', $level);
			$rs = $this->db->get();
			return $rs->row_array();
		}

		public function select_division_record_by_level($level){
			$this->db->select('div_name');
			$this->db->from('division');

			$this->db->where('level', $level);
			$q = $this->db->get();
			return $q->result_array();
		}

		public function insert_new_division($division_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('division', $division_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DIVISION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function add_nos_for_computing_division($record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('REPLACE INTO compute_division (level, nos, penalty, penalty_divs, assignedDiv, penalty_grade) VALUES('.$this->db->escape($record['level']).', '.$this->db->escape($record['nos']).', '.$this->db->escape($record['penalty']).', '.$this->db->escape($record['penalty_divs']).', '.$this->db->escape($record['assignedDiv']).', '.$this->db->escape($record['penalty_grade']).')');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DIVISION COMPUTING NOS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function get_penalty_divs($lvl = FALSE){
			$this->db->select('*');
			$this->db->from('compute_division');

			if($lvl === FALSE){
				$q = $this->db->get();
				return $q->result_array();
			}

			$this->db->where('level', $lvl);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function select_division_record_by_id($id){
			$this->db->select('*');
			$this->db->from('division');

			$this->db->where('id', $id);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function update_division($id, $division_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('division', $division_record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DIVISION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_division($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('division', NULL, array('id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DIVISION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
		//END EXAMS

		//START ACADEMIC YEAR
		public function select_all_academic_years($status = FALSE){
			$this->db->select('*');
			$this->db->from('academic_year');

			if($status === FALSE){
				$q = $this->db->get();
				return $q->result_array();
			}

			$this->db->where('status', $status);
			$q = $this->db->get();
			return $q->result_array();
		}

		public function select_class_names_by_academic_year($year){
			$this->db->select('DISTINCT(class_name), class_id');
			$this->db->from('all_results_view');
			$this->db->where('year', $year);
			$this->db->order_by('class_id', 'ASC');

			$q = $this->db->get();
			return $q->result_array();
		}


		public function select_term_name_by_academic_year($year){
			$this->db->select('DISTINCT(term_name), term_id');
			$this->db->from('all_results_view');
			$this->db->where('year', $year);
			$this->db->order_by('term_name', 'ASC');

			$q = $this->db->get();
			return $q->result_array();
		}

		public function select_academic_year_term_id($term_id){
			$this->db->select('*');
			$this->db->from('academic_year');
			$this->db->join('term', 'academic_year.id = term.aid');

			$this->db->where('term_id', $term_id);
			$this->db->limit('1');
			$q = $this->db->get();
			return $q->row_array();
		}

		public function select_academic_year_by_class_stream($class_stream_id){
			$rs = $this->db->query('SELECT * FROM academic_year WHERE status="current_academic_year" and class_level=(SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id='.$this->db->escape($class_stream_id).'))');
			return $rs->row_array();
		}

		public function select_academic_year($class_level = FALSE){
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('academic_year');
			$this->db->where('status', 'current_academic_year');

			if($class_level === FALSE){
				$q = $this->db->get();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'ACADEMIC_YEAR');
				return $q->result_array();
			}

			$this->db->where('class_level', $class_level);
			$q = $this->db->get();
			return $q->row_array();
		}

		public function insert_new_academic_year($year, $level, $begin_date, $end_date){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('CALL insert_into_academic_year_procedure("'.$begin_date.'", "'.$end_date.'", "'.$year.'", "'.$level.'")');
			$this->CI_getProcedureError();		    
		    //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'ACADEMIC_YEAR');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_academic_year_record_by_id($id){
			$this->db->select('*');
			$this->db->from('academic_year');

			$this->db->where('id', $id);
			$this->db->where('status', 'current_academic_year');

			$q = $this->db->get();		
			return $q->row_array();
		}

		public function update_academic_year($id, $academic_year_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('academic_year', $academic_year_record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'ACADEMIC_YEAR');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_new_term($term_record){
       		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('term', $term_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'TERM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function set_unset_current_term($id, $record, $academic_year, $is_current){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if($is_current === "no"){
				$this->db->query('UPDATE term SET is_current="yes" WHERE aid="'.$academic_year.'"');
				$this->activity_log($this->session->userdata('staff_id'), 'UNSET_CURRENT_TERM', 'term');
				if($this->db->error() == ""){
					$this->session->set_flashdata('error_message', 'Error updating term');
				}
				else{
					$this->session->set_flashdata('success_message', 'Successfully set as current term');
				}
			}
			else{
				$this->db->query('UPDATE term SET is_current="no" WHERE aid="'.$academic_year.'"');
				$this->activity_log($this->session->userdata('staff_id'), 'SET_CURRENT_TERM', 'term');
				if($this->db->error() == ""){
					$this->session->set_flashdata('error_message', 'Error updating term');
				}
				else{
					$this->session->set_flashdata('success_message', 'Successfully set as current term');
				}
			}
			$this->db->where('term_id', $id);
			$this->db->update('term', $record);
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function terms(){
			$this->db->select('*');
			$this->db->from('academic_year_term_view');
			$this->db->where('is_current', 'yes');
			$this->db->order_by('class_level', 'DESC');
			$this->db->order_by('term_name', 'ASC');

			$q = $this->db->get();
			return $q->result_array();
		}

		public function update_term($aid, $term_name, $term_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('term', $term_record, array('aid' => $aid, 'term_name' => $term_name), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'TERM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
		//END ACADEMIC YEAR

		//START STAFF GROUPS
		public function count_all_staff_groups($value){
			if(!empty($value)){
				$this->db->like('group_name', $value);
				$this->db->or_like('description', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('group_type');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_all_groups($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			
			if(!empty($value)){
				$this->db->like('group_name', $value);
				$this->db->or_like('description', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('group_type');
	        $this->db->limit($limit, $offset);

	        $query = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STAFF_GROUPS');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function insert_new_group($group_type_record){
       		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('group_type', $group_type_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'STAFF_GROUP');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_group_type_record_by_id($id){
			$q = $this->db->get_where('group_type', array('group_id' => $id));
			return $q->row_array();
		}

		public function update_group($id, $group_type_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('group_type', $group_type_record, array('group_id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STAFF_GROUP');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_group($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('group_type', NULL, array('group_id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'STAFF_GROUP');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_all_group_member_by_group_id($group_id){
			$query = $this->db->get_where('staff_group_members_view', array('group_id' => $group_id));
			return $query->row_array();
		}

		public function insert_remove_staff_to_group($staff_group_record, $group_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('staff_group', $staff_group_record, array('group_id' => $group_id), 'UPDATE_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE_GROUP_MEMBERS', 'STAFF_GROUP');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//Function for staff group select element
		public function select_all_staffs(){
			$query = $this->db->get('staff_plus_nok_view');
			return $query->result_array();
		}

		public function select_staff_by_group_id($group_id){
			$query = $this->db->query('SELECT firstname, lastname, concat(firstname, " ", lastname) AS staff_names, staff_group.*, group_name from staff JOIN staff_group USING(staff_id) JOIN group_type USING(group_id) WHERE staff_group.group_id='.$this->db->escape($group_id).'');

			return $query->result_array();
		}

		public function select_staff_group_by_staff_id($staff_id){
			$query = $this->db->query('SELECT staff_id, group_concat(" ", group_name) AS staff_groups FROM staff_group JOIN group_type USING(group_id) WHERE staff_id='.$this->db->escape($staff_id).'');
			return $query->row_array();
		}
		//END STAFF GROUPS

		//START ROLE
		public function add_new_role($role_record){
       		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('role_type', $role_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'ROLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_role_by_id($role_type_id){
			$this->db->select('*');
			$this->db->from('role_type');
			$this->db->where('role_type_id', $role_type_id);

			$rs = $this->db->get();
			return $rs->row_array();
		}

		public function select_modules(){
			$this->db->select('*');
			$this->db->from('module');
			
			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_permissions_by_role($role_type_id){
			$this->db->select('*');
			$this->db->from('role_permission');
			$this->db->join('permission', 'role_permission.permission_id = permission.permission_id');
			$this->db->where('role_type_id', $role_type_id);

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_common_permission($common_to){
			$this->db->select('*');
			$this->db->from('permission');
			$this->db->where('common_to', $common_to);
			//$this->db->order_by('description');
			$this->db->order_by('module_id');

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_module_id_by_permission_id($pid){
			$this->db->select('*');
			$this->db->from('permission');
			$this->db->where('permission_id', $pid);

			$rs = $this->db->get();
			return $rs->row_array()['module_id'];
		}

		//IF ALL CHECKBOXES ARE UNCHECKED
		public function remove_all_by_role($role_type_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('DELETE FROM role_module WHERE role_type_id='.$this->db->escape($role_type_id).'');
			$this->db->query('DELETE FROM role_permission WHERE role_type_id='.$this->db->escape($role_type_id).'');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'ROLE_PERMISSION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function manage_role($role_type_id, $permission_records, $role_module_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('role_type_id', $role_type_id);
			$this->db->delete('role_module');
			foreach ($role_module_record as $key => $row) {
				$this->db->query('REPLACE INTO role_module (module_id, role_type_id) VALUES('.$this->db->escape($row['module_id']).', '.$this->db->escape($role_type_id).')');
			}

			//$this->CI_mySQL('role_permission', $permission_records, array('role_type_id' => $role_type_id), 'UPDATE_BATCH', 'updated');
			$this->db->where('role_type_id', $role_type_id);
			$this->db->delete('role_permission');
			
			$this->CI_mySQL('role_permission_dummy_table', $permission_records, array('role_type_id' => $role_type_id), 'UPDATE_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'ROLE_PERMISSION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_role($role_type_id, $role_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('role_type', $role_record, array('role_type_id' => $role_type_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'ROLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function remove_role($role_type_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('role_type', NULL, array('role_type_id' => $role_type_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'ROLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function count_all_role_types($value){
			if(!empty($value)){
				$this->db->like('role_name', $value);
				$this->db->or_like('description', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('role_type');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}
		
		public function fetch_all_role_types($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			
			if(!empty($value)){
				$this->db->like('role_name', $value);
				$this->db->or_like('description', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('role_type');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'ROLES');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function select_staff_by_role_type_id($role_type_id){
			$query = $this->db->get_where('assigned_user_roles', array('role_type_id' => $role_type_id)); 
			return $query->result_array();
		}

		public function insert_remove_staff_to_role($record, $role_type_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('staff_role', $record, array('role_type_id' => $role_type_id), 'UPDATE_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STAFF_ROLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
		//END staff_role

		//START PROMOTE STUDENTS
		public function insert_promoted_audit_record($data, $level, $id){
			$q = $this->db->query('SELECT COUNT(*) AS count FROM promote_student_audit_table WHERE promote_student_audit_table.academic_year=(SELECT id FROM academic_year WHERE class_level="'.$level.'" AND status="current_academic_year" LIMIT 1) AND promotion_level="'.$level.'"');

			$o_level = "O'level"; $a_level = "A'level";	//Obtain the level to promote

			$res = $q->row_array();
         	if($res['count'] > 0){
         		$this->session->set_flashdata('exist_message', "".$level." students have already been promoted for academic_year : ".$this->select_current_academic_year_by_level($level)['year']);
         	}
         	else{         		
				$this->db->trans_start();
				/*$this->db->query('DROP TABLE IF EXISTS restore_students_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_guardian_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_enrollment_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_student_subject_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_transferred_tbl');
				$this->db->query('CREATE TABLE restore_students_tbl(SELECT * FROM student)');
				$this->db->query('CREATE TABLE restore_transferred_tbl(SELECT * FROM transferred_students)');
				$this->db->query('CREATE TABLE restore_guardian_tbl(SELECT * FROM guardian)');
				$this->db->query('CREATE TABLE restore_enrollment_tbl(SELECT * FROM enrollment)');
				$this->db->query('CREATE TABLE restore_student_subject_tbl(SELECT * FROM student_subject)');*/

				$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
				$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

				if($o_level == $level){
					$this->db->query('UPDATE student SET stte_out="completed", status="inactive" WHERE class_id="C04"');
				}
				if($a_level == $level){
					$this->db->query('UPDATE student SET stte_out="completed", status="inactive" WHERE class_id="C06"');
				}

				$this->db->insert_batch('waliomaliza_tbl', $this->students_to_insert_to_waliomaliza_tbl());

				/*echo "<pre>";
					print_r($data);
				echo "</pre>";die;*/
				//$this->db->insert('promote_student_audit_table', $data);
				$this->CI_mySQL('promote_student_audit_table', $data, NULL, 'INSERT');

				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'PROMOTE STUDENTS', 'STUDENT');				
				$this->db->trans_complete();
				
				return $this->feedbackMsg();
         	}
		}

		public function students_to_insert_to_waliomaliza_tbl(){
			$rs = $this->db->query('SELECT admission_no, academic_year as completion_year FROM student WHERE stte_out="completed" AND status="inactive" AND admission_no NOT IN(SELECT admission_no FROM waliomaliza_tbl)');
			return $rs->result_array();
		}

		public function restore_students_table($level){
			$q = $this->db->query('SELECT COUNT(*) AS count FROM promote_student_audit_table WHERE promote_student_audit_table.academic_year=(SELECT id FROM academic_year WHERE class_level='.$this->db->escape($level).' AND status="current_academic_year" LIMIT 1) AND promotion_level='.$this->db->escape($level).'');
			$res = $q->row_array();
         	if($res['count'] == 0){
         		$this->session->set_flashdata('exist_message', "You have already restored the students");
         	}
         	else{
				$this->db->trans_start();
				$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
				$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
				
				$this->db->query('DELETE FROM promote_student_audit_table WHERE academic_year=(SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level="'.$level.'" LIMIT 1)');
				$this->db->query('DELETE FROM student');
				$this->db->query('INSERT INTO student SELECT * FROM restore_students_tbl');
				$this->db->query('TRUNCATE guardian');
				$this->db->query('INSERT INTO guardian SELECT * FROM restore_guardian_tbl');
				$this->db->query('TRUNCATE enrollment');
				$this->db->query('INSERT INTO enrollment SELECT * FROM restore_enrollment_tbl');
				$this->db->query('TRUNCATE student_subject');
				$this->db->query('INSERT INTO student_subject SELECT * FROM restore_student_subject_tbl');
				$this->db->query('TRUNCATE transferred_students');
				$this->db->query('INSERT INTO transferred_students SELECT * FROM restore_transferred_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_students_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_guardian_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_enrollment_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_student_subject_tbl');
				$this->db->query('DROP TABLE IF EXISTS restore_transferred_tbl');
				$this->db->trans_complete();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'UNDO PROMOTION OF STUDENT', 'STUDENT');
			}

			return $this->feedbackMsg();
		}
		//END PROMOTE STUDENTS

		public function enable_restore_button(){
			$this->db->query('SELECT COUNT(*) AS count FROM promote_student_audit_table WHERE promote_student_audit_table.academic_year=(SELECT id FROM academic_year WHERE class_level='.$this->db->escape($level).' AND status="current_academic_year" LIMIT 1)');
		}


		//START STAFF ADMIN GROUPS
		

		public function select_all_permissions(){
			$q = $this->db->get('permission');
			return $q->result_array();
		}
		
		//START ACTIVITY LOG
		public function count_activity_log_records_by_user_id($user_id, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || user_id LIKE '%$value%' || source LIKE '%$value%' || lastname LIKE '%$value%' || activity LIKE '%$value%')");
	        }

			$this->db->select('*');
	        $this->db->from('activity_log_view');
	        $this->db->where('user_id', $user_id);

	        $rs = $this->db->get();
			return $rs->num_rows();
		}

		public function fetch_activity_log_records_by_user_id($user_id = FALSE, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || user_id LIKE '%$value%' || source LIKE '%$value%' || lastname LIKE '%$value%' || activity LIKE '%$value%')");
	        }

			$this->db->select('*');
	        $this->db->from('activity_log_view');
	        $this->db->where('user_id', $user_id);
	        //$this->db->order_by('time', 'ASC');
	        $this->db->limit($limit, $offset);		

	        $rs = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW USER ACTIVITY LOG', 'LOG');
	        $this->db->trans_complete();
			return $rs->result_array();
		}
		

		public function count_activity_log_records($value){
			if(!empty($value)){
	            $this->db->like('user_id', $value);
				$this->db->or_like('tableName', $value);
				$this->db->or_like('time', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('activity_log_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_activity_log_records($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('user_id', $value);
				$this->db->or_like('tableName', $value);
				$this->db->or_like('time', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('activity_log_view');
	        $this->db->order_by('time','DESC');
	        $this->db->limit($limit, $offset);
	        
	        $rs = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'ACTIVITY LOG');
	        $this->db->trans_complete();
			return $rs->result_array();
		}		
		//END ACTIVITY LOG

		


		/*public function check_if_terms_are_set(){
			return $this->check_if_terms_are_set();
		}*/

		/*public function check_if_terms_are_set(){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM term WHERE begin_date <= (SELECT current_date()) AND end_date >= (SELECT current_date()) AND is_current="yes"');

			return $rs->row_array()['count'];
		}*/

		public function academicYearSettings($level, $academic_year, $term_name){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM academic_year_term_view WHERE is_current = "yes" AND status = "current_academic_year" AND class_level='.$this->db->escape($level).' AND year='.$this->db->escape($academic_year).' AND term_name='.$this->db->escape($term_name).'');

			return $rs->row_array()['count'];
		}

	}
