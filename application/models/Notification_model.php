<?php
	class Notification_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function count_notifications($staff_id){
			$this->db->select('COUNT(*) AS count');
			$this->db->from('notification');
			$this->db->where('staff_id', $staff_id);
			$this->db->where('notification_status', 'unread');

			$query = $this->db->get();
			$res = $query->row_array();
         	return $res['count'];
			
		}

		public function count_announcements($staff_id){
			$query = $this->db->query('SELECT COUNT(*) AS count FROM announcement JOIN announcement_status USING(announcement_id) WHERE announcement.group_id IN(SELECT group_id FROM staff_group WHERE staff_id="'.$staff_id.'") AND staff_id="'.$staff_id.'" AND is_read="unread" AND announcement.status="show"');
			/*$this->db->from('notification');
			$this->db->where('staff_id', $staff_id);
			$this->db->where('notification_status', 'unread');*/

			//$query = $this->db->get();
			$res = $query->row_array();
         	return $res['count'];
			
		}

		public function select_notifications($staff_id){
			$status = "unread";
			$this->db->select('*');
			$this->db->from('notification');
			$this->db->where('staff_id', $staff_id);
			$this->db->where('notification_status', $status);
			//$this->db->order_by('timestamp', 'DESC');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function view($staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('notification');
			$this->db->where('staff_id', $staff_id);
			//$this->db->order_by('timestamp', 'DESC');

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'NOTIFICATION');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function update_notification($notification_id, $record){
			$this->db->where('notification_id', $notification_id);
			return $this->db->update('notification', $record);
		}

		public function select_all_groups(){
			$query = $this->db->get('group_type');
			return $query->result_array();
		}

		public function insert_announcement($group_id, $header, $msg, $staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('CALL insert_into_announcement_status_procedure('.$this->db->escape($group_id).', '.$this->db->escape($msg).', '.$this->db->escape($staff_id).', '.$this->db->escape($header).')');
			$this->CI_getProcedureError();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'ANNOUNCEMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_all_announcements($staff_id = FALSE, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(msg LIKE '%$value%' || time LIKE '%$value%' || group_name LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('all_announcements_view');
			$this->db->where('posted_by', $staff_id);
			$this->db->order_by('time', 'DESC');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'POSTED ANNOUNCEMENT');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function count_announcements_by_staff($staff_id, $value){
			if(!empty($value)){
				$this->db->where("(msg LIKE '%$value%' || time LIKE '%$value%' || group_name LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('all_announcements_view');
			$this->db->where('posted_by', $staff_id);

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function select_announcement_by_id($announcement_id){
			$this->db->select('*');
			$this->db->from('all_announcements_view');
			$this->db->where('announcement_id', $announcement_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_announcement($record, $announcement_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('announcement', $record, array('announcement_id' => $announcement_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'ANNOUNCEMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function publish_announcement($announcement_id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('announcement', $record, array('announcement_id' => $announcement_id), 'UPDATE');
			//Activity Log
			$status = ($record['status'] === "show") ? "PUBLISH" : "UNPUBLISH";
			$this->activity_log($this->session->userdata('staff_id'), $status, 'ANNOUNCEMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function count_my_announcements($staff_id, $value){
			$staff_id = $this->db->escape($staff_id);
			if(!empty($value)){
				$this->db->where("(msg LIKE '%$value%' || time LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('all_announcements_view');
			$this->db->where('group_id IN(SELECT group_id FROM staff_group WHERE staff_id='.$staff_id.')');
			$this->db->where('all_announcements_view.status', 'show');

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function select_announcements_by_staff_id($staff_id){
			$query = $this->db->query('SELECT announcement.*, announcement_status.*, staff.firstname, staff.lastname FROM announcement JOIN announcement_status USING(announcement_id) JOIN staff ON(staff.staff_id = announcement.posted_by) WHERE announcement.group_id IN (SELECT group_id FROM staff_group WHERE staff_id="'.$staff_id.'") AND announcement_status.staff_id="'.$staff_id.'" ORDER BY time DESC');
			return $query->result_array();
		}

		public function select_my_announcements($staff_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(msg LIKE '%$value%' || time LIKE '%$value%')");
	        }

			/*$this->db->select('*');
			$this->db->from('announcement');
			$this->db->join('announcement_status', 'announcement.announcement_id = announcement_status.announcement_id');
			$this->db->join('staff', 'staff.staff_id = announcement.posted_by');
			$this->db->where('announcement.group_id IN (SELECT group_id FROM staff_group WHERE staff_id='.$this->db->escape($staff_id).')');
			$this->db->where('announcement_status.staff_id', $staff_id);*/
			$rs = $this->db->query('select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,concat(`staff`.`firstname`," ",`staff`.`lastname`) AS `username` from ((`announcement` join `announcement_status` on((`announcement`.`announcement_id` = `announcement_status`.`announcement_id`))) join `staff` on((`staff`.`staff_id` = `announcement`.`posted_by`))) where (`announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where (`staff_group`.`staff_id` = '.$this->db->escape($staff_id).')) and (`announcement_status`.`staff_id` = '.$this->db->escape($staff_id).')) union select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,`admin`.`username` AS `username` from ((`announcement` join `announcement_status` on((`announcement`.`announcement_id` = `announcement_status`.`announcement_id`))) join `admin` on((`admin`.`id` = `announcement`.`posted_by`))) where (`announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where (`staff_group`.`staff_id` = '.$this->db->escape($staff_id).')) and (`announcement_status`.`staff_id` = '.$this->db->escape($staff_id).')) AND `announcement`.`status`="show"');
			
			//$this->db->order_by('time', 'DESC');
			//$this->db->limit($limit, $offset);

			//$rs = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'ANNOUNCEMENTS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function mark_announcement_status_as_read($staff_id, $announcement_id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('staff_id', $staff_id);
			$this->db->where('announcement_id', $announcement_id);
			$this->db->update('announcement_status', $record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'MARK AS READ', 'ANNOUNCEMENTS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
	}
?>