<?php
	class Class_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		//Used in student profile
		public function select_class_by_student($admission_no){
			$rs = $this->db->query('SELECT class_name, stream FROM class_level JOIN class_stream USING(class_id) WHERE class_stream_id =(SELECT class_stream_id FROM student WHERE admission_no='.$this->db->escape($admission_no).') LIMIT 1');
			return $rs->row_array();
		}

		public function class_stream_count(){
			$this->db->select('class_name, COUNT(*) AS total_streams');
	        $this->db->from('class_level');
	        $this->db->join('class_stream', 'class_level.class_id = class_stream.class_id');
	        $this->db->group_by('class_level.class_id');
	        $this->db->order_by('class_level.class_id','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function count_classes($value){
			if(!empty($value)){
	            $this->db->like('class_name', $value);
				$this->db->or_like('teacher_names', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('all_class_streams_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_classes($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('class_name', $value);
				$this->db->or_like('teacher_names', $value);
				$this->db->or_like('stream', $value);
	        }

			$this->db->select('*');
	        $this->db->from('all_class_streams_view');
	        $this->db->order_by('class_id','ASC');
	        $this->db->order_by('stream','ASC');
	        $this->db->limit($limit, $offset);
	        
	        $rs = $this->db->get();
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS_STREAMS');
			$this->db->trans_complete();

			return $rs->result_array();
		}


		public function select_stream(){
			if(!empty($value)){
	            $this->db->like('stream_name', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('stream');
	        $this->db->order_by('stream_name','ASC');

	        $rs = $this->db->get(); 
			return $rs->result_array();
		}

		//Function for getting class_level
		public function select_class_level($value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('class_name', $value);
	            $this->db->or_like('level', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('class_level');
	        $this->db->order_by('class_id','ASC');

	        $rs = $this->db->get();
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		//Function for getting class_level used in other functions
		public function select_class_lvl(){
	        $this->db->select('*');
	        $this->db->from('class_level');
	        $this->db->order_by('class_id','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}


		public function count_all_students_per_class($class_id, $term_id, $value){
	        if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student.admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('student');
	        $this->db->join('enrollment', 'student.admission_no = enrollment.admission_no');
	        $this->db->where('enrollment.class_id', $class_id);
	        if($class_id == NULL){//Important for database error "class_id = NULL"
	        	$this->db->where('enrollment.academic_year=(SELECT aid FROM term WHERE term_id='.$this->db->escape($term_id).') AND enrollment.class_id IS NULL');
	        }
	        else{
	        	$this->db->where('enrollment.academic_year=(SELECT aid FROM term WHERE term_id='.$this->db->escape($term_id).') AND enrollment.class_id='.$this->db->escape($class_id).'');
	        }
	        $this->db->order_by('student.admission_no','ASC');

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function count_student_waliofanya_paper($cid, $term_id, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student_assessment.admission_no LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('student_assessment');
	        $this->db->join('student', 'student.admission_no = student_assessment.admission_no');
			$this->db->where('student_assessment.class_id', $cid);
			$this->db->where('student_assessment.term_id', $term_id);

			$rs = $this->db->get();
			return $rs->num_rows();
		}

		//Function for getting class_stream
		public function select_class_stream($stream_id = FALSE){
			$this->db->select('stream_id, stream_name');
			$this->db->from('stream');
			if($stream_id === FALSE){
				$query = $this->db->get();
				return $query->result_array();
			}
			$this->db->where('stream_name', $stream_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		//Function used in displaying headings of class_name and stream in view_students Registrar
		public function select_class_name_and_stream(/*$id_display*/$class_stream_id){
			$this->db->select('*');
			$this->db->from('class_level');
			$this->db->join('class_stream', 'class_level.class_id = class_stream.class_id');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function get_class_monitor($csid){
			$this->db->select('class_stream.class_stream_id, class_stream.admission_no, concat(active_students_view.firstname, " ", active_students_view.lastname) as class_monitor');
	        $this->db->from('active_students_view');
	        $this->db->join('class_stream', 'class_stream.admission_no = active_students_view.admission_no');
	        $this->db->where('class_stream.class_stream_id', $csid);
	        $this->db->limit('1');

	        $rs = $this->db->get();
	        return $rs->row_array();
		}

		public function csps_for_summary_table($csid){//csps: COUNT STUDENTS PER STREAM
			$rs = $this->db->query('SELECT COUNT(*) as nos FROM active_students_view WHERE class_stream_id='.$this->db->escape($csid).'');
	        return $rs->row_array();
		}

		public function add_new_timetable($monday_timetable, $tuesday_timetable, $wednesday_timetable,$thursday_timetable, $friday_timetable){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('timetable', $monday_timetable, NULL, 'INSERT_BATCH');
			$this->CI_mySQL('timetable', $tuesday_timetable, NULL, 'INSERT_BATCH');
			$this->CI_mySQL('timetable', $wednesday_timetable, NULL, 'INSERT_BATCH');
			$this->CI_mySQL('timetable', $thursday_timetable, NULL, 'INSERT_BATCH');
			$this->CI_mySQL('timetable', $friday_timetable, NULL, 'INSERT_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'TIMETABLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function del_tt($csid){
			$this->db->query('DELETE FROM timetable WHERE class_stream_id='.$this->db->escape($csid).'');
		}

		public function delete_timetable($class_stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			//$class_stream_id = $this->db->escape($class_stream_id);
			//$this->db->query('DELETE FROM timetable WHERE class_stream_id='.$this->db->escape($class_stream_id).'');
			$this->CI_mySQL('timetable', NULL, array('class_stream_id' => $class_stream_id, 'DELETE'));
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'TIMETABLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function erase_timetable(){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('TRUNCATE timetable');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'TRUNCATE', 'TIMETABLE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_class_journal($data){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			//$this->db->insert_batch('teaching_log', $log_data);
			$this->CI_mySQL('teaching_attendance', $data, NULL, 'INSERT_BATCH');
			//$this->db->trans_complete();

			//$this->db->trans_start();
			//$this->db->insert_batch('teaching_attendance', $data);
			//$this->db->insert_batch('teaching_log', $log_data);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'CLASS JOURNAL');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function edit_teaching_attendance($class_stream_id, $date){
			$this->db->select('*');
			//$this->db->from('edit_teaching_attendance_view');
			$this->db->from('edit_teaching_attendance_view_mpya');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('date', $date);

			$q = $this->db->get();
			return $q->result_array();
		}

		public function update_teaching_attendance($ta_record, $ta_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('ta_id', $ta_id);
			//$this->db->update('teaching_attendance', $ta_record);
			$this->CI_mySQL('teaching_attendance', $ta_record, array('ta_id' => $ta_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CLASS JOURNAL');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_teaching_log($class_stream_id = FALSE, $date = FALSE){
			$this->db->select('*');
			$this->db->from('teaching_log_view');

			if($class_stream_id === TRUE && $date === TRUE){
				$q = $this->db->get();
				return $q->result_array();
			}

			$q = $this->db->get();
			return $q->result_array();
		}

		public function select_assignment_id($subject_name){
			/*$this->db->select('assignment_id');
			$this->db->from('teaching_assignment');
			$this->db->join('subject', 'subject.subject_id = teaching_assignment.subject_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_assignment.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('subject_name', $subject_name);
			$this->db->where('stream', $stream);
			$this->db->where('class_name', $class_name);

			$query = $this->db->get();
			return $query->row_array();*/

			//$this->db->select("assignment_id");
			$this->db->select("*");
			$this->db->from('filtered_teaching_assignment');
			$this->db->join('subject', 'filtered_teaching_assignment.subject_id = subject.subject_id'); 
			$this->db->where('subject_name', $subject_name);

			$query = $this->db->get();
			return $query->row_array();
		}

		//Hii function inatumika kwenye KucheckTeacherInTimetable issue ya optional subjects: NOTICE LIMIT 1
		public function optional_subjects_by_csid($class_stream_id, $term_id){
			$rs = $this->db->query('SELECT * FROM teaching_assignment JOIN subject USING(subject_id) WHERE subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="optional" AND subject_category="T") AND class_stream_id='.$this->db->escape($class_stream_id).' AND term_id='.$this->db->escape($term_id).' LIMIT 1');
			return $rs->row_array();
		}


		public function checkTeacherInTimetable($class_stream_id, $subject_id, $period_no, $weekday, $term_id){
			//Get the teacher assigned to teach the particular subject and check whether he/she has 
			//a period on th same time an weekday
			$query = $this->db->query('SELECT COUNT(*) AS count, timetable.class_stream_id FROM timetable WHERE weekday='.$this->db->escape($weekday).' AND period_no='.$this->db->escape($period_no).' AND teacher_id = (SELECT teacher_id from teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND subject_id='.$this->db->escape($subject_id).' AND term_id='.$this->db->escape($term_id).')');
			return $query->row_array();
		}

		public function checkTeacherInTimetableWhenEditing($class_stream_id, $subject_id, $period_no, $weekday, $term_id){
			//Get the teacher assigned to teach the particular subject and check whether he/she has 
			//a period on th same time an weekday
			$query = $this->db->query('SELECT COUNT(*) AS count, timetable.class_stream_id FROM timetable WHERE weekday='.$this->db->escape($weekday).' AND period_no='.$this->db->escape($period_no).' AND teacher_id = (SELECT teacher_id from teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND subject_id='.$this->db->escape($subject_id).' AND term_id='.$this->db->escape($term_id).') AND timetable.assignment_id NOT IN (SELECT assignment_id FROM teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND term_id='.$this->db->escape($term_id).')');

			return $query->row_array();
		}

		public function select_class_name($id){
			$this->db->select('class_name');
			$this->db->from('class_level');
			$this->db->where('class_id', $id);

			$query = $this->db->get();
			return $query->row_array();
		}
/*
		public function select_class_id($id){
			$this->db->select('class_name');
			$this->db->from('class_level');
			$this->db->where('class_id', $id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_stream_id($id){
			$this->db->select('class_name');
			$this->db->from('class_level');
			$this->db->where('class_id', $id);

			$query = $this->db->get();
			return $query->row_array();
		}*/

		public function select_subject_name($subject_name){
			$this->db->select('subject_name');
			$this->db->from('subject');
			$this->db->where('subject_name', $subject_name);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_teaching_assignment($class_stream_id, $term_id, $record){
			$this->db->trans_start();
			//Test Whether all subjects of the particular class_stream have been assigned a teacher
			$query = $this->db->query('SELECT COUNT(*) AS count FROM class_stream_subject WHERE subject_id NOT IN(SELECT subject_id FROM teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND term_id='.$this->db->escape($term_id).') AND class_stream_id='.$this->db->escape($class_stream_id).'');
			$count = $query->row_array()['count'];

			//Check Whether a class has atleast all subjects assigned to it
			$rs = $this->db->query('SELECT COUNT(*) AS count_subjects FROM class_stream_subject WHERE class_stream_id='.$this->db->escape($class_stream_id).'');
			$count_subjects = $rs->row_array()['count_subjects'];

			$q = $this->db->query('SELECT number_of_subjects FROM class_stream WHERE class_stream_id='.$this->db->escape($class_stream_id).'');
			$nos = $q->row_array()['number_of_subjects'];

			//Check whether the timetable already exist
			$rss = $this->db->query('SELECT COUNT(*) AS count_class_streams FROM timetable WHERE class_stream_id='.$this->db->escape($class_stream_id).'');
			$timetable_exist = $rss->row_array()['count_class_streams'];

			if($timetable_exist == 0){
				if($count == 0 && ($count_subjects != 0 && $count_subjects == $nos)){
					//Set filter_flag 0 to the entire teaching_assignment table
					$this->db->query('UPDATE teaching_assignment SET filter_flag=0');

					//Update filter_flag to one based on the class_stream_id condition
					$this->db->where('class_stream_id', $class_stream_id);
					$this->db->where('term_id', $term_id);
					$this->db->update('teaching_assignment', $record);

					//Copy all the value with filter_flag 1 to the view
					$this->db->query("CREATE OR REPLACE VIEW filtered_teaching_assignment AS SELECT * FROM teaching_assignment WHERE term_id=".$this->db->escape($term_id)." AND filter_flag=1");
					$this->session->set_flashdata('success_message', 'Choose subjects to add timetable');
				}
				else{
					$this->session->set_flashdata('error_message', 'Error: Either the class has no subjects or NOT all subjects have been assigned a teacher');
					return FALSE;
				}
			}
			else{
				$this->session->set_flashdata('error_message', 'The timetable for this class has already been added');
				return FALSE;
			}
			$this->db->trans_complete();

			//return $this->feedbackMsg();
		}

		public function checkIfTimetableExist($class_stream_id){
			$rss = $this->db->query('SELECT COUNT(*) AS ccs FROM timetable WHERE class_stream_id='.$this->db->escape($class_stream_id).'');

			return $rss->row_array()['ccs'];
		}

		public function get_optional_subjects_by_csid($class_stream_id){
			$rs = $this->db->query('SELECT * FROM teaching_assignment JOIN subject USING(subject_id) JOIN staff ON(staff.staff_id = teaching_assignment.teacher_id) WHERE subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="optional" AND subject_category="T") AND class_stream_id='.$this->db->escape($class_stream_id).'');
			return $rs->result_array();
		}

		public function get_class_journal($class_stream_id = FALSE, $day){
			$this->db->select('*');
			$this->db->from('class_journal_view');

			if($class_stream_id === FALSE){
				$query = $this->db->get();
				return $query->result_array();
			}


			//$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('timetable_class_stream_id', $class_stream_id);
			$this->db->where('weekday', $day);
			$this->db->order_by('period_no', 'ASC');

			$query = $this->db->get();
			return $query->result_array();	
		}

		public function get_class_subtopic($class_stream_id){
			/*$this->db->select('*');
			$this->db->from('class_stream_subject');  
			$this->db->join('subject', 'class_stream_subject.subject_id = subject.subject_id');
			$this->db->join('sub_topic', 'sub_topic.subject_id = subject.subject_id');
			$this->db->where('sub_topic.class_stream_id', $class_stream_id);*/
			$this->db->select('*');
			$this->db->from('subjects_subtopic_view');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		//NEW ATTENDANCE
		public function insert_student_attendance($class_stream_id, $date, $student_attendance_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('student_attendance_record', $student_attendance_record, NULL, 'INSERT_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'STUDENT ATTENDANCE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_student_attendance_record($class_stream_id, $date, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(names LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('daily_student_attendance_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('date_', $date);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT ATTENDANCE');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function select_daily_attendance_summary($class_stream_id, $date){
			$this->db->select('att_type, count(att_type) AS no_of_students, description, class_stream_id, date_, day');
			$this->db->from('daily_student_attendance_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('date_', $date);
			$this->db->group_by('att_type');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_daily_attendance_record($date, $day_of_week, $cid, $day_of_month){
			$sql = 'SELECT t2.admission_no, t2.class_stream_id, MAX('.$day_of_week.') AS "att_type" FROM (SELECT t.admission_no, t.class_stream_id, CASE WHEN t.day="'.$day_of_month.'" THEN t.att_type END AS "'.$day_of_week.'" FROM (
							SELECT admission_no, class_stream_id, student_attendance_record.att_id, att_type,
							CASE WHEN date_="'.$date.'" THEN "'.$day_of_month.'"
							ELSE NULL END AS DAY FROM student_attendance_record JOIN attendance ON(student_attendance_record.att_id = attendance.att_id)
							WHERE class_stream_id="'.$cid.'") t ) t2  GROUP BY t2.admission_no';
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		//END NEW ATTENDANCE


		//Retrieve timetable of a teacher
		public function select_timetable_by_teacher_id($teacher_id){
			$this->db->select('*');
			$this->db->from('teachers_timetable_view');
			$this->db->where('teacher_id', $teacher_id);
			$this->db->order_by('weekday');
			$this->db->order_by('period_no');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_no_of_periods_by_teacher_id($teacher_id){
			$this->db->select('weekday, count(period_no) as no_of_periods');
			$this->db->from('teachers_timetable_view');
			$this->db->where('teacher_id', $teacher_id);
			$this->db->group_by('weekday');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_class_stream_by_teacher_id($teacher_id){
			$this->db->select('*');
			$this->db->from('class_stream');
			$this->db->where('teacher_id', $teacher_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		//========================HII NI NOUMER KIUKWELI==================================
		public function create_monthly_report($class_stream_id, $month, $year, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$sql = 'SELECT t2.admission_no, t2.firstname, t2.lastname, t2.class_stream_id, t2.month, (SELECT COUNT(*) FROM student_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_attendance_record.admission_no=t2.admission_no AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM student_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_attendance_record.admission_no=t2.admission_no) AS total,
									MAX(1st) AS "1",
									MAX(2nd) AS "2",
									MAX(3rd) AS "3",
									MAX(4th) AS "4",
									MAX(5th) AS "5",
									MAX(6th) AS "6",
									MAX(7th) AS "7",
									MAX(8th) AS "8",
									MAX(9th) AS "9",
									MAX(10th) AS "10",
									MAX(11th) AS "11",
									MAX(12th) AS "12",
									MAX(13th) AS "13",
									MAX(14th) AS "14",
									MAX(15th) AS "15",
									MAX(16th) AS "16",
									MAX(17th) AS "17",
									MAX(18th) AS "18",
									MAX(19th) AS "19",
									MAX(20th) AS "20",
									MAX(21st) AS "21",
									MAX(22nd) AS "22",
									MAX(23rd) AS "23",
									MAX(24th) AS "24",
									MAX(25th) AS "25",
									MAX(26th) AS "26",
									MAX(27th) AS "27",
									MAX(28th) AS "28",
									MAX(29th) AS "29",
									MAX(30th) AS "30",
									MAX(31st) AS "31"
									FROM
									(
									SELECT t.admission_no, t.firstname, t.lastname, t.class_stream_id, t.month,
									CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
									CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
									CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
									CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
									CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
									CASE WHEN t.day="6" THEN t.att_type END AS "6th",
									CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
									CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
									CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
									CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
									CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
									CASE WHEN t.day="12" THEN t.att_type END AS "12th",
									CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
									CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
									CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
									CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
									CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
									CASE WHEN t.day="18" THEN t.att_type END AS "18th",
									CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
									CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
									CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
									CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
									CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
									CASE WHEN t.day="24" THEN t.att_type END AS "24th",
									CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
									CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
									CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
									CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
									CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
									CASE WHEN t.day="30" THEN t.att_type END AS "30th",
									CASE WHEN t.day="31" THEN t.att_type END AS "31st"
									FROM 
									(
									SELECT student_attendance_record.admission_no, firstname, lastname, student_attendance_record.class_stream_id, student_attendance_record.att_id, att_type, month,
									CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
									WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
									WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
									WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
									WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
									WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
									WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
									WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
									WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
									WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
									WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
									WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
									WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
									WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
									WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
									WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
									WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
									WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
									WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
									WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
									WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
									WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
									WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
									WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
									WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
									WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
									WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
									WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
									WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
									WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
									WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
									ELSE NULL END AS DAY FROM student_attendance_record JOIN attendance ON(student_attendance_record.att_id = attendance.att_id) JOIN student ON(student_attendance_record.admission_no = student.admission_no) WHERE student_attendance_record.class_stream_id="'.$class_stream_id.'"
									) t 
									) t2 WHERE (t2.admission_no LIKE "%'.$value.'%" || t2.firstname LIKE "%'.$value.'%" || t2.lastname LIKE "%'.$value.'%")
									GROUP BY t2.admission_no LIMIT '.$limit.' OFFSET '.$offset.'';

									$query = $this->db->query($sql);
									//Activity Log
									$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT MONTHLY ATTENDANCE');
									$this->db->trans_complete();

									return $query->result_array();
			}
			else{
				$sql = 'SELECT t2.admission_no, t2.firstname, t2.lastname, t2.class_stream_id, t2.month, (SELECT COUNT(*) FROM student_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_attendance_record.admission_no=t2.admission_no AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM student_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_attendance_record.admission_no=t2.admission_no) AS total,
									MAX(1st) AS "1",
									MAX(2nd) AS "2",
									MAX(3rd) AS "3",
									MAX(4th) AS "4",
									MAX(5th) AS "5",
									MAX(6th) AS "6",
									MAX(7th) AS "7",
									MAX(8th) AS "8",
									MAX(9th) AS "9",
									MAX(10th) AS "10",
									MAX(11th) AS "11",
									MAX(12th) AS "12",
									MAX(13th) AS "13",
									MAX(14th) AS "14",
									MAX(15th) AS "15",
									MAX(16th) AS "16",
									MAX(17th) AS "17",
									MAX(18th) AS "18",
									MAX(19th) AS "19",
									MAX(20th) AS "20",
									MAX(21st) AS "21",
									MAX(22nd) AS "22",
									MAX(23rd) AS "23",
									MAX(24th) AS "24",
									MAX(25th) AS "25",
									MAX(26th) AS "26",
									MAX(27th) AS "27",
									MAX(28th) AS "28",
									MAX(29th) AS "29",
									MAX(30th) AS "30",
									MAX(31st) AS "31"
									FROM
									(
									SELECT t.admission_no, t.firstname, t.lastname, t.class_stream_id, t.month,
									CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
									CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
									CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
									CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
									CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
									CASE WHEN t.day="6" THEN t.att_type END AS "6th",
									CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
									CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
									CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
									CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
									CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
									CASE WHEN t.day="12" THEN t.att_type END AS "12th",
									CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
									CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
									CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
									CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
									CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
									CASE WHEN t.day="18" THEN t.att_type END AS "18th",
									CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
									CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
									CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
									CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
									CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
									CASE WHEN t.day="24" THEN t.att_type END AS "24th",
									CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
									CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
									CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
									CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
									CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
									CASE WHEN t.day="30" THEN t.att_type END AS "30th",
									CASE WHEN t.day="31" THEN t.att_type END AS "31st"
									FROM 
									(
									SELECT student_attendance_record.admission_no, firstname, lastname, student_attendance_record.class_stream_id, student_attendance_record.att_id, att_type, month,
									CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
									WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
									WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
									WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
									WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
									WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
									WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
									WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
									WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
									WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
									WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
									WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
									WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
									WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
									WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
									WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
									WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
									WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
									WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
									WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
									WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
									WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
									WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
									WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
									WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
									WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
									WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
									WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
									WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
									WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
									WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
									ELSE NULL END AS DAY FROM student_attendance_record JOIN attendance ON(student_attendance_record.att_id = attendance.att_id) JOIN student ON(student_attendance_record.admission_no = student.admission_no) WHERE student_attendance_record.class_stream_id="'.$class_stream_id.'"
									) t 
									) t2 
									GROUP BY t2.admission_no LIMIT '.$limit.' OFFSET '.$offset.'';

									$query = $this->db->query($sql);
									//Activity Log
									$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT MONTHLY ATTENDANCE');
									$this->db->trans_complete();

									return $query->result_array();
			}
		}

		public function mkeka_mpya($class_id, $term_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('student_subject_score_position.*, firstname, lastname');
			$this->db->from('student_subject_score_position');
			$this->db->join('student', 'student_subject_score_position.admission_no = student.admission_no');
			$this->db->where('student_subject_score_position.class_id', $class_id);
			$this->db->where('student_subject_score_position.term_id', $term_id);

			$rs = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS RESULTS');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function whole_class_results_aggregate($class_id, $term_id){
			$this->db->select('grade, COUNT(grade) AS count');
			$this->db->from('student_assessment');			
			$this->db->where('class_id', $class_id);
			$this->db->where('term_id', $term_id);
			$this->db->group_by('grade');

			$rs = $this->db->get();
			return $rs->result_array();
		}

		//MKEKA WENYEWE WENYEWE KABISA
		/*public function create_mkeka_halisi($class_id, $term_id){
			$this->db->query("SET @sql = NULL");
			$this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT('MAX(IF(subject_id = ''', subject_id, ''', average, NULL)) AS ', subject_id)) INTO @sql FROM student_subject_score_position WHERE class_id='".$class_id."' AND term_id=".$term_id."");
			$this->db->query("SET @sql = CONCAT('SELECT admission_no, ', @sql, ' FROM student_subject_score_position WHERE term_id=".$term_id." AND class_id='".$class_id."' GROUP BY admission_no')");
			$this->db->query('PREPARE stmt FROM @sql');
			$query = $this->db->query('EXECUTE stmt');
			$this->db->query('DEALLOCATE PREPARE stmt');

			return $query->result_array();
		}*/


		public function insert_student_assessment($data, $class_id, $term_id){
			$this->db->trans_start();
			$this->db->query('DELETE FROM student_assessment WHERE class_id='.$this->db->escape($class_id).' AND term_id='.$this->db->escape($term_id).'');
			$this->db->insert_batch('student_assessment', $data);
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_student_class_position($class_id, $term_id){
			$query = $this->db->query('select student_assessment.*, concat(student.firstname, " ", student.lastname) AS names, find_in_set(average, (select group_concat(average order by average desc) from student_assessment WHERE class_id='.$this->db->escape($class_id).' AND term_id='.$this->db->escape($term_id).')) AS rank from student_assessment JOIN student USING(admission_no) WHERE student_assessment.class_id='.$this->db->escape($class_id).' AND term_id='.$this->db->escape($term_id).' ORDER BY rank ASC');

			return $query->result_array();
		}

		//OLD FOR ASSIGNING HOD
		public function select_teachers(){
			$this->db->select('*');
			$this->db->from('assign_hod_class_teacher_view');

			$query = $this->db->get();
			return $query->result_array();
		}

		//NEW FOR ASSIGNING HOD
		public function select_teachers_new($dept_id = FALSE){
			$query = $this->db->query('SELECT * FROM assign_hod_class_teacher_view WHERE staff_id IN(SELECT staff_id FROM staff_department WHERE dept_id='.$this->db->escape($dept_id).')');
			return $query->result_array();
		}

		public function assign_teacher($class_stream_id, $record, $teacher_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE class_stream SET teacher_id=NULL WHERE teacher_id='.$this->db->escape($teacher_id).'');
			$this->db->query('CALL assign_class_teacher_procedure('.$this->db->escape($class_stream_id).', '.$this->db->escape($teacher_id).')');
			$this->CI_getProcedureError();
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->update('class_stream', $record);

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ASSIGN CLASS TEACHER', 'CLASS_STREAM');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function view_enrolled_students(){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT t1.class_id, t1.class_name, t1.stream, t1.class_stream_id,  
								MAX(F1) AS "F1", MAX(F2) AS "F2", MAX(F3) AS "F3", MAX(F4) AS "F4", MAX(F5) AS "F5", MAX(F6) AS "F6" 
								FROM 
								( 
								SELECT t.class_id, t.class_name, t.stream, t.capacity, t.class_stream_id, 
								CASE WHEN t.className="Form_One" THEN concat(t.enrolled,"/",t.capacity) END AS "F1", 
								CASE WHEN t.className="Form_Two" THEN concat(t.enrolled,"/",t.capacity) END AS "F2", 
								CASE WHEN t.className="Form_Three" THEN concat(t.enrolled,"/",t.capacity) END AS "F3", 
								CASE WHEN t.className="Form_Four" THEN concat(t.enrolled,"/",t.capacity) END AS "F4", 
								CASE WHEN t.className="Form_Five" THEN concat(t.enrolled,"/",t.capacity) END AS "F5", 
								CASE WHEN t.className="Form_Six" THEN concat(t.enrolled,"/",t.capacity) END AS "F6" 
								FROM 
								( 
								SELECT academic_year.id, academic_year.year, class_level.class_id, class_stream.class_stream_id, class_name, stream, level, count(student.admission_no) AS enrolled, capacity, description, id_display, 
								CASE WHEN class_level.class_id="C01" THEN "Form_One" 
								WHEN class_level.class_id="C02" THEN "Form_Two" 
								WHEN class_level.class_id="C03" THEN "Form_Three" 
								WHEN class_level.class_id="C04" THEN "Form_Four" 
								WHEN class_level.class_id="C05" THEN "Form_Five" 
								WHEN class_level.class_id="C06" THEN "Form_Six" 
								ELSE NULL END AS className 
								FROM class_level 
								JOIN  class_stream ON(class_level.class_id = class_stream.class_id) 
								LEFT JOIN student ON(student.class_stream_id = class_stream.class_stream_id) 
								JOIN academic_year ON(class_level.level = academic_year.class_level) WHERE academic_year.status="current_academic_year" AND student.status="active" GROUP BY class_name, stream 
								) t 
								) t1 
								GROUP BY stream

			');
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ENROLLMENT SUMMARY', 'STUDENT');
			$this->db->trans_complete();

			return $query->result_array();
		}


		public function create_weekly_class_journal_report($class_stream_id, $from = FALSE, $to = FALSE){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT  firstname, lastname, teaching_assignment.assignment_id, teaching_assignment.teacher_id, teaching_assignment.subject_id, teaching_assignment.class_stream_id,  subject_name, count(*) AS no_of_periods, (SELECT count(*) from teaching_attendance WHERE status="taught" AND assignment_id=teaching_assignment.assignment_id AND teaching_attendance.date BETWEEN '.$this->db->escape($from).' AND '.$this->db->escape($to).') AS taught, (select count(*) from teaching_attendance WHERE status="untaught" AND assignment_id=teaching_assignment.assignment_id AND teaching_attendance.date BETWEEN '.$this->db->escape($from).' AND '.$this->db->escape($to).') AS untaught from timetable JOIN teaching_assignment ON(teaching_assignment.assignment_id = timetable.assignment_id) JOIN staff ON(teaching_assignment.teacher_id = staff.staff_id) JOIN subject ON(teaching_assignment.subject_id = subject.subject_id) WHERE timetable.class_stream_id='.$this->db->escape($class_stream_id).' GROUP BY timetable.assignment_id');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS JOURNAL');
			$this->db->trans_complete();

			return $query->result_array();

			//return $this->feedbackMsg();
		}

		public function select_attendance_by_date($class_stream_id, $date, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || attendance.description LIKE '%$value%')");
	        }

			$this->db->select('student_attendance_record.*, firstname, lastname, att_type, attendance.description');
			$this->db->from('student_attendance_record');
			$this->db->join('student', 'student_attendance_record.admission_no = student.admission_no');
			$this->db->join('attendance', 'attendance.att_id = student_attendance_record.att_id');
			$this->db->where('student_attendance_record.class_stream_id', $class_stream_id);
			$this->db->where('student_attendance_record.date_', $date);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SEARCH', 'STUDENT ATTENDANCE');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function count_attendance_by_date($class_stream_id, $date, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || attendance.description LIKE '%$value%')");
	        }
	        
			$this->db->select('student_attendance_record.*, firstname, lastname, att_type, attendance.description');
			$this->db->from('student_attendance_record');
			$this->db->join('student', 'student_attendance_record.admission_no = student.admission_no');
			$this->db->join('attendance', 'attendance.att_id = student_attendance_record.att_id');
			$this->db->where('student_attendance_record.class_stream_id', $class_stream_id);
			$this->db->where('student_attendance_record.date_', $date);

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function select_attendance_types(){
			$this->db->select('*');
			$this->db->from('attendance');

			$query = $this->db->get();
			return $query->result_array();
		}


		public function update_attendance($admission_no, $class_stream_id, $date, $attendance_record){			
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('admission_no', $admission_no);
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('date_', $date);
			$this->db->update('student_attendance_record', $attendance_record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STUDENT ATTENDANCE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		/*public function class_timetable($class_stream_id){
			$query = $this->db->query('SELECT  t1.period_no, t1.start_time, t1.end_time,  MAX(Monday) AS "Monday", MAX(Tuesday) AS "Tuesday", MAX(Wednesday) AS "Wednesday", MAX(Thursday) AS "Thursday", MAX(Friday) AS "Friday" FROM  (  SELECT t.subject_id, t.subject_name, t.teacher_id, t.teacher_names, t.period_no,t.start_time, t.end_time, t.day,  CASE WHEN t.day="Mon" THEN t.subject_name END AS "Monday",  CASE WHEN t.day="Tue" THEN t.subject_name END AS "Tuesday",  CASE WHEN t.day="Wed" THEN t.subject_name END AS "Wednesday",  CASE WHEN t.day="Thu" THEN t.subject_name END AS "Thursday",  CASE WHEN t.day="Fri" THEN t.subject_name END AS "Friday"  FROM  ( SELECT subject_id, subject_name, teacher_id, concat(firstname, " ", lastname) AS teacher_names, period_no, start_time, end_time,  CASE WHEN weekday="Monday" THEN "Mon"  WHEN weekday="Tuesday" THEN "Tue"  WHEN weekday="Wednesday" THEN "Wed"  WHEN weekday="Thursday" THEN "Thu"  WHEN weekday="Friday" THEN "Fri" ELSE NULL END AS "DAY"  FROM timetable_view WHERE class_stream_id="'.$class_stream_id.'" ) t  ) t1 GROUP BY period_no');

			return $query->result_array();
		}*/

		public function count_periods(){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM period');
			return $rs->row_array();
		}

		public function renderTimetable($class_stream_id, $weekday, $period_no){
			$query = $this->db->query('SELECT period_no, COUNT(*) AS no_of_periods FROM timetable_view WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND weekday='.$this->db->escape($weekday).' AND period_no='.$this->db->escape($period_no).'');
			return $query->row_array();
		}

		public function class_timetable($class_stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT  t1.period_no, t1.start_time, t1.end_time,  MAX(Monday) AS "Monday", MAX(Tuesday) AS "Tuesday", MAX(Wednesday) AS "Wednesday", MAX(Thursday) AS "Thursday", MAX(Friday) AS "Friday" FROM  (  SELECT t.subject_id, t.subject_name, t.teacher_id, t.teacher_names, t.period_no,t.start_time, t.end_time, t.day,  CASE WHEN t.day="Mon" THEN concat(t.subject_name, "-", t.teacher_names) END AS "Monday",  CASE WHEN t.day="Tue" THEN concat(t.subject_name, "-", t.teacher_names) END AS "Tuesday",  CASE WHEN t.day="Wed" THEN concat(t.subject_name, "-", t.teacher_names) END AS "Wednesday",  CASE WHEN t.day="Thu" THEN concat(t.subject_name, "-", t.teacher_names) END AS "Thursday",  CASE WHEN t.day="Fri" THEN concat(t.subject_name, "-", t.teacher_names) END AS "Friday"  FROM  ( SELECT subject_id, subject_name, teacher_id, concat(firstname, " ", lastname) AS teacher_names, period_no, start_time, end_time,  CASE WHEN weekday="Monday" THEN "Mon"  WHEN weekday="Tuesday" THEN "Tue"  WHEN weekday="Wednesday" THEN "Wed"  WHEN weekday="Thursday" THEN "Thu"  WHEN weekday="Friday" THEN "Fri" ELSE NULL END AS "DAY"  FROM timetable_view WHERE class_stream_id='.$this->db->escape($class_stream_id).' ) t  ) t1 GROUP BY period_no');
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS STREAM TIMETABLE');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function start_transaction(){
			$this->db->trans_start();	//This function is used only for timetable
		}

		public function commit_transaction(){
			$this->db->trans_complete();	//This function is used only for timetable
			return $this->feedbackMsg();
		}

		public function select_subject_choice_and_category_by_subject_id($subject_id){
			$this->db->select('subject_category, subject_choice');
			$this->db->from('subject');
			$this->db->where('subject_id', $subject_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_ta_by_subject_and_csid($class_stream_id, $subject_id, $term_id){
			$this->db->select('*');
			$this->db->from('teaching_assignment');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('subject_id', $subject_id);
			$this->db->where('term_id', $term_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_teacher_id_by_assignment_id($assignment_id){
			//$assignment_id = $this->db->escape($assignment_id);
			$query = $this->db->query('SELECT * FROM teaching_assignment WHERE assignment_id='.$this->db->escape($assignment_id).' LIMIT 1');

			return $query->row_array();
		}

		public function get_class_stream_by_filter(){
			$query = $this->db->query('SELECT * FROM teaching_assignment WHERE filter_flag=1 LIMIT 1');
			return $query->row_array();
		}

		public function get_class_lvl_by_csid($csid){
			$query = $this->db->query('SELECT * FROM class_level WHERE class_stream_id='.$this->db->escape($csid).'');
			return $query->row_array();
		}

		//This function compares if all the subjects of a particular class have been assigned a teacher
		public function compare_ta_with_css($class_stream_id, $term_id){
			$query = $this->db->query('SELECT COUNT(*) FROM class_stream_subject WHERE subject_id NOT IN(SELECT subject_id FROM teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND term_id='.$this->db->escape($term_id).') AND class_stream_id='.$this->db->escape($class_stream_id).'');
			return $query->num_rows();
		}

		public function ratiba_by_darasa_and_siku($class_stream_id, $weekday){
			$this->db->select('*');
			$this->db->from('used_to_edit_timetable_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('weekday', $weekday);

			$query = $this->db->get();
			return $query->result_array();
		}

		//function used to populate dropdowns in new_timetable
		public function get_subjects_for_timetable(){
			$this->db->select('*');
			$this->db->from('teaching_assignment');
			$this->db->join('subject', 'teaching_assignment.subject_id = subject.subject_id');
			$this->db->where('filter_flag', 1);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_teachers_teaching_optional_subjects($class_stream_id){
			/*$rs = $this->db->query('SELECT staff_id, firstname, lastname, concat(firstname, " ", lastname) AS subject_teacher_names FROM staff WHERE staff_id IN(SELECT teacher_id FROM teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="optional"))');*/
			$rs = $this->db->query('SELECT teaching_assignment.subject_id, subject_name, staff_id, firstname, lastname, concat(firstname, " ", lastname) AS subject_teacher_names FROM staff JOIN teaching_assignment ON(teaching_assignment.teacher_id = staff.staff_id) JOIN subject ON(teaching_assignment.subject_id = subject.subject_id) WHERE staff_id IN(SELECT teacher_id FROM teaching_assignment WHERE class_stream_id='.$this->db->escape($class_stream_id).' AND subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="optional")) AND teaching_assignment.class_stream_id='.$this->db->escape($class_stream_id).' AND teaching_assignment.subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="optional")');
			return $rs->result_array();
		}

		public function select_option_subject_by_class($class_stream_id){
			$this->db->select('*');
			$this->db->from('class_stream_subject_view');
			$this->db->where('subject_choice', 'optional');
			$this->db->where('subject_category', 'T');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_class_level_by_class_id($class_id){
			$this->db->select('*');
			$this->db->from('class_level');
			$this->db->where('class_id', $class_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function fetch_student_attendance_record($class_stream_id, $date, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(names LIKE '%$value%' || description LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

			$this->db->select('*');
	        $this->db->from('daily_student_attendance_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->where('date_', $date);
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();       
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT ATTENDANCE');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function search_student_to_view_attendance($value, $class_stream_id, $date/*, $limit, $offset*/){
			//$this->db->limit($limit, $offset);
			$this->db->select('*');
			$this->db->from('daily_student_attendance_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('date_', $date);
			$this->db->like('names', $value);
			$this->db->or_like('admission_no', $value);

			$query = $this->db->get();

			if($query->num_rows() > 0){
				return $query->result_array();
			}
			else{
				return $query->result_array();
			}
		}


		public function select_class_teachers_history($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('stream', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('class_teacher_history.*, class_stream.teacher_id, class_level.class_name, class_stream.stream, concat(firstname, " ", lastname) AS teacher_names');
			$this->db->from('class_teacher_history');
			$this->db->join('class_stream', 'class_teacher_history.class_stream_id = class_stream.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->join('staff', 'staff.staff_id = class_teacher_history.teacher_id');			
			//$this->db->order_by('class_teacher_history.end_date');
			$this->db->order_by('class_stream.class_stream_id', 'DESC');
			$this->db->order_by('class_teacher_history.end_date', 'ASC');
			$this->db->limit($limit, $offset);

			$rs = $this->db->get();
			 //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS TEACHER HISTORY');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_class_teachers_history($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('stream', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('class_teacher_history.*, class_level.class_name, class_stream.stream, concat(firstname, " ", lastname) AS teacher_names');
			$this->db->from('class_teacher_history');
			$this->db->join('class_stream', 'class_teacher_history.class_stream_id = class_stream.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->join('staff', 'staff.staff_id = class_teacher_history.teacher_id');

			$rs = $this->db->get();
			return $rs->num_rows();
		}


		//***************CHANGES ***************************************


		public function count_class_stream_asset($class_stream_id, $value){
			if(!empty($value)){
				$this->db->where("(status LIKE '%$value%' || asset_name LIKE '%$value%' || description LIKE '%$value%' || asset_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('class_asset_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function get_class_stream_asset($class_stream_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

	        if(!empty($value)){
				$this->db->where("(status LIKE '%$value%' || asset_name LIKE '%$value%' || description LIKE '%$value%' || asset_no LIKE '%$value%')");
	        }	        

	        $this->db->select('*');
	        $this->db->from('class_asset_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->order_by('asset_no','ASC');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get(); 
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS ASSETS');
			$this->db->trans_complete();
			return $rs->result_array();	
		}

		public function asset_reports($class_stream_id){
			$class_stream_id = $this->db->escape($class_stream_id);
			$q = $this->db->query('SELECT `class_stream`.`class_stream_id` AS `class_stream_id`,`class_asset`.`asset_name` AS `asset_name`,count(0) AS `total_assets` FROM (`class_stream` JOIN `class_asset` ON((`class_stream`.`class_stream_id` = `class_asset`.`class_stream_id`))) WHERE class_stream.class_stream_id='.$class_stream_id.' group by `class_asset`.`asset_name` union SELECT `class_stream`.`class_stream_id` AS `class_stream_id`,"Total" AS `Total`,count(0) AS `count(*)` FROM (`class_stream` JOIN `class_asset` ON((`class_stream`.`class_stream_id` = `class_asset`.`class_stream_id`))) WHERE class_stream.class_stream_id='.$class_stream_id.'');

			return $q->result_array();
		}

		public function insert_new_asset($asset_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_asset', $asset_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'CLASS_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_asset($asset_record, $asset_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_asset', $asset_record, array('asset_no' => $asset_no), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CLASS_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_asset($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('class_asset', NULL, array('id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'CLASS_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
	}
