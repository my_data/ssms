<?php
	class Teacher_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function count_all_teachers($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('subject_name', $value);
				$this->db->or_like('staff_id', $value);
			}

			$this->db->select('*');
			$this->db->from('all_teachers_view');
			$this->db->group_by('staff_id');
			
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function select_all_teachers($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('subject_name', $value);
				$this->db->or_like('staff_id', $value);
			}

			$this->db->select('all_teachers_view.*, group_concat(" ", subject_name) AS subject_names');
			$this->db->from('all_teachers_view');
			$this->db->group_by('staff_id');	
			$this->db->limit($limit, $offset);
			
			 $rs = $this->db->get();
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW TEACHERS', 'TEACHER');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function add_subjects_to_teacher($teacher_id, $subject_teacher_record, $staff_department_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('DELETE FROM subject_teacher WHERE teacher_id='.$this->db->escape($teacher_id).'');
			$this->db->insert_batch('subject_teacher', $subject_teacher_record);

			$this->db->query('DELETE FROM staff_department WHERE staff_id='.$this->db->escape($teacher_id).'');
			$this->db->insert_batch('staff_department_dummy_table', $staff_department_record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD SUBJECT TO TEACHER', 'SUBJECT TEACHER');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_subjects_by_teacher_id($teacher_id){
			$rs = $this->db->query('SELECT subject_teacher.*, subject_name FROM subject_teacher JOIN subject using(subject_id) WHERE teacher_id='.$this->db->escape($teacher_id).'');
			return $rs->result_array();
		}

		public function count_teaching_assignment($staff_id){
			$this->db->select('COUNT(*) AS count');
			$this->db->from('teaching_assignment');
			$this->db->where('teacher_id', $staff_id);
			$this->db->where('term_id IN(SELECT term_id FROM term WHERE is_current="yes")');

			$query = $this->db->get();
         	return $query->row_array()['count'];
			
		}


		//The function is used in Login controller to create an array of sessions
		public function select_assigned_classes_by_teacher_id($teacher_id){
			$this->db->select('*');
			$this->db->from('teaching_assignment');
			$this->db->where('teacher_id', $teacher_id);
			$this->db->where('term_id IN(SELECT term_id FROM term WHERE is_current="yes")');

			$rs = $this->db->get();
			return $rs->result_array();
		}

		//Retrieve teaching assignment by teacher's ID
		public function select_teachers_teaching_assingment($teacher_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(subject_name LIKE '%$value%' || class_name LIKE '%$value%' || stream LIKE '%$value%')");
	        }

			$this->db->select('teaching_assignment.teacher_id, teaching_assignment.subject_id, subject_name, class_name, stream, start_date, end_date, class_stream.class_stream_id, teaching_assignment.term_id');
			$this->db->from('teaching_assignment');
			$this->db->join('subject', 'teaching_assignment.subject_id = subject.subject_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_assignment.class_stream_id');  
			$this->db->join('class_level','class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_assignment.teacher_id', $teacher_id);
			$this->db->where('teaching_assignment.term_id IN(SELECT term_id FROM term WHERE is_current="yes")');
			$this->db->limit($limit, $offset);
			
			$query = $this->db->get();			
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'MY ASSIGNED CLASSES');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function count_teachers_teaching_assingment($teacher_id, $value){
			if(!empty($value)){
				$this->db->where("(subject_name LIKE '%$value%' || class_name LIKE '%$value%' || stream LIKE '%$value%')");
	        }

			$this->db->select('teaching_assignment.teacher_id, teaching_assignment.subject_id, subject_name, class_name, stream, start_date, end_date, class_stream.class_stream_id');
			$this->db->from('teaching_assignment');
			$this->db->join('subject', 'teaching_assignment.subject_id = subject.subject_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_assignment.class_stream_id');  
			$this->db->join('class_level','class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_assignment.teacher_id', $teacher_id);
			
			$query = $this->db->get();
			return $query->num_rows();
		}

		//For viewing timetables of assigned classes
		public function select_classes_assigned($teacher_id){
			$this->db->select('DISTINCT(class_stream_id), class_name, stream, teacher_id');
			$this->db->from('classes_assigned_view');
			$this->db->where('teacher_id', $teacher_id);
			
			$query = $this->db->get();
			return $query->result_array();

		}

		//Create daily school attendance
		/*public function create_daily_school_attendance($date, $class_stream_id){
			$sql = 'SELECT t3.class_stream_id, 
					MAX(Present) AS "Present",
					MAX(Absent) AS "Absent",
					MAX(Sick) AS "Sick",
					MAX(Permitted) AS "Permitted",
					MAX(Home) AS "Home"
					FROM
					(
					SELECT t2.att_type, t2.nos, t2.class_stream_id,
					CASE WHEN t2.att_description="Present" THEN t2.nos END AS "Present",
					CASE WHEN t2.att_description="Absent" THEN t2.nos END AS "Absent",
					CASE WHEN t2.att_description="Sick" THEN t2.nos END AS "Sick",
					CASE WHEN t2.att_description="Permitted" THEN t2.nos END AS "Permitted",
					CASE WHEN t2.att_description="Home" THEN t2.nos END AS "Home"
					FROM
					(
					SELECT t.att_type, t.nos, t.class_stream_id,
					CASE WHEN att_type="P" THEN "Present"
					WHEN att_type="A" THEN "Absent"
					WHEN att_type="S" THEN "Sick"
					WHEN att_type="T" THEN "Permitted"
					WHEN att_type="H" THEN "Home"
					ELSE NULL END AS att_description FROM
					(
					SELECT att_type, COUNT(*) AS nos, class_stream_id FROM daily_school_attendance_view WHERE class_stream_id="'.$class_stream_id.'" AND date_="'.$date.'" group by att_type
					) t
					) t2
					) t3';

				$query = $this->db->query($sql);
				return $query->result_array();
		}*/

		//Exam type
		public function select_exam_type(){
			$this->db->select('*');
			$this->db->from('exam_type');

			$q = $this->db->get();
			return $q->result_array();
		}

		public function disable_all_exam_type(){
			$this->db->trans_start();			
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE exam_type SET add_score_enabled=0');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DISABLE', 'EXAM_TYPE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_exam_type($id, $data){
			$this->db->trans_start();			
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			//$this->db->query('UPDATE exam_type SET add_score_enabled=0');
			$this->CI_mySQL('exam_type', array('add_score_enabled' => 0), NULL, 'UPDATE_ALL');
			$this->CI_mySQL('exam_type', $data, array('id' => $id), 'UPDATE');
			$this->db->query('CALL enable_exam_type_notification('.$this->db->escape($id).')');
			$this->CI_getProcedureError();
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ENABLE', 'EXAM_TYPE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}//Exam type

		//Attendance ya shule

		public function test_if_empty_record($date){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM student_attendance_record WHERE date_='.$this->db->escape($date).'');
			return $rs->row_array();
		}

		public function create_attendance_report_summary($date){
			$this->db->trans_start();			
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('
						SELECT t2.class_id, t2.class_stream_id, t2.class_name, t2.stream, t2.date_, 
						MAX(A) AS "Absent", MAX(P) AS "Present", MAX(S) AS "Sick", MAX(T) AS "Permitted", MAX(H) AS "Home",
						(SELECT COUNT(*) FROM student_attendance_record WHERE class_stream_id=t2.class_stream_id AND date_='.$this->db->escape($date).') AS total_per_stream
						FROM 
						( 
						SELECT t1.class_id, t1.class_stream_id, t1.class_name, t1.stream, t1.att_type, t1.nos, t1.date_, 
						CASE WHEN t1.attendance_type="Absent" THEN t1.nos END AS "A", 
						CASE WHEN t1.attendance_type="Present" THEN t1.nos END AS "P", 
						CASE WHEN t1.attendance_type="Sick" THEN t1.nos END AS "S", 
						CASE WHEN t1.attendance_type="Permitted" THEN t1.nos END AS "T", 
						CASE WHEN t1.attendance_type="Home" THEN t1.nos END AS "H" 
						FROM 
						( 
						SELECT t.class_id, t.class_stream_id, t.class_name, t.stream, t.att_type, t.nos, t.date_, 
						CASE WHEN att_type="A" THEN "Absent" 
						WHEN att_type="P" THEN "Present" 
						WHEN att_type="S" THEN "Sick" 
						WHEN att_type="T" THEN "Permitted" 
						WHEN att_type="H" THEN "Home" 
						ELSE NULL END AS ATTENDANCE_TYPE 
						FROM 
						( 
						SELECT * FROM daily_attendance_report_view 
						) t
						) t1
						) t2 
						WHERE t2.date_='.$this->db->escape($date).'
						GROUP BY t2.date_, t2.class_stream_id

						UNION

						SELECT 
						"", "", "Total", "", "", MAX(A) AS "Absent", MAX(P) AS "Present", MAX(S) AS "Sick", MAX(T) AS "Permitted", MAX(H) AS "Home", MAX(Jumla) AS "Total" 
						FROM ( 
						SELECT t.description, t.total, 
						CASE WHEN t.description="Absent" THEN t.total END AS "A", 
						CASE WHEN t.description="Present" THEN t.total END AS "P", 
						CASE WHEN t.description="Sick" THEN t.total END AS "S", 
						CASE WHEN t.description="Permitted" THEN t.total END AS "T", 
						CASE WHEN t.description="Home" THEN t.total END AS "H", 
						CASE WHEN t.description="total_nos" THEN t.total END AS "Jumla" 
						FROM 
						(
						SELECT description, COUNT(*) AS total FROM student_attendance_record join attendance using(att_id) 
						WHERE date_='.$this->db->escape($date).' 
						GROUP BY att_id 
						UNION 
						SELECT "total_nos", count(*) FROM student_attendance_record WHERE date_="'.$date.'" 
						) t 
						) t1;
					');

			//Activity Log
			if($date == date('Y-m-d')){
				$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'ATTENDANCE REPORT');
			}
			else{
				$this->activity_log($this->session->userdata('staff_id'), 'SEARCH', 'ATTENDANCE REPORT');
			}
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function create_individual_timetable($teacher_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT t1.period_no, t1.start_time, t1.end_time,  MAX(Monday) AS "Monday", MAX(Tuesday) AS "Tuesday", MAX(Wednesday) AS "Wednesday", MAX(Thursday) AS "Thursday", MAX(Friday) AS "Friday" FROM  (  SELECT t.subject_id, t.subject_name, t.teacher_id, t.teacher_names, t.period_no,t.start_time, t.end_time, t.day,  CASE WHEN t.day="Mon" THEN concat(t.subject_name, " - ", t.csid) END AS "Monday",  CASE WHEN t.day="Tue" THEN concat(t.subject_name, " - ", t.csid) END AS "Tuesday",  CASE WHEN t.day="Wed" THEN concat(t.subject_name, " - ", t.csid) END AS "Wednesday",  CASE WHEN t.day="Thu" THEN concat(t.subject_name, " - ", t.csid) END AS "Thursday",  CASE WHEN t.day="Fri" THEN concat(t.subject_name, " - ", t.csid) END AS "Friday"  FROM  ( SELECT subject_id, subject_name, teacher_id, concat(firstname, " ", lastname) AS teacher_names, period_no, start_time, end_time, csid, CASE WHEN weekday="Monday" THEN "Mon"  WHEN weekday="Tuesday" THEN "Tue"  WHEN weekday="Wednesday" THEN "Wed"  WHEN weekday="Thursday" THEN "Thu"  WHEN weekday="Friday" THEN "Fri" ELSE NULL END AS "DAY"  FROM timetable_view WHERE teacher_id='.$this->db->escape($teacher_id).' ) t  ) t1 GROUP BY period_no');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'PERSONAL TIMETABLE');
			$this->db->trans_complete();

			return $query->result_array();
		}

		//check tod @den//
		public function check_tod($tod){
			$query = $this->db->query('SELECT * FROM teacher_duty_roaster JOIN duty_roaster ON(teacher_duty_roaster.did = duty_roaster.id) WHERE teacher_id='.$this->db->escape($tod).' AND is_current="T"');
			return $query->row_array();
		}  


		public function save_daily_routine($records, $date, $id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('start_date, end_date');
			$this->db->from('duty_roaster');
			$this->db->where('id', $id);

			$query = $this->db->get();
			$rs = $query->row_array();

			if($rs['start_date'] <= $date && $date <= $rs['end_date']){
				$this->CI_mySQL('tod_daily_routine_tbl', $records, NULL, 'INSERT');
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'tod_daily_routine_tbl');
			}
			else{
				$this->session->set_flashdata('error_message', 'Invalid date range, the date you have selected is not within the week that you are on duty');
			}		
			$this->activity_log($this->session->userdata('staff_id'), 'ADD TOD REPORT', 'TOD ROUTINE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//@den//
		public function daily_routine($tod){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT teacher_id, id, teacher_duty_roaster.did, dayname(date_) as day, date_, wake_up_time, parade_time, lunch, dinner, events, security, comments, start_classes_time, breakfast FROM duty_roaster JOIN tod_daily_routine_tbl ON(duty_roaster.id=tod_daily_routine_tbl.did) JOIN teacher_duty_roaster ON(teacher_duty_roaster.did=tod_daily_routine_tbl.did) WHERE teacher_id='.$this->db->escape($tod).' and is_current="T"');

			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'WEEK"S TOD REPORTS');
			$this->db->trans_complete();
			return $query->result_array();
		}

      	//@den// 
		public function today_report($tod, $date){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT teacher_duty_roaster.did, dayname(date_) AS day, date_, wake_up_time, parade_time, lunch, dinner, events, security, comments, start_classes_time, breakfast, teacher_id  FROM teacher_duty_roaster JOIN tod_daily_routine_tbl ON(tod_daily_routine_tbl.did=teacher_duty_roaster.did) JOIN duty_roaster ON(teacher_duty_roaster.did=duty_roaster.id) WHERE is_current="T" AND date_='.$this->db->escape($date).' AND teacher_id='.$this->db->escape($tod).'');

			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'CURRENT DAY TOD REPORT');
			$this->db->trans_complete();
			return $query->row_array();
		}

   		//@den//
		public function today_report_to($date){
			$query = $this->db->query('SELECT * FROM teacher_duty_roaster JOIN tod_daily_routine_tbl ON(tod_daily_routine_tbl.did=teacher_duty_roaster.did) JOIN staff ON(staff.staff_id=teacher_duty_roaster.teacher_id) JOIN duty_roaster ON(teacher_duty_roaster.did=duty_roaster.id) WHERE is_current="T" AND date_='.$this->db->escape($date).'');
			return $query->row_array();	
		}

		//@den//
		public function head_tod_info(){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT duty_roaster.id as id, did, dayname(date_) as day, date_, wake_up_time, parade_time, lunch, dinner, events, security, comments, start_classes_time, breakfast FROM duty_roaster JOIN tod_daily_routine_tbl ON(duty_roaster.id = tod_daily_routine_tbl.did) WHERE is_current="T" order by date_ asc');
			
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'WEEK"S TOD REPORTS');
			$this->db->trans_complete();
			return $query->result_array();
		}

      	//@den//
		public function tod_details(){
			$query = $this->db->query('SELECT * FROM duty_roaster JOIN teacher_duty_roaster ON(teacher_duty_roaster.did = duty_roaster.id) JOIN staff ON(staff.staff_id = teacher_duty_roaster.teacher_id) WHERE is_current="T"');
			return $query->row_array();
		}

         //@den//
		public function report_head_tod_info($date){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT did, dayname(date_) as day,date_, wake_up_time, parade_time, lunch, dinner, events, security, comments, start_classes_time, breakfast FROM tod_daily_routine_tbl WHERE date_='.$this->db->escape($date).'');
			
			
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'DAILY TOD REPORT');
			$this->db->trans_complete();
			return $query->row_array();
		}


		public function week_id(){
			$query = $this->db->query('SELECT id FROM duty_roaster WHERE is_current="T"');
			return $query->row_array();
		}


		public function add_day($date){
			$query=$this->db->query('SELECT dayname(date_) as day FROM tod_daily_routine_tbl WHERE date_='.$this->db->escape($date).'');
			return $query->row_array();
		}

         //@den//
		public function save_comment($date, $sms){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->set($sms);
			$this->db->where('date_',$date);
			$this->db->update('tod_daily_routine_tbl', $sms);
			$this->db->query('CALL send_notification_to_tod_after_comment_procedure('.$this->db->escape($date).')');
			$this->CI_getProcedureError();
			
			$this->activity_log($this->session->userdata('staff_id'), 'ADD COMMENT', 'TOD REPORT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//This function is NOT used
		public function select_comments_by_date($date){
			$rs = $this->db->select('*')->from('tod_daily_routine_tbl')->where('date_', $date);
			return $rs->row_array();
		}

		// this week comments @den//
		public function this_week_comments($weekid){
			$query = $this->db->query('SELECT teacher_duty_roaster.did, dayname(date_) as day, date_, wake_up_time, parade_time,lunch,dinner, events,security, comments, start_classes_time, breakfast, teacher_id  FROM teacher_duty_roaster JOIN tod_daily_routine_tbl ON(tod_daily_routine_tbl.did = teacher_duty_roaster.did) JOIN duty_roaster ON(teacher_duty_roaster.did = duty_roaster.id) where comments != "" AND is_current="T" AND id='.$this->db->escape($weekid).'');
			return $query->result_array();

		}

		//display tods den@
		public function display_tod(){
			$query=$this->db->query('SELECT duty_roaster.id, start_date, end_date, is_current, teacher_id, firstname, lastname, phone_no FROM teacher_duty_roaster, staff, duty_roaster WHERE duty_roaster.id = teacher_duty_roaster.did AND teacher_duty_roaster.teacher_id = staff.staff_id ORDER BY end_date DESC');
			return $query->result_array();

		}
		  //display comments den@
		public function all_comments(){
		  	$query=$this->db->query('select  dayname(date_) as day,date_, comments from tod_daily_routine_tbl  where comments != "" order by date_ desc');
		  	return $query->result_array();
		}
  		//delete comments den@//
		public function delete_comment($date){
			$this->db->query('update tod_daily_routine_tbl set comments="" where date_="'.$date.'"');
			return true;
		}

		// TODs management

		public function add_tods($record, $teacher_id) {
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$rs = $this->db->query('SELECT COUNT(*) AS count, id FROM duty_roaster WHERE start_date="'.$record['start_date'].'" LIMIT 1');
			if($rs->row_array()['count'] > 0){
				$insert_id1 = $rs->row_array()['id'];
			}
			else{
				$this->CI_mySQL('duty_roaster', $record, NULL, 'INSERT');
				//$this->db->insert('duty_roaster', $record);
				$insert_id1 = $this->db->insert_id();
			}

			$field = array(
				'teacher_id' => $teacher_id,
				'did'        => $insert_id1
			);

			$this->CI_mySQL('teacher_duty_roaster', $field, NULL, 'INSERT');

			//If the week is the current week
			$xyz = $this->db->query('SELECT COUNT(*) AS x FROM duty_roaster WHERE start_date='.$this->db->escape($record['start_date']).' AND is_current="T"');

			if($xyz->row_array()['x'] == 1){
				$this->db->query('INSERT INTO staff_role (role_type_id, staff_id) VALUES(9, '.$this->db->escape($teacher_id).')');
				$this->db->query('REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES('.$this->db->escape($teacher_id).', 9, "Your are the current teacher on duty, FROM:  <b>'.$this->db->escape($rs->row_array()['start_date']).'</b>, TO: <b>'.$this->db->escape($rs->row_array()['end_date']).'</b>", '.time().')');
			}
			//$this->db->insert('teacher_duty_roaster', $field);

			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			//$this->CI_mySQL('dormitory', $record, NULL, 'INSERT', 'inserted');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'ADD TOD');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function get_all_teachers(){
			$this->db->select('staff.staff_id, firstname, lastname');
			$this->db->from('staff');
			$this->db->join('teacher', 'staff.staff_id = teacher.staff_id');		
			
			$query = $this->db->get();
			return $query->result_array();
		}

		public function count_duty_roaster($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('*');
			$this->db->from('teacher_duty_roaster');
			$this->db->join('staff', 'staff.staff_id = teacher_duty_roaster.teacher_id');
			$this->db->join('duty_roaster', 'duty_roaster.id = teacher_duty_roaster.did');

			$rs = $this->db->get();
			return $rs->num_rows();
		}

		public function select_teachers_duty_roaster($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('id, teacher_id, firstname, lastname, start_date, end_date, is_current');
			$this->db->from('teacher_duty_roaster');
			$this->db->join('staff', 'staff.staff_id = teacher_duty_roaster.teacher_id');
			$this->db->join('duty_roaster', 'duty_roaster.id = teacher_duty_roaster.did');	
			$this->db->order_by('is_current');
			$this->db->limit($limit, $offset);
			
			$query = $this->db->get();

			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'DUTY ROASTER');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function select_duty_roaster_record_by_id($id, $sid){
			$this->db->select('*');
			$this->db->from('duty_roaster');
			$this->db->join('teacher_duty_roaster', 'duty_roaster.id = teacher_duty_roaster.did');
			$this->db->where('id', $id);
			$this->db->where('teacher_id', $sid);
			
			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_week_tods($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('duty_roaster');
			$this->db->join('teacher_duty_roaster', 'duty_roaster.id = teacher_duty_roaster.did');
			$this->db->join('staff', 'teacher_duty_roaster.teacher_id = staff.staff_id');
			$this->db->where('id', $id);
			
			$query = $this->db->get();
			
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'CURRENT WEEK TODs');
			$this->db->trans_complete();
			return $query->result_array();
		}

		public function update_duty_roaster($old_teacher_id, $id, $record, $new_tid){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teacher_duty_roaster', $record, array('did' => $id, 'teacher_id' => $old_teacher_id), 'UPDATE');

			$rs = $this->db->query('SELECT start_date, end_date, COUNT(id) AS count FROM duty_roaster WHERE is_current="T" AND id='.$this->db->escape($id).'');
			
			if($rs->row_array()['count'] == 1){	//IF tod is in current roaster
				$this->db->query('UPDATE staff_role SET staff_id='.$this->db->escape($new_tid).' WHERE staff_id='.$this->db->escape($old_teacher_id).' AND role_type_id=9');
				$this->db->query('REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES('.$this->db->escape($new_tid).', 9, "Your are the current teacher on duty, FROM:  <b>'.$this->db->escape($rs->row_array()['start_date']).'</b>, TO: <b>'.$this->db->escape($rs->row_array()['end_date']).'</b>", '.time().')');
			}			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DUTY ROASTER');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function set_current_duty_roaster($id/*, $record, $current_date*/){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('CALL update_duty_roaster_procedure('.$id.')');
			$this->CI_getProcedureError();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SET CURRENT TODs', 'DUTY ROASTER');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_duty_roaster($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('duty_roaster', NULL, array('id' => $id), 'DELETE');
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DUTY ROASTER');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function head_tod_info_all($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('select duty_roaster.id as id, did, dayname(date_) as day, date_, wake_up_time, parade_time, lunch, dinner, events, security, comments, start_classes_time, breakfast from duty_roaster join tod_daily_routine_tbl on(duty_roaster.id = tod_daily_routine_tbl.did) WHERE duty_roaster.id = '.$id.' order by date_ asc');
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'TODs REPORTS');
			$this->db->trans_complete();
			return $query->result_array();
		}

      	/*//@den//
		public function tod_details_all($id){
			$query=$this->db->query('select * from duty_roaster join teacher_duty_roaster on(teacher_duty_roaster.did = duty_roaster.id) join staff on(staff.staff_id = teacher_duty_roaster.teacher_id)');
			return $query->row_array();
		}*/
	}