<?php
	class Staff_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function select_all_staffs(){
			$this->db->select('staff_id, concat(firstname, "	", lastname) AS names');
			$this->db->from('staff');

			$query = $this->db->get();
			return $query->result_array();
		}


		//New Attendance
		public function insert_staff_attendance_record($staff_attendance_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('staff_attendance_record', $staff_attendance_record, NULL, 'INSERT_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD', 'STAFF ATTENDANCE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//Register New staff
		public function register_staff($staff_record, $nok_record, /*$subject_teacher_record = FALSE, $staff_department_record = FALSE, */$subclass_record){
			//Staff type
			$staff_type = $this->input->post('staff_type');
			
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			
			$this->db->insert('staff', $staff_record);
			$this->db->insert('staff_nok', $nok_record);
			if($staff_type === 'T'){
				$this->db->insert('teacher', $subclass_record);
				//$this->db->insert_batch('subject_teacher', $subject_teacher_record);
				//$this->db->insert_batch('staff_department_dummy_table', $staff_department_record);
			}
			else if($staff_type === 'A'){
				$this->db->insert('accountant', $subclass_record);
			}
			else if($staff_type === 'L'){
				$this->db->insert('librarian', $subclass_record);
			}
			else if($staff_type === 'S'){
				$this->db->insert('secretary', $subclass_record);
			}

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REGISTER STAFF', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//Update Staff

		/*CREATE TRIGGER before_update_staff_nok_trigger BEFORE UPDATE ON staff_nok FOR EACH ROW BEGIN DECLARE x INT;  SELECT COUNT(*) FROM staff_nok WHERE staff_id=NEW.staff_id INTO x; IF x = 0 THEN INSERT INTO staff_nok () VALUES();*/
		public function update_staff($staff_id, $staff_record, $nok_record, $subject_teacher_record = NULL, $staff_department_record = NULL, $subclass_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			//Staff type
			$staff_type = $this->input->post('staff_type');
			$old_staff_type = $this->input->post('old_staff_type');
			
			$this->db->where('staff_id', $staff_id);
			$this->db->update('staff', $staff_record);

			$q = $this->db->query('SELECT COUNT(*) AS cnt FROM staff_nok WHERE staff_id='.$this->db->escape($staff_id).'');
			$res = $q->row_array();
         	if($res['cnt'] == 0){
         		$this->db->insert('staff_nok', $nok_record);
         	}
         	else{
         		$this->db->where('staff_id', $staff_id);
				$this->db->update('staff_nok', $nok_record);
         	}			

			//$this->db->query('DELETE FROM accountant WHERE staff_id="'.$staff_id.'"');
			//$this->db->query('DELETE FROM librarian WHERE staff_id="'.$staff_id.'"');
			//$this->db->query('DELETE FROM secretary WHERE staff_id="'.$staff_id.'"');
			//$this->db->query('DROP TABLE IF EXISTS copy_duty_roaster');
			//$this->db->query('CREATE TABLE copy_duty_roaster SELECT * FROM teacher_duty_roaster WHERE teacher_id="'.$staff_id.'"');
			
			//$this->CI_mySQL('teacher', NULL, array('staff_id' => $staff_id), 'DELETE');


			/*if($staff_type === 'T' && $old_staff_type === "T"){
				//$this->db->insert('teacher', $subclass_record);

				$this->db->query('DELETE FROM subject_teacher WHERE teacher_id="'.$staff_id.'"');
				$this->db->insert_batch('subject_teacher', $subject_teacher_record);

				$this->db->query('DELETE FROM staff_department WHERE staff_id="'.$staff_id.'"');
				$this->db->insert_batch('staff_department_dummy_table', $staff_department_record);

				//$this->db->query('INSERT INTO teacher_duty_roaster SELECT * FROM copy_duty_roaster');
				//$this->db->query(' DROP TABLE IF EXISTS copy_duty_roaster');
			}
			elseif($staff_type === 'T' && $old_staff_type !== "T"){
				$this->db->query('DELETE FROM accountant WHERE staff_id="'.$staff_id.'"');
				$this->db->query('DELETE FROM librarian WHERE staff_id="'.$staff_id.'"');
				$this->db->query('DELETE FROM secretary WHERE staff_id="'.$staff_id.'"');

				$this->db->insert('teacher', $subclass_record);
				$this->db->insert_batch('subject_teacher', $subject_teacher_record);
				$this->db->insert_batch('staff_department_dummy_table', $staff_department_record);

				//$this->db->query('INSERT INTO teacher_duty_roaster SELECT * FROM copy_duty_roaster');
				//$this->db->query(' DROP TABLE IF EXISTS copy_duty_roaster');
			}
			else{
				$this->db->query('DELETE FROM teacher WHERE staff_id="'.$staff_id.'"');
				$this->db->query('DELETE FROM subject_teacher WHERE teacher_id="'.$staff_id.'"');
				$this->db->query('DELETE FROM staff_department WHERE staff_id="'.$staff_id.'"');

				if($staff_type === 'A'){
					$this->db->insert('accountant', $subclass_record);
				}
				else if($staff_type === 'L'){
					$this->db->insert('librarian', $subclass_record);
				}
				else if($staff_type === 'S'){
					$this->db->insert('secretary', $subclass_record);
				}
			}*/

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE STAFF', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}//End Update Staff

		public function select_staff_information_and_role($staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('staff_plus_nok_view.*, inactive_staff.*, assigned_user_roles.role_name, group_concat(role_name, " ") AS roles');
			$this->db->from('staff_plus_nok_view');
			$this->db->join('inactive_staff', 'staff_plus_nok_view.staff_id = inactive_staff.staff_id', 'left');
			$this->db->join('assigned_user_roles', 'staff_plus_nok_view.staff_id = assigned_user_roles.staff_id', 'left');
			$this->db->where('staff_plus_nok_view.staff_id', $staff_id);
			$this->db->limit('1');

			$query = $this->db->get();			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW OWN PROFILE', 'STAFF');
			$this->db->trans_complete();
			return $query->row_array();
		}

		public function select_staff_information($staff_id){
			$this->db->select('*');
			$this->db->from('staff_plus_nok_view');
			$this->db->where('staff_id', $staff_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_staff_certifications($staff_id = FALSE){
			$this->db->select('*');
			$this->db->from('staff_qualification_view');
			$this->db->where('staff_id', $staff_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		/*public function select_staff_attendance_record($staff_id){
			$query = $this->db->query('SELECT t2.staff_id, t2.staff_names, MAX(Absent) AS "Absent", MAX(Present) AS "Present", MAX(Sick) AS "Sick", MAX(Permitted) AS "Permitted" FROM ( SELECT t1.staff_id, t1.att_type, t1.description, t1.aggregate, t1.staff_names, CASE WHEN t1.attendance_type="Absent" THEN t1.aggregate END AS "Absent", CASE WHEN t1.attendance_type="Present" THEN t1.aggregate END AS "Present", CASE WHEN t1.attendance_type="Sick" THEN t1.aggregate END AS "Sick", CASE WHEN t1.attendance_type="Permitted" THEN t1.aggregate END AS "Permitted", CASE WHEN t1.attendance_type="Home" THEN t1.aggregate END AS "Home" FROM ( SELECT t.staff_id, t.att_type, t.description, t.aggregate, t.staff_names, CASE WHEN t.att_type="A" THEN "Absent" WHEN t.att_type="P" THEN "Present" WHEN t.att_type="S" THEN "Sick" WHEN t.att_type="T" THEN "Permitted" WHEN t.att_type="H" THEN "Home" ELSE NULL END AS attendance_type FROM ( select staff.staff_id, concat(firstname, " ", lastname) AS staff_names, att_type, description, count(att_id) AS aggregate from staff_attendance_record join staff using(staff_id) join attendance using(att_id) where staff.staff_id='.$this->db->escape($staff_id).' group by att_id ) t ) t1 ) t2');

			return $query->row_array();
		}*/

		public function select_staff_attendance_record($staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT t2.year, t2.aid, t2.staff_id, t2.staff_names, MAX(Absent) AS "Absent", MAX(Present) AS "Present", MAX(Sick) AS "Sick", MAX(Permitted) AS "Permitted" FROM ( SELECT t1.year, t1.aid, t1.staff_id, t1.att_type, t1.description, t1.aggregate, t1.staff_names, CASE WHEN t1.attendance_type="Absent" THEN t1.aggregate END AS "Absent", CASE WHEN t1.attendance_type="Present" THEN t1.aggregate END AS "Present", CASE WHEN t1.attendance_type="Sick" THEN t1.aggregate END AS "Sick", CASE WHEN t1.attendance_type="Permitted" THEN t1.aggregate END AS "Permitted", CASE WHEN t1.attendance_type="Home" THEN t1.aggregate END AS "Home" FROM ( SELECT t.year, t.aid, t.staff_id, t.att_type, t.description, t.aggregate, t.staff_names, CASE WHEN t.att_type="A" THEN "Absent" WHEN t.att_type="P" THEN "Present" WHEN t.att_type="S" THEN "Sick" WHEN t.att_type="T" THEN "Permitted" WHEN t.att_type="H" THEN "Home" ELSE NULL END AS attendance_type FROM ( select year, staff_attendance_record.aid, staff.staff_id, concat(firstname, " ", lastname) AS staff_names, att_type, description, count(att_id) AS aggregate from staff_attendance_record join staff using(staff_id) join attendance using(att_id) join academic_year on(staff_attendance_record.aid = academic_year.id) where staff.staff_id='.$this->db->escape($staff_id).' group by att_id ) t ) t1 ) t2 group by t2.aid');
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STAFF"S ATTENDANCE');
			$this->db->trans_complete();
			return $query->result_array();
		}

		public function create_monthly_report($month, $year, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$sql = 'SELECT t2.staff_id, t2.firstname, t2.lastname, (SELECT COUNT(*) FROM staff_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND staff_attendance_record.staff_id=t2.staff_id AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM staff_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND staff_attendance_record.staff_id=t2.staff_id) AS total,
								MAX(1st) AS "1",
								MAX(2nd) AS "2",
								MAX(3rd) AS "3",
								MAX(4th) AS "4",
								MAX(5th) AS "5",
								MAX(6th) AS "6",
								MAX(7th) AS "7",
								MAX(8th) AS "8",
								MAX(9th) AS "9",
								MAX(10th) AS "10",
								MAX(11th) AS "11",
								MAX(12th) AS "12",
								MAX(13th) AS "13",
								MAX(14th) AS "14",
								MAX(15th) AS "15",
								MAX(16th) AS "16",
								MAX(17th) AS "17",
								MAX(18th) AS "18",
								MAX(19th) AS "19",
								MAX(20th) AS "20",
								MAX(21st) AS "21",
								MAX(22nd) AS "22",
								MAX(23rd) AS "23",
								MAX(24th) AS "24",
								MAX(25th) AS "25",
								MAX(26th) AS "26",
								MAX(27th) AS "27",
								MAX(28th) AS "28",
								MAX(29th) AS "29",
								MAX(30th) AS "30",
								MAX(31st) AS "31"
								FROM
								(
								SELECT t.staff_id, t.firstname, t.lastname,
								CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
								CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
								CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
								CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
								CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
								CASE WHEN t.day="6" THEN t.att_type END AS "6th",
								CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
								CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
								CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
								CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
								CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
								CASE WHEN t.day="12" THEN t.att_type END AS "12th",
								CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
								CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
								CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
								CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
								CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
								CASE WHEN t.day="18" THEN t.att_type END AS "18th",
								CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
								CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
								CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
								CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
								CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
								CASE WHEN t.day="24" THEN t.att_type END AS "24th",
								CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
								CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
								CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
								CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
								CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
								CASE WHEN t.day="30" THEN t.att_type END AS "30th",
								CASE WHEN t.day="31" THEN t.att_type END AS "31st"
								FROM 
								(
								SELECT staff_attendance_record.staff_id, firstname, lastname, staff_attendance_record.att_id, att_type,
								CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
								WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
								WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
								WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
								WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
								WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
								WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
								WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
								WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
								WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
								WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
								WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
								WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
								WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
								WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
								WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
								WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
								WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
								WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
								WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
								WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
								WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
								WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
								WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
								WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
								WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
								WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
								WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
								WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
								WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
								WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
								ELSE NULL END AS DAY FROM staff_attendance_record JOIN attendance ON(staff_attendance_record.att_id = attendance.att_id) JOIN staff ON(staff_attendance_record.staff_id = staff.staff_id)
								) t 
								) t2 WHERE (t2.staff_id LIKE "%'.$value.'%" || t2.firstname LIKE "%'.$value.'%" || t2.lastname LIKE "%'.$value.'%")
								GROUP BY t2.staff_id LIMIT '.$limit.' OFFSET '.$offset.'';

								$query = $this->db->query($sql);
								//Activity Log
								$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STAFFS MONTHLY ATTENDANCE');
								$this->db->trans_complete();
								return $query->result_array();
		}
		

		public function select_all_staff_attendance_record($date, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(names LIKE '%$value%' || staff_id LIKE '%$value%' || gender LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('staff_attendance_view');
			$this->db->where('date_', $date);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STAFFS DAILY ATTENDANCE');
			$this->db->trans_complete();
			return $query->result_array();
		}

		//Used to populate select element when editing staff_attendance
		public function pfs_attendance(){	
			$this->db->select('*');	
			$this->db->from('attendance');
			$this->db->where('att_type !=', 'H');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function update_attendance($staff_id, $date_, $attendance_record){			
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('staff_id', $staff_id);
			$this->db->where('date_', $date_);
			$this->db->update('staff_attendance_record', $attendance_record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STAFF ATTENDANCE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_daily_attendance_summary($date){
			$this->db->select('att_type, count(att_type) AS no_of_staffs, description, date_, day');
			$this->db->from('staff_attendance_view');
			$this->db->where('date_', $date);
			$this->db->group_by('att_type');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function count_staffs($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
	            $this->db->or_like('middlename', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('staff_id', $value);
				$this->db->or_like('gender', $value);
				$this->db->or_like('email', $value);
				$this->db->or_like('phone_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('staff_plus_nok_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_staffs($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
	            $this->db->or_like('middlename', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('staff_id', $value);
				$this->db->or_like('gender', $value);
				$this->db->or_like('email', $value);
				$this->db->or_like('phone_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('staff_plus_nok_view');
	        $this->db->order_by('staff_id','ASC');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get();	        
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STAFFS');
	        $this->db->trans_complete();
			return $rs->result_array();			
		}

		public function count_active_staffs($value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || staff_id LIKE '%$value%' || gender LIKE '%$value%' || email LIKE '%$value%' || phone_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('staff_plus_nok_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_active_staffs($limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || staff_id LIKE '%$value%' || gender LIKE '%$value%' || email LIKE '%$value%' || phone_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('staff_plus_nok_view');
	        $this->db->where('status', 'active');
	        $this->db->order_by('staff_id','ASC');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get();
	        return $rs->result_array();

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SELECT', 'staff');
		}

		//Codes for profile
		public function update_profile($staff_record, $staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('staff', $staff_record, array('staff_id' => $staff_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE OWN PROFILE', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function certify_qualification($qualification_record, $q_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('q_id', $q_id);
			$this->db->update('qualification', $qualification_record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'CERTIFY QUALIFICATION', 'QUALIFICATION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function remove_qualification($q_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('q_id', $q_id);
			$this->db->delete('qualification');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REMOVE QUALIFICATION', 'QUALIFICATION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_current_password($staff_id, $hashed_current_password){
			$q = $this->db->get_where('staff', array('staff_id' => $staff_id, 'password' => $hashed_current_password));
			return $q->num_rows();
		}

		public function update_password($staff_id, $password_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('staff', $password_record, array('staff_id' => $staff_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE PASSWORD', 'STAFF');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function retrieve_profile_picture($staff_id){
			$this->db->where('staff_id', $staff_id);		
			$query = $this->db->get('staff');
			return $query->row_array();
		}


		//load role data to fill assign role @den//
		public function load_assign_role(){
			$query=$this->db->get('role_type','asc');
			return $query->result_array();
		}
		// end load role data to fill assign role @den//

		//load staff data to fill assign role @den//
		public function load_staff(){
			$query=$this->db->get('staff','asc');
			return $query->result_array();
		}
		// end load staff data to fill assign role @den//
        
        //save staff data to fill assign role @den//
        public function save_assignment_role($record){
        	$this->db->insert('role',$record);
        	return TRUE;
        }

        // retrieve all staff role//
        public function retrieve_staff_roles(){
        $query = $this->db->query('select distinct staff.staff_id,firstname,lastname,role.role_type_id,role_name from staff ,role_type,role where staff.staff_id=role.staff_id and role.role_type_id=role_type.role_type_id');
        return $query;
        }
        // retrieve by staff_id//
        public function retrieve_staff_role($staff_id){
        $query = $this->db->query('select staff.staff_id,firstname,lastname,role.role_type_id,role_name from staff ,role_type,role where staff.staff_id="'.$staff_id.'" and staff.staff_id=role.staff_id and role.role_type_id=role_type.role_type_id');
        return $query->row_array();	
        }

        //save edited role @den/

        public function save_role_edit($record,$id){
        	$this->db->set($record);
        	$this->db->where('staff_id',$id);
        	$this->db->update('role',$record);
        	return true;
        }
         //delete @den//
        public function delete_staff_role($role_type_id, $staff_id){
        
        	$this->db->query('delete from role where role_type_id='.$role_type_id.' and staff_id="'.$staff_id.'"');
            return true;
        }
        // retrieve  staffs by id @den//
        public function all_staff($staff_id){

        	$this->db->where('staff_id',$staff_id);
        	$query=$this->db->get('staff');
        	return $query->row_array();

        }
 // save  staffs qualification @den//
        public function save_qualification($record){
        	$this->db->trans_start();
        	$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('qualification', $record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD QUALIFICATION', 'QUALIFICATION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
        }
   // retrieve  staffs by id @den//
        public function qualification_staff($staff_id){
        	$this->db->where('staff_id',$staff_id);
        	$query = $this->db->get('qualification');
        	return $query->row_array();

        }
        public function save_edit_qualification($record, $q_id){
        	$this->db->trans_start();
        	$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

        	$this->db->where('q_id', $q_id);
        	$this->db->update('qualification',$record);
        	//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE QUALIFICATION', 'QUALIFICATION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
        }


        //PICHAAAAA
        public function staff_qualification($staff_id){
        	$this->db->where('staff_id',$staff_id);
        	$query=$this->db->get('qualification');
        	return $query->result_array();
        }

        //AVATAR
		public function in_staff_image($data, $staff_id){
        	$this->db->trans_start();
        	$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

        	$this->db->where('staff_id', $staff_id);
        	$this->db->update('staff', $data);
        	//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPLOAD AVATAR', 'STAFF');
			$this->db->trans_complete();
			
			return $this->feedbackMsg();
        }


        public function select_by_staff_and_dept_id($staff_id, $dept_id){
        	$this->db->select('dept_id');
			$this->db->from('department_staff_view');
			$this->db->where('staff_id', $staff_id);
			$this->db->where('dept_id', $dept_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array()['dept_id'];
        }		
	}