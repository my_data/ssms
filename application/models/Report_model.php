<?php
	class Report_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function fetch_dormitories(){
	        $this->db->select('dorm_id, dorm_name, location, capacity, admission_no, concat(firstname, " ", lastname) AS names');
	        $this->db->from('dormitory');
	        $this->db->join('teacher', 'dormitory.teacher_id = teacher.staff_id', 'left');	
			$this->db->join('staff', 'staff.staff_id = teacher.staff_id', 'left');
	        $this->db->order_by('dorm_name','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function fetch_classes(){
			$this->db->select('*');
	        $this->db->from('all_class_streams_view');
	        $this->db->order_by('class_id','ASC');
	        $this->db->order_by('stream','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function fetch_departments(){

	        $this->db->select('department_view.*, (SELECT count(*) from department_staff_view where dept_id=department_view.dept_id) as no_of_staffs');
	        $this->db->from('department_view');
	        $this->db->order_by('dept_id','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function fetch_staffs(){
	        $this->db->select('*');
	        $this->db->from('staff');
	        $this->db->order_by('firstname','ASC');
	        $this->db->order_by('lastname','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function fetch_students($value){
			if(!empty($value)){
				$this->db->where("class_name LIKE '%$value%'");
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->order_by('class_id','ASC');
	        $this->db->order_by('firstname','ASC');
	        $this->db->order_by('lastname','ASC');
	        $this->db->order_by('admission_no','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function get_class_name($value){
			if(!empty($value)){
				$this->db->where("class_name LIKE '%$value%'");
	        }

	        $this->db->select('class_name');
	        $this->db->from('class_level');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function count_class_levels(){
			$rs = $this->db->query('SELECT COUNT(DISTINCT(class_name)) as count FROM class_level');
			return $rs->row_array()['count'];
		}

		public function count_total_number_of_students($value){
			if(!empty($value)){
				$this->db->where("class_name LIKE '%$value%'");
	        }

	        $this->db->select('COUNT(*) as total_no_students');
	        $this->db->from('active_students_view');

			$rs = $this->db->get();			
	        return $rs->row_array()['total_no_students'];
		}

		public function count_student_per_class($value){
			if(!empty($value)){
				$this->db->where("active_students_view.class_name LIKE '%$value%'");
	        }

			$this->db->select('class_level.class_name, COUNT(admission_no) as nos');
	        $this->db->from('active_students_view');
	        $this->db->join('class_level', 'class_level.class_id = active_students_view.class_id', 'right');
	        $this->db->group_by('class_level.class_id');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function get_class_asset_count(){
	        $this->db->select('class_name, asset_name, COUNT(*) no_of_assets');
	        $this->db->from('class_asset_view');
	        $this->db->group_by('class_stream_id');
	        $this->db->group_by('asset_name');
	        $this->db->order_by('class_name');

	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function get_class_assets(){
			$this->db->select('*');
	        $this->db->from('class_asset_view');
	        $this->db->order_by('class_name');
	        $this->db->order_by('status');

	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function get_class_asset_status(){
	        $this->db->select('status, COUNT(*) noa');
	        $this->db->from('class_asset_view');
	        $this->db->group_by('status');
	        
	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function get_domitory_asset(){
	        $this->db->select('dorm_name, asset_name, COUNT(*) no_of_assets');
	        $this->db->from('dorm_asset_view');
	        $this->db->group_by('dorm_id');
	        $this->db->group_by('asset_name');
	        $this->db->order_by('dorm_name');

	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function get_assets(){
			$this->db->select('*');
	        $this->db->from('dorm_asset_view');
	        $this->db->order_by('dorm_name');
	        $this->db->order_by('status');

	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function get_asset_status(){
	        $this->db->select('status, COUNT(*) noa');
	        $this->db->from('dorm_asset_view');
	        $this->db->group_by('status');
	        
	        $rs = $this->db->get();
	        return $rs->result_array();	
		}

		public function retrieve_all_selected_students($form){
			$this->db->select('selected_students.*, concat(firstname, " ", lastname) AS student_names');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('selected_students.form', $form);
			$this->db->where('academic_year.status', 'current_academic_year');
			$this->db->order_by('firstname','ASC');
			$this->db->order_by('lastname','ASC');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_transferred_in_students($value){
			if(!empty($value)){
				$this->db->where("(date_of_transfer LIKE '%$value%' || firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('student_transfer_in_view');
	        $this->db->order_by('admission_no','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function select_transferred_out_students($value){
			if(!empty($value)){
				$this->db->where("(date_of_transfer LIKE '%$value%' || firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('student_transfer_out_view');
	        $this->db->order_by('admission_no','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function retrieve_all_disabled_students(){
	        $this->db->select('*');
	        $this->db->from('disabled_students');
	        $this->db->where('statasi', 'active');
	        $this->db->order_by('firstname','ASC');
	        $this->db->order_by('lastname','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function count_disabled_students(){
	        $this->db->select('disability, COUNT(*) AS nds');
	        $this->db->from('disabled_students');
	        $this->db->where('statasi', 'active');
	        $this->db->group_by('disability');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function create_monthly_report($month, $year){
			$sql = 'SELECT t2.staff_id, t2.firstname, t2.lastname, (SELECT COUNT(*) FROM staff_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND staff_attendance_record.staff_id=t2.staff_id AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM staff_attendance_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND staff_attendance_record.staff_id=t2.staff_id) AS total,
								MAX(1st) AS "1",
								MAX(2nd) AS "2",
								MAX(3rd) AS "3",
								MAX(4th) AS "4",
								MAX(5th) AS "5",
								MAX(6th) AS "6",
								MAX(7th) AS "7",
								MAX(8th) AS "8",
								MAX(9th) AS "9",
								MAX(10th) AS "10",
								MAX(11th) AS "11",
								MAX(12th) AS "12",
								MAX(13th) AS "13",
								MAX(14th) AS "14",
								MAX(15th) AS "15",
								MAX(16th) AS "16",
								MAX(17th) AS "17",
								MAX(18th) AS "18",
								MAX(19th) AS "19",
								MAX(20th) AS "20",
								MAX(21st) AS "21",
								MAX(22nd) AS "22",
								MAX(23rd) AS "23",
								MAX(24th) AS "24",
								MAX(25th) AS "25",
								MAX(26th) AS "26",
								MAX(27th) AS "27",
								MAX(28th) AS "28",
								MAX(29th) AS "29",
								MAX(30th) AS "30",
								MAX(31st) AS "31"
								FROM
								(
								SELECT t.staff_id, t.firstname, t.lastname,
								CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
								CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
								CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
								CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
								CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
								CASE WHEN t.day="6" THEN t.att_type END AS "6th",
								CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
								CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
								CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
								CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
								CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
								CASE WHEN t.day="12" THEN t.att_type END AS "12th",
								CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
								CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
								CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
								CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
								CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
								CASE WHEN t.day="18" THEN t.att_type END AS "18th",
								CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
								CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
								CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
								CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
								CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
								CASE WHEN t.day="24" THEN t.att_type END AS "24th",
								CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
								CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
								CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
								CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
								CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
								CASE WHEN t.day="30" THEN t.att_type END AS "30th",
								CASE WHEN t.day="31" THEN t.att_type END AS "31st"
								FROM 
								(
								SELECT staff_attendance_record.staff_id, firstname, lastname, staff_attendance_record.att_id, att_type,
								CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
								WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
								WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
								WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
								WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
								WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
								WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
								WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
								WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
								WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
								WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
								WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
								WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
								WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
								WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
								WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
								WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
								WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
								WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
								WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
								WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
								WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
								WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
								WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
								WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
								WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
								WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
								WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
								WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
								WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
								WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
								ELSE NULL END AS DAY FROM staff_attendance_record JOIN attendance ON(staff_attendance_record.att_id = attendance.att_id) JOIN staff ON(staff_attendance_record.staff_id = staff.staff_id)
								) t 
								) t2 GROUP BY t2.staff_id';

								$query = $this->db->query($sql);
								return $query->result_array();
		}

		public function get_parents($value){
			if(!empty($value)){
				$this->db->where("(class_name LIKE '%$value%' || guardian_names LIKE '%$value%' || student_names LIKE '%$value%' || admission_no LIKE '%$value%' || p_region LIKE '%$value%')");
	        }

			$this->db->select('student_guardian_view.*, class_level.class_name');
	        $this->db->from('student_guardian_view');
	        $this->db->join('class_level', 'class_level.class_id = student_guardian_view.class_id');
	        $this->db->where('status', 'active');
	        $this->db->order_by('student_guardian_view.class_id', 'ASC');
	        $this->db->order_by('class_stream_id', 'ASC');
	        $this->db->order_by('student_firstname','ASC');
	        $this->db->order_by('student_lastname','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function get_parents_per_class_stream($csid){
			$this->db->select('student_guardian_view.*, class_level.class_name');
	        $this->db->from('student_guardian_view');
	        $this->db->join('class_level', 'class_level.class_id = student_guardian_view.class_id');
	        $this->db->where('status', 'active');
	        $this->db->where('class_stream_id', $csid);
	        $this->db->order_by('student_guardian_view.class_id', 'ASC');
	        $this->db->order_by('class_stream_id', 'ASC');
	        $this->db->order_by('student_firstname','ASC');
	        $this->db->order_by('student_lastname','ASC');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}


		public function get_academic_year($aid){
			$this->db->select('year');
	        $this->db->from('academic_year');
	        $this->db->where('id', $aid);
	        
	        $rs = $this->db->get();
	        return $rs->row_array()['year'];
		}

		public function get_student($admission_no){
			$this->db->select('*');
	        $this->db->from('student');
	        $this->db->where('student.admission_no', $admission_no);
	        
	        $rs = $this->db->get();
	        return $rs->row_array();
		}

		public function get_class_by_adm_no($admission_no, $aid){
			$this->db->select('concat(class_name, " ", stream) AS class_name, enrollment.admission_no');
	        $this->db->from('enrollment');
	        $this->db->join('academic_year', 'enrollment.academic_year = academic_year.id');
	        $this->db->join('class_level', 'class_level.class_id = enrollment.class_id');
	        $this->db->join('class_stream', 'enrollment.class_stream_id = class_stream.class_stream_id');
	        $this->db->where('enrollment.admission_no', $admission_no);
	        $this->db->where('enrollment.academic_year', $aid);
	        
	        $rs = $this->db->get();
	        return $rs->row_array();
		}

		public function select_result_by_academic_year($admission_no, $aid){
			$query = $this->db->query('SELECT * FROM student_progressive_results_view WHERE term_id IN (SELECT term_id FROM term WHERE aid='.$this->db->escape($aid).') AND admission_no='.$this->db->escape($admission_no).'');

			return $query->result_array();
		}

		public function checkIfResultsExist($admission_no, $term_name){
			$query = $this->db->query('SELECT COUNT(*) AS count FROM student_progressive_results_view WHERE term_name='.$this->db->escape($term_name).' AND admission_no='.$this->db->escape($admission_no).'');

			return $query->row_array();
		}

		public function select_term_id($aid, $term_name){
			$query = $this->db->query('SELECT term_id FROM term WHERE aid='.$this->db->escape($aid).' AND term_name='.$this->db->escape($term_name).'');

			return $query->row_array()['term_id'];
		}




		 public function assigned_teachers(){
                        $this->db->select('class_level.class_name, class_stream.stream, concat(staff.firstname, " ", staff.lastname) AS names, subject.subject_name');
                        $this->db->from('teaching_assignment');
                        $this->db->join('staff', 'teaching_assignment.teacher_id = staff.staff_id');
			$this->db->join('class_stream', 'teaching_assignment.class_stream_id = class_stream.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->join('subject', 'teaching_assignment.subject_id = subject.subject_id');
                        $this->db->where('teaching_assignment.term_id IN(SELECT term_id FROM term WHERE is_current="yes")');
                        $this->db->order_by('firstname','ASC');
                        $this->db->order_by('lastname','ASC');

                        $query = $this->db->get();
                        return $query->result_array();
                }

	}
