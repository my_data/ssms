<?php
	class Subject_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function get_nos_for_class($csid){
			$this->db->select('number_of_subjects');
			$this->db->from('class_stream');
			$this->db->where('class_stream_id', $csid);

			$q = $this->db->get();
			return $q->row_array()['number_of_subjects'];
		}

		public function count_subjects_by_level($level){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('studied_by', $level);
			$this->db->or_where('studied_by', "BOTH");

			$rs = $this->db->get();
			return $rs->num_rows();
		}

		public function select_subjects_by_level($level){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('studied_by', $level);
			$this->db->or_where('studied_by', "BOTH");

			$rs = $this->db->get();
			return $rs->result_array();
		}

		//Assign subject to teacher function
		public function insert_teaching_assignment($data){
			$this->db->insert('teaching_assignment', $data);
		}

		public function select_from_subjects($subject_id = FALSE){
			$this->db->select('*');
			$this->db->from('subject');

			if($subject_id === FALSE){
				$query = $this->db->get();
				return $query->result_array();
			}

			$this->db->where('subject_id', $subject_id);
			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_optional_subjects($subject_choice){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('subject_choice', $subject_choice);
			$this->db->where('subject_category', 'T');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_class_stream_by_id($subject_id, $class_id){
			//$query = $this->db->query('CALL get_class_stream_id_procedure("'.$subject_id.'", "'.$class_id.'")');
			$query = $this->db->query('SELECT class_stream_id FROM class_stream_subject WHERE subject_id="'.$subject_id.'" and class_stream_id IN(SELECT class_stream_id FROM class_stream WHERE class_id="'.$class_id.'")');
			return $query->row_array();

			//Got some problem with running the stored procedure
		}
		public function get_department_id_by_subject_id($subject_id){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('subject_id', $subject_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_subject_by_dept_id($dept_id){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('dept_id', $dept_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function count_subject_teacher_by_class($class_stream_id = FALSE, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || staff_id LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('class_subject_teachers_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_subject_teacher_by_class($class_stream_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || staff_id LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

	        //The view for the teacher assigned to teach the particular class_stream

	        $this->db->select('*');
	        $this->db->from('class_subject_teachers_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->order_by('firstname','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW TEACHERS TEACHING A CLASS', 'TEACHING_ASSIGNMENT');
			$this->db->trans_complete();

			return $rs->result_array();
		}


		public function select_topics(){
			$this->db->select('*');
	        $this->db->from('topic');

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		public function insert_teaching_log($log_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_log', $log_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD TEACHING LOG', 'LOG');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function update_start_and_topic_in_log($log_record, $log_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_log', $log_record, array('log_id' => $log_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE LOG', 'LOG');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_department_by_subject($subject_id){
			$this->db->select('dept_id');
			$this->db->from('subject');
			$this->db->where('subject_id', $subject_id);

			$query = $this->db->get();
			return $query->row_array()['dept_id'];
		}

		public function select_all_logs($log_id){
			$this->db->select('*');
			$this->db->from('teaching_log');
			$this->db->where('log_id', $log_id);

			$query = $this->db->get();	        
			return $query->row_array();
		}

		public function add_log_comment($log_id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_log', $record, array('log_id' => $log_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'COMMENTED', 'LOG');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function get_logs($dept_id = FALSE, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

			$this->db->select('class_level.class_name, class_stream.stream, teaching_log.log_id, teaching_log.teacher_id, firstname, lastname, teaching_log.subject_id, subject_name, teaching_log.topic_id, teaching_log.approved, topic_name, term_id, start_date, end_date, teaching_log.class_stream_id, teaching_log.comment, class_level.class_id, class_level.class_name, stream');
			$this->db->from('teaching_log');
			$this->db->join('staff', 'staff.staff_id = teaching_log.teacher_id');
			$this->db->join('subject', 'subject.subject_id = teaching_log.subject_id');
			$this->db->join('topic', 'topic.topic_id = teaching_log.topic_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_log.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_log.dept_id', $dept_id);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW LOG', 'LOG');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function count_logs_by_dept($dept_id, $value){
			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

	        $this->db->select('teaching_log.teacher_id, firstname, lastname, teaching_log.subject_id, subject_name, teaching_log.topic_id, topic_name, term_id, start_date, end_date, teaching_log.class_stream_id, teaching_log.comment, class_level.class_id, class_level.class_name, stream');
			$this->db->from('teaching_log');
			$this->db->join('staff', 'staff.staff_id = teaching_log.teacher_id');
			$this->db->join('subject', 'subject.subject_id = teaching_log.subject_id');
			$this->db->join('topic', 'topic.topic_id = teaching_log.topic_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_log.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_log.dept_id', $dept_id);

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function get_log_by_teacher_id($teacher_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

			$this->db->select('teaching_log.approved, class_level.class_name, class_stream.stream, teaching_log.log_id, teaching_log.teacher_id, firstname, lastname, teaching_log.subject_id, subject_name, teaching_log.topic_id, topic_name, term_id, start_date, end_date, teaching_log.class_stream_id, teaching_log.comment, class_level.class_id, class_level.class_name, stream');
			$this->db->from('teaching_log');
			$this->db->join('staff', 'staff.staff_id = teaching_log.teacher_id');
			$this->db->join('subject', 'subject.subject_id = teaching_log.subject_id');
			$this->db->join('topic', 'topic.topic_id = teaching_log.topic_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_log.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_log.teacher_id', $teacher_id);
			$this->db->limit($limit, $offset);
			/*$this->db->where('subject_id', $subject_id);
			$this->db->where('term_id', $term_id);*/

			$query = $this->db->get();
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW LOG', 'LOG');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function count_logs_by_tid($teacher_id, $value){
			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

	        $this->db->select('teaching_log.teacher_id, firstname, lastname, teaching_log.subject_id, subject_name, teaching_log.topic_id, topic_name, term_id, start_date, end_date, teaching_log.class_stream_id, teaching_log.comment, class_level.class_id, class_level.class_name, stream');
			$this->db->from('teaching_log');
			$this->db->join('staff', 'staff.staff_id = teaching_log.teacher_id');
			$this->db->join('subject', 'subject.subject_id = teaching_log.subject_id');
			$this->db->join('topic', 'topic.topic_id = teaching_log.topic_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = teaching_log.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('teaching_log.teacher_id', $teacher_id);

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function approve_log($log_id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_log', $record, array('log_id' => $log_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'APPROVED', 'LOG');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//Used in Log
		public function retrieve_subtopics(/*$topic_id*/){
			$this->db->select('*');
			$this->db->from('sub_topic');
			$this->db->join('topic', 'topic.topic_id = sub_topic.topic_id');			
			//$this->db->where('sub_topic.topic_id', $topic_id);

			$query = $this->db->get();	        
			return $query->result_array();
		}

		public function count_all_topics_per_class_and_subject($class_stream_id, $subject_id, $value){
			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('topic');
	        $this->db->where('topic.class_stream_id', $class_stream_id);
			$this->db->where('topic.subject_id', $subject_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}


		 public function get_topics_by_class_and_subject($class_stream_id, $subject_id){
                        $this->db->select('*');
                        $this->db->from('topic');
                        $this->db->join('subject', 'subject.subject_id = topic.subject_id');
                        $this->db->join('class_stream', 'class_stream.class_stream_id = topic.class_stream_id');
		//	$this->db->join('teaching_assignment', 'class_stream.class_stream_id = teaching_assignment.class_stream_id');
                        $this->db->where('topic.class_stream_id', $class_stream_id);
                        $this->db->where('topic.subject_id', $subject_id);
                        $this->db->order_by('topic_name','ASC');

                        $query = $this->db->get();
                        return $query->result_array();
                }

		public function select_topics_by_class($class_stream_id, $subject_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(topic_name LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('topic');
			$this->db->join('subject', 'subject.subject_id = topic.subject_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = topic.class_stream_id');
			$this->db->where('topic.class_stream_id', $class_stream_id);
			$this->db->where('topic.subject_id', $subject_id);
			$this->db->order_by('topic_name','ASC');
	        $this->db->limit($limit, $offset);

			$query = $this->db->get();
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW TOPICS', 'TOPIC');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function count_all_subtopic_by_topic($class_stream_id, $topic_id, $value){
			if(!empty($value)){
				$this->db->where("(subtopic_name LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('sub_topic');
	        $this->db->where('sub_topic.class_stream_id', $class_stream_id);
			$this->db->where('sub_topic.topic_id', $topic_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_subtopics_by_topic($class_stream_id, $topic_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(subtopic_name LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('sub_topic');
			$this->db->join('subject', 'subject.subject_id = sub_topic.subject_id');
			$this->db->join('topic', 'topic.topic_id = sub_topic.topic_id');
			$this->db->join('class_stream', 'class_stream.class_stream_id = sub_topic.class_stream_id');
			$this->db->where('sub_topic.class_stream_id', $class_stream_id);
			$this->db->where('sub_topic.topic_id', $topic_id);
			$this->db->order_by('subtopic_name','ASC');
	        $this->db->limit($limit, $offset);

			$query = $this->db->get();
	        //Activity Log
	       	$this->activity_log($this->session->userdata('staff_id'), 'VIEW SUB-TOPICS', 'TOPIC');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function insert_new_topic($topic_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('topic', $topic_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD TOPIC', 'TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function insert_new_subtopic($subtopic_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('sub_topic', $subtopic_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD SUB-TOPIC', 'SUB-TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_topic($topic_id, $topic_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('topic', $topic_record, array('topic_id' => $topic_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE TOPIC', 'TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_subtopic($subtopic_id, $subtopic_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('sub_topic', $subtopic_record, array('subtopic_id' => $subtopic_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE SUB-TOPIC', 'SUB-TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_topic($topic_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('topic', NULL, array('topic_id' => $topic_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REMOVE TOPIC', 'TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_subtopic($subtopic_id){
			$this->db->trans_start();
			$this->CI_mySQL('sub_topic', NULL, array('subtopic_id' => $subtopic_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REMOVE SUB-TOPIC', 'SUB-TOPIC');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_subject_by_topic_id($topic_id){
			$this->db->select('*');
			$this->db->from('topic');
			$this->db->join('subject', 'subject.subject_id = topic.subject_id');
			$this->db->where('topic_id', $topic_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}


		public function select_subject_teachers(){
			$this->db->select('subject_teacher.*, subject_name, firstname, middlename, lastname');
			$this->db->from('subject');
			$this->db->join('subject_teacher', 'subject.subject_id = subject_teacher.subject_id');
			$this->db->join('staff', 'staff.staff_id = subject_teacher.teacher_id');
			$this->db->order_by('firstname', 'ASC');
			$this->db->order_by('middlename', 'ASC');
			$this->db->order_by('firstname', 'ASC');

			$query = $this->db->get();
			return $query->result_array();
		}

		//Function hii inatumika ku_authenticate user kwenye subtopics
		public function select_class_stream_and_subject($topic_id){
			$this->db->select('class_stream_id, subject_id');
			$this->db->from('topic');
			$this->db->where('topic_id', $topic_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}
	}
