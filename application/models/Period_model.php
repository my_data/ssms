<?php
	class Period_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function insert_period($period_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('period', $period_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'PERIOD');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function view_period($period_no = FALSE){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('period');

			if($period_no === FALSE){
				$query = $this->db->get();
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'PERIOD');
				$this->db->trans_complete();
				return $query->result_array();
			}

			$this->db->where('period_no', $period_no);
			$query = $this->db->get();
			
			$this->db->trans_complete();
			return $query->row_array();
		}

		public function update_period($period_no, $period_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('period', $period_record, array('period_no' => $period_no), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'PERIOD');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_period($period_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('period', NULL, array('period_no' => $period_no), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'PERIOD');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function check_periods(){
			$this->db->select('COUNT(*) AS count');
			$this->db->from('period');

			$query = $this->db->get();
			return $query->row_array()['count'];
		}

		public function insert_break($break_record){
			$rs = $this->db->query('SELECT end_time FROM period WHERE period_no="'.$break_record['after_period_no'].'"');

			$next_period = $break_record['after_period_no'] + 1;
			$rs_next = $this->db->query('SELECT start_time FROM period WHERE period_no="'.$next_period.'"');

			if($rs_next->row_array()['start_time'] == $rs->row_array()['end_time']){
				$this->session->set_flashdata('error_message', "Cannot Add Time for BREAK after this period, check for the time space in the period");
				redirect('periods');
			}
			else{
				$duration = (strtotime($rs_next->row_array()['start_time']) - strtotime($rs->row_array()['end_time']))/60;

				$break_rec_values = array(
					'after_period' => $break_record['after_period_no'],
					'description' => $break_record['description'],
					'duration' => $duration
				);

				/*echo "<pre>";
					print_r($break_rec_values);
				echo "</pre>";die;
*/
				$this->db->trans_start();
				$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
				$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

				$this->CI_mySQL('break_time', $break_rec_values, NULL, 'INSERT');
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'BREAK_TIME');
				$this->db->trans_complete();

				return $this->feedbackMsg();
			}
		}

		public function select_breaks(){
			$this->db->select('*');
			$this->db->from('break_time');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_break_by_id($bid){
			$this->db->select('*');
			$this->db->from('break_time');
			$this->db->where('bid', $bid);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_break($bid, $break_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('break_time', $break_record, array('bid' => $bid), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'CHANGE BREAK DESCRIPTION');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function break_period($bid){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('break_time', NULL, array('bid' => $bid), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'REMOVE BREAK');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}
	}