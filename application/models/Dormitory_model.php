<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	

	Class Dormitory_model extends MY_Model{

		function __construct(){
			parent::__construct();
			$this->load->database();	
		}

		//Used in student profile
		public function select_dormitory_by_student($admission_no){
			$rs = $this->db->query('SELECT * FROM dormitory WHERE dorm_id =(SELECT dorm_id FROM student WHERE admission_no='.$this->db->escape($admission_no).') LIMIT 1');
			return $rs->row_array();
		}

		public function view_domitories($teacher_id = FALSE){
			$this->db->select('dorm_id, dorm_name, location, capacity, admission_no, concat(firstname, " ", lastname) AS names');
			$this->db->from('dormitory');
			$this->db->join('teacher', 'dormitory.teacher_id = teacher.staff_id', 'left');	
			$this->db->join('staff', 'staff.staff_id = teacher.staff_id', 'left');

			if ($teacher_id === FALSE){	
				$query = $this->db->get();
            	return $query->result_array();
        	}

        	$this->db->where('teacher_id', $teacher_id);

        	$query = $this->db->get();
        	return $query->row_array();
		}

		public function view_dorm($dom = FALSE){	//Used for the purpose of all in view
			if ($dom === FALSE){

			$this->db->select('*');
			$this->db->from('dorm_capacity_view');

            $query = $this->db->get();
            return $query->result_array();

        	}
		}

		public function submit($record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('dormitory', $record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function edit_domitory($id){
			$this->db->where('dorm_id', $id);
			$query = $this->db->get('dormitory');
			if($query->num_rows() > 0 ){
				return $query->row();
			}
			else {
				return false;
			}

		}

		public function update_domitory($id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('dormitory', $record, array('dorm_id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_domitory($dorm_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dormitory', NULL, array('dorm_id' => $dorm_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function count_domitory_asset($dorm_id, $value){
			if(!empty($value)){
				$this->db->where("(status LIKE '%$value%' || asset_name LIKE '%$value%' || description LIKE '%$value%' || asset_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('dorm_asset_view');
	        $this->db->where('dorm_id', $dorm_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function get_domitory_asset($dorm_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

	        if(!empty($value)){
				$this->db->where("(status LIKE '%$value%' || asset_name LIKE '%$value%' || description LIKE '%$value%' || asset_no LIKE '%$value%')");
	        }	        

	        $this->db->select('*');
	        $this->db->from('dorm_asset_view');
	        $this->db->where('dorm_id', $dorm_id);
	        $this->db->order_by('asset_no','ASC');
	        $this->db->limit($limit, $offset);
	        $rs = $this->db->get(); 
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DORMITORY ASSETS');
			$this->db->trans_complete();
			return $rs->result_array();	
		}

		public function asset_reports($dorm_id){
			$dorm_id = $this->db->escape($dorm_id);
			$q = $this->db->query('SELECT `dormitory`.`dorm_id` AS `dorm_id`,`dorm_asset`.`asset_name` AS `asset_name`,count(0) AS `total_assets` FROM (`dormitory` JOIN `dorm_asset` ON((`dormitory`.`dorm_id` = `dorm_asset`.`dorm_id`))) WHERE dormitory.dorm_id='.$dorm_id.' group by `dorm_asset`.`asset_name` union SELECT `dormitory`.`dorm_id` AS `dorm_id`,"Total" AS `Total`,count(0) AS `count(*)` FROM (`dormitory` JOIN `dorm_asset` ON((`dormitory`.`dorm_id` = `dorm_asset`.`dorm_id`))) WHERE dormitory.dorm_id='.$dorm_id.'');

			return $q->result_array();
		}

		public function insert_new_asset($asset_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dorm_asset', $asset_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DORMITORY_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_asset($asset_record, $asset_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dorm_asset', $asset_record, array('asset_no' => $asset_no), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DORMITORY_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_asset($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dorm_asset', NULL, array('id' => $id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DORMITORY_ASSET');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function assign_dorm_leader($dorm_leader_record, $dorm_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('dormitory', $dorm_leader_record, array('dorm_id' => $dorm_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD DORMITORY LEADER', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

//students part


		public function get_dom(){
			$condition = 'no_of_students < capacity';

			$this->db->select('*');
			$this->db->from('dorm_capacity_view');
			$this->db->where($condition);

			$query = $this->db->get();
			return $query->result();
		}


		public function submit1(){

			if ($this->input->post('submit')){

				$query=array(
					's_name'=>$this->input->post('student_name'),
					'dorm_id'=>$this->input->post('dorm_name')
				);

				$this->db-> insert ('students',$query);

			if($this->db-> affected_rows > 0){
				return true; 
			}else {
				return false;
			}
				}
		}


		public function shiftdomitory_student($id){
			$this->db->where('admission_no', $id);
			$query = $this->db->get('student');
			if($query->num_rows() > 0 ){

				return $query->row();
			}
			else {
				return false;
			}
		}

		/*
		after_shift_student_to_another_dormitory_trigger
		BEGIN 
		IF(OLD.dorm_id != NEW.dorm_id) THEN 
			UPDATE accomodation_history SET end_date=NOW() WHERE admission_no=OLD.admission_no AND end_date IS NULL; INSERT INTO accomodation_history(admission_no, dorm_id, start_date) VALUES(NEW.admission_no, NEW.dorm_id, NOW()); 
        END IF;
		END*/

		public function update_shiftdomitory($admission_no, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE dormitory SET admission_no=NULL WHERE dorm_id=(SELECT dorm_id FROM student WHERE admission_no='.$this->db->escape($admission_no).' LIMIT 1)');
			$this->CI_mySQL('student', $record, array('admission_no' => $admission_no), 'UPDATE');

			$this->db->query('UPDATE accomodation_history SET end_date="'.date('Y-m-d').'" WHERE admission_no='.$this->db->escape($admission_no).' AND end_date IS NULL');

			$this->db->query('INSERT INTO accomodation_history(admission_no, dorm_id, start_date) VALUES('.$this->db->escape($admission_no).', '.$this->db->escape($record['dorm_id']).', "'.date('Y-m-d').'")');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SHIFT STUDENT TO ANOTHER DORMITORY', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		//assign teacher

		public function get_teacher($teacher = FALSE){
			if ($teacher === FALSE){

               $this->db->select('teacher.staff_id, concat(firstname, " ", lastname) AS names');
               $this->db->from('staff');
               $this->db->join('teacher', 'staff.staff_id = teacher.staff_id');
               $this->db->where('teacher.staff_id', $teacher);
               $this->db->where('status', 'active');

               $query = $this->db->get();
               return $query->result_array();
        	}
		}


		public function assign_t($id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE dormitory SET teacher_id=NULL WHERE teacher_id='.$this->db->escape($record['teacher_id']).'');
			$this->db->query('CALL assign_dorm_master_procedure('.$this->db->escape($id).', '.$this->db->escape($record['teacher_id']).')');
			$this->CI_getProcedureError();

			$this->CI_mySQL('dormitory', $record, array('dorm_id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD DORMITORY MASTER', 'DORMITORY');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


	   public function get_dorm_id_by_dorm_master_id($teacher_id){
	   		$this->db->select('*');
	   		$this->db->from('dormitory');
	   		$this->db->where('teacher_id', $teacher_id);
	   		$this->db->limit('1');

	   		$query = $this->db->get();  
	      	return $query->row_array();  
	   }


		public function insert_student_roll_call($roll_call_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('student_roll_call_record', $roll_call_record, NULL, 'INSERT_BATCH');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD ROLL-CALL', 'STUDENT ROLL-CALL');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function select_dormitory_by_id($dorm_id){
			$this->db->select('*');
			$this->db->from('dormitory');
			$this->db->where('dorm_id', $dorm_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		//The below function is not used yet
		public function create_student_dorm_roll_call_report($date, $dorm_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT t1.admission_no, t1.firstname, t1.lastname, t1.date_, t1.month, t1.dorm_id, MAX(A) AS "day_of_month" FROM ( SELECT t.id, t.admission_no, t.firstname, t.lastname, t.dorm_id, t.att_id, t.att_type, t.date_, t.month,  CASE WHEN t.day="SIKU" THEN t.att_type END AS "A" FROM ( SELECT id, admission_no, student.firstname, student.lastname, student_roll_call_record.dorm_id, student_roll_call_record.att_id, att_type, date_, month, CASE WHEN date_="'.$date.'" THEN "SIKU" ELSE NULL END AS day  from student_roll_call_record join student using(admission_no) join attendance using(att_id) WHERE student_roll_call_record.dorm_id='.$dorm_id.') t ) t1 GROUP BY t1.admission_no');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ROLL-CALL REPORT', 'DORMITORY');
			$this->db->trans_complete();

			return $query->result_array();
		}//End function NOT used


		public function select_student_roll_call_record($dorm_id, $date, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(admission_no LIKE '%$value%' || firstname LIKE '%$value%' || lastname LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('daily_student_roll_call_view');
			$this->db->where('dorm_id', $dorm_id);
			$this->db->where('date_', $date);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			$this->activity_log($this->session->userdata('staff_id'), 'DAILY ROLL-CALL', 'DORMITORY');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function select_daily_roll_call_summary($dorm_id, $date){
			$this->db->select('att_type, count(att_type) AS no_of_students, description, dorm_id, date_');
			$this->db->from('daily_student_roll_call_view');
			$this->db->where('dorm_id', $dorm_id);
			$this->db->where('date_', $date);
			$this->db->group_by('att_type');

			$query = $this->db->get();
			return $query->result_array();
		}


		//I HATE HII KITU JAMANI
		public function create_monthly_roll_call_report($dorm_id, $month, $year, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$sql = 'SELECT t2.admission_no, t2.firstname, t2.lastname, t2.dorm_id, t2.month, (SELECT COUNT(*) FROM student_roll_call_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_roll_call_record.admission_no=t2.admission_no AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM student_roll_call_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_roll_call_record.admission_no=t2.admission_no) AS total,
									MAX(1st) AS "1",
									MAX(2nd) AS "2",
									MAX(3rd) AS "3",
									MAX(4th) AS "4",
									MAX(5th) AS "5",
									MAX(6th) AS "6",
									MAX(7th) AS "7",
									MAX(8th) AS "8",
									MAX(9th) AS "9",
									MAX(10th) AS "10",
									MAX(11th) AS "11",
									MAX(12th) AS "12",
									MAX(13th) AS "13",
									MAX(14th) AS "14",
									MAX(15th) AS "15",
									MAX(16th) AS "16",
									MAX(17th) AS "17",
									MAX(18th) AS "18",
									MAX(19th) AS "19",
									MAX(20th) AS "20",
									MAX(21st) AS "21",
									MAX(22nd) AS "22",
									MAX(23rd) AS "23",
									MAX(24th) AS "24",
									MAX(25th) AS "25",
									MAX(26th) AS "26",
									MAX(27th) AS "27",
									MAX(28th) AS "28",
									MAX(29th) AS "29",
									MAX(30th) AS "30",
									MAX(31st) AS "31"
									FROM
									(
									SELECT t.admission_no, t.firstname, t.lastname, t.dorm_id, t.month,
									CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
									CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
									CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
									CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
									CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
									CASE WHEN t.day="6" THEN t.att_type END AS "6th",
									CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
									CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
									CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
									CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
									CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
									CASE WHEN t.day="12" THEN t.att_type END AS "12th",
									CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
									CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
									CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
									CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
									CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
									CASE WHEN t.day="18" THEN t.att_type END AS "18th",
									CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
									CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
									CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
									CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
									CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
									CASE WHEN t.day="24" THEN t.att_type END AS "24th",
									CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
									CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
									CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
									CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
									CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
									CASE WHEN t.day="30" THEN t.att_type END AS "30th",
									CASE WHEN t.day="31" THEN t.att_type END AS "31st"
									FROM 
									(
									SELECT student_roll_call_record.admission_no, firstname, lastname, student_roll_call_record.dorm_id, student_roll_call_record.att_id, att_type, month,
									CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
									WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
									WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
									WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
									WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
									WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
									WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
									WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
									WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
									WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
									WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
									WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
									WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
									WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
									WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
									WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
									WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
									WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
									WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
									WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
									WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
									WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
									WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
									WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
									WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
									WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
									WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
									WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
									WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
									WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
									WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
									ELSE NULL END AS DAY FROM student_roll_call_record JOIN attendance ON(student_roll_call_record.att_id = attendance.att_id) JOIN student ON(student_roll_call_record.admission_no = student.admission_no) WHERE student_roll_call_record.dorm_id="'.$dorm_id.'"
									) t 
									) t2 WHERE (t2.admission_no LIKE "%'.$value.'%" || t2.firstname LIKE "%'.$value.'%" || t2.lastname LIKE "%'.$value.'%")
									GROUP BY t2.admission_no LIMIT '.$limit.' OFFSET '.$offset.'';

									$query = $this->db->query($sql);

									//Activity Log
		       						$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'MONTHLY ROLL CALL');
									$this->db->trans_complete();

									return $query->result_array();
			}
			else{
				$sql = 'SELECT t2.admission_no, t2.firstname, t2.lastname, t2.dorm_id, t2.month, (SELECT COUNT(*) FROM student_roll_call_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_roll_call_record.admission_no=t2.admission_no AND att_id=(SELECT att_id FROM attendance WHERE description="Present"))  AS avg, (SELECT COUNT(*) FROM student_roll_call_record WHERE date_ LIKE "%'.$year.'-'.$month.'%" AND student_roll_call_record.admission_no=t2.admission_no) AS total,
									MAX(1st) AS "1",
									MAX(2nd) AS "2",
									MAX(3rd) AS "3",
									MAX(4th) AS "4",
									MAX(5th) AS "5",
									MAX(6th) AS "6",
									MAX(7th) AS "7",
									MAX(8th) AS "8",
									MAX(9th) AS "9",
									MAX(10th) AS "10",
									MAX(11th) AS "11",
									MAX(12th) AS "12",
									MAX(13th) AS "13",
									MAX(14th) AS "14",
									MAX(15th) AS "15",
									MAX(16th) AS "16",
									MAX(17th) AS "17",
									MAX(18th) AS "18",
									MAX(19th) AS "19",
									MAX(20th) AS "20",
									MAX(21st) AS "21",
									MAX(22nd) AS "22",
									MAX(23rd) AS "23",
									MAX(24th) AS "24",
									MAX(25th) AS "25",
									MAX(26th) AS "26",
									MAX(27th) AS "27",
									MAX(28th) AS "28",
									MAX(29th) AS "29",
									MAX(30th) AS "30",
									MAX(31st) AS "31"
									FROM
									(
									SELECT t.admission_no, t.firstname, t.lastname, t.dorm_id, t.month,
									CASE WHEN t.day="1" THEN t.att_type END AS "1st", 
									CASE WHEN t.day="2" THEN t.att_type END AS "2nd", 
									CASE WHEN t.day="3" THEN t.att_type END AS "3rd", 
									CASE WHEN t.day="4" THEN t.att_type END AS "4th", 
									CASE WHEN t.day="5" THEN t.att_type END AS "5th", 
									CASE WHEN t.day="6" THEN t.att_type END AS "6th",
									CASE WHEN t.day="7" THEN t.att_type END AS "7th", 
									CASE WHEN t.day="8" THEN t.att_type END AS "8th", 
									CASE WHEN t.day="9" THEN t.att_type END AS "9th", 
									CASE WHEN t.day="10" THEN t.att_type END AS "10th", 
									CASE WHEN t.day="11" THEN t.att_type END AS "11th", 
									CASE WHEN t.day="12" THEN t.att_type END AS "12th",
									CASE WHEN t.day="13" THEN t.att_type END AS "13th", 
									CASE WHEN t.day="14" THEN t.att_type END AS "14th", 
									CASE WHEN t.day="15" THEN t.att_type END AS "15th", 
									CASE WHEN t.day="16" THEN t.att_type END AS "16th", 
									CASE WHEN t.day="17" THEN t.att_type END AS "17th", 
									CASE WHEN t.day="18" THEN t.att_type END AS "18th",
									CASE WHEN t.day="19" THEN t.att_type END AS "19th", 
									CASE WHEN t.day="20" THEN t.att_type END AS "20th", 
									CASE WHEN t.day="21" THEN t.att_type END AS "21st", 
									CASE WHEN t.day="22" THEN t.att_type END AS "22nd", 
									CASE WHEN t.day="23" THEN t.att_type END AS "23rd", 
									CASE WHEN t.day="24" THEN t.att_type END AS "24th",
									CASE WHEN t.day="25" THEN t.att_type END AS "25th", 
									CASE WHEN t.day="26" THEN t.att_type END AS "26th", 
									CASE WHEN t.day="27" THEN t.att_type END AS "27th", 
									CASE WHEN t.day="28" THEN t.att_type END AS "28th", 
									CASE WHEN t.day="29" THEN t.att_type END AS "29th", 
									CASE WHEN t.day="30" THEN t.att_type END AS "30th",
									CASE WHEN t.day="31" THEN t.att_type END AS "31st"
									FROM 
									(
									SELECT student_roll_call_record.admission_no, firstname, lastname, student_roll_call_record.dorm_id, student_roll_call_record.att_id, att_type, month,
									CASE WHEN date_="'.$year.'-'.$month.'-01" THEN "1" 
									WHEN date_="'.$year.'-'.$month.'-02" THEN "2" 
									WHEN date_="'.$year.'-'.$month.'-03" THEN "3" 
									WHEN date_="'.$year.'-'.$month.'-04" THEN "4" 
									WHEN date_="'.$year.'-'.$month.'-05" THEN "5" 
									WHEN date_="'.$year.'-'.$month.'-06" THEN "6" 
									WHEN date_="'.$year.'-'.$month.'-07" THEN "7" 
									WHEN date_="'.$year.'-'.$month.'-08" THEN "8" 
									WHEN date_="'.$year.'-'.$month.'-09" THEN "9" 
									WHEN date_="'.$year.'-'.$month.'-10" THEN "10" 
									WHEN date_="'.$year.'-'.$month.'-11" THEN "11" 
									WHEN date_="'.$year.'-'.$month.'-12" THEN "12" 
									WHEN date_="'.$year.'-'.$month.'-13" THEN "13" 
									WHEN date_="'.$year.'-'.$month.'-14" THEN "14" 
									WHEN date_="'.$year.'-'.$month.'-15" THEN "15" 
									WHEN date_="'.$year.'-'.$month.'-16" THEN "16" 
									WHEN date_="'.$year.'-'.$month.'-17" THEN "17" 
									WHEN date_="'.$year.'-'.$month.'-18" THEN "18" 
									WHEN date_="'.$year.'-'.$month.'-19" THEN "19" 
									WHEN date_="'.$year.'-'.$month.'-20" THEN "20" 
									WHEN date_="'.$year.'-'.$month.'-21" THEN "21" 
									WHEN date_="'.$year.'-'.$month.'-22" THEN "22" 
									WHEN date_="'.$year.'-'.$month.'-23" THEN "23" 
									WHEN date_="'.$year.'-'.$month.'-24" THEN "24"
									WHEN date_="'.$year.'-'.$month.'-25" THEN "25" 
									WHEN date_="'.$year.'-'.$month.'-26" THEN "26" 
									WHEN date_="'.$year.'-'.$month.'-27" THEN "27" 
									WHEN date_="'.$year.'-'.$month.'-28" THEN "28" 
									WHEN date_="'.$year.'-'.$month.'-29" THEN "29" 
									WHEN date_="'.$year.'-'.$month.'-30" THEN "30" 
									WHEN date_="'.$year.'-'.$month.'-31" THEN "31"  
									ELSE NULL END AS DAY FROM student_roll_call_record JOIN attendance ON(student_roll_call_record.att_id = attendance.att_id) JOIN student ON(student_roll_call_record.admission_no = student.admission_no) WHERE student_roll_call_record.dorm_id="'.$dorm_id.'"
									) t 
									) t2 GROUP BY t2.admission_no LIMIT '.$limit.' OFFSET '.$offset.'';

									$query = $this->db->query($sql);

									//Activity Log
		       						$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'MONTHLY ROLL CALL');
									$this->db->trans_complete();

									return $query->result_array();
			}
		}
		
		public function count_students_per_dormitory($dorm_id, $value){
			if(!empty($value)){
				$this->db->where("(admission_no LIKE '%$value%' || firstname LIKE '%$value%' || lastname LIKE '%$value%' || class_name LIKE '%$value%' || stream LIKE '%$value%')");
			}

			$rs = $this->db->select('*')->from('students_in_dormitory_view')->where('dorm_id', $dorm_id)->get();
			return $rs->num_rows();
		}

		public function fetch_students_by_dormitory($dorm_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(admission_no LIKE '%$value%' || firstname LIKE '%$value%' || lastname LIKE '%$value%' || class_name LIKE '%$value%' || stream LIKE '%$value%')");
			}

			$rs = $this->db->select('*')->from('students_in_dormitory_view')->where('dorm_id', $dorm_id)->limit($limit, $offset)->order_by('firstname', 'ASC')->get();

			//Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'VIEW STUDENTS IN DORMITORY', 'DORMITORY');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function get_students_not_in_dormitory($dorm_id){
			$this->db->select('*');
			$this->db->from('students_in_dormitory_view');
			$this->db->where('dorm_id !=', $dorm_id);
			$this->db->or_where('dorm_id IS NULL');

			$query = $this->db->get();
			return $query->result_array();
		}


		public function select_accomodation_history($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dorm_name', $value);
				//$this->db->or_like('year', $value);
				$this->db->or_like('accomodation_history.start_date', $value);
				$this->db->or_like('accomodation_history.end_date', $value);
			}


			$this->db->select('student.admission_no, concat(firstname, " ", lastname) AS student_names, dormitory.dorm_id, dormitory.dorm_name, accomodation_history.admission_no, accomodation_history.start_date, accomodation_history.end_date');
			$this->db->from('accomodation_history');
			$this->db->join('dormitory', 'accomodation_history.dorm_id = dormitory.dorm_id');
			$this->db->join('student', 'student.admission_no = accomodation_history.admission_no');		

			$this->db->order_by('student.firstname');
			$this->db->order_by('student.lastname');
			$this->db->order_by('accomodation_history.end_date');			
			$this->db->order_by('dormitory.dorm_id');
			$this->db->limit($limit, $offset);

			$rs = $this->db->get();
			//Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'VIEW ACCOMODATION HISTORY', 'ACCOMODATION HISTORY');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_accomodation_history($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dorm_name', $value);
				//$this->db->or_like('year', $value);
				$this->db->or_like('accomodation_history.start_date', $value);
				$this->db->or_like('accomodation_history.end_date', $value);
			}

			$this->db->select('concat(firstname, " ", lastname) AS student_names, dormitory.dorm_id, dormitory.dorm_name, accomodation_history.admission_no, accomodation_history.start_date, accomodation_history.end_date');
			$this->db->from('accomodation_history');
			$this->db->join('dormitory', 'accomodation_history.dorm_id = dormitory.dorm_id');
			$this->db->join('student', 'student.admission_no = accomodation_history.admission_no');

			$rs = $this->db->get();
			return $rs->num_rows();
		}


		public function select_dorm_masters_history($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dorm_name', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('dorm_master_history.*, dormitory.dorm_name, concat(firstname, " ", lastname) AS teacher_names');
			$this->db->from('dorm_master_history');
			$this->db->join('dormitory', 'dorm_master_history.dorm_id = dormitory.dorm_id');
			$this->db->join('staff', 'staff.staff_id = dorm_master_history.teacher_id');			
			$this->db->order_by('dormitory.dorm_id', 'DESC');
			$this->db->order_by('dorm_master_history.end_date', 'ASC');
			$this->db->limit($limit, $offset);

			$rs = $this->db->get();
			//Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'VIEW DORMITORY MASTER HISTORY', 'DORMITORY');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_dorm_masters_history($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dorm_name', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('dorm_master_history.*, dormitory.teacher_id, dormitory.dorm_name, concat(firstname, " ", lastname) AS teacher_names');
			$this->db->from('dorm_master_history');
			$this->db->join('dormitory', 'dorm_master_history.dorm_id = dormitory.dorm_id');
			$this->db->join('staff', 'staff.staff_id = dorm_master_history.teacher_id');

			$rs = $this->db->get();
			return $rs->num_rows();
		}

		public function end_stay_in_dorm($dorm_id, $record){
			$error = FALSE;
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$level = "O'Level";
			$rs = $this->db->query('SELECT end_date FROM academic_year WHERE status="current_academic_year" AND class_level='.$this->db->escape($level).'');

			if($rs->row_array()['end_date'] === $record['end_date']){	
				$this->db->query('UPDATE accomodation_history SET end_date='.$this->db->escape($record['end_date']).' WHERE dorm_id='.$this->db->escape($dorm_id).' AND admission_no IN(SELECT admission_no FROM student WHERE dorm_id='.$this->db->escape($dorm_id).' AND status="active")');
				$this->db->query('UPDATE student SET dorm_id=NULL WHERE dorm_id='.$this->db->escape($dorm_id).'');
			}
			else{
				$error = TRUE;				
			}

			//Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'END STAY IN DORMITORY', 'ACCOMODATION HISTORY');
		    
		    $this->db->trans_complete();		

		    if($error == TRUE){
				return FALSE;
			}
			else{
				return $this->feedbackMsg();
			}
		}

}

?>