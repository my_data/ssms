<?php
	class Student_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function select_divisions($level){
			if($level === "A'Level"){
				$this->db->where("level = '' || level = 'A'");
			}
			if($level === "O'Level"){
				$this->db->where("level = '' || level = 'O'");
			}

			$this->db->select('*');
			$this->db->from('division');

			$q = $this->db->get();
			return $q->result_array();
		}

		/*public function select_students_by_class_stream(){
			$this->db->select("admission_no,concat(firstname, '	',lastname) AS names, class_stream_id");
			$this->db->from("student");

			$query = $this->db->get();
			return $query->result_array();
		}*/

		public function insert_into_transfer($record, $admission_no, $data){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->insert('transferred_students', $record);
			$this->db->where('admission_no', $admission_no);
			$this->db->update('student', $data);

			$this->activity_log($this->session->userdata('staff_id'), 'TRANSFER STUDENT', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_total_students_by_stream(){
			$this->db->select('*');
			$this->db->from('total_students_by_stream_view');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function count_all_selected_students($form, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || examination_no LIKE '%$value%' || form LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('selected_students.form', $form);
			$this->db->where('academic_year.status', 'current_academic_year');

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function retrieve_all_selected_students($form, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || examination_no LIKE '%$value%' || form LIKE '%$value%')");
	        }

			$this->db->select('selected_students.*, concat(firstname, " ", lastname) AS student_names');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('selected_students.form', $form);
			$this->db->where('academic_year.status', 'current_academic_year');
			//$this->db->where('academic_year.class_level');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();			
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SELECTED STUDENTS');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function count_selected_students($form){
			$this->db->select('reported, COUNT(examination_no) as nos');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('selected_students.form', $form);
			$this->db->where('academic_year.status', 'current_academic_year');
			$this->db->group_by('reported');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_selected_student_by_exam_no($exam_no){
			$this->db->select('*');
			$this->db->from('selected_students');
			$this->db->where('examination_no', $exam_no);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_selected_student($old_exam_no, $selected_student_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('examination_no', $old_exam_no);
			$this->db->update('selected_students', $selected_student_record);
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'SELECTED STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function count_all_completed_students($value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%' || year LIKE '%$value%')");
	        }

	        $this->db->select('*');
			$this->db->from('completed_students_view');
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function retrieve_all_completed_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || waliomaliza_tbl.admission_no LIKE '%$value%' || year LIKE '%$value%')");
	        }

	        //$this->db->select('completed_students_view.*, waliomaliza_tbl.*, concat(firstname, " ", lastname) AS student_names');
			//$this->db->from('completed_students_view');
			//$this->db->join('waliomaliza_tbl', 'waliomaliza_tbl.admission_no = completed_students_view.admission_no');
			$this->db->select('firstname, lastname, concat(firstname, " ", lastname) AS student_names, waliomaliza_tbl.*, year, class_level');
			$this->db->from('student');
			$this->db->join('waliomaliza_tbl', 'waliomaliza_tbl.admission_no = student.admission_no');
			$this->db->join('academic_year', 'waliomaliza_tbl.completion_year = academic_year.id');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'COMPLETED STUDENTS');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function count_all_completed_students_by_year($academic_year, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%' || year LIKE '%$value%')");
	        }

	        $this->db->select('*');
			//$this->db->from('completed_students_view');
			$this->db->from('student');
			$this->db->join('waliomaliza_tbl', 'waliomaliza_tbl.admission_no = student.admission_no');
			$this->db->join('academic_year', 'waliomaliza_tbl.completion_year = academic_year.id');
			$this->db->where('year', $academic_year);

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function retrieve_all_completed_students_by_year($academic_year, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || waliomaliza_tbl.admission_no LIKE '%$value%' || year LIKE '%$value%')");
	        }

	        $this->db->select('completed_students_view.*, waliomaliza_tbl.*, concat(firstname, " ", lastname) AS student_names');
			$this->db->from('completed_students_view');
			$this->db->join('waliomaliza_tbl', 'waliomaliza_tbl.admission_no = completed_students_view.admission_no');
			$this->db->where('year', $academic_year);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'SEARCH', 'COMPLETED STUDENTS');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function update_completed_students($record, $admission_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('admission_no', $admission_no);
			$this->db->update('waliomaliza_tbl', $record);
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'COMPLETED STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function count_all_unreported_students($value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || examination_no LIKE '%$value%' || form LIKE '%$value%')");
	        }

			$this->db->select('*');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('academic_year.status', 'current_academic_year');
			$this->db->where('reported', 'no');

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function retrieve_all_unreported_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || examination_no LIKE '%$value%' || form LIKE '%$value%')");
	        }

			$this->db->select('selected_students.*, concat(firstname, " ", lastname) AS student_names');
			$this->db->from('selected_students');
			$this->db->join('academic_year', 'academic_year.id = selected_students.academic_year');
			$this->db->where('academic_year.status', 'current_academic_year');
			$this->db->where('reported', 'no');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'UNREPORTED STUDENTS');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function select_students_by_stte($stte, $limit, $offset, $value){
			if($stte === "IN"){
				//$query = $this->db->get('student_transfer_in_view');
				if(!empty($value)){
					$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%' || class_name LIKE '%$value%')");
		        }

		        $this->db->select('*');
		        $this->db->from('student_transfer_in_view');
		        $this->db->order_by('admission_no','ASC');
		        $this->db->limit($limit, $offset);

		        $rs = $this->db->get();
		        return $rs->result_array();
			}
			if($stte === "OUT"){
				//$query = $this->db->get('student_transfer_out_view');
				if(!empty($value)){
					$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%' || class_name LIKE '%$value%')");
		        }

		        $this->db->select('*');
		        $this->db->from('student_transfer_out_view');
		        $this->db->order_by('admission_no','ASC');
		        $this->db->limit($limit, $offset);

		        $rs = $this->db->get();
		        return $rs->result_array();
			}

			//return $query->result_array();
		}


		public function select_expelled_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('expelled_students_view');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'EXPELLED STUDENTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_expelled_students($stte, $value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('expelled_students_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function select_suspended_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('suspended_students_view');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SUSPENDED STUDENTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_suspended_students($stte, $value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('suspended_students_view');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}


		public function count_all_expelled_students($stte){
			$q = $this->db->get_where('expelled_students_view', array('stte_out' => $stte));
			return $q->num_rows();
		}

		public function delete_student($admission_no, $data, $suspension_reason_record, $discipline_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = '.$this->db->escape($this->session->userdata('staff_id')).'');
			$this->db->query('SET @ip_address = '.$this->db->escape($_SERVER["REMOTE_ADDR"]).'');

			$this->db->where('admission_no', $admission_no);
			$this->db->update('student', $data);
			$this->db->insert('suspended_students', $suspension_reason_record);
			$this->db->insert('discipline', $discipline_record);
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'SUSPEND STUDENT', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function restore_student($admission_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE student SET status="active", stte_out="" WHERE admission_no="'.$admission_no.'"');
			$this->db->query('DELETE FROM transferred_students WHERE admission_no="'.$admission_no.'" AND transfer_status="OUT"');
			$this->db->query('DELETE FROM suspended_students WHERE admission_no="'.$admission_no.'"');
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'RESTORE SUSPENDED STUDENT', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_disability($disability_record, $admission_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('student', $disability_record, array('admission_no' => $admission_no), 'UPDATE');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE STUDENT DISABILITY', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_student_info($student_record, $guardian_record, $admission_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

				if($this->input->post('transferred') === 'transferred_in'){
					$self_form_receipt = "Received";
					$transfer_status = "IN";
					$transfer_letter = "Available";

					$transferred_record = array(
						'admission_no' => $admission_no,
						'school' => $student_record['former_school'],
						'date_of_transfer' => $this->input->post('admission_date'),
						'transfer_status' => $transfer_status,
						'transfer_letter' => $transfer_letter,
						'self_form_receipt' => $self_form_receipt,
						'form' => $this->input->post('class_id')
					);

					$this->db->where('admission_no', $admission_no);
					$this->db->delete('transferred_students');
					$this->db->insert('transferred_students', $transferred_record);
				}

				if($this->input->post('transferred') === 'selected'){
					$this->db->where('admission_no', $admission_no);
					$this->db->delete('transferred_students');
				}


			$this->db->where('admission_no', $admission_no);
			$this->db->update('student', $student_record);
			$this->CI_mySQL('guardian', $guardian_record, array('admission_no' => $admission_no), 'UPDATE');
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_student_by_id($admission_no){
			$this->db->select('*');
			$this->db->from('student_guardian_view');
			$this->db->where('admission_no', $admission_no);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function insert_selected_students($selected_student_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->insert('selected_students', $selected_student_record);
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'ADD', 'SELECTED STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function upload_selected_students($selected_student_record){
			return $this->db->insert_batch('selected_students', $selected_student_record);
		}

		public function upload_students($target_dir){
			return $this->db->query('LOAD DATA INFILE "'.$target_dir.'" INTO TABLE selected_students FIELDS TERMINATED BY "," LINES TERMINATED BY "\n"');
		}

		public function count_students_by_class_and_subject($class_stream_id, $subject_id, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select("*");
			$this->db->from('students_by_class_and_subject_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('subject_id', $subject_id);

			$rs = $this->db->get();
			return $rs->num_rows();
		}

		//Used when entering score
		public function select_students_by_class_and_subject($class_stream_id, $subject_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

			$this->db->select("*");
			$this->db->from('students_by_class_and_subject_view');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('subject_id', $subject_id);
			$this->db->limit($limit, $offset);
			//$this->db->where('add_score_enabled', '1');

			$rs = $this->db->get();
			return $rs->result_array();
		}

		//Matokeo somo moja kwa stream
		public function select_results_by_class_and_stream($class_stream_id, $subject_id, $term_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student.admission_no LIKE '%$value%')");
	        }

			$this->db->select('class_level.class_id, student.admission_no,concat(firstname, " ",lastname) AS names, class_stream.class_stream_id, subject.subject_id, subject_name, class_name, stream, exam_type.id, exam_name, start_date, end_date, marks, exam_type.add_score_enabled, ssa_id, student_subject_assessment.term_id');
			$this->db->from('student_subject_assessment');
			$this->db->join('subject', 'student_subject_assessment.subject_id = subject.subject_id');
			$this->db->join('student', 'student_subject_assessment.admission_no = student.admission_no');
			$this->db->join('class_stream', 'student_subject_assessment.class_stream_id = class_stream.class_stream_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->join('exam_type', 'exam_type.id = student_subject_assessment.etype_id');
			$this->db->where('student_subject_assessment.class_stream_id', $class_stream_id);
			$this->db->where('student_subject_assessment.subject_id', $subject_id);
			$this->db->where('student_subject_assessment.term_id', $term_id);
			$this->db->where('add_score_enabled', '1');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function update_student_results($ssa_id,  $ssa_record, $subject_id, $term_id, $csid, $cid){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('student_subject_assessment', $ssa_record, array('ssa_id' => $ssa_id), 'UPDATE');
			//$this->db->query('CALL calculate_subject_rank_procedure('.$this->db->escape($subject_id).', '.$this->db->escape($term_id).','.$this->db->escape($csid).','.$this->db->escape($cid).')');
			$this->CI_getProcedureError();

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE SCORE', 'STUDENT_SUBJECT_ASSESSMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function get_enabled_exam_type(){
			$this->db->select('*');
			$this->db->from('exam_type');
			$this->db->where('add_score_enabled', '1');
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function insert_score($score_record, $class_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('student_subject_assessment', $score_record, NULL, 'INSERT_BATCH');
			//IN CASE nikishangaa why nimeitia huku procedure..!!! NOTE: Procedure imegoma ku_capture subject_rank on first insert ndo maana ipo huku
			//$this->db->query('CALL calculate_subject_rank_procedure('.$this->db->escape($score_record[0]['subject_id']).', '.$this->db->escape($score_record[0]['term_id']).', '.$this->db->escape($score_record[0]['class_stream_id']).','.$this->db->escape($class_id).')');
			//$this->CI_getProcedureError();

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD SCORE', 'STUDENT_SUBJECT_ASSESSMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function validate_date_of_adding_score($date){
			$date = $this->db->escape($date);
			$rs = $this->db->query('SELECT * FROM exam_type WHERE start_date <= '.$date.' AND end_date >= '.$date.' AND add_score_enabled LIMIT 1');
			return $rs->num_rows();
		}

		public function select_students_waliofanya_mtihani($csid){
			$this->db->select('*');
			$this->db->from('student_assessment');
			$this->db->where('class_stream_id', $csid);

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_students_by_class(/*$id_display = FALSE*/$class_stream_id = FALSE){
			$this->db->select("*");
			$this->db->from("active_students_view");

			if(/*$id_display === FALSE*/ $class_stream_id === FALSE){
				$query = $this->db->get();
				return $query->result_array();
			}

			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('status', 'current_academic_year');
			//$this->db->where('id_display', $id_display);
			$query = $this->db->get();
			return $query->result_array();
		}

		//HAHHAHAHA

		public function count_all_disabled_students($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
				$this->db->or_like('admission_no', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('disability', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('disabled_students');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function retrieve_all_disabled_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
				$this->db->or_like('admission_no', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('disability', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('disabled_students');
	        $this->db->where('statasi', 'active');
	        $this->db->order_by('firstname','ASC');
	        $this->db->order_by('lastname','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DISABLED STUDENTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_students_per_stream($class_stream_id = FALSE, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_students_by_stream($class_stream_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'CLASS STREAM STUDENTS');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_students_per_stream_for_results($class_stream_id = FALSE, $term_id = FALSE, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->where('id=(SELECT aid FROM term where term_id='.$term_id.')');
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_students_by_stream_for_results($class_stream_id, $term_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('active_students_view');
	        $this->db->where('class_stream_id', $class_stream_id);
	        $this->db->where('active_students_view.id=(SELECT aid FROM term where term_id='.$term_id.')');
	        $this->db->where('admission_no IN(SELECT admission_no FROM student_subject_score_position WHERE  term_id='.$term_id.')');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        return $rs->result_array();
		}

		//HAHHAHAH

		public function select_students_by_dormitory($dorm_id){//Used load_roll_call
			$this->db->select("student.admission_no, concat(firstname, '	',lastname) AS names, class_stream_id");
			$this->db->from("student");
			$this->db->where('dorm_id', $dorm_id);
			$this->db->where('(stte_out != "completed" || stte_out IS NULL || stte_out = "")');

			$query = $this->db->get();
			return $query->result_array();
		}


		public function select_all_students_by_class_and_term($class_id, $term_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student.admission_no LIKE '%$value%')");
	        }

			$this->db->select("((SELECT ROUND(AVG(average), 2) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.")) AS average, (SELECT ROUND(SUM(average), 2) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.") AS total, (SELECT COUNT(*) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.") AS number_of_subjects, student.admission_no, firstname, lastname, concat(firstname, '	',lastname) AS names, (SELECT class_name FROM class_level WHERE class_id='".$class_id."') AS class_name, student_subject_score_position.class_id, student_subject_score_position.class_stream_id");
			$this->db->from("student");
			$this->db->join("student_subject_score_position", "student_subject_score_position.admission_no = student.admission_no");
			$this->db->where("student_subject_score_position.class_id", $class_id);
			$this->db->where("student_subject_score_position.term_id", $term_id);
			$this->db->group_by('admission_no');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			return $query->result_array();
		}

		//Iliwahi kutumika kwenye mkeka wa academic kabla ya kuleta magumashi
		/*public function select_all_students_by_class_and_term($class_id, $term_id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || student.admission_no LIKE '%$value%')");
	        }

			$this->db->select("((SELECT ROUND(AVG(average), 2) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.")) AS average, (SELECT ROUND(SUM(average), 2) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.") AS total, (SELECT COUNT(*) FROM student_subject_score_position WHERE admission_no=student.admission_no AND class_id='".$class_id."' AND term_id=".$term_id.") AS number_of_subjects, student.admission_no, firstname, lastname, concat(firstname, '	',lastname) AS names, class_name, stream, student.class_id, student.class_stream_id");
			$this->db->from("student");
			$this->db->join("class_level", "student.class_id = class_level.class_id");
			$this->db->join("class_stream", "student.class_stream_id = class_stream.class_stream_id", "left");
			$this->db->where("student.class_id", $class_id);
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			return $query->result_array();
		}*/

		public function select_all_students($class_id = FALSE){
			$this->db->select("admission_no, firstname, lastname, concat(firstname, '	',lastname) AS names, class_name, stream, class_id");
			$this->db->from("active_students_view");
			//$this->db->from("student");
			//$this->db->join("class_level", "student.class_id = class_level.class_id");
			//$this->db->join("class_stream", "student.class_stream_id = class_stream.class_stream_id", "left");

			if($class_id === FALSE){
				$query = $this->db->get();
				return $query->result_array();
			}

			//$this->db->where("student.class_id", $class_id);
			$this->db->where("class_id", $class_id);
			$query = $this->db->get();
			return $query->result_array();
		}


		//ENROLLMENT
		public function select_students_enrollment($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('stream', $value);
				$this->db->or_like('year', $value);
			}

			$this->db->select('enrollment.*, concat(student.firstname, " ", student.lastname) AS student_names, concat(class_name, " ", stream) AS class_name, year');
			$this->db->from('enrollment');
			$this->db->join('student', 'student.admission_no = enrollment.admission_no');
			$this->db->join('class_level', 'enrollment.class_id = class_level.class_id');
			$this->db->join('class_stream', 'enrollment.class_stream_id = class_stream.class_stream_id');
			$this->db->join('academic_year', 'enrollment.academic_year = academic_year.id');				
			$this->db->order_by('enrollment.admission_no');
			$this->db->limit($limit, $offset);

			$rs = $this->db->get();	        
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'ENROLLMENT');
			$this->db->trans_complete();

	        return $rs->result_array();
		}

		public function count_students_enrollment($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('class_name', $value);
				$this->db->or_like('stream', $value);
				$this->db->or_like('year', $value);
			}

			$this->db->select('enrollment.*, concat(student.firstname, " ", student.lastname) AS student_names, concat(class_name, " ", stream) AS class_name, year');
			$this->db->from('enrollment');
			$this->db->join('student', 'student.admission_no = enrollment.admission_no');
			$this->db->join('class_level', 'enrollment.class_id = class_level.class_id');
			$this->db->join('class_stream', 'enrollment.class_stream_id = class_stream.class_stream_id');
			$this->db->join('academic_year', 'enrollment.academic_year = academic_year.id');

			$rs = $this->db->get();
			return $rs->num_rows();
		}
		//END ENROLLMENT

		public function insert_discipline_record($discipline_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('discipline', $discipline_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD', 'DISCIPLINE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function get_indiscipline_record($id){
			$this->db->select('*');
			$this->db->from('discipline');
			$this->db->where('id', $id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_indiscipline_record($id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			/*$this->db->where('class_stream_id', $class_stream_id);
			return $this->db->update('class_stream', $record);*/
			$this->CI_mySQL('discipline', $record, array('id' => $id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UUPDATE', 'DISCIPLINE');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		//Get the values of academic the current academic year
		public function select_current_academic_year(){
			$status = 'current_academic_year';	//The index in table academic_year
			$this->db->select('*');
			$this->db->from('academic_year');
			$this->db->where('status', $status);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_academic_year_by_class_id($class_id){
			$query = $this->db->query('SELECT * FROM academic_year WHERE class_level=(SELECT level FROM class_level WHERE class_id="'.$class_id.'") AND status="current_academic_year";');
			return $query->row_array();
		}

		//The function is used in students to get the id of current academic year for registration of student
		public function select_current_academic_year_by_level($level){
			$status = 'current_academic_year';	
			$this->db->select('*');
			$this->db->from('academic_year');
			$this->db->where('status', $status);
			$this->db->where('class_level', $level);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		} 

		public function select_tribes(){
			$this->db->select('*');
			$this->db->from('tribe');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_tribeByName($tribe_name){
			$this->db->select('*');
			$this->db->from('tribe');
			$this->db->where('tribe_name',$tribe_name);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->result_array();
			}
			else{
				return false;
			}
		}

		public function select_regions(){
			$this->db->select('*');
			$this->db->from('region');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_months(){
			$this->db->select('*');
			$this->db->from('month');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_religions(){
			$this->db->select('*');
			$this->db->from('religion');
			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_religionByName($religion_name){
			$this->db->select('*');
			$this->db->from('religion');
			$this->db->where('religion_name',$religion_name);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->result_array();
			}
			else{
				return false;
			}
		}

		public function select_classes(){
			$this->db->select('*');
			$this->db->from('class_level');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_streams(){
			$this->db->select('*');
			$this->db->from('stream');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_class_streams(){
			$this->db->select('class_stream.*, class_name, level');
			$this->db->from('class_stream');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_dormitories(){
			$this->db->select('*');
			$this->db->from('dormitory');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function insert_student_record($student_record, $guardian_record, $admission_no){
			$admission_no_ = $this->db->escape($admission_no);
			
			$this->db->trans_start();
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');

			$errorCode = $this->CI_registerStudent('student', $student_record, NULL, 'INSERT');
			$errorMessage = $this->db->error()['message'];

			$errorCodeGuardian = $this->CI_registerStudent('guardian', $guardian_record, NULL, 'INSERT');
			
			if($this->input->post('transferred') === 'transferred_in'){
				$self_form_receipt = "Received";
				$transfer_status = "IN";
				$transfer_letter = "Available";

				$transferred_record = array(
					'admission_no' => $admission_no,
					'school' => $student_record['former_school'],
					'date_of_transfer' => $this->input->post('admission_date'),
					'transfer_status' => $transfer_status,
					'transfer_letter' => $transfer_letter,
					'self_form_receipt' => $self_form_receipt,
					'form' => $student_record['class_id']
				);

				$this->db->where('admission_no', $admission_no);
				$this->db->delete('transferred_students');
				$errorCodeTransferred = $this->CI_registerStudent('transferred_students', $transferred_record, NULL, 'INSERT');
			}
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REGISTER STUDENT', 'STUDENT');
			$this->db->trans_complete();
			//return $this->feedbackMsg();
			if($errorCode == 0 && $errorCodeGuardian == 0 && $errorCodeTransferred == 0){
				$this->session->set_flashdata('success_message', "Student has been successfully registered");
			}
			else{
				$this->session->set_flashdata('error_message', $errorMessage);
			}
		}

		public function update_class_stream(/*$data*/$admission_no, $subject_id, $class_stream_id, $std_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE enrollment SET class_stream_id="'.$class_stream_id.'" WHERE admission_no="'.$admission_no.'" AND class_id=(SELECT class_id FROM class_stream WHERE class_stream_id='.$this->db->escape($class_stream_id).')');
			$this->db->where('admission_no', $admission_no);
			$this->db->update('student', $std_record);
			$this->db->query('CALL insert_into_student_subject_after_updating_stream_procedure("'.$admission_no.'", "'.$subject_id.'","'.$class_stream_id.'")');
			$this->CI_getProcedureError();

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ASSIGN OPTIONAL SUBJECT', 'STUDENT_SUBJECT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
			//$this->db->insert('boshen', $data);
			/*$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->CI_mySQL('subject', $subject_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'subject');
			$this->db->trans_complete();*/
		}


		public function shift_student_to_cs($admission_no, $class_stream_id, $aid, $std_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE enrollment SET class_stream_id='.$this->db->escape($class_stream_id).' WHERE admission_no='.$this->db->escape($admission_no).' AND academic_year='.$this->db->escape($aid).'');
			$this->db->where('admission_no', $admission_no);
			$this->db->update('student', $std_record);
			$this->db->query('CALL after_shifting_class_stream_procedure("'.$admission_no.'", "'.$class_stream_id.'")');
			$this->CI_getProcedureError();

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SHIFT STUDENT TO CLASS', 'STUDENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		/*public function insert_promoted_audit_record($data){
			return $this->db->insert('promote_student_audit_table', $data);
		}*/

		public function select_student_record($admission_no){
			$this->db->select('*');
			$this->db->from('student_guardian_view');
			$this->db->where('admission_no', $admission_no);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_student_discipline_rec($admission_no){
			$this->db->select('*');
			$this->db->from('discipline');
			$this->db->where('admission_no', $admission_no);

			$query = $this->db->get();       
			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT"S DISCIPLINE');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		public function get_students_not_in_class_stream($class_stream_id, $class_id){
			$this->db->select('*');
			$this->db->from('active_students_view');
			$this->db->where('class_stream_id !=', $class_stream_id);
			$this->db->where('class_id', $class_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		/*public function select_student_attendance_record($admission_no){
			$query = $this->db->query('SELECT t2.admission_no, t2.student_names, MAX(Absent) AS "Absent", MAX(Present) AS "Present", MAX(Sick) AS "Sick", MAX(Permitted) AS "Permitted", MAX(Home) AS "Home" FROM ( SELECT t1.admission_no, t1.att_type, t1.description, t1.aggregate, t1.student_names, CASE WHEN t1.attendance_type="Absent" THEN t1.aggregate END AS "Absent", CASE WHEN t1.attendance_type="Present" THEN t1.aggregate END AS "Present", CASE WHEN t1.attendance_type="Sick" THEN t1.aggregate END AS "Sick", CASE WHEN t1.attendance_type="Permitted" THEN t1.aggregate END AS "Permitted", CASE WHEN t1.attendance_type="Home" THEN t1.aggregate END AS "Home" FROM ( SELECT t.admission_no, t.att_type, t.description, t.aggregate, t.student_names, CASE WHEN t.att_type="A" THEN "Absent" WHEN t.att_type="P" THEN "Present" WHEN t.att_type="S" THEN "Sick" WHEN t.att_type="T" THEN "Permitted" WHEN t.att_type="H" THEN "Home" ELSE NULL END AS attendance_type FROM ( select student.admission_no, concat(firstname, " ", lastname) AS student_names, att_type, description, count(att_id) AS aggregate from student_attendance_record join student using(admission_no) join attendance using(att_id) where student.admission_no="'.$admission_no.'" group by att_id ) t ) t1 ) t2');

			return $query->result_array();
		}*/

		public function select_student_attendance_record($admission_no){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$query = $this->db->query('SELECT t2.term_id, t2.term_name, t2.year, t2.admission_no, t2.student_names, MAX(Absent) AS "Absent", MAX(Present) AS "Present", MAX(Sick) AS "Sick", MAX(Permitted) AS "Permitted", MAX(Home) AS "Home" FROM ( SELECT t1.term_id, t1.term_name, t1.year, t1.admission_no, t1.att_type, t1.description, t1.aggregate, t1.student_names, CASE WHEN t1.attendance_type="Absent" THEN t1.aggregate END AS "Absent", CASE WHEN t1.attendance_type="Present" THEN t1.aggregate END AS "Present", CASE WHEN t1.attendance_type="Sick" THEN t1.aggregate END AS "Sick", CASE WHEN t1.attendance_type="Permitted" THEN t1.aggregate END AS "Permitted", CASE WHEN t1.attendance_type="Home" THEN t1.aggregate END AS "Home" FROM ( SELECT t.term_id, t.term_name, t.year, t.admission_no, t.att_type, t.description, t.aggregate, t.student_names, CASE WHEN t.att_type="A" THEN "Absent" WHEN t.att_type="P" THEN "Present" WHEN t.att_type="S" THEN "Sick" WHEN t.att_type="T" THEN "Permitted" WHEN t.att_type="H" THEN "Home" ELSE NULL END AS attendance_type FROM ( select term_name, student_attendance_record.term_id, academic_year.year, student.admission_no, concat(firstname, " ", lastname) AS student_names, att_type, description, count(att_id) AS aggregate from student_attendance_record join student using(admission_no) join attendance using(att_id) join term on(student_attendance_record.term_id = term.term_id) join academic_year on(academic_year.id = term.aid) where student.admission_no='.$this->db->escape($admission_no).' group by att_id, term_id ) t ) t1 ) t2 group by t2.term_id');

			//Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENT"S ATTENDANCE');
			$this->db->trans_complete();

	        return $query->result_array();
		}

		//Discipline
		public function count_discipline_records($value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('discipline.admission_no', $value);
				$this->db->or_like('description', $value);
				$this->db->or_like('decision', $value);
				$this->db->or_like('discipline.date', $value);
	        }

			$this->db->select('*');
			$this->db->from('discipline');
			$this->db->join('student', 'student.admission_no = discipline.admission_no');

			$query = $this->db->get();
			return $query->num_rows();
		}

		public function select_discipline_records($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('discipline.admission_no', $value);
				$this->db->or_like('description', $value);
				$this->db->or_like('decision', $value);
				$this->db->or_like('discipline.date', $value);
	        }

			$this->db->select('*');
			$this->db->from('discipline');
			$this->db->join('student', 'student.admission_no = discipline.admission_no');
			$this->db->order_by('discipline.date', 'DESC');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'INDISCIPLINE RECORDS');
			$this->db->trans_complete();
			return $query->result_array();
		}

		public function select_student_enrollment_history($admission_no){
			$this->db->select('*');
			$this->db->from('student_enrollment_academic_year_view');
			$this->db->where('admission_no', $admission_no);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function new_generate_mkeka($csid, $sub_id, $term_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			/*if(!empty($value)){
				$this->db->like('student_subject_score_position.admission_no');
				$this->db->or_like('firstname');
				$this->db->or_like('lastname');
			}*/

			if(!empty($value)){
				$this->db->where("(student_subject_score_position.admission_no LIKE '%$value%' || lastname LIKE '%$value%' || firstname LIKE '%$value%')");
			}

			$this->db->select('concat(student.firstname, " ", student.lastname) AS names, student_subject_score_position.*');
			$this->db->from('student');
			$this->db->join('student_subject_score_position', 'student.admission_no = student_subject_score_position.admission_no');
			$this->db->where('student_subject_score_position.class_stream_id', $csid);
			$this->db->where('student_subject_score_position.subject_id', $sub_id);
			$this->db->where('student_subject_score_position.term_id', $term_id);
			//$this->db->order_by('firstname');
			//$this->db->order_by('lastname');
			$this->db->order_by('subject_rank', 'ASC');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
			//activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SUBJECT RESULTS');
			$this->db->trans_complete();
			return $query->result_array();
		}

		//GENERATE MKEKA PER CLASS STREAM
		public function generate_mkeka_wa_darasa($class_stream_id, $subject_id, $term_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$mkeka_query = $this->db->query('SELECT t2.admission_no, t2.firstname, t2.lastname, t2.class_id, t2.class_stream_id, t2.subject_id, t2.names, t2.etype_id, t2.marks,
					MAX(ES1) AS "monthly_one", MAX(ES2) AS "midterm", MAX(ES3) AS "monthly_two", MAX(ES4) AS "terminal",
					(
					SELECT ROUND(((SELECT marks FROM student_subject_assessment WHERE admission_no=t2.admission_no AND subject_id="'.$subject_id.'" AND etype_id="E04" AND term_id='.$term_id.') + sum(marks)/count(etype_id))/2, 2) AS test_average from student_subject_assessment where admission_no=t2.admission_no AND subject_id="'.$subject_id.'" AND etype_id IN("E01", "E02", "E03") AND term_id='.$term_id.'
					) AS average
					FROM
					(
					SELECT t1.admission_no, t1.firstname, t1.lastname, t1.class_id, t1.class_stream_id, t1.subject_id, t1.names, t1.etype_id, t1.marks,
					CASE WHEN t1.subject_name="ES1" THEN t1.marks END AS "ES1", 
					CASE WHEN t1.subject_name="ES2" THEN t1.marks END AS "ES2", 
					CASE WHEN t1.subject_name="ES3" THEN t1.marks END AS "ES3",
					CASE WHEN t1.subject_name="ES4" THEN t1.marks END AS "ES4"
					FROM 
					(
					SELECT t.subject_id, t.names, t.firstname, t.lastname, t.etype_id, t.admission_no, t.marks, t.class_id, t.class_stream_id,
					CASE WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E01") THEN "ES1"
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E02") THEN "ES2" 
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E03") THEN "ES3"
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E04") THEN "ES4"
					ELSE NULL END AS subject_name
					FROM
					(
					SELECT student.admission_no,concat(firstname, " ",lastname) AS names, firstname, lastname, class_stream.class_stream_id, subject.subject_id, subject_name, class_level.class_id, class_name, stream, exam_type.id, exam_name, start_date, end_date, marks, etype_id FROM student_subject_assessment JOIN subject ON (student_subject_assessment.subject_id = subject.subject_id) JOIN student ON (student_subject_assessment.admission_no = student.admission_no) JOIN class_stream ON(student_subject_assessment.class_stream_id = class_stream.class_stream_id) JOIN class_level ON(class_level.class_id = class_stream.class_id) JOIN exam_type ON(exam_type.id = student_subject_assessment.etype_id) WHERE student_subject_assessment.class_stream_id="'.$class_stream_id.'" AND student_subject_assessment.subject_id="'.$subject_id.'" AND student_subject_assessment.term_id='.$term_id.' AND (student.admission_no LIKE "%'.$value.'%" || firstname LIKE "%'.$value.'%" || lastname LIKE "%'.$value.'%")
					) t
					) t1
					) t2
					GROUP BY t2.admission_no ORDER BY average DESC LIMIT '.$limit.' OFFSET '.$offset.'');
				
				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SUBJECT RESULTS');
				$this->db->trans_complete();
				return $mkeka_query->result_array();
			}
			else{
				$mkeka_query = $this->db->query('SELECT t2.admission_no, t2.firstname, t2.lastname, t2.class_id, t2.class_stream_id, t2.subject_id, t2.names, t2.etype_id, t2.marks,
					MAX(ES1) AS "monthly_one", MAX(ES2) AS "midterm", MAX(ES3) AS "monthly_two", MAX(ES4) AS "terminal",
					(
					SELECT ROUND(((SELECT marks FROM student_subject_assessment WHERE admission_no=t2.admission_no AND subject_id="'.$subject_id.'" AND etype_id="E04" AND term_id='.$term_id.') + sum(marks)/count(etype_id))/2, 2) AS test_average from student_subject_assessment where admission_no=t2.admission_no AND subject_id="'.$subject_id.'" AND etype_id IN("E01", "E02", "E03") AND term_id='.$term_id.'
					) AS average
					FROM
					(
					SELECT t1.admission_no, t1.firstname, t1.lastname, t1.class_id, t1.class_stream_id, t1.subject_id, t1.names, t1.etype_id, t1.marks,
					CASE WHEN t1.subject_name="ES1" THEN t1.marks END AS "ES1", 
					CASE WHEN t1.subject_name="ES2" THEN t1.marks END AS "ES2", 
					CASE WHEN t1.subject_name="ES3" THEN t1.marks END AS "ES3",
					CASE WHEN t1.subject_name="ES4" THEN t1.marks END AS "ES4"
					FROM 
					(
					SELECT t.subject_id, t.names, t.firstname, t.lastname, t.etype_id, t.admission_no, t.marks, t.class_id, t.class_stream_id,
					CASE WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E01") THEN "ES1"
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E02") THEN "ES2" 
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E03") THEN "ES3"
					WHEN (t.subject_id = "'.$subject_id.'" && t.etype_id="E04") THEN "ES4"
					ELSE NULL END AS subject_name
					FROM
					(
					SELECT student.admission_no,concat(firstname, " ",lastname) AS names, firstname, lastname, class_stream.class_stream_id, subject.subject_id, subject_name, class_level.class_id, class_name, stream, exam_type.id, exam_name, start_date, end_date, marks, etype_id FROM student_subject_assessment JOIN subject ON (student_subject_assessment.subject_id = subject.subject_id) JOIN student ON (student_subject_assessment.admission_no = student.admission_no) JOIN class_stream ON(student_subject_assessment.class_stream_id = class_stream.class_stream_id) JOIN class_level ON(class_level.class_id = class_stream.class_id) JOIN exam_type ON(exam_type.id = student_subject_assessment.etype_id) WHERE student_subject_assessment.class_stream_id="'.$class_stream_id.'" AND student_subject_assessment.subject_id="'.$subject_id.'" AND student_subject_assessment.term_id='.$term_id.'
					) t
					) t1
					) t2
					GROUP BY t2.admission_no ORDER BY average DESC LIMIT '.$limit.' OFFSET '.$offset.'');

				//Activity Log
				$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'SUBJECT RESULTS');
				$this->db->trans_complete();
				return $mkeka_query->result_array();
			}
		}

		public function get_grade_aggregate_single_subject($class_stream_id, $subject_id, $term_id){
			$this->db->select('grade, COUNT(grade) AS count');
			$this->db->from('student_subject_score_position');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('subject_id', $subject_id);
			$this->db->where('term_id', $term_id);
			$this->db->group_by('grade');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_grade_aggregate($class_stream_id, $term_id){
			$this->db->select('grade, COUNT(*) AS count');
			$this->db->from('student_assessment');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->where('term_id', $term_id);
			$this->db->group_by('grade');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_o_level_grade_system(){
			$this->db->select('*');
			$this->db->from('o_level_grade_system_tbl');
			$this->db->order_by('grade', 'ASC');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_a_level_grade_system(){
			$this->db->select('*');
			$this->db->from('a_level_grade_system_tbl');
			$this->db->order_by('grade', 'ASC');

			$query = $this->db->get();
			return $query->result_array();
		}


		//Kwa ajili ya position
		public function insert_score_position($data, $class_stream_id, $subject_id, $term_id){
			$this->db->trans_start();
			$this->db->query('DELETE FROM student_subject_score_position WHERE class_stream_id="'.$class_stream_id.'" AND subject_id="'.$subject_id.'" AND term_id='.$term_id.'');
			$this->db->insert_batch('student_subject_score_position', $data);
			$this->db->trans_complete();
		}

		public function select_student_score_position($class_stream_id, $subject_id, $term_id){
			$query = $this->db->query('select student_subject_score_position.*, concat(student.firstname, " ", student.lastname) AS names, find_in_set(average, (select group_concat(average order by average desc) from student_subject_score_position WHERE class_stream_id="'.$class_stream_id.'" AND subject_id="'.$subject_id.'" AND term_id='.$term_id.')) AS rank from student_subject_score_position JOIN student USING(admission_no) WHERE student_subject_score_position.class_stream_id="'.$class_stream_id.'" AND subject_id="'.$subject_id.'" AND term_id='.$term_id.' ORDER BY rank ASC');

			return $query->result_array();
		}

		public function select_single_student_result($admission_no, $term_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('student_result_view');
			$this->db->where('admission_no', $admission_no);
			$this->db->where('term_id', $term_id);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'REPORT OF STUDENT"S RESULTS');
			$this->db->trans_complete();
			return $query->result_array();
		}

		public function select_etypes_by_id($admission_no, $term_id){
			$this->db->select('COUNT(DISTINCT(etype_id)) AS no_etypes');
			$this->db->from('student_subject_assessment');
			$this->db->where('admission_no', $admission_no);
			$this->db->where('term_id', $term_id);
			$this->db->where('etype_id != "E04"');

			$query = $this->db->get();
			return $query->row_array();
		}
		

		public function select_single_student_position($admission_no){
			$query = $this->db->query('SELECT student_assessment.*, class_name, stream, student.academic_year, concat(student.firstname, " ", student.lastname) AS names, find_in_set(average, (SELECT group_concat(average order by average desc) from student_assessment WHERE class_id=(SELECT class_id FROM student WHERE admission_no="'.$admission_no.'" LIMIT 1))) AS rank from student_assessment JOIN student USING(admission_no) JOIN class_level ON(student_assessment.class_id = class_level.class_id) JOIN class_stream ON(student_assessment.class_stream_id = class_stream.class_stream_id) WHERE student_assessment.class_id=(SELECT class_id FROM student WHERE admission_no="'.$admission_no.'" LIMIT 1) AND student_assessment.admission_no="'.$admission_no.'" LIMIT 1');

			return $query->row_array();
		}

		public function select_student_position($admission_no, $class_id, $term_id){
			$query = $this->db->query('SELECT student_assessment.*, class_name, stream, student.academic_year, concat(student.firstname, " ", student.lastname) AS names, find_in_set(average, (SELECT group_concat(average order by average desc) from student_assessment WHERE class_id="'.$class_id.'" AND term_id='.$term_id.')) AS rank from student_assessment JOIN student ON(student_assessment.admission_no = student.admission_no) JOIN class_level ON(student_assessment.class_id = class_level.class_id) JOIN class_stream ON(student_assessment.class_stream_id = class_stream.class_stream_id) WHERE student_assessment.class_id="'.$class_id.'" AND term_id='.$term_id.' AND student_assessment.admission_no="'.$admission_no.'" LIMIT 1');

			return $query->row_array();
		}

		public function select_active_students($class_stream_id = FALSE){
			$this->db->select('*');
			$this->db->from('active_students_view');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function update_class_monitor($class_stream_id, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			/*$this->db->where('class_stream_id', $class_stream_id);
			return $this->db->update('class_stream', $record);*/
			$this->CI_mySQL('class_stream', $record, array('class_stream_id' => $class_stream_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD', 'CLASS MONITOR');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_class_monitor($class_stream_id){
			$this->db->select('*');
			$this->db->from('class_stream');
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}
		
		public function get_all_transferred_in_students_data($stte, $value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
			}

			$this->db->select('*');
			$this->db->from('student_transfer_in_view');
			$this->db->where('stte', $stte);

			$rs = $this->db->get();
			return $rs->num_rows();
		}
		
		public function get_all_transferred_students_data($stte, $value){
			if(!empty($value)){
	            $this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('admission_no', $value);
	        }

	        $this->db->select('*');
	        $this->db->from('student_transfer_out_view');
	        $this->db->where('stte_out', $stte);
	        $rs = $this->db->get();
	        return $rs->num_rows();
		}


		public function select_transferred_in_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('student_transfer_in_view');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'TRANSFERRED IN STUDENT');
			$this->db->trans_complete();
			return $rs->result_array();
		}

		public function select_transferred_out_students($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || admission_no LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('student_transfer_out_view');
	        $this->db->order_by('admission_no','ASC');
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();	        
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'TRANSFERRED OUT STUDENT');
			$this->db->trans_complete();
			return $rs->result_array();
		}

		//CHECK IF EXISTS USING AJAX
		public function check_if_exist($value){
			$query = $this->db->query("SELECT * FROM selected_students WHERE examination_no='".$value."'");
			return $query->num_rows();
		}

		public function get_selected_student_by_exam_no($value){
			$examination_no = $this->db->escape($value);
			$query = $this->db->query("SELECT * FROM selected_students WHERE examination_no=".$examination_no."");
			return $query->row_array();
		}

		public function mark_as_reported($exam_no, $record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('examination_no', $exam_no);
			$this->db->update('selected_students', $record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SET REPORTED', 'SELECTED STUDENTS');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//GET CLASS LEVEL TO BE USED IN OBTAINING TERM_ID IN get_students_by_class_and_subject()
		//AND USED IN FUNCTION WITH THIS VIEW "view_class_stream_results_bwiru"
		public function select_class_level_by_class_stream_id($class_stream_id){
			$this->db->select('*');
			$this->db->from('class_level');
			$this->db->join('class_stream', 'class_level.class_id = class_stream.class_id');
			$this->db->where('class_stream_id', $class_stream_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function class_name_for_prog_res($admission_no){
			$this->db->select('enrollment.*, class_level.class_name');
			$this->db->from('class_level');
			$this->db->join('enrollment', 'class_level.class_id = enrollment.class_id');
			$this->db->where('admission_no', $admission_no);
			$this->db->order_by('class_id', 'ASC');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_current_term_id_by_level($level){
			$this->db->select('*');
			$this->db->from('current_term_academic_year_view');
			$this->db->where('class_level', $level);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function get_current_term_id_by_class_stream_id($class_stream_id){
			$query = $this->db->query('SELECT * FROM current_term_academic_year_view WHERE class_level=(SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id="'.$class_stream_id.'"))');
			return $query->row_array();
		}

		/*public function get_student_class_level($admission_no){
			$query = $this->db->query('SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM student WHERE admission_no="'.$admission_no.'" LIMIT 1)');

			return $query->row_array();
		}*/

		public function get_student_class_level($admission_no){
			$query = $this->db->query('SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM enrollment WHERE admission_no="'.$admission_no.'" LIMIT 1)');

			return $query->row_array();
		}

		public function calculate_o_division($admission_no, $term_id){
			$query = $this->db->query('SELECT SUM(unit_point) AS points, CASE WHEN SUM(unit_point) THEN (SELECT div_name FROM division WHERE level="O" AND starting_points <= SUM(unit_point) && ending_points >= SUM(unit_point)) END AS "division_name", '.$admission_no.' AS adm_no FROM (SELECT * FROM calculate_o_level_units_view WHERE admission_no="'.$admission_no.'" AND term_id="'.$term_id.'" LIMIT 7) t');

			return $query->row_array();
		}

		public function calculate_a_division($admission_no, $term_id){
			$rs = $this->db->query('SELECT nos FROM compute_division WHERE level="A" LIMIT 1');
			if($rs->row_array() == TRUE){
				$query = $this->db->query('SELECT SUM(points) AS points, CASE WHEN SUM(points) THEN (SELECT div_name FROM division WHERE level="A" AND starting_points <= SUM(points) && ending_points >= SUM(points)) END AS "division_name", '.$admission_no.' AS adm_no FROM (SELECT * FROM calculate_a_level_units_view WHERE admission_no="'.$admission_no.'" AND term_id="'.$term_id.'" LIMIT '.$rs->row_array()['nos'].') t');

				return $query->row_array();
			}
			else{
				return FALSE;
				//echo "error"; exit;
				//$this->session->set_flashdata('error_message', 'Error, set minimum number of subjects for computing division to have division and points in display');
			}
		}

		public function count_students_per_class($class_id, $term_id){
			//$query = $this->db->query('SELECT * FROM enrollment WHERE class_id='.$this->db->escape($class_id).' AND academic_year=(SELECT aid FROM term WHERE term_id='.$this->db->escape($term_id).')');
			$query = $this->db->query('SELECT * FROM student_assessment WHERE class_id='.$this->db->escape($class_id).' AND term_id='.$this->db->escape($term_id).'');
			return $query->num_rows();	
		}

		public function select_class_by_std_and_term($admission_no, $term_id){
			$query = $this->db->query('SELECT class_id FROM enrollment WHERE admission_no='.$this->db->escape($admission_no).' AND academic_year=(SELECT aid FROM term WHERE term_id='.$this->db->escape($term_id).')');

			return $query->row_array()['class_id'];	
		}

		/*public function count_students_per_class($admission_no, $term_id){
			$query = $this->db->query('SELECT * FROM enrollment where class_id=(SELECT class_id FROM student WHERE admission_no='.$this->db->escape($admission_no).') AND academic_year=(SELECT aid FROM term WHERE term_id='.$this->db->escape($term_id).')');

			return $query->num_rows();			
		}*/

		
		/*public function count_students_per_class($admission_no){
			$query = $this->db->query('SELECT * FROM student where class_id=(SELECT class_id FROM student WHERE admission_no="'.$admission_no.'")');
			return $query->num_rows();
		}*/

		//USED IN ADMIN TO PRINT INDIVIDUAL RESULT KWENEYE MKEKA
		public function students_per_class($class_id, $term_id){
			$query = $this->db->query('SELECT * FROM student_assessment WHERE class_id="'.$class_id.'" AND term_id='.$term_id.'');

			//$this->db->get();
			return $query->num_rows();
		}

		public function get_class_teacher($admission_no){
			$query = $this->db->query(' SELECT staff_id, firstname, lastname, concat(firstname, " ", lastname) AS teacher_names, phone_no FROM staff WHERE staff_id=(SELECT teacher_id FROM class_stream WHERE class_stream_id=(SELECT class_stream_id FROM student WHERE admission_no="'.$admission_no.'"))');

			return $query->row_array();
		}
		//END CODE


		public function count_students_by_class_and_dept_id($dept_id, $class_id, $value){
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || active_students_view.admission_no LIKE '%$value%')");
	        }

	        $this->db->select('DISTINCT(active_students_view.admission_no), firstname, lastname');
	        $this->db->from('active_students_view');
	        $this->db->join('student_subject', 'active_students_view.admission_no = student_subject.admission_no');
	        $this->db->join('subject', 'student_subject.subject_id = subject.subject_id');
	        $this->db->join('department', 'department.dept_id = subject.dept_id');
	        $this->db->where('active_students_view.class_id', $class_id);
	        $this->db->where('department.dept_id', $dept_id);

	        $rs = $this->db->get();
	        return $rs->num_rows();
		}

		public function fetch_students_by_class_and_dept_id($dept_id, $class_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || lastname LIKE '%$value%' || active_students_view.admission_no LIKE '%$value%')");
	        }

	        $this->db->select('DISTINCT(active_students_view.admission_no), firstname, lastname');
	        $this->db->from('active_students_view');
	        $this->db->join('student_subject', 'active_students_view.admission_no = student_subject.admission_no');
	        $this->db->join('subject', 'student_subject.subject_id = subject.subject_id');
	        $this->db->join('department', 'department.dept_id = subject.dept_id');
	        $this->db->where('active_students_view.class_id', $class_id);
	        $this->db->where('department.dept_id', $dept_id);
	        $this->db->limit($limit, $offset);

	        $rs = $this->db->get();
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'STUDENTS IN DEPARTMENT');
			$this->db->trans_complete();
			return $rs->result_array();
		}




		//PROGRESSIVE
		public function select_single_student_result_by_term($admission_no, $term_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->select('*');
			$this->db->from('student_progressive_results_view');
			$this->db->where('admission_no', $admission_no);
			$this->db->where('term_id', $term_id);

			$query = $this->db->get();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'PROGRESSIVE RESULTS');
			$this->db->trans_complete();
			return $query->result_array();
		}


		public function select_single_student_position_by_term($admission_no, $term_id){
			$query = $this->db->query('SELECT student_assessment.*, class_name, stream, term.aid, academic_year.year, student.academic_year, concat(student.firstname, " ", student.lastname) AS names, find_in_set(average, (SELECT group_concat(average order by average desc) FROM student_assessment WHERE class_id=(SELECT class_id FROM enrollment WHERE admission_no="'.$admission_no.'" AND academic_year=(SELECT aid FROM term where term_id='.$term_id.' LIMIT 1)) AND term_id='.$term_id.')) AS rank FROM student_assessment JOIN student USING(admission_no) JOIN class_level ON(student_assessment.class_id = class_level.class_id) JOIN class_stream ON(student_assessment.class_stream_id = class_stream.class_stream_id) JOIN term ON(student_assessment.term_id = term.term_id) JOIN academic_year ON(academic_year.id = term.aid) WHERE student_assessment.class_id=(SELECT class_id FROM enrollment WHERE admission_no="'.$admission_no.'" AND academic_year=(SELECT aid FROM term WHERE term_id='.$term_id.') LIMIT 1) AND student_assessment.admission_no="'.$admission_no.'" AND student_assessment.term_id='.$term_id.' LIMIT 1');

			return $query->row_array();
		}
		//END PROGRESSIVE











		//AVATAR
		public function in_student_image($data, $admission_no){
        	//$this->db->set($data);
        	$this->db->trans_start();
        	$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
        	$this->db->where('admission_no', $admission_no);
        	$this->db->update('student', $data);
        	//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPLOAD IMAGE', 'STUDENT');
        	$this->db->trans_complete();

        	return $this->feedbackMsg();
        }
		//@den profile picture;
		public function profile_picture($admission_no){
			$this->db->where('admission_no', $admission_no);
			$query = $this->db->get('student');
			return $query->row_array();
		}

		//END AVATAR






		  // All students @den
		public function retrieve_all_students(){
			$query=$this->db->get('active_students_view');
			return $query;
		}// End All students @den






		public function CI_registerStudent($table_name, $datas, $where, $action){
		    //To INSERT data
		    if($action === 'INSERT'){
		    	$this->db->insert($table_name, $datas);
		    }

		    //To UPDATE data
		    if($action === 'UPDATE'){
			    $this->db->where($where);
			    $this->db->update($table_name, $datas);
			}
		    //To DELETE data
		    if($action === 'DELETE'){
		    	$this->db->where($where);
			    $this->db->delete($table_name);
		    }			    

		    $error = $this->db->error();
		    $errorCode = $error['code'];
		    $errorText='';

		    switch($errorCode){
		    	case 1644:
		    		$errorText = $error['message'];
		    		break;
		    	case 1451:
		    		$errorText = 'Cannot delete '.$table_name.'';
		    		break;
		        case 1062:
		            $errorText = 'The record you are trying to enter already exist';
		            break;
		        case 1452:
		            $errorText = 'Foreign key Constraint';
		            break;
		        case 1146:
		            $errorText = 'Table Not exist';
		            break;
		        case 0:
		            $errorText = 'Record Successfully Inserted';
		            break;
		        default:
		            $errorText = 'other errors';
		            break;
		    }

		    //then print or return $errorText
		    return $errorCode;
		    //echo $errorText;
		}
	}
