<?php
	class Login_model extends MY_Model{
		public function __construct(){
			$this->load->database();
		}

		public function select_modules($staff_id = FALSE){
			$this->db->select('*');
			$this->db->from('module');
			$this->db->join('role_module', 'module.module_id = role_module.module_id');
			$this->db->join('staff_role', 'staff_role.role_type_id = role_module.role_type_id');

			if($staff_id === FALSE){
				$rs = $this->db->get();
				return $rs->result_array();
			}			

			$this->db->where('staff_id', $staff_id);
			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_role_permissions_by_staff_id($staff_id){
			$this->db->select('*');
			$this->db->from('staff_role_permission_view');
			$this->db->where('staff_id', $staff_id);

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function select_admin_permissions(){
			$rs = $this->db->query('SELECT * FROM permission JOIN super_admin_permission using(permission_id)');
			return $rs->result_array();
		}

		public function select_admin_modules(){
			$rs = $this->db->query('SELECT * FROM module JOIN super_admin_module using(module_id)');
			return $rs->result_array();
		}

		public function login_user($username, $password){
			if($username === 'Administrator'){
				$this->db->select('*');
				$this->db->from('admin');
				$this->db->where('username', $username);
				$this->db->where('password', $password);
				$this->db->limit('1');
			}
			else{
				$this->db->select('staff.*, lock_account_tbl.account_locked');
				$this->db->from('staff');
				$this->db->join('lock_account_tbl', 'lock_account_tbl.username = staff.phone_no', 'left');
				$this->db->where('phone_no', $username);
				$this->db->where('password', $password);
				$this->db->where('status', 'active');
				$this->db->limit('1');
			}				

			$query = $this->db->get();

			if($query->num_rows() > 0){
				return $query->row_array();
			}
			else{
				return false;
			}
		}

		public function update_past_login($time, $username){			
			$last_login_record = array('last_log' => $time);

			$this->db->where('phone_no', $username);
			$this->db->update('staff', $last_login_record);
		}

		public function last_login_admin($time, $id){			
			$last_login_record = array('last_log' => $time);

			$this->db->where('id', $id);
			$this->db->update('admin', $last_login_record);
		}

		public function select_user_roles($staff_id){
			$this->db->select('*');
			$this->db->from('assigned_user_roles');
			$this->db->where('staff_id', $staff_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function erase_record_from_lock_account_table($username){
			$this->db->where('username', $username);
			return $this->db->delete('lock_account_tbl');
		}

		public function login_attempt_failed($username){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
			$q = $this->db->query('SELECT COUNT(*) AS count_staff FROM staff WHERE phone_no="'.$username.'"');
			$count_staff = $q->row_array()['count_staff'];

			$query = $this->db->query('SELECT COUNT(*) AS count, count_failed_attempt FROM lock_account_tbl WHERE username="'.$username.'"');
			$returned_value = $query->row_array()['count_failed_attempt'];
			$count = $query->row_array()['count'];

			$lock_account = 'YES';
			if($count == 0){
				$count_attempt = array(
					'username' => $username,
					'account_locked' => 'NO',
					'count_failed_attempt' => ++$returned_value
				);
				if($count_staff > 0){
					$this->db->insert('lock_account_tbl', $count_attempt);
				}
			}
			else{
				if($returned_value == 4){
					$count_attempt = array(
						'account_locked' => $lock_account,
						'count_failed_attempt' => ++$returned_value
					);
					$event_name = "unlock_account_event_".substr($username, 1);
					/*echo $event_name;exit;*/
					$this->db->query('SET GLOBAL event_scheduler =  "ON"');
					$this->db->query('CREATE EVENT '.$event_name.' ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 5 MINUTE DO BEGIN DELETE FROM lock_account_tbl WHERE username="'.$username.'"; END');
					$this->session->set_flashdata('locked_message', 'Account Locked, try again after sometime');
				}
				else{
					$count_attempt = array(
						'count_failed_attempt' => ++$returned_value
					);
					if($returned_value > 4){
						$this->session->set_flashdata('locked_message', 'Account Locked, try again after sometime');
					}
				}
			}

			if($count_staff > 0){
				$this->db->where('username', $username);
				$this->db->update('lock_account_tbl', $count_attempt);
			}				
			$this->db->trans_complete();
		}
	}
?>