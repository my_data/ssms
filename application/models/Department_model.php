<?php
	class Department_model extends MY_Model{

		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function create_new_dept($department_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			//$count = $this->check_user_permission($this->session->userdata('staff_id'), "Add New Department");
			/*if($count == 0){
				$this->session->set_flashdata('error_message', 'You do NOT have permission to Add New Department');
				return;
			}*/
			/*$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');*/
			$this->CI_mySQL('department', $department_record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'INSERT', 'DEPARTMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_department_by_type($dept_type = FALSE){
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DEPARTMENT');

			$this->db->select('*');
			$this->db->from('department_view');

			$this->db->where('dept_type', $dept_type);
			$query = $this->db->get();
			return $query->result_array();
		}

		public function delete_dept($id){
			$this->db->where('dept_id', $id);
			$this->db->delete('department');
			return true;
		}

		public function select_department_dept_id($dept_id = FALSE){
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DEPARTMENT');

			$this->db->select('*');
			$this->db->from('department');
			$this->db->where('dept_id', $dept_id);

			$query = $this->db->get();
			return $query->row_array();
		}

		public function update_department($dept_id, $department_record){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('department', $department_record, array('dept_id' => $dept_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'DEPARTMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function delete_department($dept_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('department', NULL, array('dept_id' => $dept_id), 'DELETE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'DEPARTMENT');
			$rs = $this->db->trans_complete();

			return $this->feedbackMsg();
		}

		//Teaching assignment
		public function select_subject_by_department($dept_id){
			$this->db->select('*');
			$this->db->from('subject');
			$this->db->where('dept_id', $dept_id);

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_teacher_by_department($dept_id){
			$this->db->select('staff.staff_id, concat(firstname, " ", lastname) AS names');		//THINK OF QUERYING "staff_department" table
			$this->db->from('teacher');
			$this->db->join('subject_teacher', 'subject_teacher.teacher_id = teacher.staff_id');
			$this->db->join('subject', 'subject_teacher.subject_id = subject.subject_id');
			$this->db->join('department', 'subject.dept_id = department.dept_id');
			$this->db->join('staff', 'staff.staff_id = teacher.staff_id');
			$this->db->where('department.dept_id', $dept_id);
			$this->db->group_by('staff.staff_id');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_class_name_by_department($dept_id){
			$this->db->select('class_level.class_id, class_name, stream, class_stream.class_stream_id');
			$this->db->from('class_stream');
			$this->db->join('class_stream_subject', 'class_stream_subject.class_stream_id = class_stream.class_stream_id');
			$this->db->join('subject', 'class_stream_subject.subject_id = subject.subject_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('dept_id', $dept_id);
			$this->db->group_by('class_name');
			$this->db->order_by('class_id');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function select_stream_by_department($dept_id){
			$this->db->select('class_level.class_id, class_name, stream, class_stream.class_stream_id');
			$this->db->from('class_stream');
			$this->db->join('class_stream_subject', 'class_stream_subject.class_stream_id = class_stream.class_stream_id');
			$this->db->join('subject', 'class_stream_subject.subject_id = subject.subject_id');
			$this->db->join('class_level', 'class_level.class_id = class_stream.class_id');
			$this->db->where('dept_id', $dept_id);
			$this->db->group_by('stream');
			$this->db->order_by('stream');

			$query = $this->db->get();
			return $query->result_array();
		}

		public function insert_teaching_assignment_record($data){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_assignment', $data, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ASSIGN TEACHING SUBJECT', 'TEACHING_ASSIGNMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function check_before_extend_ta($dept_id){
			$this->db->select('COUNT(class_stream_id) As count');
			$this->db->from('teaching_assignment');
			$this->db->where('dept_id', $dept_id);
			$this->db->where('term_id NOT IN (SELECT term_id FROM term WHERE is_current="yes")');

			$query = $this->db->get();
			return $query->row_array()['count'];
		}

		public function extend_teaching_assignment($dept_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('CALL extend_teaching_assignment_procedure('.$this->db->escape($dept_id).')');
			$this->CI_getProcedureError();
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'EXTEND TEACHING ASSIGNMENT', 'TEACHING_ASSIGNMENT');
			//$this->session->set_flashdata('success_message', 'Successfully extended to this current_term');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function update_teaching_assignment_record($tar, $assignment_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->CI_mySQL('teaching_assignment', $tar, array('assignment_id' => $assignment_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE', 'TEACHING_ASSIGNMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function check_css_before_assign($dept_id){
			$rs = $this->db->query('SELECT COUNT(subject_id) AS cnt FROM class_stream_subject WHERE subject_id IN (SELECT subject_id FROM subject WHERE dept_id='.$this->db->escape($dept_id).')');
			return $rs->row_array()['cnt'];
		}

		public function get_department_id_by_hod($staff_id){
			$this->db->select('*');
			$this->db->from('department');
			$this->db->where('staff_id', $staff_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function select_staff_by_department($dept_id){
			$query = $this->db->query('SELECT department_staff_view.*, GROUP_CONCAT("\t", subject_name) AS subjects FROM department_staff_view WHERE dept_id="'.$dept_id.'" GROUP BY staff_id');
			return $query->result_array();
		}

		public function count_ta_per_department($dept_id, $value){
			$dept_id = $this->db->escape($dept_id);
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || subject_name LIKE '%$value%' || gender LIKE '%$value%' || lastname LIKE '%$value%' || class_name LIKE '%$value%')");
	        }

			$this->db->select('*');
	        $this->db->from('teaching_assignment_department_view');
	        $this->db->where('dept_id='.$dept_id.' AND subject_id IN(SELECT subject_id FROM subject WHERE dept_id='.$dept_id.')');

	        $rs = $this->db->get();
			return $rs->num_rows();
		}

		public function select_teaching_assignment_by_department($dept_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$dept_id = $this->db->escape($dept_id);
			if(!empty($value)){
				$this->db->where("(firstname LIKE '%$value%' || subject_name LIKE '%$value%' || gender LIKE '%$value%' || lastname LIKE '%$value%' || class_name LIKE '%$value%')");
	        }

	        $this->db->select('*');
	        $this->db->from('teaching_assignment_department_view');
	        $this->db->where('dept_id='.$dept_id.' AND subject_id IN(SELECT subject_id FROM subject WHERE dept_id='.$dept_id.')');
	        $this->db->limit($limit, $offset);

			$rs = $this->db->get();
			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'TEACHING_ASSIGNMENT');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function delete_teaching_assignment($teacher_id, $subject_id, $class_stream_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->where('teacher_id', $teacher_id);
			$this->db->where('subject_id', $subject_id);
			$this->db->where('class_stream_id', $class_stream_id);
			$this->db->delete('teaching_assignment');

			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'DELETE', 'TEACHING_ASSIGNMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}

		public function select_teaching_assignment_id($assignment_id){
			$this->db->select('*');
			$this->db->from('teaching_assignment_department_view');
			$this->db->where('assignment_id', $assignment_id);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}


		public function select_classes_by_department($dept_id, $value){
			if(!empty($value)){
				$this->db->where("(class_name LIKE '%$value%' || subject_name LIKE '%$value%')");
	        }

	        $this->db->select('department_class_stream_subject_view.*, GROUP_CONCAT("\t", subject_name) AS subjects');
			$this->db->from('department_class_stream_subject_view');
			$this->db->where('dept_id', $dept_id);
			$this->db->group_by('class_stream_id');

			$rs = $this->db->get();
			return $rs->result_array();
		}

		public function view_all_depts(){
			$this->db->select('*');
			$this->db->from('department_view');

			$query = $this->db->get();
			return $query->result_array();
		}


		public function get_departments_by_staff_id($staff_id){
			$this->db->select('*');
			$this->db->from('department_staff_view');
			$this->db->where('staff_id', $staff_id);

			$query = $this->db->get();
			return $query->result_array();
		}


		public function count_staff_per_department($dept_id, $value){
			if(!empty($value)){
				$this->db->where("(staff_names LIKE '%$value%' || subject_name LIKE '%$value%' || gender LIKE '%$value%')");
	        }

			$this->db->from('department_staff_view');
			$this->db->where('dept_id', $dept_id);
			$this->db->group_by('staff_id');

			$q = $this->db->get();
			return $q->num_rows();
		}

		public function fetch_staff_by_department($dept_id, $limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->where("(staff_names LIKE '%$value%' || subject_name LIKE '%$value%' || gender LIKE '%$value%')");
	        }

	        $this->db->select('department_staff_view.*, GROUP_CONCAT("\t", subject_name) AS subjects');
			$this->db->from('department_staff_view');
			$this->db->where('dept_id', $dept_id);
			$this->db->group_by('staff_id');
			$this->db->limit($limit, $offset);

			$query = $this->db->get();
				
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'DEPARTMENT_STAFFS');
			$this->db->trans_complete();

			return $query->result_array();
		}

		public function assign_hod($dept_id, $record, $staff_id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->query('UPDATE department SET staff_id=NULL WHERE staff_id='.$this->db->escape($staff_id).'');
			$this->db->query('CALL assign_hod_procedure('.$this->db->escape($dept_id).', '.$this->db->escape($staff_id).')');
			$this->CI_getProcedureError();
			
			$this->CI_mySQL('department', $record, array('dept_id' => $dept_id), 'UPDATE');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ASSIGN HOD', 'DEPARTMENT');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function select_hods_history($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dept_name', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('hod_history.*, department.dept_name, concat(firstname, " ", lastname) AS staff_names');
			$this->db->from('hod_history');
			$this->db->join('department', 'hod_history.dept_id = department.dept_id');
			$this->db->join('staff', 'staff.staff_id = hod_history.staff_id');
			$this->db->order_by('department.dept_id', 'DESC');
			$this->db->order_by('hod_history.end_date', 'ASC');

			$this->db->limit($limit, $offset);

			$rs = $this->db->get();			
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW', 'HODs HISTORY');
			$this->db->trans_complete();

			return $rs->result_array();
		}

		public function count_hods_history($value){
			if(!empty($value)){
				$this->db->like('firstname', $value);
				$this->db->or_like('lastname', $value);
				$this->db->or_like('dept_name', $value);
				$this->db->or_like('start_date', $value);
				$this->db->or_like('end_date', $value);
			}

			$this->db->select('hod_history.*, department.staff_id, department.dept_name, concat(firstname, " ", lastname) AS staff_names');
			$this->db->from('hod_history');
			$this->db->join('department', 'hod_history.dept_id = department.dept_id');
			$this->db->join('staff', 'staff.staff_id = hod_history.staff_id');

			$rs = $this->db->get();
			return $rs->num_rows();
		}
	}