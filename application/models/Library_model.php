<?php

	class Library_model extends MY_Model{
		function __construct(){
			parent::__construct();
			$this->load->database();
		}

	

		public function addBook($data){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->insert("book_type", $data);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD BOOK', 'BOOK');
			$this->db->trans_complete();
			
			return $this->feedbackMsg();	
		}


		public function check_book($isbn){
			$this->db->where('isbn', $isbn);
			$check = $this->db->get('book_type');
			return $check->num_rows();
		} 


		public function retrievebooktype($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BOOK_TITLE', $value);
	        }

			$this->db->limit($limit,$offset);
		    $query = $this->db->get("book_type_report");			
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW BOOKS', 'BOOK');
			$this->db->trans_complete();

	        return $query;
		}


		public function deleted_type($id){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->delete('book_type', array('ISBN' => $id));
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'REMOVE BOOK', 'BOOK');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function edit($id){
			$dat = $this->db->get_where('book_type', array('ISBN' => $id))->row();
			return $dat;
		}


		public function updatebook_type($data, $isbn){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

    		$this->db->set($data);
    		$this->db->where('ISBN', $isbn);
    		$datas = $this->db->update('book_type', $data); // gives UPDATE mytable SET field = field+1 WHERE id = 2
    		//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE BOOK', 'BOOK');
			$this->db->trans_complete();

			return $this->feedbackMsg();
		}


		public function populate_dropdown(){	
			$this->db->select('ISBN, BOOK_TITLE, EDITION');
			$this->db->order_by('ISBN', "asc");

			$query = $this->db->get('book_type');
			if($query){
    			$query = $query->result_array();
    			return $query;
			}
		}

		public function ed_indiv_Book($data, $id, $isbn){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->set($data);
			$this->db->where('ISBN',$isbn);
			$this->db->where('ID',$id);	
			$this->db->update("book",$data);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'UPDATE BOOK STATUS', 'BOOK');
			$this->db->trans_complete();	

			return $this->feedbackMsg();
		}


		public function ret_indiv_bbok(){
			$result = $this->db->get('book_copy_report');
		}


		public function ind_book_report($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('CODE', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BOOK_TITLE', $value);
        	}

			$this->db->limit($limit,$offset);
			$query = $this->db->get("book_copy_report");			
	        //Activity Log
	        $this->activity_log($this->session->userdata('staff_id'), 'VIEW BOOK COPIES', 'BOOK');
			$this->db->trans_complete();

	        return $query;
		}


		public function ad_to_book($id){
			$dat = $this->db->get_where('book', array('ID' => $id))->row();
			return $dat;
		}


		public function fill_book($id){
			$dat = $this->db->get_where('book_type', array('ISBN' => $id))->row();
			return $dat;
		}

		public function delete_book($id){
			return $this->db->delete('book', array('ID' => $id));	
		}


		public function edit_book($id){
			$dat = $this->db->get_where('book', array('ID' => $id))->row();
			return $dat;
		}


		public function view_type_in($id, $limit, $offset, $value){
			if(!empty($value)){
				$this->db->like('ID', $value);
	        }

			$this->db->limit($limit, $offset);
			//$this->db->where('ISBN', $id);
			//$query = $this->db->get("available_books_view");
		    $query = $this->db->get_where("available_books_view", array('ISBN' => $id));
			return $query->result();
		}



		public function select_classes(){
		 	$q = $this->db->get('class_level');
			return $q->result_array();
		}


		public function total_type($value = ""){
			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BOOK_TITLE', $value);
	        }

	        $rs = $this->db->get('book_type_report');
	        return $rs->num_rows();
	    }

		
		public function total_byISBN($id, $value = ""){
			if(!empty($value)){
				$this->db->like('ID', $value);
	        }
			$query = $this->db->get_where("available_books_view", array('ISBN' => $id));
			return $query->num_rows();
		}


		public function total_books($value = ""){
			if(!empty($value)){
	            $this->db->like('CODE', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BOOK_TITLE', $value);
	        }

	        //$rs = $this->db->get('book');
	        $rs = $this->db->get('book_copy_report');
	        return $rs->num_rows();
	    }



		public function total_books_remained(){
			$dat = $this->db->get('remained_book_view');
			return $dat->num_rows();
		}


		public function getSubjects(){
			$data=$this->db->get('subject');
			return $data->result_array();
		} 


		public function borrow($data){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			$this->db->insert("book_loan_info", $data);
			$this->activity_log($this->session->userdata('staff_id'), 'ISSUE A BOOK COPY', 'BOOK');
			$this->db->trans_complete();
			return TRUE;
		}


		public function borrow_book_department($record){
			$this->db->insert_batch('book_loan_info', $record);
		}


		public function add_title($id){
			return $this->db->get_where('book_copy_report', array('CODE' => $id))->row();
		}

		public function type_title($id){
			$dat = $this->db->get_where('book_type', array('ISBN' => $id))->row();
			return $dat;	
		}


		public function type(){
			$type = $this->db->get('borrower_type');
			 return $type->result_array();
		}


		public function borrowed($limit, $offset, $value){
			if(!empty($value)){
				$this->db->like('ID', $value);
				$this->db->or_like('BORROWER_DESCRIPTION', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BORROWER_ID', $value);
				$this->db->or_like('BORROWER_TYPE', $value);
	        }

			$this->db->limit($limit,$offset);
		    $query = $this->db->get("borrowed_report");
			return $query;
		}


		public function insert_books($data){
			return $this->db->insert_batch('book', $data);
		}


		public function available($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('ID', $value);
				$this->db->or_like('BOOK_TITLE', $value);
	        }

			$this->db->limit($limit,$offset);
		    $query = $this->db->get("available_books_view");			
		    //Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'VIEW BOOK COPIES', 'BOOK');
			$this->db->trans_complete();

		    return $query;
		}


	
		public function total_available($value = ""){
			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('ID', $value);
				$this->db->or_like('BOOK_TITLE', $value);
	        }

	        $rs = $this->db->get('available_books_view');
	        return $rs->num_rows();
	    }


 		public function fill_to_returnbook($id){
	   		return $this->db->get_where('borrowed_report', array('TRANSACTION_ID' => $id))->row();
		}


		public function return_book($field, $book, $isbn, $code){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

		    $this->db->set($field);
		    $this->db->where('ISBN',$isbn);
		    $this->db->where('ID',$code);
		    $datas = $this->db->update('book_loan_info', $field); // gives UPDATE TRANSACT
		    $this->db->set($book);
		    $this->db->where('ISBN', $isbn);
		    $this->db->where('ID', $code);
		    $dat = $this->db->update('book', $book); // gives UPDATE book table
		    //Activity Log
		    $this->activity_log($this->session->userdata('staff_id'), 'UPDATE BOOK COPY RETURN', 'BOOK');
		    $this->db->trans_complete();
		}


		public function returned_book(){
			return $this->db->get('returned_book_view');
		}


		public function total_lost_books($value){
			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('ID', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
	        }
	        //$rs = $this->db->get('book');
	        $rs = $this->db->get('lost_book_view');
	        return $rs->num_rows();
		}


		public function lost_books($limit, $offset, $value){
			$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('ISBN', $value);
				$this->db->or_like('ID', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
	        }

			$this->db->limit($limit,$offset);
		    $query = $this->db->get("lost_book_view");
		    //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'VIEW LOST BOOKS', 'BOOK');
			$this->db->trans_complete();
			return $query;
		}


	 	public function borrowing_history($limit, $offset, $value){
	 		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('DATE_BORROWED', $value);
				$this->db->or_like('BORROWER_TYPE', $value);
				$this->db->or_like('NAMES', $value);
				$this->db->or_like('FLAG', $value);
				$this->db->or_like('RETURN_DATE', $value);
				$this->db->or_like('DATE_BORROWED', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('ID', $value);
	        }

			$this->db->limit($limit,$offset);
		    $query = $this->db->get("borrowing_report");
		    //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'BORROWING HISTORY', 'BOOK');
			$this->db->trans_complete();
			return $query;
		}


		public function total_history($value = ""){
			if(!empty($value)){
	            $this->db->like('DATE_BORROWED', $value);
				$this->db->or_like('BORROWER_TYPE', $value);
				$this->db->or_like('NAMES', $value);
				$this->db->or_like('FLAG', $value);
				$this->db->or_like('RETURN_DATE', $value);
				$this->db->or_like('DATE_BORROWED', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('ID', $value);
	        }

	        $rs = $this->db->get('borrowing_report');
	        return $rs->num_rows();
	    }


 		public function title_sum_up(){
 	     	return $this->db->get('title_sum_up_view');
 		}


	 	public function total_borrowed($value = ""){
			if(!empty($value)){
				$this->db->like('ID', $value);
				$this->db->or_like('BORROWER_DESCRIPTION', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('BORROWER_ID', $value);
	        }

	        //WHY ARE WESELECTING AND COUNTING FROM TWO DIFFERENT TABLES???????????
	        $rs = $this->db->get('borrowed_report');
	        return $rs->num_rows();
	    }


 		public function lost_book_total(){
	 	 	$this->db->where('STATUS','lost');
	 	 	$number = $this->db->get('book');

	 	 	return $number->num_rows();
 		}

 		public function total_normal_book(){
	 	 	$this->db->where('STATUS','normal');
	 	 	$number = $this->db->get('book');

	 	 	return $number->num_rows();
 		}


		public function total_poor_book(){
	 	 	$this->db->where('STATUS', 'bad');
	 	 	$number = $this->db->get('book');

	 	 	return $number->num_rows();
		}
 

		public function total_expired_books($value){
			if(!empty($value)){
				$this->db->like('ID', $value);
				$this->db->or_like('BORROWER_ID', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('NAMES', $value);
				$this->db->or_like('TIME_LEFT', $value);
				$this->db->or_like('DATE_BORROWED', $value);
				$this->db->or_like('RETURN_DATE', $value);
	        }

	        $rs = $this->db->get('expired_book_view');
	        return $rs->num_rows();
	    }


	    public function expired_books($limit, $offset, $value){
	    	$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');

			if(!empty($value)){
				$this->db->like('ID', $value);
				$this->db->or_like('BORROWER_ID', $value);
				$this->db->or_like('ISBN', $value);
				$this->db->or_like('BOOK_TITLE', $value);
				$this->db->or_like('AUTHOR_NAME', $value);
				$this->db->or_like('NAMES', $value);
				$this->db->or_like('TIME_LEFT', $value);
				$this->db->or_like('DATE_BORROWED', $value);
				$this->db->or_like('RETURN_DATE', $value);
	        }

	        $this->db->limit($limit, $offset);
	        $query = $this->db->get('expired_book_view');
	        //Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'EXPIRED BOOKS', 'BOOK');
			$this->db->trans_complete();
	        return $query;
	    }


 		public function count_borrowed($temp){//counts book copies borrowed by the same users
	 		$this->db->where('borrower_id', $temp);
	 		$this->db->where('FLAG', 'borrowed');
	    	$number = $this->db->get('book_loan_info');

	 		return $number->num_rows();
 		}


	 	public function check_user($temp){//checks if user exists//
			$this->db->where('borrower_id',$temp);
			$status = $this->db->get('borrowers');

			return $status->num_rows();
		}


		function avoid_same($temp, $id, $isbn){//Get book copies borrowed by the customer
		 	$this->db->where('borrower_id', $temp);
		 	$this->db->where('id', $id);
		 	$this->db->where('isbn', $isbn);
		 	$this->db->where('FLAG', 'borrowed');
		    $status = $this->db->get('book_loan_info');

		    return $status->num_rows();
		 }


	 	public function get_total_available_copies($isbn){
	 		$this->db->select('COUNT(*) AS total_available_copies');
	 		$this->db->from('book');
	 		$this->db->where('isbn',$isbn);

	 		$rs = $this->db->get();
			return $rs->row_array();
	 	}

	 	public function add_copies_by_isbn($record){
	 		$this->db->insert_batch('book', $record);
	 	}

	 	public function check_if_borrower_exist($value){
	 		$query = $this->db->query("SELECT * FROM borrowers WHERE borrower_id = '".$value."'");
			return $query->num_rows();
	 	}

	 	public function get_borrower_name($value){
	 		$query = $this->db->query("SELECT * FROM borrowers WHERE borrower_id = '".$value."'");
			return $query->row_array();
	 	}

	 	public function select_book_by_isbn_and_code($isbn, $id){
	 		$query = $this->db->query("SELECT * FROM available_books_view WHERE ISBN = '".$isbn."' AND ID = '".$id."'");
			return $query->row();
	 	}

	 	public function get_borrower_types(){
	 		$query = $this->db->query("SELECT * from books_borrowing_limit");
			return $query->result_array();
	 	}

	 	public function select_btype($id){
	 		$query = $this->db->query("SELECT * from books_borrowing_limit WHERE blid=".$this->db->escape($id)."");
			return $query->row_array();
	 	}

	 	public function update_borrowing_limit($id, $record){
	 		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
	 		
			$this->db->where('blid', $id);
			$this->db->update("books_borrowing_limit", $record);
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'SET BORROWING LIMIT', 'BORROWER');
				$this->db->trans_complete();
	 	}

	 	public function add_borrowing_limit($record){
	 		$this->db->trans_start();
			$this->db->query('SET @user_id = "'.$this->session->userdata('staff_id').'"');
			$this->db->query('SET @ip_address = "'.$_SERVER["REMOTE_ADDR"].'"');
	 		
			$this->CI_mySQL('books_borrowing_limit', $record, NULL, 'INSERT');
			//Activity Log
			$this->activity_log($this->session->userdata('staff_id'), 'ADD BORROWING LIMIT', 'BORROWER');
			$this->db->trans_complete();
	 	}

	 	public function check_borrowing_limit($borrower_type){
	 		$query = $this->db->query("SELECT * from books_borrowing_limit WHERE b_type=".$this->db->escape($borrower_type)."");
			return $query->row_array()['borrowing_limit'];
	 	}

	 	public function select_borrowers(){
	 		$query = $this->db->query("SELECT DISTINCT(b_type) FROM borrowers");
			return $query->result_array();
	 	}
	}

?>