<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Teachers extends MY_Controller{
		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');
			//$this->load->model('teacher_model');
			//$this->load->model('student_model');
			//$this->load->model('subject_model');
			//$this->load->model('class_model');
			//$this->load->model('period_model');
			$this->load->library('pagination');
			$this->load->helper('url');
			$this->load->library('form_validation');

			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }	
			//$this->errorAcademicYearSettings();
		}

		public function index(){
			if(!($this->session->userdata('view_all_teachers') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_teacher') != ""){
	            $value = trim($this->input->post('search_teacher'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'teachers/index/'.$value;
			$config['total_rows'] = $this->teacher_model->count_all_teachers($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['teachers'] = $this->teacher_model->select_all_teachers($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['display_back'] = "NO";

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['title'] = $this->breadCrumb("teachers", NULL, NULL, NULL, "All Teachers", NULL, NULL);


			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/view_teachers');
			$this->load->view('templates/foot');
		}

		public function add_subject_teacher($teacher_id){
			if(!($this->session->userdata('add_subjects_to_teacher') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("teachers", "teachers/add_subject_teacher/".$teacher_id, NULL, NULL, "All Teachers", "Add / Remove Subject ", NULL);

			$data['teacher_id'] = $teacher_id;
			$data['subject_records'] = $this->admin_model->fetch_subjects_for_use();;
			$data['subject_teacher_records'] = $this->teacher_model->select_subjects_by_teacher_id($teacher_id);

			if(!file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			if($this->form_validation->run('add_subject_teacher') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('teachers/add_subjects_to_teacher');
				$this->load->view('templates/foot');
			} 
			else{
				$data['subjects'] = $this->input->post('subject_id');
				
				foreach ($data['subjects'] as $key => $value) {
					$subject_teacher_record[] = array(
						'subject_id' => $value,
						'teacher_id' => $teacher_id
					);

					$dept_id = $this->subject_model->select_from_subjects($value)['dept_id'];//Get the dept id
					$staff_department_record[] = array(
						'staff_id' => $teacher_id,
						'dept_id' => $dept_id
					);
				}

				$rs = $this->teacher_model->add_subjects_to_teacher($teacher_id, $subject_teacher_record, $staff_department_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
				redirect('teachers');
			}
		}

		public function fetch_teacher(){
			//$this->load->model('teacher_model');
			//$id = "Kyaruzi";
			$id=$this->input->get('id');
			$fetch_data = $this->teacher_model->make_datatables($id);
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				$sub_array[] = $row->staff_id;
				$sub_array[] = $row->firstname;
				$sub_array[] = $row->lastname;
				$sub_array[] = '<button type="button" name="update" id="'.$row->staff_id.'" class="btn btn-primary btn-xs" >Edit</button>';
				$sub_array[] = '<button type="button" name="delete" id="'.$row->staff_id.'" class="btn btn-danger btn-xs" >Delete</button>';
				$sub_array[] = '<a href="'.base_url() . 'admin/edit_staff/' . $row->staff_id.'">Edit</a>';

				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->teacher_model->get_all_data($id),
				"recordsFiltered"	=>	$this->teacher_model->get_filtered_data($id),
				"data"				=>	$data
			);
			echo json_encode($output);
		}

		public function home(){
			$data['title'] = "Teacher Homepage";
			$dataa = array(
				'year' => $this->uri->segment(3),
				'month' => $this->uri->segment(4)
			);

			// Creating template for table
			$prefs['template'] = '
			{table_open}
			<div class="table-responsive">
			<table id="calendar" cellpadding="0" cellspacing="0">{/table_open}

			{heading_row_start}<tr>{/heading_row_start}

			{heading_previous_cell}<th class="prev_sign"><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
			{heading_title_cell}<th style="color:#FFFFFF;" colspan="{colspan}">{heading}</th>{/heading_title_cell}
			{heading_next_cell}<th class="next_sign"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

			{heading_row_end}</tr>{/heading_row_end}

			//Deciding where to week row start
			{week_row_start}<tr class="week_name" >{/week_row_start}
			//Deciding  week day cell and  week days
			{week_day_cell}<td >{week_day}</td>{/week_day_cell}
			//week row end
			{week_row_end}</tr>{/week_row_end}

			{cal_row_start}<tr>{/cal_row_start}
			{cal_cell_start}<td>{/cal_cell_start}

			{cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
			{cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

			{cal_cell_no_content}{day}{/cal_cell_no_content}
			{cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

			{cal_cell_blank}&nbsp;{/cal_cell_blank}

			{cal_cell_end}</td>{/cal_cell_end}
			{cal_row_end}</tr>{/cal_row_end}

			{table_close}</table></div>{/table_close}
			';

			$prefs['day_type'] = 'short';
			$prefs['show_next_prev'] = true;
			$prefs['next_prev_url'] = base_url() . 'teachers/home';

			// Loading calendar library and configuring table template
			// Load view page
			//$this->load->view('calendar_show', $data);
			$this->load->library('calendar', $prefs);
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/homepage', $dataa);
			$this->load->view('templates/foot');
		}

		public function get_teachers_classes($teacher_id){
			$data['title'] = $this->breadCrumb("myclasses", NULL, NULL, NULL, "My Classes", NULL, NULL);

			$allowed = FALSE;
			if($this->session->userdata('view_assigned_classes') == 'ok' && $this->session->userdata('staff_id') === $teacher_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);
			

			if ($this->input->post('search_student') != "") {
				$value = $this->input->post('search_student');
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'teacher/assigned_classes/'.$teacher_id.'/'.$value;
			$config['total_rows'] = $this->teacher_model->count_teachers_teaching_assingment($teacher_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['myclasses'] = $this->teacher_model->select_teachers_teaching_assingment($teacher_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['topics'] = $this->subject_model->select_topics();
			//$data['logs'] = $this->subject_model->select_teaching_log();

			/*echo "<pre>";
				print_r($data['logs']);
			echo "</pre>";die;*/


			//$data['title'] = "My Classes";
			$data['display_back'] = "";

			$data['teacher_id'] = $teacher_id;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/class_teacher');
			$this->load->view('templates/foot');
		}

		//Datatables
		public function fetch_student_by_class_stream_and_subject(){
			$class_stream_id = $this->input->get('class_stream_id');
			$subject_id = $this->input->get('subject_id');
			//$cid = "C04E";
			$fetch_data = $this->student_model->make_datatables_for_select_students_by_class_and_subject($class_stream_id, $subject_id);
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				$sub_array[] = $row->admission_no;
				$sub_array[] = $row->firstname;
				$sub_array[] = $row->lastname;
				$sub_array[] = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.base_url() . 'students/get_student_profile/' . $row->admission_no.'" data-toggle="tooltip" data-placement="right" title="Results" class="fa fa-user"></a>';
				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->student_model->get_all_data_select_students_by_class_and_subject($class_stream_id, $subject_id),
				"recordsFiltered"	=>	$this->student_model->get_filtered_data_select_students_by_class_and_subject($class_stream_id, $subject_id),
				"data"				=>	$data
			);
			echo json_encode($output);
		}
		//End Datatables


		public function get_students_by_class_stream_and_subject($class_stream_id = NULL, $subject_id = NULL){
			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'teacher/myclasses/'.$class_stream_id.'/'.$subject_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_by_class_and_subject($class_stream_id, $subject_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['mystudents'] = $this->student_model->select_students_by_class_and_subject($class_stream_id, $subject_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
			

			$data['title'] = $this->breadCrumb("teacher/assigned_classes/".$this->session->userdata('staff_id'), "class_students", NULL, NULL, "My Classes", "Students - ".$this->class_model->select_class_name_and_stream($class_stream_id)['class_name'] . " " . $this->class_model->select_class_name_and_stream($class_stream_id)['stream'], NULL);


			$data['display_back'] = "";
			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $subject_id;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_students_by_class_and_subject');
			$this->load->view('templates/foot');

		}

		public function add_score($teacher_id){
			$allowed = FALSE;
			if($this->session->userdata('add_score_of_own_subject') == 'ok' && $this->session->userdata('staff_id') === $teacher_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			
			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'teachers/add_score/'.$teacher_id.'/'.$value;
			$config['total_rows'] = $this->teacher_model->count_teachers_teaching_assingment($teacher_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['myclasses'] = $this->teacher_model->select_teachers_teaching_assingment($teacher_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['teacher_id'] = $teacher_id;
			$data['title'] = "Create Scoresheet";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/add_score');
			$this->load->view('templates/foot');
		}

		
		public function school_attendance_report($date = NULL){
			if(!($this->session->userdata('view_school_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('date') != ""){
				$date = $this->input->post('date');
				$count_search_records = $this->teacher_model->test_if_empty_record($date)['count'];
				if($count_search_records == 0){
					$this->session->set_flashdata('att_record_message', 'No records found for your search');
				}
			}
			else{
				$date = date('Y-m-d');
				$count_records = $this->teacher_model->test_if_empty_record($date)['count'];
				if($count_records == 0){
					$this->session->set_flashdata('att_record_message', 'Search for the report by entering the date above');
				}
			}

			$data['title'] = $this->breadCrumb("teachers/school_attendance_report", NULL, NULL, NULL, "School Attendance: " . $date, NULL, NULL);
			
			$data['school_attendance_records'] = $this->teacher_model->create_attendance_report_summary($date);

			$data['att_records'] = ($this->input->post('date') != "") ? $count_search_records : $count_records;

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/school_attendance_report');
			$this->load->view('templates/foot');
		}

		public function individual_timetable($teacher_id){
			$allowed = FALSE;
			if($this->session->userdata('view_personal_timetable') == 'ok' && $this->session->userdata('staff_id') === $teacher_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['title'] = $this->breadCrumb("teacher/mytimetable".$teacher_id, NULL, NULL, NULL, "Personal Timetable", NULL, NULL);
			
			$data['myclasses_title'] = "Classes Assigned";
			$data['teacher_periods'] = $this->class_model->select_no_of_periods_by_teacher_id($teacher_id);
			$data['timetable_records'] = $this->teacher_model->create_individual_timetable($teacher_id);
			
			$data['class_streams'] = $this->teacher_model->select_classes_assigned($teacher_id);


			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/personal_timetable');
			$this->load->view('templates/foot');
		}

		//PANGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

		   // display daily routine @den//
		public function daily_routine($teacher_id = NULL){
			if(!($this->session->userdata('current_tod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

           	if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

		  	$data['staff'] = $this->teacher_model->check_tod($teacher_id);
		  	$data['teacher_id'] = $teacher_id;
		  	
		  	if($this->form_validation->run('add_tod_routine') == FALSE){
			  	$this->load->view('templates/teacher_header', $data);
			  	$this->load->view('teachers/tod_header_@den',$data);
				$this->load->view('teachers/tod_daily_routine_@den');
				$this->load->view('templates/foot');
			}
			else{
				$this->save_daily_routine();
			}
		}


		  // save teacther daily routine @den//
 
       	public function save_daily_routine(){
       		if(!($this->session->userdata('current_tod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

       		if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$wakeup = $this->input->post('wakeup');
			$parade = $this->input->post('parade');
			$class = $this->input->post('class');
			$breakfast = $this->input->post('break');
			$lunch = $this->input->post('lunch');
			$dinner = $this->input->post('dinner');
			$events = $this->input->post('events');
			$security = $this->input->post('security');
			$did = $this->input->post('id');
			$date = $this->input->post('date');

       	   	$records=array(
				'date_' => $date,
				'wake_up_time' => $wakeup,
				'parade_time' => $parade,
				'start_classes_time' => $class,
				'breakfast' => $breakfast,
				'lunch' => $lunch,
				'dinner' => $dinner,
				'events' => $events,
				'security' => $security,
				'did' => $did //duty id for this tod//
			);

			//$current_date = date('Y-m-d');
       	   
       		$rs = $this->teacher_model->save_daily_routine($records, $date, $did);
       		if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
       	   	redirect('teachers/daily_routine/'.$did);
       	}     // save teacther daily routine @den//

        // display daily routine @den//
		public function display_week_routine($tod = NULL){
            if(!($this->session->userdata('current_tod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

            if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

		  	$data['daily'] = $this->teacher_model->daily_routine($tod);
		    $data['staff'] = $this->teacher_model->check_tod($tod);

		  	$this->load->view('templates/teacher_header', $data);
		    $this->load->view('teachers/tod_header_@den', $data);
			$this->load->view('teachers/week_routine_@den');
			$this->load->view('templates/foot');
		}

		// display daily routine @den//
		public function today_routine_report($tod = NULL, $date = NULL){
			if(!($this->session->userdata('current_tod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

 			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

		  	$data['daily']=$this->teacher_model->today_report($tod, $date);
		    $data['staff']=$this->teacher_model->check_tod($tod);

		  	$this->load->view('templates/teacher_header', $data);
		    $this->load->view('teachers/tod_header_@den',$data);
			$this->load->view('teachers/tod_today_report_@den');
			$this->load->view('templates/foot');
		}

        //display this week comments den@//
        public function display_week_comments($tod = NULL){
        	if(!($this->session->userdata('current_tod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

          	if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

		  	$data['daily'] = $this->teacher_model->daily_routine($tod);
		    $data['staff'] = $this->teacher_model->check_tod($tod);

		  	$this->load->view('templates/teacher_header', $data);
		    $this->load->view('teachers/tod_header_@den',$data);
			$this->load->view('teachers/tod_head_comments_@den');
			$this->load->view('templates/foot');
		}

		// display info to h/m @den//
		public function today_routine_report_to($date){
			//WHAT IS THIS FUNCTION DOING EXACTLY
			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['daily'] = $this->teacher_model->today_report_to($date);

			$this->load->view('templates/teacher_header', $data);
			/*$this->load->view('teachers/tod_header_@den',$data);*/
			$this->load->view('teachers/head_duty_comments_@den');
			$this->load->view('templates/foot');
		}

		   // display daily tod to h/m @den//
		public function head_tod_info(){
			if(!($this->session->userdata('manage_tod_reports') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['title'] = "Weekly tod report";
			$data['staff'] = $this->teacher_model->tod_details();
			$data['routine_records'] = $this->teacher_model->head_tod_info();
			$data['week'] = $this->teacher_model->week_id();

			$data['duty_roaster_id']=$this->teacher_model->week_id()['id'];
			//$data['comments'] = $this->teacher_model->select_comments_by_date($date);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/head_master_tod_header');
			$this->load->view('teachers/head_tod_week_view_@den');
			$this->load->view('templates/foot');
		}

		// display report to h/m
		public function report_head_tod_info($date){
			if(!($this->session->userdata('view_tod_report') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['title']="Weekly tod report";
		  	$data['day'] = $this->teacher_model->add_day($date)['day'];
		  	$data['date'] = $date;

			$data['staff']=$this->teacher_model->tod_details();
			$data['daily']=$this->teacher_model->report_head_tod_info($date);
			$data['duty_roaster_id']=$this->teacher_model->week_id()['id'];

			$data['modalID'] = explode('-', $data['date'])[0] . explode('-', $data['date'])[1] . explode('-', $data['date'])[2];

			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/head_master_tod_header');
			$this->load->view('teachers/report_head_daily_view_@den');
			$this->load->view('templates/foot');
		}

		public function view_week($id){
			if(!($this->session->userdata('manage_tod_reports') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$data['title'] = "Weekly tod report";
			//$data['staff'] = $this->teacher_model->tod_details_all();
			$data['routine_records'] = $this->teacher_model->head_tod_info_all($id);
			//$data['week'] = $this->teacher_model->week_id();

			$data['duty_roaster_id'] = $id;
			//$data['comments'] = $this->teacher_model->select_comments_by_date($date);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/head_master_tod_header');
			$this->load->view('teachers/head_tod_week_view_@den');
			$this->load->view('templates/foot');
		}

		//add comments den@//
		public function add_comment($date){
			if(!($this->session->userdata('manage_tod_reports') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}


			$sm = trim($this->input->post('msg'));
			
			$sms = array('comments' => $sm);

			$rs = $this->teacher_model->save_comment($date,$sms);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
			redirect('teachers/report_head_tod_info/'.$date);
		}

   		//week comments den@//
		public function this_week_tods($id = NULL){
			if(!($this->session->userdata('view_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

		 	if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

		 	$data['title'] = "This Week TODs";
		    $data['teacher_records'] = $this->teacher_model->select_week_tods($id);
		    $data['duty_roaster_id'] = $id;
		    
		  	$this->load->view('templates/teacher_header', $data);
		  	$this->load->view('teachers/head_master_tod_header');
		    $this->load->view('teachers/this_week_tods');
			$this->load->view('templates/foot');

		}	

        //delete comments den@//
		public function delete_comment($date){
			if(!($this->session->userdata('manage_tod_reports') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}
			
			$rs = $this->teacher_model->delete_comment($date);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
		  	redirect('teachers/all_comments');
		}

		//derick 
		//Tods management

		public function assign_new_tod(){
			if(!($this->session->userdata('manage_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Assign TOD of the Week";
			$data['teacher_names'] = $this->teacher_model->get_all_teachers();

			$teacher_id = $this->input->post('t_id');
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');

			$range = (strtotime(''.$end_date.'') - strtotime(''.$start_date.''))/(24*60*60);

			if(date("D", strtotime(''.$start_date.'')) === "Mon" && date("D", strtotime(''.$end_date.'')) === "Sun" && $range === 6){
				if($this->form_validation->run('assign_tod') == FALSE){
					$this->load->view('templates/teacher_header', $data);
					$this->load->view('teachers/new_tod');
					$this->load->view('templates/foot');
				}
				else{
					$record = array(
						/*'teacher_id' => $teacher_id,*/
						'start_date' => $start_date,
						'end_date' => $end_date
					);
				

					$rs = $this->teacher_model->add_tods($record, $teacher_id);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect(base_url('teachers/assign_new_tod') );
				}
			}
			else{
				if(!$this->session->flashdata('success_message') && $this->input->post('assign_tod')){
					$this->session->set_flashdata('error_message', 'The Date range is invalid');
				}
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('teachers/new_tod');
				$this->load->view('templates/foot');
				//redirect('teachers/assign_new_tod');
			}
		}


		public function teachers_duty_roaster(){
			if(!($this->session->userdata('view_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if ($this->input->post('search_roaster') != "") {
				$value = $this->input->post('search_roaster');
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'teachers/teachers_duty_roaster/'.$value;
			$config['total_rows'] = $this->teacher_model->count_duty_roaster($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['duty_roaster_record'] = $this->teacher_model->select_teachers_duty_roaster($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
			
			$data['title'] = $this->breadCrumb("teachers_duty_roaster", NULL, NULL, NULL, "Teacher Duty Roster", NULL, NULL);

			//$data['duty_roaster_record'] = $this->teacher_model->select_teachers_duty_roaster();
			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			
			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/view_duty_roaster');
			$this->load->view('templates/foot');
		}

		public function edit_duty_roaster($id = NULL, $sid = NULL){
			if(!($this->session->userdata('manage_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Assign TOD of the Week";
			$data['record'] = $this->teacher_model->select_duty_roaster_record_by_id($id, $sid);
			$data['teacher_names'] = $this->teacher_model->get_all_teachers();

			$data['sid'] = $sid;

			$this->form_validation->set_rules('t_id', 'Teacher Name', 'required');

			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('teachers/edit_duty_roaster');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_duty_roaster();
			}
		}

		public function update_duty_roaster(){
			if(!($this->session->userdata('manage_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$new_teacher_id = $this->input->post('t_id');
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$id = $this->input->post('id');
			$old_teacher_id = $this->input->post('teacher_id');

			

			if($this->input->post('update_record')){
				$record = array(
					'teacher_id' => $new_teacher_id
				);

				$rs = $this->teacher_model->update_duty_roaster($old_teacher_id, $id, $record, $new_teacher_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect(base_url('teachers/teachers_duty_roaster'));
			}
		}

		public function set_current_duty_roaster($id){
			if(!($this->session->userdata('manage_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$record = array(
				'is_current' => "T"
			);

			$current_date = date('Y-m-d');

			$rs = $this->teacher_model->set_current_duty_roaster($id/*, $record, $current_date*/);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully set as current TOD");
			}

			redirect(base_url('teachers/teachers_duty_roaster'));
		}

		public function delete_duty_roaster($id){
			if(!($this->session->userdata('manage_tods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->teacher_model->delete_duty_roaster($id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect(base_url('teachers/teachers_duty_roaster'));
		}
	}
