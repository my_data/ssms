<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Periods extends MY_Controller{
		public function __construct(){
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}

			$this->load->library('form_validation');
			//$this->load->model('period_model');
			//$this->load->model('admin_model');

			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }
			//$this->errorAcademicYearSettings();
		}

		public function index(){
			if(!($this->session->userdata('view_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_period') != ""){
	            $value = trim($this->input->post('search_period'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = 0;
	        }

			if(! file_exists(APPPATH.'views/periods')){
				show_404();
			}

			$data['periods'] = $this->period_model->view_period();
			$data['breaks'] = $this->period_model->select_breaks();

			$data['title'] = $this->breadCrumb("periods", NULL, NULL, NULL, "All Periods", NULL, NULL);
			
			//print_r($data['periods']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('periods/view_periods');
			$this->load->view('templates/foot');
		}

		public function new_period(){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("periods/new_period", NULL, NULL, NULL, "Add Period", NULL, NULL);
			
			if($this->form_validation->run('add-edit_period') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('periods/new_period');
				$this->load->view('templates/foot');
			}
			else{
				if(strtotime($this->input->post('end_time')) > strtotime($this->input->post('start_time'))){
					$period_record = array(
						'period_no' => $this->input->post('period_no'),
						'start_time' => $this->input->post('start_time'),
						'end_time' => $this->input->post('end_time'),
						'duration' => (strtotime($this->input->post('end_time')) - strtotime($this->input->post('start_time')))/60
					);

					$rs = $this->period_model->insert_period($period_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect('periods');
				}
				else{
					$this->session->set_flashdata('error_message', "Start Time can not be less or equal to the End Time");
					redirect('periods');
				}
			}
		}

		public function edit($period_no = NULL){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['period_record'] = $this->period_model->view_period($period_no);

			if(! file_exists(APPPATH.'views/periods')){
				show_404();
			}

			$data['title'] = $this->breadCrumb("periods", "periods/edit_period", NULL, NULL, "All Periods", "Edit Period No: " . $period_no, NULL);

			$data['period_no'] = $period_no;

			if($this->form_validation->run('add-edit_period') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('periods/edit_period');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_period();
			}				
		}

		public function update_period(){
			if(strtotime($this->input->post('end_time')) > strtotime($this->input->post('start_time'))){
				$period_no = $this->input->post('period_no');
				$start_time = $this->input->post('start_time');
				$end_time = $this->input->post('end_time');
				$duration = (strtotime($this->input->post('end_time')) - strtotime($this->input->post('start_time')))/60;

				$period_record = array(
					'start_time' => $start_time,
					'end_time' => $end_time,
					'duration' => $duration
				);

				$rs = $this->period_model->update_period($period_no, $period_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('periods');
			}
			else{
				$this->session->set_flashdata('error_message', "Start Time can not be less or equal to the End Time");
				redirect('periods');
			}
		}

		public function delete($period_no){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->period_model->delete_period($period_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect(base_url() . 'periods');
		}

		public function new_break(){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['periods'] = $this->period_model->view_period();

			$data['title'] = $this->breadCrumb("periods", "periods/new_break", NULL, NULL, "All Periods", "Set Breaks", NULL);
			
			if($this->form_validation->run('add-edit_break') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('periods/new_break');
				$this->load->view('templates/foot');
			}
			else{
				$break_record = array(
					'after_period_no' => $this->input->post('after_period_no'),
					'description' => $this->input->post('description')
				);

				$rs = $this->period_model->insert_break($break_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
				redirect('periods');
			}
		}

		public function edit_break($bid = NULL){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['break_record'] = $this->period_model->select_break_by_id($bid);

			if(! file_exists(APPPATH.'views/periods')){
				show_404();
			}

			$data['title'] = "Edit Break";
			$data['bid'] = $bid;

			if($this->form_validation->run('add-edit_break') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('periods/edit_break');
				$this->load->view('templates/foot');
			}
			else{
				$break_record = array(
					'description' => $this->input->post('description')
				);

				$rs = $this->period_model->update_break($bid, $break_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('periods');
			}				
		}

		public function remove_break($bid){
			if(!($this->session->userdata('manage_periods') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$rs = $this->period_model->break_period($bid);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect(base_url() . 'periods');
		}
	}
