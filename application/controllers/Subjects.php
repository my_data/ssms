<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Subjects extends MY_Controller{
		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');
			//$this->load->model('subject_model');
			//$this->load->model('student_model');
			//$this->load->model('class_model');
			//$this->load->model('department_model');
			$this->load->library('form_validation');			
			$this->load->library('pagination');

			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }			//$this->errorAcademicYearSettings();
		}

		public function addLog($csid = NULL, $sub_id = NULL){
                        if(!($this->session->userdata('manage_teaching_log') == 'ok')){
                                $this->authenticate_user_permission('home', FALSE);
                        }

                        $this->authenticate_assigned_subjects($csid, $sub_id, 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));

			//echo $csid . ", " . $sub_id;

			$data['topics'] = $this->subject_model->get_topics_by_class_and_subject($csid, $sub_id);
			$data['title'] = "Add Log";			

			if(! file_exists(APPPATH.'views/subjects')){
                                show_404();
                        }

			$data['csid'] = $csid;
			$data['sub_id'] = $sub_id;
  
			
			$this->load->view('templates/teacher_header', $data);
                        $this->load->view('subjects/addLog');
                        $this->load->view('templates/foot');
		}
		
		public function fetch_subject(){
			$fetch_data = $this->subject_model->make_datatables();
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				$sub_array[] = $row->subject_id;
				$sub_array[] = $row->subject_name;
				$sub_array[] = '<button type="button" name="update" id="'.$row->subject_id.'" class="btn btn-primary btn-xs" >Edit</button>';
				$sub_array[] = '<button type="button" name="delete" id="'.$row->subject_id.'" class="btn btn-danger btn-xs" >Delete</button>';

				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->subject_model->get_all_data(),
				"recordsFiltered"	=>	$this->subject_model->get_filtered_data(),
				"data"				=>	$data
			);
			echo json_encode($output);
		}

		//START SUBJECTS
		public function index(){
			if(!($this->session->userdata('view_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_subject') != ""){
	            $value = trim($this->input->post('search_subject'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'subjects/index/'.$value;
			$config['total_rows'] = $this->admin_model->count_subjects($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['subjects'] = $this->admin_model->fetch_subjects($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Subjects";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			$data['display_back'] = "NO";

			//Special for modal
			$data['subject_teachers_record'] = $this->subject_model->select_subject_teachers();

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/view_subjects');
			$this->load->view('templates/foot');
			
		}

		public function edit_subject($subject_id){
			if(!($this->session->userdata('manage_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Edit: " . $this->admin_model->select_subject_by_id($subject_id)['subject_name'];
			$data['subject'] = $this->admin_model->select_subject_by_id($subject_id);
			$data['departments'] = $this->department_model->view_all_depts();

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			$data['subject_id'] = $subject_id;

			if($this->form_validation->run('add-edit_subject') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('subjects/edit_subject');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_subject();
			}
		}

		public function update_subject(){
			if(!($this->session->userdata('manage_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_category = $this->input->post('subject_category');
			$subject_name = $this->input->post('subject_name');
			$department_id = $this->input->post('department_id');
			$subject_choice = $this->input->post('subject_choice');
			$subject_id = $this->input->post('subject_id');
			$studied_by = $this->input->post('studied_by');
			$subject_type = $this->input->post('subject_type');

			if($this->input->post('update_subject')){
				$subject_record = array(
					'subject_choice' => $subject_choice,
					'subject_name' => $subject_name,
					'dept_id' => $department_id,
					'studied_by' => $studied_by,
					'subject_category' => $subject_category,
					'subject_type' => $subject_type
				);

				$rs = $this->admin_model->update_subject($subject_id, $subject_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect(base_url() . 'subjects');
			}
		}

		public function add_new_subject(){
			if(!($this->session->userdata('manage_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "New Subject";
			$data['departments'] = $this->department_model->view_all_depts();

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			if($this->form_validation->run('add-edit_subject') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('subjects/new_subject');
				$this->load->view('templates/foot');
			}
			else{
				$subject_category = $this->input->post('subject_category');
				$department_id = $this->input->post('department_id');
				$subject_choice = $this->input->post('subject_choice');
				$subject_name = $this->input->post('subject_name');	
				$studied_by = $this->input->post('studied_by');
				$subject_id = $this->input->post('subject_id');
				$subject_type = $this->input->post('subject_type');
				
				//Adding into array for posting to model
				$subject_record = array(
					'subject_id' => $subject_id,
					'subject_choice' => $subject_choice,
					'subject_name' => $subject_name,
					'dept_id' => $department_id,
					'subject_category' => $subject_category,
					'studied_by' => $studied_by,
					'subject_type' => $subject_type
				);

				$rs = $this->admin_model->insert_new_subject($subject_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
				redirect('subjects/add_new_subject');
			}
		}

		public function delete_subject($subject_id){
			if(!($this->session->userdata('manage_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_subject($subject_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect(base_url() . 'subjects');
		}
		//END SUBJECTS


		public function get_subject_teacher_by_class($class_stream_id){
			$allowed = FALSE;
			if($this->session->userdata('user_role') == 'Admin' || ($this->session->userdata('view_assigned_teachers_in_own_class') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$data['class_stream_id'] = $class_stream_id;

			$config = array();
			$config['base_url'] = base_url() . 'myclass/subject_teachers/'.$class_stream_id.'/'.$value;
			$config['total_rows'] = $this->subject_model->count_subject_teacher_by_class($class_stream_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			//$page = $this->uri->segment(4);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['teachers'] = $this->subject_model->select_subject_teacher_by_class($class_stream_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
			//$data['title'] = "List of Teachers teaching : " . $class_stream_id;

			$data['title'] = $this->breadCrumb("myclass/subject_teachers/".$class_stream_id, NULL, NULL, NULL, "Teachers Assigned", NULL, NULL);
			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('teachers/class_subject_teacher');
			$this->load->view('templates/foot');
		}

		public function topics($class_stream_id = NULL, $subject_id = NULL){
			$data['title'] = $this->breadCrumb("teacher/assigned_classes/".$this->session->userdata('staff_id'), "mytopics", NULL, NULL, "My Classes", "Topics - ".$this->subject_model->select_from_subjects($subject_id)['subject_name'], NULL);

			if(!($this->session->userdata('view_topics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));

			if($this->input->post('search_topic') != ""){
	            $value = trim($this->input->post('search_topic'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'subjects/topics/'.$class_stream_id.'/'.$subject_id.'/'.$value;
			$config['total_rows'] = $this->subject_model->count_all_topics_per_class_and_subject($class_stream_id, $subject_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			//$page = $this->uri->segment(3);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['topics'] = $this->subject_model->select_topics_by_class($class_stream_id, $subject_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
			//$data['title'] = "Topics : " . $class_stream_id . "	" . $subject_id;
			$data['display_back'] = "";

			//$data['topics'] = $this->subject_model->select_topics_by_class($class_stream_id, $subject_id);

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}


			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $subject_id;
			$data['subject_name'] = $this->subject_model->select_from_subjects($subject_id)['subject_name'];

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/view_topics_by_class_stream');
			$this->load->view('templates/foot');
		}

		public function add_new_topic(){
			if(!($this->session->userdata('manage_topics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$topic_name = $this->input->post('topic_name');

			if($this->form_validation->run('add-edit-topic') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('subjects/topics/'.$class_stream_id.'/'.$subject_id);
			}
			else{
				$topic_record = array(
					'topic_name' => $topic_name,
					'class_stream_id' => $class_stream_id,
					'subject_id' => $subject_id
				);

				$rs = $this->subject_model->insert_new_topic($topic_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("A new topic has been added");
				}
				redirect('subjects/topics/'.$class_stream_id.'/'.$subject_id.'');
			}
		}

		public function update_topic($topic_id){
			if(!($this->session->userdata('manage_topics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');				//THIS VALUES ARE FOR
			$class_stream_id = $this->input->post('class_stream_id');	//REDIRECTION

			$topic_name = $this->input->post('topic_name');

			if($this->form_validation->run('add-edit-topic') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('subjects/topics/'.$class_stream_id.'/'.$subject_id);
			}
			else{
				$topic_record = array(
					'topic_name' => $topic_name
				);

				$rs = $this->subject_model->update_topic($topic_id, $topic_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('subjects/topics/'.$class_stream_id.'/'.$subject_id.'');	//THIMK OF A BETTER SOLUTION
			}
		}

		public function delete_topic($topic_id){
			if(!($this->session->userdata('manage_topics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');				//THIS VALUES ARE FOR
			$class_stream_id = $this->input->post('class_stream_id');	//REDIRECTION

			$rs = $this->subject_model->delete_topic($topic_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('subjects/topics/'.$class_stream_id.'/'.$subject_id.'');	//THIMK OF A BETTER SOLUTION
		}


		//SUBTOPICS
		public function subtopics($class_stream_id = NULL, $topic_id = NULL){			
			if(!($this->session->userdata('view_subtopics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			//Get subject_id na class_stream_id ili kujua kama ame_assign_iwa kufundisha hilo somo
			$subject_id = $this->subject_model->select_class_stream_and_subject($topic_id)['subject_id'];
			//$class_stream_id = $this->subject_model->select_class_stream_and_subject($topic_id)['class_stream_id'];

			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));

			if($this->input->post('search_subtopic') != ""){
	            $value = trim($this->input->post('search_subtopic'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'subjects/subtopics/'.$class_stream_id.'/'.$topic_id.'/'.$value;
			$config['total_rows'] = $this->subject_model->count_all_subtopic_by_topic($class_stream_id, $topic_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			//$page = $this->uri->segment(3);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['subtopics'] = $this->subject_model->select_subtopics_by_topic($class_stream_id, $topic_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			//$data['title'] = "Sub Topics : " . $topic_id;
			$data['title'] = $this->breadCrumb("teacher/assigned_classes/".$this->session->userdata('staff_id'), "subjects/topics/".$class_stream_id.'/'.$subject_id.'', "subtopics", NULL, "My Classes", "Topics", "Subtopics - ".$this->subject_model->select_subject_by_topic_id($topic_id)['subject_name']);

			$data['display_back'] = "";

			//$data['subtopics'] = $this->subject_model->select_subtopics_by_topic($class_stream_id, $topic_id);
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $this->subject_model->select_subject_by_topic_id($topic_id)['subject_id'];
			$data['topic_name'] = $this->subject_model->select_subject_by_topic_id($topic_id)['topic_name'];
			$data['topic_id'] = $topic_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/view_subtopics_by_topics');
			$this->load->view('templates/foot');
		}

		public function add_new_subtopic(){
			if(!($this->session->userdata('manage_subtopics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$topic_id = $this->input->post('topic_id');
			$subtopic_name = $this->input->post('subtopic_name');

			if($this->form_validation->run('add-edit-subtopic') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('subjects/subtopics/'.$class_stream_id.'/'.$topic_id);
			}
			else{
				$subtopic_record = array(
					'subtopic_name' => $subtopic_name,
					'class_stream_id' => $class_stream_id,
					'subject_id' => $subject_id,
					'topic_id' => $topic_id
				);

				$rs = $this->subject_model->insert_new_subtopic($subtopic_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("New subtopic has been added");
				}
				redirect('subjects/subtopics/'.$class_stream_id.'/'.$topic_id.'');
			}
		}

		public function update_subtopic($subtopic_id){
			if(!($this->session->userdata('manage_subtopics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');				//THIS VALUES ARE FOR
			$class_stream_id = $this->input->post('class_stream_id');	//REDIRECTION

			$topic_id = $this->input->post('topic_id');
			$subtopic_name = $this->input->post('subtopic_name');


			if($this->form_validation->run('add-edit-subtopic') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('subjects/subtopics/'.$class_stream_id.'/'.$topic_id);
			}
			else{
				$subtopic_record = array(
					'subtopic_name' => $subtopic_name
				);

				$rs = $this->subject_model->update_subtopic($subtopic_id, $subtopic_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('subjects/subtopics/'.$class_stream_id.'/'.$topic_id.'');	
			}
		}

		public function delete_subtopic($subtopic_id){
			if(!($this->session->userdata('manage_subtopics') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$topic_id = $this->input->post('topic_id');				//THIS VALUES ARE FOR
			$class_stream_id = $this->input->post('class_stream_id');	//REDIRECTION

			$rs = $this->subject_model->delete_subtopic($subtopic_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('subjects/subtopics/'.$class_stream_id.'/'.$topic_id.'');	//THIMK OF A BETTER SOLUTION
		}


		//START ASSIGNING SUBJECT TO CLASS STREAM CODE
		public function assign_subject_to_class(){
			if(!($this->session->userdata('manage_class_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Assign Subject";
			$data['classes'] = $this->class_model->select_class_level(0);
			$data['streams'] = $this->class_model->select_stream();
			$data['subjects'] = $this->subject_model->select_from_subjects();

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			if($this->form_validation->run('assign_subject_to_class_stream') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('subjects/assign_subject');
				$this->load->view('templates/foot');
			}
			else{
				$class_id = $this->input->post('class_id');
				$stream_id = $this->input->post('stream_id');
				$data['subject_id'] = $this->input->post('subject_id');
				
				$class_stream_id = $class_id . $stream_id;				

				foreach ($data['subject_id'] as $key => $value) {

					$assign_subject_record[] = array(
						'class_stream_id' => $class_stream_id,
						'subject_id' => $value
					);
				}
				
				$x = 0;
				if(is_array($data['subject_id'])){
					$x = count($data['subject_id']); 
				}
				$nos = $this->subject_model->get_nos_for_class($class_stream_id);
				
				if($nos == $x){
					$rs = $this->admin_model->insert_assign_subject($assign_subject_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}	
					redirect('subjects/class_stream_subjects');
				}
				else{
					$this->session->set_flashdata('error_message', 'Error, the total count of subjects do not correspond with the number of subjects of this class');
					redirect('subjects/class_stream_subjects'); 
				}
			}
		}

		public function class_stream_subjects(){
			if(!($this->session->userdata('view_class_stream_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_class') != ""){
	            $value = trim($this->input->post('search_class'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'subjects/class_stream_subjects/'.$value;
			$config['total_rows'] = $this->admin_model->count_class_stream_subjects($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['class_stream_subjects'] = $this->admin_model->fetch_class_stream_subjects($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Class Stream Subjects";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			$data['display_back'] = "NO";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/view_class_stream_subjects');
			$this->load->view('templates/foot');
		}

		public function edit_assigned_subject($class_stream_id){
			if(!($this->session->userdata('manage_class_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['class_stream_subject'] = $this->admin_model->select_class_stream_subject_by_class_stream_id($class_stream_id);

			$data['class_stream_subjects'] = $this->admin_model->select_class_stream_subjects_by_class_stream_id($class_stream_id);

			$data['title'] = "Edit: " . $data['class_stream_subject']['class_name'] . " " .$data['class_stream_subject']['stream'];
			$data['classes'] = $this->class_model->select_class_level(0);
			$data['streams'] = $this->class_model->select_stream();
			$data['subjects'] = $this->subject_model->select_from_subjects();

			if(! file_exists(APPPATH.'views/subjects')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;

			if($this->form_validation->run('update_assign_subject_to_class_stream') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('subjects/edit_assigned_subject');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_class_stream_subject();
			}
		}

		public function update_class_stream_subject(){
			if(!($this->session->userdata('manage_class_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('update_assigned_subject')){
				$class_stream_id = $this->input->post('class_stream_id');
				$data['subject_id'] = $this->input->post('subject_id');
				
							

				foreach ($data['subject_id'] as $key => $value) {

					$assign_subject_record[] = array(
						'class_stream_id' => $class_stream_id,
						'subject_id' => $value
					);
				}

				$x = 0;
				if(is_array($data['subject_id'])){
					$x = count($data['subject_id']);
				}
				$y = $this->subject_model->get_nos_for_class($class_stream_id);

				if($x == $y){
					$rs = $this->admin_model->update_assigned_subject($class_stream_id, $assign_subject_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect('subjects/class_stream_subjects');

				}
				else{
					$this->session->set_flashdata('error_message', 'Error, the total count of subjects do not correspong th the number of subjects for this class');
					redirect('subjects/edit_assigned_subject/'.$class_stream_id);
				}
			}
		}

		public function delete_assigned_subject($class_stream_id){
			if(!($this->session->userdata('manage_class_subjects') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_class_stream_subject($class_stream_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('subjects/class_stream_subjects');
		}

		public function log_topic(){
			$teacher_id = $this->input->post('teacher_id');
			$subject_id = $this->input->post('subject_id');
			$topic_id = $this->input->post('topic_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$start_date = $this->input->post('start_date');
			//$end_date = $this->input->post('end_date');
			$term_id = $this->student_model->get_current_term_id_by_class_stream_id($class_stream_id)['term_id'];

			if($this->form_validation->run('add_log') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect(base_url() . 'teacher/assigned_classes/'.$teacher_id);
			}
			else{
				if($start_date >= date('Y-m-d')):
					$log_record = array(
						'teacher_id' => $teacher_id,
						'subject_id' => $subject_id,
						'topic_id' => $topic_id,
						'class_stream_id' => $class_stream_id,
						'start_date' => $start_date,
						'dept_id' => $this->subject_model->select_department_by_subject($subject_id),
						'term_id' => $term_id 
					);

					$rs = $this->subject_model->insert_teaching_log($log_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect(base_url() . 'subjects/get_log_by_teacher_id/'.$teacher_id);
				else:
					$this->session->set_flashdata('error_message', "Error, invalid date. The start date should NOT be past today's date");
					redirect(base_url() . 'subjects/get_log_by_teacher_id/'.$teacher_id);
				endif;
			}
		}




		//************CHANGES**************************
		public function edit_log($log_id = NULL){

			 if(!($this->session->userdata('manage_teaching_log') == 'ok')){
                                $this->authenticate_user_permission('home', FALSE);
                        }

			$data['title'] = "Edit Log";
			$data['log_record'] = $this->subject_model->select_all_logs($log_id);
			$data['topics'] = $this->subject_model->select_topics();

			$this->authenticate_assigned_subjects($data['log_record']['class_stream_id'], $data['log_record']['subject_id'], 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));
			
			if($this->form_validation->run('add_log') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('subjects/edit_log');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_start_and_topic_in_log($log_id);
			}
		}


		public function update_start_and_topic_in_log($log_id){
			$teacher_id = $this->input->post('teacher_id');
			$subject_id = $this->input->post('subject_id');
			$topic_id = $this->input->post('topic_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$old_start_date = $this->input->post('old_start_date');
			$term_id = $this->student_model->get_current_term_id_by_class_stream_id($class_stream_id)['term_id'];
//echo $end_date; exit;
			if($start_date >= $old_start_date && ($end_date == "" || $start_date <= $end_date)):
				$log_record = array(
					'topic_id' => $topic_id,
					'start_date' => $start_date
				);

				//print_r($log_record); exit;

				$rs = $this->subject_model->update_start_and_topic_in_log($log_record, $log_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
			}		
				redirect(base_url() . 'subjects/get_log_by_teacher_id/'.$teacher_id);		
			else:
					$this->session->set_flashdata('error_message', "Error, invalid date. The start date should be the same as the existing start_date or beyond the already existing start date, and make sure the start date is greater than end date");
					redirect(base_url() . 'subjects/get_log_by_teacher_id/'.$teacher_id);
			endif;

		}

		public function get_log_by_teacher_id($teacher_id = NULL){		
			$allowed = FALSE;
                        if($this->session->userdata('manage_teaching_log') == 'ok' && $this->session->userdata('staff_id') === $teacher_id){
                                $allowed = TRUE;
                        }

                        $this->authenticate_user_permission('home', $allowed);

			/*echo "<pre>";
			print_r($data['subtopics']);
			echo "</pre>";*/
			if ($this->input->post('search_log') != "") {
				$value = $this->input->post('search_log');
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'subjects/get_log_by_teacher_id/'.$teacher_id.'/'.$value;
			$config['total_rows'] = $this->subject_model->count_logs_by_tid($teacher_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['logs'] = $this->subject_model->get_log_by_teacher_id($teacher_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['subtopics'] = $this->subject_model->retrieve_subtopics();
			//$data['topics'] = $this->subject_model->select_topics();
			//$data['all_logs'] = $this->subject_model->select_all_logs();


			$data['title'] = "My Logs";
			$data['p'] = $page;
			$data['display_back'] = "";

			$data['teacher_id'] = $teacher_id;
			$data['dept_id'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/logs');
			$this->load->view('templates/foot');
		}

		public function add_log_comment($log_id){
			 if(!($this->session->userdata('manage_teaching_log') == 'ok')){
                                $this->authenticate_user_permission('home', FALSE);
                        }

                       //$this->authenticate_assigned_subjects($csid, $sub_id, 'teacher/assigned_classes/'.$this->session->userdata('staff_id'));


			if($this->form_validation->run('add_topic_end_date') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
			}
			else{
				if($this->input->post('start_date') <= $this->input->post('end_date')){
					$record = array(
						'comment' => $this->input->post('comment'),
						'end_date' => $this->input->post('end_date')
					);

					$rs = $this->subject_model->add_log_comment($log_id, $record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
				}
				else{
					$this->session->set_flashdata('error_message', 'Error, invalid date. The end date should be beyond the start date');
				}			
			}
			redirect(base_url() . 'subjects/get_log_by_teacher_id/'.$this->input->post('staff_id'));
		}


		public function subject_teaching_logs($dept_id = NULL){
			 $allowed = FALSE;
                        if($this->session->userdata('view_teaching_logs') == 'ok' || ($this->session->userdata('view_teaching_log_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
                                $allowed = TRUE;
                        }

                        $this->authenticate_user_permission('home', $allowed);

			if ($this->input->post('search_log') != "") {
				$value = $this->input->post('search_log');
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'subjects/subject_teaching_logs/'.$dept_id.'/'.$value;
			$config['total_rows'] = $this->subject_model->count_logs_by_dept($dept_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['logs'] = $this->subject_model->get_logs($dept_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['subtopics'] = $this->subject_model->retrieve_subtopics();
			//$data['topics'] = $this->subject_model->select_topics();
			//$data['all_logs'] = $this->subject_model->select_all_logs();


			$data['title'] = $this->breadCrumb("departments", "subject_teaching_logs/".$dept_id, NULL, NULL, "All Departments", "".$this->department_model->select_department_dept_id($dept_id)['dept_name']."  Department Teaching Logs", NULL);
			
			$data['p'] = $page;
			$data['display_back'] = "";

			$data['dept_id'] = $dept_id;
			$data['teacher_id'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('subjects/logs');
			$this->load->view('templates/foot');
		}

		public function approve($log_id, $dept_id){
			 $allowed = FALSE;
                        if($this->session->userdata('view_teaching_log_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id){
                                $allowed = TRUE;
                        }

                        $this->authenticate_user_permission('home', $allowed);

			$comment = $this->subject_model->select_all_logs($log_id)['comment'];
			$approved = $this->subject_model->select_all_logs($log_id)['approved'];
			if($comment == ""){
				$this->session->set_flashdata('error_message', 'Not allowed to approve while the topic is yet to be completed');
			}
			else{
				if($approved == "yes"){
					$this->session->set_flashdata('error_message', 'You have already approved this log');
				}
				else{
					$record = array(
						'approved' => 'yes'
					);
					$rs = $this->subject_model->approve_log($log_id, $record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully Approved");
					}
				}			
			}
			redirect(base_url() . 'subject_teaching_logs/'.$dept_id);
		}
	}
