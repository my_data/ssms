<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Login extends MY_Controller{
		protected $session_data = array();

		public function __construct(){
			parent:: __construct();
			$this->load->library('form_validation');
			//$this->load->model('admin_model');
			//$this->load->model('login_model');
			//$this->load->model('class_model');
			//$this->load->model('department_model');
			//$this->load->model('dormitory_model');
			//$this->load->model('notification_model');		
			//$this->load->model('teacher_model');	

			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }		
		}

		public function index(){
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if($this->form_validation->run()){
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$hashed_password = sha1($password);
				if($username === "Administrator"){					
					if($this->login_model->login_user($username, $hashed_password)){
						$time = date('Y-m-d h:i:s');	//For Last Login
						$this->login_model->last_login_admin($time, $this->login_model->login_user($username, $hashed_password)['id']);	//End For Last Login

						$session_data = array(
							'permanent_role' => $username,
							'staff_id' => $this->login_model->login_user($username, $hashed_password)['id'],
							'user_role' => $this->login_model->login_user($username, $hashed_password)['user_role'],
							'logged_in' => TRUE
						);
						$this->session->set_userdata($session_data);

						//NOTIFICATION SESSION
						$notification = $this->notification_model->count_notifications($this->login_model->login_user($username, $hashed_password)['id']);
						$this->session->set_userdata('notifications', $notification);

						$notification_messages = $this->notification_model->select_notifications($this->login_model->login_user($username, $hashed_password)['id']);
						$this->session->set_userdata('notification_messages', $notification_messages);
						//END NOTIFICATION SESSION

						//Modules Admin
						$modules = $this->login_model->select_admin_modules();
						$this->session->set_userdata('modules', $modules);
						//End Modules

						//Get All Modules And Staff Role Permissions Seen By Staff
						$roles_permission = $this->login_model->select_admin_permissions();
						$this->session->set_userdata('roles_permission', $roles_permission);

						//GET PAST ACADEMIC YEARS
						$session_past_years = $this->admin_model->select_all_academic_years("past_academic_year");
						$this->session->set_userdata("past_miaka", $session_past_years);
						//END OBTAINING PAST ACADEMIC YEARS

						redirect(base_url() . 'home');
					}
					else{
						$this->session->set_flashdata('error_message', 'Invalid Password, please try again');						
						redirect(base_url() . 'login');
					}
				}
				else{
					if($this->login_model->login_user($username, $hashed_password) && ($this->login_model->login_user($username, $hashed_password)['account_locked'] == "" || $this->login_model->login_user($username, $hashed_password)['account_locked'] == "NO")){
						$session_data = array(
							'phone_no' => $username,
							'firstname' => $this->login_model->login_user($username, $hashed_password)['firstname'],
							'lastname' => $this->login_model->login_user($username, $hashed_password)['lastname'],
							'permanent_role' => $this->login_model->login_user($username, $hashed_password)['staff_type'],
							'staff_id' => $this->login_model->login_user($username, $hashed_password)['staff_id'],
							'user_role' => $this->login_model->login_user($username, $hashed_password)['user_role'],
							'logged_in' => TRUE
						);
						$this->session->set_userdata($session_data);

						//Get class_stream array for a specific class teacher
						$class_stream_id = $this->class_model->select_class_stream_by_teacher_id($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('class_stream', $class_stream_id);
						//End grabbing class_stream_id array

						//Grab department_id
						$department_id = $this->department_model->get_department_id_by_hod($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('dept', $department_id);
						//End of daperment id

						//Grab dormitory id
						$dormitory_id = $this->dormitory_model->get_dorm_id_by_dorm_master_id($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('dormitory', $dormitory_id);
						//End dorm id

						//Grabbing subjects and classes assigned
						$classes_assigned_to_teach = $this->teacher_model->select_assigned_classes_by_teacher_id($this->login_model->login_user($username, $hashed_password)['staff_id']
						);
						$this->session->set_userdata('classes_assigned_to_teach', $classes_assigned_to_teach);
						//End subjects and classes assigned

						//Get All Modules And Staff Role Permissions Seen By Staff
						/*$roles_permission = $this->login_model->select_role_permissions_by_staff_id($this->login_model->login_user($username, $hashed_password)['staff_id']
						);
						$this->session->set_userdata('roles_permission', $roles_permission);*/

						$modules = $this->login_model->select_modules($this->login_model->login_user($username, $hashed_password)['staff_id']
						);
						$this->session->set_userdata('modules', $modules);
						//End Modules


						//Setting the user roles
						$user_role = $this->login_model->select_user_roles($this->login_model->login_user($username, $hashed_password)['staff_id']
						);
						$this->session->set_userdata('roles', $user_role);
						//End setting many roles

						//NOTIFICATION SESSION
						$notification = $this->notification_model->count_notifications($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('notifications', $notification);

						$notification_messages = $this->notification_model->select_notifications($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('notification_messages', $notification_messages);
						//END NOTIFICATION SESSION

						//ANNOUNCEMENT SESSION
						$announcements = $this->notification_model->count_announcements($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('announcements', $announcements);
						//ANNOUNCEMENT SESSION

						//TEACHING ASSIGNMENT COUNT
						$assigned_classes = $this->teacher_model->count_teaching_assignment($this->login_model->login_user($username, $hashed_password)['staff_id']);
						$this->session->set_userdata('teaching_classes', $assigned_classes);
						//END TEACHING ASSIGNMENT

						//GET ALL ACADEMIC YEARS
						$session_years = $this->admin_model->select_all_academic_years();
						$this->session->set_userdata("miaka", $session_years);

						//GET PAST ACADEMIC YEARS
						$session_past_years = $this->admin_model->select_all_academic_years("past_academic_year");
						$this->session->set_userdata("past_miaka", $session_past_years);
						//END OBTAINING PAST ACADEMIC YEARS

						//Special for class_teacher results
						$session_terms = $this->admin_model->select_term_name_by_academic_year($this->admin_model->select_academic_year_by_class_stream($this->class_model->select_class_stream_by_teacher_id($this->login_model->login_user($username, $hashed_password)['staff_id'])['class_stream_id'])['year']);
						$this->session->set_userdata("mihula", $session_terms);
						//END Special for class_teacher results
						//END GETTING ACADEMIC YEARS

						//ERASE record from lock_account_tbl
						$this->login_model->erase_record_from_lock_account_table($username);
						//END ERASE record from lock_account_tbl

						//Role of the staff table
						$staff_role = $this->login_model->login_user($username, $hashed_password)['user_role'];
						if($staff_role === "teacher" || $staff_role === "librarian" || $staff_role === "secretary" || $staff_role === "accountant"){
							//Get All Modules And Staff Role Permissions Seen By Staff
							$roles_permission = $this->login_model->select_role_permissions_by_staff_id($this->login_model->login_user($username, $hashed_password)['staff_id']);
							$this->session->set_userdata('roles_permission', $roles_permission);

							$time = date('Y-m-d h:i:s');	//For Last Login
							$this->login_model->update_past_login($time, $username);	//End For Last Login

							redirect(base_url() . 'home');
						}
					}
					else{
						//Get the username of the failed attempt
						$this->login_model->login_attempt_failed($username);
						if(!$this->session->flashdata('locked_message')){
							$this->session->set_flashdata('error_message', 'Invalid Username or Password');
						}
						else{
							$this->session->set_flashdata('error_message', $this->session->flashdata('locked_message'));
						}
						redirect(base_url() . 'login');
					}
				}
			}
			else{
				$this->login_form();
			}
		}

		public function logout(){
			$this->session->unset_userdata($session_data);
			$this->session->sess_destroy();
			redirect(base_url() . 'login');
		}


		public function login_form(){
			//NOT ALLOWED TO VIEW LOGIN PAGE BEFORE LOGGING OUT
			if($this->session->userdata('logged_in') == TRUE){				
				redirect('home');
			}
			$this->load->view('login_form');
		}
	}



//For API
/*
Twilio
https://www.programmableweb.com
*/
