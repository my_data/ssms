<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Admin extends MY_Controller{

		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');
			//$this->load->model('class_model');
			$this->load->library('form_validation');
			$this->load->library('pagination');
			//$this->load->model('department_model');
			//$this->load->model('dormitory_model');
			//$this->load->model('subject_model');
			//$this->load->model('staff_model');
			//$this->load->model('student_model');

			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }	
			//$this->errorAcademicYearSettings();
		}

		/*public function index(){
			$this->load->view('templates/admin_header');
			$this->load->view('admin/home');
			$this->load->view('templates/admin_footer');
		}*/
		public function change_password($id){
			$data['title'] = $this->breadCrumb("change_password/".$id, NULL, NULL, NULL, "Change Password", NULL, NULL);

			if(!($this->session->userdata('change_password') == 'ok') && $this->session->userdata('user_role') == "Admin"){
				$this->authenticate_user_permission('home', FALSE);
			}

			//$data['title'] = "Change Password";

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['id'] = $id;

			if($this->form_validation->run('change_password') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/change_password');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_password();
			}
		}

		public function update_password(){
			if(!($this->session->userdata('change_password') == 'ok') && $this->session->userdata('user_role') !== "Admin"){
				$this->authenticate_user_permission('home', FALSE);
			}

			$id = $this->input->post('id');
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password');
			$confirm_new_password = $this->input->post('confirm_new_password');

			$hashed_current_password = sha1($current_password);

			$query = $this->admin_model->select_current_password($id, $hashed_current_password);

			if($query === 1){
				$password_record = array(
					'password' => sha1($confirm_new_password)
				);

				$rs = $this->admin_model->update_password($id, $password_record);
				//$this->session->set_flashdata('success_message', 'Password successfully changed');
				if($rs){
					$this->transactionSuccessfullyCommitted("Password successfully changed");
				}
				redirect('admin/change_password/'.$id);
			}
			else{
				$this->session->set_flashdata('error_message', 'Invalid Current Password');
				redirect('admin/change_password/'.$id);
			}
		}

		public function audit(){
			if(!($this->session->userdata('view_audit_trail') == 'ok') && $this->session->userdata('user_role') !== "Admin"){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Audit Data";
			

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/audit');
			$this->load->view('templates/foot');
		}


		public function audit_student_results(){
			if(!($this->session->userdata('view_audit_trail') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/audit_student_results/'.$value;
			$config['total_rows'] = $this->admin_model->count_audit_student_results($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['audit_student_results_data'] = $this->admin_model->select_audit_student_results($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();


			$data['title'] = "Audit Data";
			$data['x'] = "student_results"; //Flag for if za kwenye auit_data view

			//$data['audit_staff_attendance_data'] = $this->admin_model->select_audit_data_staff_attendance();
			
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/audit_data');
			$this->load->view('templates/foot');
		}


		public function audit_student_attendance_data(){
			if(!($this->session->userdata('view_audit_trail') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/audit_student_attendance_data/'.$value;
			$config['total_rows'] = $this->admin_model->count_audit_student_attendance_data($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['audit_student_attendance_data'] = $this->admin_model->select_audit_data_student_attendance($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();


			$data['title'] = "Audit Data";
			$data['x'] = "student_attendance"; //Flag for if za kwenye auit_data view

			//$data['audit_staff_attendance_data'] = $this->admin_model->select_audit_data_staff_attendance();
			
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/audit_data');
			$this->load->view('templates/foot');
		}

		public function audit_staff_attendance_data(){
			if(!($this->session->userdata('view_audit_trail') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/audit_staff_attendance_data/'.$value;
			$config['total_rows'] = $this->admin_model->count_audit_staff_attendance_data($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['audit_staff_attendance_data'] = $this->admin_model->select_audit_data_staff_attendance($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();


			$data['title'] = "Audit Data";
			$data['x'] = "staff_attendance"; //Flag for if za kwenye auit_data view

			//$data['audit_staff_attendance_data'] = $this->admin_model->select_audit_data_staff_attendance();
			
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/audit_data');
			$this->load->view('templates/foot');
		}

		public function db_backup(){
			if(!($this->session->userdata('db_backup') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$this->database_backup();
			//redirect('');
		}

		public function update_student_details(){
			if(!($this->session->userdata('update_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$transferred = $this->input->post('transferred');
			$admission_no = $this->input->post('admission_no');
			$g_lastname = $this->input->post('g_lastname');
			$g_firstname = $this->input->post('g_firstname');
			$g_middlename = $this->input->post('g_middlename');
			$firstname = $this->input->post('firstname');
			$lastname = $this->input->post('lastname');
			$gender = $this->input->post('gender');
			$box = $this->input->post('box');
			$email = $this->input->post('email');
			$p_address = $this->input->post('p_address');
			$phone_no = $this->input->post('phone_no');
			$former_school = $this->input->post('former_school');
			$occupation = $this->input->post('occupation');
			$rel_type = $this->input->post('rel_type');
			$tribe_id = $this->input->post('tribe_id');
			$religion_id = $this->input->post('religion_id');
			$p_region = $this->input->post('p_region');
			$region = $this->input->post('region');
			$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year');

			$dob = $year . "-" . $month . "-" . $day;	//Concatenate for inserting dob

			$disabled = $this->input->post('disabled');

			if($disabled === "disabled"){
				$disabled = "yes";
				$disability = $this->input->post('disability');
			}
			else{
				$disability = "";
			}

			$student_record = array(
				'stte' => $transferred,
				'admission_no' => $admission_no,			
				'firstname' => $firstname,
				'lastname' => $lastname,
				'home_address' => $box,			
				'former_school' => $former_school,				
				'tribe_id' => $tribe_id,
				'religion_id' => $religion_id,			
				'region' => $region,
				'dob' => $dob,
				'disabled' => $disabled,
				'disability' => $disability
			);

			$guardian_record = array(
				'lastname' => $g_lastname,
				'firstname' => $g_firstname,
				'middlename' => $g_middlename,
				'email' => $email,
				'p_box' => $p_address,
				'phone_no' => $phone_no,
				'occupation' => $occupation,
				'rel_type' => $rel_type,
				'p_region' => $p_region,
				'gender' =>$gender
			);			

			$rs = $this->student_model->update_student_info($student_record, $guardian_record, $admission_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect(base_url() . 'admin/edit_student/'.$admission_no);
		}

		public function user_log_records($staff_id = NULL){
			if(!($this->session->userdata('view_staff_activity_log') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$config = array();
			$config['base_url'] = base_url() . 'admin/user_log_records/'.$staff_id;
			$config['total_rows'] = $this->admin_model->count_activity_log_records_by_user_id($staff_id);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = $this->uri->segment(4);
			$data['activity_log_records'] = $this->admin_model->fetch_activity_log_records_by_user_id($staff_id, $config['per_page'], $page);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Actitvity Log Records";
			$data['flag'] = "IS_SET";
			$data['user_id'] = $staff_id;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['display_back'] = "NO";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/view_activity_log_records');
			$this->load->view('templates/foot');
		}

		public function activity_log_records(){
			if(!($this->session->userdata('view_activity_log') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_log') != ""){
	            $value = trim($this->input->post('search_log'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/activity_log_records/'.$value;
			$config['total_rows'] = $this->admin_model->count_activity_log_records($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['activity_log_records'] = $this->admin_model->fetch_activity_log_records($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Actitvity Log Records";
			$data['flag'] = "NOT_SET";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['display_back'] = "NO";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/view_activity_log_records');
			$this->load->view('templates/foot');
		}

		//START GROUP TYPE
		public function groups(){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_group') != ""){
	            $value = trim($this->input->post('search_group'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/groups/'.$value;
			$config['total_rows'] = $this->admin_model->count_all_staff_groups($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['group_types'] = $this->admin_model->select_all_groups($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			//$data['title'] = "Groups";
			//$data['title'] = $this->breadCrumb("admin/groups", NULL, NULL, NULL, "Staff Groups",NULL, NULL);
			$data['title'] = $this->mainLink("#admin_panel", "Admin Settings")." | ".$this->breadCrumb("admin/groups", NULL, NULL, NULL, "Staff Groups",NULL, NULL);

			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/groups');
			$this->load->view('templates/foot');
		}

		public function add_new_group(){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			//$data['title'] = "New Group";
			$data['title'] = $this->mainLink("#admin_panel", "Admin Settings")." | ". $this->breadCrumb("admin/groups", "add_group", NULL, NULL, "Staff Groups", "Add New Group ", NULL);

			if(!file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('add-edit_staff_group') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/new_group_type');
				$this->load->view('templates/foot');
			} 
			else{
				$group_type_record = array(
					'group_name' => $this->input->post('group_name'),
					'description'=> $this->input->post('description')
				);

				$rs = $this->admin_model->insert_new_group($group_type_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("New group successfully created");
				}
				redirect('admin/groups');
			}
		}

		public function edit_group($id){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] =$this->mainLink("#admin_panel", "Admin Settings")." | ".$this->breadCrumb("admin/groups", "edit_group", NULL, NULL, "Staff Groups", "Edit " .$this->admin_model->select_group_type_record_by_id($id)['group_name']. " Group", NULL);
			
			$data['group_record'] = $this->admin_model->select_group_type_record_by_id($id);

			// echo $data['exam_type']['academic_year_id']; exit;

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['id'] = $id;

			if($this->form_validation->run('add-edit_staff_group') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/edit_group');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_group();
			}
		}

		public function update_group(){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('update_group_type')){
				$group_name = $this->input->post('group_name');
				$description = $this->input->post('description');
				$id = $this->input->post('id');
				
				$group_type_record = array(
					'group_name' => $group_name,
					'description' => $description
				);

				$rs = $this->admin_model->update_group($id, $group_type_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('admin/groups');
			}
		}

		public function delete_group($id){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_group($id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Group successfully removed");
			}
			redirect('admin/groups');
		}

		public function add_or_remove_staff_to_group($group_id = FALSE){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("admin/groups", "add_group", NULL, NULL, "Staff Groups", "Add / Remove Staff to " .$this->admin_model->select_group_type_record_by_id($group_id)['group_name']. " Group", NULL);
			

			$data['group_id'] = $group_id;
			$data['staffs'] = $this->admin_model->select_all_staffs();
			$data['staff_group_record'] = $this->admin_model->select_staff_by_group_id($group_id);

			if(!file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('add_staff_to_group') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/new_staff_to_group');
				$this->load->view('templates/foot');
			} 
			else{
				$data['staff_ids'] = $this->input->post('staff_id');

				foreach ($data['staff_ids'] as $key => $value) {
					$record[] = array(
						'staff_id' => $value,
						'group_id' => $group_id
					);
				}

				$rs = $this->admin_model->insert_remove_staff_to_group($record, $group_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('admin/view_members/'.$group_id);
			}
		}

		public function view_members($group_id){
			if(!($this->session->userdata('manage_groups') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("admin/groups", "group_members", NULL, NULL, "Staff Groups","View Group Members", NULL);
			
			$data['staff_group'] = $this->admin_model->select_all_group_member_by_group_id($group_id);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['group_id'] = $group_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/staff_groups');
			$this->load->view('templates/foot');
		}
		//END GROUP TYPE

		//START ROLE
		public function add_new_role(){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("admin/roles", "add_new_role", NULL, NULL, "Manage Roles", "Add New Role", NULL);
			//$data['title'] = "New Role";

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('add-edit_new_role') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/new_role');
				$this->load->view('templates/foot');
			}
			else{
				$role_record = array(
					'role_name' => $this->input->post('role_name'),
					'description' => $this->input->post('description')
				);

				$rs = $this->admin_model->add_new_role($role_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("New role successfully created");
				}
				redirect(base_url() . 'admin/roles');
				//redirect(base_url() . 'admin/add_new_role');
			}				
		}

		public function roles(){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_role') != ""){
	            $value = trim($this->input->post('search_role'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'admin/roles/'.$value;
			$config['total_rows'] = $this->admin_model->count_all_role_types($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['role_types'] = $this->admin_model->fetch_all_role_types($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
   			$data['title'] = $this->mainLink("#admin_panel", "Admin Settings")." | ".$this->breadCrumb("roles", NULL, NULL, NULL, "Manage Roles", NULL, NULL);

			//$data['title'] = "Roles";
			//$data['role_types'] = $this->admin_model->select_all_role_types();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['display_back'] = "NO";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/view_roles');
			$this->load->view('templates/foot');
		}

		public function manage_role($role_type_id){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("admin/roles", "manage_role", NULL, NULL, "Manage Roles", "Assign Roles to ".$this->admin_model->select_role_by_id($role_type_id)['role_name'], NULL);
			$data['role_type_id'] = $role_type_id;

			$data['modules'] = $this->admin_model->select_modules();
			$data['permission_records'] = $this->admin_model->select_permissions_by_role($role_type_id);
			$data['common_permissions'] = $this->admin_model->select_common_permission('all');
			$data['class_teacher_permission'] = $this->admin_model->select_common_permission('class_teachers');
			$data['hod_permission'] = $this->admin_model->select_common_permission('hods');
			$data['dorm_master_permission'] = $this->admin_model->select_common_permission('dormitory_masters');
			$data['other_permissions'] = $this->admin_model->select_common_permission('other');

			/*echo "<pre>";
				print_r($data['modules']);
			echo "</pre>";
			die*/;

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('manage_role') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/manage_role');
				$this->load->view('templates/foot');
			}
			else{
				$permissions = $this->input->post('permission');

				if($permissions != FALSE){
					foreach ($permissions as $key => $row) {
						$permission_records[] = array(
							'role_type_id' => $role_type_id,
							'permission_id' => $row
						);

						$role_module_record[] = array(
							'role_type_id' => $role_type_id,
							'module_id' => $this->admin_model->select_module_id_by_permission_id($row)
						);
						//echo $this->admin_model->select_module_id_by_permission_id($row) . "<br/>";
					}
						
					$rs = $this->admin_model->manage_role($role_type_id, $permission_records, $role_module_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect(base_url() . 'admin/manage_role/'.$role_type_id);
				}
				else{
					//IN CASE ALL CHECKBOXES ARE unchecked
					$rs = $this->admin_model->remove_all_by_role($role_type_id);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect(base_url() . 'admin/manage_role/'.$role_type_id);				
				}
			}

		}

		public function edit_role($role_type_id){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] =$this->mainLink("#admin_panel", "Admin Settings")." | ".$this->breadCrumb("admin/roles", "edit_role", NULL, NULL, "Manage Roles", "Edit ".$this->admin_model->select_role_by_id($role_type_id)['role_name']." Role", NULL);
			//$data['title'] = "Edit Role";

			$data['role_record'] = $this->admin_model->select_role_by_id($role_type_id);
			$data['role_type_id'] = $role_type_id;

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('add-edit_new_role') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/edit_role');
				$this->load->view('templates/foot');
			}
			else{
				$role_record = array(
					'role_name' => $this->input->post('role_name'),
					'description' => $this->input->post('description')
				);

				$rs = $this->admin_model->update_role($role_type_id, $role_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Role successfully updated");
				}
				redirect(base_url() . 'admin/edit_role/'.$role_type_id);
			}
		}

		public function add_user_to_role($role_type_id){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
            
			$data['title'] =$this->mainLink("#admin_panel", "Admin Settings")." | ".$this->breadCrumb("admin/roles", "add_user_to _role", NULL, NULL, "Manage Roles", "Add/Remove Staff to ".$this->admin_model->select_role_by_id($role_type_id)['role_name']." Role", NULL);
			//$data['title'] = "Manage Role";

			$data['role_type_id'] = $role_type_id;
			$data['staffs'] = $this->admin_model->select_all_staffs();
			$data['staff_role_records'] = $this->admin_model->select_staff_by_role_type_id($role_type_id);

			if(!file_exists(APPPATH.'views/admin')){
				show_404();
			}

			if($this->form_validation->run('add_or_remove_staff_to_role') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/add_remove_staff_to_role');
				$this->load->view('templates/foot');
			} 
			else{
				$data['staff_ids'] = $this->input->post('staff_id');

				foreach ($data['staff_ids'] as $key => $value) {
					$record[] = array(
						'staff_id' => $value,
						'role_type_id' => $role_type_id
					);

				}
				$rs = $this->admin_model->insert_remove_staff_to_role($record, $role_type_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("User role successfully added");
				}
				redirect('admin/roles');
			}
		}

		public function remove_role($role_type_id){
			if(!($this->session->userdata('manage_roles') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$rs = $this->admin_model->remove_role($role_type_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Role successfully removed");
			}
			redirect('admin/roles/');
		}
		//END ROLE

		//PROMOTE STUDENTS SECTION
		public function promote(){
			if(!($this->session->userdata('promote_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}


			$data['title'] = "Promote Student";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/promote_student');
			$this->load->view('templates/foot');
		}

		public function promote_student(){
			if(!($this->session->userdata('promote_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(isset($_POST['promote'])){
				$level = $this->input->post('level');
				$user_id = $this->input->post('user_id');
				$academic_year = $this->admin_model->select_academic_year($level);

				/*echo $academic_year['year'];
				exit;*/

				$record = array(
					'promotion_level' => $level,
					'promoted_by' => $user_id,
					'academic_year' => $academic_year['id']
				);

				/*$this->admin_model->insert_promoted_audit_record($record, $level, $academic_year['id']);
					if(!$this->session->flashdata('exist_message')){
						$this->session->set_flashdata('success_message', 'Successfully promoted');
						redirect('admin/promote');
					}*/
				$rs = $this->admin_model->insert_promoted_audit_record($record, $level, $academic_year['id']);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully promoted");
				}
				redirect('admin/promote');
			}
			else{
				/*if($this->input->post('restore')){
					$level = $this->input->post('level');

					$this->admin_model->restore_students_table($level);
					$this->session->set_flashdata('success_message', 'Successfully restored');
					redirect('admin/promote');
				}*/
				if($this->input->post('demote')){
					$level = $this->input->post('level');
					$admission_no = $this->input->post('admission_no');
					$aid = $this->student_model->select_current_academic_year_by_level($level);//Obtain academic year id
					echo $aid;
					exit;

					$rs = $this->admin_model->restore_students_table($level);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully restored");
					}
					redirect('admin/promote');
				}
				redirect('admin/promote');
			}
		}
		//END PROMOTE STUDENTS	

		public function matokeo(){
			if(!($this->session->userdata('view_school_results') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'school/matokeo/'.$value;
			$config['total_rows'] = $this->admin_model->count_all_academic_year_for_results($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['records'] = $this->admin_model->select_years_for_results($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Yearly Results";
			$data['exam_types'] = $this->admin_model->select_exam_types();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['display_back'] = "NO";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('admin/school_matokeo');
			$this->load->view('templates/foot');
		}

		public function edit_student_results($admission_no = NULL, $etype_id = NULL, $term_id = NULL){
			if(!($this->session->userdata('edit_student_results') == 'ok') || $admission_no == NULL || $etype_id == NULL || $term_id == NULL){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Edit Results";
			$data['admission_no'] = $admission_no;
			$data['etype_id'] = $etype_id;
			$data['term_id'] = $term_id;

			$data['result_records'] = $this->admin_model->edit_student_results($admission_no, $etype_id, $term_id);

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['links'] = "";
			$data['display_back'] = "NO";

			//Array special for updating student_subject_score_position
			$etypes = array(
				'E01' => 'monthly_one', 
				'E02' => 'midterm',
				'E03' => 'monthly_two',
				'E04' => 'terminal'
			);

			/*echo "<pre>";
				print_r($etypes);
			echo "</pre>";
			die;*/

			if($this->form_validation->run('update_results') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('admin/edit_student_results');
				$this->load->view('templates/foot');
			}
			else{
				$subject_id = $this->input->post('subject_id');
				$class_stream_id = $this->input->post('class_stream_id');
				$class_id = $this->admin_model->select_class_stream_by_id($class_stream_id)['class_id'];
				
				//$mmmt = $etypes[$etype_id];	//Kwa ajili ya index ya student_subject_score_position
											//Never change the id in the database
				$student_marks = array(
					'marks' => $this->input->post('score')
				);

				$rs = $this->admin_model->update_student_results($admission_no, $etype_id, $term_id, $subject_id, $student_marks, $class_stream_id, $class_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect(base_url() . 'admin/edit_student_results/'.$admission_no.'/'.$etype_id.'/'.$term_id);
			}
		}
	}
