<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Notification extends MY_Controller{
		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('notification_model');

			if(!($this->session->userdata('view_notifications') == 'ok')){
				redirect('home');
			}
			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }
			//$this->errorAcademicYearSettings();
		}

		public function count_notifications($staff_id){
			$data['total'] = $this->notification_model->count_notifications($staff_id);

			$this->load->view('templates/teacher_header', $data);
		}

		public function view($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('view_notifications') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['title'] = $this->breadCrumb("notification/view".$staff_id, NULL, NULL, NULL, "Notifications", NULL, NULL);
			
			$data['notification_msgs'] = $this->notification_model->view($staff_id);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/view_notifications', $data);
			$this->load->view('templates/foot', $data);
		}

		public function mark_read(){
			$notification_id = $this->input->post('notification_id');
			$staff_id = $this->input->post('staff_id');
			$status = 'read';

			$allowed = FALSE;
			if($this->session->userdata('view_notifications') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('notification_id')){
				$record = array(
					'notification_status' => $status
				);

				$rs = $this->notification_model->update_notification($notification_id, $record);

				//Update Notification
				$notification = $this->notification_model->count_notifications($staff_id);
				$this->session->set_userdata('notifications', $notification);

				$notification_messages = $this->notification_model->select_notifications($staff_id);
				$this->session->set_userdata('notification_messages', $notification_messages);
				//Update Notification
				if($rs){
					$this->transactionSuccessfullyCommitted("Marked as read");
				}
				redirect('notification/view/'.$staff_id);
			}
			else{
				redirect('notification/view/'.$this->session->userdata('staff_id'));
			}
		}
	}
