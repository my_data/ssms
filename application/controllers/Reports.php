<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Reports extends MY_Controller{

		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}

			//$this->load->model('admin_model');
			//$this->load->model('class_model');
			//$this->load->model('department_model');
			//$this->load->model('dormitory_model');
			//$this->load->model('subject_model');
			//$this->load->model('staff_model');
			//$this->load->model('student_model');
			//$this->load->model('teacher_model');
			$this->load->model('library_model');
			//$this->load->model('report_model');

			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }			
			//$this->errorAcademicYearSettings();
		}

		

		public function dormitories(){
			$data['dormitories'] = $this->report_model->fetch_dormitories();
			$data['data'] = $this->dormitory_model->view_dorm();
		
			$data['tooltip'] = "Dormitories";
			$data['title'] = "LIST OF ALL DORMITORIES";
			$data['display_options'] = "";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/dormitories');
			$this->load->view('templates/foot');
		}

		public function class_streams(){
			$data['classes'] = $this->report_model->fetch_classes();
			$data['teachers'] = $this->class_model->select_teachers();
			$data['class_streams_count'] = $this->class_model->class_stream_count();
			
			$data['tooltip'] = "Class Streams";
			$data['title'] = "LIST OF ALL CLASS STREAMS";
			$data['display_options'] = "";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/class_streams');
			$this->load->view('templates/foot');
		}

		public function departments(){			
			$data['departments'] = $this->report_model->fetch_departments();

			$data['tooltip'] = "Departments";
			$data['title'] = "LIST OF ALL DEPARTMENTS";
			$data['display_options'] = "";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/departments');
			$this->load->view('templates/foot');
		}

		public function staffs(){			
			$data['staffs'] = $this->report_model->fetch_staffs();

			$data['tooltip'] = "Staffs";
			$data['title'] = "LIST OF ALL STAFFS";
			$data['display_options'] = "";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/staffs');
			$this->load->view('templates/foot');
		}

		public function students(){
			$value = "";
			if($this->input->post('search_by_class') != ""){
	            $value = trim($this->input->post('search_by_class'));
	            $this->no_record_found_message();
	        }

			$data['students'] = $this->report_model->fetch_students($value);
			$data['no_students_count'] = $this->report_model->count_student_per_class($value);
			$data['tnos'] = $this->report_model->count_total_number_of_students($value);


			$jina_la_darasa = array();
			foreach ($this->report_model->get_class_name($value) as $key => $row) {
				//Capture jina la darasa iwapo search result will be more than one
				$jina_la_darasa[] = $row['class_name'];
			}

			$class_name = implode(' & ', $jina_la_darasa);

			$data['check'] = $jina_la_darasa;
			

			$data['tooltip'] = "Students";
			$data['title'] = (!empty($value) && count($this->report_model->get_class_name($value)) != $this->report_model->count_class_levels()) ? "LIST OF STUDENTS: " . $class_name : "LIST OF ALL STUDENTS";
			$data['display_options'] = "STUDENTS_LIST";
			
            if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/students');
			$this->load->view('templates/foot');
		}

		public function assets(){		
			$data['assets'] = $this->report_model->get_assets();	
			$data['asset_count'] = $this->report_model->get_domitory_asset();
			$data['asset_status'] = $this->report_model->get_asset_status();

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "Assets";
			$data['title'] = "ASSETS OVERVIEW";
			$data['assets_title'] = "LIST OF ALL DORMITORY ASSETS";
			$data['display_options'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/dormitory_assets');
			$this->load->view('templates/foot');
		}

		public function class_assets(){		
			$data['assets'] = $this->report_model->get_class_assets();	
			$data['asset_count'] = $this->report_model->get_class_asset_count();
			$data['asset_status'] = $this->report_model->get_class_asset_status();

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "Assets";
			$data['title'] = "ASSETS OVERVIEW";
			$data['assets_title'] = "LIST OF ALL CLASS ASSETS";
			$data['display_options'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/class_assets');
			$this->load->view('templates/foot');
		}


		public function selected($form = NULL){
			$data['students'] = array();
			$data['count_reported'] = array();
			$class_name = "";

			if($form == "form_one"):
				$form = "I";
				$class_name = "FORM ONE";
				$data['students'] = $this->report_model->retrieve_all_selected_students($form);
				$data['count_reported'] = $this->student_model->count_selected_students($form);
			endif;
			if($form == "form_five"):
				$form = "V";
				$class_name = "FORM FIVE";
				$data['students'] = $this->report_model->retrieve_all_selected_students($form);
				$data['count_reported'] = $this->student_model->count_selected_students($form);
			endif;

			$data['tooltip'] = "Students";
			$data['title'] = "LIST OF ALL SELECTED STUDENTS: " . $class_name;
			$data['display_options'] = "SELECTED";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/selected_students');
			$this->load->view('templates/foot');
		}

		public function transferred_in(){
			$value = "";
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }

			$transfer_status = "IN";
			
			$data['students'] = $this->report_model->select_transferred_in_students($value);
			

			$data['tooltip'] = "Students";
			$data['title'] = "LIST OF ALL TRANSFERRED IN STUDENTS";
			$data['display_options'] = "TRANSFERRED_IN";

			$data['status'] = $transfer_status;
			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/transferred_in');
			$this->load->view('templates/foot');
		}

		public function transferred_out(){
			$value = "";
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }

			$transfer_status = "OUT";
			
			$data['students'] = $this->report_model->select_transferred_out_students($value);
			

			$data['tooltip'] = "Students";
			$data['title'] = "LIST OF ALL TRANSFERRED OUT STUDENTS";
			$data['display_options'] = "TRANSFERRED_OUT";

			$data['status'] = $transfer_status;
			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/transferred_out');
			$this->load->view('templates/foot');
		}

		public function disabled_students(){
			$data['students'] = $this->report_model->retrieve_all_disabled_students();
			$data['count_by_disabilty'] = $this->report_model->count_disabled_students();

			$data['tooltip'] = "Students";
			$data['disabled_title'] = "LIST OF STUDENTS WITH DISABILITY";
			$data['title'] = "OVERVIEW OF STUDENTS WITH DISABILITY";
			$data['display_options'] = "";

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/disabled_students');
			$this->load->view('templates/foot');
		}


		public function staff_attendance_report($year_and_month = NULL){
			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['title'] = "Attendance Report";
			$x = 0;

			if($this->input->post('monthValue') != ""){
				$search_month_id_name = trim($this->input->post('monthValue'));
				$year = trim($this->input->post('yearSearch'));
				$month = explode('-', $search_month_id_name)[0];

				$year_and_month = $year . '-' . $month;

				$x = 1;	//For flag
			}
			else{
				if($year_and_month != NULL){
					$month = explode('-', $year_and_month)[1];
					$year = explode('-', $year_and_month)[0];
				}
				else{
					$month = explode('-', date('Y-m'))[1];
					$year = explode('-', date('Y-m'))[0];
				}
				$x = 1;	//For flag
			}

			$data['monthly_attendance_records'] = $this->report_model->create_monthly_report($month, $year);

			
			//Flag used in view for feedeback message
			if($x === 0){
				$data['flag'] = TRUE;
			}
			else{
				$data['flag'] = FALSE;	
			}

			$data['tooltip'] = "Attendance";
			$data['title'] = "<strong>Attendance Report:</strong> &nbsp;". date('F', mktime(0,0,0,$month,10)) . ", " . $year;
			$data['display_options'] = "STAFF_ATTENDANCE";			

			$data['date'] = $year_and_month;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/staff_monthly_attendance_report');
			$this->load->view('templates/foot');
		}

		public function enrolled(){
			$data['title'] = "Total Number of Students";
			$data['no_of_students'] = $this->class_model->view_enrolled_students();

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "";
			$data['title'] = "ENROLLED STUDENTS PER STREAM: " . date('Y');
			$data['display_options'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/enrolled_students');
			$this->load->view('templates/foot');
		}

		public function parents(){
			$value = "";
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }

			$data['parents'] = $this->report_model->get_parents($value);

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "";
			$data['title'] = "LIST OF ALL PARENTS";
			$data['display_options'] = "PARENTS";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/parents');
			$this->load->view('templates/foot');
		}

		public function wazazi($csid = NULL){
			$data['parents'] = $this->report_model->get_parents_per_class_stream($csid);

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "";
			$data['title'] = "LIST OF ALL PARENTS";
			$data['display_options'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/parents');
			$this->load->view('templates/foot');
		}

		public function accomodation_history(){
			$data['accomodation_data'] = $this->report_model->get_accomodation_history();

			/*echo "<pre>";
				print_r($data['parents']);
			echo "</pre>";die;*/

			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "";
			$data['title'] = "ACCOMODATION HISTORY";
			$data['display_options'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/accomodation_history');
			$this->load->view('templates/foot');
		}


		public function yearly_results($adm_no = NULL, $aid = NULL){
			$data['results_data'] = $this->report_model->select_result_by_academic_year($adm_no, $aid);
			$data['std_record'] = $this->report_model->get_student($adm_no);
			$data['record'] = $this->report_model->get_class_by_adm_no($adm_no, $aid);

			$data['ft_record'] = $this->report_model->checkIfResultsExist($adm_no, 'first_term')['count'];
			$data['st_record'] = $this->report_model->checkIfResultsExist($adm_no, 'second_term')['count'];

			//echo $this->report_model->select_term_id($aid, 'second_term'); exit;
			if($this->report_model->select_term_id($aid, 'first_term') != ""):
				$data['ft_pos'] = $this->student_model->select_single_student_position_by_term($adm_no, $this->report_model->select_term_id($aid, 'first_term'));
			endif;

			if($this->report_model->select_term_id($aid, 'second_term') != ""):
				$data['st_pos'] = $this->student_model->select_single_student_position_by_term($adm_no, $this->report_model->select_term_id($aid, 'second_term'));
			endif;


			if(! file_exists(APPPATH.'views/reports')){
				show_404();
			}

			$data['tooltip'] = "";
			$data['display_options'] = "";
			$data['title'] = "EXAMINATION RESULTS: " . $this->report_model->get_academic_year($aid);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('reports/printHeader');
			$this->load->view('reports/yearly_results');
			$this->load->view('templates/foot');
			/*echo "<pre>";
				print_r($this->report_model->select_result_by_academic_year($adm_no, $aid));
			echo "</pre>";*/

		}	

		
		 public function assigned_teachers(){
                        $data['records'] = $this->report_model->assigned_teachers();

                        $data['title'] = "LIST OF SUBJECTS AND ASSIGNED TEACHERS";
                        $data['tooltip'] = "";
                        $data['display_options'] = "";

                        if(! file_exists(APPPATH.'views/reports')){
                                show_404();
                        }

			//print_r($data['records']);exit;

                        $this->load->view('templates/teacher_header', $data);
                        $this->load->view('reports/printHeader');
                        $this->load->view('reports/assigned_teachers');
                        $this->load->view('templates/foot');
                }

	}
