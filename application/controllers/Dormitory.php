<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dormitory extends MY_Controller{

	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
		
			
		$this->load->library('form_validation');
		//$this->load->model('admin_model');
		//$this->load->model('dormitory_model');
		//$this->load->model('student_model');
		$this->load->helper('url');
		$this->load->library('pagination');

		 if($this->session->userdata('user_role') !== "Admin"){
                 	$this->refreshRoles();
                 }	
		//$this->errorAcademicYearSettings();
	}

	public function view_all(){
		if(!($this->session->userdata('view_dormitories') == 'ok')){
			$this->authenticate_user_permission('home', FALSE);
		}
			
		if($this->input->post('search_dormitory') != ""){
	        $value = trim($this->input->post('search_dormitory'));
	        $this->no_record_found_message();
        }
        else{
            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }

		$config = array();
		$config['base_url'] = base_url() . 'dormitory/view_all/'.$value;
		$config['total_rows'] = $this->admin_model->count_dormitory($value);
		$config['per_page'] = 10;
		$config['num_links'] = 3;
		//$config['url_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
		$data['dormitories'] = $this->admin_model->fetch_dormitories($config['per_page'], $page, $value);
		$data['links'] = $this->pagination->create_links();

		$data['title'] = $this->breadCrumb("dormitories", NULL, NULL, NULL, "All Dormitories", NULL, NULL);
		
		//$data['dorms'] = $this->dormitory_model->view_domitories();
		$data['available'] = $this->dormitory_model->view_dorm();
		$data['offset'] = $page;

		$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		if(! file_exists(APPPATH.'views/dormitories')){
			show_404();
		}

		$data['display_back'] = "NO";

		$this->load->view('templates/teacher_header');
		$this->load->view('dormitories/view', $data);
		$this->load->view('templates/foot');
	} 

	public function assets($dorm_id = NULL){
		$allowed = FALSE;
		if($this->session->userdata('view_assets_in_dormitory') == 'ok' || ($this->session->userdata('view_assets_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		if($this->input->post('search_asset') != ""){
            $value = trim($this->input->post('search_asset'));
            $this->no_record_found_message();
        }
        else{
            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
        }
		$config = array();
		$config['base_url'] = base_url() . 'dormitory/assets/'.$dorm_id.'/'.$value;
		$config['total_rows'] = $this->dormitory_model->count_domitory_asset($dorm_id, $value);
		$config['per_page'] = 10;
		$config['num_links'] = 3;
		//$config['url_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);
		//$page = $this->uri->segment(4);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
		$data['assets'] = $this->dormitory_model->get_domitory_asset($dorm_id, $config['per_page'], $page, $value);
		$data['links'] = $this->pagination->create_links();

		$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		if(! file_exists(APPPATH.'views/dormitories')){
			show_404();
		}

		$data['display_back'] = "NO";

		$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/assets/".$dorm_id, NULL, NULL, "All Dormitories", "Assets in ". $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name']. " Dormitory ", NULL);
		
		$data['dorm_id'] = $dorm_id;
		$data['editModalID'] = $dorm_id . $page;
		$data['offset'] = $page;

		//$data['assets'] = $this->dormitory_model->get_domitory_asset($dorm_id);

		$data['assets_quantity'] = $this->dormitory_model->asset_reports($dorm_id);

		$this->load->view('templates/teacher_header');
		$this->load->view('dormitories/view_dormitory_assets', $data);
		$this->load->view('templates/foot');
	} 

	public function new_asset(){
		$dorm_id = $this->input->post('dorm_id');
		
		$allowed = FALSE;
		if($this->session->userdata('manage_dormitory_assets') == 'ok' || ($this->session->userdata('add_asset_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		$asset_no = $this->input->post('asset_no');
		$asset_name = $this->input->post('asset_name');
		$status = $this->input->post('status');
		$description = $this->input->post('description');


		if($this->form_validation->run('add-edit_asset') === FALSE){
			$this->session->set_flashdata('errors', validation_errors());
			redirect('dormitory/assets/'.$dorm_id);
			//$this->assets($dorm_id);
		}
		else{
			$asset_record = array(
				'dorm_id' => $dorm_id,
				'asset_name' => $asset_name,
				'asset_no' => $asset_no,
				'status' => $status,
				'description' =>$description
			);

			$rs = $this->dormitory_model->insert_new_asset($asset_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Asset successfully added");
			}
			redirect('dormitory/assets/'.$dorm_id);
		}
	}

	public function update_asset(){
		$dorm_id = $this->input->post('dorm_id');

		$allowed = FALSE;
		if($this->session->userdata('manage_dormitory_assets') == 'ok' || ($this->session->userdata('update_asset_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		
		$asset_no = $this->input->post('asset_no');
		$old_asset_no = $this->input->post('old_asset_no');
		$asset_name = $this->input->post('asset_name');
		$status = $this->input->post('status');
		$description = $this->input->post('description');

		if($this->form_validation->run('add-edit_asset') === FALSE){
			$this->session->set_flashdata('errors', validation_errors());
			redirect('dormitory/assets/'.$dorm_id);
		}
		else{
			$asset_record = array(
				'asset_name' => $asset_name,
				'asset_no' => $asset_no,
				'status' => $status,
				'description' =>$description
			);

			$rs = $this->dormitory_model->update_asset($asset_record, $old_asset_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('dormitory/assets/'.$dorm_id);
		}
	}

	public function delete_asset($id, $dorm_id){
		$allowed = FALSE;
		if($this->session->userdata('manage_dormitory_assets') == 'ok' || ($this->session->userdata('remove_asset_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		$rs = $this->dormitory_model->delete_asset($id);
		if($rs){
			$this->transactionSuccessfullyCommitted("Successfully removed");
		}
		redirect('dormitory/assets/'.$dorm_id);
	}

	public function assign_dorm_leader(){
		if(!($this->session->userdata('add_dormitory_leader') == 'ok')){
			redirect(base_url() . 'home');
		}

		$dorm_id = $this->input->post('dorm_id');
		$admission_no = $this->input->post('admission_no');

		if($this->form_validation->run('assign_leader') == FALSE){
			$this->session->set_flashdata('errors', validation_errors());
			redirect('dormitory/view_students/'.$dorm_id);
		}
		else{
			$dorm_leader_record = array(
				'admission_no' => $admission_no
			);

			$rs = $this->dormitory_model->assign_dorm_leader($dorm_leader_record, $dorm_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Dormitory leader successfully added");
			}
			redirect('dormitory/view_students/'.$dorm_id);
		}
	}

	public function add(){
		if(!($this->session->userdata('manage_dormitory') == 'ok')){
			$this->authenticate_user_permission('home', FALSE);
		}

			$data['title'] = $this->breadCrumb("dormitory/add", NULL, NULL, NULL, "Add Domitory" ,NULL, NULL);
		
			if($this->form_validation->run('add-edit_dormitory') == FALSE){
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/add');
			$this->load->view('templates/foot');
		}
		else{
			$this->submit();
		}
	}

	public function submit(){
		$record = array(
			'dorm_name' => $this->input->post('dorm_name'),
			'location' => $this->input->post('dorm_location'),
			'capacity' => $this->input->post('dorm_capacity')
		);

		$rs = $this->dormitory_model->submit($record);
		if($rs){
			$this->transactionSuccessfullyCommitted("Dormitory successfully added");
		}
		redirect(base_url('dormitory/view_all'));
	}

	public function edit($id){
		if(!($this->session->userdata('manage_dormitory') == 'ok') || $id == NULL){
			$this->authenticate_user_permission('home', FALSE);
		}

		$data['title'] = $this->breadCrumb("dormitory/view_all", "edit_domitory", NULL, NULL, "All Dormitories", "Edit ". $this->dormitory_model->select_dormitory_by_id($id)['dorm_name']." Dormitory", NULL);
		
		$data['data'] = $this->dormitory_model->edit_domitory($id);
		$id = $this->input->post('txt_hidden');

		$record = array(
			'dorm_name' => $this->input->post('dorm_name'),
			'location' => $this->input->post('dorm_location'),
			'capacity' => $this->input->post('dorm_capacity')
		);

		if($this->form_validation->run('add-edit_dormitory') == FALSE){
			$this->load->view('templates/teacher_header');
			$this->load->view('dormitories/edit', $data);
			$this->load->view('templates/foot');
		}
		else{
			$rs = $this->dormitory_model->update_domitory($id, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect(base_url('dormitory/view_all'));
		}
	}


	public function delete($id){
		if(!($this->session->userdata('manage_dormitory') == 'ok')){
			$this->authenticate_user_permission('home', FALSE);
		}

		$rs = $this->dormitory_model->delete_domitory($id);
		if($rs){
			$this->transactionSuccessfullyCommitted("Successfully removed");
		}
		redirect(base_url('dormitory/view_all'));	
	}

	/*public function dormitory_asset($id){
		$data['title'] = "Dormitory Assets";

		$data['data'] = $this->dormitory_model->get_domitory_asset($id);

		$data['dataa'] = $this->dormitory_model->asset_reports($id);

		$this->load->view('templates/teacher_header');
		$this->load->view('dormitories/view_dormitory_assets', $data);
		$this->load->view('templates/foot');

	}*/



//student part



	//This function was previously used, before it was replaced my bootstrap modal
	public function add_student($dorm_id){
		$data['title'] = "Allocate Student To Dormitory";

		/*$data['dormitories'] = $this->dormitory_model->view_domitories();*/
		$data['students'] = $this->dormitory_model->get_students_not_in_dormitory($dorm_id);

		$data['dorm_id'] = $dorm_id;
		$data['dorm_name'] = $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name'];

		if(! file_exists(APPPATH.'views/dormitories')){
			show_404();
		}

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/allocate_student');
			$this->load->view('templates/foot');
		}
		else{
			$dorm_id = $this->input->post('dorm_id');
			$admission_no = $this->input->post('admission_no');

			$record = array(
				'dorm_id' => $dorm_id
			);

			$results = $this->dormitory_model->update_shiftdomitory($admission_no, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully shifted to dormitory");
			}
			redirect(base_url('dormitory/view_all') );
		}
	}


	public function shift_student($admission_no = NULL){
		if(!($this->session->userdata('transfer_student_to_another_dormitory') == 'ok')){
			$this->authenticate_user_permission('home', 'FALSE');
		}

		$data['title'] = "Shift Student to another Domitory";
		$data['data'] = $this->dormitory_model->shiftdomitory_student($admission_no);
		$data['dorms'] = $this->dormitory_model->get_dom($admission_no);

		$admission_no = $this->input->post('admission_no');
		$dorm_id = $this->input->post('dorm_id');
		$curr_dorm = $this->input->post('current_dorm');

		if($this->form_validation->run('shift_student_to_dorm') == FALSE){
			$this->load->view('templates/teacher_header');
			$this->load->view('dormitories/shiftdomitory', $data);
			$this->load->view('templates/foot');
		}
		else{
			$record = array(
				'dorm_id' => $dorm_id
			);

			$rs = $this->dormitory_model->update_shiftdomitory($admission_no, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully shifted to dormitory");
			}
			redirect(base_url('dormitory/view_students/'.$curr_dorm));
		}		
	}

	public function add_student_to_dorm(){
		if(!($this->session->userdata('add_student_to_dormitory') == 'ok')){
			$this->authenticate_user_permission('home', 'FALSE');
		}

		$admission_no = $this->input->post('admission_no');
		$dorm_id = $this->input->post('dorm_id');

		if($this->form_validation->run('add_student_to_dorm') == FALSE){
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url('dormitory/view_students/'.$dorm_id) );
		}
		else{
			$record = array(
				'dorm_id' => $dorm_id
			);

			$rs = $this->dormitory_model->update_shiftdomitory($admission_no, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
			redirect(base_url('dormitory/view_students/'.$dorm_id) );
		}		
	}


	//assign teacher dom

	public function assignteacher($id = NULL){
		if(!($this->session->userdata('add_dormitory_master') == 'ok')){
			$this->authenticate_user_permission('home', 'FALSE');
		}

		$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/assignteacher/".$id, NULL, NULL, "All Dormitories", "Assign Teacher in ". $this->dormitory_model->select_dormitory_by_id($id)['dorm_name']. " Dormitory", NULL);
		
		$data['data'] = $this->dormitory_model->edit_domitory($id);

		$data['teacher'] = $this->dormitory_model->get_teacher();

		$id = $this->input->post('txt_hidden');
		$record = array(
			'teacher_id' => $this->input->post('t_name'),				
		);

		if($this->form_validation->run('assign_dorm_master') == FALSE){
			$this->load->view('templates/teacher_header');
			$this->load->view('dormitories/assignteacher', $data);
			$this->load->view('templates/foot');
		}
		else{
		//	if($this->input->post('start_date') < date('Y-m-d')){
		//		$this->session->set_flashdata('error_message', 'Invalid Start Date');
		//		redirect(base_url('dormitory/assignteacher/'.$id));
		//	}
		//	else{
				$rs = $this->dormitory_model->assign_t($id, $record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Dormitory master successfully added");
				}
				redirect(base_url('dormitory/view_all'));
		//	}
		}

	}


	//START ROLL CALL FORM
	public function load_roll_call($dorm_id){
		$allowed = FALSE;
		if($this->session->userdata('add_roll_call') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		$data['title'] = "Student Roll Call: "; /*. $dorm_id;*/

		$data['dorm_id'] = $dorm_id;

		$data['dorm_students'] = $this->student_model->select_students_by_dormitory($dorm_id);

		if(! file_exists(APPPATH.'views/dormitories')){
			show_404();
		}

		$this->load->view('templates/teacher_header', $data);
		$this->load->view('dormitories/new_roll_call');
		$this->load->view('templates/foot');
	}
	//END ROLL CALL FORM

	public function save_student_roll_call(){
		$data['admission_no'] = $this->input->post('admission_no');
		$dorm_id = $this->input->post('dorm_id');
		$date = $this->input->post('date');			
		$data['att'] = $this->input->post('attendance');

		$time = strtotime($date);
		$month = date("F",$time);
		
		if($date > date('Y-m-d')){
			$this->session->set_flashdata('error_message', "Error, you are attempting to enter the date that is beyond today's date");
			redirect('dormitory/load_roll_call/'.$dorm_id);
		}
		else{
			foreach ($data['admission_no'] as $key => $value) {
				$roll_call_record[] = array(
					'admission_no' => $value,
					'dorm_id' => $dorm_id,
					'date_' => $date,
					'month' => $month,
					'att_id' => $data['att'][$key]
				);
			}
			$rs = $this->dormitory_model->insert_student_roll_call($roll_call_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
			redirect('dormitory/load_roll_call/'.$dorm_id);
		}
	}

	public function get_student_roll_call_by_dormitory($date, $dorm_id){
		//$date = $this->input->post('date');
		//$dorm_id = $this->input->post('dorm_id');

		$allowed = FALSE;
		if($this->session->userdata('view_roll_call') == 'ok' || ($this->session->userdata('view_roll_call_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
			$allowed = TRUE;
		}

		$this->authenticate_user_permission('home', $allowed);

		$data['title'] = "Report";
		$data['student_roll_call_records'] = $this->dormitory_model->create_student_dorm_roll_call_report($date, $dorm_id);

		$data['dormitory_id'] = $dorm_id;

		if(! file_exists(APPPATH.'views/dormitories')){
			show_404();
		}

		$this->load->view('templates/teacher_header', $data);
		$this->load->view('dormitories/student_daily_oll_call_report');
		$this->load->view('templates/foot');

	}


	//***************************** ROLL CALL REPORT ****************************************

		public function roll_call_report($dorm_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_roll_call') == 'ok' || ($this->session->userdata('view_roll_call_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/roll_call_report/".$dorm_id, NULL, NULL, "All Dormitories", "Roll Call Report ". $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name']. " Dormitory", NULL);
			

			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$data['dorm_id'] = $dorm_id;
			$data['is_daily'] = "";	//!Important: Avoid Undefined variable: is_monthly
			$data['is_monthly'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/student_roll_call_report_header');
			$this->load->view('templates/foot');
		}

		public function daily_roll_call($dorm_id = NULL, $date = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_roll_call') == 'ok' || ($this->session->userdata('view_roll_call_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));	//Search ya kutafuta mwanafunzi
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
			}

			if($this->input->post('search_record') != ""){
				$date = $this->input->post('date_');	//Search ya kutafuta by date
			}

			//Custom New Code
			$config = array();
			$config['base_url'] = base_url() . 'dormitory/daily_roll_call/'.$dorm_id.'/'.$date.'/'.$value;
			$config['total_rows'] = $this->dormitory_model->count_students_per_dormitory($dorm_id, $value);
			$config['per_page'] = 5;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['students'] = $this->dormitory_model->select_student_roll_call_record($dorm_id, $date, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			//End Custom New Code
			
			$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/roll_call_report/".$dorm_id, NULL, NULL, "All Dormitories", "Roll Call Report ". $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name']. " Dormitory " . $date, NULL);


			$data['dorm_id'] = $dorm_id;
			$data['date'] = $date;

			$data['is_daily'] = "YES";	//These are special for the search field ...
			$data['is_monthly'] = "NO";	//.. they help redirect the search to the corresponding attendance

			$data['no_of_students']  = $this->dormitory_model->select_daily_roll_call_summary($dorm_id, $date);

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/student_roll_call_report_header');
			$this->load->view('dormitories/student_daily_roll_call_report');
			$this->load->view('templates/foot');
		}


		public function monthly_roll_call($dorm_id = NULL, $year_and_month = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_roll_call') == 'ok' || ($this->session->userdata('view_roll_call_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($year_and_month == NULL){
				$year_and_month = explode('-', date('Y-m-d'))[0] . '-' . explode('-', date('Y-m-d'))[1];
			}

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
			}

			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$data['title'] = "Roll Call Monthly";
			$x = 0;

			if($this->input->post('monthValue') != ""){
				$search_month_id_name = trim($this->input->post('monthValue'));
				$dorm_id_search = trim($this->input->post('dorm_id'));
				$year = trim($this->input->post('yearSearch'));
				$month = explode('-', $search_month_id_name)[0];

				$year_and_month = $year . '-' . $month;

				$x = 1;	//For flag
			}
			else{
				$month = explode('-', $year_and_month)[1];
				$year = explode('-', $year_and_month)[0];

				$x = 1;	//For flag
			}

			//Custom New Code
			$config = array();
			$config['base_url'] = base_url() . 'dormitory/monthly_roll_call/'.$dorm_id.'/'.$year_and_month.'/'.$value;
			$config['total_rows'] = $this->dormitory_model->count_students_per_dormitory($dorm_id, $value);
			$config['per_page'] = 5;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['monthly_roll_call_records'] = $this->dormitory_model->create_monthly_roll_call_report($dorm_id, $month, $year, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			//End Custom New Code

			//Flag used in view for feedeback message
			if($x === 0){
				$data['flag'] = TRUE;
			}
			else{
				$data['flag'] = FALSE;	
			}


			$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/roll_call_report/".$dorm_id, NULL, NULL, "All Dormitories", "Roll Call Report ". $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name']. " Dormitory " . $month . " : " . $year, NULL);


			$data['dorm_id'] = $dorm_id;
			$data['date'] = $year_and_month;

			$data['is_daily'] = "NO";	//These are special for the search field ...
			$data['is_monthly'] = "YES";	//.. they help redirect the search to the corresponding attendance

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/student_roll_call_report_header');
			$this->load->view('dormitories/student_monthly_roll_call_report');
			$this->load->view('templates/foot');
		}

		public function view_students($dorm_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('students_in_dormitory') == 'ok' || ($this->session->userdata('view_students_in_own_dormitory') == 'ok' && $this->session->userdata('dormitory')['dorm_id'] === $dorm_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			if($this->input->post('search_student') != ""){
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20", ' ', ($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
			}

			$config = array();
			$config['base_url'] = base_url() . 'dormitory/view_students/'.$dorm_id.'/'.$value;
			$config['total_rows'] = $this->dormitory_model->count_students_per_dormitory($dorm_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			$data['students'] = $this->dormitory_model->fetch_students_by_dormitory($dorm_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['students_not_in'] = $this->dormitory_model->get_students_not_in_dormitory($dorm_id);

			$data['dorm_id'] = $dorm_id;
			$data['dorm_name'] = $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name'];
			$data['dorm_leader'] = $this->dormitory_model->select_dormitory_by_id($dorm_id)['admission_no'];

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = $this->breadCrumb("dormitory/view_all", "dormitory/view_students/".$dorm_id, NULL, NULL, "All Dormitories", "Students in ". $this->dormitory_model->select_dormitory_by_id($dorm_id)['dorm_name']. " Dormitory", NULL);
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/view_students');
			$this->load->view('templates/foot');
		}


		public function end_stay_in_dorm(){
			$dorm_id = $this->input->post('dorm_id');
			$date_ = $this->input->post('date_');

			$record = array(
				'end_date' => $date_
			);

			$rs = $this->dormitory_model->end_stay_in_dorm($dorm_id, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Accomodation Successfully Ended");
			}
			else{
				$this->session->set_flashdata('error_message', 'Error: End Date Should Correspond with the year end_date');
			}
			redirect(base_url('dormitory/view_students/'.$dorm_id) );
		}


		public function accomodation_history(){
			if($this->session->userdata('view_dormitories') == 'ok'){
				$allowed = TRUE;
			}

			if($this->input->post('search_student') != ""){
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
			}

			$config = array();
			$config['base_url'] = base_url() . 'dormitory/accomodation_history/'.$value;
			$config['total_rows'] = $this->dormitory_model->count_accomodation_history($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['accomodation_data'] = $this->dormitory_model->select_accomodation_history($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = "Accomodation History";
			$data['p'] = $page;
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/accomodation_history');
			$this->load->view('templates/foot');
		}

		public function current_and_past_dorm_masters(){
			if($this->session->userdata('view_dormitories') == 'ok'){
				$allowed = TRUE;
			}

			if($this->input->post('search_record') != ""){
				$value = trim($this->input->post('search_record'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
			}

			$config = array();
			$config['base_url'] = base_url() . 'dormitory/current_and_past_dorm_masters/'.$value;
			$config['total_rows'] = $this->dormitory_model->count_dorm_masters_history($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['d_master_records'] = $this->dormitory_model->select_dorm_masters_history($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			
			if(! file_exists(APPPATH.'views/dormitories')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = "Past And Current Dormitory Masters";
			$data['p'] = $page;
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('dormitories/dorm_master_history');
			$this->load->view('templates/foot');
		}
}

?>
