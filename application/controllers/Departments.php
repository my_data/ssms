<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Departments extends MY_Controller{
		public function __construct(){
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}			
			
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error error_message_color">', '</div><p>');
			//$this->load->model('admin_model');
			//$this->load->model('class_model');
			//$this->load->model('department_model');
			//$this->load->model('subject_model');
			//$this->load->model('student_model');
			$this->load->helper('url');
			$this->load->library('pagination');
			
			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }	
			//$this->errorAcademicYearSettings();
		}

		public function index(){
			if(!($this->session->userdata('view_departments') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_department') != ""){
	            $value = trim($this->input->post('search_department'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'departments/index/'.$value;
			$config['total_rows'] = $this->admin_model->count_department($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['departments'] = $this->admin_model->fetch_departments($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = $this->breadCrumb("departments", NULL, NULL, NULL, "All Departments", NULL, NULL);
			
			$data['display_back'] = "NO";
			//$data['title'] = "Subject Departments";

			//$data['departments'] = $this->department_model->select_department_by_type($dept_type);
			$data['subjects'] = $this->subject_model->select_from_subjects();

			$data['teachers'] = $this->class_model->select_teachers();//For populating select element
			//print_r($data['teachers']); exit;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$data['p'] = $page;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/view_depts');
			$this->load->view('templates/foot');
		}


		public function add_hod($dept_id = NULL){
			if(!($this->session->userdata('add_hod') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['staffs'] = $this->department_model->select_staff_by_department($dept_id);

			$data['dept_id'] = $dept_id;
			$dept_name = $this->department_model->select_department_dept_id($dept_id)['dept_name'];
			$data['current_hod'] = $this->department_model->select_department_dept_id($dept_id)['staff_id'];
			$data['title'] = "Add Head Of Department: " . $dept_name;



			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			if($this->form_validation->run('assign_hod') === FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('departments/assign_hod');
				$this->load->view('templates/foot');
			}
			else{
				$staff_id = $this->input->post('staff_id');
				
				$record = array(
					'staff_id' => $staff_id
				);

				$rs = $this->department_model->assign_hod($dept_id, $record, $staff_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("HOD Successfully Added");
				}
				redirect('departments');
			}
		}

		/*public function assign_hod($dept_id){
			if(!($this->session->userdata('add_hod') == 'ok')){
				redirect(base_url() . 'home');
			}

			$staff_id = $this->input->post('staff_id');

			$record = array(
				'staff_id' => $staff_id
			);

			$this->department_model->assign_hod($dept_id, $record, $staff_id);
			redirect('departments');
		}
*/
		public function teaching_assignment_form($dept_id){
			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('assign_teacher_to_subject') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			if($this->department_model->check_css_before_assign($dept_id) != 0){
				$data['title'] = "Assign Teacher To Subject";

				$data['dept_id'] = $dept_id;

				$data['records'] = $this->department_model->select_teacher_by_department($dept_id);	//Get the data for select elements
				$data['class_names'] = $this->department_model->select_class_name_by_department($dept_id);
				$data['streams'] = $this->department_model->select_stream_by_department($dept_id);
				$data['subjects'] = $this->department_model->select_subject_by_department($dept_id);


				if($this->form_validation->run('add-edit_ta') === FALSE){
					$this->load->view('templates/teacher_header', $data);
					$this->load->view('departments/new_teaching_assignment');
					$this->load->view('templates/foot');
				}
				else{
					$subject_id = $this->input->post('subject_id');
					$teacher_id = $this->input->post('teacher_id');
					$class_id = $this->input->post('class_id');
					$stream = $this->input->post('stream');
					$start_date = $this->input->post('start_date');
					$end_date = $this->input->post('end_date');
					//$term = $this->input->post('term');
					$dept_id = $this->input->post('dept_id');

					$class_stream_id = $class_id . $stream;	//Concatenate to obtain the class_stream_id for db

					$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->select_class_level_by_class_stream_id($class_stream_id)['level'])['term_id'];

					$teaching_assignment_record = array(
						'subject_id' => $subject_id,
						'teacher_id' => $teacher_id,
						'class_stream_id' => $class_stream_id,
						'start_date' => $start_date,
						'end_date' => $end_date,
						'dept_id' => $dept_id,
						'term_id' => $data['term_id']
						//'term' => $term
					);

					$rs = $this->department_model->insert_teaching_assignment_record($teaching_assignment_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully Assigned");
					}
					redirect('departments/teaching_assignment_form/'.$dept_id);
				}
			}		
			else{
				$this->load->view('templates/teacher_header');
				$this->load->view('errors/ta_error');
				$this->load->view('templates/foot');
			}	
		}

		public function assign_all($dept_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('assign_teacher_to_subject') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->department_model->check_css_before_assign($dept_id) != 0){
				$rs = $this->department_model->extend_teaching_assignment($dept_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully Extended");
				}
				redirect(base_url() . 'departments/subject_assignment/'.$dept_id);
			}
			else{
				$this->session->set_flashdata('error_message', 'Cannot extend teaching assignment while classes have no subjects');
			}
		}

		public function edit_teaching_assignment($dept_id, $assignment_id){
			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('update_teaching_assignment_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$this->session->set_userdata('assignment_id', $assignment_id);

			$data['title'] = $this->breadCrumb("departments/subject_assignment/".$dept_id, "departments/edit_teaching_assignment/".$dept_id .$assignment_id, NULL, NULL, "".$this->department_model->select_department_dept_id($dept_id)['dept_name']."  Department Teaching Assignment", "Edit", NULL);

			$data['dept_id'] = $dept_id;	

			$data['assignment_record'] = $this->department_model->select_teaching_assignment_id($assignment_id);

			$data['records'] = $this->department_model->select_teacher_by_department($dept_id);	//Get the data for select elements
			$data['class_names'] = $this->department_model->select_class_name_by_department($dept_id);
			$data['streams'] = $this->department_model->select_stream_by_department($dept_id);
			$data['subjects'] = $this->department_model->select_subject_by_department($dept_id);

			if($this->form_validation->run('add-edit_ta') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('departments/edit_teaching_assignment');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_teaching_assignment($assignment_id);
			}
		}

		public function update_teaching_assignment($assignment_id){
			if(isset($_POST['update_teaching_assignment'])){
				$subject_id = $this->input->post('subject_id');
				$teacher_id = $this->input->post('teacher_id');
				$class_id = $this->input->post('class_id');
				$stream = $this->input->post('stream');
				$start_date = $this->input->post('start_date');
				$end_date = $this->input->post('end_date');
				//$term = $this->input->post('term');
				$dept_id = $this->input->post('dept_id');

				$class_stream_id = $class_id . $stream;	//Concatenate to obtain the class_stream_id for db
				$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->select_class_level_by_class_stream_id($class_stream_id)['level'])['term_id'];

				$tar = array(
					'subject_id' => $subject_id,
					'teacher_id' => $teacher_id,
					'class_stream_id' => $class_stream_id,
					'start_date' => $start_date,
					'end_date' => $end_date,
					'dept_id' => $dept_id,
					'term_id' => $data['term_id']
				);

				$rs = $this->department_model->update_teaching_assignment_record($tar, $assignment_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('departments/edit_teaching_assignment/'.$dept_id.'/'.$assignment_id);
			}
			else{
				redirect('departments/edit_teaching_assignment/'.$this->session->userdata('dept')['dept_id'].'/'.$this->session->userdata('assignment_id'));
			}
		}


		public function get_staff_by_department($dept_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('manage_department') == 'ok' || ($this->session->userdata('view_own_departments_staffs') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			if($this->input->post('search_staff') != "") {
				$value = trim($this->input->post('search_staff'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
			}
			
			$config = array();
			$config['base_url'] = base_url() . 'departments/get_staff_by_department/'.$dept_id.'/'.$value;
			$config['total_rows'] = $this->department_model->count_staff_per_department($dept_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['staffs'] = $this->department_model->fetch_staff_by_department($dept_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = $this->breadCrumb("departments", "departments/get_staff_by_department/".$dept_id, NULL, NULL, "All Departments", "Staffs On ".$this->department_model->select_department_dept_id($dept_id)['dept_name']." Department", NULL);
			
			$data['dept_id'] = $dept_id;

			//$data['staffs'] = $this->department_model->select_staff_by_department($dept_id);
			//$data['subjects'] = $this->department_model->select_subject_by_department($dept_id);

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/department_staffs');
			$this->load->view('templates/foot');
		}

		public function classes($dept_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('view_department_classes') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			if($this->input->post('search_record') != "") {
				$value = trim($this->input->post('search_record'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
			}
			
			$config = array();
			$config['base_url'] = base_url() . 'departments/classes/'.$dept_id.'/'.$value;
			$config['total_rows'] = $this->department_model->count_staff_per_department($dept_id, $value);
			$config['per_page'] = 6;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['classes'] = $this->department_model->select_classes_by_department($dept_id, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Classes: " . $dept_id;
			//$data['classes'] = $this->department_model->select_classes_by_department($dept_id);
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$data['dept_id'] = $dept_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/view_department_classes');
			$this->load->view('templates/foot');
		}

		public function subject_assignment($dept_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('view_assigned_teachers_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_assignment') != "") {
				$value = trim($this->input->post('search_assignment'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
			}
			
			$config = array();
			$config['base_url'] = base_url() . 'departments/subject_assignment/'.$dept_id.'/'.$value;
			$config['total_rows'] = $this->department_model->count_ta_per_department($dept_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['assignment_record'] = $this->department_model->select_teaching_assignment_by_department($dept_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = $this->breadCrumb("departments", "departments/subject_assignment/".$dept_id, NULL, NULL, "All Departments", "".$this->department_model->select_department_dept_id($dept_id)['dept_name']."  Department Teaching Assignment", NULL);
			
			$data['display_back'] = "";

			//For hiding/showing extend all button
			$data['show'] = ($this->department_model->check_before_extend_ta($dept_id) == 0) ? "no" : "yes";
			//echo $data['show']; exit;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$data['dept_id'] = $dept_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/view_assigned_subjects');
			$this->load->view('templates/foot');

		}

		public function delete_teaching_assignment(){
			$dept_id = $this->input->post('dept_id');

			$allowed = FALSE;
			if($this->session->userdata('user_role') === "Admin" || ($this->session->userdata('remove_teaching_assignment_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$teacher_id = $this->input->post('teacher_id');
			$subject_id = $this->input->post('subject_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$dept_id = $this->input->post('dept_id');

			/*echo "C=" . $class_stream_id . "-S=" . $subject_id . "-T=" . $teacher_id;
			exit;*/

			$rs = $this->department_model->delete_teaching_assignment($teacher_id, $subject_id, $class_stream_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully Removed");
			}
			redirect('departments/subject_assignment/'.$dept_id);
		}

		/*public function view_departments(){
			$data['title'] = "Departments";

			$data['departments'] = $this->department_model->view_all_depts();

			if(empty($data['departments'])){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/view_depts');
			$this->load->view('templates/foot');
		}*/

		/*public function get_subject_departments($dept_type){
			$config = array();
			$config['base_url'] = base_url() . 'departments/get_subject_departments';
			$config['total_rows'] = $this->admin_model->count_department();
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = $this->uri->segment(3);
			$data['departments'] = $this->admin_model->fetch_departments($config['per_page'], $page);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Departments";

			if(! file_exists(APPPATH.'views/admin')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = "Subject Departments";

			//$data['departments'] = $this->department_model->select_department_by_type($dept_type);
			$data['subjects'] = $this->subject_model->select_from_subjects();

			if(empty($data['departments'])){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/view_depts');
			$this->load->view('templates/foot');
		}*/


		public function new_department(){
			if(!($this->session->userdata('manage_department') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "New Department";

			if($this->form_validation->run('add-edit_department') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('departments/new_dept');
				$this->load->view('templates/foot');
			}
			else{
				$department_record = array(
					'dept_type' => 'subject_department',
					'dept_loc' => $this->input->post('dept_loc'),
					'dept_name' => $this->input->post('dept_name')
				);

				$rs = $this->department_model->create_new_dept($department_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("New department has been added");
				}
				redirect('departments');
			}
		}

		public function edit_department($dept_id = NULL){
			if(!($this->session->userdata('manage_department') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['dept_record'] = $this->department_model->select_department_dept_id($dept_id);

			if(empty($data['dept_record'])){
				show_404();
			}

			$data['title'] = "Edit: ";

			if($this->form_validation->run('add-edit_department') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('departments/edit_dept');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_department();
			}				
		}

		public function update_department(){
			if(!($this->session->userdata('manage_department') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$dept_id = $this->input->post('dept_id');

			$department_record = array(
				'dept_loc' => $this->input->post('dept_loc'),
				'dept_name' => $this->input->post('dept_name')
			);

			$rs = $this->department_model->update_department($dept_id, $department_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('departments');
		}

		public function delete_department($dept_id){
			if(!($this->session->userdata('manage_department') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->department_model->delete_department($dept_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect(base_url() . 'departments');
		}


		public function current_and_past_hods(){
			if(!($this->session->userdata('view_departments') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
				$value = trim($this->input->post('search_record'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
			}

			$config = array();
			$config['base_url'] = base_url() . 'departments/current_and_past_hods/'.$value;
			$config['total_rows'] = $this->department_model->count_hods_history($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['hods_records'] = $this->department_model->select_hods_history($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			
			if(! file_exists(APPPATH.'views/departments')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = "Past And Current HODs";
			$data['p'] = $page;
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('departments/hod_history');
			$this->load->view('templates/foot');
		}
	}
