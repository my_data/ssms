<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Exam extends MY_Controller{
		public function __construct(){
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');
			//$this->load->model('teacher_model');

			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }	
			//$this->errorAcademicYearSettings();
		}

		//START EXAMS
		public function exam_types(){
			if(!($this->session->userdata('view_exam_types') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/exam_types", NULL, NULL, NULL, "Exam Types" , NULL, NULL);
			$data['exam_types'] = $this->admin_model->select_exam_types();

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('exam/view_exam_types');
			$this->load->view('templates/foot');
		}

		/*public function add_exam_type(){
			if(!($this->session->userdata('add_exam_type') == 'ok')){
				redirect(base_url() . 'home');
			}

			$data['title'] = "New Exam Type";
			$data['academic_years'] = $this->admin_model->select_academic_year();

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->form_validation->run('add-edit__exam_type') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/new_exam_type');
				$this->load->view('templates/foot');
			}
			else{
				$academic_year = $this->input->post('academic_year');
				$exam_name_and_exam_types = $this->input->post('exam_name_and_exam_types');
				$level = $this->input->post('level');

				$get_value = explode("/", $exam_name_and_exam_types);
				$o_level_exam_type_id = $get_value[0];
				$exam_name = $get_value[1];
				$a_level_exam_type_id = $get_value[2];

				if($level === 'A'){
					$etid = $a_level_exam_type_id;
				}
				if($level === 'O'){
					$etid = $o_level_exam_type_id;
				}

				$this->admin_model->insert_new_exam_type($etid, $exam_name);
				redirect('exam/exam_types');
			}
		}*/

		public function edit_exam_type($etid){
			if(!($this->session->userdata('update_exam_type') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/exam_types", "edit_exam_type", NULL, NULL, "Exam Types" , "Edit ".$this->admin_model->select_exam_types($etid)['exam_name'], NULL);
		
			$data['exam_type'] = $this->admin_model->select_exam_types($etid);
			$data['academic_years'] = $this->admin_model->select_academic_year();


			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$data['etid'] = $etid;

			if($this->form_validation->run('edit_exam_type') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/edit_exam_type');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_exam_type();
			}
		}

		public function update_exam_type(){
			if(!($this->session->userdata('update_exam_type') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('update_exam_type')){
				$exam_name = $this->input->post('exam_name');
				$start_date = $this->input->post('start_date');
				$end_date = $this->input->post('end_date');
				$etid = $this->input->post('id');

				$rs = $this->admin_model->update_exam_type($etid, $exam_name, $start_date, $end_date);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('exam/exam_types');
			}
		}

		public function delete_exam_type($etid){
			if(!($this->session->userdata('update_exam_type') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_exam_type($etid);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('exam/exam_types');
		}

		public function o_grade(){
			if(!($this->session->userdata('view_o_grade') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/o_grade", NULL, NULL, NULL, "'O' Level Grade System" ,NULL, NULL);
			
			$data['o_grades'] = $this->admin_model->select_o_grade();

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('exam/view_o_grade');
			$this->load->view('templates/foot');
		}

		public function add_o_grade(){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/o_grade", "add_a_grade", NULL, NULL, "'O' Level Grade System" ,"Add Grade", NULL);
			
			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->form_validation->run('add-edit_o_level_grade_system') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/new_o_grade');
				$this->load->view('templates/foot');
			}
			else{
				$start_mark = $this->input->post('start_mark');
				$end_mark = $this->input->post('end_mark');
				$grade = $this->input->post('grade');
				$unit_point = $this->input->post('unit');
				$remarks = $this->input->post('remarks');


				if($start_mark >= $end_mark){
					echo "<script>
							javascript:alert('Invalid range: End Mark should be greater than Start Mark'); 
							window.location = '".base_url()."exam/add_o_grade'
						</script>";
					exit;
				}
				else{
					$o_grade_record = array(
						'start_mark' => $start_mark,
						'end_mark' => $end_mark,
						'grade' => $grade,
						'remarks' => $remarks,
						'unit_point' => $unit_point
					);

					$rs = $this->admin_model->insert_new_o_grade($o_grade_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect('exam/o_grade');
				}
			}
		}

		public function edit_o_grade($id){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/o_grade", "edit_o_grade", NULL, NULL, "'O' Level Grade System" ,"Edit Grade", NULL);
			
			$data['o_grade_record'] = $this->admin_model->select_o_grade_record_by_id($id);

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$data['id'] = $id;

			if($this->form_validation->run('add-edit_o_level_grade_system') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/edit_o_grade');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_o_grade();
			}
		}

		public function update_o_grade(){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('update_o_grade')){
				$start_mark = $this->input->post('start_mark');
				$end_mark = $this->input->post('end_mark');
				$grade = $this->input->post('grade');
				$unit_point = $this->input->post('unit');
				$remarks = $this->input->post('remarks');
				$id = $this->input->post('id');

				$grade_exists = $this->admin_model->select_o_grade($grade)['grade'];

				if($start_mark >= $end_mark){
					echo "<script>
							javascript:alert('Invalid range: End Mark should be greater than Start Mark'); 
							window.location = '".base_url()."exam/edit_o_grade/".$id."'
						</script>";
					exit;
				}
				else if(empty($grade_exists)){
					echo "<script>
							javascript:alert('Invalid grade: ".$grade."'); 
							window.location = '".base_url()."exam/edit_o_grade/".$id."'
						</script>";
					exit;
				}
				else{
					$o_grade_record = array(
						'start_mark' => $start_mark,
						'end_mark' => $end_mark,
						'grade' => $grade,
						'unit_point' => $unit_point,
						'remarks' => $remarks
					);

					$rs = $this->admin_model->update_o_grade($id, $o_grade_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect('exam/o_grade');
				}
			}
		}

		public function delete_o_grade($id){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_o_grade($id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('exam/o_grade');
		}

		public function a_grade(){
			if(!($this->session->userdata('view_a_grade') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/a_grade", NULL, NULL, NULL, "'A' Level Grade System" ,NULL, NULL);
			
			$data['a_grades'] = $this->admin_model->select_a_grade();

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('exam/view_a_grade');
			$this->load->view('templates/foot');
		}

		public function add_a_grade(){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/a_grade", "add_a_grade", NULL, NULL, "'A' Level Grade System" ,"Add Grade", NULL);
			
			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->form_validation->run('add-edit_a_level_grade_system') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/new_a_grade');
				$this->load->view('templates/foot');
			}
			else{
				$start_mark = $this->input->post('start_mark');
				$end_mark = $this->input->post('end_mark');
				$grade = $this->input->post('grade');
				$remarks = $this->input->post('remarks');
				$points = $this->input->post('points');

				if($start_mark >= $end_mark){
					echo "<script>
							javascript:alert('Invalid range: End Mark should be greater than Start Mark'); 
							window.location = '".base_url()."exam/add_a_grade'
						</script>";
					exit;
				}
				else{
					$a_grade_record = array(
						'start_mark' => $start_mark,
						'end_mark' => $end_mark,
						'grade' => $grade,
						'remarks' => $remarks,
						'points' => $points
					);

					$rs = $this->admin_model->insert_new_a_grade($a_grade_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect('exam/a_grade');
				}
			}
		}

		public function edit_a_grade($id){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/a_grade", "edit_a_grade", NULL, NULL, "'A' Level Grade System" ,"Edit Grade", NULL);
			
			$data['a_grade_record'] = $this->admin_model->select_a_grade_record_by_id($id);

			$data['id'] = $id;

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->form_validation->run('add-edit_a_level_grade_system') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/edit_a_grade');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_a_grade();
			}
		}

		public function update_a_grade(){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('update_a_grade')){
				$start_mark = $this->input->post('start_mark');
				$end_mark = $this->input->post('end_mark');
				$grade = $this->input->post('grade');
				$remarks = $this->input->post('remarks');
				$points = $this->input->post('points');

				$id = $this->input->post('id');

				//$grade_exists = $this->admin_model->select_a_grade($grade)['grade'];

				if($start_mark >= $end_mark){
					echo "<script>
							javascript:alert('Invalid range: End Mark should be greater than Start Mark'); 
							window.location = '".base_url()."exam/edit_a_grade/".$id."'
						</script>";
					exit;
				}/*
				else if(empty($grade_exists)){
					echo "<script>
							javascript:alert('Invalid grade: ".$grade."'); 
							window.location = '".base_url()."exam/edit_a_grade/".$id."'
						</script>";
					exit;
				}*/
				else{
					$a_grade_record = array(
						'start_mark' => $start_mark,
						'end_mark' => $end_mark,
						'grade' => $grade,
						'remarks' => $remarks,
						'points' => $points
					);

					$rs = $this->admin_model->update_a_grade($id, $a_grade_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect('exam/a_grade');
				}
			}
		}

		public function delete_a_grade($id){
			if(!($this->session->userdata('manage_o_level_grading_system') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_a_grade($id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('exam/a_grade');
		}

		public function division(){
			if(!($this->session->userdata('view_divisions') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/division", NULL, NULL, NULL, "Division" , NULL, NULL);
			
			$data['divisions'] = $this->admin_model->select_division();
			$data['compute_division'] =  $this->admin_model->select_computing_division();
			$data['a_grades'] = $this->admin_model->select_a_grade();
			$data['o_grades'] = $this->admin_model->select_o_grade();

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('exam/view_division');
			$this->load->view('templates/foot');
		}

		public function add_division(){
			if(!($this->session->userdata('manage_division') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = $this->breadCrumb("exam/division", "add_division", NULL, NULL, "Division" , "Add Division", NULL);
			
			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->form_validation->run('add-edit_division') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/new_division');
				$this->load->view('templates/foot');
			}
			else{
				$min_points = $this->input->post('min_points');
				$max_points = $this->input->post('max_points');
				$div_name = $this->input->post('division');
				$level = $this->input->post('level');

				//$grade_exists = $this->admin_model->select_a_grade($grade)['grade'];

				if($min_points >= $max_points){
					echo "<script>
							javascript:alert('Invalid range: Max Points should be greater than Min Points'); 
							window.location = '".base_url()."exam/add_division'
						</script>";
					exit;
				}/*
				else if(empty($grade_exists)){
					echo "<script>
							javascript:alert('Invalid grade: ".$grade."'); 
							window.location = '".base_url()."exam/add_a_grade'
						</script>";
					exit;
				}*/
				else{
					$division_record = array(
						'starting_points' => $min_points,
						'ending_points' => $max_points,
						'level' => $level,
						'div_name' => $div_name
					);

					$rs = $this->admin_model->insert_new_division($division_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully added");
					}
					redirect('exam/division');
				}
			}
		}

		public function edit_division($id){
			if(!($this->session->userdata('manage_division') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$data['division_record'] = $this->admin_model->select_division_record_by_id($id);

			// echo $data['exam_type']['academic_year_id']; exit;
			$division = ($data['division_record']['level'] == "A") ? "'A'Level" : "'O'Level";

			$data['title'] = $this->breadCrumb("exam/division", "edit_division", NULL, NULL, "Division" , "Edit  ".$division."  Division ".$data['division_record']['div_name'], NULL);

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			$data['id'] = $id;

			if($this->form_validation->run('add-edit_division') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('exam/edit_division');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_division();
			}
		}

		public function update_division(){
			if(!($this->session->userdata('manage_division') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "New Division";

			if(! file_exists(APPPATH.'views/exam')){
				show_404();
			}

			if($this->input->post('update_division')){
				$min_points = $this->input->post('min_points');
				$max_points = $this->input->post('max_points');
				$div_name = $this->input->post('division');
				$level = $this->input->post('level');
				$id = $this->input->post('id');

				//$grade_exists = $this->admin_model->select_a_grade($grade)['grade'];

				if($min_points >= $max_points){
					echo "<script>
							javascript:alert('Invalid range: Max Points should be greater than Min Points'); 
							window.location = '".base_url()."exam/add_division/".$id."'
						</script>";
					exit;
				}/*
				else if(empty($grade_exists)){
					echo "<script>
							javascript:alert('Invalid grade: ".$grade."'); 
							window.location = '".base_url()."exam/add_a_grade/".$id."'
						</script>";
					exit;
				}*/
				else{
					$division_record = array(
						'starting_points' => $min_points,
						'ending_points' => $max_points,
						'level' => $level,
						'div_name' => $div_name
					);

					$rs = $this->admin_model->update_division($id, $division_record);
					if($rs){
						$this->transactionSuccessfullyCommitted("Successfully updated");
					}
					redirect('exam/division');
				}
			}
		}

		public function delete_division($id){
			if(!($this->session->userdata('manage_division') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->delete_division($id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
			redirect('exam/division');
		}

		public function enable_exam_type($id = NULL){
			if(!($this->session->userdata('enable_or_disable_exam_type') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$add_score_enabled = $this->input->post('add_score_enabled');

			$exam_record = array(
				'id' => $id,
				'add_score_enabled' => $add_score_enabled
			);

			if($this->input->post('disable')){
				$rss = $this->teacher_model->disable_all_exam_type();
			}

			$rs = $this->teacher_model->update_exam_type($id, $exam_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully enabled");
			}
			if($rss){
				$this->transactionSuccessfullyCommitted("All exam types have been disabled");
			}
			redirect('exam/exam_types');
		}

		public function add_nos_for_computing_division(){
			if($this->form_validation->run('add_cd') == FALSE){
				redirect('exam/division');
			}
			else{
				$level = $this->input->post('level');
				$nos = $this->input->post('nos');
				$penalty = $this->input->post('penalty');
				$penalty_divs[] = $this->input->post('penalty_divs');
				$assignedDiv = $this->input->post('assignedDiv');
				$penalty_grade = $this->input->post('penalty_grade');
				$str = NULL;

				if($penalty == "yes"){
					foreach ($penalty_divs as $key => $pd) {
						$str = implode(',', $pd);
					}
				}
				else{
					$str = NULL;
					$assignedDiv = NULL;
					$penalty_grade = NULL;
				}

				//echo $str . ' - ' . $assignedDiv . ' - ' . $penalty; exit;

				$record = array(
					'level' => $level,
					'nos' => $nos,
					'penalty' => $penalty,
					'penalty_divs' => $str,
					'assignedDiv' => $assignedDiv,
					'penalty_grade' => $penalty_grade				
				);

				$rs = $this->admin_model->add_nos_for_computing_division($record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
				redirect('exam/division');
			}
		}
	}
?>
