<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Year extends MY_Controller{
		public function __construct(){
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');

			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }
			//$this->errorAcademicYearSettings();
		}

		public function academic_years(){
			if(!($this->session->userdata('view_academic_years') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['academic_year_title'] = "Academic Years";
			$data['term_title'] = "Terms";
			$data['academic_years'] = $this->admin_model->select_academic_year();
			$data['terms'] = $this->admin_model->terms();

			if(! file_exists(APPPATH.'views/year')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('year/view_academic_year');
			$this->load->view('templates/foot');
		}

		public function add_academic_year(){
			if(!($this->session->userdata('add_academic_year') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "New Academic Year";

			if(! file_exists(APPPATH.'views/year')){
				show_404();
			}

			$data['mwaka_wa_masomo'] = "";
			$data['level'] = "";

			$monthName = date("F", strtotime(date('Y-m-d')));

			if($monthName == "May"){
				$data['mwaka_wa_masomo'] = date("Y", strtotime(date('Y-m-d'))) . "-" . (date("Y", strtotime(date('Y-m-d'))) + 1);
				$data['level'] = "A'Level";
			}

			if($monthName == "January"){
				$data['mwaka_wa_masomo'] = date("Y", strtotime(date('Y-m-d')));
				$data['level'] = "O'Level";
			}



			if($this->form_validation->run('add-edit_academic_year') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('year/new_academic_year');
				$this->load->view('templates/foot');
			}
			else{
				$month_of_start_date = date("m", strtotime($this->input->post('start_date')));
				$month_of_end_date = date("m", strtotime($this->input->post('end_date')));

				$yr_of_start_date = date("Y", strtotime($this->input->post('start_date')));
				$yr_of_end_date = date("Y", strtotime($this->input->post('end_date')));

				$ym_start_date = $yr_of_start_date . "-" . $month_of_start_date;
				$ym_end_date = $yr_of_end_date . "-" . $month_of_end_date;


				if($ym_start_date == date('Y-m') && $this->input->post('start_date') < $this->input->post('end_date')){
					$year = $this->input->post('year');
					$level = $this->input->post('level');
					$begin_date = $this->input->post('start_date');
					$end_date = $this->input->post('end_date');

					$rs = $this->admin_model->insert_new_academic_year($year, $level, $begin_date, $end_date);
					if($rs){
					$this->transactionSuccessfullyCommitted("New academic year has been added");
				}
					redirect('year/academic_years');
				}
				else{
					$this->session->set_flashdata('error_message', "Invalid Start Date And / Or End Date");
					redirect('year/academic_years');
				}
			}
		}

		public function set_current_term(){
			if($this->input->post('is_current')){
				if($this->input->post('is_current') === "yes"){
					$is_current = "no";
				}

				if($this->input->post('is_current') === "no"){
					$is_current = "yes";
				}
				$term_id = $this->input->post('term_id');
				
				$record = array(
					'is_current' => $is_current,
				);

				$academic_year = $this->input->post('academic_year');

				$rs = $this->admin_model->set_unset_current_term($term_id, $record, $academic_year, $is_current);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully set");
				}
				redirect('year/academic_years');
			}
		}

		public function edit_academic_year($id){
			if(!($this->session->userdata('update_academic_year') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Edit";
			$data['academic_year_record'] = $this->admin_model->select_academic_year_record_by_id($id);

			// echo $data['exam_type']['academic_year_id']; exit;

			if(! file_exists(APPPATH.'views/year')){
				show_404();
			}

			if($this->form_validation->run('add-edit_academic_year') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('year/edit_academic_year');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_academic_year();
			}
		}

		public function update_academic_year(){
			$month_of_start_date = date("m", strtotime($this->input->post('start_date')));
			$month_of_end_date = date("m", strtotime($this->input->post('end_date')));

			$yr_of_start_date = date("Y", strtotime($this->input->post('start_date')));
			$yr_of_end_date = date("Y", strtotime($this->input->post('end_date')));

			$ym_start_date = $yr_of_start_date . "-" . $month_of_start_date;
			$ym_end_date = $yr_of_end_date . "-" . $month_of_end_date;


			if($ym_start_date == date('Y-m') && $this->input->post('start_date') < $this->input->post('end_date')){
				$year = $this->input->post('year');
				$level = $this->input->post('level');
				$begin_date = $this->input->post('start_date');
				$end_date = $this->input->post('end_date');
				$id = $this->input->post('id');
				
				$academic_year_record = array(
					'year' => $year,
					'end_date' => $end_date,
					'class_level' => $level,
					'start_date' => $begin_date
				);

				$rs = $this->admin_model->update_academic_year($id, $academic_year_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('year/academic_years');
			}
			else{
				$this->session->set_flashdata('error_message', "Invalid Start Date And / Or End Date".date('Y-m'));
				redirect('year/academic_years');
			}
		}

		public function add_new_term(){

			if(! file_exists(APPPATH.'views/year')){
				show_404();
			}

			if($this->form_validation->run('add-edit_term') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('year/academic_years');
			}  
			else{
				$term_record = array(
					'term_name' => $this->input->post('term_name'),
					'end_date'=> $this->input->post('end_date'),
					'begin_date' =>$this->input->post('begin_date'),
					'aid'=>$this->input->post('academic_year')
				);

				$rs = $this->admin_model->insert_new_term($term_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("New term has been added");
				}
				redirect('year/academic_years');
			}
		}

		public function update_term(){
			if($this->form_validation->run('add-edit_term') == FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('year/academic_years');
			}  
			else{
				$aid = $this->input->post('aid');
				$term_name = $this->input->post('term_name');
				$begin_date = $this->input->post('begin_date');
				$end_date = $this->input->post('end_date');
				
				$term_record = array(
					'end_date'=> $end_date,
					'begin_date' => $begin_date
				);


				$rs = $this->admin_model->update_term($aid, $term_name, $term_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('year/academic_years');
			}
		}
	}
?>

		
