<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Library extends MY_Controller{
		function __construct(){
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}

			$this->load->model('library_model');
			$this->load->model('department_model');
			$this->load->model('admin_model');
			$this->load->library('pagination');

		//$this->errorAcademicYearSettings();
		}


		public function index(){
			$this->retbooktype();
		}

		/* insert book type */
		public function insertbooktype(){
			$this->form_validation->set_rules('isbn', 'ISBN', 'trim|required|min_length[0]|max_length[20]');
			$this->form_validation->set_rules('title', 'Book title', 'trim|required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('author', 'Author', 'trim|required|min_length[3]|max_length[100]');
			$this->form_validation->set_rules('edition', 'Edition', 'trim|required|integer|min_length[1]');
			$this->form_validation->set_rules('class', 'class', 'trim|required');



			if($this->form_validation->run()){
	    		$field = array(
					'isbn' => $this->input->post('isbn'),
					'book_title' => $this->input->post('title'),
					'author_name' => $this->input->post('author'),
					'edition' => $this->input->post('edition'),
					'class_id' => $this->input->post('class'),
					'LAST_EDIT' => date('Y-m-d H:i:s')
				);
			     /*Add data to model */
			    $isbn = $this->input->post('isbn');
			    $check_book = $this->library_model->check_book($isbn);
			    if($check_book >= 1){
			     	$this->session->set_flashdata('error_message', 'Can not duplicate the book type,the book type already exist');
			        $this->addbooktype();
				}
			    else{
			        $submit = $this->library_model->addBook($field);
			    	if($submit){
				    	$this->session->set_flashdata('response', 'Book type added successfully');
				    	$this->addbooktype();
					}else{
					    $this->session->set_flashdata('error_message', 'Can not add book type, fill all the fields');	
				    }			
			    }
	        }
	        else{
	            $this->session->set_flashdata('error_message', 'Can not add book type, fill all the fields');
	            $this->addbooktype();
	       	}
	       
		}/*  end insert book type */

	
		public function retbooktype(){	
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

		    $data['title'] = "BOOK TITLES ";
			//$this->load->database();
			$total_rows = $this->library_model->total_type($value);

			$config = array();
			$config['base_url'] = base_url() . 'library/retbooktype/'.$value;
	        $config['total_rows'] = $total_rows;
	        $config['num_links'] = 3;
	        $config['per_page'] = 10;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';

	        $data['x'] = $this->library_model->retrievebooktype($config['per_page'], $page, $value);
	        $data['links'] = $this->pagination->create_links();
	        $data['classes'] = $this->library_model->select_classes();

	       	/*--- pagination-----.*/
	       	$data['total_title'] = "Total types";
			$total['y'] = $this->library_model->total_type();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $total);
			//$this->load->view('libraries/viewbooktype',$data);
			$this->load->view('libraries/viewbooktype_new', $data);
			$this->load->view('templates/foot');
		}


		public function book_transact(){
			$this->form_validation->set_rules('isbn', 'ISBN', 'trim|required|min_length[2]|max_length[20]');
			$this->form_validation->set_rules('borID', 'Borrower ID', 'trim|required|min_length[0]|max_length[40]');
			$this->form_validation->set_rules('statusbef', 'status before', 'trim|required|min_length[3]|max_length[30]');
			$this->form_validation->set_rules('status', 'current status', 'trim|required');

			if($this->form_validation->run()){
	    		$field=array(
					'isbn' => $this->input->post('isbn'),
					'book_title' => $this->input->post('title'),
					'author_name' => $this->input->post('author'),
					'edition' => $this->input->post('edition')
				);
			    /*Add data to model */
			    $resul = $this->library_model->addBook($field);
			    if($resul){
			    	return true;
			    }
			    else{
			    	return false;
			    }
			}
			$this->loadpage1;                        
		}/*  end insert book type */
	

		public function delete_type1($typeid){
			if ($this->library_model->deleted_type($typeid)){
				$this->session->set_flashdata('response', 'Record deleted successfully');
			}
			else{
				$this->session->set_flashdata('error_message', 'Deletion failed');
			}
	      	return redirect('library/retbooktype');
		}


		public function edittype($id){
			$dat['classes'] = $this->library_model->select_classes();

			$data = $this->library_model->edit($id);
			$datas['title'] = $data->BOOK_TITLE;
			$datas['author'] = $data->AUTHOR_NAME;
			$datas['edition'] = $data->EDITION;
			$datas['isbn'] = $id;

		    $this->load->view('templates/teacher_header', $dat);
			$this->load->view('libraries/editbook_type', $datas);
			$this->load->view('templates/foot');
		}


		public function fill_editbook($id){
			$data['y'] = $this->library_model->edit_book($id); /* add code,isbn*/

		    $this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/edit_book');
			$this->load->view('templates/foot');
		}


		public function update_book_type(){
			$this->form_validation->set_rules('title', 'Book title', 'trim|required|max_length[100]|min_length[2]');
			$this->form_validation->set_rules('author', 'Author', 'trim|required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('edition', 'Edition', 'trim|integer|required');
			$this->form_validation->set_rules('isbn', 'Book ISBNN', 'trim|required|max_length[100]|min_length[0]');
			
			if($this->form_validation->run()){
				$data = array(
					/*'ISBN'=>$this->input->post('isbn'),	*/
					'BOOK_TITLE' => $this->input->post('title'),
					'AUTHOR_NAME' => $this->input->post('author'),
					'EDITION' => $this->input->post('edition'),
					'LAST_EDIT' => date('Y-m-d H:i:s')
				);
				$old_isbn = $this->input->post('isbn');
				$this->library_model->updatebook_type($data, $old_isbn);
				/*update bbook type*/

				if($this->library_model->updatebook_type($data, $old_isbn)){
					$this->session->set_flashdata('response', 'Record updated successfully');	
				}
				else{
					$this->session->set_flashdata('error_message', 'Update failed');
				}
			}
			return redirect('library/retbooktype');	
		}


		public function add_book(){
			$this->form_validation->set_rules('code','Book code','trim|required|min_length[0]|max_length[40]');

			if($this->form_validation->run()){
	    		$field=array(
					'STATUS'=>$this->input->post('status'),
					'LAST_EDIT'=>date('Y-m-d H:i:s')
				);

			    /*Add data to model */
		        $isbn = $this->input->post('isbn');
		        $code = $this->input->post('code');
			    $resul = $this->library_model->ed_indiv_Book($field,$code,$isbn);

			    if($resul){
				 	$this->session->set_flashdata('response', 'book updated succefully');
				}
				else{
					$this->session->set_flashdata('error_message', 'Failed to add book type');
				}
			}		    
			/*$id=$this->input->post('isbn');*/                      
			return redirect('library/book1_report','refresh');	
		}/* end libraries controller*/


		public function book1_report(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }


			$report['title'] = "All Copies";

			//$this->load->database();
			$config = array();
			$config['base_url'] = base_url() . 'library/book1_report/'.$value;
	        $config['total_rows'] = $this->library_model->total_books($value);;
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';

	        $report['y'] = $this->library_model->ind_book_report($config['per_page'], $page, $value);
	        $report['links'] = $this->pagination->create_links();
		   
		    $report['x'] = $this->library_model->total_books();

		    $report['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
		    
		    $this->load->view('templates/teacher_header', $report);
			//$this->load->view('libraries/book1_report');
			$this->load->view('libraries/book1_report_new');
			$this->load->view('templates/foot');
		}


		/* fill data to book borrowing panel */
		public function to_booktransct($id){
			$data = $this->library_model->ad_to_book($id);
			$title = $this->library_model->add_title($id);
			$datas['type'] = $this->library_model->type();
			$datas['code'] = $id;
			$datas['title'] = $title->BOOK_TITLE;
			$datas['author'] = $title->AUTHOR_NAME;
			$datas['edition'] = $title->EDITION;
			$datas['isbn'] = $data->isbn;

			$this->load->view('templates/teacher_header', $datas);
			$this->load->view('libraries/borrow_book');
			$this->load->view('templates/foot');
		}


		public function borrow_book(){
		    $borrower = $this->input->post('borrower');	    
		    $id = $this->input->post('code');
		    $isbn = $this->input->post('isbn');
		    $date_ = $this->input->post('date');
		    $current_date = date('Y-m-d');
		    $borrower_type = $this->library_model->get_borrower_name($borrower)['b_type'];

		    $this->form_validation->set_rules('code','Book Code','trim|required|min_length[0]|max_length[40]');
			$this->form_validation->set_rules('isbn','ISBN','trim|required|min_length[0]|max_length[30]');
			$this->form_validation->set_rules('borrower','Borrower ID','trim|required');
			$this->form_validation->set_rules('date','Date','trim|required');
			/*$this->form_validation->set_rules('status','Status','trim|required');*/
			if ($this->form_validation->run()){
				$field=array(
					'ID' => $id,
					   'ISBN' => $isbn,
					     'BORROWER_ID' => $borrower,
					       'RETURN_DATE' => $date_,
					          'FLAG' => 'borrowed',
					             'DATE_BORROWED' => date('Y-m-d H:i:s'),
					               'BORROWER_TYPE' => $borrower_type,
					                  'LAST_EDIT' => date('Y-m-d H:i:s')
				);

	            $avoid_same = $this->library_model->avoid_same($borrower, $id, $isbn);
			    $check = $this->library_model->check_user($borrower);//return user existence//
	            $count = $this->library_model->count_borrowed($borrower);//return nm of books borrowed//

		        if ($check == 1) {//checks if user exists//
		            if ($avoid_same < 1) {	          
	                	if($count < $this->library_model->check_borrowing_limit($borrower_type)){//checks if num of copies exceeds 4//
	                		if($current_date < $date_){
	                			$this->library_model->borrow($field);	
								$this->session->set_flashdata('response', 'Action confirmed');
								return $this->available();
	                		}
							else{
								$this->session->set_flashdata('error_message', 'Invalid Date: The Return Date must be ....');
							}
				     	}
					    else{
						$this->session->set_flashdata('error_message','Action cannot be done. Borrowing limit !, '.$count .' copies found for '.$borrower_type.'&nbsp , Required max '.$this->library_model->check_borrowing_limit($borrower_type));
		                }
		            }
		            else{
		            	$this->session->set_flashdata('error_message', 'The copy already borrowed by the same user !, try other available copies');              
		            }
	            }
	            else{             
	             	$this->session->set_flashdata('error_message', 'The borrower does not exists');
	            }
			}
			return $this->available();
		}


		public function delete_book($typeid){
			if($this->library_model->delete_book($typeid)){
			 	$this->session->set_flashdata('response', 'Record deleted successfully');
			}
			else{
				$this->session->set_flashdata('error_message', 'Deletion failed');
			}
			return redirect('library/book1_report');
		}


		public function class_loader(){
			$query = $this->library_model->class_load();

		    if($query){
		    	$data['class'] = $query;
		  
		       	$this->load->view('templates/teacher_header');
				$this->load->view('libraries/addbooktype', $data);
				$this->load->view('templates/foot');
			}
		}


		public function retrieve_isbn($id){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
		    }

			//$this->load->database();
			$config = array();
			$config['base_url'] = base_url().'library/retrieve_isbn/'.$id.'/'.$value;
	        $config['total_rows'] = $this->library_model->total_byISBN($id, $value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
	        $data['books'] = $this->library_model->view_type_in($id, $config['per_page'], $page, $value);
	        $data['links'] = $this->pagination->create_links();

			$data['isbn'] = $id;
			$data['x'] = $this->library_model->total_byISBN($id);
			//$data['y'] = $this->library_model->total_books();
			$dat = $this->library_model->type_title($id);

			$data['title'] = $dat->BOOK_TITLE;
			//$data['type'] = $this->library_model->type();
			$data['author'] = $dat->AUTHOR_NAME;
			$data['edition'] = $dat->EDITION;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header',$data);
			//$this->load->view('libraries/book_isbn');
			$this->load->view('libraries/book_isbn_new');
			$this->load->view('templates/foot');
		}


		public function fill_book($id){
			$data['number'] = '';
			$data['isbn'] = $id;
			$dat = $this->library_model->fill_book($id);
			$data['title'] = $dat->BOOK_TITLE;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/viewbooktype');
			$this->load->view('templates/foot');
		}

		//Lists of borrowed books//
		public function borrowed(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

			//$this->load->database();
			$config = array();
			$config['base_url'] = base_url().'library/borrowed/'.$value;
	        $config['total_rows'] = $this->library_model->total_borrowed($value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
	        $data['x'] = $this->library_model->borrowed($config['per_page'], $page, $value);
	        $data['links'] = $this->pagination->create_links();
		   
	        $data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
		
		    $data['total'] = $this->library_model->total_borrowed();
			$this->load->view('templates/teacher_header', $data);
			//$this->load->view('libraries/borrowed');
			$this->load->view('libraries/borrowed_new');
			$this->load->view('templates/foot');
		}
		//Lists of borrowed books//


		public function add_number(){
			$data['isbn'] = '';
			$data['title'] = '';

			$number = $this->input->post('number');
		 	if(isset($number) and !empty($number)){
				$data['number'] = $number;

				$this->load->view('templates/teacher_header');
				$this->load->view('sample/new_books', $data);
				$this->load->view('templates/foot');
			}
			else{
				$number = 0;
			}
		}


		public function save_books(){
			$data['books'] = $this->input->post('book');
			$isbn = $this->input->post('isbn');

			foreach ($data['books'] as $key => $value) {
				$book_record[] = array(
					'ID' => $value,
					'isbn' => $isbn,
					'LAST_EDIT' => date('Y-m-d H:i:s')
				);
			}
			$data = $this->library_model->insert_books($book_record);

			if ($data){
				$this->session->set_flashdata('response','Books saved');
			}
			else{
				$this->session->set_flashdata('error_message','Book copies were not inserted!,you have inserted copies minimum 10,required is 10 complete');
			}
 			return redirect('library/retbooktype');			
		}


		public function available(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

	        //$this->load->database();
			$config = array();
			$config['base_url'] = base_url().'library/available/'.$value;
	        $config['total_rows'] = $this->library_model->total_available($value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';

	       	$data['x'] = $this->library_model->available($config['per_page'], $page, $value);
	       	$data['links'] = $this->pagination->create_links();
		   
	       	$data['total'] = $this->library_model->total_books();
	       	$data['type'] = $this->library_model->type();
	       	$data['all'] = $this->library_model->total_available();

	       	$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		   	$this->load->view('templates/teacher_header', $data);
		   	//$this->load->view('libraries/availables');
		   	$this->load->view('libraries/availables_new');
		   	$this->load->view('templates/foot');
		}


		public function fill_return_book($id){
	        $data = $this->library_model->fill_to_returnbook($id);
			$datas['title'] = $data->BOOK_TITLE;
			$datas['author'] = $data->AUTHOR_NAME;
			$datas['code'] = $data->ID;
		    $datas['transaction_id'] = $data->TRANSACTION_ID;
			$datas['edition'] = $data->EDITION;
			$datas['isbn'] = $data->ISBN;

			$this->load->view('templates/teacher_header', $datas);
			$this->load->view('libraries/return_book');
			$this->load->view('templates/foot');
		}


		public function return_book(){ 
	     	$field = array(     	       
				'STATUS_AFTER' => $this->input->post('status'),
				'FLAG' => $this->input->post('availability'),
				'DATE_DUE_BACK' => date("Y-m-d"),
				'LAST_EDIT' => date('Y-m-d H:i:s')				
			);

	     	$book = array(
	        	'LAST_EDIT' => date('Y-m-d H:i:s'),
	        	'STATUS' => $this->input->post('availability')	
	     	);

	        $code = $this->input->post('code');
	        $isbn = $this->input->post('isbn');
	        $resul = $this->library_model->return_book($field, $book, $isbn, $code);
			  
			if ($resul){
			    return redirect('library/borrowed', 'refresh');
				$this->session->set_flashdata('response', 'action done successfully');			     
			}
			else{
				$this->session->set_flashdata('error_message', 'Error, action failed');			     
			}                     
			return redirect('library/borrowed', 'refresh');	
		}


		public function returned_book(){
			$data['x']=$this->library_model->returned_book();
			/*$data['total']=$this->library_model->total_books();
			$data['all']=$this->library_model->total_available();
			*/
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/returned_book');
			$this->load->view('templates/foot');
		}


		public function lost_books(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

	    	//$this->load->database();
			$config  =  array();
			$config['base_url'] = base_url().'library/lost_books/'.$value;
	        $config['total_rows'] = $this->library_model->total_lost_books($value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
	       	$data['x'] = $this->library_model->lost_books($config['per_page'], $page, $value);

	       	$data['links'] = $this->pagination->create_links();

	       	$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		   	$this->load->view('templates/teacher_header', $data);
		   	//$this->load->view('libraries/borrowing_history');
		   	$this->load->view('libraries/lost_books_new');
		   	$this->load->view('templates/foot');
		}


		public function borrowing_history(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

	    	//$this->load->database();
			$config  =  array();
			$config['base_url'] = base_url().'library/borrowing_history/'.$value;
	        $config['total_rows'] = $this->library_model->total_history($value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
	       	$data['x'] = $this->library_model->borrowing_history($config['per_page'], $page, $value);

	       	$data['links'] = $this->pagination->create_links();

	       	$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		   	$this->load->view('templates/teacher_header', $data);
		   	//$this->load->view('libraries/borrowing_history');
		   	$this->load->view('libraries/borrowing_history_new');
		   	$this->load->view('templates/foot');	
		}


		public function title_sum_up(){//Presents title sum up report//
			$data['t'] = $this->library_model->total_type();
			$data['borrowed'] = $this->library_model->total_borrowed();
			$data['lost'] = $this->library_model->lost_book_total();
			$data['available']= $this->library_model->total_available();
			$data['all'] = $this->library_model->total_books();
			$data['normal'] = $this->library_model->total_normal_book();
			$data['poor'] = $this->library_model->total_poor_book();
			$data['x'] = $this->library_model->total_books_remained();
			$data['s'] = $this->library_model->title_sum_up();

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/book_general_report');
			$this->load->view('templates/foot');

		}

		public function addbooktype(){
			$data['classes'] = $this->library_model->select_classes();

			if(! file_exists(APPPATH.'views/libraries')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			//$this->load->view('libraries/addbooktype');
			$this->load->view('libraries/addbooktype_new');
			$this->load->view('templates/foot');
		}


		public function expired_books(){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		    }

	    	//$this->load->database();
			$config  =  array();
			$config['base_url'] = base_url().'library/expired_books/'.$value;
	        $config['total_rows'] = $this->library_model->total_expired_books($value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
	       	$data['x'] = $this->library_model->expired_books($config['per_page'], $page, $value);

	       	$data['links'] = $this->pagination->create_links();

	       	$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

		   	$this->load->view('templates/teacher_header', $data);
		   	//$this->load->view('libraries/borrowing_history');
		   	$this->load->view('libraries/expired_books_new');
		   	$this->load->view('templates/foot');
		}


		public function add_copies($isbn){
			$total_available_copies = $this->library_model->get_total_available_copies($isbn)['total_available_copies'];
			$no_of_copies = $this->input->post('no_of_copies');

	 		for ($i = 1; $i <= $no_of_copies; $i++) { 
	 			$copy_number = $total_available_copies + $i;
				$record[] = array(
					'ISBN' => $isbn,
					'ID' => $copy_number,
					'LAST_EDIT' => date('Y-m-d')
				);
			}

			$this->library_model->add_copies_by_isbn($record);
			redirect(base_url() . 'library/retbooktype');
		}


		public function dept_borrow($id){
			if($this->input->post('search_record') != ""){
		        $value = trim($this->input->post('search_record'));
		        $this->no_record_found_message();
		    }
		    else{
		        $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
		    }

			//$this->load->database();
			$config = array();
			$config['base_url'] = base_url().'library/dept_borrow/'.$id.'/'.$value;
	        $config['total_rows'] = $this->library_model->total_byISBN($id, $value);
	        $config['per_page'] = 10;
	        $config['num_links'] = 3;
	        $config['full_tag_open'] = "<url class='pagination'>";
	        $config['full_tag_close'] = "</ul>";
	        $config['num_tag_open'] = "<li>";
	        $config['num_tag_close'] = "</li>";
	        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] = "<li>";
	        $config['next_tag_close'] = "</li>";
	        $config['prev_tag_open'] = "<li>";
	        $config['prev_tag_close'] = "</li>";
	        $config['first_tag_open'] = "<li>";
	        $config['first_tag_close'] = "</li>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag_close'] = "</li>";

	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
	        $data['books'] = $this->library_model->view_type_in($id, $config['per_page'], $page, $value);
	        $data['links'] = $this->pagination->create_links();

			$data['isbn'] = $id;
			$data['x'] = $this->library_model->total_byISBN($id);
			$dat = $this->library_model->type_title($id);

			$data['title'] = $dat->BOOK_TITLE;
			$data['author'] = $dat->AUTHOR_NAME;
			$data['edition'] = $dat->EDITION;

			//For populating select element with departments
			$data['departments'] = $this->department_model->view_all_depts();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/department_borrower');
			$this->load->view('templates/foot');
		}


		public function borrow_book_department($isbn){
			$data['book_copies'] = $this->input->post('book_copies');
			$borrower = $this->input->post('dept_id');
		    $date_ = $this->input->post('date');
		    $current_date = date('Y-m-d');

		    $borrowing_copies = count($data['book_copies']);

		    $count = $this->library_model->count_borrowed($borrower);

		    $count_borrowing_limit = $this->library_model->check_borrowing_limit($this->library_model->get_borrower_name($borrower)['b_type']);


		    $this->form_validation->set_rules('date','Return Date', 'trim|required');
		    $this->form_validation->set_rules('dept_id','Department', 'trim|required');

		    if($this->form_validation->run()){
		    	if($data['book_copies'] == TRUE){
					foreach ($data['book_copies'] as $key => $value){
						$record[] = array(
							'ID' => $value,
							'ISBN' => $isbn,
							'BORROWER_ID' => $borrower,
							'RETURN_DATE' => $date_,
							'FLAG' => 'borrowed',
							'DATE_BORROWED' => date('Y-m-d H:i:s'),
							'BORROWER_TYPE' => $this->library_model->get_borrower_name($borrower)['b_type'],
							'LAST_EDIT' => date('Y-m-d H:i:s')
						);

					}
					/*echo "<pre>";
						print_r($record);
					echo "</pre>"; die*/
					if($current_date < $date_){
						if(($borrowing_copies + $count) <= $count_borrowing_limit){
							$this->library_model->borrow_book_department($record);
							$this->session->set_flashdata('response', 'Successfully borrowed');
						}
						 else{
					    	$this->session->set_flashdata('error_message','Action cannot be done. Borrowing limit !, '.$count .' copies found for '.$this->library_model->get_borrower_name($borrower)['b_type'].'&nbsp , Required max '.$this->library_model->check_borrowing_limit($this->library_model->get_borrower_name($borrower)['b_type']));
					    }
					}
					else{
						$this->session->set_flashdata('error_message', 'Invalid date');
					}				
				}
				else{
					$this->session->set_flashdata('error_message', 'No Book Code has been selected');
				}
				$this->dept_borrow($isbn);
			}
			else{
				//Weka Flashdata ya failure hapa ......
				$this->dept_borrow($isbn);
			}		
		}


		public function check_borrower(){
			$value = $this->input->post('search');

			$query = $this->library_model->check_if_borrower_exist($value);
			if($query === 1){
				$borrower = $this->library_model->get_borrower_name($value)['borrower_description'];
				echo "Borrower Name: <strong class='text-primary'>" . $borrower . "<strong> ";
			}
			else{
				echo "<strong class='text-danger'>Does NOT Exist</strong>";
			}
		}

		public function new_borrow_book($isbn, $id){

			if(! file_exists(APPPATH.'views/libraries')){
				show_404();
			}

			$data['row'] = $this->library_model->select_book_by_isbn_and_code($isbn, $id);

			$this->load->view('templates/teacher_header', $data);
			//$this->load->view('libraries/addbooktype');
			$this->load->view('libraries/borrow_book_new');
			$this->load->view('templates/foot');
		}

		public function edit_btype($id){
			$data['title'] = "Borrowing Limit";
			if(! file_exists(APPPATH.'views/libraries')){
				show_404();
			}

			$data['borrower'] = $this->library_model->select_btype($id);

			//echo $data['borrower']['b_type']; exit;

			$this->form_validation->set_rules('b_type','Borrower Type','trim|required');
		    $this->form_validation->set_rules('b_limit','Borrowing Limit','trim|required|numeric');

		    if($this->form_validation->run() == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('libraries/borrowing_limit');
				$this->load->view('templates/foot');
			}
			else{
				$record = array(
					'borrowing_limit' => $this->input->post('b_limit')
				);
				$this->library_model->update_borrowing_limit($id, $record);
				//Weka Flash Data Hapa......
				$this->borrower_types();
			}
		}


		public function borrower_types(){
			$data['title'] = "Borrower Types";
			if(! file_exists(APPPATH.'views/libraries')){
				show_404();
			}

			//For populating select element with departments
			$data['borrower_types'] = $this->library_model->get_borrower_types();

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('libraries/borrower_types');
			$this->load->view('templates/foot');
		}

		public function add_borrowing_limit(){
			$data['title'] = "Add Borrowing Linit";

			if(! file_exists(APPPATH.'views/libraries')){
				show_404();
			}

			$this->form_validation->set_rules('b_type','Borrower Type','trim|required');
		    $this->form_validation->set_rules('b_limit','Borrowing Limit','trim|required|numeric');

		    $data['borrowers'] = $this->library_model->select_borrowers();

			if($this->form_validation->run() == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('libraries/add_borrowing_limit');
				$this->load->view('templates/foot');
			}
			else{
				$b_type = $this->input->post('b_type');
				$b_limit = $this->input->post('b_limit');
				$record = array(
					'b_type' => $b_type,
					'borrowing_limit' => $b_limit
				);

				$rs = $this->library_model->add_borrowing_limit($record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Record successfully added");
				}
				redirect('library/borrower_types');
			}
		}

	}


