<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Import_Export_CSV extends MY_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->library('excel');
			$this->load->library('csvimport');
			$this->load->model('student_model');

			//$this->errorAcademicYearSettings();
		}

		// EXPORT DATA @den
		public function action(){
			$this->load->library("excel");
			$object = new PHPExcel();

			$object->setActiveSheetIndex(0);

			$table_columns = array("ISBN", "AUTHOR NAME", "EDITION", "BOOK TITLE");

			$column = 0;

			foreach($table_columns as $field)
			{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
			}

			$employee_data = $this->Library_op->excel();

			$excel_row = 2;

			foreach($employee_data as $row)
			{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['ISBN']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['AUTHOR_NAME']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['EDITION']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['BOOK_TITLE']);

			$excel_row++;
			}

			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Book titles.xls"');
			$object_writer->save('php://output');
		}

		//import data csv @den
		public function upload_selected_students(){
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
			foreach($file_data as $row){
				$data[] = array(
						'examination_no'=>$row["examination_no"],
				    	'first_name' => $row["first name"],
				    	'last_name'  => $row["last name"],
				    	'dob' => $row['dob'],
				    	'former_school' => $row['former_school'],
				    	'gender' => $row['gender'],
				    	'nationality' => $row['nationality'],				    	
				    	'ward' => $row['ward'],
				    	'orphan' => $row['orphan'],
				    	'g_firstname' => $row['g_firstname'],
				    	'g_lastname' => $row['g_lastname'],
				    	'disability' => $row['disability'],
				    	'region' => $row['region'],
				    	'district' => $row['district'],
				    	'tel_no'   => $row['tel_no'],
				    	'box' => $row['box'],
				    	'email' => $row["email"],
				    	'form' => $row['form'],
				    	'reported' => $row['reported']/*,
				    	'academic_year' => 6*/
				);
			}
			$this->student_model->upload_selected_students($data);
			//redirect('library/import_form');
		}


		//import data csv @den
		public function upload_results(){
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
			foreach($file_data as $row){
				$data[] = array(
						'admission_no' => $row["admission_no"],
				    	'subject_id' => $row["first subject_id"],
				    	'etype_id'  => $row["last etype_id"],
				    	'class_stream_id' => $row['class_stream_id'],
				    	'e_date' => $row['e_date'],
				    	'term_id' => $row['term_id'],
				    	'marks' => $row['marks']
				);
			}
			$this->student_model->upload_results($data);
		}
	}