<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Students extends MY_Controller{
		public function __construct(){

			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->load->model('admin_model');
			//$this->load->model('student_model');
			//$this->load->model('subject_model');
			//$this->load->model('teacher_model');
			//$this->load->model('class_model');
			//$this->load->model('dormitory_model');
			$this->load->library('pagination');
			//$data['title'] = "NEVER GET ON A BAD SIDE OF SMALL MINDED PEOPLE WHO HAVE A LITTLE AUTHORITY";
			//$data['title'] = "I HAVE NEVER LET MY SCHOOLING GET IN THE WAY OF MY EDUCATION";
		
			if($this->session->userdata('user_role') !== "Admin"){	
				$this->refreshRoles();
			}
			//$this->errorAcademicYearSettings();	
		}

		//Result for a single student
		public function result($admission_no, $term_id){
			//$data['title'] = "NEVER GET ON A BAD SIDE OF SMALL MINDED PEOPLE WHO HAVE A LITTLE AUTHORITY";
			$data['title'] = "Students Results";
			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);

			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $data['record']['class_stream_id']){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['admission_no'] = $admission_no;
			$data['term_id'] = $term_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/matokeo_single_msm');	
			$this->load->view('templates/foot');
		}

		//Result for a single student
		public function result_b($admission_no, $term_id){
			//$data['title'] = "NEVER GET ON A BAD SIDE OF SMALL MINDED PEOPLE WHO HAVE A LITTLE AUTHORITY";
			$data['title'] = "Students Results";
			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);


			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}

			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $data['record']['class_stream_id']){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['admission_no'] = $admission_no;
			$data['term_id'] = $term_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/matokeo_single_bwiru');	
			$this->load->view('templates/foot');
		}

		public function progressive_results($admission_no){
			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['image']=$this->student_model->profile_picture($admission_no);

			$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level'])['term_id'];

			$data['class_level_array'] = $this->student_model->get_student_class_level($admission_no);

			$data['class_names'] = $this->student_model->class_name_for_prog_res($admission_no);

			if($this->input->post('get_results')){
				$term_id = $this->input->post('term_id');
				$class_id = $this->input->post('class_id');
				//echo $term_id;
			}
			else{
				$term_id = $data['term_id'];
				$class_id = $this->student_model->select_class_by_std_and_term($admission_no, $term_id);
				//echo $term_id;

			}
			/*echo $term_id;
			exit;*/

			$data['mkeka_single'] = $this->student_model->select_single_student_result_by_term($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($class_id, $term_id);


			$data['records'] = $this->student_model->select_student_enrollment_history($admission_no);

			$data['adm_no'] = $admission_no;

			$data['title'] = "Results: " . $this->input->post('get_results');

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/student_profile_header');
			$this->load->view('students/student_result_header');
			$this->load->view('students/progressive_result');
			$this->load->view('templates/foot');
		}

		public function get_students_by_class($class_stream_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_students_in_class') == 'ok' || ($this->session->userdata('view_students_in_own_class') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

	        //echo $class_stream_id . ": " . $value;exit;

			$config = array();
			$config['base_url'] = base_url() . 'students/get_students_by_class/'.$class_stream_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_per_stream($class_stream_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);

			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['students'] = $this->student_model->fetch_students_by_stream($class_stream_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			//$class_stream_id = $this->input->get('cid');
			//$class_stream_id = $this->input->get('cid');

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;

			$data['class'] = $this->class_model->select_class_name_and_stream($class_stream_id);
			$data['stream'] = $this->class_model->select_class_name_and_stream($class_stream_id);

			$get_level = $this->class_model->select_class_name_and_stream($class_stream_id)['level'];
			$data['level'] = $this->student_model->select_current_academic_year_by_level(''.$get_level.'');

			$data['title'] = $data['class']['class_name'] . " " . $data['stream']['stream'];
			//$data['students'] = $this->student_model->select_students_by_class($class_stream_id);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_students_by_stream');
			$this->load->view('templates/foot');
		}

		public function fetch_student_by_class_stream(){
			$cid = $this->input->get('cid');
			//$cid = "C04E";
			$fetch_data = $this->student_model->make_datatablesa($cid);
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				$sub_array[] = $row->admission_no;
				$sub_array[] = $row->firstname;
				$sub_array[] = $row->lastname;
				$sub_array[] = $row->class_name;
				$sub_array[] = $row->stream;
				$sub_array[] = '<a href="'.base_url() . 'students/get_student_profile/' . $row->admission_no.'" data-toggle="tooltip" data-placement="right" title="View Profile"><img src="'.base_url().'assets/images/22.png"></img></a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="'.base_url() . 'students/edit/' . $row->admission_no.'" class="fa fa-pencil" data-toggle="tooltip" data-placement="right" title="Edit"></a>';
				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->student_model->get_all_dataa($cid),
				"recordsFiltered"	=>	$this->student_model->get_filtered_dataa($cid),
				"data"				=>	$data
			);
			echo json_encode($output);
		}

		
		

		public function registered(){
			//if(!($this->errorAcademicYearSettings())):
				if(!($this->session->userdata('view_registered_students') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}

				if($this->input->post('search_student') != ""){
		            $value = trim($this->input->post('search_student'));
		            $this->no_record_found_message();
		        }
		        else{
		            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
		        }
				$config = array();
				$config['base_url'] = base_url() . 'students/registered/'.$value;
				$config['total_rows'] = $this->admin_model->count_all_students($value);
				$config['per_page'] = 10;
				$config['num_links'] = 3;
				//$config['url_segment'] = 3;

				$config['full_tag_open'] = "<ul class='pagination'>";
				$config['full_tag_close'] = "</ul>";
				$config['num_tag_open'] = "<li>";
				$config['num_tag_close'] = "</li>";
				$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
				$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
				$config['next_tag_open'] = "<li>";
				$config['next_tagl_close'] = "</li>";
				$config['prev_tag_open'] = "<li>";
				$config['prev_tagl_close'] = "</li>";
				$config['first_tag_open'] = "<li>";
				$config['first_tagl_close'] = "</li>";
				$config['last_tag_open'] = "<li>";
				$config['last_tagl_close'] = "</li>";

				$this->pagination->initialize($config);
				//$page = $this->uri->segment(3);
				$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
				$data['students'] = $this->admin_model->fetch_students($config['per_page'], $page, $value);
				$data['links'] = $this->pagination->create_links();

				$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

	            if(! file_exists(APPPATH.'views/students')){
					show_404();
				}

				$data['display_back'] = "NO";

				$data['title'] = "All Students";
				//$data['students'] = $this->student_model->select_students_by_class();


				$status = "optional";
				$data['op_subjects'] = $this->subject_model->select_optional_subjects($status);//For populating
				$data['subjects'] = $this->subject_model->select_from_subjects();	//select element

				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/view_all_students_registrar');
				$this->load->view('templates/foot');
			//endif;
		}

		
		public function fetch_class_streams(){
			$fetch_data = $this->class_model->make_datatables();
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				//$sub_array[] = $row->class_id;
				//$sub_array[] = $row->class_stream_id;
				$sub_array[] = $row->class_name;
				$sub_array[] = $row->stream;
				$sub_array[] = $row->capacity;
				$sub_array[] = $row->enrolled;
				/*$sub_array[] = '<button type="button" name="update" id="'.$row->class_stream_id.'" class="btn btn-primary btn-xs" >Edit</button>';*/
				$sub_array[] = ''.form_open("students/register_student").'
								<input type="hidden" name="class_stream_id" value="'.$row->class_stream_id.'" />
						<input type="hidden" name="class_id" value="'.$row->class_id.'" />
						<input type="hidden" name="class_name" value="'.$row->class_name.'" />
						<input type="hidden" name="stream" value="'.$row->stream.'" />
						<input type="hidden" name="id" value="'.$row->stream.'" />
						<input type="hidden" name="year" value="'.$row->year.'" />
								<input type="submit" name="add_new_student" class="btn btn-primary btn-xs "  />
								'.form_close();

				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->class_model->get_all_data(),
				"recordsFiltered"	=>	$this->class_model->get_filtered_data(),
				"data"				=>	$data
			);
			echo json_encode($output);
		}

		//This function adds the already registered student to the partucular class stream
		public function add_student($class_stream_id){
			if(!($this->session->userdata('shift_student_to_another_stream') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Allocate Student To Class Stream";

			/*$data['dormitories'] = $this->dormitory_model->view_domitories();*/
			$class_id = $this->class_model->select_class_name_and_stream($class_stream_id)['class_id'];
			$class_name = $this->class_model->select_class_name_and_stream($class_stream_id)['class_name'];
			$data['students'] = $this->student_model->get_students_not_in_class_stream($class_stream_id, $class_id);

			$data['class_stream_id'] = $class_stream_id;
			$data['class_name'] = $class_name;

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->form_validation->set_rules('admission_no', 'Student Names', 'required');

			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/add_student_to_class_stream');
				$this->load->view('templates/foot');
			}
			else{
				$admission_no = $this->input->post('admission_no');
				$aid = $this->admin_model->select_academic_year_by_class_stream($class_stream_id)['id'];

				$record = array(
					'class_stream_id' => $class_stream_id
				);

				

				$rs = $this->student_model->shift_student_to_cs($admission_no, $class_stream_id, $aid, $record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully shifted");
				}
				redirect(base_url('students/get_students_by_class/'.$class_stream_id));
			}
		}

		public function expel_student($admission_no){
			if(!($this->session->userdata('suspend_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data = array('stte_out' => 'expelled', 'status' => 'inactive');
			$reason_data = array(
				'admission_no' => $admission_no,
				'suspension_reason' => $this->input->post('suspension_reason'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => '2070-01-01'
			);

			$discipline_record = array(
				'admission_no' => $admission_no,
				'description' => $this->input->post('suspension_reason'),
				'decision' => 'EXPELLED FROM STUDIES',
				'date' => date('Y-m-d')
			);

			$rs = $this->student_model->delete_student($admission_no, $data, $reason_data, $discipline_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully expelled");
			}
			redirect('students/registered');
		}

		public function suspend_student($admission_no){
			if(!($this->session->userdata('suspend_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data = array('stte_out' => 'suspended', 'status' => 'active');
			$reason_data = array(
				'admission_no' => $admission_no,
				'suspension_reason' => $this->input->post('suspension_reason'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date')
			);

			$suspension_duration = (strtotime(''.$this->input->post('to_date').'') - strtotime(''.$this->input->post('from_date').''))/(24*60*60);

			$discipline_record = array(
				'admission_no' => $admission_no,
				'description' => $this->input->post('suspension_reason'),
				'decision' => 'SUSPENDED FOR ' . $suspension_duration ." Days",
				'date' => date('Y-m-d')
			);

			$rs = $this->student_model->delete_student($admission_no, $data, $reason_data, $discipline_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully suspended");
			}
			redirect('students/registered');
		}

		public function check_existance(){
			$value = $this->input->post('search');

			$query = $this->student_model->check_if_exist($value);
			if($query === 1){
				$fn = $this->student_model->get_selected_student_by_exam_no($value)['firstname'];
				$exam_no = $this->student_model->get_selected_student_by_exam_no($value)['examination_no'];
				echo "<a href='".base_url()."students/register_student'>".$fn."</a>";
			}
			else{
				//echo "User Does Not Exist";
			}
		}

		public function add_selected(){
			if(!($this->session->userdata('manage_selected_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Add New";


			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['regions'] = $this->student_model->select_regions();

			if($this->form_validation->run('add_new_selected') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/new_selected_student');
				$this->load->view('templates/foot');
			}
			else{
				$exam_no = $this->input->post('exam_no');
				$fn = $this->input->post('firstname');
				$ln = $this->input->post('lastname');
				//$mn = $this->input->post('smiddlename');
				$gender = $this->input->post('gender');
				/*$day = $this->input->post('day');
				$month = $this->input->post('month');
				$year = $this->input->post('year');*/
				$nationality = $this->input->post('nationality');
				$box = $this->input->post('box');
				$phone_no = $this->input->post('tel_no');
				$region = $this->input->post('region');
				$email = $this->input->post('email');
				$ward = $this->input->post('ward');
				$district = $this->input->post('district');
				$orphan = $this->input->post('orphan');
				$form = $this->input->post('form');
				$former_school = $this->input->post('former_school');
				$g_firstname = $this->input->post('g_firstname');
				$g_lastname = $this->input->post('g_lastname');

				//$dob = $year . '-' . $month . '-' . $day;
				$dob = $this->input->post('dob');
				//$doa = $this->input->post('doa');

				$get_level = explode('-', $form);

				$id = $this->student_model->select_current_academic_year_by_level(''.$get_level[1].'')['id'];

				/*echo $exam_no . "<br />";
				echo $fn . "<br />";
				echo $ln . "<br />";
				echo $gender . "<br />";
				echo $dob . "<br />";
				echo $nationality . "<br />";
				echo $box . "<br />";
				echo $region . "<br />";
				echo $email . "<br />";
				echo $ward . "<br />";
				echo $district . "<br />";
				echo $orphan . "<br />";
				echo $form . "<br />";
				echo $former_school . "<br />";
				echo $g_firstname . "<br />";
				echo $g_lastname . "<br />";*/

				$selected_student_record = array(
					'examination_no' => $exam_no,
					'firstname' => $fn,
					'lastname' => $ln,
                                      //  'smiddlename' => $mn,
					'gender' => $gender,
					'form' => $get_level[0],
					'orphan' => $orphan,
					'nationality' => $nationality,
					'ward' => $ward,
					'district' => $district,
					'region' => $region,
					'tel_no' => $phone_no,
					'box' => $box,
					'email' => $email,
					'dob' => $dob,
					//'doa' => $doa,
					'former_school' => $former_school,
					'g_firstname' => $g_firstname,
					'g_lastname' => $g_lastname,
					'academic_year' => $id

				);

				$rs = $this->student_model->insert_selected_students($selected_student_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
				if($get_level[0] == "I"){
					redirect('students/selected/form_one');	
				}
				if($get_level[0] == "V"){
					redirect('students/selected/form_five');	
				}
							
			}
		}

		public function mark_reported($exam_no, $reported){
			if($reported === 'yes'){
				if(!($this->session->userdata('mark_as_unreported') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}
				$record = array(
					'reported' => 'no'
				);
			}
			if($reported === 'no'){
				if(!($this->session->userdata('mark_as_reported') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}
				$record = array(
					'reported' => 'yes'
				);
			}		

			$rs = $this->student_model->mark_as_reported($exam_no, $record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('students/selected');
		}

		public function selected($form = NULL){
			if(!($this->session->userdata('view_selected_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$transfer_status = "OUT";
			$config = array();
			$config['base_url'] = base_url() . 'students/selected/'.$value;
			$config['total_rows'] = $this->student_model->count_all_selected_students($form, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';

			$data['students'] = array();
			$data['count_reported'] = array();


			$data['form'] = $form; //Initialize kuepuka error
			if($form == "form_one"):
				$data['form'] = $form;
				$form = "I";
			$data['students'] = $this->student_model->retrieve_all_selected_students($form, $config['per_page'], $page, $value);
			$data['count_reported'] = $this->student_model->count_selected_students($form);
			endif;
			if($form == "form_five"):
				$data['form'] = $form;
				$form = "V";
			$data['students'] = $this->student_model->retrieve_all_selected_students($form, $config['per_page'], $page, $value);
			$data['count_reported'] = $this->student_model->count_selected_students($form);
			endif;
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			$data['title'] = "Selected Students";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/selected_students');
			$this->load->view('templates/foot');
		}

		public function edit_selected($exam_no){
			if(!($this->session->userdata('manage_selected_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['title'] = "Edit";
			$data['exam_no'] = $exam_no;

			$data['selected_student_record'] = $this->student_model->select_selected_student_by_exam_no($exam_no);
			//$data['tribes'] = $this->student_model->select_tribes();
			//$data['religions'] = $this->student_model->select_religions();
			$data['regions'] = $this->student_model->select_regions();

			if($this->form_validation->run('edit_selected_student') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/edit_selected');
				$this->load->view('templates/foot');
			}
			else{
				$old_exam_no = $this->input->post('old_exam_no');
				$exam_no = $this->input->post('exam_no');
				$fn = $this->input->post('firstname');
				$ln = $this->input->post('lastname');
				//$ln = $this->input->post('smiddlename');

				$gender = $this->input->post('gender');
				$nationality = $this->input->post('nationality');
				$box = $this->input->post('box');
				$phone_no = $this->input->post('tel_no');
				$region = $this->input->post('region');
				$email = $this->input->post('email');
				$ward = $this->input->post('ward');
				$district = $this->input->post('district');
				$orphan = $this->input->post('orphan');
				$form = $this->input->post('form');
				$former_school = $this->input->post('former_school');
				$g_firstname = $this->input->post('g_firstname');
				$g_lastname = $this->input->post('g_lastname');
				$dob = $this->input->post('dob');
				//$doa = $this->input->post('doa');

				$get_level = explode('-', $form);

				$id = $this->student_model->select_current_academic_year_by_level(''.$get_level[1].'')['id'];

				$selected_student_record = array(
					'examination_no' => $exam_no,
					'firstname' => $fn,
					'lastname' => $ln,
					//'smiddlename' => $mn,
					'gender' => $gender,
					'form' => $get_level[0],
					'orphan' => $orphan,
					'nationality' => $nationality,
					'ward' => $ward,
					'district' => $district,
					'region' => $region,
					'tel_no' => $phone_no,
					'box' => $box,
					'email' => $email,
					'dob' => $dob,
					//'doa' => $doa,
					'former_school' => $former_school,
					'g_firstname' => $g_firstname,
					'g_lastname' => $g_lastname,
					'academic_year' => $id

				);

				$rs = $this->student_model->update_selected_student($old_exam_no, $selected_student_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				if($get_level[0] == "I"){
					redirect('students/selected/form_one');	
				}
				if($get_level[0] == "V"){
					redirect('students/selected/form_five');	
				}
			}
		}


		/*public function kinabo(){
			$year = $this->input->post('filter_year');

			$this->alumni();
		}*/

		public function alumni(){
			if(!($this->session->userdata('manage_alumni') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	        	if($this->input->post('filter_year')){
	            	$value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        	}
	        	else{
	        		$value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        	}
	        }

	        //Get Posted year from filter search ya alumni
			$year = $this->input->post('filter_year');


			$config = array();
			if($this->input->post('filter_year')){
				$config['base_url'] = base_url() . 'students/alumni_year/'.$year.'/'.$value;
				$config['total_rows'] = $this->student_model->count_all_completed_students_by_year($year, $value);
			}
			else{
				$config['base_url'] = base_url() . 'students/alumni/'.$value;
				$config['total_rows'] = $this->student_model->count_all_completed_students($value);
			}

			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			if($this->input->post('filter_year')){
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
				$data['students'] = $this->student_model->retrieve_all_completed_students_by_year($year, $config['per_page'], $page, $value);
			}
			else{
				$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
				$data['students'] = $this->student_model->retrieve_all_completed_students($config['per_page'], $page, $value);
			}
			
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			$data['title'] = "Completed Students: " . $year;
			$data['past_academic_years'] = $this->admin_model->select_all_academic_years("past_academic_year");

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			if($this->input->post('filter_year')){
				redirect(base_url() . 'students/alumni_year/'.$year);
			}
			else{
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/alumni');
				$this->load->view('templates/foot');
			}
		}

		public function alumni_year($year = NULL){
			if(!($this->session->userdata('manage_alumni') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			//Get Posted year from filter search ya alumni
			//$year = $this->input->post('filter_year');
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$transfer_status = "OUT";
			$config = array();
			$config['base_url'] = base_url() . 'students/alumni_year/'.'/'.$year.'/'.$value;
			$config['total_rows'] = $this->student_model->count_all_completed_students_by_year($year, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['students'] = $this->student_model->retrieve_all_completed_students_by_year($year, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			$data['title'] = "Completed Students: " . $year;
			$data['year'] = $year;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}
 
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/alumni_year');
			$this->load->view('templates/foot');
		}

		public function add_completion_details(){
			if(!($this->session->userdata('add_completion_details') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$admission_no = $this->input->post('admission_no');
			$index_no = $this->input->post('index_no');
			$division = $this->input->post('division');
			$points = $this->input->post('points');

			$record = array(
				'index_no' => $index_no,
				'division' => $division,
				'points' => $points,
			);

			$rs = $this->student_model->update_completed_students($record, $admission_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
			redirect('students/alumni');
		}

		public function update_kachukua_cheti($admission_no){
			if(!($this->session->userdata('add_completion_details') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$date = $this->input->post('date');
			$slip_no = $this->input->post('slip_no');
			$kashachukua_cheti = $this->input->post('kashachukua_cheti');

			if($kashachukua_cheti === "hapana"){
				$kashachukua_cheti = "ndiyo";
			}
			else{
				$kashachukua_cheti = "hapana";
			}


			$record = array(
				'tarehe_ya_kuchukua' => $date,
				'slip_no' => $slip_no,
				'kashachukua_cheti' => $kashachukua_cheti
			);

			$rs = $this->student_model->update_completed_students($record, $admission_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			if($this->input->post('year')){
				redirect('students/alumni_year/'.$this->input->post('year'));
			}
			else{
				redirect('students/alumni');
			}
		}


		public function disabled(){
			if(!($this->session->userdata('view_disabled_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$transfer_status = "OUT";
			$config = array();
			$config['base_url'] = base_url() . 'students/disabled/'.$value;
			$config['total_rows'] = $this->student_model->count_all_disabled_students($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->retrieve_all_disabled_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			$data['title'] = "Disabled Students";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/disabled_students');
			$this->load->view('templates/foot');

		}

		public function transferred_in(){
			if(!($this->session->userdata('view_transferred_in_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$transfer_status = "IN";
			$config = array();
			$config['base_url'] = base_url() . 'students/transferred_in/'.$value;
			$config['total_rows'] = $this->student_model->get_all_transferred_in_students_data($transfer_status, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->select_transferred_in_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";

			$data['title'] = "Transferred IN Students";

			//$data['students'] = $this->student_model->select_students_by_stte($transfer_status);

			$data['status'] = $transfer_status;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/transferred_in_students');
			$this->load->view('templates/foot');
		}

		public function fetch_transferred_students(){
			$stte = $this->input->get('stte_out');

			$fetch_data = $this->student_model->make_transferred_students_datatables($stte);
			$data = array();
			foreach($fetch_data as $row){
				$sub_array = array();
				$sub_array[] = $row->firstname;
				//$sub_array[] = $row->smiddlename;
				$sub_array[] = $row->lastname;
				$sub_array[] = $row->school;
				$sub_array[] = $row->status;
				$sub_array[] = $row->class_name;
				$sub_array[] = $row->date_of_transfer;
				$sub_array[] = $row->transfer_letter;
				$sub_array[] = $row->self_form_receipt;
				$sub_array[] = '<a href="'.base_url().'students/get_student_profile/'.$row->admission_no.'" class="fa fa-eye btn btn-primary btn-xs" data-toggle="tooltip" data-placement="right" title="Student Profile"></a>';
				$sub_array[] = ''.form_open('students/restore_student/'.$row->admission_no).'
					<input type="hidden" name="transfer" value="transfer" />
					<button type="submit" name="restore_student" class="btn btn-primary btn-xs" data-placement="right" data-toggle="tooltip" title="Restore student" onClick="return confirm("Are you sure you want to restore this student?");" /><span class="fa fa-undo"></span></button>'.form_close().'
				';
				$data[] = $sub_array;
			}

			$output = array(
				"draw"				=>	intval($_POST["draw"]),
				"recordsTotal"		=>	$this->student_model->get_all_transferred_students_data($stte),
				"recordsFiltered"	=>	$this->student_model->get_filtered_transferred_students_data($stte),
				"data"				=>	$data
			);
			echo json_encode($output);
		}

		public function transferred_out(){
			if(!($this->session->userdata('view_transferred_out_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$transfer_status = "OUT";
			$config = array();
			$config['base_url'] = base_url() . 'students/transferred_out/'.$value;
			$config['total_rows'] = $this->student_model->get_all_transferred_students_data($transfer_status, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->select_transferred_out_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";

			$data['title'] = "Transferred OUT Students";

			//$data['students'] = $this->student_model->select_students_by_stte($transfer_status);

			//$data['status'] = $transfer_status;
			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/transferred_out_students');
			$this->load->view('templates/foot');
		}

		public function transfer_student(){
			if(!($this->session->userdata('transfer_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$admission_no = $this->input->post('admission_no');
			$school_name = $this->input->post('school_name');
			$date = $this->input->post('date');
			$form = $this->input->post('form');
			$self_form_receipt = $this->input->post('self_form_receipt');
			$transfer_letter = $this->input->post('transfer_letter');
			$transfer_status = $this->input->post('transfer_status');

			$record = array(
				'admission_no' => $admission_no,
				'school' => $school_name,
				'date_of_transfer' => $date,
				'transfer_status' => $transfer_status,
				'transfer_letter' => $transfer_letter,
				'self_form_receipt' => $self_form_receipt,
				'form' => $form
			);

			$data = array('stte_out' => 'transferred_out', 'status' => 'inactive');

			$rs = $this->student_model->insert_into_transfer($record, $admission_no, $data);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully transferred");
			}
			redirect(base_url() . 'students/registered');

		}

		public function restore_student($admission_no){				
			if($this->input->post('expell') === "expell"){
				if(!($this->session->userdata('restore_suspended_student') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}

				$rs = $this->student_model->restore_student($admission_no);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully restored");
				}
				redirect('students/expelled');
			}
			if($this->input->post('suspend') === "suspend"){
				if(!($this->session->userdata('restore_suspended_student') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}

				$rs = $this->student_model->restore_student($admission_no);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully restored");
				}
				redirect('students/suspended');
			}
			if($this->input->post('transfer') === "transfer"){
				if(!($this->session->userdata('restore_transferred_out_students') == 'ok')){
					$this->authenticate_user_permission('home', FALSE);
				}

				$rs = $this->student_model->restore_student($admission_no);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully restored");
				}
				redirect('students/transferred_out');
			}
		}

		public function suspended(){
			if(!($this->session->userdata('view_suspended_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$stte = "suspended";
			$config = array();
			$config['base_url'] = base_url() . 'students/suspended/'.$value;
			$config['total_rows'] = $this->student_model->count_suspended_students($stte, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->select_suspended_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			
			$data['title'] = "Suspended Students";

			//Use in restore
			$data['flag'] = "SUSPEND";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/expelled_student_view');
			$this->load->view('templates/foot');
		}

		public function expelled(){
			if(!($this->session->userdata('view_suspended_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$stte = "expelled";
			$config = array();
			$config['base_url'] = base_url() . 'students/expelled/'.$value;
			$config['total_rows'] = $this->student_model->count_expelled_students($stte, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->select_expelled_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			
			$data['title'] = "Expelled Students";
			
			//Use in restore
			$data['flag'] = "EXPELL";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/expelled_student_view');
			$this->load->view('templates/foot');
		}

		public function unreported(){
			if(!($this->session->userdata('view_unreported_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$transfer_status = "OUT";
			$config = array();
			$config['base_url'] = base_url() . 'students/unreported/'.$value;
			$config['total_rows'] = $this->student_model->count_all_unreported_students($value);
			$config['per_page'] = 1;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['students'] = $this->student_model->retrieve_all_unreported_students($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO"; 

			$data['title'] = "Unreported Students";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/unreported_students');
			$this->load->view('templates/foot');

		}

		public function assign_monitor($class_stream_id){
			$allowed = FALSE;
			if($this->session->userdata('add_class_monitor') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['title'] = "Assign Monitor";
			$data['students'] = $this->student_model->select_students_by_class($class_stream_id);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;
			$data['class_monitor'] = $this->student_model->select_class_monitor($class_stream_id)['admission_no'];

			if($this->form_validation->run('assign_leader') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('classes/add_class_monitor');
				$this->load->view('templates/foot');
			}
			else{
				$admission_no = $this->input->post('admission_no');
				$record = array(
					'admission_no' => $admission_no
				);
				$rs = $this->student_model->update_class_monitor($class_stream_id, $record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Monitor successfully added");
				}
				redirect('students/assign_monitor/'.$class_stream_id);
			}
		}

		public function load_new_discipline(){
			if(!($this->session->userdata('manage_indiscipline_records') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['students'] = $this->student_model->select_all_students();

			$data['discipline_title'] = "Add Discipline Record";

			if($this->form_validation->run('add_discipline') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/new_discipline');
				$this->load->view('templates/foot');
			}
			else{
				$this->save_new_discipline();
			}				
		}

		public function update_indiscipline_record($id){
			if(!($this->session->userdata('manage_indiscipline_records') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['indiscipline_record'] = $this->student_model->get_indiscipline_record($id);
			$data['students'] = $this->student_model->select_all_students();
			$data['discipline_title'] = "Update Discipline Record";
			$data['id'] = $id;

			if($this->form_validation->run('add_discipline') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/edit_discipline');
				$this->load->view('templates/foot');
			}
			else{
				$date = $this->input->post('date');
				$admission_no = $this->input->post('admission_no');
				$description = $this->input->post('description');
				$decision = $this->input->post('decision');
				$id = $this->input->post('id');

				$discipline_record = array(
					'date' => $date,
					'admission_no' => $admission_no,
					'description' => $description,
					'decision' => $decision
				);

				$rs = $this->student_model->update_indiscipline_record($id, $discipline_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Record successfully updated");
				}
				redirect('students/update_indiscipline_record/'.$id);
			}
		}

		public function save_new_discipline(){
			if(!($this->session->userdata('manage_indiscipline_records') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$date = $this->input->post('date');
			$admission_no = $this->input->post('admission_no');
			$description = $this->input->post('description');
			$decision = $this->input->post('decision');

			$discipline_record = array(
				'date' => $date,
				'admission_no' => $admission_no,
				'description' => $description,
				'decision' => $decision
			);

			$rs = $this->student_model->insert_discipline_record($discipline_record);
			if($rs){
				$this->transactionSuccessfullyCommitted("Indiscipline record successfully added");
			}
			redirect('discipline/add_record');
		}

		public function scoresheet($class_stream_id = NULL, $subject_id = NULL){
			if(!($this->session->userdata('add_score_of_own_subject') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'home');

			if ($this->input->post('search_student') != "") {
				$value = $this->input->post('search_student');
			}
			else{
				$value = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			}


			$config = array();
			$config['base_url'] = base_url() . 'students/scoresheet/'.$class_stream_id.'/'.$subject_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_by_class_and_subject($class_stream_id, $subject_id, $value);
			$config['per_page'] = 100;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['students'] = $this->student_model->select_students_by_class_and_subject($class_stream_id, $subject_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$start_date = $this->student_model->get_enabled_exam_type()['start_date'];
			$end_date = $this->student_model->get_enabled_exam_type()['end_date'];

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $subject_id;

			$data['score_title'] = "Scoresheet";
			$data['display_back'] = "";

			$data['exam_date_and_type'] = $this->student_model->get_enabled_exam_type()['exam_name'] . ", <strong>From:</strong> " . $start_date . " <strong>To:</strong> " . $end_date;
			$data['examtype_id'] = $this->student_model->get_enabled_exam_type()['id'];

			$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->select_class_level_by_class_stream_id($class_stream_id)['level'])['term_id'];
			//$data['term_id'] = $level;

			$data['check_enabled'] = $this->student_model->get_enabled_exam_type();
			$data['p'] = $page;

			$exam_date = $this->input->post('edate');
			$allowed_to_add_marks = $this->student_model->validate_date_of_adding_score($exam_date);
			

				if($this->form_validation->run('add_score') == FALSE){
					$this->load->view('templates/teacher_header', $data);
					$this->load->view('students/scoresheet');
					$this->load->view('templates/foot');
				}
				else{
					if($allowed_to_add_marks === 1){
						$this->save_marks($exam_date);
					}
					else{
						$this->session->set_flashdata('error_message', 'Error, the date you have entered is in an invalid range');
						redirect(base_url() . 'students/scoresheet/'.$class_stream_id.'/'.$subject_id);
					}
				}	
		}



		public function save_marks($exam_date){
			if(!($this->session->userdata('add_score_of_own_subject') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['admission_no'] = $this->input->post('admission_no');
			$subject_id = $this->input->post('subject_id');
			$class_id = $this->input->post('class_id');
			$etype_id = $this->input->post('etype');
			$class_stream_id = $this->input->post('class_stream_id');
			//$exam_date = $this->input->post('edate');
			$data['score'] = $this->input->post('score');
			$term_id = $this->input->post('term_id');

			//Imewekwa hapa coz ya $subject_id & $class_stream_id
			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'home');
			
			/*foreach ($data['score'] as $key => $value) {
				echo $value . " , " . is_numeric($value) . "<br/>";
			}exit;*/

			foreach ($data['admission_no'] as $key => $value) {
				$student_subject_assessment_record[] = array(
					'admission_no' => $value,
					'subject_id' => $subject_id,
					'etype_id' => $etype_id,
					//'class_id' => $class_id,
					'class_stream_id' => $class_stream_id,
					'e_date' => $exam_date,
					'term_id' => $term_id,
					'marks' => (is_numeric($data['score'][$key])) ? $data['score'][$key] : NULL//$data['score'][$key]
				);
			}
			$rs = $this->student_model->insert_score($student_subject_assessment_record, $class_id);

			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully added");
			}
			redirect('students/scoresheet/'.$class_stream_id .'/'.$subject_id);
		}

		public function save_new_student(){
			if(!($this->session->userdata('register_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('register_student')){
				$adm_no = $this->input->post('admission_no');
				$fn = $this->input->post('firstname');
				$ln = $this->input->post('lastname');
			       // $mn = $this->input->post('smiddlename');
				$old_dob = $this->input->post('dob');	//OLD dob

				$day = $this->input->post('day');
				$month = $this->input->post('month');
				$year = $this->input->post('year');
				$dob = $year . "-" . $month . "-" . $day; //NEW dob
				//$doa = $year . "-" . $month . "-" . $day; //NEW dob


				$tribe_id = $this->input->post('tribe_id');
				$religion_id = $this->input->post('religion_id');
				$box = $this->input->post('box');
				$region = $this->input->post('region');
				$adm_date = $this->input->post('admission_date');
				$class_id = $this->input->post('class_id');
				$former_school = $this->input->post('former_school');
				$ac_year_id = $this->input->post('academic_year_id');
				$dorm_id = $this->input->post('dorm_id');
				$class_id = $this->input->post('class_id');
				$stream_name = $this->input->post('stream_name');
				$class_stream_id = $this->input->post('class_stream_id');	//Got from session		
				$transferred = $this->input->post('transferred');


				//$class_stream_id = $class_id . $stream_name;

				$student_record = array(
					'admission_no' => $adm_no,
					'firstname' => $fn,
			                //'smiddlename' => $mn,
					'lastname' => $ln,
					'dob' => $dob,
					//'doa' => $doa,
					'tribe_id' => $tribe_id,
					'religion_id' => $religion_id,
					'home_address' => $box,
					'region' => $region,
					'class_id' => $class_id,
					'class_stream_id' => $class_stream_id,
					'dorm_id' => $dorm_id,
					'former_school' => $former_school,
					'academic_year' => $ac_year_id,
					'date' => $adm_date,
					'stte' => $transferred
				);
				

				$gfn = $this->input->post('g_firstname');
				$gmn = $this->input->post('g_middlename');
				$gln = $this->input->post('g_lastname');
				$gender = $this->input->post('gender');
				$occupation = $this->input->post('occupation');
				$rel_type = $this->input->post('rel_type');
				$phone_no = $this->input->post('phone_no');
				$email = $this->input->post('email');
				$p_address = $this->input->post('p_address');
				$p_region = $this->input->post('p_region');

				$guardian_record = array(
					'admission_no' => $adm_no,
					'firstname' => $gfn,
					'middlename' => $gmn,
					'lastname' => $gln,
					'gender' => $gender,
					'rel_type' => $rel_type,
					'email' => $email,
					'occupation' => $occupation,
					'phone_no' => $phone_no,
					'p_box'	=> $p_address,
					'p_region' => $p_region
				);

				$rs = $this->student_model->insert_student_record($student_record, $guardian_record, $adm_no);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully registered");
				}
				redirect('students/register_student');
			}
			else{
				redirect('students/register_student');
			}
		}

		public function new_student($exam_no = NULL){
			if(!($this->session->userdata('register_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = '<a href="">Home</a> | <a href="">Students</a> | <a href="">Add New</a>';

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}
			
			$tribe_id='';
			$religion_id='';
			$data['tribes'] = $this->student_model->select_tribes();
			$data['religions'] = $this->student_model->select_religions();
			$data['classes'] = $this->student_model->select_class_streams();
			$data['dormitories'] = $this->student_model->select_dormitories();
			$data['regions'] = $this->student_model->select_regions();
			$data['months'] = $this->student_model->select_months();

			//Special for retrieving values if registered from selected student
			$data['ssd'] = $this->student_model->get_selected_student_by_exam_no($exam_no);

			if($data['ssd']):
				$data['dob'] = $this->student_model->get_selected_student_by_exam_no($exam_no)['dob'];
				$data['siku'] = explode('-', $data['dob'])[2];
				$data['mwezi'] = explode('-', $data['dob'])[1];
				$data['mwaka'] = explode('-', $data['dob'])[0];
			endif;

			// if($this->form_validation->run() == FALSE){
			// 	if($this->input->post('admission_no')){
			// 	$this->session->set_flashdata('error_validation', "Failed to add student, correct the errors then submit the form");
			// 	}
				
			// 	$this->load->view('templates/teacher_header', $data);
			// 	//$this->load->view('students/register_new_student');
			// 	$this->load->view('students/form_phase');
			// 	$this->load->view('templates/foot');
			// }
			// else{
			if (isset($_POST['admission_no'])) {
				$adm_no = $this->input->post('admission_no');
				$fn = $this->input->post('firstname');
				$ln = $this->input->post('lastname');
				//$mn = $this->input->post('smiddlename');
				$old_dob = $this->input->post('dob');	//USED DOB

				$day = $this->input->post('day');
				$month = $this->input->post('month');
				$year = $this->input->post('year');
				$dob = $year . "-" . $month . "-" . $day; //NE
				//$doa = $year . "-" . $month . "-" . $day; 

				//select tribe id from tribe
				$tribe_name=$this->input->post('tribe_id');
				if($tribe_id_obj = $this->student_model->select_tribeByName($tribe_name)){
					$tribe_id = $tribe_id_obj[0]['tribe_id'];
				}
				//end of select tribe id

				//select religion id from religion
				$religion_name=$this->input->post('religion_id');
				if($religion_id_obj = $this->student_model->select_religionByName($religion_name)){
				$religion_id= $religion_id_obj[0]['religion_id'];
				}
				//end of select religion id

				//$religion_id = $this->input->post('religion_id');
				$box = $this->input->post('box');
				$region = $this->input->post('region');
				$adm_date = $this->input->post('admission_date');
				$former_school = $this->input->post('former_school');
				$dorm_id = $this->input->post('dorm_id');
				$stream_name = $this->input->post('stream_name');
				$class_stream_id_and_class_id = $this->input->post('class_stream_id');	//Got from session		
				$transferred = $this->input->post('transferred');
				$disabled = $this->input->post('disabled');


				if($disabled === "disabled"){
					$disabled = "yes";
					$disability = $this->input->post('disability');
				}
				else{
					$disability = "";
				}

				$class_id = explode('-', $class_stream_id_and_class_id)[1];
				$class_stream_id = explode('-', $class_stream_id_and_class_id)[0];
				$ac_year_id = $this->student_model->select_academic_year_by_class_id($class_id)['id'];

				$student_record = array(
					'admission_no' => $adm_no,
					'firstname' => $fn,
					'lastname' => $ln,
					//'smiddlename' => $mn,
					'dob' => $old_dob,
					'disabled' => $disabled,
					'disability' => $disability,
					'tribe_id' => $tribe_id,
					'religion_id' => $religion_id,
					'home_address' => $box,
					'region' => $region,
					'class_id' => $class_id,
					'class_stream_id' => $class_stream_id,
					'dorm_id' => $dorm_id,
					'former_school' => $former_school,
					'academic_year' => $ac_year_id,
					'date' => $adm_date,
					'stte' => $transferred
				);
				

				$gfn = $this->input->post('g_firstname');
				$gmn = $this->input->post('g_middlename');
				$gln = $this->input->post('g_lastname');
				$gender = $this->input->post('gender');
				$occupation = $this->input->post('occupation');
				$rel_type = $this->input->post('rel_type');
				$phone_no = $this->input->post('phone_no');
				$email = $this->input->post('email');
				$p_address = $this->input->post('p_address');
				$p_region = $this->input->post('p_region');

				$guardian_record = array(
					'admission_no' => $adm_no,
					'firstname' => $gfn,
					'middlename' => $gmn,
					'lastname' => $gln,
					'gender' => $gender,
					'rel_type' => $rel_type,
					'email' => $email,
					'occupation' => $occupation,
					'phone_no' => $phone_no,
					'p_box'	=> $p_address,
					'p_region' => $p_region
				);

				$rs = $this->student_model->insert_student_record($student_record, $guardian_record, $adm_no);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully registered");
				}
				redirect('students/new_student');
			}
			else{
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/form_phase');
				$this->load->view('templates/foot');
			}
			//}
		}


		public function assign_optional_subject($admission_no){
			if(!($this->session->userdata('assign_option_subject') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$subject_id = $this->input->post('subject_id');
			$class_id = $this->input->post('class_id');

			$class_stream_id = $this->subject_model->select_class_stream_by_id($subject_id, $class_id)['class_stream_id'];

			if($class_stream_id == ""){
				$this->session->set_flashdata('error_message', 'Error, make sure the class stream has the optional subject you are attempting to assign to this student');
				redirect('students/registered');
			}
			else{
				$std_record = array(
					'class_stream_id' => $class_stream_id
				);

				$rs = $this->student_model->update_class_stream($admission_no, $subject_id, $class_stream_id, $std_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully assigned");
				}
				redirect('students/registered');
			}
		}

		public function get_student_profile($admission_no){
			$data['title'] = "Student Profile: " . $this->student_model->select_student_record($admission_no)['student_names'];

			$data['student_profile'] = $this->student_model->select_student_record($admission_no);
			$data['current_dormitory'] = $this->dormitory_model->select_dormitory_by_student($admission_no);
			$data['current_class'] = $this->class_model->select_class_by_student($admission_no);

			$data['adm_no'] = $admission_no;

			$data['image']=$this->student_model->profile_picture($admission_no);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/student_profile_header');
			$this->load->view('students/student_profile');
			$this->load->view('templates/foot');
		}

		public function discipline($admission_no){
			$data['title'] = "Discipline record(s)";
			$data['discipline_records'] = $this->student_model->select_student_discipline_rec($admission_no);

			$data['image']=$this->student_model->profile_picture($admission_no);
			$data['adm_no'] = $admission_no;

			$data['student_names'] = $this->student_model->select_student_record($admission_no);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/student_profile_header');
			$this->load->view('students/view_discipline_individual');
			$this->load->view('templates/foot');
		}

		public function attendance($admission_no){
			$data['title'] = "Attendance record: " . $this->student_model->select_student_record($admission_no)['student_names'];
			$data['attendance_records'] = $this->student_model->select_student_attendance_record($admission_no);

			$data['image']=$this->student_model->profile_picture($admission_no);
			$data['adm_no'] = $admission_no;

			$data['student_names'] = $this->student_model->select_student_record($admission_no);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/student_profile_header');
			$this->load->view('students/view_student_attendance');
			$this->load->view('templates/foot');
		}

		public function get_students_by_class_stream($class_stream_id = NULL){
			/*$allowed = FALSE;
			if($this->session->userdata('view_studets_in_class') == 'ok' || ($this->session->userdata('view_students_in_own_class') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_role($this->session->userdata('class_stream')['class_stream_id'], $class_stream_id, 'home', $allowed);
*/
			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$data['class_stream_id'] = $class_stream_id;

			$config = array();
			$config['base_url'] = base_url() . 'myclass/students/'.$class_stream_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_per_stream($class_stream_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			//$page = $this->uri->segment(4);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['mystudents'] = $this->student_model->fetch_students_by_stream($class_stream_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "All Students: " . $this->class_model->select_class_name_and_stream($class_stream_id)['class_name'] . " " . $this->class_model->select_class_name_and_stream($class_stream_id)['stream'];
			//$data['mystudents'] = $this->student_model->select_students_by_class($class_stream_id);

			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			//For class monitor na total no of students
			$data['class_monitor'] = $this->class_model->get_class_monitor($class_stream_id);
			$data['nos'] = $this->class_model->csps_for_summary_table($class_stream_id);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_students_by_stream_class_teacher_new');
			$this->load->view('templates/foot');
		}

		public function discipline_records(){
			if(!($this->session->userdata('view_indiscipline_records') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			$data['title'] = "Indiscipline Records";

			//$data['title'] = "NEVER LET UR SCHOOLING GET IN THE WAY OF EDUCATION";
			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'students/discipline_records/'.$value;
			$config['total_rows'] = $this->student_model->count_discipline_records($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['discipline_records'] = $this->student_model->select_discipline_records($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			//$data['title'] = "Discipline record(s)";
			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/teachers')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_discipline');
			$this->load->view('templates/foot');
		}


		//Edit results
		public function edit_results($class_stream_id = NULL, $subject_id = NULL){
			if(!($this->session->userdata('update_score_of_own_subject') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'home');

			$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->select_class_level_by_class_stream_id($class_stream_id)['level'])['term_id'];

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			}


			$config = array();
			$config['base_url'] = base_url() . 'students/edit_results/'.$class_stream_id.'/'.$subject_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_by_class_and_subject($class_stream_id, $subject_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['mystudents'] = $this->student_model->select_results_by_class_and_stream($class_stream_id, $subject_id, $data['term_id'], $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$start_date = $this->student_model->get_enabled_exam_type()['start_date'];
			$end_date = $this->student_model->get_enabled_exam_type()['end_date'];

			if($data['mystudents'] != FALSE):
				$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			else:
				$data['x_of_y_entries'] = "";
			endif;

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $subject_id;

			$data['display_back'] = "";

			$data['exam_date_and_type'] = $this->student_model->get_enabled_exam_type()['exam_name'] . ", <strong>From:</strong> " . $start_date . " <strong>To:</strong> " . $end_date;
			$data['examtype_id'] = $this->student_model->get_enabled_exam_type()['id'];

			$data['check_enabled'] = $this->student_model->get_enabled_exam_type();

			$data['title'] = "Results: " . $class_stream_id;


			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			if($this->form_validation->run('update_results') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/edit_results');
				$this->load->view('templates/foot');
			}
			else{
				$ssa_id = $this->input->post('ssa_id');
				$this->update_results($ssa_id);
			}
		}

		public function update_results($ssa_id){
			if(!($this->session->userdata('update_score_of_own_subject') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$student_marks = array(
				'marks' => $this->input->post('score')
			);

			$class_stream_id = $this->input->post('class_stream_id');
			$term_id = $this->input->post('term_id');
			$subject_id = $this->input->post('subject_id');
			$class_id = $this->input->post('clss_id');

			//Iko hapa coz ya kupata parameters 
			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'home');

			$rs = $this->student_model->update_student_results($ssa_id, $student_marks, $subject_id, $term_id, $class_stream_id, $class_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('students/edit_results/'.$class_stream_id.'/'.$subject_id);

			//redirect($cid.'/'.$sub_id.'/results');
		}

		//START RESULTS LOOP
		public function print_students_results($class_stream_id, $term_id){
			$allowed = FALSE;
			if($this->session->userdata('view_school_results') == 'ok' || ($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$students_array['students_record'] = $this->student_model->select_students_waliofanya_mtihani($class_stream_id);

			$this->load->view('templates/teacher_header');
			$this->load->view('students/printAllHeader');

			foreach ($students_array['students_record'] as $key => $value) {
				$this->print_all($value['admission_no'], $term_id);
			}

			$this->load->view('students/printAllFooter');
			$this->load->view('templates/foot');
		}

		//The function is called hapo kwa juu mazee
		public function print_all($admission_no, $term_id){
			//****************************************************************************************
			$data = array();
			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}
			/*$data['grade_systems'] = $this->student_model->select_o_level_grade_system();
			$data['divisions'] = $this->student_model->select_divisions($class_level);*/

			$upimaji = array(
				'0'=>"Utekelezaji wa wajibu",
				'1'=>'Kuwahi darasani na kazini na kuwepo muda wote',
				'2'=>'Kufanya kazi kwa juhudi na maarifa',
				'3'=>'Utunzaji wa mali ya umma',
				'4'=>'Ushirikiano na wenzake',
				'5'=>'Heshima kwa walimu, wanafunzi wenzake',
				'6'=>'Sifa ya kuwa kiongozi na wenzake kutafuta ushauri',
				'7'=>'Utii na kufuata maagizo kwa hiari',
				'8'=>'Usafi wa binafsi',
				'9'=>'Uaminifu',
				'10'=>'',
				'11'=>'',
				'12'=>'',
				'13'=>''
			);

			$upimaji_index = array(
				'0'=>"901",
				'1'=>'902',
				'2'=>'903',
				'3'=>'904',
				'4'=>'905',
				'5'=>'906',
				'6'=>'907',
				'7'=>'908',
				'8'=>'909',
				'9'=>'910',
				'10'=>'',
				'11'=>'',
				'12'=>'',
				'13'=>''
			);

			$first_array = array();
			$second_array = array();

			foreach ($data['grade_system'] as $key => $value) {

				$first_min_array = array();
				$first_min_array[] = $upimaji_index[$key];
				$first_min_array[] = $upimaji[$key];
				$first_min_array[] = $value['grade'];
				$first_min_array[] = $value['start_mark'];
				$first_min_array[] = $value['end_mark'];
				$first_min_array[] = $value['remarks'];

				$first_array[] = $first_min_array;

				if($upimaji[$key] == 'Heshima kwa walimu, wanafunzi wenzake'){
					$update_key = $key + 1;

					foreach ($data['divisions'] as $key => $division) {

						$second_min_array = array();
						$second_min_array[] = $upimaji_index[$update_key];
						$second_min_array[] = $upimaji[$update_key];
						$second_min_array[] = $division['starting_points'];
						$second_min_array[] = $division['ending_points'];
						$second_min_array[] = $division['div_name'];

						$second_array[] = $second_min_array;

						$update_key++;
					}
					break;
				}
			}

			$data['title'] = "Please Work";

			$data['aaaa'] = $first_array;
			$data['bbbb'] = $second_array;
			//****************************************************************************************

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);

			//$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no);


			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}
			
			$this->load->view('students/print_all_results', $data);
		}

		//END RESULTS LOOP

		public function print_results($admission_no, $term_id){
			if(!($this->session->userdata('view_own_class_results') == 'ok' || $this->session->userdata('view_school_results') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			/*$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position($admission_no);
			$data['no_of_students'] = $this->student_model->count_students_per_class($admission_no, $term_id);*/

			//****************************************************************************************
			$data = array();
			
			/*$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level'])['term_id'];
			$term_id = $data['term_id'];*/


			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}

			//$data['grade_systems'] = $this->student_model->select_o_level_grade_system();
			//$data['divisions'] = $this->student_model->select_divisions($class_level);

			$upimaji = array(
				'0'=>"Utekelezaji wa wajibu",
				'1'=>'Kuwahi darasani na kazini na kuwepo muda wote',
				'2'=>'Kufanya kazi kwa juhudi na maarifa',
				'3'=>'Utunzaji wa mali ya umma',
				'4'=>'Ushirikiano na wenzake',
				'5'=>'Heshima kwa walimu, wanafunzi wenzake',
				'6'=>'Sifa ya kuwa kiongozi na wenzake kutafuta ushauri',
				'7'=>'Utii na kufuata maagizo kwa hiari',
				'8'=>'Usafi wa binafsi',
				'9'=>'Uaminifu',
				'10'=>'',
				'11'=>'',
				'12'=>'',
				'13'=>''
			);

			$upimaji_index = array(
				'0'=>"901",
				'1'=>'902',
				'2'=>'903',
				'3'=>'904',
				'4'=>'905',
				'5'=>'906',
				'6'=>'907',
				'7'=>'908',
				'8'=>'909',
				'9'=>'910',
				'10'=>'',
				'11'=>'',
				'12'=>'',
				'13'=>''
			);

			$first_array = array();
			$second_array = array();

			foreach ($data['grade_system'] as $key => $value) {

				$first_min_array = array();
				$first_min_array[] = $upimaji_index[$key];
				$first_min_array[] = $upimaji[$key];
				$first_min_array[] = $value['grade'];
				$first_min_array[] = $value['start_mark'];
				$first_min_array[] = $value['end_mark'];
				$first_min_array[] = $value['remarks'];

				$first_array[] = $first_min_array;

				if($upimaji[$key] == 'Heshima kwa walimu, wanafunzi wenzake'){
					$update_key = $key + 1;

					foreach ($data['divisions'] as $key => $division) {

						$second_min_array = array();
						$second_min_array[] = $upimaji_index[$update_key];
						$second_min_array[] = $upimaji[$update_key];
						$second_min_array[] = $division['starting_points'];
						$second_min_array[] = $division['ending_points'];
						$second_min_array[] = $division['div_name'];

						$second_array[] = $second_min_array;

						$update_key++;
					}
					break;
				}
			}

			$data['title'] = "Please Work";

			$data['aaaa'] = $first_array;
			$data['bbbb'] = $second_array;
			//****************************************************************************************

			

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);
			//$data['academic_year'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level']);
			//$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);



			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/individual_results');
			$this->load->view('templates/foot');
		}

		//MKEKA WA DARASA
		public function subject_results($teacher_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('manage_score_of_assigned_subjects') == 'ok' && $this->session->userdata('staff_id') === $teacher_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}

			$config = array();
			$config['base_url'] = base_url() . 'students/subject_results/'.$teacher_id.'/'.$value;
			$config['total_rows'] = $this->teacher_model->count_teachers_teaching_assingment($teacher_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['myclasses'] = $this->teacher_model->select_teachers_teaching_assingment($teacher_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();
			$data['title'] = "My Classes Results";

			$data['display_back'] = "";

			$data['teacher_id'] = $teacher_id;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_results_of_assigned_subjects');
			$this->load->view('templates/foot');
		}

		//The function that is supposed to be called mkeka wa darasa
		public function student_results($class_stream_id = NULL, $subject_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('manage_score_of_assigned_subjects') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id){
				$allowed = TRUE;
			}

			$this->authenticate_assigned_subjects($class_stream_id, $subject_id, 'students/subject_results/'.$this->session->userdata('staff_id'));
			
			$class_level = $this->student_model->select_class_level_by_class_stream_id($class_stream_id)['level'];

			$data['term_id'] = $this->student_model->get_current_term_id_by_level($class_level)['term_id'];

			if ($this->input->post('search_student') != "") {
				$value = trim($this->input->post('search_student'));
				$this->no_record_found_message();
			}
			else{
				$value = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			}


			$config = array();
			$config['base_url'] = base_url() . 'students/student_results/'.$class_stream_id.'/'.$subject_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_by_class_and_subject($class_stream_id, $subject_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['mkeka'] = $this->student_model->new_generate_mkeka($class_stream_id, $subject_id, $data['term_id'], $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();




			$data['title'] = "Students Results";		
			$data['display_back'] = "";

			$data['grade_groups'] = $this->student_model->get_grade_aggregate_single_subject($class_stream_id, $subject_id, $data['term_id']);

			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
			}

			if($data['mkeka'] != FALSE):
				$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			else:
				$data['x_of_y_entries'] = "";
			endif;

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['class_stream_id'] = $class_stream_id;
			$data['subject_id'] = $subject_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_results_by_class_stream_and_subject');	
			$this->load->view('templates/foot');
		}
		//End mkeka

		function get_grade_aggregate_single_subject($class_stream_id, $subject_id, $term_id){
			$d = $this->student_model->get_grade_aggregate_single_subject($class_stream_id, $subject_id, $term_id);
			echo "<pre>";
				print_r($d);
			echo "</pre>";
		}

		public function update_disability(){
			$disability = $this->input->post('disability');
			$admission_no = $this->input->post('admission_no');

			$disability_record = array(
				'disability' => $disability
			);

			$rs = $this->student_model->update_disability($disability_record, $admission_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('students/disabled');
		}

		public function edit($admission_no){
			if(!($this->session->userdata('update_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['title'] = "Edit " . $admission_no;

			$data['student_record'] = $this->student_model->select_student_by_id($admission_no);
			$data['tribes'] = $this->student_model->select_tribes();
			$data['religions'] = $this->student_model->select_religions();
			$data['regions'] = $this->student_model->select_regions();
			$data['months'] = $this->student_model->select_months();

			if($this->form_validation->run('edit_student') == FALSE){
				//echo "12345";exit;
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('students/edit_student');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_info();
			}
		}

		public function update_info(){
			if(!($this->session->userdata('update_student') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$transferred = $this->input->post('transferred');
			$admission_no = $this->input->post('admission_no');
			$current_admission_no = $this->input->post('current_admission_no');
			$g_lastname = $this->input->post('g_lastname');
			$g_firstname = $this->input->post('g_firstname');
			$g_middlename = $this->input->post('g_middlename');
			$firstname = $this->input->post('firstname');
			$lastname = $this->input->post('lastname');
			$gender = $this->input->post('gender');
			$box = $this->input->post('box');
			$email = $this->input->post('email');
			$p_address = $this->input->post('p_address');
			$phone_no = $this->input->post('phone_no');
			$former_school = $this->input->post('former_school');
			$occupation = $this->input->post('occupation');
			$rel_type = $this->input->post('rel_type');
			$tribe_id = $this->input->post('tribe_id');
			$religion_id = $this->input->post('religion_id');
			$p_region = $this->input->post('p_region');
			$region = $this->input->post('region');
			$dob = $this->input->post('dob');
			/*$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year')*/;

			//$dob = $year . "-" . $month . "-" . $day;	//Concatenate for inserting dob
			$disabled = $this->input->post('disabled');

			if($disabled === "disabled"){
				$disabled = "yes";
				$disability = $this->input->post('disability');
			}
			else{
				$disability = "";
			}

			$student_record = array(
				'stte' => $transferred,
				'admission_no' => $admission_no,			
				'firstname' => $firstname,
				//'smiddlename' => $smiddlename,
				'lastname' => $lastname,
				'home_address' => $box,			
				'former_school' => $former_school,				
				'tribe_id' => $tribe_id,
				'religion_id' => $religion_id,			
				'region' => $region,
				'dob' => $dob,
				'disabled' => $disabled,
				'disability' => $disability
			);

			$guardian_record = array(
				'lastname' => $g_lastname,
				'firstname' => $g_firstname,
				'middlename' => $g_middlename,
				'email' => $email,
				'p_box' => $p_address,
				'phone_no' => $phone_no,
				'occupation' => $occupation,
				'rel_type' => $rel_type,
				'p_region' => $p_region,
				'gender' =>$gender
			);			

			$rs = $this->student_model->update_student_info($student_record, $guardian_record, $current_admission_no);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully updated");
			}
			redirect('students/registered');
		}

		public function upload_selected_students(){
			if(!file_exists('uploads')){
				mkdir('uploads');
			}

			@$target_dir = "uploads/".$_FILES['file_to_upload']['name'];	
			@$tmp = $_FILES['file_to_upload']['tmp_name'];

			move_uploaded_file($tmp, $target_dir);

			$this->student_model->upload_students($target_dir);
		}

		public function score_position(){
			$data['admission_no'] = $this->input->post('admission_no');
			$data['monthly_one'] = $this->input->post('monthly_one');
			$data['midterm'] = $this->input->post('midterm');
			$data['monthly_two'] = $this->input->post('monthly_two');
			$data['average'] = $this->input->post('average');
			$data['terminal'] = $this->input->post('terminal');
			$data['grade'] = $this->input->post('grade');
			$data['class_id'] = $this->input->post('class_id');
			$data['rank'] = $this->input->post('rank');
			$subject_id = $this->input->post('subject_id');
			$class_stream_id = $this->input->post('class_stream_id');
			$term_id = $this->input->post('term_id');


			if(!empty($data['admission_no'])){
				foreach ($data['admission_no'] as $key => $value) {
					$record[] = array(
						'admission_no' => $value,
						'monthly_one' => $data['monthly_one'][$key],
						'midterm' => $data['midterm'][$key],
						'monthly_two' => $data['monthly_two'][$key],
						'average' => $data['average'][$key],
						'terminal' => $data['terminal'][$key],
						'subject_id' => $subject_id,
						'subject_rank' => $data['rank'][$key],
						'class_id' => $data['class_id'][$key],
						'class_stream_id' => $class_stream_id,
						'grade' => $data['grade'][$key],
						'term_id' => $term_id
					);
				}
				$this->student_model->insert_score_position($record, $class_stream_id, $subject_id, $term_id);
				redirect('students/student_results/'.$class_stream_id.'/'.$subject_id);
			}
			else{
				//redirect('students/get_student_score_position/');
			}
		}

		public function students_in_department($dept_id = NULL, $class_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_students_in_own_department') == 'ok' && $this->session->userdata('dept')['dept_id'] === $dept_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_student') != ""){
	            $value = trim($this->input->post('search_student'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'students/students_in_department/'.$dept_id.'/'.$class_id.'/'.$value;
			$config['total_rows'] = $this->student_model->count_students_by_class_and_dept_id($dept_id, $class_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);

			$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : '0';
			$data['mystudents'] = $this->student_model->fetch_students_by_class_and_dept_id($dept_id, $class_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['title'] = "Students" . $class_id;
			//$data['mystudents'] = $this->student_model->select_all_students($class_id);

			$data['class_id'] = $class_id;
			$data['dept_id'] = $dept_id;

			$status = "optional";
			$data['op_subjects'] = $this->subject_model->select_optional_subjects($status);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/view_students_by_department');
			$this->load->view('templates/foot');
		}

		   //End  Retrieve students @Den


	    //START UPLOAD AVATAR
	    public function student_image($admission_no){
	    	$data['title'] = "New Profile picture";
			$data['discipline_records'] = $this->student_model->select_student_discipline_rec($admission_no);
            $data['image']=$this->student_model->profile_picture($admission_no);
			$data['adm_no'] = $admission_no;

			$data['student_names'] = $this->student_model->select_student_record($admission_no);

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}
           
	    	$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/student_profile_header');
			$this->load->view('students/add_image_@den');
			$this->load->view('templates/foot');

	    }


	   	public function upload_image($admission_no){
	   		if(!($this->session->userdata('upload_student_photo') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$config['upload_path'] = './assets/images';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['max_size']	= '700';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$firstname = $this->student_model->select_student_by_id($admission_no)['student_firstname'];
			$lastname = $this->student_model->select_student_by_id($admission_no)['student_lastname'];
			//$smiddlename = $this->student_model->select_student_by_id($admission_no)['smiddlename'];
			//Special for the consistent file name
			$config['file_name'] = strtolower($firstname . "_" . $lastname);
			//$this->load->library('image_lib', $config);
		     // resize image
		    //$this->image_lib->resize();

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('image_file')){
				if(! file_exists(APPPATH.'views/students')){
					show_404();
				}
	            $error = array(
	            	'error_msg' => $this->upload->display_errors()
	            ); 

	            $this->session->set_flashdata('error_upload', $error['error_msg']);
		    	redirect('students/get_student_profile/'.$admission_no);	
			}
			else{
				$data = $this->input->post();
				$info = $this->upload->data();
				$image_path = base_url("assets/images/".$info['raw_name'].$info['file_ext']);
				
				$image_record = array('image' => $image_path);//image link

				unset($data['submit']);
				$rs = $this->student_model->in_student_image($image_record, $admission_no);//update student  
				
                if($rs){
					$this->transactionSuccessfullyCommitted("Successfully uploaded");
				}
		        redirect('students/get_student_profile/'.$admission_no);
           
			}
		}

		//END AVATAR UPLOAD	    

		public function print_results_by_grade($admission_no, $term_id){			
			//****************************************************************************************
			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}
			/*$data['grade_system'] = $this->student_model->select_o_level_grade_system();
			$data['divisions'] = $this->student_model->select_divisions($class_level);*/

			/*$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level'])['term_id'];
			$term_id = $data['term_id'];*/

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);
			//print_r($data['mkeka_single']); exit;

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);

			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_school_results') == 'ok' || ($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $data['record']['class_stream_id'])){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			//$data['academic_year'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level']);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);
			//$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);

			$data['personal_record'] = $this->student_model->select_student_by_id($admission_no);
			$data['count_etypes'] = $this->student_model->select_etypes_by_id($admission_no, $term_id)['no_etypes'];
			
			//echo $data['count_etypes']; exit;
			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			//The variable is used in individual_results_bwiru for testing whether results are computed by division or grade
			$data['compute_by_division'] = "";
			$data['compute_by_grade'] = "OK";
			
			$this->load->view('templates/teacher_header');
			$this->load->view('students/individual_results_bwiru', $data);
			$this->load->view('templates/foot');
		}

		public function print_results_by_division($admission_no, $term_id){
			//****************************************************************************************
			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}
			/*$data['grade_system'] = $this->student_model->select_o_level_grade_system();
			$data['divisions'] = $this->student_model->select_divisions($class_level);*/

			/*$data['term_id'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level'])['term_id'];
			$term_id = $data['term_id'];*/

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_school_results') == 'ok' || ($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $data['record']['class_stream_id'])){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			//$data['academic_year'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level']);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);
			//$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			$data['personal_record'] = $this->student_model->select_student_by_id($admission_no);
			$data['count_etypes'] = $this->student_model->select_etypes_by_id($admission_no, $term_id)['no_etypes'];

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['compute_by_division'] = "OK";
			$data['compute_by_grade'] = "";

			
			$this->load->view('templates/teacher_header');
			$this->load->view('students/individual_results_bwiru', $data);
			$this->load->view('templates/foot');
		}

		//START PRINT ALL BWIRU
		//START RESULTS LOOP
		public function print_loop_by_grade_bwiru($class_stream_id, $term_id){
			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_school_results') == 'ok' || ($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			$students_array['students_record'] = $this->student_model->select_students_waliofanya_mtihani($class_stream_id);

			$this->load->view('templates/teacher_header');
			$this->load->view('students/printAllHeaderBwiru');

			foreach ($students_array['students_record'] as $key => $value) {
				$this->print_all_by_grade_bwiru($value['admission_no'], $term_id);
			}

			$this->load->view('students/printAllFooterBwiru');
			$this->load->view('templates/foot');
		}

		public function print_all_by_grade_bwiru($admission_no, $term_id){
			//****************************************************************************************
			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}
			/*$data['grade_system'] = $this->student_model->select_o_level_grade_system();
			$data['divisions'] = $this->student_model->select_divisions($class_level);*/

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			//$data['academic_year'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level']);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);
			/*$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);*/
			$data['count_etypes'] = $this->student_model->select_etypes_by_id($admission_no, $term_id)['no_etypes'];
			$data['personal_record'] = $this->student_model->select_student_by_id($admission_no);			

			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['compute_by_division'] = "";
			$data['compute_by_grade'] = "OK";
			
			$this->load->view('students/print_all_results_bwiru', $data);
		}

		public function print_loop_by_division_bwiru($class_stream_id, $term_id){
			//Imewekwa hapa kwa ajili ya kupata class_stream_id
			$allowed = FALSE;
			if($this->session->userdata('view_school_results') == 'ok' || ($this->session->userdata('view_own_class_results') == 'ok' && $this->session->userdata('class_stream')['class_stream_id'] === $class_stream_id)){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);


			$students_array['students_record'] = $this->student_model->select_students_waliofanya_mtihani($class_stream_id);

			$this->load->view('templates/teacher_header');
			$this->load->view('students/printAllHeaderBwiru');

			foreach ($students_array['students_record'] as $key => $value) {
				$this->print_all_by_division_bwiru($value['admission_no'], $term_id);
			}

			$this->load->view('students/printAllFooterBwiru');
			$this->load->view('templates/foot');
		}

		public function print_all_by_division_bwiru($admission_no, $term_id){
			//****************************************************************************************
			//Obtain class_level
			$class_level = $this->admin_model->select_academic_year_term_id($term_id)['class_level'];
			if($class_level === "O'Level"){
				$data['grade_system'] = $this->student_model->select_o_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_o_division($admission_no, $term_id);
			}
			elseif($class_level === "A'Level"){
				$data['grade_system'] = $this->student_model->select_a_level_grade_system();
				$data['divisions'] = $this->student_model->select_divisions($class_level);
				$data['division_and_points'] = $this->student_model->calculate_a_division($admission_no, $term_id);
			}
			

			$data['mkeka_single'] = $this->student_model->select_single_student_result($admission_no, $term_id);

			//Special for retrieving position, names, academic_year etc
			$data['record'] = $this->student_model->select_single_student_position_by_term($admission_no, $term_id);
			$data['no_of_students'] = $this->student_model->count_students_per_class($this->student_model->select_class_by_std_and_term($admission_no, $term_id), $term_id);
			$data['class_teacher'] = $this->student_model->get_class_teacher($admission_no);
			//$data['academic_year'] = $this->student_model->get_current_term_id_by_level($this->student_model->get_student_class_level($admission_no)['level']);
			$data['academic_year'] = $this->admin_model->select_academic_year_term_id($term_id);
			$data['count_etypes'] = $this->student_model->select_etypes_by_id($admission_no, $term_id)['no_etypes'];
			$data['personal_record'] = $this->student_model->select_student_by_id($admission_no);


			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['compute_by_division'] = "OK";
			$data['compute_by_grade'] = "";
			
			$this->load->view('students/print_all_results_bwiru', $data);
		}
		//END PRINT ALL BWIRU


		public function enrollment(){
			if(!($this->session->userdata('view_registered_students') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
				$value = trim($this->input->post('search_record'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
			}

			$config = array();
			$config['base_url'] = base_url() . 'students/enrollment/'.$value;
			$config['total_rows'] = $this->student_model->count_students_enrollment($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['enrollment_data'] = $this->student_model->select_students_enrollment($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);
			
			if(! file_exists(APPPATH.'views/students')){
				show_404();
			}

			$data['display_back'] = "NO";
			$data['title'] = "Enrollment";
			$data['p'] = $page;
			
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('students/students_enrollment');
			$this->load->view('templates/foot');
		}
	}
