<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Staffs extends MY_Controller{
		public function __construct(){
			parent:: __construct();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			
			//$this->refreshRoles();
	
			$this->load->model('staff_model');
			$this->load->model('subject_model');
			$this->load->model('student_model');
			$this->load->model('department_model');
			$this->load->model('notification_model');
			$this->load->library('pagination');
			$this->load->model('admin_model');

			if($this->session->userdata('user_role') !== "Admin"){
                                $this->refreshRoles();
                        }
			//$this->errorAcademicYearSettings();
		}
		
		public function new_announcement(){
			if(!($this->session->userdata('manage_posted_announcements') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['groups'] = $this->notification_model->select_all_groups();

			$data['title'] = $this->breadCrumb("new_announcement", NULL, NULL, NULL, "New Announcement", NULL, NULL);
			

			if($this->form_validation->run('announcement') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/new_announcement');
				$this->load->view('templates/foot');
			}
			else{
				$group_id = $this->input->post('group_id');
				$heading = $this->input->post('heading');
				$msg = $this->input->post('msg');
				$staff_id = $this->input->post('staff_id');

				$rs = $this->notification_model->insert_announcement(/*$record*/$group_id, $heading, $msg, $staff_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Announcement successfully posted");
				}
				redirect('staffs/new_announcement');
			}				
		}

		public function announcements($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('manage_posted_announcements') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_announcement') != ""){
	            $value = trim($this->input->post('search_announcement'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/announcements/'.$staff_id.'/'.$value;
			$config['total_rows'] = $this->notification_model->count_announcements_by_staff($staff_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['announcement_records'] = $this->notification_model->select_all_announcements($staff_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['display_back'] = "";

			$data['title'] = $this->breadCrumb("announcements/".$staff_id, NULL, NULL, NULL, "Posted Announcement", NULL, NULL);
			$data['staff_id'] = $staff_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/announcements');
			$this->load->view('templates/foot');
		}

		public function edit_announcement($announcement_id = FALSE){
			$allowed = FALSE;
			if($this->session->userdata('manage_posted_announcements') == 'ok' && $this->session->userdata('staff_id') === $this->notification_model->select_announcement_by_id($announcement_id)['posted_by']){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['groups'] = $this->notification_model->select_all_groups();
			$data['ar'] = $this->notification_model->select_announcement_by_id($announcement_id);
			$staff_id = $this->notification_model->select_announcement_by_id($announcement_id)['posted_by'];

			$data['title'] = $this->breadCrumb("staffs/announcements/".$staff_id, "Edit", NULL, NULL, "Posted Announcement", "Edit Announcement", NULL);
			

			if($this->form_validation->run('announcement') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/edit_announcement');
				$this->load->view('templates/foot');
			}
			else{
				$user_id = $this->session->userdata('staff_id');
				$heading = $this->input->post('heading');
				$msg = $this->input->post('msg');
				$announcement_id = $this->input->post('announcement_id');

				$record = array(
					'posted_by' => $user_id,
					'heading' => $heading,
					'msg' => $msg
				);

				$rs = $this->notification_model->update_announcement($record, $announcement_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Announcement updated");
				}
				redirect('staffs/announcements/'.$staff_id);
			}
		}

		public function publish_announcement($announcement_id){
			if(!($this->session->userdata('manage_posted_announcements') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('status')){
				$user_id = $this->input->post('user_id');
				if($this->input->post('status') === "show"){
					$status = "hide";
				}

				if($this->input->post('status') === "hide"){
					$status = "show";
				}
				
				$record = array(
					'status' => $status,
				);

				$rs = $this->notification_model->publish_announcement($announcement_id, $record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully publish");
				}
				redirect('staffs/announcements/'.$user_id);
			}
		}

		public function my_announcements($staff_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_announcements') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('search_announcement') != ""){
	            $value = trim($this->input->post('search_announcement'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/my_announcements/'.$staff_id.'/'.$value;
			$config['total_rows'] = $this->notification_model->count_my_announcements($staff_id, $value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['my_announcements'] = $this->notification_model->select_my_announcements($staff_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "";

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;
			$data['title'] = $this->breadCrumb("my_announcements/".$staff_id, NULL, NULL, NULL, "Announcements", NULL, NULL);
	

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/my_announcements');
			$this->load->view('templates/foot');
		}

		public function mark_as_read(){
			$announcement_id = $this->input->post('announcement_id');
			$staff_id = $this->input->post('staff_id');
			$status = 'read';

			if($this->input->post('announcement_id')){
				$record = array(
					'is_read' => $status
				);

				$rs = $this->notification_model->mark_announcement_status_as_read($staff_id, $announcement_id, $record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Marked as read");
				}

				//Update Announcements
				$announcement = $this->notification_model->count_announcements($staff_id);
				$this->session->set_userdata('announcements', $announcement);

				$announcement_records = $this->notification_model->select_announcements_by_staff_id($staff_id);
				$this->session->set_userdata('announcement_records', $announcement_records);
				//Update Announcements

				redirect('staffs/my_announcements/'.$staff_id);
			}
			else{
				redirect('staffs/my_announcements/'.$this->session->userdata('staff_id'));
			}
		}

		public function load_new_attendance(){
			if(!($this->session->userdata('manage_staff_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staffs'] = $this->staff_model->select_all_staffs();

			$data['title'] = "All Staffs";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/new_attendance');
			$this->load->view('templates/foot');
		}

		public function update_staff_attendance(){
			$date_ = $this->input->post('date_');
			$staff_id = $this->input->post('staff_id');
			$att_id = $this->input->post('att_id');
			$val = $this->input->post('v');
			$offset = $this->input->post('offset');

			if($this->form_validation->run('edit_attendance') == FALSE){
				redirect('staffs/daily_attendance/'.$date_.'/'.$val.'/'.$offset);
			}
			else{
				$attendance_record = array(
					'att_id' => $att_id
				);

				$rs = $this->staff_model->update_attendance($staff_id, $date_, $attendance_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Successfully updated");
				}
				redirect('staffs/daily_attendance/'.$date_.'/'.$val.'/'.$offset);
			}
		}

		public function attendance_report(){
			if(!($this->session->userdata('view_staff_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Attendance Report";

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}
			
			$data['is_daily'] = "";	//!Important: Avoid Undefined variable: is_monthly
			$data['is_monthly'] = "";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_attendance_report_header');
			$this->load->view('templates/foot');
		}


		public function daily_attendance($date = NULL){
			if(!($this->session->userdata('view_staff_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if ($this->input->post('search_staff') != "") {
				$value = trim($this->input->post('search_staff'));	//Search ya kutafuta staff
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
			}

			if($this->input->post('search_record') != ""){
				$date = $this->input->post('tarehe');	//Search ya kutafuta by date
			}

			//Custom New Code
			$config = array();
			$config['base_url'] = base_url() . 'staffs/daily_attendance/'.$date.'/'.$value;
			$config['total_rows'] = $this->staff_model->count_staffs($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['staffs'] = $this->staff_model->select_all_staff_attendance_record($date, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			//End Custom New Code
			
			$data['title'] = "Staff Attendance";

			$data['date'] = $date;
			$data['is_daily'] = "YES";	//These are special for the search field ...
			$data['is_monthly'] = "NO";	//.. they help redirect the search to the corresponding attendance

			$data['no_of_staffs']  = $this->staff_model->select_daily_attendance_summary($date);

			//For populating the select element when editing staff_attendance
			$data['attendance_records'] = $this->staff_model->pfs_attendance();

			$data['v'] = $value;		//For Redirection in case kuna error in update att_record
			$data['offset'] = $page;	//For Redirection in case kuna error in update att_record

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/classes')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_attendance_report_header');
			$this->load->view('staffs/staff_daily_attendance_report');
			$this->load->view('templates/foot');
		}

		public function monthly_attendance_report($year_and_month = NULL){
			if(!($this->session->userdata('view_staff_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($year_and_month == NULL){
				$year_and_month = explode('-', date('Y-m-d'))[0] . '-' . explode('-', date('Y-m-d'))[1];
			}

			if ($this->input->post('search_staff') != "") {
				$value = trim($this->input->post('search_staff'));
				$this->no_record_found_message();
			}
			else{
				$value = str_replace("%20",' ',($this->uri->segment(5)) ? $this->uri->segment(4) : 0);
			}

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['title'] = "Attendance Report";
			$x = 0;

			if($this->input->post('monthValue') != ""){
				$search_month_id_name = trim($this->input->post('monthValue'));
				$year = trim($this->input->post('yearSearch'));
				$month = explode('-', $search_month_id_name)[0];

				$year_and_month = $year . '-' . $month;

				$x = 1;	//For flag
			}
			else{
				$month = explode('-', $year_and_month)[1];
				$year = explode('-', $year_and_month)[0];

				$x = 1;	//For flag
			}

			//Custom New Code
			$config = array();
			$config['base_url'] = base_url() . 'staffs/monthly_attendance_report/'.$year_and_month.'/'.$value;
			$config['total_rows'] = $this->staff_model->count_staffs($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['monthly_attendance_records'] = $this->staff_model->create_monthly_report($month, $year, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['display_back'] = "NO";
			//End Custom New Code

			//Flag used in view for feedeback message
			if($x === 0){
				$data['flag'] = TRUE;
			}
			else{
				$data['flag'] = FALSE;	
			}


			$data['title'] = "Attendance Report: ". $month . " : " . $year;

			$data['date'] = $year_and_month;

			$data['is_daily'] = "NO";	//These are special for the search field ...
			$data['is_monthly'] = "YES";	//.. they help redirect the search to the corresponding attendance

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_attendance_report_header');
			$this->load->view('staffs/staff_monthly_attendance_report');
			$this->load->view('templates/foot');
		}

		public function save_staff_attendance(){
			if(!($this->session->userdata('manage_staff_attendance') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$level = "O'Level";	// O'Level Academic Year is like the regular year unlike A'Level Academic year
			$data['staff_id'] = $this->input->post('staff_id');
			$date = $this->input->post('date');			
			$data['att'] = $this->input->post('attendance');
			$aid = $this->admin_model->select_academic_year($level)['id'];
			$reason = $this->input->post('reason');


			$days = array(1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday", 0 => "Sunday");

			$time = strtotime($date);
			$month = date("F",$time);

			$dayofweek = date('w', strtotime($date));
			$weekday = $days[$dayofweek];	//Obtain the day of week in text
			

			if($weekday == "Saturday" || $weekday == "Sunday"){
				$this->session->set_flashdata('error_message', 'Date should fall on weekdays, NOT weekends');
				redirect('staffs/load_new_attendance');
			}
			else{
				if($this->form_validation->run('add_staff_attendance') == FALSE){
					$this->session->set_flashdata('errors', validation_errors());
					redirect('staffs/load_new_attendance');
				}
				else{
					if($date > date('Y-m-d')){
						$this->session->set_flashdata('error_message', "Error, the date you are attemting to enter is beyond today's date");
						redirect('staffs/load_new_attendance/'.$data['staff_id']);
					}
					else{
						foreach ($data['staff_id'] as $key => $value) {
							$staff_attendance_record[] = array(
								'staff_id' => $value,
								'day' => $weekday,
								'date_' => $date,
								'aid' => $aid,
								'att_id' => $data['att'][$key],
								'reason' => $reason
							);
						}
	
						$rs = $this->staff_model->insert_staff_attendance_record($staff_attendance_record);
						if($rs){
							$this->transactionSuccessfullyCommitted("Attendance has been added");
						}
						redirect('staffs/load_new_attendance');
					}
				}
			}
		}

		public function new_staff(){
			if(!($this->session->userdata('manage_staff') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "New Staff";
			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['regions'] = $this->student_model->select_regions();
			$data['subjects'] = $this->subject_model->select_from_subjects();
			$data['staffs'] = $this->staff_model->select_all_staffs();

			if($this->form_validation->run('add_staff') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/new_staff');
				//$this->load->view('staffs/form_phase');
				$this->load->view('templates/foot');
			}
			else{
				$this->add_new_staff();
			}			
		}

		public function add_new_staff(){
			if(!($this->session->userdata('manage_staff') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$staff_id = $this->input->post('staff_id');
			$firstname = $this->input->post('firstname');
			$middlename = $this->input->post('middlename');
			$lastname = $this->input->post('lastname');
			$dob = $this->input->post('dob');
			$marital_status = $this->input->post('marital_status');
			$gender = $this->input->post('gender');
			$email = $this->input->post('email');
			$phone_no = $this->input->post('phone_no');
			$staff_type = $this->input->post('staff_type');


			$hashed_password = sha1($lastname);

			//RELATIVE'S INFORMATION
			$r_firstname = $this->input->post('r_firstname');
			$r_lastname = $this->input->post('r_lastname');
			$r_box = $this->input->post('r_box');
			$r_region = $this->input->post('r_region');
			$r_gender = $this->input->post('r_gender');
			$r_relationship = $this->input->post('r_relationship');
			$r_phone = $this->input->post('r_phone');

			if($staff_type === "T"){
				$user_role = 'teacher';
			}
			if($staff_type === "A"){
				$user_role = 'accountant';
			}
			if($staff_type === "L"){
				$user_role = 'library';
			}
			if($staff_type === "S"){
				$user_role = 'secretary';
			}


			//Assign staff record to an array
			$staff_record = array(
				'staff_id' => $staff_id,
				'firstname' => $firstname,
				'middlename' => $middlename,
				'lastname' => $lastname,
				'dob' => $dob,
				'marital_status' => $marital_status,
				'gender' => $gender,
				'email' => $email,
				'phone_no' => $phone_no,
				'staff_type' => $staff_type,
				'user_role' => $user_role,
				'password' => $hashed_password
			);//End assign to staff_record array


			//Assign nok record in an array
			$nok_record = array(
				'staff_id' => $staff_id,
				'firstname' => $r_firstname,
				'lastname' => $r_lastname,
				'gender' => $r_gender,
				'relation_type' => $r_relationship,
				'phone_no' => $r_phone,
				'p_box' => $r_box,
				'p_region' => $r_region
			);//END


			$subject_teacher_record = array();	//Initializa array
			$staff_department_record = array();	//Initializa array
			if(isset($_POST['subject_id'])){
				$data['subjects'] = $this->input->post('subject_id');
				$subjects = array();	//Create new Array
				$subjects = $data['subjects'];	//Assign the returned array into $subjects array


				
				foreach ($subjects as $key => $value) {
					$subject_teacher_record[] = array(
						'subject_id' => $value,
						'teacher_id' => $staff_id
					);

					$dept_id = $this->subject_model->select_from_subjects($value)['dept_id'];//Get the dept id
					$staff_department_record[] = array(
						'staff_id' => $staff_id,
						'dept_id' => $dept_id
					);
				}
			}	

			$subclass_record = array(
				'staff_id' => $staff_id	//The record to be inserting in one of staff's subclasses
			);

			//Start testing the staff type
			if($staff_type == "T"){
				//insert staff_department_record & subject_teacher_record only when ni teacher
				$rs = $this->staff_model->register_staff($staff_record, $nok_record, /*$subject_teacher_record, $staff_department_record, */$subclass_record);
			}
			else{
				//if not teacher set staff_dept_record & sub_teacher_record to NULL
				$rs = $this->staff_model->register_staff($staff_record, $nok_record, /*$subject_teacher_record = NULL, $staff_department_record = NULL, */$subclass_record);
			}
			//End testing staff_type
			if($rs){
				$this->transactionSuccessfullyCommitted("New staff successfully added");
			}
			redirect('staffs/new_staff');
		}


		public function profile($staff_id){
			//Test whether the user is allowed to see the staff profile 
			//i.e: Whether it is that staff is in that particular department			
			$allowed = FALSE;
			if($this->session->userdata('view_staff_details') == 'ok' || ($this->session->userdata('view_own_departments_staffs') == 'ok' && $this->session->userdata('dept')['dept_id'] === $this->staff_model->select_by_staff_and_dept_id($staff_id, $this->session->userdata('dept')['dept_id']))){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);
			//End testing

			$data['title'] = "STAFF: " . $staff_id;
			$data['staff_info'] = $this->staff_model->select_staff_information($staff_id);
			$data['certifications'] = $this->staff_model->select_staff_certifications($staff_id);
			$data['image'] = $this->staff_model->retrieve_profile_picture($staff_id);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_profile_header');
			$this->load->view('staffs/staff_profile');
			$this->load->view('templates/foot');
		}

		public function attendance($staff_id){
			//Test whether the user is allowed to see the staff profile 
			//i.e: Whether it is that staff is in that particular department
			$allowed = FALSE;
			if($this->session->userdata('view_staff_details') == 'ok' || ($this->session->userdata('view_own_departments_staffs') == 'ok' && $this->session->userdata('dept')['dept_id'] === $this->staff_model->select_by_staff_and_dept_id($staff_id, $this->session->userdata('dept')['dept_id']))){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);
			//End testing

			$data['title'] = "Attendance record";
			$data['staff_attendance_record'] = $this->staff_model->select_staff_attendance_record($staff_id);
			$data['image'] = $this->staff_model->retrieve_profile_picture($staff_id);

			$data['staff_id'] = $staff_id;

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_profile_header');
			$this->load->view('staffs/view_staff_attendance');
			$this->load->view('templates/foot');
		}

		public function myProfile($staff_id = NULL){
			$allowed = FALSE;
			if($this->session->userdata('view_own_profile') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			/*$staff_names = $this->staff_model->select_staff_information($staff_id)['firstname'] . " " . $this->staff_model->select_staff_information($staff_id)['lastname'];
			$data['title'] = $staff_names;*/
			$data['title'] = $this->breadCrumb("myProfile/".$staff_id, NULL, NULL, NULL, "Profile", NULL, NULL);
			
			$data['staff_info'] = $this->staff_model->select_staff_information_and_role($staff_id);
			$data['staff_group_info'] = $this->admin_model->select_staff_group_by_staff_id($staff_id);
			$data['certifications'] = $this->staff_model->select_staff_certifications($staff_id);
			$data['image'] = $this->staff_model->retrieve_profile_picture($staff_id);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/myProfile', array('error' => ''));
			$this->load->view('templates/foot');
		}

		public function edit_profile($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('update_own_profile') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			$data['title'] = $this->breadCrumb("staffs/myProfile/".$staff_id, "edit_profile/".$staff_id, NULL, NULL, "Profile", "Edit", NULL);
			$data['staff_record'] = $this->staff_model->select_staff_information($staff_id);
			$data['certifications'] = $this->staff_model->select_staff_certifications($staff_id);


			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;

			if($this->form_validation->run('update_profile') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/edit_profile');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_profile($staff_id);
			}
		}

		public function update_profile($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('update_own_profile') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);

			if($this->input->post('update_profile')){
				$firstname = $this->input->post('firstname');
				$middlename = $this->input->post('middlename');
				$lastname = $this->input->post('lastname');
				$dob = $this->input->post('dob');
				$marital_status = $this->input->post('marital_status');
				$gender = $this->input->post('gender');
				$email = $this->input->post('email');
				$phone_no = $this->input->post('phone_no');

				$staff_record = array(
					'firstname' => $firstname,
					'middlename' => $middlename,
					'lastname' => $lastname,
					'dob' => $dob,
					'marital_status' => $marital_status,
					'gender' => $gender,
					'email' => $email,
					'phone_no' => $phone_no
				);

				$rs = $this->staff_model->update_profile($staff_record, $staff_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Profile successfully updated");
				}
				redirect('staffs/edit_profile/'.$staff_id);
			}
		}

		public function change_password($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('change_password') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);			

			$data['title'] = $this->breadCrumb("change_password/".$staff_id, NULL, NULL, NULL, "Change Password", NULL, NULL);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;

			if($this->form_validation->run('change_password') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/change_password');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_password();
			}
		}

		public function update_password(){
			$staff_id = $this->input->post('staff_id');
			$allowed = FALSE;
			if($this->session->userdata('change_password') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			//$this->authenticate_user_permission('home', $allowed);

			$staff_id = $this->input->post('staff_id');
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password');
			$confirm_new_password = $this->input->post('confirm_new_password');

			$hashed_current_password = sha1($current_password);

			$query = $this->staff_model->select_current_password($staff_id, $hashed_current_password);

			if($query === 1){
				$password_record = array(
					'password' => sha1($confirm_new_password)
				);

				$rs = $this->staff_model->update_password($staff_id, $password_record);
				if($rs){
					$this->transactionSuccessfullyCommitted("Password successfully changed");
				}
				redirect('staffs/change_password/'.$staff_id);
			}
			else{
				$this->session->set_flashdata('error_message', 'Invalid Current Password');
				redirect('staffs/change_password/'.$staff_id);
			}
		}        
     
     // retrieve all staffs @de//
        public function index(){
        	if(!($this->session->userdata('view_staffs') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

        	if($this->input->post('search_staff') != ""){
	            $value = trim($this->input->post('search_staff'));
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/index/'.$value;
			$config['total_rows'] = $this->staff_model->count_staffs($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['staffs'] = $this->staff_model->fetch_staffs($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['certifications'] = $this->staff_model->select_staff_certifications();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['display_back'] = "NO";

			$data['title'] = "Staffs";
			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/view_all_staffs');
			$this->load->view('templates/foot');

		}

		public function password_reset(){
			if(!($this->session->userdata('reset_password') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_staff') != ""){
	            $value = trim($this->input->post('search_staff'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/password_reset/'.$value;
			$config['total_rows'] = $this->staff_model->count_active_staffs($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['staffs'] = $this->staff_model->fetch_active_staffs($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['display_back'] = "NO";

			$data['title'] = "Staffs";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/password_reset');
			$this->load->view('templates/foot');
		}

		public function reset($staff_id){
			if(!($this->session->userdata('reset_password') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('reset')){
				$record = array(
					'staff_id' => $staff_id
				);

				$rs = $this->admin_model->reset_password($record, $staff_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Password has been successfully reset");
				}
				redirect('staffs/password_reset');
			}
		}

		public function reset_all(){
			if(!($this->session->userdata('reset_password') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$rs = $this->admin_model->reset_all_passwords();
			if($rs){
				$this->transactionSuccessfullyCommitted("All passwords are successfully reset to default password");
			}
			redirect('staffs/password_reset');
		}

		public function manage_accounts(){
			if(!($this->session->userdata('manage_staff_accounts') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_staff') != ""){
	            $value = trim($this->input->post('search_staff'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/manage_accounts/'.$value;
			$config['total_rows'] = $this->staff_model->count_staffs($value);
			$config['per_page'] = 10;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
			$data['staffs'] = $this->staff_model->fetch_staffs($config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['display_back'] = "NO";

			$data['title'] = "Staffs";

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/view_staffs_manage_accounts');
			$this->load->view('templates/foot');
		}

		public function edit_staff($staff_id){
			if(!($this->session->userdata('manage_staff') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$data['title'] = "Edit Staff";
			$data['staff_record'] = $this->staff_model->select_staff_information($staff_id);
			$data['regions'] = $this->student_model->select_regions();
			$data['subject_teacher_record'] = $this->admin_model->select_subject_teacher_by_teacher_id($staff_id);
			$data['subjects'] = $this->subject_model->select_from_subjects();
			$data['role'] = $this->admin_model->select_user_role_by_id($staff_id);


			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;

			if($this->form_validation->run('edit_staff') == FALSE){
				$this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/edit_staff');
				$this->load->view('templates/foot');
			}
			else{
				$this->update_staff();
			}
		}

		public function user_log_records($staff_id = NULL){
			if(!($this->session->userdata('view_staff_activity_log') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			if($this->input->post('search_record') != ""){
	            $value = trim($this->input->post('search_record'));
	            $this->no_record_found_message();
	        }
	        else{
	            $value = str_replace("%20",' ',($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
	        }

			$config = array();
			$config['base_url'] = base_url() . 'staffs/user_log_records/'.$staff_id.'/'.$value;
			$config['total_rows'] = $this->admin_model->count_activity_log_records_by_user_id($staff_id, $value);
			$config['per_page'] = 5;
			$config['num_links'] = 3;
			//$config['url_segment'] = 3;

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = "</ul>";
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = "</li>";
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : '0';
			$data['activity_log_records'] = $this->admin_model->fetch_activity_log_records_by_user_id($staff_id, $config['per_page'], $page, $value);
			$data['links'] = $this->pagination->create_links();

			$data['title'] = "Actitvity Log Records";
			$data['flag'] = "IS_SET";
			$data['user_id'] = $staff_id;

			$data['x_of_y_entries'] = $this->renderPagination($page, $config['per_page'], $config['total_rows']);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;
			$data['image'] = $this->staff_model->retrieve_profile_picture($staff_id);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_profile_header');
			$this->load->view('staffs/view_activity_log_records');
			$this->load->view('templates/foot');
		}

		public function staff_details($staff_id = NULL){
			//Test whether the user is allowed to see the staff profile 
			//i.e: Whether it is that staff is in that particular department
			if(!($this->session->userdata('view_staff_details') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			//End testing

			//$data['title'] = "STAFF: " . $staff_id;
			$data['title'] = "ADMINISTRATIVE DETAILS";
			$data['staff_info'] = $this->staff_model->select_staff_information_and_role($staff_id);
			//$data['staff_admin_info'] = $this->admin_model->select_admin_group_by_staff_id($staff_id);
			$data['staff_group_info'] = $this->admin_model->select_staff_group_by_staff_id($staff_id);

			if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

			$data['staff_id'] = $staff_id;
			$data['image'] = $this->staff_model->retrieve_profile_picture($staff_id);

			$this->load->view('templates/teacher_header', $data);
			$this->load->view('staffs/staff_profile_header');
			$this->load->view('staffs/staff_details');
			$this->load->view('templates/foot');
		}

		public function update_staff(){
			if(!($this->session->userdata('manage_staff') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$staff_id = $this->input->post('staff_id');
			$firstname = $this->input->post('firstname');
			$middlename = $this->input->post('middlename');
			$lastname = $this->input->post('lastname');
			$dob = $this->input->post('dob');
			$marital_status = $this->input->post('marital_status');
			$gender = $this->input->post('gender');
			$email = $this->input->post('email');
			$phone_no = $this->input->post('phone_no');
			$staff_type = $this->input->post('staff_type');


			//RELATIVE'S INFORMATION
			$r_firstname = $this->input->post('r_firstname');
			$r_lastname = $this->input->post('r_lastname');
			$r_box = $this->input->post('r_box');
			$r_region = $this->input->post('region');
			$r_gender = $this->input->post('r_gender');
			$r_relationship = $this->input->post('r_relationship');
			$r_phone = $this->input->post('r_phone');

			/*if($staff_type === "T"){
				$user_role = 'teacher';
			}
			if($staff_type === "A"){
				$user_role = 'accountant';
			}
			if($staff_type === "L"){
				$user_role = 'library';
			}
			if($staff_type === "S"){
				$user_role = 'secretary';
			}
*/

			//Assign staff record to an array
			$staff_record = array(
				//'staff_id' => $staff_id,
				'firstname' => $firstname,
				'middlename' => $middlename,
				'lastname' => $lastname,
				'dob' => $dob,
				'marital_status' => $marital_status,
				'gender' => $gender,
				'email' => $email,
				'phone_no' => $phone_no,
				/*'staff_type' => $staff_type,
				'user_role' => $user_role*/
			);//End assign to staff_record array


			//Assign nok record in an array
			$nok_record = array(
				'staff_id' => $staff_id,
				'firstname' => $r_firstname,
				'lastname' => $r_lastname,
				'gender' => $r_gender,
				'relation_type' => $r_relationship,
				'phone_no' => $r_phone,
				'p_box' => $r_box,
				'p_region' => $r_region
			);//END


			/*$subject_teacher_record = array();	//Initializa array
			$staff_department_record = array();	//Initializa array
			if(isset($_POST['subject_id'])){
				$data['subjects'] = $this->input->post('subject_id');
				$subjects = array();	//Create new Array
				$subjects = $data['subjects'];	//Assign the returned array into $subjects array


				
				foreach ($subjects as $key => $value) {
					$subject_teacher_record[] = array(
						'subject_id' => $value,
						'teacher_id' => $staff_id
					);

					$dept_id = $this->subject_model->select_from_subjects($value)['dept_id'];//Get the dept id
					$staff_department_record[] = array(
						'staff_id' => $staff_id,
						'dept_id' => $dept_id
					);
				}
			}	*/

			$subclass_record = array(
				'staff_id' => $staff_id	//The record to be inserting in one of staff's subclasses
			);

			//Start testing the staff type
			if($staff_type == "T"){
				//echo $staff_id; exit;
				//insert staff_department_record & subject_teacher_record only when ni teacher
				$rs = $this->staff_model->update_staff($staff_id, $staff_record, $nok_record, $subject_teacher_record, $staff_department_record, $subclass_record);
			}
			else{
				//if not teacher set staff_dept_record & sub_teacher_record to NULL
				$rs = $this->staff_model->update_staff($staff_id, $staff_record, $nok_record, $subject_teacher_record = NULL, $staff_department_record = NULL, $subclass_record);
			}
			//End testing staff_type
			if($rs){
				$this->transactionSuccessfullyCommitted("Record Successfully updated");
			}
			redirect('staffs/edit_staff/'.$staff_id);
		}


		public function delete_staff($staff_id){
			if(!($this->session->userdata('activate_deactivate_staff_account') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
			$posted_status = $this->input->post('status');
			$reason = $this->input->post('reason');
			$time = $this->input->post('time_');
			//$staff_id = $this->input->post('staff_id');

			$inactive_record = array(
				'staff_id' => $staff_id,
				'reason' => $reason,
				'deactivated_date' => $time
			);

			if($posted_status === 'active'){			
				$status = 'inactive';
				$data = array(
					'status' => $status
				);

				$q = $this->admin_model->insert_remove_deactivation_reason($inactive_record, $staff_id);
			}
			else{
				$status = 'active';
				$data = array(
					'status' => $status
				);

				$q = $this->admin_model->insert_remove_deactivation_reason($inactive_record, $staff_id);
			}

			if($q){
				$rs = $this->admin_model->delete_staff($staff_id, $data);
				if($rs){
					$this->transactionSuccessfullyCommitted("Record Successfully updated");
				}
			}
			redirect(base_url() . 'staffs/manage_accounts');
		}
		//END STAFFS


		//Important when one is adding own qualification
	    public function add_staff_qualification($staff_id){
	    	$allowed = FALSE;
			if($this->session->userdata('add_own_qualification') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);		    	

	 		if(! file_exists(APPPATH.'views/staffs')){
				show_404();
			}

	        $data['staff'] = $this->staff_model->all_staff($staff_id);
	        $data['staff_id'] = $staff_id;

			$data['title'] = $this->breadCrumb("add_staff_qualification/".$staff_id, NULL, NULL, NULL, "Add Qualification", NULL, NULL);

	        if($this->form_validation->run('add-edit_qualification') == FALSE) {
		        $this->load->view('templates/teacher_header', $data);
				$this->load->view('staffs/add_qualification_@den');
				$this->load->view('templates/foot');
			}
			else{
				$this->save_qualification();
			}
	    }

	    public function remove_qualification($staff_id, $q_id){
	    	if(!($this->session->userdata('manage_staff_qualification') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

	    	$rs = $this->staff_model->remove_qualification($q_id);
	    	if($rs){
				$this->transactionSuccessfullyCommitted("Successfully removed");
			}
	    	redirect('staffs/profile/'.$staff_id); 
	    }

     	public  function save_qualification(){
            $staff_id = $this->input->post('staff_id');
            $certify = $this->input->post('certify');

            if($this->form_validation->run('add-edit_qualification') == FALSE) {
            	$this->session->set_flashdata('errors', validation_errors());
				redirect('staffs/profile/'.$staff_id); 
            }
            else{
				$record = array(
					'staff_id' =>$this->input->post('staff_id') ,
				    'certification' =>$this->input->post('certification') ,
				    'description' =>$this->input->post('description') ,
				    'university' =>$this->input->post('university') ,
				    'completion_date' =>$this->input->post('year'),
				    'certified' => $certify
			    );

		     	$rs = $this->staff_model->save_qualification($record);
		     	if($rs){
					$this->transactionSuccessfullyCommitted("Successfully added");
				}
		     	if($this->input->post('posted_by') === "account_owner"){
		     		redirect('staffs/myProfile/'.$staff_id); 
		     	}
		     	else{
		     		redirect('staffs/profile/'.$staff_id); 
		     	}
		    }
		}

     	public  function save_edit_qualification(){
     		if(!($this->session->userdata('manage_staff_qualification') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}
			
            $q_id = $this->input->post('q_id');
            $staff_id = $this->input->post('staff_id');
            $certify = 'yes';

			if ($this->form_validation->run('add-edit_qualification') === FALSE){
				$this->session->set_flashdata('errors', validation_errors());
				redirect('staffs/profile/'.$staff_id);     
		    }

		    $record = array(
		    	'certification' =>$this->input->post('certification') ,
                'description' =>$this->input->post('description') ,
                'university' =>$this->input->post('university') ,
                'completion_date' =>$this->input->post('year'),
				'certified' => $certify
		    );

		    $rs = $this->staff_model->save_edit_qualification($record, $q_id);
		    if($rs){
				$this->transactionSuccessfullyCommitted("Record successfully updated");
			}
		    redirect('staffs/profile/'.$staff_id);   
		}



		public function certify($staff_id, $q_id){
			if(!($this->session->userdata('manage_staff_qualification') == 'ok')){
				$this->authenticate_user_permission('home', FALSE);
			}

			$certify = "yes";
			$qualification_record=array(
				'certified' => $certify
		    );

			$rs = $this->staff_model->certify_qualification($qualification_record, $q_id);
			if($rs){
				$this->transactionSuccessfullyCommitted("Successfully certified");
			}
			redirect('staffs/profile/'.$staff_id); 
		}
//End of save edit @den

		/*public function check_password_accuracy(){
			$value = $this->input->post('current_password');
			$staff_id = $this->input->post('staff_id');
			$hashed_password = sha1($value);

			$query = $this->staff_model->check_if_compares($hashed_password);
			if($query === 1){
				echo "OK";
			}
			else{
				"No";
			}
		}*/


		//UPLOAD STAFF PHOTO
		public function upload_photo($staff_id){
			$allowed = FALSE;
			if($this->session->userdata('update_own_profile') == 'ok' && $this->session->userdata('staff_id') === $staff_id){
				$allowed = TRUE;
			}

			$this->authenticate_user_permission('home', $allowed);	
			
		    $config['upload_path'] = './assets/images';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '700';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$firstname = $this->staff_model->select_staff_information($staff_id)['firstname'];
			$lastname = $this->staff_model->select_staff_information($staff_id)['lastname'];
			//Special for the consistent file name
			$config['file_name'] = strtolower($firstname . "_" . $lastname);

	        $this->load->library('upload', $config);
		
	  		if(!$this->upload->do_upload('image_file')){	             
				if(! file_exists(APPPATH.'views/staffs')){
					show_404();
				}
	            $error = array(
	            	'error_msg' => $this->upload->display_errors()
	            ); 

	            $this->session->set_flashdata('error_upload', $error['error_msg']);
	            redirect('staffs/myProfile/'.$staff_id);	        	
		    }
			else{
				$data = $this->input->post();
				$info = $this->upload->data();
				$image_path = base_url("/assets/images/".$info['raw_name'].$info['file_ext']);
				
				$image_record = array('staff_image' => $image_path);//image link

				unset($data['submit']);
				$rs = $this->staff_model->in_staff_image($image_record, $staff_id);
				if($rs){
					$this->transactionSuccessfullyCommitted("Photo successfully added");
				}
			    redirect('staffs/myProfile/'.$staff_id);	           
			}
		}
	}
