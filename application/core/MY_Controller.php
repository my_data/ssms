<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Controller extends CI_Controller{
		public function __construct()	{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('file');
			$this->load->helper('download');
			$this->load->library('zip');
			
			$this->load->model('admin_model');
			$this->load->model('class_model');
			$this->load->model('department_model');
			$this->load->model('dormitory_model');
			$this->load->model('subject_model');
			$this->load->model('staff_model');
			$this->load->model('student_model');
			$this->load->model('login_model');
			$this->load->model('notification_model');
			$this->load->model('teacher_model');
			$this->load->model('report_model');
			$this->load->model('library_model');
			$this->load->model('period_model');			
		}

	
		public function refreshRoles(){
			//NOTIFICATION SESSION
           		$notification = $this->notification_model->count_notifications($this->session->userdata('staff_id'));
            		$this->session->set_userdata('notifications', $notification);

            		$notification_messages = $this->notification_model->select_notifications($this->session->userdata('staff_id'));
            		$this->session->set_userdata('notification_messages', $notification_messages);
            		//END NOTIFICATION SESSION

            		//ANNOUNCEMENT SESSION
            		$announcements = $this->notification_model->count_announcements($this->session->userdata('staff_id'));
            		$this->session->set_userdata('announcements', $announcements);
            		//ANNOUNCEMENT SESSION

           		 //TEACHING ASSIGNMENT COUNT
           		$assigned_classes = $this->teacher_model->count_teaching_assignment($this->session->userdata('staff_id'));
           		$this->session->set_userdata('teaching_classes', $assigned_classes);
           		//END TEACHING ASSIGNMENT

			//Grabbing subjects and classes assigned
           		$classes_assigned_to_teach = $this->teacher_model->select_assigned_classes_by_teacher_id($this->session->userdata('staff_id'));
		        $this->session->set_userdata('classes_assigned_to_teach', $classes_assigned_to_teach);
           		 //End subjects and classes assigned

			//Get class_stream array for a specific class teacher
			$class_stream_id = $this->class_model->select_class_stream_by_teacher_id($this->session->userdata('staff_id'));
			$this->session->set_userdata('class_stream', $class_stream_id);
			//End grabbing class_stream_id array

			//Grab department_id
			$department_id = $this->department_model->get_department_id_by_hod($this->session->userdata('staff_id'));
			$this->session->set_userdata('dept', $department_id);
			//End of daperment id

			//Grab dormitory id
			$dormitory_id = $this->dormitory_model->get_dorm_id_by_dorm_master_id($this->session->userdata('staff_id'));
			$this->session->set_userdata('dormitory', $dormitory_id);
			//End dorm id			

			$modules = $this->login_model->select_modules($this->session->userdata('staff_id'));
			$this->session->set_userdata('modules', $modules);
			//End Modules
			
			//Setting the user roles
			$user_role = $this->login_model->select_user_roles($this->session->userdata('staff_id'));
			$this->session->set_userdata('roles', $user_role);
			//End setting many roles

			$roles_permission = $this->login_model->select_role_permissions_by_staff_id($this->session->userdata('staff_id'));
			$this->session->set_userdata('roles_permission', $roles_permission);
		}

		public function breadCrumb($a = NULL, $b = NULL, $c = NULL, $d = NULL, $display = NULL, $display2 = NULL, $display3 = NULL){
			if($b == NULL && $c == NULL && $d == NULL){
				return $display;
			}
			else if($c == NULL && $d == NULL){
				return "<a href='".base_url()."".$a."'>".$display."</a> | ".$display2."";
			}
			else if($d == NULL){
				return "<a href='".base_url()."".$a."'>".$display."</a> | "."<a href='".base_url()."".$b."'>".$display2."</a> | ".$display3;
			}
			else{
				return "<a href='".base_url()."".$a."'>".$display."</a> | "."<a href='".base_url()."".$b."'>".$display2."</a> | "."<a href='".base_url()."".$c."'>".$display3."</a> | ".$d;
			}
		}

		public function mainLink($id, $name){
			return "<a href='#' data-toggle='collapse' data-target='".$id."'>".$name."</a>";
		}

		public function no_record_found_message(){
			$this->session->set_flashdata('search_message', 'No record matches your search');
		}

		public function transactionSuccessfullyCommitted($successMessage){
			$this->session->set_flashdata('success_message', $successMessage);
		}

		public function database_backup(){
			$this->load->dbutil();
			$dbFormat = array(
				'format'		=> 'zip',
				'filename'		=> 'school_management_system.sql',
				'add_drop'		=> TRUE,		// Whether to add DROP TABLE statements to backup file
        		'add_insert'	=> TRUE,		// Whether to add INSERT data to backup file
        		'newline'       => "\n"			// Newline character used in backup file
			);
			$backup = $this->dbutil->backup($dbFormat);
			$dbName = 'back_up_on-'.date('Y:m:d H:i:s').'zip';
			$saveDB = base_url() . 'assets/db_file/'.$dbName;
			write_file($saveDB, $backup);
			force_download($dbName, $backup);;
		}

		public function renderPagination($page, $per_page, $total_rows){
			$from = (int)$page + 1;

			if($page + $per_page > $total_rows) {
			    $to = $total_rows;
			} 
			else{
			    $to = (int)$page + $per_page;
			}
			if($total_rows === 0){
				return NULL;
			}
			else{
				return "Showing " . $from . " to " . $to . " of " . $total_rows . " entries";
			}
		}

		public function authenticate_user_permission($redirect_to, $allowed){
			if($allowed === FALSE){
				$this->session->set_flashdata('error_message', 'Access denied');
				redirect(base_url() . $redirect_to);
			}
		}

		public function authenticate_assigned_subjects($class_stream_id, $subject_id, $redirect_to){
			$authenticated = FALSE;
			foreach ($this->session->userdata('classes_assigned_to_teach') as $key => $value) {
				$class_stream_subject = $value['class_stream_id'] . '-' . $value['subject_id'];
				if($class_stream_subject === ($class_stream_id.'-'.$subject_id)){
					$authenticated = TRUE;
				}
			}

			if($authenticated == FALSE){
				$this->session->set_flashdata('error_message', 'Access denied, you are only allowed to access your own contents');
				redirect(base_url() . $redirect_to);
			}
			else{
				return $authenticated;
			}
		}

		public function errorAcademicYearSettings(){			
			if(7 <= date('m') && date('m') <= 12){
				$o_lvl_academic_year = date('Y');
				$a_lvl_academic_year = date('Y').'-'.(date('Y')+1);

				$o_lvl_count = $this->admin_model->academicYearSettings("O'Level", $o_lvl_academic_year, "second_term");

				$a_lvl_count = $this->admin_model->academicYearSettings("A'Level", $a_lvl_academic_year, "first_term");

				if(!($o_lvl_count == 1 && $a_lvl_count == 1)){
					//return FALSE;
					$this->load->view('templates/teacher_header');
					$this->load->view('errors/academic_year_error');	
					$this->load->view('templates/foot');
				}
				else{
					return FALSE;
				}

				//echo "O: " . $o_lvl_count . "A: " . $a_lvl_count;
			}
			if(1 <= date('m') && date('m') <= 6){
				$o_lvl_academic_year = date('Y');
				$a_lvl_academic_year = (date('Y')-1).'-'.date('Y');

				$o_lvl_count = $this->admin_model->academicYearSettings("O'Level", $o_lvl_academic_year, "first_term");

				$a_lvl_count = $this->admin_model->academicYearSettings("A'Level", $a_lvl_academic_year, "second_term");

				if(!($o_lvl_count == 1 && $a_lvl_count == 1)){
					//return FALSE;
					$this->load->view('templates/teacher_header');
					$this->load->view('errors/academic_year_error');	
					$this->load->view('templates/foot');
				}
				else{
					return FALSE;
				}

				//echo "O: " . $o_lvl_count . "A: " . $a_lvl_count;
			}
		}

		
	}
?>
