<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Model extends CI_Model{
		public function __construct()	{
			parent::__construct();
		}

		//Returning the 1 for success and 0 for error in the entire system
		public function feedbackMsg(){
			if($this->db->trans_status() === TRUE){
				return TRUE;
			}
			else{
				$this->db->trans_rollback();
		    	return FALSE;
		    }
		}

		//Check out this function
		public function CI_mySQL($table_name, $datas, $where, $action){
		    //To INSERT data
		    if($action === 'INSERT'){
		    	$this->db->insert($table_name, $datas);
		    }

		    if($action === 'INSERT_BATCH'){
		    	$this->db->insert_batch($table_name, $datas);
		    }

		    if($action === 'UPDATE_BATCH'){
		    	$this->db->where($where);
			    $this->db->delete($table_name);
		    	$this->db->insert_batch($table_name, $datas);
		    }

		    if($action === 'UPDATE_ALL'){
			    $this->db->update($table_name, $datas);
			}

		    //To UPDATE data
		    if($action === 'UPDATE'){
			    $this->db->where($where);
			    $this->db->update($table_name, $datas);
			}
		    //To DELETE data
		    if($action === 'DELETE'){
		    	$this->db->where($where);
			    $this->db->delete($table_name);
		    }			    

		    $error = $this->db->error();
		    $errorCode = $error['code'];
		    $errorText='';

		    switch($errorCode){
		    	case 1644:
		    		$errorText = $error['message'];
		    		break;
		    	case 1451:
		    		$errorText = 'Cannot Delete '.ucfirst($table_name).'';
		    		break;
		        case 1062:
		            $errorText = 'The record you are trying to enter already exist';
		            break;
		        case 1452:
		            $errorText = 'Foreign key Constraint';
		            break;
		        case 1146:
		            $errorText = 'Table Not exist';
		            break;		        
		        default:
		            $errorText = 'other errors';
		            break;
		    }

		    //then print or return $errorText
		    //return $errorCode;
		    //return $errorText;
		    if($errorCode != 0){
		    	$this->session->set_flashdata('error_message', ''.$errorText.'');
		    	return;
		    }
		}

		public function CI_getProcedureError(){
			$error = $this->db->error();
		    $errorCode = $error['code'];
		    $errorText='';

		    switch($errorCode){
		    	case 1644:
		    		$errorText = $error['message'];
		    		break;
		        case 1062:
		            $errorText = 'The record you are trying to enter already exist';
		            break;
		        case 1452:
		            $errorText = 'Foreign key Constraint';
		            break;
		        case 1146:
		            $errorText = 'Table Not exist';
		            break;		        
		        default:
		            $errorText = 'other errors';
		            break;
		    }

		     if($errorCode != 0){
		    	$this->session->set_flashdata('error_message', ''.$errorText.'');
		    	return;
		    }
		}

		public function check_user_permission($user_id, $permission){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM role_permission WHERE role_type_id IN (SELECT role_type_id FROM staff_role WHERE staff_id='.$this->db->escape($user_id).') AND permission_id = (SELECT permission_id FROM permission WHERE description = '.$this->db->escape($permission).')');
			$count = $rs->row_array()['count'];
			return $count;
		}

		//The function is used in students to get the id of current academic year for registration of student
		public function select_current_academic_year_by_level($level){
			$status = 'current_academic_year';	
			$this->db->select('*');
			$this->db->from('academic_year');
			$this->db->where('status', $status);
			$this->db->where('class_level', $level);
			$this->db->limit('1');

			$query = $this->db->get();
			return $query->row_array();
		}

		public function activity_log($user_id, $activity, $tableName){
			$time = date("Y-m-d h:i:sa");
			$ip_address = $_SERVER['REMOTE_ADDR'];

			$activity_log_record = array(
				'activity' => $activity,
				'tableName' => $tableName,
				'time' => $time,
				'source' => $ip_address,
				'destination' => '',
				'user_id' => $user_id

			);

			$this->db->insert('activity_log', $activity_log_record);
			
			//$this->CI_mySQL('activity_log', $activity_log_record, NULL, 'INSERT');
		}

		/*public function check_if_terms_are_set(){
			$rs = $this->db->query('SELECT COUNT(*) AS count FROM term WHERE begin_date <= (SELECT current_date()) AND end_date >= (SELECT current_date()) AND is_current="yes"');

			return $rs->row_array()['count'];
		}*/		
	}
?>