<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Sample{
		public $var;
		protected $CI;

		public function __construct(){
			$this->CI =& get_instance();
		}

		public function show_msg($var){
			echo $var;
		}

		public function add_numbers($x, $y){
			return $x + $y;
		}

		public function kiabway(){
			$this->CI->load->helper('form');


		}
	}