
  <div class="white-area-content">
      <div class="db-header clearfix">

  <div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
  </div>
      <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

          <?php echo form_open('subjects/topics/'.$class_stream_id.'/'.$subject_id); ?>


                <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_topic" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                  <?php echo form_close(); ?>
  </div>
<?php if($this->session->userdata('manage_topics') == 'ok'): ?>
     <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#topicModal" >New Topic</a>
<?php endif; ?>

      </div>


  </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
           <tr class="table-header"> <!-- <th>ID#</th> -->
            <td style="text-align:center;">Topic Name</td>
            <?php if($this->session->userdata('view_subtopics') == 'ok'): ?>
              <td style="text-align:center;">Sub-Topics</td>
            <?php endif; ?>
            <?php if($this->session->userdata('manage_topics') == 'ok'): ?>
              <td style="text-align:center;">Edit Topic</td>
              <td style="text-align:center;">Delete Topic</td>
            <?php endif; ?>
          </tr>
        </thead>
        <tbody>
        <?php if ($topics == FALSE): ?>
            <tr>
              <td colspan="4">
                        <?php
                            $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Topics for this subject";
                            echo $message;
                        ?>
                    </td>
            </tr>
        <?php else: ?>
          <?php foreach($topics as $topic): ?>
            <tr>
              <!-- <td><?php echo $topic['topic_id']; ?></td> -->
              <td><?php echo $topic['topic_name']; ?></td>
              <?php if($this->session->userdata('view_subtopics') == 'ok'): ?>
                <td style="text-align:center;">
                  <a href="<?php echo base_url() . 'subjects/subtopics/' . $topic['class_stream_id'] . '/' . $topic['topic_id']; ?>" data-toggle="tooltip" data-placement="bottom" title="View SubTopics" >Subtopics</a> 
                </td>
              <?php endif; ?>
              <?php if($this->session->userdata('manage_topics') == 'ok'): ?>
                <td style="text-align:center;">
                  <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-placement="bottom" title="Edit Topic" data-target="#editTopicModal<?php echo $topic['topic_id']; ?>" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></i></a> 
                </td>
                <td style="text-align:center;">
                  <?php echo form_open('subjects/delete_topic/'.$topic['topic_id']); ?>
                    <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                    <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                      <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Delete Topic" value="Delete" onClick="return confirm('Are you sure you want to delete this topic?');" /><span class="glyphicon glyphicon-trash"></span></button>
                  <?php echo form_close(); ?>
                </td>
              <?php endif; ?>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
      </table>
      <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
    </div>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'admin/students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

<!--START ADD TOPIC-->

<div class="modal fade" id="topicModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Close</span>
              </button>
              <h4 class="modal-title" id="myModalLabel"><?php echo "Add Topic to: ". $subject_name; ?></h4>
          </div>
          <?php echo form_open('subjects/add_new_topic'); ?>
          <!-- Modal Body -->
          <div class="modal-body">
            <div class="form-group">
                  <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                  <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />  
                  <label >Topic Name: </label>
                  <input type="text" name="topic_name" class="form-control" required/>
            </div>                   
          </div>
           <!-- Modal Footer -->
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
              <button type="submit" class="btn btn-primary">ADD TOPIC</button>
          </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


<!-- START EDIT TOPIC -->
<?php foreach ($topics as $topic): ?>

  <div class="modal fade" id="editTopicModal<?php echo $topic['topic_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit: " . $topic['topic_name']; ?></h4>
            </div>
            <?php echo form_open('subjects/update_topic/'.$topic['topic_id']); ?>
            <!-- Modal Body -->
            <div class="modal-body"> 
              <!-- THE HIDDEN TYPES ARE FOR REDIRECTION IN CONTROLLER -->
                <div class="form-group">
                     <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                     <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                   <label>Topic Name: </label>
                     <input type="text" class="form-control" name="topic_name" value="<?php echo $topic['topic_name']; ?>" required/>                   
                </div>
            </div>
             <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">UPDATE TOPIC</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!-- END EDIT TOPIC -->