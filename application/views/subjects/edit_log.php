
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open("subjects/update_start_and_topic_in_log/".$log_record['log_id'], $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="start_date">Start Date :</label>
			<div class="col-sm-10">
				<input value="<?php echo $log_record['start_date']; ?>" type="date" name="start_date" class="form-control" id="start_date" required />
				<div class="error_message_color">
					<?php echo form_error('start_date'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="description">Topic :</label>
			<div class="col-sm-10">
				<select name="topic_id" class="form-control" required>
					<?php foreach($topics as $topic): ?>
						<?php if($log_record['class_stream_id'] === $topic['class_stream_id'] && $log_record['subject_id'] === $topic['subject_id']): ?>
							<option <?php if($topic['topic_id'] == $log_record['topic_id']){ echo "selected='true'"; } ?> value="<?php echo $topic['topic_id']; ?>"><?php echo $topic['topic_name']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>		
				</select>
				<div class="error_message_color">
					<?php echo form_error('topic_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="teacher_id" value="<?php echo $log_record['teacher_id']; ?>" />
			<input type="hidden" name="old_start_date" value="<?php echo $log_record['start_date']; ?>" />
			<input type="hidden" name="end_date" value="<?php echo $log_record['end_date']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_log" value="Update Log" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>