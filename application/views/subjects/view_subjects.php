
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
<?php echo form_open('subjects'); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_subject" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
<?php echo form_close(); ?>

</div>
<?php if($this->session->userdata('manage_subjects') == 'ok'): ?>
    <a href="<?php echo base_url() . 'subjects/add_new_subject'; ?>" class="btn btn-primary btn-sm">Add Subject</a>
<?php endif; ?>
</div>

</div>



<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<!-- <th width="20%">class Name#</th>
			<th width="10%" >Stream</th> -->
			<td>Subject</td>
			<td>Department</td>
			<td>Category</td>
			<td>Choice</td> 
			<td>Description</td>
            <?php if($this->session->userdata('manage_subjects') == 'ok' || $this->session->userdata('view_subject_teachers') == 'ok'): ?>
			 <td align="center">Action</td>
            <?php endif; ?>
			<!-- <th width="5%">Edit</th>
			<th width="5%">Delete</th> -->
		</tr>
	</thead>
	<tbody>
	<?php if ($subjects == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Subjects";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php
			foreach ($subjects as $key => $subject):
		?>
			<tr>
				<td><?php echo $subject['subject_name']; ?></td>
				<td><?php echo $subject['dept_name']; ?></td>
				<td>
					<?php 
						if($subject['subject_category'] === "N"){
							echo "Non Technical";
						}
						else{
							echo "Technical";
						}
					?>
				 </td>
				<td><?php echo $subject['subject_choice']; ?></td>
				<td><?php echo $subject['subject_type']; ?></td>
                <?php if($this->session->userdata('manage_subjects') == 'ok' || $this->session->userdata('view_subject_teachers') == 'ok'): ?>
    				<td align="center">
    					<!-- <a href="<?php echo base_url(); ?>subjects/subject/<?php echo $subject['subject_id']; ?>" class="btn btn-xs btn-eye">View</a>-->&nbsp;
    					<?php if($this->session->userdata('manage_subjects') == 'ok'): ?>
                            <a href="<?php echo base_url(); ?>subjects/edit_subject/<?php echo $subject['subject_id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
        					<a href="<?php echo base_url(); ?>subjects/delete_subject/<?php echo $subject['subject_id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to delete this subject?');"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
    					<?php endif; ?>
                        <?php if($this->session->userdata('view_subject_teachers') == 'ok'): ?>
                            <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#subjectTeacher<?php echo $subject['subject_id']; ?>" title="Available Teachers" data-placement="bottom"><span class="fa fa-eye"></span></a>
    				    <?php endif; ?>
                    </td>
                <?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
	<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'subjects/subjects'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>



<!-- START SUBJECT TEACHER MODAL -->
<?php foreach ($subjects as $key => $subject): ?>
<div class="modal fade" id="subjectTeacher<?php echo $subject['subject_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $subject['subject_name'] . " Teachers"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              <ol>
                <?php foreach($subject_teachers_record as $row): ?>
                    <?php if($subject['subject_id'] === $row['subject_id']): ?>
                    	<li>
                    		<?php 
                    			echo $row['firstname'] . " " . $row['middlename'] . " " . $row['lastname'];
                    		?>
                    	</li>
                	<?php endif; ?>
                <?php endforeach; ?>
              </ol>                               
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<!--END SUBJECT TEACHER MODAL -->
