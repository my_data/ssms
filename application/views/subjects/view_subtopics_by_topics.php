
  <div class="white-area-content">
      <div class="db-header clearfix">

  <div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
  </div>
      <div class="db-header-extra form-inline text-right"> 
  <div class="form-group has-feedback no-margin">

          <?php echo form_open('subjects/subtopics/'.$class_stream_id.'/'.$topic_id); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_subtopic" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                  <?php echo form_close(); ?>
  </div>
  <?php if($this->session->userdata('manage_subtopics') == 'ok'): ?>
   <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#subtopicModal" >New SubTopic</a> 
  <?php endif; ?>

      </div>

 
  </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="error_message_color">
  <?php echo $this->session->flashdata('errors'); ?>
</div>

<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <!-- <th>ID#</th> -->
      <td style="text-align:center;">Sub-Topic Name</td>
      <?php if($this->session->userdata('manage_subtopics') == 'ok'): ?>
        <td style="text-align:center;">Edit Subtopic</td>
        <td style="text-align:center;">Delete Subtopic</td>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
    <?php if ($subtopics == FALSE): ?>
            <tr>
              <td colspan="3">
                        <?php
                            $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Subtopics for this topic";
                            echo $message;
                        ?>
                    </td>
            </tr>
        <?php else: ?>
    <?php foreach($subtopics as $subtopic): ?>
      <tr>
        <!-- <td><?php echo $topic['subtopic_id']; ?></td> -->
        <td><?php echo $subtopic['subtopic_name']; ?></td>
        <?php if($this->session->userdata('manage_subtopics') == 'ok'): ?>
          <td style="text-align:center;">
            <a href="" class="btn btn-primary btn-xs glyphicon glyphicon-pencil" data-toggle="modal" data-target="#editSubTopicModal<?php echo $subtopic['subtopic_id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit SubTopic"></a> 
          </td>
          <td style="text-align:center;">
              <?php echo form_open('subjects/delete_subtopic/'.$subtopic['subtopic_id']); ?>
                  <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                  <input type="hidden" name="topic_id" value="<?php echo $subtopic['topic_id']; ?>" />
                  <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                  <button type="submit" class="btn btn-danger btn-xs" value="Delete" onClick="return confirm('Are you sure you want to delete this subtopic?');" data-toggle="tooltip" data-placement="bottom" title="Delete SubTopic"/><span class="glyphicon glyphicon-trash"></span></button>
              <?php echo form_close(); ?>
          </td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'admin/students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

<!--START ADD SUBTOPIC-->

<div class="modal fade" id="subtopicModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo "Add SubTopic to: " . $topic_name; ?></h4>
        </div>
        <?php echo form_open('subjects/add_new_subtopic'); ?>
        <!-- Modal Body -->
        <div class="modal-body">
            <div class="form-group">
                <input type="hidden" name="topic_id" value="<?php echo $topic_id; ?>" />
                <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />  
                <label>SubTopic Name:</label> 
                <input class="form-control" type="text" name="subtopic_name" />
            </div>                   
        </div>
         <!-- Modal Footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
            <button type="submit" class="btn btn-primary">ADD SUBTOPIC</button>
        </div>
        <?php echo form_close(); ?>
    </div>
    </div>
</div>
<!--END ADD SUBTOPIC-->


<!-- START EDIT SUBTOPIC -->
<?php foreach ($subtopics as $subtopic): ?>

    <div class="modal fade" id="editSubTopicModal<?php echo $subtopic['subtopic_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit: " . $subtopic['subtopic_name']; ?></h4>
            </div>
            <?php echo form_open('subjects/update_subtopic/'.$subtopic['subtopic_id']); ?>
            <!-- Modal Body -->
            <div class="modal-body"> 
                <!-- THE HIDDEN TYPES ARE FOR REDIRECTION IN CONTROLLER -->
                <div class="form-group">
                    <input type="hidden" name="topic_id" value="<?php echo $subtopic['topic_id']; ?>" />
                    <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
                    <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                    <label>SubTopic Name:</label> 
                    <input class="form-control" type="text" name="subtopic_name" value="<?php echo $subtopic['subtopic_name']; ?>" />                   
                </div>
            </div>
             <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">UPDATE SUBTOPIC</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!-- END EDIT TOPIC -->