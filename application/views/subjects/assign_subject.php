
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('subjects/assign_subject_to_class', $attributes); ?>
		<div class="form-group">
			<label for="class_name">Class Name</label>
			<select name="class_id" class="form-control" id="class_id"  onchange="concatenate_x('class_id', 'stream_id_name');" >
				<option value="">--Choose Class--</option>
				<?php foreach($classes as $class): ?>
					<option <?php echo set_select('class_id', $class['class_id'], $this->input->post('class_id')); ?> value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('class_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="stream">Stream</label>
			<select name="stream_id" class="form-control" id="stream_id"  onchange="concatenate_x('class_id', 'stream_id');" >
				<option value="">--Choose Stream--</option>
				<?php foreach($streams as $stream): ?>
					<option <?php //echo set_select('stream_id', $stream['stream_name'], $this->input->post('stream_id')); ?> value="<?php echo $stream['stream_name']; ?>"><?php echo $stream['stream_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('class_stream_id'); ?>
			</div>
		</div>
		<input type="hidden" name="class_stream_id" id="concatenate_csid" value="" />
		<div class="form-group">
			<label for="subject">Subject</label>
			<select name="subject_id[]" class="form-control chosen-select" multiple id="level" >
				<?php foreach($subjects as $subject): ?>
					<option <?php echo set_select('subject_id[]', $subject['subject_id'], $this->input->post('subject_id[]')); ?> value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_id[]'); ?>
			</div>
		</div>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="assign_subject" value="Assign Subjects" />
		</div>
	<?php echo form_close(); ?>
</div>