
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
 <div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">

<?php echo form_open('subjects/class_stream_subjects'); ?>
<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_class" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
<?php echo form_close(); ?>
</div>
<?php if($this->session->userdata('manage_class_subjects') == 'ok'): ?>
	<a href="<?php echo base_url() . 'subjects/assign_subject_to_class'; ?>" class="btn btn-primary btn-sm">Assign Subject To Class</a>
<?php endif; ?>
</div>

</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td width="15%">Class</td>
			<td width="75%">Subjects</td>
			<?php if($this->session->userdata('manage_class_subjects') == 'ok'): ?>
				<td width="10%" align="center">Action</td>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($class_stream_subjects == FALSE): ?>
        <tr>
          <td colspan="3">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Class Stream Subjects";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php
			foreach ($class_stream_subjects as $key => $class_stream_subject):
		?>
			<tr>
				<td><?php echo $class_stream_subject['class_name'] . " " . $class_stream_subject['stream']; ?></td>
				<td contenteditable="false"><?php echo $class_stream_subject['class_subjects']; ?></td>	
				<?php if($this->session->userdata('manage_class_subjects') == 'ok'): ?>
					<td>			
						<!-- <a href="<?php echo base_url(); ?>subjects/subject/<?php echo $subject['subject_id']; ?>" class="btn btn-xs btn-eye">View</a>-->&nbsp;
						<a href="<?php echo base_url(); ?>subjects/edit_assigned_subject/<?php echo $class_stream_subject['class_stream_id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
						<a href="<?php echo base_url(); ?>subjects/delete_assigned_subject/<?php echo $class_stream_subject['class_stream_id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to delete this class stream?');"><span class="glyphicon glyphicon-trash"></span></a>
					</td>
				<?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
	<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'subjects/class_streams'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>