
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('subjects/add_new_subject', $attributes); ?>
		<div class="form-group">
			<label for="subject_id">Subject ID</label>
				<!-- <div id="successMessage" class="success_message_color">
					<?php 
						if($this->session->flashdata('success_message')){
							echo $this->session->flashdata('success_message');
						}
					?>
				</div>
				<div id="errorMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('error_message')){
							echo $this->session->flashdata('error_message');
						}
					?>
				</div>
				<div id="warningMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('exist_message')){
							echo $this->session->flashdata('exist_message');
						}
					?>
				</div> -->
				<input value="<?php echo set_value('subject_id'); ?>" type="text" class="form-control" name="subject_id" placeholder="SUB01" />
				<div class="error_message_color">
					<?php echo form_error('subject_id'); ?>
				</div>
		</div>
		<div class="form-group">
			<label for="subject_name">Subject</label>
			<input value="<?php echo set_value('subject_name'); ?>" type="text" class="form-control" name="subject_name" placeholder="Basic Mathematics" />
			<div class="error_message_color">
				<?php echo form_error('subject_name'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="department_id">Department</label>
			<select name="department_id" class="form-control" id="department_id" required="true">
				<option value="">--Choose--</option>
				<?php foreach($departments as $department): ?>
					<option <?php echo set_select('department_id', $department['dept_id'], $this->input->post('department_id')); ?> value="<?php echo $department['dept_id']; ?>"><?php echo $department['dept_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('department_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="subject_category">Category</label>
			<select name="subject_category" class="form-control" id="subject_category" required="true">
				<option value="">--Choose--</option>
				<option <?php echo set_select('subject_category', "T", $this->input->post('subject_category')); ?> value="T">Technical</option>
				<option <?php echo set_select('subject_category', "N", $this->input->post('subject_category')); ?> value="N">Non Technical</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_category'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="subject_choice">Choice</label>
			<select name="subject_choice" class="form-control" id="subject_choice" required="true">
				<option value="">--Choose--</option>
				<option <?php echo set_select('subject_choice', "optional", $this->input->post('subject_choice')); ?> value="optional">Optional</option>
				<option <?php echo set_select('subject_choice', "compulsory", $this->input->post('subject_choice')); ?> value="compulsory">Compulsory</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_choice'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="studied_by">Studied by</label>
			<select name="studied_by" class="form-control" id="studied_by" required="true">
				<option value="">--Choose--</option>
				<option<?php echo set_select('studied_by', "O", $this->input->post('studied_by')); ?> value="O">O'Level</option>
				<option<?php echo set_select('studied_by', "A", $this->input->post('studied_by')); ?> value="A">A'Level</option>
				<option<?php echo set_select('studied_by', "BOTH", $this->input->post('studied_by')); ?> value="BOTH">Both Levels</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('studied_by'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="subject_type">Subject Type</label>
			<select name="subject_type" class="form-control" id="subject_type" required >
				<option value="">--Choose--</option>
				<option value="Core">Core</option>
				<option value="Core_With_Penalty">Core With Penalty</option>
				<option value="Supplement">Supplement</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_type'); ?>	
			</div>
		</div>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_new_subject" value="Add Subject" />
		</div>
	<?php echo form_close(); ?>
</div>
