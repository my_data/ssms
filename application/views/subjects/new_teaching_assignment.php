
<h3 align="center"><?php echo $title; ?></h3><hr />

<?php echo validation_errors(); ?>

<?php echo form_open(''.base_url() .'subjects/teaching_assignment'); ?>
<table class="table table-striped table-hover table-condensed">
	<tr>
		<td><label for="subject">Subject Name: </label></td>
		<td><input type="text" name="subid" /></td>
	</tr>
	<tr>
		<td><label for="stream">Stream: </label></td>
		<td><input type="text" name="stream_id" /></td>
	</tr>
	<tr>
		<td><label for="teacher">Teacher: </label></td>
		<td><input type="text" name="tid" /></td>
	</tr>
	<tr>
		<td><label for="start_date">Start Date: </label></td>
		<td><input type="date" name="start_date" /></td>
	</tr>
	<tr>
		<td><label for="end_date">End Date: </label></td>
		<td><input type="date" name="end_date" /><br /></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="add_assignment" value="Assign" class="btn btn-info" /></td>
	</tr>
<?php echo form_close(); ?>
</table>