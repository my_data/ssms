
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('subjects/edit_assigned_subject/'.$class_stream_id, $attributes); ?>
		<div class="form-group">
			<label for="class_name">Class Name</label>
			<input type="text" name="class_name" class="form-control" id="class_name" value="<?php echo $class_stream_subject['class_name']; ?>" readonly />
		</div>
		<div class="form-group">
			<label for="stream">Stream</label>
			<input type="text" name="stream_name" class="form-control" id="stream_name" value="<?php echo $class_stream_subject['stream']; ?>" readonly />
		</div>
		<div class="form-group">
			<label for="subject">Subject</label>
			<select name="subject_id[]" class="form-control chosen-select" multiple id="level" >
				<?php foreach($subjects as $subject): ?>
					<option 
						<?php
							//Excellent JOB
							foreach ($class_stream_subjects as $css) {
								if($css['subject_name'] === $subject['subject_name']){
									echo "selected='true'";
								}
							}
						?>
					value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_id[]'); ?>
			</div>
		</div>
		<div class="form-group">
		<input type="hidden" name="class_stream_id" value="<?php echo $class_stream_subject['class_stream_id'];; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_assigned_subject" value="Update" />
		</div>
	<?php echo form_close(); ?>
</div>