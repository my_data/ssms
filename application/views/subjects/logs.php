

  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 
                <?php if($teacher_id): ?>
                    <?php echo form_open('mylogs/'.$teacher_id); ?>
                <?php endif; ?>
                <?php if($dept_id): ?>
                    <?php echo form_open('subject_teaching_logs/'.$dept_id); ?>
                <?php endif; ?>
<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_log" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                        <?php echo form_close(); ?>
     
            </div>

          </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<!-- <table id="teacher_data" class="table table-striped table-hover table-condensed table-bordered"> -->
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
            <td>Class</td>
            <td>Subject</td>
			<td>Topic Name</td>
			<td>Subtopics</td>
			<td>Start date</td>
			<td>End date</td>
      <td>Teacher Names</td>
      <td>Comment</td>
        <?php if((($dept_id && $this->session->userdata('dept')['dept_id'] === $dept_id) || $teacher_id) && ($this->session->userdata('manage_teaching_log') == 'ok' || $this->session->userdata('view_teaching_log_in_own_department') == 'ok')): ?>
      <td>Action</td>
        <?php endif; ?>
		</tr>
	</thead>
	<tbody>
  <?php if ($logs == FALSE): ?>
        <tr>
          <td colspan="10">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Logs";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
      <?php $x = 1; ?>
		<?php foreach($logs as $log): ?>
			<tr>
				<td><?php echo $x+$p; ?></td>
        <td><?php echo $log['class_name'] . " " . $log['stream']; ?></td>
        <td><?php echo $log['subject_name']; ?></td>
        <td><?php echo $log['topic_name']; ?></td>
        <td align="center"><a href="#" data-toggle="modal" data-target="#viewSubtopics<?php echo $log['topic_id']; ?>" class="btn btn-xs btn-primary" data-placement="bottom" title="View Subtopics"><span class="fa fa-eye"></span></a></td>
        <td><?php echo $log['start_date']; ?></td>
        <td><?php echo $log['end_date']; ?></td>
				<td><?php echo $log['firstname'] . " " . $log['lastname']; ?></td>
        <td align="center"><a href="#" data-toggle="modal" data-target="#viewComment<?php echo $log['log_id']; ?>" class="btn btn-xs btn-primary" data-placement="bottom" title="View Comment">C</a></td>
		<?php if((($dept_id && $this->session->userdata('dept')['dept_id'] === $dept_id) || $teacher_id) && ($this->session->userdata('manage_teaching_log') == 'ok' || $this->session->userdata('view_teaching_log_in_own_department') == 'ok')): //Admin and other user are not allowed to view this?>
        <td align="center">
        <?php if($teacher_id && $this->session->userdata('manage_teaching_log') == 'ok'): //Start if teacher_id?>
            <?php if($log['approved'] == "no"): ?>
                <a href="#" class="btn btn-primary btn-xs" data-target="#addComment<?php echo $log['log_id']; ?>" data-toggle="modal" data-placement="bottom" title="Add / Edit Comment"><span class="fa fa-plus"></span></a>&nbsp;
            <?php else: ?>
                <button class="btn btn-primary btn-xs" disabled title="The log has already been approved" data-placement="bottom"><span class="fa fa-plus"></span></button>&nbsp;
            <?php endif; ?>
            <?php if($log['approved'] == "no"): ?>
                <a href="<?php echo base_url() . 'subjects/edit_log/'.$log['log_id'].''; ?>" class="btn btn-primary btn-xs" data-placement="bottom" title="Edit Start date and Topic" ><span class="fa fa-pencil"></span></a>&nbsp;
            <?php else: ?>
                <button class="btn btn-primary btn-xs" disabled title="This log has already been approved" data-placement="bottom"><span class="fa fa-pencil"></span></button>&nbsp;
            <?php endif; ?>
        <?php endif; //End if teacher_id ?>
        <?php if(($dept_id && $this->session->userdata('dept')['dept_id'] === $dept_id) && $this->session->userdata('view_teaching_log_in_own_department') == 'ok'): ?>
            <?php if($log['approved'] == "no"): ?>
                <a href="<?php echo base_url() . 'subjects/approve/'.$log['log_id'].'/'.$dept_id.''; ?>" onClick="return confirm('Are you sure you want to approve this log?');" class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="Approve">A</a>
            <?php else: ?>
                <button class="btn btn-primary btn-xs" disabled title="This log has already been approved" data-placement="bottom"><span>A</span></button>&nbsp;
            <?php endif; ?>
        <?php endif; ?>
        </td>
        <?php endif; //End IF of not allowed to view this?> 
			</tr>
      <?php $x++; ?>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>

<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'admin/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>






<!-- START MODAL ADD COMMENT -->
<?php $x = 1; // Kwa ajili ya Modal ?>
<?php foreach($logs as $log): ?>
<div class="modal fade" id="addComment<?php echo $log['log_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Topic End Date:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("subjects/add_log_comment/".$log['log_id'], $attributes);
                ?>        
                <input type="hidden" name="staff_id" value="<?php echo $log['teacher_id']; ?>" />
                <input type="hidden" name="start_date" value="<?php echo $log['start_date']; ?>" />
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="topic" >Comment</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" name="comment" cols="42" rows="5"><?php echo $log['comment']; ?></textarea>
                      </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="end_date" >End Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="end_date" value="<?php echo $log['end_date']; ?>" class="form-control" required />
                    </div>
                </div> 
                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label" for="end_date" >End Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="end_date" class="form-control" />
                    </div>
                </div>  -->                      
            </div>            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>" />
                <input type="hidden" name="subject_id" value="<?php echo $log['subject_id']; ?>" />
                <input type="hidden" name="class_stream_id" value="<?php echo $log['class_stream_id']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">SAVE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php $x++; //Kwa ajili ya modal ?>
<?php endforeach; ?>
<!-- END MODAL COMMENT -->

<!--START MODAL OF VIEW SUBTOPICS -->
<?php foreach($logs as $log): ?>
<div class="modal fade" id="viewSubtopics<?php echo $log['topic_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "SUBTOPICS:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              <ol>
                <?php foreach($subtopics as $st): ?>
                  <?php if($st['topic_id'] == $log['topic_id']): ?>
                    <li><?php echo $st['subtopic_name']; ?></li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ol>                               
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<!--END MODAL OF VIEW SUBTOPICS -->

<!--START MODAL OF VIEW COMMENT -->
<?php foreach($logs as $log): ?>
<div class="modal fade" id="viewComment<?php echo $log['log_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Comment:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              <ul>                    
                <?php foreach($logs as $lg): ?>
                    <?php if($lg['log_id'] == $log['log_id']): ?>
                        <?php if($lg['comment'] == ""): ?>
                            <li>No comment to display</li>
                        <?php else: ?>
                            <li><?php echo $lg['comment']; ?></li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>            
              </ul>                               
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<!--END MODAL OF VIEW COMMENT -->




<!-- START MODAL EDIT LOG -->

<?php $x = 1; // Kwa ajili ya Modal ?>
<?php foreach($logs as $log): ?>
<div class="modal fade" id="editLog<?php echo $log['class_stream_id'] .'-'. $log['subject_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit Log:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">  
                <?php 
    /*echo "<pre>";
                print_r($all_logs);
            echo "</pre>";*/
?>  
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("subjects/update_start_and_topic_in_log/".$log['log_id'], $attributes);
                ?>        
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="topic" >Topic Name</label>
                    <div class="col-sm-8">                     
                        <select name="">
                            <?php foreach($topics as $topic): ?>
                                <option <?php if($topic['topic_id'] === $log['topic_id']){ echo "selected='true'"; } ?>><?php echo $topic['topic_name']; ?></option>
                            <?php endforeach; ?>
                        </select>   
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="start_date" >Start Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="start_date" value="<?php echo $log['start_date']; ?>" class="form-control" required />
                    </div>
                </div> 
                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label" for="end_date" >End Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="end_date" class="form-control" />
                    </div>
                </div>  -->                      
            </div>            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">UPDATE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php $x++; //Kwa ajili ya modal ?>
<?php endforeach; ?>
<!-- END MODAL EDIT LOG -->