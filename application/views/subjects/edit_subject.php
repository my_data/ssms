
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-pencil-square"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<hr>
	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('subjects/edit_subject/'.$subject_id, $attributes); ?>
		<div class="form-group">
			<label for="subject_name">Subject</label>
			<input type="text" class="form-control" name="subject_name" value="<?php echo $subject['subject_name']; ?>" id="subject_name" />
			<div class="error_message_color">
				<?php echo form_error('subject_name'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="department_id">Department</label>
			<select name="department_id" class="form-control" id="department_id">
				<?php foreach($departments as $department): ?>
					<option <?php if($subject['dept_id'] === $department['dept_id']){ echo "selected='true'"; } ?> value="<?php echo $department['dept_id']; ?>"><?php echo $department['dept_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('department_id'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="subject_category">Category</label>
			<select name="subject_category" class="form-control" id="subject_category">
				<option <?php if($subject['subject_category'] === "T"){ echo "selected='true'"; } ?> value="T">Technical</option>
				<option <?php if($subject['subject_category'] === "N"){ echo "selected='true'"; } ?> value="N">Non Technical</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_category'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="subject_choice">Choice</label>
			<select name="subject_choice" class="form-control" id="subject_choice">
				<option <?php if($subject['subject_choice'] === "optional"){ echo "selected='true'"; } ?> value="optional">Optional</option>
				<option <?php if($subject['subject_choice'] === "compulsory"){ echo "selected='true'"; } ?> value="compulsory">Compulsory</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_choice'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="studied_by">Studied by</label>
			<select name="studied_by" class="form-control" id="studied_by" required="true">
				<option <?php if($subject['studied_by'] === "O"){ echo "selected='true'"; } ?> value="O">O'Level</option>
				<option <?php if($subject['studied_by'] === "A"){ echo "selected='true'"; } ?> value="A">A'Level</option>
				<option <?php if($subject['studied_by'] === "BOTH"){ echo "selected='true'"; } ?> value="BOTH">Both Levels</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('studied_by'); ?>
			</div>
		</div>
		<div class="form_group">
			<label for="subject_type">Subject Type</label>
			<select name="subject_type" class="form-control" id="subject_type" required >
				<option <?php if($subject['subject_type'] == "Core"){ echo "selected='true'"; } ?> value="Core">Core</option>
				<option <?php if($subject['subject_type'] == "Core_With_Penalty"){ echo "selected='true'"; } ?> value="Core_With_Penalty">Core With Penalty</option>
				<option <?php if($subject['subject_type'] == "Supplement"){ echo "selected='true'"; } ?> value="Supplement">Supplement</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('subject_type'); ?>
			</div>
		</div><br/>
		<div class="form-group">
			<input type="hidden" name="subject_id" value="<?php echo $subject['subject_id']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_subject" value="Update" onClick="return confirm('Save Changes?');" />
		</div>
	<?php echo form_close(); ?>
</div>
