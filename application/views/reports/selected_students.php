<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>STATUS</td>
    <?php foreach($count_reported as $cr): ?>
      <td>
      <?php
        $status = ($cr['reported'] == "yes") ? "REPORTED" : "UNREPORTED";
        echo $status;
       ?>
      </td>
    <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>    
    <td>NUMBER OF STUDENTS</td>
      <?php foreach($count_reported as $cr): ?>
    <td><?php echo $cr['nos']; ?></td>
      <?php endforeach; ?>
    </tr>
  </tbody>
</table>

<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr>
      <th>S/No.</th>
      <th>Examination#</th>
      <th>Student Names#</th>
      <th>Form</th>
      <th>Reported</th>
    </tr>
  </thead>
  <tbody>
      <?php if ($students == FALSE): ?>
          <tr>
            <td colspan="5">
                      <?php
                          $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students";
                          echo $message;
                      ?>
                  </td>
          </tr>
      <?php else: ?>
        <?php $x = 1; ?>
      <?php foreach($students as $student): ?>
        <tr>
          <td><?php echo $x."."; ?></td>
          <td><?php echo $student['examination_no']; ?></td>
          <td><?php echo $student['student_names']; ?></td>
          <td><?php echo $student['form']; ?></td>
          <td><?php echo $student['reported']; ?></td>
        </tr>
        <?php $x++; ?>
      <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
