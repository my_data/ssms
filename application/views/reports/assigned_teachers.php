
	<table class="table table-responsive table-condensed table-bordered">

		<thead class="table-header">
			<tr>
				<td>No.</td>
				<td>Names</td>
				<td>Class</td>
				<td>Subject</td>
				<!--<td>Available</td>
				<td>Domitory Teacher</td>-->
			</tr>
		</thead>
		<tbody>
			<?php if($records == FALSE): ?>
				<tr>
			    	<td colspan="4">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No assigned teachers this term";
	                        echo $message;
	                    ?>
	                </td>
			    </tr>
			<?php else: ?>
				<?php $x = 1; ?>
				<?php foreach ($records as $key => $row): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $row['names']; ?></td>
				<td><?php echo $row['class_name']. " " . $row['stream']; ?></td>
				<td><?php echo $row['subject_name']; ?></td>	
				<!--<td><?php echo $data[$key]['no_of_students']; ?></td>			
				<td><?php echo $row['names']; ?></td>-->
				<?php $x++; ?>
			</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>

	</table>

</div>
<!-- END PRINT -->
