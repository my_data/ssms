<table class="table table-responsive table-condensed table-bordered">

	<thead>
		<tr class="table-header">
			<td>Class Name</td>
			<td>Asset Name</td>
			<td>Quantity</td>
		</tr>
	</thead>
	<tbody>
		<?php $value = ""; ?>
		<?php foreach($asset_count as $ac): ?>
			<tr>
				<td><?php if($ac['class_name'] != $value){ echo $ac['class_name']; } ?></td>
				<td><?php echo $ac['asset_name']; ?></td>
				<td><?php echo $ac['no_of_assets']; ?></td>
			</tr>
			<?php $value = $ac['class_name']; ?>
		<?php endforeach; ?>
	</tbody>

</table>
<br/><br/>
<table class="table table-responsive table-condensed table-bordered">

	<thead>
		<tr class="table-header">
			<td>Status</td>
			<td>Quantity</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($asset_status as $as): ?>
			<tr>
				<td><?php echo $as['status']; ?></td>
				<td><?php echo $as['noa']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
<div class="break" >
</div>

<u><?php echo $assets_title; ?></u><br/><br/>
	<table class="table table-responsive table-condensed">

		<thead>
			<tr class="table-header">
				<td>S/No.</td>
				<td>Asset Name</td>
				<td>TAG ID</td>
				<td>Status</td>
				<td>Description</td>
			</tr>
		</thead>
		<tbody>
		<?php if($assets == FALSE): ?>
			<td rowspan="6">
				<?php
					$message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "The are currently no assets in this dormitory";
					echo $message;
				?>
			</td>
		<?php else: ?>
			<?php if($assets): $x = 1; ?>

				<?php foreach ($assets as $row): ?>
					<tr>
						<td><?php echo $x."."; ?></td>
						<td><?php echo $row['asset_name']; ?></td>
						<td><?php echo $row['asset_no']; ?></td>
						<td><?php echo $row['status']; ?></td>
						<td><?php echo $row['description']; ?></td>					
					</tr>
				<?php $x++; endforeach; ?>
			<?php endif; ?>
		<?php endif; ?>
		</tbody>
	</table>
</div>