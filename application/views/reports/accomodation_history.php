<table class="table table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
			<td>Student Names</td>
			<td>Dormitory Name</td>
			<td>From</td>
			<td>TO</td>
			<td>Academic Year</td>
		</tr>
	</thead>
	<tbody>
    <?php if ($accomodation_data == FALSE): ?>
        <tr>
          <td colspan="6">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    	<?php $x = 1; ?>
		<?php foreach($accomodation_data as $ad): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $ad['student_names']; ?></td>				
				<td><?php echo $ad['dorm_name']; ?></td>
				<td><?php echo $ad['start_date']; ?></td>
				<td><?php if($ad['end_date'] == NULL){ echo "-"; }else{ echo $ad['end_date']; } ?></td>
				<td><?php echo $ad['year']; ?></td>				
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>