<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr class="table-header">
            <td>No.</td>
            <td>Staff Names</td>
            <td>Gender</td>
            <td>Phone#</td>
            <td>Role</td>
            <td>Status</td>
        </tr>
    </thead>
    <tbody>
    <?php if($staffs == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Staffs";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; ?>
    <?php foreach($staffs as $staff): ?>
        <tr>
            <td><?php echo $x."."; ?></td>
            <td><?php echo $staff['firstname'] . " " .$staff['middlename'] . " " . $staff['lastname']; ?></td>
            <td><?php echo $staff['gender']; ?></td>
            <td><?php echo $staff['phone_no']; ?></td>
            <td><?php echo $staff['user_role']; ?></td>
            <td><?php echo $staff['status']; ?></td>
        </tr>
        <?php $x++; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>