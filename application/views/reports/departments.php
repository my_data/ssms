<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr class="table-header">
            <td>No.</td>
            <td>Department Name</td>
            <td>Location</td>
            <td>HOD</td>
            <td>No Of Staffs</td>
        </tr>
    </thead>
    <tbody>
    <?php if($departments == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Departments";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; ?>
    <?php foreach($departments as $dept): ?>
        <tr>
            <td><?php echo $x."."; ?></td>
            <td><?php echo $dept['dept_name']; ?></td>
            <td><?php echo $dept['dept_loc']; ?></td>
            <td><?php echo $dept['firstname'] . " " . $dept['lastname']; ?></td>
            <td><?php echo $dept['no_of_staffs']; ?></td>
        </tr>
        <?php $x++; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>