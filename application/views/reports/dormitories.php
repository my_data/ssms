
	<table class="table table-responsive table-condensed table-bordered">

		<thead class="table-header">
			<tr>
				<td>No.</td>
				<td>Domitory Name</td>
				<td>Location</td>
				<td>Capacity</td>
				<td>Available</td>
				<td>Domitory Teacher</td>
			</tr>
		</thead>
		<tbody>
			<?php if($dormitories == FALSE): ?>
				<tr>
			    	<td colspan="4">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this Dormitory";
	                        echo $message;
	                    ?>
	                </td>
			    </tr>
			<?php else: ?>
				<?php $x = 1; ?>
				<?php foreach ($dormitories as $key => $row): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $row['dorm_name']; ?></td>
				<td><?php echo $row['location']; ?></td>
				<td><?php echo $row['capacity']; ?></td>	
				<td><?php echo $data[$key]['no_of_students']; ?></td>			
				<td><?php echo $row['names']; ?></td>
				<?php $x++; ?>
			</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>

	</table>

</div>
<!-- END PRINT -->
