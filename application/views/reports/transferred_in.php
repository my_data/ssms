
<table class="table table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>School From</td>
			<!-- <td>Status</td> -->
			<td>Class Name</td>
			<td>Date of transfer</td>
			<td>Transfer letter</td>
			<td>Self form receipt</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($students == FALSE): ?>
        <tr>
          <td colspan="9">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Transferred IN Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    	<?php $x = 1; ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['school']; ?></td>
				<!-- <td><?php echo $student['transfer_status']; ?></td> -->
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['date_of_transfer']; ?></td>
				<td><?php echo $student['transfer_letter']; ?></td>
				<td><?php echo $student['self_form_receipt']; ?></td>				
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
</table>

  </div>
</div>