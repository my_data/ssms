<table class="table table-responsive table-condensed table-bordered">

	<thead>
		<tr class="table-header">
			<td>Disability</td>
			<td>Number Of Students</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($count_by_disabilty as $cbd): ?>
			<tr>
				<td><?php echo $cbd['disability']; ?></td>
				<td><?php echo $cbd['nds']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
<br/><br/>
<u><?php echo $disabled_title; ?></u>
<br/><br/>
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
			<td>Admission#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Class</td>
			<td>Stream</td>
      		<td>Disability</td>
		</tr>
	</thead>
	<tbody>
    <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Disabled Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    	<?php $x = 1; ?>
		<?php foreach($students as $student): ?>			
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>
				<td><?php echo $student['disability']; ?></td>
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>