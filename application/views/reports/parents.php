<table class="table table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
			<td>Parent / Guardian Names</td>
			<td>Address</td>
			<td>Phone#</td>
			<!-- <td>Email</td> -->
			<td>Student Names</td>
			<td>Class Name</td>
		</tr>
	</thead>
	<tbody>
    <?php if ($parents == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Registered Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    	<?php $x = 1; ?>
		<?php foreach($parents as $pr): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $pr['g_firstname'] . " " . $pr['g_lastname']; ?></td>
				<td><?php if($pr['p_box'] != ""){ echo $pr['p_box'] . ", " . $pr['p_region']; } ?></td>				
				<td><?php echo $pr['phone_no']; ?></td>
				<!-- <td><?php echo $pr['email']; ?></td> -->
				<td><?php echo $pr['student_names']; ?></td>
				<td><?php echo $pr['class_name']; ?></td>				
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>