<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
	<tr>
		<td align="center">ADMISSION NO</td>
		<td align="center">FIRSTNAME</td>
		<td align="center">LASTNAME</td>
		<td align="center">MIDDLENAME</td>
		<td align="center">CLASS</td>
		<td align="center">DATE OF BIRTH</td>
		<td align="center">HOME ADDRESS</td>
	</tr>
	<tr>
		<td align="center"><?php echo $std_record['admission_no']; ?></td>
		<td align="center"><?php echo $std_record['firstname']; ?></td>
		<td align="center"><?php echo $std_record['lastname']; ?></td>
		<td align="center"></td>
		<td align="center"><?php echo $record['class_name']; ?></td>
		<td align="center"><?php echo $std_record['dob']; ?></td>
		<td align="center">P.O.Box <?php echo $std_record['home_address'] . ", " . $std_record['region']; ?></td>
	</tr>
</table>

<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
	<tr>
		<td>
			<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
				<tr>
					<td align="center" colspan="4">FIRST TERM</td>
				</tr>
				<tr>
					<td align="center">SUBJECT</td>
					<td align="center">MARKS</td>
					<td align="center">GRADE</td>
					<td align="center">SUBJECT RANK</td>
					<!-- <td align="center">CLASS</td>
					<td align="center">DATE OF BIRTH</td>
					<td align="center">HOME ADDRESS</td> -->
				</tr>
				<?php if($ft_record == 0): ?>
					<tr>
        				<td colspan="4">
		                  	<?php
		                      	$message = "There are currently No Records";
		                      	echo $message;
		                  	?>
				        </td>
				    </tr>
				<?php else: ?>
				<?php foreach($results_data as $row): ?>
					<?php if($row['term_name'] == 'first_term'): ?>
						<tr>
							<td align="center"><?php echo $row['subject_name']; ?></td>
							<td align="center"><?php echo $row['average']; ?></td>
							<td align="center"><?php echo $row['grade']; ?></td>
							<td align="center"><?php echo $row['subject_rank']; ?></td>
						</tr>						
					<?php endif; ?>
					<!-- <tr>
						<td align="center"><?php echo "Electrical Engineering Science"; ?></td>
						<td align="center"><?php  echo "90" ?></td>
						<td align="center"><?php  echo "A"; ?></td>
						<td align="center">1</td>
					</tr> -->
				<?php endforeach; ?>
					<tr>
						<td colspan="2" align="center">Overall Grade</td>
						<td colspan="1" align="center"><?php echo $ft_pos['average']; ?></td>
						<td colspan="1" align="center"><?php echo $ft_pos['grade']; ?></td>
					</tr>
				<?php endif; ?>
			</table>
		</td>
		<td>
		<td>
			<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
				<tr>
					<td align="center" colspan="4">SECOND TERM</td>
				</tr>
				<tr>
					<td align="center">SUBJECT</td>
					<td align="center">MARKS</td>
					<td align="center">GRADE</td>
					<td align="center">SUBJECT RANK</td>
					<!-- <td align="center">CLASS</td>
					<td align="center">DATE OF BIRTH</td>
					<td align="center">HOME ADDRESS</td> -->
				</tr>
				<?php if($st_record == 0): ?>
					<tr>
        				<td colspan="4">
		                  	<?php
		                      	$message = "There are currently No Records";
		                      	echo $message;
		                  	?>
				        </td>
				    </tr>
				<?php else: ?>
				<?php foreach($results_data as $row): ?>
					<?php if($row['term_name'] == 'second_term'): ?>
						<tr>
							<td align="center"><?php echo $row['subject_name']; ?></td>
							<td align="center"><?php echo $row['average']; ?></td>
							<td align="center"><?php echo $row['grade']; ?></td>
							<td align="center"><?php echo $row['subject_rank']; ?></td>
						</tr>
					<?php endif; ?>
					<!-- <tr>
						<td align="center"><?php echo "Electrical Engineering Science"; ?></td>
						<td align="center"><?php  echo "90" ?></td>
						<td align="center"><?php  echo "A"; ?></td>
						<td align="center">1</td>
					</tr> -->
				<?php endforeach; ?>
					<tr>
						<td colspan="2" align="center">Overall Grade</td>
						<td colspan="1" align="center"><?php echo $st_pos['average']; ?></td>
						<td colspan="1" align="center"><?php echo $st_pos['grade']; ?></td>
					</tr>
				<?php endif; ?>
			</table>
		</td>
	</tr>
</table>