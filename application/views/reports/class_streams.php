<table class="table table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Class Name</td>
    <?php foreach($class_streams_count as $csc): ?>
      <td><?php echo $csc['class_name']; ?></td>
    <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>    
    <td>No. of Streams</td>
      <?php foreach($class_streams_count as $csc): ?>
    <td><?php echo $csc['total_streams']; ?></td>
      <?php endforeach; ?>
    </tr>
  </tbody>
</table>

<table class="table table-condensed table-bordered">
	<thead>
		<tr>
			<th>S/No.</th>
			<th>Class Name#</th>
			<th>Stream</th>
			<th>Capacity</th>
			<th>Enrolled</th>
			<th>Description</th>
			<th>Class Teacher</th>
		</tr>
	</thead>
	<tbody>
    <?php if ($classes == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Classes";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php $x = 1; ?>
		<?php foreach($classes as $cs): ?>
          <tr>
            <td><?php echo $x."."; ?></td>
            <td><?php echo $cs['class_name']; ?></td>
            <td><?php echo $cs['stream']; ?></td>
            <td><?php echo $cs['capacity']; ?></td>
            <td><?php echo $cs['enrolled']; ?></td>
            <td><?php echo $cs['description']; ?></td>
            <td><?php echo $cs['teacher_names']; ?></td>
          </tr>
          <?php $x++; ?>
      <?php endforeach; ?>
    <?php endif; ?>
	</tbody>
</table>

</div>
<!-- END PRINT -->