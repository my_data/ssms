<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Class Name</td>
    <?php foreach($no_students_count as $nsc): ?>
      <td><?php echo $nsc['class_name']; ?></td>
    <?php endforeach; ?>
    <?php if(count($check) != 1): ?>
      <td>Total</td> 
    <?php endif; ?>
    </tr>
  </thead>
  <tbody>    
    <td>No. of Students</td>
      <?php foreach($no_students_count as $nsc): ?>
    <td><?php echo $nsc['nos']; ?></td>
      <?php endforeach; ?>
      <?php if(count($check) != 1): ?>
    <td><?php echo "<strong>" . $tnos . "</strong>"; ?></td>
      <?php endif; ?>
    </tr>
  </tbody>
</table>

<table class="table table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>S/No.</td>
			<td>Admission#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Class</td>
			<td>Stream</td>
		</tr>
	</thead>
	<tbody>
    <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Registered Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    	<?php $x = 1; ?>
		<?php foreach($students as $student): ?>
			<tr>
				<td><?php echo $x."."; ?></td>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>				
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>