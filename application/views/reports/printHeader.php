

<div class="white-area-content">

<?php if($display_options == "SELECTED"): ?>
  <a href="<?php echo base_url() . 'reports/selected/form_one'; ?>" class="btn btn-primary btn-sm">Form One</a>
    <a href="<?php echo base_url() . 'reports/selected/form_five'; ?>" class="btn btn-primary btn-sm">Form Five</a>
<?php endif; ?>

<?php if($display_options == "STUDENTS_LIST"): ?>
    <div class="db-header-extra form-inline"> 
    <div class="form-group has-feedback no-margin">
      
<?php echo form_open('reports/students'); ?>

      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_by_class" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>
            &nbsp; &nbsp;
<div class="text-right">
            <button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
        <span class="glyphicon glyphicon-print"></span>
      </button> 
</div>    
          </div>
        </div>
<?php endif; ?>

<?php if($display_options == "TRANSFERRED_IN"): ?>
    <div class="db-header-extra form-inline"> 
    <div class="form-group has-feedback no-margin">
            <div class="input-group">
        <?php echo form_open('reports/transferred_in'); ?>
          <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
          <div class="input-group-btn">
            <input type="hidden" id="search_type" value="0">
            <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>                    
          </div>
        <?php echo form_close(); ?>
            </div>
                        &nbsp; &nbsp;
<div class="text-right">
            <button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
        <span class="glyphicon glyphicon-print"></span>
      </button>
</div>       
          </div>
        </div>
<?php endif; ?>

<?php if($display_options == "TRANSFERRED_OUT"): ?>
    <div class="db-header-extra form-inline"> 
    <div class="form-group has-feedback no-margin">
            <div class="input-group">
        <?php echo form_open('reports/transferred_out'); ?>
          <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
          <div class="input-group-btn">
            <input type="hidden" id="search_type" value="0">
            <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>                    
          </div>
        <?php echo form_close(); ?>
            </div>          
              &nbsp; &nbsp;
<div class="text-right">
            <button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
        <span class="glyphicon glyphicon-print"></span>
      </button> 
  </div>    
          </div>
        </div>
<?php endif; ?>

<?php if($display_options == "PARENTS"): ?>
    <div class="db-header-extra form-inline"> 
    <div class="form-group has-feedback no-margin">
            <div class="input-group">
        <?php echo form_open('reports/parents'); ?>
          <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
          <div class="input-group-btn">
            <input type="hidden" id="search_type" value="0">
            <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>                    
          </div>
        <?php echo form_close(); ?>
            </div>           
             &nbsp; &nbsp;
<div class="text-right">
            <button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
        <span class="glyphicon glyphicon-print"></span>
      </button>
  </div>     
          </div>
        </div>
<?php endif; ?>

<?php if($display_options == "STAFF_ATTENDANCE"): ?>
    <?php echo form_open('reports/staff_attendance_report'); ?>
  <div class="col-xs-3">
    <select class="form-control" name="monthValue" id="monthValue" required>
      <option value="">--Choose Month--</option>
      <option value="1-January">January</option>
      <option value="2-February">February</option>
      <option value="3-March">March</option>
      <option value="4-April">April</option>
      <option value="5-May">May</option>
      <option value="6-June">June</option>
      <option value="7-July">July</option>
      <option value="8-August">August</option>
      <option value="9-September">September</option>
      <option value="10-October">October</option>
      <option value="11-November">November</option>
      <option value="12-December">December</option>
    </select>
  </div>
  <div class="col-xs-4">
    <?php $date = date('Y-m-d'); $year = explode('-', $date)[0]; ?>
    <select class="form-control" name="yearSearch" id="yearSearch" required>
      <?php 
        for($x = $year; $x >= $year - 10; $x--){
      ?>
          <option <?php if($x === $year){ echo "selected"; } ?> value="<?php echo $x; ?>"><?php echo $x; ?></option>
      <?php
        }
      ?>      
    </select>
  </div>
  <div class="col-xs-2">
    <input type="submit" name="search_month" value="Search" class="btn btn-primary btn-sm" />
  </div>
<?php echo form_close(); ?>
            &nbsp; &nbsp;
<div class="text-right">
    <button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
        <span class="glyphicon glyphicon-print"></span>
      </button>
 </div>      
<?php endif; ?>

<?php if($display_options != "TRANSFERRED_IN" && $display_options != "TRANSFERRED_OUT" && $display_options != "STAFF_ATTENDANCE" && $display_options != "PARENTS" && $display_options != "STUDENTS_LIST"): ?>
<button onclick="printDiv('printDocument')" style="float:right;" class="btn btn-primary btn-xs;" title="Print <?php echo $tooltip; ?>"/>
    <span class="glyphicon glyphicon-print"></span>
  </button>
<?php endif; ?>
<br/><br/>

<!-- START PRINT -->
<div id="printDocument">
  <table width="100%">
    <tr>
      <td rowspan="4" width="6%"><img src="<?php echo base_url();?>/assets/images/129.jpg" class="img-responsive" alt=""/></td>
    </tr> 
    <tr>
      <td align="center"><b>WIZARA YA ELIMU, SAYANSI, TEKNOLOJIA NA MAFUNZO YA UFUNDI</b></td>
    </tr>
    <tr>
      <td align="center"><b>SHULE YA SEKONDARI YA UFUNDI BWIRU WAVULANA</b></td>
    </tr>
    <tr>
      <td align="center"><b>S.L.P 217 MWANZA</b></td>
    </tr>
    
  </table>
<br/>
<div class="page-header-title"> &nbsp;&nbsp;<?php echo "<u>" . $title . "</u>"; ?></div>
<br/><br/>