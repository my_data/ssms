
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

      <div class="form-group has-feedback no-margin">
         
                <?php echo form_open('dormitory/accomodation_history'); ?>
               
    <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
     </div>

                        <?php echo form_close(); ?>
                  
          


            </div>&nbsp;&nbsp;
            <!-- <button onclick="printDiv('printAH')" class="btn btn-primary btn-xs;" title="Print"/><span class="glyphicon glyphicon-print"></span></button> --> 
          </div>
      </div>

<div id="successMessage" class="success_message_color">
  <?php 
    if($this->session->flashdata('success_message')){
      echo $this->session->flashdata('success_message');
    }
  ?>
</div>
<div id="errorMessage" class="error_message_color">
  <?php
    if($this->session->flashdata('error_message')){
      echo $this->session->flashdata('error_message');
    }
  ?>
</div>
<div id="warningMessage" class="error_message_color">
  <?php
    if($this->session->flashdata('exist_message')){
      echo $this->session->flashdata('exist_message');
    }
  ?>
</div>
<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <!-- <td width="2%" align="center">S/No.</td> -->
        <td width="25%" align="center">Student Names</td>
        <td width="28%" align="center">Dormitory Name</td>
        <td width="15%" align="center">From</td>
        <td width="15%" align="center">TO</td>
        <!-- <td width="15%" align="center">Academic Year</td> -->
      </tr>      
    </thead>
    <tbody>
    <?php if($accomodation_data == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; $adm_no = ""; ?>
    <?php foreach($accomodation_data as $ad): ?>
        <tr>
            <!-- <td align="center"><?php echo $x+$p."."; ?></td> -->
            <td align="center">
              <?php 
                if($adm_no != $ad['admission_no']){               
                  if($ad['end_date'] == NULL){ echo "<span style='color:#0000FF;'>". $ad['student_names'] . "</span>"; }else{ echo $ad['student_names']; }
                }
              ?>
             </td>
            <td align="center">
              <?php
                 if($ad['end_date'] == NULL){ echo "<span style='color:#0000FF;'>". $ad['dorm_name'] . "</span>"; }else{ echo $ad['dorm_name']; }
              ?>
            </td>
            <td align="center"><?php echo $ad['start_date']; ?></td>
            <td align="center"><?php if($ad['end_date'] == NULL){ echo "-"; }else{ echo $ad['end_date']; } ?></td>
            <!-- <td align="center"><?php echo $ad['year']; ?></td> -->
        </tr>
        <?php $x++; $adm_no = $ad['admission_no']; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>