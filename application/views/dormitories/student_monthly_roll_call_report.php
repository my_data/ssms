<?php echo form_open('dormitory/monthly_roll_call/'.$dorm_id); ?>
    <div class="col-xs-3">
        <select name="monthValue" id="monthValue" class="form-control" required>
          <option value="">--Choose Month--</option>
          <option value="1">January</option>
          <option value="2">February</option>
          <option value="3">March</option>
          <option value="4">April</option>
          <option value="5">May</option>
          <option value="6">June</option>
          <option value="7">July</option>
          <option value="8">August</option>
          <option value="9">September</option>
          <option value="10">October</option>
          <option value="11">November</option>
          <option value="12">December</option>
        </select>
    </div>
<input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
  <div class="col-xs-4">
    <?php $date = date('Y-m-d'); $year = explode('-', $date)[0]; ?>
    <select class="form-control" name="yearSearch" id="yearSearch" required>
      <?php 
        for($x = $year; $x >= $year - 10; $x--){
      ?>
          <option <?php if($x === $year){ echo "selected"; } ?> value="<?php echo $x; ?>"><?php echo $x; ?></option>
      <?php
        }
      ?>      
    </select>
  </div>
  <div class="col-xs-2">
    <input type="submit" name="search_month" value="Search" class="btn btn-primary btn-sm" />
  </div>
<?php echo form_close(); ?>
<br /><br /><br />

    <div id="exhaustive_table" class="table-responsive">
        <table class="table table-striped table-hover table-condensed table-bordered">
            <thead class="table-header">
              <tr>
                <td>Student Names</td>
                <?php
                  $x = 1;
                  while($x <= 31){
                    echo "<th>$x</th>";
                    $x++;
                  }
                ?>
              </tr>
            </thead>
            <tbody>
            <?php if($flag): ?>
              <td colspan="32">
                Select a Month to search for a report
              </td>
            <?php else: ?>
              <?php foreach($monthly_roll_call_records AS $monthly_roll_call_record): ?>
                <tr>
                  <td>
                    <?php
                      echo $monthly_roll_call_record['firstname'] . " " . $monthly_roll_call_record['lastname'];
                    ?>            
                  </td>
                  <td><?php echo $monthly_roll_call_record['1'] ?></td>
                  <td><?php echo $monthly_roll_call_record['2'] ?></td>
                  <td><?php echo $monthly_roll_call_record['3'] ?></td>
                  <td><?php echo $monthly_roll_call_record['4'] ?></td>
                  <td><?php echo $monthly_roll_call_record['5'] ?></td>
                  <td><?php echo $monthly_roll_call_record['6'] ?></td>
                  <td><?php echo $monthly_roll_call_record['7'] ?></td>
                  <td><?php echo $monthly_roll_call_record['8'] ?></td>
                  <td><?php echo $monthly_roll_call_record['9'] ?></td>
                  <td><?php echo $monthly_roll_call_record['10'] ?></td>
                  <td><?php echo $monthly_roll_call_record['11'] ?></td>
                  <td><?php echo $monthly_roll_call_record['12'] ?></td>
                  <td><?php echo $monthly_roll_call_record['13'] ?></td>
                  <td><?php echo $monthly_roll_call_record['14'] ?></td>
                  <td><?php echo $monthly_roll_call_record['15'] ?></td>
                  <td><?php echo $monthly_roll_call_record['16'] ?></td>
                  <td><?php echo $monthly_roll_call_record['17'] ?></td>
                  <td><?php echo $monthly_roll_call_record['18'] ?></td>
                  <td><?php echo $monthly_roll_call_record['19'] ?></td>
                  <td><?php echo $monthly_roll_call_record['20'] ?></td>
                  <td><?php echo $monthly_roll_call_record['21'] ?></td>
                  <td><?php echo $monthly_roll_call_record['22'] ?></td>
                  <td><?php echo $monthly_roll_call_record['23'] ?></td>
                  <td><?php echo $monthly_roll_call_record['24'] ?></td>
                  <td><?php echo $monthly_roll_call_record['25'] ?></td>
                  <td><?php echo $monthly_roll_call_record['26'] ?></td>
                  <td><?php echo $monthly_roll_call_record['27'] ?></td>
                  <td><?php echo $monthly_roll_call_record['28'] ?></td>
                  <td><?php echo $monthly_roll_call_record['29'] ?></td>
                  <td><?php echo $monthly_roll_call_record['30'] ?></td>
                  <td><?php echo $monthly_roll_call_record['31'] ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
        </table> 
        <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>     
    </div>
    <div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

<?php endif; ?>