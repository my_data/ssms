
<div class="white-area-content">
<div class="db-header clearfix">

 <h3>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-bed" style="font-size:30px;"></i></span>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></h3>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<form role="form" action="<?php echo base_url('dormitory/shift_student'); ?>" method="post" class="form-horizontal">
		<br/>
		<div class="form-group">
			<label for="s_name" class="col-xs-2 text-right label-control">Dormitory Name:</label>
			<div class="col-xs-10 ">
				<input type="text" value="<?php echo $dorm_name; ?>" name="dorm_name" class="form-control" disabled>
				<input type="hidden" value="<?php echo $dorm_id; ?>" name="dorm_id">
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="dom_name" class="col-xs-2 text-right label-control">Student Name:</label>
			<div class="col-xs-10">   
				<select class="form-control " name="admission_no" >
					<option value="">--Select Student--</option>
					<?php foreach ($students as $student): ?>
						<option value="<?php echo $student['admission_no']; ?>" ><?php echo $student['firstname'] . " " . $student['lastname']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="submit" class=" label-control"></label>
			<div class="col-xs-12">
				<input type="Submit" name="shift_student" class="btn btn-primary form-control" value="Transfer Student">
			</div>
		</div>

	</form>


</div>