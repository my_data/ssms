<!-- <div class="col-md-9" style="padding-top: 50px; padding-bottom: 100px;">

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="glyphicon glyphicon-bed" style="font-size:30px;"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">

			<div  align="right">
				<table>
					<tr>
						<td>
							<?php echo form_open('dormitory/monthly_roll_call_report/'.$dorm_id . '/'. date('Y-m-d')); ?>
							
								<a href="<?php echo base_url() . 'dormitory/today_roll_call_report/'.$dorm_id . '/'. date('Y-m-d'); ?>" class="btn btn-primary">Daily</a>&nbsp; &nbsp; &nbsp;
								<input type="hidden" name="date_" value="<?php echo date('Y-m-d'); ?>" />
							  	<input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
							  	<input type="submit" name="monthly" value="Monthly" class="btn btn-primary">
							<?php echo form_close(); ?>
						</td>
					</tr>
				</table>
			</div>

			</div>
		</div>
	</div>
</div>
 -->





  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
              	<?php if($is_monthly === "YES"): ?>
                	<?php echo form_open('dormitory/monthly_roll_call/'.$dorm_id.'/'.$date); ?>
                <?php endif; ?>
                <?php if($is_daily === "YES"): ?>
                	<?php echo form_open('dormitory/daily_roll_call/'.$dorm_id.'/'.$date); ?>
                <?php endif; ?>
                <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                      <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?>
                            
                    </div>
              </div>
            </div>
            &nbsp;
            <a href="<?php echo base_url() . 'dormitory/daily_roll_call/'.$dorm_id . '/'. date('Y-m-d'); ?>" class="btn btn-primary btn-sm">Today</a>
            &nbsp;
            <a href="<?php echo base_url() . 'dormitory/monthly_roll_call/'.$dorm_id.'/'.explode('-', date('Y-m-d'))[0].'-'.explode('-', date('Y-m-d'))[1]; ?>" class="btn btn-primary btn-sm">Monthly</a>
            
          </div>
      </div>

