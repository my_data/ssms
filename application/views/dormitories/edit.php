
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title">&nbsp;&nbsp;<span><i class="glyphicon glyphicon-bed"></i></span>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
	

	<form role="form" action="<?php echo base_url('dormitory/edit/'.$data->dorm_id); ?>" method="post" class="form-horizontal">

		<input type="hidden" value="<?php echo $data->dorm_id ?>" name="txt_hidden" >
		<br/>
		<div class="form-group">
			<label for="dorm_name" class="col-md-3 label-control">Domitory Name:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_name'); ?>
			</div>
				<input type="text" value="<?php echo $data->dorm_name; ?>" name="dorm_name" class="form-control" placeholder="Enter Dormitory Name" >
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="location" class="col-md-3 label-control">Domitory Location:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_location'); ?>
			</div>
				<input type="text" value="<?php echo $data->location; ?>" name="dorm_location" class="form-control" placeholder="Enter Dormitory Location" >
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="capacity" class="col-md-3 label-control">Domitory Capacity:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_capacity'); ?>
			</div>
				<input type="number" value="<?php echo $data->capacity; ?>" name="dorm_capacity" class="form-control" placeholder="Enter Dormitory Capacity" >
			</div>
		</div>
		<br/>
		<div class="form-group">
			<!-- <label for="submit" class=""></label> -->
			<div class="col-md-12 ">
				<input type="Submit" name="update" class="btn btn-primary form-control" value="Update">
			</div>
		</div>

	</form>


</div>
