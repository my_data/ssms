
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title">&nbsp;&nbsp;<span><i class="glyphicon glyphicon-bed"></i></span>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<form role="form" action="<?php echo base_url('dormitory/assignteacher/'.$data->dorm_id); ?>" method="post" class="form-horizontal">

		<input type="hidden" value="<?php echo $data->dorm_id; ?>" name="txt_hidden" >

		<div class="form-group">			
			<label for="dom_name" class="col-xs-2 text-right">Domitory Name:</label>
			<div class="col-xs-10 ">				
				<input type="text" value="<?php echo $data->dorm_name; ?>" name="dorm_name" class="form-control" placeholder="Enter Domitory Name" disabled>
			</div>
		</div>
		<br/>

		<div class="form-group">
			<label for="t_name" class="col-xs-2 text-right">Teacher Name:</label>
			
			<div class="col-xs-10">
			<div class="error_message_color">
				<?php echo form_error('t_name'); ?>
			</div>
				<select class="form-control" name="t_name" id="myselect" >
						<option value="">Select Teacher</option>
						<?php foreach ($teacher as $teacher): ?>
							<option <?php if($teacher['staff_id'] == $data->teacher_id){ echo "selected='true'"; } ?> value="<?php echo $teacher['staff_id']; ?>" ><?php echo $teacher['names']; ?></option>
						<?php endforeach; ?>
				</select>
			</div>
		</div>
		<!-- <br/>
		<div class="form-group">
			<label for="start_date" class="col-xs-2 text-right">Starting Date:</label>
			<div class="col-xs-10 ">
				<div class="error_message_color">
					<?php echo form_error('start_date'); ?>
				</div>
				<input type="date" name="start_date" class="form-control" placeholder="Enter Starting Date" >
				<div id="errorMessage" class="error_message_color">
				  <?php
				    if($this->session->flashdata('error_message')){
				      echo $this->session->flashdata('error_message');
				    }
				  ?>
				</div>
			</div>
		</div> -->
		<br/>
		<div class="form-group">
			<label for="submit"></label>
			<div class="col-xs-12 ">
				<input type="Submit" name="assign" class="btn btn-primary form-control" value="Assign Teacher">
			</div>
		</div>

	</form>


</div>
