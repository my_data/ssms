
<div class="white-area-content">
<div class="db-header clearfix">

<?php if($dorm_id == FALSE): echo "No Dormitory Selected"; //If no dorm id is selected?>
<?php else: ?>

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
 <div class="db-header-extra form-inline text-right"> 

 <div class="form-group has-feedback no-margin"> 
<?php echo form_open('dormitory/view_students/'.$dorm_id); ?>
<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
 </div>
<?php echo form_close(); ?>
</div>

    <a href="#" class="btn btn-sm btn-primary" data-target="#assignDormLeaderModal" data-toggle="modal" title="Add Dormitory Leader" data-placement="bottom">Add Dormitory Leader</a><br/>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">

	<thead class="table-header">
		<tr>
			<td align="center" width="30%">Student Names</td>	
			<td align="center" width="30%">Class Name</td>
			<td align="center" width="30%">Stream</td>
			<td align="center" width="10%">Action
                <span style="float: right;">
                    <a href="#" class="btn btn-primary btn-xs" title="End Of Dormitory Stay" data-toggle="modal" data-target="#endDormStayModal" data-placement="bottom"><strong>X</strong></a>
                  </span>
            </td>
		</tr>
	</thead>
	<tbody>
		<?php if ($students == FALSE): ?>
	    	<tr>
		    	<td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this Dormitory";
                        echo $message;
                    ?>
                </td>
		    </tr>
		<?php else: ?>
			<?php foreach ($students as $row): ?>
			<tr>
				<td align="center"><?php echo $row['names']; if($row['admission_no'] == $dorm_leader){ echo "&nbsp;<span style='color:#0000FF;' title='Dormitory Leader'>*</span>"; } ?></td>
				<td align="center"><?php echo $row['class_name']; ?></td>			
				<td align="center"><?php echo $row['stream']; ?></td>
				<!-- <td align="center"><?php echo $row['names']; ?></td> -->
				<td align="center">
				<a href='<?php echo base_url() . 'students/get_student_profile/' . $row['admission_no'];  ?>' class="btn btn-primary btn-xs" aria-hidden="true" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a>&nbsp;
				<?php if($this->session->userdata('transfer_student_to_another_dormitory') == "ok"): ?>
					<a href="<?php echo base_url('dormitory/shift_student/' .$row['admission_no']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="right" title="Transfer Student"><i class="glyphicon glyphicon-transfer"></i></a>
				<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody> 
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'dormitory/view_students/'.$dorm_id; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>
<?php endif; ?>

<!-- START MODAL ADD DORM LEADER -->
    <div class="modal fade" id="assignDormLeaderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Dormitory Leader:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("dormitory/assign_dorm_leader", $attributes);
                ?>         
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="admission_no" >Student Name</label>
                    <div class="col-sm-8">                     
	            		<select name="admission_no" class="form-control" required>
			                <option value="">--Choose Student--</option>
			                <?php foreach($students as$student): ?>
			                	<option value="<?php echo $student['admission_no']; ?>"><?php echo $student['names']; ?></option>
			            <?php endforeach; ?>
		                </select>
                    </div>
                </div>         
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">SAVE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- END MODAL ASSIGN DORM LEADER -->

<!-- START MODAL ALLOCATE STUDENT -->
    <div class="modal fade" id="addStudentToDormitory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Alocate Student To Dormitory: " . $dorm_name; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("dormitory/add_student_to_dorm", $attributes);
                ?>         
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="admission_no" >Student Name</label>
                    <div class="col-sm-8">                     
	            		<select name="admission_no" class="form-control" required>
			                <option value="">--Choose Student--</option>
			                <?php foreach($students_not_in as $std): ?>
			                	<option value="<?php echo $std['admission_no']; ?>"><?php echo $std['names']; ?></option>
			            <?php endforeach; ?>
		                </select>
                    </div>
                </div>         
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">ADD STUDENT</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- END MODAL ALLOCATE STUDENT -->



<!-- START MODAL AND STAY DAY -->
    <div class="modal fade" id="endDormStayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "End Dormitory Stay: " . $dorm_name; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("dormitory/end_stay_in_dorm", $attributes);
                ?>         
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="admission_no" >Date</label>
                    <div class="col-sm-8">                     
                        <input type="date" name="date_" class="form-control" required />
                    </div>
                </div>         
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="end_stay" class="btn btn-primary">SAVE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- END MODAL AND STAY DAY -->
