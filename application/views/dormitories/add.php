
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"><span><i class="glyphicon glyphicon-bed"></i></span>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></div>
    
</div>
  

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<form role="form" action="<?php echo base_url('dormitory/add'); ?>" method="post" class="form-horizontal">
		<br/>
		<div class="form-group">
			<label for="dom_name" class="col-md-3 label-control">Domitory Name:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_name'); ?>
			</div>
				<input type="text" name="dorm_name" class="form-control" placeholder="Enter Dormitory Name" value="<?php echo set_value('dorm_name'); ?>">
			</div>
		</div>
		<br/>

		<div class="form-group">
			<label for="location" class="col-md-3 label-control">Domitory Location:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_location'); ?>
			</div>
				<input type="text" name="dorm_location" class="form-control" placeholder="Enter Dormitory Location" value="<?php echo set_value('dorm_location'); ?>">
			</div>
		</div>
		<br/>

		<div class="form-group">
			<label for="capacity" class="col-md-3 label-control">Domitory Capacity:</label>
			<div class="col-md-9 ">
			<div class="error_message_color">
				<?php echo form_error('dorm_capacity'); ?>
			</div>
				<input type="number" name="dorm_capacity" class="form-control" placeholder="Enter Dormitory Capacity" min="0" value="<?php echo set_value('dorm_capacity'); ?>">
			</div>
		</div>
		<br/>

		<div class="form-group">
			<label for="submit" class=""></label>
			<div class="col-md-12 ">
				<input type="Submit" name="submit" class="btn btn-primary form-control" value="Save">
			</div>
		</div>

	</form>


</div>
