
<div class="white-area-content">
<div class="db-header clearfix">

 <h3>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-bed" style="font-size:30px;"></i></span>&nbsp;&nbsp;&nbsp;<?php echo $title; ?></h3>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<form role="form" action="<?php echo base_url('dormitory/shift_student/'.$data->admission_no); ?>" method="post" class="form-horizontal">

		<input type="hidden" value="<?php echo $data->admission_no ?>" name="admission_no" >
		<br/>
		<div class="form-group">
			<label for="s_name" class="col-xs-2 text-right label-control">Student Name:</label>
			<div class="col-xs-10 ">
				<input type="text" value="<?php echo $data->firstname . " " . $data->lastname ?>" name="s_name" class="form-control" disabled>
			</div>
		</div>

		<br/>
		<div class="form-group">
			<label for="dom_name" class="col-xs-2 text-right label-control">Domitory Name:</label>

			<div class="col-xs-10">   
				<select class="form-control " name="dorm_id" id="myselect" >
					<option value="">--Select Domitory--</option>
					<?php foreach ($dorms as $dormitory): ?>
						<option <?php if($dormitory->dorm_id == $data->dorm_id){ echo "selected='true'"; } ?> value="<?php echo $dormitory->dorm_id; ?>" ><?php echo $dormitory->dorm_name; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('dorm_id'); ?>
				</div>
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="submit" class=" label-control"></label>
			<div class="col-xs-12">
				<input type="hidden" name="current_dorm" value="<?php echo $data->dorm_id; ?>" />
				<input type="Submit" name="shift_student" class="btn btn-primary form-control" value="Transfer Student">
			</div>
		</div>

	</form>


</div>
