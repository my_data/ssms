
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
     
                <?php echo form_open('dormitory/current_and_past_dorm_masters'); ?>
                
          <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
         </div>
                        <?php echo form_close(); ?>
              
            </div>
          </div>
      </div>

<div class="form-group">
  <?php if($this->session->flashdata('success_message')): ?> 
    <div class="alert alert-dismissible alert-success text algin-center">
      <?php echo $this->session->flashdata('success_message'); ?>
    </div>
  <?php endif;?>
  <?php if($this->session->flashdata('error_validation')): ?> 
    <div class="alert alert-dismissible alert-warning text algin-center">
      <?php echo $this->session->flashdata('error_validation'); ?>
    </div>
  <?php endif;?>
  <?php if($this->session->flashdata('error_message')): ?> 
    <div class="alert alert-dismissible alert-danger text algin-center">
      <?php echo $this->session->flashdata('error_message'); ?>
    </div>
  <?php endif;?>
</div>

<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <!-- <td width="2%" align="center">S/No.</td> -->
        <td width="28%" align="center">Dormitory Name</td>
        <td width="25%" align="center">Teacher Names</td>        
        <td width="15%" align="center">From</td>
        <td width="15%" align="center">TO</td>
      </tr>      
    </thead>
    <tbody>
    <?php if($d_master_records == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; $dm_id = ""; ?>
    <?php foreach($d_master_records as $dmr): ?>
        <tr>
            <!-- <td align="center"><?php echo $x+$p."."; ?></td> -->
            <td align="center">
              <?php
                if ($dm_id != $dmr['dorm_id']){                  
                  echo $dmr['dorm_name']; 
                }
              ?>
            </td>
            <td align="center"><?php if($dmr['end_date'] == NULL){ echo "<span style='color:#0000FF;'>". $dmr['teacher_names'] . "</span>"; }else{ echo $dmr['teacher_names']; } ?><?php  ?></td>            
            <td align="center"><?php echo $dmr['start_date']; ?></td>
            <td align="center"><?php if($dmr['end_date'] == NULL){ echo "-"; }else{ echo $dmr['end_date']; } ?></td>
        </tr>
        <?php $x++; $dm_id = $dmr['dorm_id']; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>
&nbsp;
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>