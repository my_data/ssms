
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="glyphicon glyphicon-bed"></span>&nbsp;<?php echo $title; ?></div>
 <div class="db-header-extra form-inline text-right"> 

<div class="form-group has-feedback no-margin">
<?php echo form_open('dormitory/view_all'); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_dormitory" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
 </div>
<?php echo form_close(); ?>
</div>
<?php if($this->session->userdata('manage_dormitory') == 'ok'): ?>
	<a href="<?php echo base_url() . 'dormitory/add'; ?>" class="btn btn-primary btn-sm">Add Dormitory</a>
<?php endif; ?>
</div>


</div>
<div class="form-group">
	<?php if($this->session->flashdata('success_message')): ?> 
		<div class="alert alert-dismissible alert-success text algin-center">
			<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_validation')): ?> 
		<div class="alert alert-dismissible alert-warning text algin-center">
			<?php echo $this->session->flashdata('error_validation'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_message')): ?> 
		<div class="alert alert-dismissible alert-danger text algin-center">
			<?php echo $this->session->flashdata('error_message'); ?>
		</div>
	<?php endif;?>
</div>
<!-- <div class="alert alert-dismissible alert-danger text algin-center"> -->
	<?php echo validation_errors(); ?>
<!-- </div> -->
<div class="table table-responsive">
	<table class="table table-responsive table-striped table-hover table-condensed table-bordered">

		<thead class="table-header">
			<tr>
				<!-- <td>ID</td> -->
				<td width="30%" align="center">Domitory Name</td>
				<td width="20%" align="center">Location</td>
				<td width="3%" align="center">Capacity</td>
				<td width="2%" align="center">Available</td>
				<td width="20%" align="center">Domitory Teacher</td>
				<?php if($this->session->userdata('students_in_dormitory') == 'ok' || $this->session->userdata('manage_dormitory') == 'ok'): ?>
					<td width="25%" align="center">Action
						<span style="float: right;">
							<?php if($this->session->userdata('manage_dormitory') == 'ok'): ?>
								<a href="<?php echo base_url() . 'dormitory/current_and_past_dorm_masters'; ?>" class="btn btn-primary btn-xs" title="Past And Current Dormitory Masters" data-placement="bottom"><strong>DMH</strong></a>
							<?php endif; ?>
							&nbsp;
							<?php if($this->session->userdata('students_in_dormitory') == 'ok'): ?>
								<a href="<?php echo base_url() . 'dormitory/accomodation_history'; ?>" class="btn btn-primary btn-xs" title="Accomodation History" data-placement="bottom"><strong>AH</strong></a>
							<?php endif; ?>
						</span>
					</td>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
			<?php if($dormitories == FALSE): ?>
				<tr>
			    	<td colspan="4">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this Dormitory";
	                        echo $message;
	                    ?>
	                </td>
			    </tr>
			<?php else: ?>
				<?php foreach ($dormitories as $key => $row): ?>
			<tr>
				<!-- <td><?php echo $row['dorm_id'];?></td> -->
				<td align="center"><?php echo $row['dorm_name']; ?></td>
				<td align="center"><?php echo $row['location']; ?></td>
				<td align="center"><?php echo $row['capacity']; ?></td>	
				<td align="center"><?php echo $available[$key+$offset]['no_of_students']; ?></td>			
				<td align="center"><?php echo $row['names']; ?></td>
				<?php if($this->session->userdata('students_in_dormitory') == 'ok' || $this->session->userdata('manage_dormitory') == 'ok' || $this->session->userdata('add_dormitory_master') == 'ok' || $this->session->userdata('manage_dormitory_assets') == 'ok' || $this->session->userdata('view_roll_call') == 'ok'): ?>
					<td align="center">
						<?php if($this->session->userdata('students_in_dormitory') == 'ok'): ?>
							<a href="<?php echo base_url('dormitory/view_students/' .$row['dorm_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="View Students"><span class="fa fa-eye"></span></a>
						<?php endif; ?>
						<?php if($this->session->userdata('manage_dormitory') == 'ok'): ?>
							<a href="<?php echo base_url('dormitory/edit/' .$row['dorm_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
						<?php endif; ?>
						<?php if($this->session->userdata('add_dormitory_master') == 'ok'): ?>
						<a href="<?php echo base_url('dormitory/assignteacher/' .$row['dorm_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Assign Dormitory Master">A</a><!-- &nbsp; -->
						<?php endif; ?>
						<?php if($this->session->userdata('view_roll_call') == 'ok'): ?>
							<a href="<?php echo base_url('dormitory/roll_call_report/' .$row['dorm_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Roll Call">R</a>
						<?php endif; ?>
						<?php if($this->session->userdata('manage_dormitory_assets') == 'ok'): ?>	
							<a href="<?php echo base_url('dormitory/assets/' .$row['dorm_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="View Dormitory Assets"><span class="glyphicon glyphicon-bed"></span></a> 
						<?php endif; ?>
						<?php if($this->session->userdata('manage_dormitory') == 'ok'): ?>
							<a href="<?php echo base_url('dormitory/delete/' .$row['dorm_id']);?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Do you really want to delete <?php echo $row['dorm_name']; ?> dormitory?')"><i class="glyphicon glyphicon-trash"></i></i></a>
						<?php endif; ?>					
					</td>
				<?php endif; ?>
			</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>

	</table>
	<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'dormitory/view_all'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>
