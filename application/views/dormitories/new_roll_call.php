
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<?php $attributes = array('onSubmit' => 'return validate_new_attendance()'); ?>
<?php echo form_open('dormitory/save_student_roll_call'); ?>

<label for="date">Date</label>
<input type="date" name="date" id="date" required />

<div class="input-group-btn">
   
      </div>
</div>
</div>


</div>
</div>

<?php
	echo $this->session->flashdata('error_view');
?>
<input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>">

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th>Names</th>
			<th>Present</td>
			<th>Absent</th>
			<th>Permitted</th>
			<th>Home</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($dorm_students == FALSE): ?>
	      	<tr>
		        <td colspan="6">
		            <?php
		                $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in in this dormitory";
		                echo $message;
		            ?>
		         </td>
	     	</tr>
		<?php else: ?>
		<?php $x = 1; ?> 
		<?php foreach($dorm_students as $dorm_student): ?>
			<tr>
				<td>
					<?php echo $dorm_student['names']; ?>
					<input type="hidden" name="admission_no[]" value="<?php echo $dorm_student['admission_no']; ?>">
				</td>
				<!-- CHECKBOX -->
				<td>
					<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="2" checked="true" />
					<input type="hidden" name="" value="chb<?php echo $x; ?>">
				</td>
				<td>
					<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="1">
					<input type="hidden" name="" value="chb<?php echo $x; ?>">
				</td>
				<td>
					<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="3">
					<input type="hidden" name="" value="chb<?php echo $x; ?>">
				</td>
				<td>
					<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="5">
					<input type="hidden" name="" value="chb<?php echo $x; ?>">
				</td>
			</tr>
			<?php $x++; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
</div>
<div align="center">
	<input type="submit" name="submit" value="Save Roll Call" class="btn btn-primary form-control" onClick="return confirm('Are you sure you want to add this roll call record, it cannot be modified');" />
</div>
</form>