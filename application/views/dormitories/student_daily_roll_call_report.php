<?php echo form_open('dormitory/daily_roll_call/'.$dorm_id); ?>
    <div class="form-group">
        <div class="col-xs-1">
          <label>Date: </label>
        </div>

        <div class="col-xs-3">
            <input class="form-control" type="date" name="date_" />
            
            <input type="hidden" name="dorm_id" value="<?php echo $dorm_id; ?>" />
        </div>

        <div class="col-xs-1">
          <input type="submit" name="search_record" value="Search" class="btn btn-primary btn-sm"/>
        </div>
    </div>
<?php echo form_close(); ?>

<br/><br/><br/>

      <div class="row">

      <h4 align="center"><strong>Summary</strong></h4>
          <div id="summary_table" class="table-responsive">
              <table class="table table-striped table-hover table-condensed table-bordered">
                  <thead>
                      <tr class="table-header">
                        <td width="50%" align="center">Description</td>
                        <td width="50%" align="center">No of students</td>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($no_of_students as $nos): ?>
                      <tr>
                        <td align="center"><?php echo $nos['description']; ?></td>
                        <td align="center"><?php echo $nos['no_of_students']; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
              </table>
          </div>

      <h4 align="center"><strong>Attendance</strong></h4>

      <div id="exhaustive_table" class="table-responsive">
          <table class="table table-striped table-hover table-condensed table-bordered">
              <thead>
                <tr class="table-header">
                  <td align="center" width="50%">Student Names</td>
                  <td align="center" width="50%">Status</td>
                </tr>
              </thead>
              <tbody>
              <?php if ($students == FALSE): ?>
                <tr>
                  <td colspan="2">
                            <?php
                                $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Roll Call Records";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
                <?php foreach($students as $student): ?>
                  <tr>
                    <td align="center"><?php echo $student['firstname'] . " " . $student['lastname']; ?></td>
                    <td align="center"><?php echo $student['att_type']; ?></td>
                <?php endforeach; ?>
              </tbody>
          </table>  
          <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>      
      </div>

      <div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

<?php endif; ?>
