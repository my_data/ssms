
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>

            <div class="db-header-extra form-inline text-right"> 
            
            <div class="form-group has-feedback no-margin">
                <?php if($is_monthly === "YES"): ?>
                  <?php echo form_open('classes/monthly_attendance_report/'.$class_stream_id.'/'.$date); ?>
                <?php endif; ?>
                <?php if($is_daily === "YES"): ?>
                  <?php echo form_open('classes/daily_attendance/'.$class_stream_id.'/'.$date); ?>
                <?php endif; ?>
                
                      <div class="input-group ">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn" aria-haspopup="true" aria-expanded="false">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
                     </div>
                
                          <?php echo form_close(); ?>

            </div>

            <a href="<?php echo base_url() . 'classes/daily_attendance/'.$class_stream_id . '/'. date('Y-m-d'); ?>" class="btn btn-primary btn-sm">Today</a>
            &nbsp;
            <a href="<?php echo base_url() . 'classes/monthly_attendance_report/'.$class_stream_id.'/'.explode('-', date('Y-m-d'))[0].'-'.explode('-', date('Y-m-d'))[1]; ?>" class="btn btn-primary btn-sm">Monthly</a>
           
            </div>
         
          </div>
           





