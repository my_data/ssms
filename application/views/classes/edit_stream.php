
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('classes/update_stream', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="stream_name">Stream :</label>
			<div class="col-sm-10">
				<input type="text" name="stream_name" value="<?php echo $stream['stream_name']; ?>" class="form-control">
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="stream_id" value="<?php echo $stream['stream_id']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_stream" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>