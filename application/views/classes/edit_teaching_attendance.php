
<div class="white-area-content">
<div class="db-header clearfix">

 <h3><img src="<?php echo base_url();?>/assets/images/jounal.jpg" class="img-responsive pull-left gap-right " alt="..."><?php echo $title; ?></h3>
    
</div>
<div class="clearfix">


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('classes/edit_teaching_attendance/'.$cs); ?>

<div class="form-group">
	<div class="col-xs-1">
		<label>Date:</label>
	</div>
	<div class="col-xs-3">
		<input class="form-control" type="date" name="date" required />
	</div>
	<div class="col-xs-4">
	<input type="submit" name="search_journal" value="Search" class="btn btn-primary btn-sm" />
	</div>
</div>

<?php echo form_close(); ?>
</br></br></br>

<div class="col-xs-12">
<div class="table-responsive" id="edit_class_journal">
	<table class="table table-bordered table-striped table-hover table-condensed" >
		<thead>
			<tr class="table-header">
					<td width="5%">Period#</td>
					<!-- <th width="5%">Time</th> -->
					<td width="25%">Subject</td>
					<td width="25%">Subject Teacher</td>
					<td width="15%" align="center">Topic Taught</td>
					<td width="15%" align="center">Sub Topic</td>
					<td width="10%" align="center">Abs</td>
					<td width="5%" align="center">Action</td>
					<!-- <th width="5%">Action</th> -->
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; ?>
			<?php foreach($records as $record): ?>
				<?php echo form_open('classes/edit_teaching_attendance/'.$record['class_stream_id'].'/'.$record['date']); ?>
			<tr>			
				<td align="center" contenteditable="false" class="period_no">
					<?php echo $record['period_no']; ?>
					<input type="hidden" name="period_no[]" value="<?php echo $record['period_no']; ?>">
					<input type="hidden" name="ta_id" value="<?php echo $record['ta_id']; ?>">
				</td>
				<td contenteditable="false" class="subject">
					<?php echo $record['subject_name']; ?>
				</td>
				<td contenteditable="false" class="subject_teacher">
					<?php echo $record['subject_teacher']; ?>
				</td>
				<td contenteditable="false" class="topic_taught">
					<select class="form-control" name="status" id="status<?php echo $i; ?>">
						<option value="taught" <?php if($record['status'] === "taught"){ echo "selected='true'"; } ?>>Taught</option>
						<option value="untaught" <?php if($record['status'] === "untaught"){ echo "selected='true'"; } ?>>NOT Taught</option>
					</select>
				</td>
				<td contenteditable="false" class="sub_topic">
					<select class="form-control" name="subtopic" id="subtopic<?php echo $i; ?>">
						<option value="0">--None--</option>
						<?php foreach ($subtopics as $subtopic): ?>
						<?php
							if($subtopic['subject_id'] == $record['subject_id']){
						?>
								<option <?php if($record['subTopicID'] === $subtopic['subtopic_id']){ echo "selected='true'"; } ?> value='<?php echo $subtopic['subtopic_id']; ?>'>
									<?php echo $subtopic['subtopic_name']; ?>
								</option>";
						<?php
							}
						?>						
						<?php endforeach; ?>
					</select>
				</td>
				<td align="center">
					<input class="form-control" type="number" value="<?php echo $record['no_of_absentees']; ?>" name="absentees" style="width: 50px"; id="abs<?php echo $i; ?>" min="0" />
				</td>
				<td>
						<input type="hidden" name="date_" value="<?php echo $date; ?>" />
						<input type="submit" name="update_ta" value="update" class="btn btn-xs btn-primary" onClick="return alert('Save changes?');" />
					<?php echo form_close(); ?>
				</td>
			</tr>
			<?php $i++; endforeach; ?>
		</tbody>
	</table>
</div> <!-- END OF EDIT CLASS_JOURNAL DIV-->
	</form>
