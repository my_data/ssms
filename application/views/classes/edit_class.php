

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-pencil-square"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('classes/edit_class/'.$class_id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_name">Class Name :</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="class_name" value="<?php echo $class['class_name']; ?>" id="class_name" />
				<div class="error_message_color">
					<?php echo form_error('class_name'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="level">Level :</label>
			<div class="col-sm-10">
				<select name="level" class="form-control" id="level">
					<option <?php if($class['level'] === "O'Level"){ echo "selected='true'"; }?> value="O'Level">O'level</option>
					<option <?php if($class['level'] === "A'Level"){ echo "selected='true'"; }?> value="A'Level">A'level</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('level'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="class_id" value="<?php echo $class['class_id']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_class" value="Update" onClick="return confirm('Save Changes?');" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>