

  <div class="white-area-content">
      <div class="db-header clearfix">

  <div class="page-header-title"> <!-- <span class="fa fa-book"></span> --><?php echo $title; ?>
  </div>
      <div class="db-header-extra form-inline"> 

          <div class="form-group has-feedback no-margin">
        <div class="input-group">
          
        </div>
      </div>

   <a href="#" onclick="printDiv('printClassTimetable')" ><span class="btn btn-xs btn-primary glyphicon glyphicon-print"></span></a>

  </div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- END PRINTING -->
<div id="printClassTimetable" title="Print Timetable">

  <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr>
        <th>Period#</th>
        <th>Start</th>
        <th>End</th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
      </tr>
    </thead>
    <tbody>
     <?php if ($class_timetable == FALSE): ?>
      <tr>
        <td colspan="8">
              <?php
                  $message = "No Timetable created for this class";
                  echo $message;
              ?>
          </td>
      </tr>
    <?php else: ?>
      <?php foreach($class_timetable as $ct): ?>
          <tr>
            <td><?php echo $ct['period_no']; ?></td>
            <td><?php echo $ct['start_time']; ?></td>
            <td><?php echo $ct['end_time']; ?></td>
            <td>
              <?php
                if($ct['Monday'] != ""){
                  $x = 0;

                  foreach ($opt_subjects as $key => $opts) {
                    if($opts['subject_name'] == explode('-', $ct['Monday'])[0] && $monday_check[$ct['period_no']] > 1){
                      $x++;
                      echo "<span title='".$names_of_teachers_teaching_optional_subjects."' data-toggle='tooltip' data-placement='bottom'>" . $opts_sub_to_display . "</span>";
                    }
                  }
                  if($x == 0){
                    echo "<span title='".explode('-', $ct['Monday'])[1]."' data-toggle='tooltip' data-placement='bottom'>" . explode('-', $ct['Monday'])[0] . "</span>";
                  }
                }
                else{
                  echo "Free Period";
                }
              ?>              
            </td>
            <td>
              <?php
                if($ct['Tuesday'] != ""){
                  $x = 0;
                  foreach ($opt_subjects as $opts) {
                    if($opts['subject_name'] == explode('-', $ct['Tuesday'])[0] && $tuesday_check[$ct['period_no']] > 1){
                      $x++;
                     echo "<span title='".$names_of_teachers_teaching_optional_subjects."' data-toggle='tooltip' data-placement='bottom'>" . $opts_sub_to_display . "</span>";
                    }
                  }
                  if($x == 0){
                    echo "<span title='".explode('-', $ct['Tuesday'])[1]."' data-toggle='tooltip' data-placement='bottom'>" . explode('-', $ct['Tuesday'])[0] . "</span>";
                  }
                }
                else{
                  echo "Free Period";
                }
              ?>              
            </td>
            <td>
              <?php
                if($ct['Wednesday'] != ""){
                  $x = 0;
                  foreach ($opt_subjects as $opts) {
                    if($opts['subject_name'] == explode('-', $ct['Wednesday'])[0] && $wednesday_check[$ct['period_no']] > 1){
                      $x++;
                     echo "<span title='".$names_of_teachers_teaching_optional_subjects."' data-toggle='tooltip' data-placement='bottom'>" . $opts_sub_to_display . "</span>";
                    }
                  }
                  if($x == 0){
                    echo "<span title='".explode('-', $ct['Wednesday'])[1]."' data-toggle='tooltip' data-placement='bottom'>" . explode('-', $ct['Wednesday'])[0] . "</span>";
                  }
                }
                else{
                  echo "Free Period";
                }
              ?>              
            </td>
            <td>
              <?php
                if($ct['Thursday'] != ""){
                  $x = 0;
                  foreach ($opt_subjects as $opts) {
                    if($opts['subject_name'] == explode('-', $ct['Thursday'])[0] && $thursday_check[$ct['period_no']] > 1){
                      $x++;
                      echo "<span title='".$names_of_teachers_teaching_optional_subjects."' data-toggle='tooltip' data-placement='bottom'>" . $opts_sub_to_display . "</span>";
                    }
                  }
                  if($x == 0){
                    echo "<span title='".explode('-', $ct['Thursday'])[1]."' data-toggle='tooltip' data-placement='bottom'>" . explode('-', $ct['Thursday'])[0] . "</span>";
                  }
                }
                else{
                  echo "Free Period";
                }
              ?>              
            </td>
            <td>
              <?php
                if($ct['Friday'] != ""){
                  $x = 0;
                  foreach ($opt_subjects as $opts) {
                    if($opts['subject_name'] == explode('-', $ct['Friday'])[0] && $friday_check[$ct['period_no']] > 1){
                      $x++;
                      echo "<span title='".$names_of_teachers_teaching_optional_subjects."' data-toggle='tooltip' data-placement='bottom'>" . $opts_sub_to_display . "</span>";
                    }
                  }
                  if($x == 0){
                    echo "<span title='".explode('-', $ct['Friday'])[1]."' data-toggle='tooltip' data-placement='bottom'>" . explode('-', $ct['Friday'])[0] . "</span>";
                  }
                }
                else{
                  echo "Free Period";
                }
              ?>              
            </td>
          </tr>
    <?php
        foreach ($breaks as $br):
          if($ct['period_no'] === $br['after_period']):
          ?>
            <tr>
              <td colspan="8" align="center" style="font-size: 16px;"><?php echo $br['description']; ?></td>
            </tr>
          <?php
          endif;
        endforeach;
    ?>
      <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
  </table>
</div>

</div><!-- END PRINTING -->

<!-- START TIMETABLE  -->
<?php foreach ($class_timetable as $cs): ?>

  <div class="modal fade" id="class_stream_timetable<?php //echo $cs['class_stream_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php //echo "Timetable: " . $cs['class_name'] . " " . $cs['stream']; ?></h4>
                <ul>
                  <li><?php ?></li>
                </ul>
            </div>
            <!-- Modal Body -->
            <div class="modal-body"> 
                                
            </div>
             <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!-- END TIMETABLE -->