<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('classes/daily_attendance/'.$class_stream_id); ?>
<div class="form-group">

<div class="col-xs-1">
  <label>Date: </label>
</div>

<div class="col-xs-3">
  <input class="form-control" type="date" name="tarehe" required />
</div>

<div class="col-xs-1">
<input type="submit" name="search_record" value="Search" class="btn btn-primary btn-sm"/>
</div>
</div>
<?php echo form_close(); ?>
<br/><br/><br/>
<div class="row">
<!-- <div id="successMessage" class="success_message_color">
  <?php 
    if($this->session->flashdata('success_message')){
      echo $this->session->flashdata('success_message');
    }
  ?>
</div>
<div id="errorMessage" class="error_message_color">
  <?php
    if($this->session->flashdata('error_message')){
      echo $this->session->flashdata('error_message');
    }
  ?>
</div>
<div id="warningMessage" class="error_message_color">
  <?php
    if($this->session->flashdata('exist_message')){
      echo $this->session->flashdata('exist_message');
    }
  ?>
</div> -->
<h4 align="center">Summary</h4>
<div id="summary_table" class="table-responsive">
 <table class="table table-striped table-hover table-condensed table-bordered">
    <!-- <thead>
      <tr class="table-header">
        <td width="50%" align="center">Description</td>
        <td width="50%" align="center">No of students</td>
      </tr>
    </thead> -->
    <tbody>
     <!--  <?php foreach($no_of_students as $nos): ?>
        <tr>
          <td align="center"><?php echo $nos['description']; ?></td>
          <td align="center"><?php echo $nos['no_of_students']; ?></td>
        </tr>
      <?php endforeach; ?> -->
      <tr  class="table-header">
        <td width="20%" align="center">Description</td>
        <?php foreach($no_of_students as $nos): ?>
            <td align="center"><?php echo $nos['description']; ?></td>
        <?php endforeach; ?>
      </tr>
      <tr>
        <td width="20%" align="center">No of Students</td>
        <?php foreach($no_of_students as $nos): ?>
            <td align="center"><?php echo $nos['no_of_students']; ?></td>
        <?php endforeach; ?>
      </tr>
    </tbody>
  </table>
</div>

<h4 align="center">Attendance</h4>

<div id="exhaustive_table" class="table-responsive">
 <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <td align="center" width="50%">Student Names</td>
        <td align="center" width="50%">Status</td>
      </tr>
    </thead>
    <tbody>
    <?php if ($students == FALSE): ?>
      <tr>
        <td colspan="2">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Attendance Records";
                      echo $message;
                  ?>
              </td>
      </tr>
  <?php else: ?>
      <?php foreach($students as $student): ?>
        <tr>
          <td align="center"><?php echo $student['names']; ?></td>
          <td align="center"><?php echo $student['description']; ?></td>
      <?php endforeach; ?>
    </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>
<?php endif; ?>