
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
        <div class="db-header-extra form-inline text-right"> 
        <div class="form-group has-feedback no-margin">  
                
                <?php echo form_open('classes/index'); ?>
                 <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_class" placeholder="Search ..." id="form-search-input" />
                      <div class="input-group-btn" aria-haspopup="true" aria-expanded="false">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                      </div>
                  </div>
                        <?php echo form_close(); ?>
        </div>
<?php if($this->session->userdata('manage_class') == 'ok'): ?>
            <a href="<?php echo base_url() . 'classes/add_class_stream'; ?>" class="btn btn-primary btn-sm">Add Class Stream</a>
<?php endif; ?>        
        </div>           
              
        </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Class Name</td>
    <?php foreach($class_streams_count as $csc): ?>
      <td><?php echo $csc['class_name']; ?></td>
    <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>    
    <td>No. of Streams</td>
      <?php foreach($class_streams_count as $csc): ?>
    <td><?php echo $csc['total_streams']; ?></td>
      <?php endforeach; ?>
    </tr>
  </tbody>
</table>


<!-- <table id="class_data" class="table table-striped table-hover table-condensed"> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<!-- <th width="20%">Class Name#</th>
			<th width="10%" >Stream</th> -->
			<td>Class Name#</td>
			<td>Stream</td>
			<td>Capacity</td>
			<td>Enrolled</td>
			<td>Description</td>
			<td>Class Teacher</td>
      <?php if($this->session->userdata('view_students_in_class') == 'ok' || $this->session->userdata('manage_class') == 'ok' || $this->session->userdata('assign_class_teacher') == 'ok' || $this->session->userdata('manage_class_assets') == 'ok' || $this->session->userdata('view_assigned_teachers_in_class') == 'ok'): ?>
  			<td align="center">Action
          <?php if($this->session->userdata('manage_class') == 'ok'): ?>
            <span style="float: right;">
                <a href="<?php echo base_url() . 'classes/current_and_past_class_teachers'; ?>" class="btn btn-primary btn-xs" title="Past And Current Class Teachers" data-placement="bottom"><strong>CTH</strong></a>
            </span>
          <?php endif; ?>
        </td>
      <?php endif; ?>
		</tr>
	</thead>
	<tbody>
    <?php if ($classes == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Classes";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach($classes as $cs): ?>
          <tr>
            <td><?php echo $cs['class_name']; ?></td>
            <td><?php echo $cs['stream']; ?></td>
            <td><?php echo $cs['capacity']; ?></td>
            <td><?php echo $cs['enrolled']; ?></td>
            <td><?php echo $cs['description']; ?></td>
            <td><?php echo $cs['teacher_names']; ?></td>
            <?php if($this->session->userdata('view_students_in_class') == 'ok' || $this->session->userdata('manage_class') == 'ok' || $this->session->userdata('assign_class_teacher') == 'ok' || $this->session->userdata('manage_class_assets') == 'ok' || $this->session->userdata('view_assigned_teachers_in_class') == 'ok'): ?>
              <td align="center">
                <?php if($this->session->userdata('view_students_in_class') == 'ok'): ?>
                  <a href="<?php echo base_url() . 'students/get_students_by_class/'.$cs['class_stream_id']; ?>" class="btn btn-primary btn-xs" title="View Students" data-placement="bottom"><span class="fa fa-eye"></span></a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('assign_class_teacher') == 'ok'): ?>
                  <a href="" class="btn btn-xs btn-primary" title="Add Class Teacher" data-placement="bottom" data-toggle="modal" data-target="#myModal<?php echo $cs['class_stream_id']; ?>" >A</a>
                  &nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('view_assigned_teachers_in_class') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>myclass/subject_teachers/<?php echo $cs['class_stream_id']; ?>" title="Teachers currently teaching this class" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('manage_class_assets') == 'ok'): ?>
                  <a href="<?php echo base_url('classes/assets/' .$cs['class_stream_id']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="View Class Assets"><span class="glyphicon glyphicon-bed"></span></a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('manage_class') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>classes/edit_class_stream/<?php echo $cs['class_stream_id']; ?>" title="Edit Class Stream" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
                  <a href="<?php echo base_url(); ?>classes/delete_class_stream/<?php echo $cs['class_stream_id']; ?>" class="btn btn-xs btn-danger" onClick="return confirm('Are you sure you want to delete this class stream?');"><span class="glyphicon glyphicon-trash" ></span></a>
                <?php endif; ?>
              </td>
            <?php endif; ?>
          </tr>
      <?php endforeach; ?>
    <?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>      
    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>








<?php foreach ($classes as $class): ?>

	<div class="modal fade" id="myModal<?php echo $class['class_stream_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Assign Class Teacher to: " . $class['class_name'] . " " . $class['stream']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form')
            	?>            
                <?php echo form_open('classes/assign_teacher/'.$class['class_stream_id'], $attributes); ?>
                  	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="teacher_names" >Teacher Name</label>
                    	<div class="col-sm-8">
                        	<select name="staff_id" class="form-control">
                        		<option value="">--Choose--</option>
                        		<?php foreach($teachers as $teacher): ?>
                        			<option value="<?php echo $teacher['staff_id']; ?>" 
                        			<?php 
                        				if($teacher['staff_id'] == $class['teacher_id']){
                        					echo "selected";
                        				}
                        			?>><?php echo $teacher['firstname'] . " " . $teacher['lastname']; ?></option>
                        		<?php endforeach; ?>
                        	</select>
                    	</div>
                  	</div>
                  	<div class="form-group">
	                    <label  class="col-sm-4 control-label" for="Form">Form: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="form" id="form" value="<?php echo $class['class_name']; ?>" readonly/>
                    	</div>
	                </div>
	                <div class="form-group">
	                    <label  class="col-sm-4 control-label" name="stream" for="stream">Stream: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="stream" id="stream" value="<?php echo $class['stream']; ?>" readonly/>
                    	</div>
	                </div>                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>