
<div class="white-area-content">
<div class="db-header clearfix">

 <h3><img src="<?php echo base_url();?>/assets/images/jounal.jpg" class="img-responsive pull-left gap-right " alt="..."><?php echo $title; ?></h3>
    
</div>
<div class="clearfix">
<?php echo form_open('classes/class_journal_weekly_report/'.$this->session->userdata('class_stream')['class_stream_id']); ?>
	  
	<div class="form-group">
		<div class="col-xs-1">
			<label>From :</label>
		</div>
		<div class="col-xs-3">
			<input class="form-control" type="date" name="from_date"  />
		</div>
		<div class="col-xs-1">
			<label>To :</label>
		</div>
		<div class="col-xs-3">
			<input class="form-control" type="date" name="to_date"  />
		</div>
		<div class="col-xs-4">
		<input type="submit" name="search_journal_report" value="Search" class="btn btn-primary btn-sm" />
		</div>
	</div>	
	<!-- From: <input type="date" name="from_date" />
  To: <input type="date" name="to_date" />
    <input type="submit" name="search_journal_report" value="Search" /> -->
<?php echo form_close(); ?>
