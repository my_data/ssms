
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"><?php echo $title; ?></div>
 </div>

<?php echo validation_errors(); ?>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table-responsive">
<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th width="10%">Period#</th>
			<th width="18%">Monday</th>
			<th width="18%">Tuesday</th>
			<th width="18%">Wednesday</th>
			<th width="18%">Thursday</th>
			<th width="18%">Friday</th>
		</tr>
	</thead>
	<tbody>
		<!-- 
			https://www.youtube.com/watch?v=UliJeDbc4cw 
		-->
		<!-- <?php echo form_open('classes/extract_to_view'); ?>
			<div class="form-group">
				
				<div class="col-md-3">
					<select name="class_name" id="class_name" class="form-control">
						<option value="">--Choose Class--</option>
						<?php foreach($classes as $class_level): ?>
							
							<option value="<?php echo $class_level['class_id']; ?>">
								<a href=""><?php echo $class_level['class_name']; ?></a>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-3">
					<input type="hidden" id="class_name_value" />
					<select name="stream" id="stream" class="form-control">
						<option value="">--Choose Stream--</option>
						<?php foreach($streams as $stream): ?>
							<option value="<?php echo $stream['stream_name']; ?>">
								<?php echo $stream['stream_name']; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-3">
					<input type="hidden" id="stream_name_value" />
					<input type="submit" name="filter" value="Create New Timetable" class="btn btn-primary "/>
					
				</div>
			</div>
		</form> -->

		<?php echo form_open('classes/edit_timetable/'.$class_stream_id); ?>
		<!-- <div id="successMessage" class="success_message_color">
			<?php 
				if($this->session->flashdata('success_message')){
					echo $this->session->flashdata('success_message');
				}
			?>
		</div>
		<div id="errorMessage" class="error_message_color">
			<?php
				if($this->session->flashdata('error_message')){
					echo $this->session->flashdata('error_message');
				}
			?>
		</div>
		<div id="warningMessage" class="error_message_color">
			<?php
				if($this->session->flashdata('exist_message')){
					echo $this->session->flashdata('exist_message');
				}
			?>
		</div> -->
		<?php $count_period_no = 0; ?>
		<?php foreach($periods as $p): ?>
			<tr>
				<td>
					<?php echo $p['period_no']; ?>
					<input type="hidden" name="period_no[]" value="<?php echo $p['period_no']; ?>" />
					
				</td>
				<div class="form-group">
				<td>
					<select name="monday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Monday"; ?>" onchange="checkTeacherInTimetableWhenEditingMonday(this.id)" required>
						<?php $mx = 0; // For testing and display the word Optional Technical Subjects ?>
						<option value="free">Free Period</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php foreach ($rtm as $rt) {
					 			if($subject['subject_id'] == $rt['subject_id'] && $p['period_no'] == $rt['period_no']){
					 				echo "selected";
					 				if($rt['subject_choice'] == "optional"){
					 					$mx++; //Increment value by 1 to make sure if Optional Subjects or The actual subject name was chosen... Eg: Electronocs
					 				}
					 			}
					 		}?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<option value="ots" <?php if($mx > 1){ echo "selected"; } ?>>Optional Technical Subjects</option>
					 </select>
					<input type="hidden" name="monday[]" value="Monday" />
					<div id="<?php echo "mon".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				<td>
					<select name="tuesday_subject[]" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Tuesday"; ?>" onchange="checkTeacherInTimetableWhenEditingTuesday(this.id)" class="form-control" required>
						<option value="free">Free Period</option>
						<?php $tx = 0; // For testing and display the word Optional Technical Subjects ?>
						<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php foreach ($rtt as $rt) {
					 			if($subject['subject_id'] == $rt['subject_id'] && $p['period_no'] == $rt['period_no']){
					 				echo "selected";
					 				if($rt['subject_choice'] == "optional"){
					 					$tx++;
					 				}
					 			}
					 		}?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<option value="ots" <?php if($tx > 1){ echo "selected"; } ?>>Optional Technical Subjects</option>
					 </select>
					<input type="hidden" name="tuesday[]" value="Tuesday" />
					<div id="<?php echo "tue".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				<td>
					<select name="wednesday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Wednesday"; ?>" onchange="checkTeacherInTimetableWhenEditingWednesday(this.id)" required>
						<option value="free">Free Period</option>
						<?php $wx = 0; // For testing and display the word Optional Technical Subjects ?>
						<option value="ots">Optional Technical Subjects</option>
						<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php foreach ($rtw as $rt) {
					 			if($subject['subject_id'] == $rt['subject_id'] && $p['period_no'] == $rt['period_no']){
					 				echo "selected";
					 				if($rt['subject_choice'] == "optional"){
					 					$wx++;
					 				}
					 			}
					 		}?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<option value="ots" <?php if($wx > 1){ echo "selected"; } ?>>Optional Technical Subjects</option>
					 </select>
					<input type="hidden" name="wednesday[]" value="Wednesday" />
					<div id="<?php echo "wed".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				<td>
					<select name="thursday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Thursday"; ?>" onchange="checkTeacherInTimetableWhenEditingThursday(this.id)" required>
						<option value="free">Free Period</option>
						<?php $thx = 0; // For testing and display the word Optional Technical Subjects ?>
						<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php foreach ($rth as $rt) {
					 			if($subject['subject_id'] == $rt['subject_id'] && $p['period_no'] == $rt['period_no']){
					 				echo "selected";
					 				if($rt['subject_choice'] == "optional"){
					 					$thx++;
					 				}
					 			}
					 		}?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<option value="ots" <?php if($thx > 1){ echo "selected"; } ?>>Optional Technical Subjects</option>
					 </select>
					<input type="hidden" name="thursday[]" value="Thursday" />
					<div id="<?php echo "thu".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>

				<td>
					<select name="friday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Friday"; ?>" onchange="checkTeacherInTimetableWhenEditingFriday(this.id)" required>
						<option value="free">Free Period</option>
						<?php $fx = 0; // For testing and display the word Optional Technical Subjects ?>
						<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php foreach ($rtf as $rt) {
					 			if($subject['subject_id'] == $rt['subject_id'] && $p['period_no'] == $rt['period_no']){
					 				echo "selected";
					 				if($rt['subject_choice'] == "optional"){
					 					$fx++;
					 				}
					 			}
					 		}?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<option value="ots" <?php if($fx > 1){ echo "selected"; } ?>>Optional Technical Subjects</option>
					 </select>
					<input type="hidden" name="friday[]" value="Friday" />
					<div id="<?php echo "fri".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				</div>
			</tr>
			<?php $count_period_no++; ?>
		<?php endforeach; ?>
				<td></td>
				<input type="hidden" value="<?php echo $class_stream_id; ?>" name="class_stream_id">
				<td colspan="5"><input type="submit" name="submit" value="Update Timetable" id="id-submit" class="btn btn-primary form-control"></td>
		</form>
	</tbody>
</table>
</div>
<!-- <div class="form-group">
	<input type="submit" name="submit" value="Create" id="id-submit" class="btn btn-primary form-control">
</div>
</form> -->
