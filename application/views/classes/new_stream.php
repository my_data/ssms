
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('classes/add_stream', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="stream">Stream :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('stream'); ?>" type="text" name="stream" class="form-control">
				<div class="error_message_color">
					<?php echo form_error('stream'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_stream" value="Add Stream" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>