
<div class="white-area-content">
<div class="db-header clearfix">

 <h3><?php echo $title; ?></h3>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


  
  <?php echo form_open('students/assign_monitor/'.$class_stream_id); ?>
<br/>
  <div class="form-group">
    <div class="col-xs-2">
      <label>Student Names: <label>
    </div>
    <div class="col-xs-10">
      <select name="admission_no" class="form-control">
        <option value="">--Choose--</option>
        <?php foreach($students as $student): ?>
          <option value="<?php echo $student['admission_no']; ?>" 
            <?php if($student['admission_no'] == $class_monitor){
              echo "selected='true'";
            } 
            ?>><?php echo $student['student_names']; ?></option>
        <?php endforeach; ?>
      </select>
      <div class="error_message_color">
        <?php echo form_error('admission_no'); ?>
      </div>
    </div>
  </div>
  <br/><br/>
  <div align="center">

    <input type="submit" name="assign_monitor" value="Assign" class="btn btn-primary form-control"  onClick="return confirm('Save changes?')">
  
  </div> 
  <br/>    
   <?php echo form_close(); ?>

</div>