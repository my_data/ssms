
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('classes/add_class', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_id">Class Id :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('class_id'); ?>" type="text" name="class_id" class="form-control" required name="class_id" class="form-control" placeholder="C01"/>
				<div class="error_message_color">
					<?php echo form_error('class_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_name">Class Name :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('class_name'); ?>" type="text" name="class_name" class="form-control" required name="class_name" class="form-control" placeholder="Form One"/>
				<div class="error_message_color">
					<?php echo form_error('class_name'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="level">Level :</label>
			<div class="col-sm-10">
				<select name="level" class="form-control" id="level" required>
					<option value="">--Choose Level--</option>
					<option <?php echo set_select('level', "O'Level", $this->input->post('level')); ?> value="O'Level">O'Level</option>
					<option <?php echo set_select('level', "A'Level", $this->input->post('level')); ?> value="A'Level">A'Level</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('level'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_class" value="Add Class" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>