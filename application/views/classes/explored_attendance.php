
	<div class="white-area-content">
	<div class="db-header clearfix">

	<div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline"> 

	    <div class="form-group has-feedback no-margin">  
					<?php echo form_open('classes/attendance/'.$class_stream_id.'/'.$date); ?>
					  
                      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
                     </div>
                 
                 <?php echo form_close(); ?>
	</div>	

		<a href="<?php echo base_url() . 'school_attendance_report/'.date('Y-m-d'); ?>" class="btn btn-primary btn-sm">School Report</a>

	</div>
	
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td width="20%">Admission#</td>
			<td width="40%">Names</td>
			<!-- <td>Lastname</td> -->
			<!-- <td>Class</td>
			<td>Stream</td> -->
			<td width="30%">Status</td>
			<td width="10%">Action</td>
			<!-- <td>Edit</td>
			<td>Tranfer</td>
			<td>Expell</td> -->
		</tr>
	</thead>
	<tbody>
	 <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "No students found in this class";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['names']; ?></td>
				<td><?php echo $student['description']; ?></td>
				<!-- <td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td> -->
				<td align="center"><a href='<?php echo base_url() . 'students/get_student_profile/' . $student['admission_no']; ?>' class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a>
				</td>				
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>