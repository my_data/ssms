<!--<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>-->


	<?php echo form_open('classes/timetable'); ?>
		<?php $key = 0; ?>
		<?php foreach($periods as $p): ?>
			<tr>
				<td>
					<?php echo $p['period_no']; ?>
					<input type="hidden" name="period_no[]" value="<?php echo $p['period_no']; ?>" />
					
				</td>
				<div class="form-group">
				<td><?php //$key = $p['period_no']; ?>
					<input type="hidden" value="<?php //echo $key; ?>">
					<select name="monday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Monday"; ?>" onchange="checkTimetableMonday(this.id)" required>
						<option value="">--Choose--</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>" <?php //echo set_select('monday_subject[]', ''.$subject["subject_id"].'', $this->input->post('monday_subject[]')); ?>><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<?php if($lvl == "O'Level"): ?>
					 		<option value="ots">Optional Technical Subjects</option>
					 	<?php endif; ?>
					 	<option value="free">Free Period</option>
					 </select>
					<input type="hidden" name="monday[]" value="Monday" />
					<div id="<?php echo "mon".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
 				<td>
					<select name="tuesday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Tuesday"; ?>" onchange="checkTimetableTuesday(this.id)" required>
						<option value="">--Choose--</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<?php if($lvl == "O'Level"): ?>
					 		<option value="ots">Optional Technical Subjects</option>
					 	<?php endif; ?>
					 	<option value="free">Free Period</option>
					 </select>
					<input type="hidden" name="tuesday[]" value="Tuesday" />
					<div id="<?php echo "tue".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				<td>
					<select name="wednesday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Wednesday"; ?>" onchange="checkTimetableWednesday(this.id)" required>
						<option value="">--Choose--</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<?php if($lvl == "O'Level"): ?>
					 		<option value="ots">Optional Technical Subjects</option>
					 	<?php endif; ?>
					 	<option value="free">Free Period</option>
					 </select>
					<input type="hidden" name="wednesday[]" value="Wednesday" />
					<div id="<?php echo "wed".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				<td>
					<select name="thursday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Thursday"; ?>" onchange="checkTimetableThursday(this.id)" required>
						<option value="">--Choose--</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<?php if($lvl == "O'Level"): ?>
					 		<option value="ots">Optional Technical Subjects</option>
					 	<?php endif; ?>
					 	<option value="free">Free Period</option>
					 </select>
					<input type="hidden" name="thursday[]" value="Thursday" />
					<div id="<?php echo "thu".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>

				<td>
					<select name="friday_subject[]" class="form-control" id="<?php echo $p['period_no'] . '-' . $class_stream_id . '-' . "Friday"; ?>" onchange="checkTimetableFriday(this.id)" required>
						<option value="">--Choose--</option>
					 	<?php foreach($subjects as $subject): ?>
					 		<option value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
					 	<?php endforeach; ?>
					 	<?php if($lvl == "O'Level"): ?>
					 		<option value="ots">Optional Technical Subjects</option>
					 	<?php endif; ?>
					 	<option value="free">Free Period</option>
					 </select>
					<input type="hidden" name="friday[]" value="Friday" />
					<div id="<?php echo "fri".$p['period_no']; ?>" class="error_message_color timetableErrorMessage"></div>
				</td>
				</div>
			</tr>
		<?php $key++; endforeach; ?>
				<td></td>
				<input type="hidden" value="<?php echo $class_stream_id; ?>" name="class_stream_id">
				<td colspan="5"><input type="submit" name="submit" value="Create Timetable" class="btn btn-primary form-control" onclick="return confirm('Are you sure you want to create this timetable');" /></td>
		</form>
	</tbody>
</table>


<!--
SELECT COUNT(*) FROM timetable WHERE weekday="Monday" AND period_no=1 AND teacher_id = (SELECT teacher_id from teaching_assignment WHERE class_stream_id="C03E" AND subject_id="SUB01")
-->
