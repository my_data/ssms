<?php echo form_open('classes/monthly_attendance_report/'.$class_stream_id); ?>
  <div class="col-xs-3">
    <select class="form-control" name="monthValue" id="monthValue" required>
      <option value="">--Choose Month--</option>
      <option value="01-January">January</option>
      <option value="02-February">February</option>
      <option value="03-March">March</option>
      <option value="04-April">April</option>
      <option value="05-May">May</option>
      <option value="06-June">June</option>
      <option value="07-July">July</option>
      <option value="08-August">August</option>
      <option value="09-September">September</option>
      <option value="10-October">October</option>
      <option value="11-November">November</option>
      <option value="12-December">December</option>
    </select>
  </div>
    <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
  <div class="col-xs-4">
    <?php $date = date('Y-m-d'); $year = explode('-', $date)[0]; ?>
    <select class="form-control" name="yearSearch" id="yearSearch" required>
      <?php 
        for($x = $year; $x >= $year - 10; $x--){
      ?>
          <option <?php if($x === $year){ echo "selected"; } ?> value="<?php echo $x; ?>"><?php echo $x; ?></option>
      <?php
        }
      ?>      
    </select>
  </div>
  <div class="col-xs-2">
    <input type="submit" name="search_month" value="Search" class="btn btn-primary btn-sm" />
  </div>
<?php echo form_close(); ?>

<br /><br /><br />
<div id="exhaustive_table" class="table-responsive" style="font-size: 12px;">
 <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <td>Student Names</td>
        <?php
          $x = 1;
          while($x <= 31){
            echo "<td>$x</td>";
            $x++;
          }
        ?>
        <td>Avg</td>
      </tr>
    </thead>
    <tbody>
    <?php if($flag == TRUE): ?>
      <td colspan="32">
          <?php
              $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : " Select a Month to search for a report";
              echo $message;
          ?>
      </td>
    <?php else: ?>
      <?php foreach($monthly_attendance_records AS $monthly_attendance_record): ?>
        <tr>
          <td>
            <?php
              echo $monthly_attendance_record['firstname'] . " " . $monthly_attendance_record['lastname'];
            ?>            
          </td>
          <td><?php echo $monthly_attendance_record['1'] ?></td>
          <td><?php echo $monthly_attendance_record['2'] ?></td>
          <td><?php echo $monthly_attendance_record['3'] ?></td>
          <td><?php echo $monthly_attendance_record['4'] ?></td>
          <td><?php echo $monthly_attendance_record['5'] ?></td>
          <td><?php echo $monthly_attendance_record['6'] ?></td>
          <td><?php echo $monthly_attendance_record['7'] ?></td>
          <td><?php echo $monthly_attendance_record['8'] ?></td>
          <td><?php echo $monthly_attendance_record['9'] ?></td>
          <td><?php echo $monthly_attendance_record['10'] ?></td>
          <td><?php echo $monthly_attendance_record['11'] ?></td>
          <td><?php echo $monthly_attendance_record['12'] ?></td>
          <td><?php echo $monthly_attendance_record['13'] ?></td>
          <td><?php echo $monthly_attendance_record['14'] ?></td>
          <td><?php echo $monthly_attendance_record['15'] ?></td>
          <td><?php echo $monthly_attendance_record['16'] ?></td>
          <td><?php echo $monthly_attendance_record['17'] ?></td>
          <td><?php echo $monthly_attendance_record['18'] ?></td>
          <td><?php echo $monthly_attendance_record['19'] ?></td>
          <td><?php echo $monthly_attendance_record['20'] ?></td>
          <td><?php echo $monthly_attendance_record['21'] ?></td>
          <td><?php echo $monthly_attendance_record['22'] ?></td>
          <td><?php echo $monthly_attendance_record['23'] ?></td>
          <td><?php echo $monthly_attendance_record['24'] ?></td>
          <td><?php echo $monthly_attendance_record['25'] ?></td>
          <td><?php echo $monthly_attendance_record['26'] ?></td>
          <td><?php echo $monthly_attendance_record['27'] ?></td>
          <td><?php echo $monthly_attendance_record['28'] ?></td>
          <td><?php echo $monthly_attendance_record['29'] ?></td>
          <td><?php echo $monthly_attendance_record['30'] ?></td>
          <td><?php echo $monthly_attendance_record['31'] ?></td>
          <td><?php echo $monthly_attendance_record['avg'] . '/' . $monthly_attendance_record['total']; ?></td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
  
</div>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>
