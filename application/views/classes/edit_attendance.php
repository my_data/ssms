

  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
            
                <?php echo form_open('classes/edit_attendance/'.$class_stream_id.'/'.$date_); ?>
               
        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
        </div>

                       <?php echo form_close(); ?>
                 
              </div>
            </div>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr>
			<th>Student Names</th>
			<th>Description</th>
      <th>Options</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
    <?php if ($attendance_records == FALSE): ?>
        <tr>
          <td colspan="3">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach($attendance_records as $ar): ?>
          <tr>
            <td><?php echo $ar['firstname'] . " " . $ar['lastname']; ?></td>
            <td><?php echo $ar['description']; ?></td>
            <td>
              <?php echo form_open('classes/edit_attendance/'.$ar['class_stream_id'].'/'.$ar['date_']); ?>
              <select name="att_id" class="form-control">
                <option value="">--Select--</option>
                <?php foreach($attendance_types as $at): ?>
                  <option value="<?php echo $at['att_id']; ?>"><?php echo $at['description']; ?></option>
                <?php endforeach; ?>
              </select>
            </td>
            <td>
              <input type="hidden" name="admission_no" value="<?php echo $ar['admission_no']; ?>" />
              <input type="submit" name="submit" class="form-control btn btn-primary" value="UPDATE" />&nbsp;
              <?php echo form_close(); ?>
            </td>
          </tr>
      <?php endforeach; ?>
    <?php endif; ?>
	</tbody>
</table>
<!-- <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>
 -->
