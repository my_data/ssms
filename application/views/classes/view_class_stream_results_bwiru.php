
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

<div class="form-group has-feedback no-margin">      
<?php if($results_by_grade === "OK"): ?>
  <?php echo form_open('classes/results_by_grade/'.$class_stream_id.'/'.$term_id); ?>
<?php endif; ?>
<?php if($results_by_division === "OK"): ?>
  <?php echo form_open('classes/results_by_division/'.$class_stream_id.'/'.$term_id); ?>
<?php endif; ?>
  
                      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn" aria-haspopup="true" aria-expanded="false">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
                     </div>
                
  
<?php echo form_close(); ?>
</div>

        <a href="<?php echo base_url() . 'classes/results_by_grade/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-sm btn-primary" title="Compute by Grade" data-placement="bottom" data-toggle="tooltip">Grade</a>
        <a href="<?php echo base_url() . 'classes/results_by_division/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-sm btn-primary" title="Compute by Division" data-placement="bottom" data-toggle="tooltip">Division</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if($results_by_grade === "OK"): ?>
          <a href="<?php echo base_url() . 'students/print_loop_by_grade_bwiru/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-xs btn-primary glyphicon glyphicon-print" title="Print All Results" data-placement="bottom" data-toggle="tooltip"></a>
        <?php endif; ?>
        <?php if($results_by_division === "OK"): ?>
          <a href="<?php echo base_url() . 'students/print_loop_by_division_bwiru/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-xs btn-primary glyphicon glyphicon-print" title="Print All Results" data-placement="bottom" data-toggle="tooltip"></a>
        <?php endif; ?>

</div>

</div>
<div class="form-group">
	<?php if($this->session->flashdata('error_message')): ?>
		<div class="alert alert-dismissible alert-danger text align-center">
			<?php echo $this->session->flashdata('error_message'); ?>
		</div>
	<?php endif; ?>
</div>
<?php if($results_by_grade === "OK"): ?>
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Grade</td>
      <?php foreach($grade_system as $gs): ?>
        <td><?php echo $gs["grade"]; ?></td>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
     <tr>
        <td>NO. STUDENTS</td>
        <?php foreach($grade_system as $gs): ?>
          <?php $x = 0; ?>
          <?php foreach($grade_groups as $gg): ?>
            <?php if($gg['grade'] === $gs['grade']): ?>
              <td><?php echo $gg['count']; ?></td>
            <?php else: ?>
              <?php $x++; ?>
              <?php if($x == count($grade_groups)): ?>
                <td><?php echo "0"; ?></td>
              <?php endif; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
    <tr>
  </tbody>
</table>
<?php endif; ?>

<?php if($results_by_division === "OK"): ?>
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Division</td>
      <?php foreach($divNames as $dN): ?>
        <td><?php echo $dN["div_name"]; ?></td>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
     <tr>
      <td>NO. STUDENTS</td>
      <?php foreach($divNames as $dN): ?>
        <?php $x = 0; ?>
        <?php foreach($whatever as $wt): ?>
          <?php if($wt['0'] === $dN['div_name']): ?>
            <td><?php echo $wt['1']; ?></td>
          <?php else: ?>
            <?php $x++; ?>
            <?php if($x == count($whatever)): ?>
              <td><?php echo "0"; ?></td>
            <?php endif; ?>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endforeach; ?>
    <tr>
  </tbody>
</table>
<?php endif; ?>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Admission#</td>
      <td>Firstname</td>
      <td>Lastname</td>
      <td>Action</td>
    </tr>
  </thead>
 <tbody>
    <?php if ($mystudents == FALSE): ?>
      <tr>
        <td colspan="4">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this class";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
    <?php foreach($mystudents as $mystudent): ?>
      <tr>
        <td><?php echo $mystudent['admission_no']; ?></td>
        <td><?php echo $mystudent['firstname']; ?></td>
        <td><?php echo $mystudent['lastname']; ?></td>
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <?php if($results_by_grade === "OK"): ?>
          	<a href="<?php echo base_url() . 'students/print_results_by_grade/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
            &nbsp;&nbsp;
          <a href="<?php echo base_url() . 'students/result_b/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Preview Results"><span class="fa fa-clone"></span></a> 
          <?php endif; ?>
          <?php if($results_by_division === "OK"): ?>
          	<a href="<?php echo base_url() . 'students/print_results_by_division/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
            &nbsp;&nbsp;
          <a href="<?php echo base_url() . 'students/result_b/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Preview Results"><span class="fa fa-clone"></span></a> 
          <?php endif; ?>
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'classes/get_students_by_stream/'.$class_stream_id.''; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>

  <div align="right">
    <?php echo $links; ?>
  </div> 
