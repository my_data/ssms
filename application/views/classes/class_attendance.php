
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              
                <?php echo form_open('classes/class_attendance'); ?>
                   
        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_class" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
        </div>
                    
                          <?php echo form_close(); ?>
              
            </div>

            <!-- <a href="<?php echo base_url() . 'admin/add_class_stream'; ?>" class="btn btn-primary btn-sm">Add Class Stream</a>
 -->
          </div>
      </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>



<table class="table table-responsive table-striped table-hover table-condensed table-bordered">

		<thead>
			<tr class="table-header">
				<td>Class Name</td>
				<td>Stream</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<?php
				if ($data)
				{
					foreach ($data as $row){

			?>
			<tr>
				<td><?php echo $row['class_name']; ?></td>	
				<td><?php echo $row['stream']; ?></td>			
				<td>
					<a href="<?php echo base_url() . 'classes/attendance_report/'.$row['class_stream_id']; ?>" class="btn btn-primary btn-xs" title="View Attendance" data-placement="bottom"><span class="fa fa-eye"></span></a>&nbsp;
				</td>
			</tr>
			<?php
			}
		}
			?>
		</tbody>

	</table>
  <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>