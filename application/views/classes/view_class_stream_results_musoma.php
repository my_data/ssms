
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">


<?php echo form_open('classes/students_by_stream/'.$class_stream_id.'/'.$term_id); ?>

                      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn" aria-haspopup="true" aria-expanded="false">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
                     </div>
<?php echo form_close(); ?>
       
</div>

  <a href="<?php echo base_url() . 'students/print_students_results/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-sm btn-primary">Print All Results</a>

</div>

</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Admission#</td>
      <td>Firstname</td>
      <td>Lastname</td>
      <td>Action</td>
    </tr>
  </thead>
 <tbody>
      <?php if ($mystudents == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this class";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php foreach($mystudents as $mystudent): ?>
      <tr>
        <td><?php echo $mystudent['admission_no']; ?></td>
        <td><?php echo $mystudent['firstname']; ?></td>
        <td><?php echo $mystudent['lastname']; ?></td>
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo base_url() . 'students/print_results/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print Results"><span class="glyphicon glyphicon-print"></span></a>&nbsp;&nbsp;
          <a href="<?php echo base_url() . 'students/result/' . $mystudent['admission_no'].'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Preview Results"><span class="fa fa-clone"></span></a>
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'classes/get_students_by_stream/'.$class_stream_id; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div> 