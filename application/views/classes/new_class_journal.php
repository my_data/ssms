
<div class="white-area-content">
<div class="db-header clearfix">

 <h3><img src="<?php echo base_url();?>/assets/images/jounal.jpg" class="img-responsive pull-left gap-right " alt="..."><?php echo $title; ?></h3>
    
</div>
<div class="error_message_color">
	<?php echo validation_errors(); ?>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open(); ?>
	<div class="form-group">

		<div class="col-xs-4">
		  <h4><?php echo $title . "	" . $this->input->post('day'); ?></h4>
		<?php $attributes = array('id' => 'class_journal_form'); ?>

		</div>

		<div class="col-xs-3">
			<select name="day" class="form-control" required>
				<option value="">--Select Day--</option>
				<option value="Monday">Monday</option>
				<option value="Tuesday">Tuesday</option>
				<option value="Wednesday">Wednesday</option>
				<option value="Thursday">Thursday</option>
				<option value="Friday">Friday</option>
			</select>
			
		</div>

		<div class="col-xs-5">
			<input type="submit" name="load" value="Load" class="btn btn-primary " onclick="" />
		</div>

	</div>
	
<?php echo form_close(); ?>

<br/><br/><br/>
<div id="class_journal">
<?php echo form_open('classes/load_class_journal/'.$class_stream_id, $attributes); ?><!--START INSIDE FORM-->
<?php //echo form_open('classes/save_class_journal/', $attributes); ?><!--START INSIDE FORM-->
	<input type="hidden" name="weekday" value="<?php echo $this->input->post('day'); ?>" />
	<input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
	
	<table>
	<tr>
		<td align="center" width="60"><b>Date :</b></td>
		<td width="40"><input class="form-control" type="date" name="date_" id="date_" required/></td>
	</tr>
	</table>
	<br/>
	<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover table-condensed" >
			<thead>
				<tr class="table-header">
					<td width="5%">Period#</td>
					<!-- <th width="5%">Time</th> -->
					<td width="25%">Subject</td>
					<td width="25%">Subject Teacher</td>
					<td width="15%" align="center">Topic Taught</td>
					<td width="20%" align="center">Sub Topic</td>
					<td width="10%" align="center">Abs</td>
					<!-- <th width="5%">Action</th> -->
				</tr>
			</thead>
			<tbody>
			<?php if ($class_journal_records == FALSE): ?>
		      <tr>
		        <td colspan="6">
		              <?php
		                  $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "Load the weekday to add class journal record";
		                  echo $message;
		              ?>
		          </td>
		      </tr>
		    <?php else: ?>
				<?php $i = 1; ?>
				<?php foreach($class_journal_records as $record): ?>
<?php if($record['subject_choice'] === "optional"): ?>
	<?php $i = 1; //For echoing period_no only once ?>
	<?php foreach($optional_subjects as $os): ?>
		<tr>			
					<td align="center" contenteditable="false" class="period_no">
						<?php if($i === 1){ echo $record['period_no']; } $i++; ?>
						<input type="hidden" name="period_no[]" value="<?php echo $record['period_no']; ?>">
						<input type="hidden" name="t_id[]" value="<?php echo $record['t_id']; ?>">
					</td>
					<td contenteditable="false" class="subject">
						<?php if($record['subject_id'] == ""): ?>
							Free Period
						<?php else: ?>
							<?php echo $os['subject_name']; ?>
							<input type="hidden" name="subject_id[]" value="<?php echo $record['subject_name']; ?>">
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="subject_teacher">
						<?php if($record['subject_teacher'] == ""): ?>
							None
						<?php else: ?>
							<?php echo $os['firstname'] . " " . $os['lastname']; ?>
						<input type="hidden" name="assignment_id[]" value="<?php echo $record['assignment_id']; ?>">
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="topic_taught">
						<?php if($record['subject_id'] == ""): ?>
							<select class="form-control" name="status[]" readonly>
								<option value="none">--None--</option>
							</select>
						<?php else: ?>
							<select class="form-control" name="status[]" id="status<?php echo $i; ?>">
								<option value="">--Select--</option>
								<option <?php echo set_select('status[]', 'taught', $this->input->post('status[]')); ?> value="taught">Taught</option>
								<option <?php echo set_select('status[]', 'untaught', $this->input->post('status[]')); ?> value="untaught">NOT Taught</option>
							</select>
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="sub_topic">
						<?php if($os['subject_id'] == ""): ?>
								<select class="form-control" name="subtopic[]" id="subtopic<?php echo $i; ?>" readonly>
								<option value="0">--None--</option>
							</select>
						<?php else: ?>
							<select class="form-control" name="subtopic[]" id="subtopic<?php echo $i; ?>">
								<!-- <option value="">--Select--</option> -->
								<option value="0">--None--</option>
								<?php foreach ($subtopics as $subtopic): ?>
								<?php
									if($subtopic['subject_id'] == $os['subject_id']){
								?>
										<option value='<?php echo $subtopic['subtopic_id']; ?>' <?php echo  set_select('subtopic[]', ''.$subtopic["subtopic_id"].'', $this->input->post('subtopic[]')); ?>>
											<?php echo $subtopic['subtopic_name']; ?>
										</option>"
								<?php
									}
								?>						
								<?php endforeach; ?>
							</select>
						<?php endif; ?>							
					</td>
					<td align="center">
						<?php if($record['subject_id'] == ""): ?>
							None
						<?php else: ?>
							<input class="form-control" type="number" name="absentees[]" style="width: 50px"; id="abs<?php echo $i; ?>" min="0" />
						<?php endif; ?>
					</td>
				</tr>
	<?php endforeach; ?>
<?php else: ?>
	<tr>			
					<td align="center" contenteditable="false" class="period_no">
						<?php echo $record['period_no']; ?>
						<input type="hidden" name="period_no[]" value="<?php echo $record['period_no']; ?>">
						<input type="hidden" name="t_id[]" value="<?php echo $record['t_id']; ?>">
					</td>
					<!-- <td contenteditable="false" class="time">
						<?php echo $record['weekday']; ?>
						<input type="hidden" name="weekday[]" value="<?php echo $record['weekday']; ?>">
					</td> -->
					<td contenteditable="false" class="subject">
						<?php if($record['subject_id'] == ""): ?>
							Free Period
						<?php else: ?>
							<?php echo $record['subject_name']; ?>
							<input type="hidden" name="subject_id[]" value="<?php echo $record['subject_name']; ?>">
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="subject_teacher">
						<?php if($record['subject_teacher'] == ""): ?>
							None
						<input type="hidden" name="assignment_id[]" value="iko_null" />
						<?php else: ?>
							<?php echo $record['subject_teacher']; ?>
						<input type="hidden" name="assignment_id[]" value="<?php echo $record['assignment_id']; ?>">
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="topic_taught">
						<?php if($record['subject_id'] == ""): ?>
						<select class="form-control" name="status[]" readonly>
							<option value="none">--None--</option>
						</select>
						<?php else: ?>
							<select class="form-control" name="status[]" id="status<?php echo $i; ?>">
								<option value="">--Select--</option>
								<option <?php echo set_select('status[]', 'taught', $this->input->post('status[]')); ?> value="taught">Taught</option>
								<option <?php echo set_select('status[]', 'untaught', $this->input->post('status[]')); ?> value="untaught">NOT Taught</option>
							</select>
						<?php endif; ?>
					</td>
					<td contenteditable="false" class="sub_topic">
						<?php if($record['subject_id'] == ""): ?>
						<select class="form-control" name="subtopic[]" id="subtopic<?php echo $i; ?>" readonly>
							<option value="0">--None--</option>
						</select>
						<?php else: ?>
							<select class="form-control" name="subtopic[]" id="subtopic<?php echo $i; ?>">
								<!-- <option value="">--Select--</option> -->
								<option value="0">--None--</option>
								<?php foreach ($subtopics as $subtopic): ?>
								<?php
									if($subtopic['subject_id'] == $record['subject_id']){
								?>
										<option value='<?php echo $subtopic['subtopic_id']; ?>' <?php echo  set_select('subtopic[]', ''.$subtopic["subtopic_id"].'', $this->input->post('subtopic[]')); ?>>
											<?php echo $subtopic['subtopic_name']; ?>
										</option>"
								<?php
									}
								?>						
								<?php endforeach; ?>
							</select>
						<?php endif; ?>							
					</td>
					<td align="center">
						<?php if($record['subject_id'] == ""): ?>
							None
						<input type="hidden" name="absentees[]" value="0" />
						<?php else: ?>
							<input class="form-control" type="number" name="absentees[]" style="width: 50px"; id="abs<?php echo $i; ?>" min="0" />
						<?php endif; ?>
					</td>
				</tr>
<?php endif; ?>
				
				<?php $i++; endforeach; ?>
			</tbody>
		</table>
	<div>
	<br/>

	<div align="center" id="save_button" >
		<input type="submit" name="save" value="Save" id="save" class="btn btn-primary form-control" />
	</div>
	<br/>

</div> <!-- END OF CLASS_JOURNAL DIV-->
<?php endif; //The save button should not be displayed if there are class journal records?>
	</form>