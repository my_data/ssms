

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('classes/edit_class_stream/'.$class_stream_id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_id">Class Name :</label>
			<div class="col-sm-10">
				<!-- <select name="class_id" class="form-control" id="class_id" required onchange="concatenate('class_id', 'stream_id_name');" >
					<?php foreach($classes as $class): ?>
						<option <?php if($class['class_id'] === $class_stream['class_id']){ echo "selected='true'"; } ?> value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
					<?php endforeach; ?>
				</select> -->
				<input type="text" name="class_id" value="<?php echo $class_stream['class_name']; ?>" class="form-control" readonly />
				<div class="error_message_color">
					<?php echo form_error('class_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="stream">Stream :</label>
			<div class="col-sm-10">
				<!-- <select name="stream_id_name" class="form-control" id="stream_id_name" required onchange="concatenate('class_id', 'stream_id_name');" >
					<?php foreach($streams as $stream): ?>
						<option <?php if($stream['stream_id'] === $class_stream['stream_id']){ echo "selected='true'"; } ?> value="<?php echo $stream['stream_name'] ."-". $stream['stream_id']; ?>"><?php echo $stream['stream_name']; ?></option>
					<?php endforeach; ?>
				</select> -->
				<input type="text" name="class_stream_id" value="<?php echo $class_stream['stream']; ?>" class="form-control" readonly />
				<div class="error_message_color">
					<?php echo form_error('class_stream_id'); ?>
				</div>
			</div>
		</div>
		<input type="hidden" name="class_stream_id" id="concatenate_csid" value="" />
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="description">Description :</label>
			<div class="col-sm-10">
				<input type="text" name="description" class="form-control" value="<?php echo $class_stream['description']; ?>">
				<div class="error_message_color">
					<?php echo form_error('description'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="capacity">Capactity :</label>
			<div class="col-sm-10">
				<input type="number" name="capacity" class="form-control" min="0" value="<?php echo $class_stream['capacity']; ?>">
				<div class="error_message_color">
					<?php echo form_error('capacity'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="nos">No Of Subjects :</label>
			<div class="col-sm-10">
				<input value="<?php echo $class_stream['number_of_subjects']; ?>" type="number" name="nos" class="form-control" min="3">
				<div class="error_message_color">
					<?php echo form_error('nos'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<!-- <input type="hidden" name="old_class_stream_id" class="form-control" min="0" value="<?php echo $class_stream['class_stream_id']; ?>"> -->
			<input type="hidden" name="class_stream_id" class="form-control" min="0" value="<?php echo $class_stream['class_stream_id']; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_class_stream" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>