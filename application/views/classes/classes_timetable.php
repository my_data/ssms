

  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                 <div class="form-group has-feedback no-margin">
                <div class="text-right"></div>
                <?php echo form_open('classes/classes_timetable'); ?>
                    
                      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_class" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
                     </div>
                  
                          <?php echo form_close(); ?>
              
            </div>

            <a href="<?php echo base_url() . 'classes/create_new_timetable'; ?>" class="btn btn-primary btn-sm">Add New Timetable</a>
          </div>
      </div>




<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <td width="47%">Class</td>
        <td width="40%">Stream</td>
        <?php if($this->session->userdata('manage_timetable') == 'ok' || $this->session->userdata('view_class_timetable') == 'ok'): ?>
          <td width="13%" align="center">Action&nbsp;
              <?php if($this->session->userdata('manage_timetable') == 'ok'): ?>
                <a href="<?php echo base_url() . 'classes/erase_timetable' ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Erase The Entire Timetable" data-placement="bottom"><span class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure you want to erase the entire time? This action cannot be undone');"></span></a>
              <?php endif; ?>
          </td>
        <?php endif; ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach($class_streams as $cs): ?>
          <tr>
            <td><?php echo $cs['class_name']; ?></td>
            <td><?php echo $cs['stream']; ?></td>
            <?php if($this->session->userdata('manage_timetable') == 'ok' || $this->session->userdata('view_class_timetable') == 'ok'): ?>
              <td align="center">
                <a href="<?php echo base_url() . 'classes/get_class_stream_timetable/'.$cs['class_stream_id']; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" title="View Timetable" data-placement="bottom"><span class="fa fa-eye"></span></a>&nbsp;
                <?php if($this->session->userdata('manage_timetable') == 'ok'): ?>
                  <a href="<?php echo base_url() . 'classes/edit_timetable/'.$cs['class_stream_id']; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit Timetable" data-placement="bottom"><span class="fa fa-pencil"></span></a>&nbsp;
                  <a href="<?php echo base_url() . 'classes/erase_timetable/'.$cs['class_stream_id']; ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete <?php echo $cs['class_name'] . " " . $cs['stream']; ?> Timetable" data-placement="bottom"><span class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure you want to erase the <?php echo $cs['class_name'] . " " . $cs['stream']; ?>? This action cannot be undone');"></span></a>&nbsp;
                <?php endif; ?>
              </td>
            <?php endif; ?>
          </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
 </div>     
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes/classes_timetable'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
