<div class="white-area-content">
<div class="db-header clearfix">
<div class="page-header-title"><?php echo $title;?></div>
<h3 align="center"><?php echo  $this->input->post('class_name') . $this->input->post('stream'); ?></h3><br />
</div>

<div id="errorMessage" class="error_message_color">
	<?php //echo validation_errors();	?>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

&nbsp;

<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th width="10%">Period#</th>
			<th width="18%">Monday</th>
			<th width="18%">Tuesday</th>
			<th width="18%">Wednesday</th>
			<th width="18%">Thursday</th>
			<th width="18%">Friday</th>
		</tr>
	</thead>
	<tbody>
		<!-- <div id="successMessage" class="success_message_color">
			<?php 
				if($this->session->flashdata('success_message')){
					echo $this->session->flashdata('success_message');
				}
			?>
		</div>
		<div id="errorMessage" class="error_message_color">
			<?php
				if($this->session->flashdata('error_message')){
					echo $this->session->flashdata('error_message');
				}
			?>
		</div>
		<div id="warningMessage" class="error_message_color">
			<?php
				if($this->session->flashdata('exist_message')){
					echo $this->session->flashdata('exist_message');
				}
			?>
		</div> -->
		<?php echo form_open('classes/create_new_timetable'); ?>
			<div class="form-group">
				<!-- Codes to populate the select elements -->
				<div class="col-md-3">
					<select name="class_name" id="class_name" class="form-control" onchange="concatenate_for_filter(this.id, 'stream');" <?php if($disable_elements == "yeah"){ echo "disabled"; } ?>>
						<option value="">--Choose Class--</option>
						<?php foreach($classes as $class_level): ?>
							<!-- 
								https://www.youtube.com/watch?v=UliJeDbc4cw 
							-->
							<option <?php echo set_select('class_name', $class_level['class_id'], $this->input->post('class_name')); ?> value="<?php echo $class_level['class_id']; ?>">
								<?php echo $class_level['class_name']; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-3">
					<input type="hidden" id="class_name_value" />
					<select name="stream" id="stream" class="form-control" onchange="concatenate_for_filter('class_name', this.id);" <?php if($disable_elements == "yeah"){ echo "disabled"; } ?>>
						<option value="">--Choose Stream--</option>
						<?php foreach($streams as $stream): ?>
							<option <?php echo set_select('stream', $stream['stream_name'], $this->input->post('stream')); ?>stream value="<?php echo $stream['stream_name']; ?>">
								<?php echo $stream['stream_name']; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-3">
					<input type="hidden" id="csid" name="csid" value="<?php echo set_value('csid'); ?>" />
					<input type="hidden" id="stream_name_value" />
					<input type="hidden" name="allowed_to_create_timetable" value="yes">
					<input type="submit" name="filter" value="Create New Timetable" class="btn btn-primary"  <?php if($disable_elements == "yeah"){ echo "disabled"; } ?>/>
					<!-- END OF populating the select elements -->
				</div>
			</div><br/><br/>
		<?php echo form_close(); ?>
