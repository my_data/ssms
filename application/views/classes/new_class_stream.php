
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'/*, 'onSubmit' => 'return validate_new_class()'*/); ?>
	<?php echo form_open('classes/add_class_stream', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_id">Class Name :</label>
			<div class="col-sm-10">
				<select name="class_id" class="form-control" id="class_id" required onchange="concatenate('class_id', 'stream_id_name');">
					<option value="">--Choose Class--</option>
					<?php foreach($classes as $class): ?>
						<option <?php echo set_select('class_id', $class['class_id'], $this->input->post('class_id')); ?> value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('class_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="stream">Stream :</label>
			<div class="col-sm-10">
				<select name="stream_id_name" class="form-control" id="stream_id_name" required onchange="concatenate('class_id', 'stream_id_name');" >
					<option value="">--Choose Stream--</option>
					<?php foreach($streams as $stream): ?>
						<option <?php //echo set_select('stream_id_name', $stream['stream_name']."-". $stream['stream_id'], $this->input->post('stream_id_name')); ?> value="<?php echo $stream['stream_name']."-". $stream['stream_id']; ?>"><?php echo $stream['stream_name']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('class_stream_id'); ?>
				</div>
			</div>
		</div>
		<input type="hidden" name="class_stream_id" id="concatenate_csid" value="" />
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="description">Description :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('description'); ?>" type="text" name="description" class="form-control">
				<div class="error_message_color">
					<?php echo form_error('description'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="capacity">Capactity :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('capacity'); ?>" type="number" name="capacity" class="form-control" min="0">
				<div class="error_message_color">
					<?php echo form_error('capacity'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="nos">No Of Subjects :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('nos'); ?>" type="number" name="nos" class="form-control" min="3">
				<div class="error_message_color">
					<?php echo form_error('nos'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_class_stream" value="Add Class Stream" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>