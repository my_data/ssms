<!-- <div class="col-md-9 add_score" id="content"> -->
<?php if($by_grade == "OK"): ?>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Grade</td>
     <?php foreach($grade_system as $gs): ?>
        <td><?php echo $gs["grade"]; ?></td>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
     <tr>
        <td>NO. STUDENTS</td>
        <?php foreach($grade_system as $gs): ?>
          <?php $x = 0; ?>
          <?php foreach($grade_groups as $gg): ?>
            <?php if($gg['grade'] === $gs['grade']): ?>
              <td><?php echo $gg['count']; ?></td>
            <?php else: ?>
              <?php $x++; ?>
              <?php if($x == count($grade_groups)): ?>
                <td><?php echo "0"; ?></td>
              <?php endif; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
    <tr>
  </tbody>
</table>
</div>
<?php endif; ?>

<?php if($by_divison == "OK"): ?>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Division</td>
      <?php foreach($divNames as $dN): ?>
        <td><?php echo $dN["div_name"]; ?></td>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
     <tr>
      <td>NO. STUDENTS</td>
      <?php foreach($divNames as $dN): ?>
        <?php $x = 0; ?>
        <?php foreach($whatever as $wt): ?>
          <?php if($wt['0'] === $dN['div_name']): ?>
            <td><?php echo $wt['1']; ?></td>
          <?php else: ?>
            <?php $x++; ?>
            <?php if($x === count($whatever)): ?>
              <td><?php echo "0"; ?></td>
	    <?php //$x++; ?>
            <?php endif; ?>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endforeach; ?>
    <tr>
  </tbody>
</table>
</div>
<?php endif; ?>

<?php
  $A = 0; $B_PLUS = 0; $B = 0; $C = 0; $D = 0; $F = 0;
?>

<?php //echo form_open('classes/position'); ?>
  <input type="hidden" name="class_id" value="<?php echo $class_id; ?>" />
  <input type="hidden" name="term_id" value="<?php echo $term_id; ?>">

<div id="exhaustive_table" class="table table-responsive" style="font-size: 12px">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Student Names</td>
			<?php foreach($subjects as $subject): ?>
				<td><?php echo $subject['subject_name']; ?></td>
			<?php endforeach; ?>
			<?php
					//Codes for rendering the table header values
					/*$urefu = 0;
					foreach ($subjects as $key => $value) {
						if(strlen($value['subject_name']) > $urefu){
							$urefu = strlen($value['subject_name']);
						}
					}

					echo "<br/><br/>";


					$x = "";
					$word = "";
					for ($a = 0; $a < $urefu; $a++) {
					echo "<tr class='table-header'>";
				if($a+1 == $urefu){ echo "<td>Student Names</td>"; }else{ echo "<td></td>"; }
						foreach ($subjects as $key => $row) {
							$sub_name = $row['subject_name'];
							if(substr($row['subject_name'], $a, 1) == ""){
								$word = "*";
								echo "<td>" . $word . "</td>";
							}
							else{
								$word =  substr($row['subject_name'], $a, 1);
								echo "<td>" . $word . "</td>";
							}					
						}
						$x .= $word . ' - ';
						$word = "";

					}
*/
				//END rendering CODES
			?>
		<?php if($by_grade == "OK"): ?>
			<td>Total</td>
		    <td>AVG</td>
		    <td>Grade</td>
		<?php endif; ?>
		<?php if($by_divison == "OK"): ?>
			<td>Division</td>
		    <td>Points</td>
		<?php endif; ?>
			<td>Pos</td>
		    <td align="center">Action</td>
		</tr>
	</thead>
	<tbody>
		<?php if ($records == FALSE || $students == FALSE): ?>
	    	<tr>
		    	<td rowspan="<?php echo $no_of_subjects; ?>">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are No Results for this term";
                        echo $message;
                    ?>
                </td>
		    </tr>
		<?php else: ?>
		<?php foreach($students as $student): ?>
			<tr>
				<td><?php echo $student['firstname'] . " " . $student['lastname']; ?></td>
				<?php foreach($subjects as $subject): ?>
					<?php $x = 0; ?>
						<?php foreach($records as $key => $row): ?>
							<?php if($row['admission_no'] === $student['admission_no']): ?>
								<?php if($subject['subject_id'] === $row['subject_id']): ?>
									<td><?php echo $row['average']; ?></td>
								<?php else: ?>
									<?php $x++; ?>
									<?php if($x == $student['number_of_subjects']): ?>
										<td></td>
									<?php endif; ?>
								<?php endif; ?>
								
							<?php endif; ?>
						<?php endforeach; ?>
				<?php endforeach; ?>
				<?php if($by_grade == "OK"): ?>
					<td><?php echo $student['total']; ?></td>
			    	<td><?php echo $student['average']; ?></td>
			    	<td>
			          <?php foreach($grade_system as $gs): ?>
			            <?php
			              if($gs['start_mark'] <= $student['average'] && $student['average'] <= $gs['end_mark']){
			                echo $gs['grade'];
			            ?>
			              <input type="hidden" name="grade[]" value="<?php echo $gs['grade']; ?>" />
			            <?php
			              }
			            ?>
			          <?php endforeach; ?>
			        </td>
			    <?php endif; ?>
			    <?php if($by_divison == "OK"): ?>
			    	<td>
			          	<?php foreach($xyz as $d): ?>
			          		<?php if($d['adm_no'] === $student['admission_no']): ?>
			          			<?php echo $d['division_name']; ?>
			          		<?php endif; ?>
			          	<?php endforeach; ?>
			        </td>
			        <td>
			        	<?php foreach($xyz as $d): ?>
			        		<?php if($d['adm_no'] === $student['admission_no']): ?>
			        			<?php echo $d['points']; ?>
			          		<?php endif; ?>
			          	<?php endforeach; ?>
			        </td>
			    <?php endif; ?>
		        <?php if($student['average'] != ""): //Restrict from echoing the position and print buttons ?>
		        <td>
		          <?php foreach($mkeka as $pos): ?>
		            <?php
		              if($pos['admission_no'] === $student['admission_no']){
		                echo $pos['rank'];
		              }
		            ?>
		          <?php endforeach; ?>
		        </td>
		        <td align="center">
		    
		         <?php //if($this->session->userdata('user_role') === "admin"): ?>
		           <!-- <a href="<?php echo base_url() . 'admin/print_results/'.$student['admission_no'].'/'.$class_id.'/'.$term_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Results Musoma"><span class="glyphicon glyphicon-print"></span></a>&nbsp; -->
		           <?php if($by_grade == "OK"): ?>
		           <a href="<?php echo base_url() . 'students/print_results_by_grade/'.$student['admission_no'].'/'.$term_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Results By Grade"><span class="glyphicon glyphicon-print"></span></a>
		           <?php endif; ?>&nbsp;
		           <?php if($by_divison == "OK"): ?>
		           <a href="<?php echo base_url() . 'students/print_results_by_division/'.$student['admission_no'].'/'.$term_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Results By Division"><span class="glyphicon glyphicon-print"></span></a>&nbsp;
		           <?php endif; ?>
		         <?php //endif; ?>
		         <?php //if($this->session->userdata('academic_role_active') === "on"): ?>
		           <!-- <a href="<?php echo base_url() . 'students/print_results/'.$student['admission_no'].'/'.$term_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Results"><span class="glyphicon glyphicon-print"></span></a> -->
		         <?php endif; ?>
		    <?php //endif; // End Restricting from echoing position and print buttons ?>
		        </td>
			</tr>
			<input type="hidden" name="class_stream_id[]" value="<?php echo $student['class_stream_id']; ?>" />
	      <input type="hidden" name="admission_no[]" value="<?php echo $student['admission_no']; ?>" />
	      <input type="hidden" name="average[]" value="<?php echo $student['average']; ?>" />
		<?php endforeach; ?>
	<?php endif; //End of testing if records zipo ?>
		<?php //if($this->session->userdata('academic_role_active') === "on"): ?>
	        <!-- <input type="submit" name="compute_class_position" value="Compute" class="btn btn-success btn-xs" /> -->
	      <?php //endif; ?>
	</tbody>
</table>

<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>

<?php echo form_close(); //Close the score/position form open ?>

    <div align="right">
      <?php echo $links; ?>      
    </div>
  </div>
</div>

