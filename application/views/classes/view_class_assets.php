
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
	</div>
	<div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
<?php echo form_open('classes/assets/'.$class_stream_id); ?>
				
	<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_asset" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
    </div>

<?php echo form_close(); ?>
				
	</div>
		<?php if($this->session->userdata('manage_class_assets') == "ok"): ?>
			<a href="#" data-target="#addAssetModal<?php echo $class_stream_id; ?>" data-toggle="modal" class="btn btn-primary btn-sm">Add New Asset</a>
		<?php endif; ?>
	</div>

</div>


<div class="form-group">
	<?php if($this->session->flashdata('success_message')): ?> 
		<div class="alert alert-dismissible alert-success text algin-center">
			<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_validation')): ?> 
		<div class="alert alert-dismissible alert-warning text algin-center">
			<?php echo $this->session->flashdata('error_validation'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_message')): ?> 
		<div class="alert alert-dismissible alert-danger text algin-center">
			<?php echo $this->session->flashdata('error_message'); ?>
		</div>
	<?php endif;?>
</div>
<?php if($this->session->flashdata('errors')): ?> 
<div class="alert alert-dismissible alert-danger text algin-center">
	<?php echo $this->session->flashdata('errors'); ?>
</div>
<?php endif;?>

	<table class="table table-responsive table-striped table-hover table-condensed table-bordered">

		<thead>
			<tr class="table-header">
				<!-- <td>ID</td> -->
				<td>Asset Name</td>
			<?php foreach($assets_quantity as $row): ?>
				<td><?php echo $row['asset_name']; ?></td>
			<?php endforeach; ?>				
			</tr>
		</thead>
		<tbody>
			<?php if ($assets): ?>
				<tr>
					<td><strong>Quantity</strong></td>
					<?php foreach ($assets_quantity as $row): ?>
						<td><?php echo $row['total_assets']; ?></td>
					<?php endforeach; ?>			
				</tr>
			<?php endif; ?>
		</tbody>

	</table>




	<table class="table table-responsive table-striped table-hover table-condensed table-bordered">

		<thead>
			<tr class="table-header">
				<td>ID</td>
				<td>Asset Name</td>
				<td>TAG ID</td>
				<td>Status</td>
				<td>Description</td>
				<?php if($this->session->userdata('update_asset_in_class') == "ok" || $this->session->userdata('remove_asset_from_class') == "ok"): ?>
					<td align="center">Action</td>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
		<?php if($assets == FALSE): ?>
			<td rowspan="6">
				<?php
					$message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "The are currently no assets in this classes";
					echo $message;
				?>
			</td>
		<?php else: ?>
			<?php if($assets): $x = 1; ?>

				<?php foreach ($assets as $row): ?>
					<tr>
						<td><?php echo $x+$offset; ?></td>
						<td><?php echo $row['asset_name']; ?></td>
						<td><?php echo $row['asset_no']; ?></td>
						<td><?php echo $row['status']; ?></td>
						<td><?php echo $row['description']; ?></td>	
						<?php if($this->session->userdata('manage_class_assets') == "ok"): ?>
							<td align="center">
								<a href="#" data-target="#editClassAssetModal<?php echo $row['class_stream_id']; ?>" data-toggle="modal" data-placement="bottom" title="Edit" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
								<a href="<?php echo base_url() . 'classes/delete_asset/'.$row['id'].'/'.$row['class_stream_id']; ?>" class="btn btn-danger btn-xs" data-placement="bottom" title="Delete" onClick="return confirm('Are you sure you want to remove this: <?php echo $row['asset_name']; ?>, with asset no: <?php echo $row['asset_no']; ?>?');"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						<?php endif; ?>					
					</tr>
				<?php $x++; endforeach; ?>
			<?php endif; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<!-- PAGINATION LINKS -->
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'classes/assets/'.$class_stream_id; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>
<!-- END PAGINATION LINKS -->


<!-- START MODAL ADD NEW ASSET -->
    <div class="modal fade" id="addAssetModal<?php echo $class_stream_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Asset:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("classes/new_asset", $attributes);
                ?>         
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="asset_name" >Asset Name</label>
                    <div class="col-sm-8">
                      <input type="text" name="asset_name" class="form-control" placeholder="Meza.." required>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="asset_no" >Asset No</label>
                    <div class="col-sm-8">
                      <input type="text" name="asset_no" class="form-control" placeholder="Enter Asset id" required>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="status" >Status</label>
                    <div class="col-sm-8">                     
	            		<select name="status" class="form-control" required>
			                <option value="">--Choose status--</option>
			                <option value="Good">Good</option>
			                <option value="Fair">Fair</option>
			                <option value="Poor">Poor</option>
		                </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="description" >Description</label>
                    <div class="col-sm-8">                     
	            		<select name="description" class="form-control" required>
			                <option value="">--option--</option>
			                <option value="Good Condition">Good Condition</option>
			                <option value="Need Repair">Need Repair</option>			                
			                <option value="Need Replacement">Need Replacement</option>
		                </select>
                    </div>
                </div>         
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">ADD ASSET</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- END MODAL ADD NEW ASSET -->


<!-- START MODAL EDIT ASSET -->
<?php $y = 1; foreach($assets as $row): ?>
    <div class="modal fade" id="editClassAssetModal<?php echo $row['class_stream_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit Asset: " . $row['asset_no']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("classes/update_asset", $attributes);
                ?>      
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="asset_no" >Asset No</label>
                    <div class="col-sm-8">
                      <input type="text" name="asset_no" class="form-control" placeholder="Enter Asset id" value="<?php echo $row['asset_no']; ?>" required>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="asset_name" >Asset Name</label>
                    <div class="col-sm-8">
                      <input type="text" name="asset_name" class="form-control" value="<?php echo $row['asset_name']; ?>" required>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="status" >Status</label>
                    <div class="col-sm-8">                     
	            		<select name="status" class="form-control" required>
			                <option value="">--option--</option>
			                <option <?php if($row['status'] == "Good"){ echo "selected"; } ?> value="Good">Good</option>
			                <option <?php if($row['status'] == "Fair"){ echo "selected"; } ?> value="Fair">Fair</option>
			                <option  <?php if($row['status'] == "Poor"){ echo "selected"; } ?> value="Poor">Poor</option>
		                </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="description" >Description</label>
                    <div class="col-sm-8">                     
	            		<select name="description" class="form-control" required>
			                <option value="">--option--</option>
			                <option <?php if($row['description'] == "Good Condition"){ echo "selected"; } ?> value="Good Condition">Good Condition</option>
			                <option <?php if($row['description'] == "Need Repair"){ echo "selected"; } ?> value="Need Repair">Need Repair</option>			                
			                <option <?php if($row['description'] == "Need Replacement"){ echo "selected"; } ?> value="Need Replacement">Need Replacement</option>
		                </select>
                    </div>
                </div>         
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
            	<!-- <input type="text" value="<?php echo $class_stream_id."DORM".$y; ?>" > -->
            	<input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
                <input type="hidden" name="old_asset_no" value="<?php echo $row['asset_no']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">UPDATE ASSET</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php $y++; endforeach; ?>
<!-- END MODAL EDIT ASSET -->