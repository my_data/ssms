
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
                <?php echo form_open('classes/current_and_past_class_teachers'); ?>
                <input type="text" class="form-control input-sm" name="search_record" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                      <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?>
                    </div>
              </div>
            </div>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <!-- <td width="2%" align="center">S/No.</td> -->
        <td width="28%" align="center">Class</td>
        <td width="25%" align="center">Teacher Names</td>
        <td width="15%" align="center">From</td>
        <td width="15%" align="center">TO</td>
      </tr>      
    </thead>
    <tbody>
    <?php if($class_teacher_records == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; $cid = ""; ?>
    <?php foreach($class_teacher_records as $ctr): ?>
        <tr>
            <!-- <td align="center"><?php echo $x+$p."."; ?></td> -->
            <td align="center">
              <?php
                if ($cid != $ctr['class_stream_id']) {
                  echo $ctr['class_name'] . " " . $ctr['stream'];
                }
              ?>
            </td>
            <td align="center"><?php if($ctr['end_date'] == NULL){ echo "<span style='color:#0000FF;'>". $ctr['teacher_names'] . "</span>"; }else{ echo $ctr['teacher_names']; } ?><?php  ?></td>            
            <td align="center"><?php echo $ctr['start_date']; ?></td>
            <td align="center"><?php if($ctr['end_date'] == NULL){ echo "-"; }else{ echo $ctr['end_date']; } ?></td>
        </tr>
        <?php $x++; $cid = $ctr['class_stream_id']; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>

<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>

    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>