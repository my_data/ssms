<br/>
<br/>
<br/>
<div class="table-responsive">
      <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
          <tr class="table-header">
            <td width="10%" align="center">Staff No.</td>
            <td width="30%">Teacher Names</td>
            <td width="30%">Subject</td>
            <td width="10%" align="center">Total</td>
            <td width="10%" align="center">Taught</td>
            <td width="10%" align="center">Untaught</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach($records as $record): ?>
            <tr>
              <td align="center"><?php echo $record['teacher_id']; ?></td>
              <td><?php echo $record['firstname'] . " " . $record['lastname']; ?></td>
              <td><?php echo $record['subject_name']; ?></td>
              <td align="center"><?php echo $record['no_of_periods']; ?></td>
              <td align="center"><?php echo $record['taught']; ?></td>
              <td align="center"><?php echo $record['untaught']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
  </div>
</div>