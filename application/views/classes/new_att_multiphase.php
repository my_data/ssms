
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<?php $attributes = array('id' => 'formWizard', 'onSubmit' => 'return false'); ?>
<?php echo form_open('classes/save_student_att', $attributes); ?>

<label for="date">Date</label>
<input type="date" name="date" id="date" required />

<div class="input-group-btn">
   
      </div>
</div>
</div>


</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>">
<div class="table table-responsive">
<table id="kashushura_dataa_not_usedhidden" class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th>Names</th>
			<!-- <th>Names</th> -->
			<th>Present</td>
			<th>Absent</th>
			<th>Sick</th>
			<th>Permitted</th>
			<th>Home</th>
		</tr>
	</thead>

		<tbody >
		<?php if ($stream_students == FALSE): ?>
	      	<tr>
		        <td colspan="6">
		            <?php
		                $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in in this class";
		                echo $message;
		            ?>
		         </td>
	     	</tr>
		<?php else: ?>
			<?php $x = 1; ?> 
			<?php //count($stream_students); ?>
			<?php foreach($stream_students as $stream_student): ?>
				<tr>
					<td>
						<?php echo $stream_student['student_names']; ?>
						<input type="hidden" name="admission_no[]" value="<?php echo $stream_student['admission_no']; ?>">
					</td>
					<!-- CHECKBOX -->
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="2" checked="true" />
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="1">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="3">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="4">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="5">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<!-- END CHECKBOX -->
				</tr>			
				<?php $x++; ?>
			<?php endforeach; ?>
		</tbody>
</table>
<div align="center">
	<input type="submit" name="submit" value="Save Attendance Record" class="btn btn-primary form-control" onClick="return confirm('Are you sure you want to add this attendance record, it cannot be modified');" />
<?php endif; //The save button should not be displayed if there are bo students in class ?>
</div>

<!-- <div id="step1">
	<input type="text" name="" />
	<button onClick="nextPhase1();">Next</button>
</div>
<div id="step2">
	hhahahahahaha
	<button onClick="nextPhase2();">Next</button>
</div> -->

<?php echo form_close(); ?>