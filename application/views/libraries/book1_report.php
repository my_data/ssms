
<div class="panel panel-default">
 <div class="panel-body">
   


<table class="table table-responsive">

<?php if($error=$this->session->flashdata('response')):?>
  <div class="alert alert-dismissible alert-success">
    <?php echo $error;?>
    <a class="close" data-dismiss="alert" aria-label="close" ><span class="glyphicon glyphicon-ok"</span></a>
  </div>
<?php endif;?>

<thead>
<tr>
<td>
<h4>&nbsp ALL COPIES &nbsp<span class="badge">&nbsp<?php echo $x;?></span></h4>
</td>
<td>
<form class="form-inline" role="search" action="<?php echo base_url().'library/search_code';?>" method="POST" >
            <div class="col-md-10">  
    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search titles" name="searchcode" id="searchcode">
    <div class="input-group-btn">
      <button class="btn btn-primary" type="submit">
        <i class="glyphicon glyphicon-search " ></i>
      </button>
    </div>
  </div>
  </div>
      </form>
      </td>
      <td>
     
      </td>
      <td>Dictionary:</td>
    	<td style="color:#DC143C"><span class="glyphicon glyphicon-remove">&nbsp;&nbsp;Poor,</td>
        <td style="color:#00B8D4"><span class="glyphicon glyphicon-ok">&nbsp;Normal/Good</td>
    
    </tr> 

    </thead> 
 </table>     
<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered table-responsive ">
<thead>

<tr class="text-info">
	<th>Isbn#</th>
	<th>Code#</th>
	<th>Title</th>
	<th>Author</th>
	<th>Edition</th>
	<th>Status</th>
	<th style="color:green;">Options</th>

</tr>
</thead>
<tbody>

	<?php if(count($y)):?>
	<?php foreach ($y->result() as $row) {
		?>
<tr>		
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->CODE;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<td><?php echo $row->AUTHOR_NAME;?></td>
<td><?php echo $row->EDITION;?></td>

<?php if($row->STATUS=="bad"):?>
<td style="color:#DC143C"><span class="glyphicon glyphicon-remove"></span></td>
<?php else:?>

<td style="color:#00B8D4"><span class="glyphicon glyphicon-ok"></span></td>
<?php endif;?>
 
<td>
<?php
     echo '<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal'.$row->CODE.'">Edit Status
			</a>';?>
    <?php echo form_open('library/add_book');?>
    <?php		echo '<div id="myModal'.$row->CODE.'" class="modal fade" tabindex="-1" role="dialog" >';
  			 echo '<div class="modal-dialog" >
			    <!-- Modal content-->';
			   echo  '<div class="modal-content" style="background-color:#DDDDDD">';
			    	echo '<div class="modal-header">';
			        	echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
			        	 echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h4>';
			        	  echo '<h4 class="modal-title">Edition:&nbsp '.$row->EDITION.' </h4>';
			    	echo '</div>';
			      	 echo '<div class="modal-body">';?>
			      	 <h4>Change book status</h4>
			      	 <input type="hidden" name="isbn" value="<?php echo $row->ISBN;?>" />
			      	 <table class="table" style="background-color:#DDDDDD">
			      	 	
	                  <tr>
		              <th><!-- <label>CODE</label> --></th>
		              <th><input type="hidden" name="code" id="code" value="<?php echo $row->CODE ?>" class="form-control" /><span>
			          <?php echo form_error('code');?></span></th>

			          </tr>
			          <tr>
	                
		              <td><label>BOOK STATUS</label></td>
		              <td><label><input type="radio" name="status" value="normal" enabled>Normal&nbsp&nbsp<input type="radio" name="status" value="bad">Poor</label>
		              <span class="text-danger"><?php echo form_error('status'); ?></span>
		              </td>
		              </tr>
		
			      	 </table>
			      
			     

			     <?php 
			      	echo '</div>';
			      	echo '<div class="modal-footer">';
			      			 echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
			      	
			        	echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      	</div>
			    </div>
		 	</div>
		</div>
			';
	
?>

 	 <?php echo form_close();?>
 	 <!-- /Update book type modal-->

</td>

</tr>

	<?php
	}

	?>
<?php else:?>
	<tr>
  <div class="alert alert-dismissible alert-warning">
    <?php echo 'No recod found';?>
    <a class="close" data-dismiss="alert" aria-label="close" ><span class="glyphicon glyphicon-remove"</span></a>
  </div>
</tr>
<?php endif;?>

</tbody>
</table>
</div>
<div class="center pull-right">
   <?php echo $links;?>
  </div>
  </div> 
  
  </div>
  
  
  </div>

  </div>


