
	<table class="table table-striped table-hover table-responsive table-bordered">
	<thead>
	<tr>
	
	<th><h4><span class="glyphicon glyphicon-transfer"></span>&nbspBOOK BORROWING</h4></th>

<th><a href="<?php echo base_url('library/retbooktype');?>"><span class="glyphicon glyphicon-briefcase"></span>&nbspBook Type Log
</a></th>

<th><a href="<?php echo base_url('library/book1_report');?>"><span class="glyphicon glyphicon-book"></span> &nbspBook Log</a></th>


</tr>
</thead>
</table>

<div class="col-md-3"></div>
<div class="panel panel-default">
<div class="panel-body">
<?php echo form_open('library/borrow_book');?>
	<div class="col-xs-4">
	
				<h4>AUTHOR:&nbsp<?php echo $author;?></h4>
				
				
				<h4>TITLE:&nbsp<?php echo $title;?>;&nbsp<?php echo $edition;?>&nbsp Edition</h4>
				
		
	<table class="table" >
	<tbody>
	<tr>
		<td><label>Book #CODE</label></td>
		<td><input type="text" name="code" id="code" value="<?php echo $code;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('code'); ?></span></td>
	</tr>
	<tr>
		<td><label>Book ISBN</label></td>
		<td><input type="text" name="isbn" id="isbn" value="<?php echo $isbn;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
	</tr>
	<tr>
		<td><label>Borrower ID</label></td>
		<td><input type="text" name="borrower" class="form-control">
		<span class="text-danger"><?php echo form_error('borrower'); ?></span></td>

	</tr>
	<tr>
	<td><label>Borrower type</label></td>
	<td><select name="type" class="form-control">
		<?php foreach($type as $ty): ?>
			<option value="<?php echo $ty['SYMBOL']; ?>"><?php echo $ty['TYPE']; ?></option>
		<?php endforeach; ?></select>
		<span class="text-danger"><?php echo form_error('status'); ?></span></td></tr>
	
	<tr>
		<td><label>Date of return</label></td>
		<td><input type="date" name="date" class="form-control">
		<span class="text-danger"><?php echo form_error('date'); ?></span></td>
	</tr>
	

	</tbody>

	</table>
	<div class="form-group text-right">
			<button type="submit" class="btn btn-success" onclick="return borrowBook()">Submit<br></button>
		</div>

	</div>
		<?php echo form_close();?>

		</div>
		</div>
		<div class="col-xs-3"></div>