
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title .'&nbsp' ?>
        <span class="badge">&nbsp;<?php echo $x; ?></span></div>
         <div class="db-header-extra form-inline"> 

 <?php echo form_open(base_url() . 'library/book1_report'); ?>
                             
     <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?> 
                            
                 
    </div>
 

 
</div>            


<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="text-left">           
           
            <i class="glyphicon glyphicon-remove">&nbsp;Poor&nbsp;</i>
             <i class="glyphicon glyphicon-ok">&nbsp;Normal/Good</i>
</div>
<br/>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered table-responsive ">
<thead>

<tr>
  <th>Isbn#</th>
  <th>Code#</th>
  <th>Title</th>
  <th>Author</th>
  <th>Edition</th>
  <th>Status</th>
  <th >Options</th>

</tr>
</thead>
<tbody>

  <?php if(count($y)):?>
  <?php foreach ($y->result() as $row) {
    ?>
<tr>    
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->CODE;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<td><?php echo $row->AUTHOR_NAME;?></td>
<td><?php echo $row->EDITION;?></td>

<?php if($row->STATUS=="bad"):?>
<td><span class="glyphicon glyphicon-remove"></span></td>
<?php else:?>

<td><span class="glyphicon glyphicon-ok"></span></td>
<?php endif;?>
 
<td>
<?php
     echo '<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal'.$row->CODE.'"><span class="fa fa-pencil" title="Update book status"></span>
      </a>';?>
    <?php echo form_open('library/add_book');?>
    <?php   echo '<div id="myModal'.$row->CODE.'" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                 echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h4>';
                  echo '<h4 class="modal-title">Edition:&nbsp '.$row->EDITION.' </h4>';
            echo '</div>';
               echo '<div class="modal-body">';?>
               <h4>Change book status</h4>
               <input type="hidden" name="isbn" value="<?php echo $row->ISBN;?>" />
               <table class="table" style="background-color:#DDDDDD">
                
                    <tr>
                  <th><!-- <label>CODE</label> --></th>
                  <th><input type="hidden" name="code" id="code" value="<?php echo $row->CODE ?>" class="form-control" /><span>
                <?php echo form_error('code');?></span></th>

                </tr>
                <tr>
                  
                  <td><label>BOOK STATUS</label></td>
                  <td><label><input type="radio" name="status" value="normal" enabled>Normal&nbsp;&nbsp;<input type="radio" name="status" value="bad">Poor</label>
                  <span class="text-danger"><?php echo form_error('status'); ?></span>
                  </td>
                  </tr>
    
               </table>
            
           

           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';
                   echo '<button type="submit" class="btn btn-primary" onclick="return confirmUpdate() data-dismiss="modal">SAVE</button>';
              
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
              </div>
          </div>
      </div>
    </div>
      ';
  
?>

   <?php echo form_close();?>
   <!-- /Update book type modal-->

</td>

</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr>
  <div class="alert alert-dismissible alert-warning">
    <?php echo 'No recod found';?>
    <a class="close" data-dismiss="alert" aria-label="close" ><span class="glyphicon glyphicon-remove"></span></a>
  </div>
</tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
</div>
<div class="center pull-right">
   <?php echo $links;?>
  </div>
  </div> 
  
  </div>
  
  
  </div>

  </div>


