
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp; BORROWING HISTORY &nbsp;</span>
        </div>
<div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
<?php echo form_open(base_url() . 'library/borrowing_history'); ?>
                                                            
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                      <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search" ></i>
                         </button> 
                      </div>
      </div>
<?php echo form_close(); ?> 
 </div>

            Dictionary:<label>lost=<span class="glyphicon glyphicon-remove"> </span>
            <label>returned=<span class="glyphicon glyphicon-ok"> </span> </label>,
          <label > Still out=<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> </label>
                         
</div>

</div>          
  

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<div class="tables table-responsive" >

<table class="table table-striped table-hover table-responsive table-bordered table-condensed" >

<tr>
  <th>Code#</th>
  <th>Isbn#</th>
  <!-- <th>TITLE</th> -->
  <!-- <th>LIBRARIAN ID</th> -->
  <th>Cus-id#</th>
    <th>Names</th>
  <th>Date_borrowed</th>
  <th>Expected-return</th>
  <th>Date-due back  </th>
  <th>Status</th>
  
  
  
<!--  <th style="color:green;">ACTIONS</th> -->

</tr>



  <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {
    ?>
<tr>    
<td><?php echo $row->ID;?></td>
<td><?php echo $row->ISBN;?></td>
<!-- <td><?php echo $row->BOOK_TITLE;?></td> -->
<!-- <td><?php echo $row->AUTHOR_NAME;?></td> -->
<!-- -->
<td><?php echo $row->BORROWER_ID;?></td>
<td><?php echo $row->NAMES;?></td> 
<td><?php echo $row->DATE_BORROWED;?></td>
<td><?php echo $row->RETURN_DATE;?></td>
<?php if($row->DATE_DUE_BACK==""):?>
  <td>Pending..</td>
    
<?php else:?>

<td><?php echo $row->DATE_DUE_BACK;?></td>
<?php endif;?>
<?php if($row->FLAG=='lost'):?>
<td><span class="glyphicon glyphicon-remove"></span></td>
<?php elseif($row->FLAG=='returned'):?>
<td><span class="glyphicon glyphicon-ok"></span></td>
<?php else:?>
<td><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i></td>
<?php endif;?>


<!-- <td>

<?php echo anchor("library/to_booktransct/{$row->ID}","Borrow");?>&nbsp</a><span>|</span>
<?php echo anchor("library/fill_editbook/{$row->ID}","Edit");?><span class="glyphicon glyphicon-pencil"></span>

</td> -->

</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr class="text-info"><td>No record matches</td></tr>
<?php endif;?>

</table>

<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<!-- <a href="#" id="print1" onclick="javascript:printreport1('borrowing-report')">Preview print</a>
  --> <div class="center">
   <?php echo $links;?>
  </div>    
</div>
</div>
</div>
</div>
