
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;Borrow Book 
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <!-- <div class="input-group">
                <?php echo form_open(base_url() . 'library/available'); ?>
                
                <input type="text" class="form-control input-sm" name="search_record" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                  <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?> 
                            
                    </div>
              </div> -->
            </div>
            <!-- TESTING PURPOSE -->
                  <!-- <input type="text" class="form-control input-sm" name="search_borrower" placeholder="Search Borrower" id="search" />
               <div class="result_borrower"></div>   -->
                <!-- END TESTING PURPOSE -->
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('library/borrow_book');?>

    <div class="form-group"><label>Book #CODE</label>
    <input type="text" name="code" id="code" class="form-control" value="<?php echo $row->ID; ?>"><span class="text-danger"><?php echo form_error('code'); ?></span>
    </div>
   <div class="form-group">
   <label>Book ISBN</label>
    <input type="text" name="isbn" id="isbn" class="form-control" value="<?php echo $row->ISBN; ?>">
    <span class="text-danger"><?php echo form_error('isbn'); ?></span>
    </div>
    <div class="form-group">
    <label>Borrower ID</label>
    <input type="text" class="form-control" name="borrower" placeholder="Search Borrower" id="search" />
    <div class="result_borrower"></div>
     <span class="text-danger"><?php echo form_error('borrower'); ?></span>
       
      </div>
    <!-- <  <label>Borrower type</label><
  <select name="type" class="form-con">
    <?php foreach($type as $ty): ?>
      <option value="<?php echo $ty['SYMBOL']; ?>"><?php echo $ty['TYPE']; ?></option>
    <?php endforeach; ?></select>
    <span class="text-danger"><?php echo form_error('status'); ?></span><</-->
   <div class="form-group">
    <label>Date of return</label>
    <input type="date" name="date" id="date" class="form-control">
    <span class="text-danger"><?php echo form_error('date'); ?></span>
   </div>

<!--   </tbody> -->


<div class="form-group">
      <button type="submit" class="btn btn-primary btn-sm form-control">BORROW<br></button>
    </div>

<?php echo form_close();?>
 

  </div>
</div>


