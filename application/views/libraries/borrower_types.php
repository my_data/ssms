

  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">
              <!-- <div class="input-group">
                <?php echo form_open('borrower_types/index'); ?>
                <input type="text" class="form-control input-sm" name="search_department" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                      <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?>
                    </div>
              </div> -->
              <a href="<?php echo base_url() . 'library/add_borrowing_limit'; ?>" class="btn btn-primary btn-sm">Add Borrowing Limit</a>
            </div>
          </div>
      </div>
<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr class="table-header">
            <td width="30%" align="center">Borrower Type</td>
            <td width="15%" align="center">Borrowing Limit</td>
            <td width="15%" align="center">Edit</td>
        </tr>
    </thead>
    <tbody>
    <?php if($borrower_types == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No borrower_types";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
    <?php foreach($borrower_types as $b_type): ?>
        <tr>
            <td align="center"><?php echo $b_type['b_type']; ?></td>
            <td align="center"><?php echo $b_type['borrowing_limit']; ?></td>
            <td align="center">
            <a href="<?php echo base_url(); ?>library/edit_btype/<?php echo $b_type['blid']; ?>" class="btn btn-primary btn-xs" data-target="modal" data-placement="bottom" title="Edit Borrower Type"><span class="fa fa-pencil"></span></a>&nbsp;
            </td>
        </tr>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>
