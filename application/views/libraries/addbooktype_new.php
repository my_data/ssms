
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;Add new Book Types</div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('library/insertbooktype');?>
<table class="table table-hover">
	<tr>
		<td><label>ISBN</label></td>
		<td><input type="text" name="isbn" class="form-control" value="<?php echo set_value('isbn'); ?>" /><span>
			<?php echo form_error('isbn'); ?></span></td>
	</tr>
	<tr>
		<td><label>Title</label></td>
		<td><input type="text" name="title" class="form-control" value="<?php echo set_value('title'); ?>" /><span>
			<?php echo form_error('title');?></span></td>
	</tr>
	<tr>
		<td><label>Author</label></td>
		<td><input type="text" name="author" class="form-control" value="<?php echo set_value('author'); ?>" /><span>
			<?php echo form_error('author');?></span></td>
	</tr>
	<tr>
		<td><label>Edition</label></td>
		<td><input type="text" name="edition" class="form-control" value="<?php echo set_value('edition'); ?>" /><span>
			<?php echo form_error('edition');?></span></td>
	</tr>
	<tr>
		<td><label>Class</label></td>
		<td><select type="text" name="class" class="form-control">
		<option value="--select-- ">--select--</option>
		<?php foreach($classes as $class): ?>
			<option  <?php echo set_select('class', ''.$class["class_id"].'', $this->input->post('class')); ?> value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
		<?php endforeach?>

		</select><span>
			<?php echo form_error('class');?></span></td>
	</tr>

	
<tr>
<td></td>
<td>
			<button type="submit" class="btn btn-primary form-control">Submit<br></button>
</td>
</tr>
<?php echo form_close();?>
	
</table>
</div>


  </div>

