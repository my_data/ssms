
      <div class="db-header clearfix">

        <div class="page-header-title"><strong><i class="fa fa-pie-chart text-warning " aria-hidden="true"></i>Book Copies'general report &nbsp;<?php echo '('.date("Y").')';?></strong>
          </div>
         <div class="db-header-extra form-inline"></div>
      </div>
   
<div class="row">
<div class="col-md-6" padding='0'>
    <div id="chart_div" style="width:400px; height: 400px;"></div>
</div>

<div class="col-md-6" padding='0'>    
    <div id="chart_cond" style="width: 400px; height: 400px;"></div>
    
</div>
  </div>
   
 
