

<div class="white-area-content">
<div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp; BORROWED BOOK COPIES &nbsp;<span class="badge"><?php echo $total; ?></span></span>
        </div>
 <div class="db-header-extra form-inline"> 


  <?php echo form_open(base_url() . 'library/borrowed'); ?>
                      
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                      <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search" ></i>
                         </button> 
                      </div>
      </div>
 <?php echo form_close(); ?> 
                            
                   
</div>
</div>



<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-responsive table-condensed table-bordered">
<thead>
<tr>
     

  <th>Code#</th>
  <th>Title</th>
  <th>Cusromer#</th>
  <th>Names</th>
  <th>Type</th>
  <th>Date borrowed</th>
  <!-- <th>STATUS BEFORE</th> -->
  <th>Date return</th>
  <th>Time left</th>
  
  
  <th>Action</th>

</tr>
</thead>
<tbody>

  <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {

    ?>

<tr>
<td><?php echo $row->ID;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<td><?php echo $row->BORROWER_ID;?></td>
<td><?php echo $row->BORROWER_DESCRIPTION;?></td>
<td><?php echo $row->BORROWER_TYPE;?></td>
<td><?php echo $row->DATE_BORROWED;?></td>
<!-- <td><?php echo $row->STATUS_BEFORE;?></td> -->
<td style="background-color:#CED4DA"><?php echo $row->RETURN_DATE;?></td>
<?php if($row->TIME_LEFT<-1):?>
  <td style="color:red">Expired <?php echo $row->TIME_LEFT;?> days </td>
  <?php elseif($row->TIME_LEFT==0):?>
    <td class="text-info"><?php echo "Tomorrow";?> </td>
  <?php elseif($row->TIME_LEFT==-1):?>
    <td class="text-success"><?php echo "Today";?> </td>
    
    
<?php else:?>

<td style="color:green"><?php echo $row->TIME_LEFT;?> days</td>
<?php endif;?>
<td class="text-center">
<?php echo '<a href="" class=" btn btn-primary btn-xs fa fa-shopping-cart" aria-hidden="true" data-toggle="modal" data-target="#myModal'.$row->TRANSACTION_ID.'"
data-toggle="tooltip" data-placement="center" title="Return this Copy">
      </a>';?>
<!--return book modal-->


<?php   echo '<div id="myModal'.$row->TRANSACTION_ID.'" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" >';
            echo '<div class="modal-header">';
            echo "<h4>BOOK COPY RETURN PANEL</h4>";
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                
                echo '<h4 class="modal-title">Customer:&nbsp' .$row->BORROWER_DESCRIPTION.'  </h4>';
                echo '<h4 class="modal-title">CODE:&nbsp' .$row->BORROWER_ID.' ,&nbspType&nbsp '.$row->BORROWER_TYPE.'  </h4>';
               
                echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.' </h4>';
                echo '<h4 class="modal-title">Edition:&nbsp' .$row->EDITION.'  </h4>';
                echo '<h4 class="modal-title">Author:&nbsp' .$row->AUTHOR_NAME.'  </h4>';
             echo '</div>';
               echo '<div class="modal-body">';?>
         <?php echo form_open('library/return_book');?>

 <input type="hidden" value="<?php echo $row->TRANSACTION_ID;?>" name="tra" class="hidden">
 <table class="table">
<tr>
    <td><label>Book #CODE</label></td>
    <td><input type="text" name="code" id="code" value="<?php echo $row->ID;?>" class="form-control">
    <span class="text-danger"><?php echo form_error('code'); ?></span></td>
  </tr>
  <tr>
    <td><label>Book ISBN</label></td>
    <td><input type="text" name="isbn" id="isbn" value="<?php echo $row->ISBN;?>" class="form-control">
    <span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
  </tr>
     <tr>
    <td><label>Status after</label></td>
    <td><label><input type="radio" name="status" class="status" value="Normal" checked="true">Normal&nbsp;&nbsp;<input type="radio" name="status" class="status" id="ava" value="bad">Bad</label>
    <span class="text-danger"><?php echo form_error('status'); ?></span></td>
    
  </tr>
  <tr>
    <td><label>Availability</label></td>
    <td><label><input type="radio" name="availability" class="ava" value="Returned" checked>Returned&nbsp;&nbsp;<input type="radio" name="availability" class="ava" value="lost">Lost</label>
    <span class="text-danger"><?php echo form_error('availability'); ?></span></td>
    
  </tr>

  </table>
  




<div class="form-group text-right">
 <a class="btn btn-md btn-danger" href="<?php echo base_url('library/borrowed/'); ?>">Cancel 
</a>
      <button type="submit" class="btn btn-success">Return<br></button>
    </div>
<?php echo form_close();?>
           </div>
              
          </div>
      </div>
    </div>
  

</td>

</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr>No record found</tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
  <div class="text-center">
  <label class="label-control"><?php echo $links;?></label>

  </div>    


  


