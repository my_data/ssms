

<div class="col-md-6">

<?php if($error=$this->session->flashdata('response')):?>
	<div class="alert alert-dismissible alert-warning text algin-center">
		<?php echo $error;?>
	</div>
<?php endif;?>
</div>


<div class="panel">
<div class="panel-heading">
	<h4>Add new Book Types</h4>
</div>
<div class="panel-body">
<?php echo form_open('library/insertbooktype');?>
<table class="table table-hover">
	<tr>
		<td><label>ISBN</label></td>
		<td><input type="text" name="isbn" class="form-control" /><span>
			<?php echo form_error('isbn');?></span></td>
	</tr>
	<tr>
		<td><label>Title</label></td>
		<td><input type="text" name="title" class="form-control" /><span>
			<?php echo form_error('title');?></span></td>
	</tr>
	<tr>
		<td><label>Author</label></td>
		<td><input type="text" name="author" class="form-control" /><span>
			<?php echo form_error('author');?></span></td>
	</tr>
	<tr>
		<td><label>Edition</label></td>
		<td><input type="text" name="edition" class="form-control" /><span>
			<?php echo form_error('edition');?></span></td>
	</tr>
	<tr>
		<td><label>Class</label></td>
		<td><select type="text" name="class" class="form-control">
		<option value="--select-- ">--select--</option>
		<?php foreach($classes as $class): ?>
			<option value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
		<?php endforeach?>

		</select><span>
			<?php echo form_error('class');?></span></td>
	</tr>

	

</table>
<div class="form-group text-right">
			<button type="submit" class="btn btn-success">Submit<br></button>
		</div>

<?php echo form_close();?>


</div>
	

</div>


  </div>
  </div>

