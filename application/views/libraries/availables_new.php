
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;Available Book Copies &nbsp; <span class="badge"><?php echo $all."/".$total;?></span>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
                <?php echo form_open(base_url() . 'library/available'); ?>
                
                <input type="text" class="form-control input-sm" name="search_record" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                  <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?> 
                            
                    </div>
              </div>
            </div>
            <!-- TESTING PURPOSE -->
                  <!-- <input type="text" class="form-control input-sm" name="search_borrower" placeholder="Search Borrower" id="search" />
               <div class="result_borrower"></div> -->  
                <!-- END TESTING PURPOSE -->
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
<thead>
<tr>
  <th>Code#</th>
  <th>Isbn#</th>
  <th>Title</th>
  
  
  
  <th class="text-center">Action</th>

</tr>
</thead>
<tbody>

  <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {
    ?>
<tr>    
<td ><?php echo $row->ID;?></td>
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>


<td class="text-center">

<a href="<?php echo base_url() . 'library/new_borrow_book/'.$row->ISBN.'/'.$row->ID; ?>" title="Borrow this Copy" class="btn btn-primary btn-xs fa fa-shopping-cart"></a>

<?php echo form_open('library/borrow_book');?>
      
    <?php   echo '<div id="borrow_book'.$row->ID.'" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                 //
                  echo '<h5 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h5>'; 
            echo '</div>';
               echo '<div class="modal-body">';?>
               
   <table class="table" style="background-color:#DDDDDD">
  <tbody>
  <tr>
    <td><label>Book #CODE</label></td>
    <td><input type="text" name="code" id="code" value="<?php echo $row->ID ?>" class="form-control">
    <span class="text-danger"><?php echo form_error('code'); ?></span></td>
  </tr>
  <tr>
    <td><label>Book ISBN</label></td>
    <td><input type="text" name="isbn" id="isbn" value="<?php echo $row->ISBN ;?>" class="form-control">
    <span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
  </tr>
  <tr>
    <td><label>Borrower ID</label></td>
    <td><input type="text" name="borrower" id="borrower" class="form-control">
    <span class="text-danger"><?php echo form_error('borrower'); ?></span>
      <span class="text-danger" id="availability"></span>
    </td>

  </tr>
<tr>
  <td><label>Borrower type</label></td>
  <td><select name="type" class="form-control">
    <?php foreach($type as $ty): ?>
      <option value="<?php echo $ty['SYMBOL']; ?>"><?php echo $ty['TYPE']; ?></option>
    <?php endforeach; ?></select>
    <span class="text-danger"><?php echo form_error('status'); ?></span></td></tr>
  
  <tr>
    <td><label>Date of return</label></td>
    <td><input type="date" name="date" id="date" class="form-control">
    <span class="text-danger"><?php echo form_error('date'); ?></span></td>
  </tr>
  

  </tbody>

  </table>
            
           

           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';
                   echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
              
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
    </div>
      ';
  
?>

   <?php echo form_close();?>

</td>

</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr>No record found</tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
 <div class="center">
   <?php echo $links;?>
  </div>

<!-- insert book type modal-->

<?php echo form_open('library/insertbooktype');?>
      
    <?php   echo '<div id="add_type" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                 echo '<h4 class="modal-title">Add new Book Title</h4>';
                  /*echo '<h4 class="modal-title">Edition:&nbsp  </h4>';*/
            echo '</div>';
               echo '<div class="modal-body">';?>
               
               <table class="table">
  <tr>
    <td><label>ISBN</label></td>
    <td><input type="text" name="isbn" class="form-control" /><span>
      <?php echo form_error('isbn');?></span></td>
  </tr>
  <tr>
    <td><label>Title</label></td>
    <td><input type="text" name="title" class="form-control" /><span>
      <?php echo form_error('title');?></span></td>
  </tr>
  <tr>
    <td><label>Author</label></td>
    <td><input type="text" name="author" class="form-control" /><span>
      <?php echo form_error('author');?></span></td>
  </tr>
  <tr>
    <td><label>Edition</label></td>
    <td><input type="text" name="edition" class="form-control" /><span>
      <?php echo form_error('edition');?></span></td>
  </tr>
  <tr>
    <td><label>Class</label></td>
    <td><select type="text" name="class" class="form-control">
    <option value="--select-- ">--select--</option>
    <?php foreach($classes as $class): ?>
      <option value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
    <?php endforeach?>

    </select><span>
      <?php echo form_error('class');?></span></td>
  </tr>

  

</table>
            
           

           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';
                   echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
              
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
    </div>
      ';
  
?>

   <?php echo form_close();?>
   <!-- /insert book type modal-->
 
 </div> 
 
 </div> 
  </div>
  </div>








  
