
  <div class="white-area-content">
      <div class="db-header clearfix">

      <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        &nbsp;<span class="badge">&nbsp;<?php echo $y; ?></span>
      </div>
 <div class="db-header-extra form-inline text-right"> 

<div class="form-group has-feedback no-margin">
<?php echo form_open(base_url() . 'library/retbooktype'); ?>

      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                      <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search" ></i>
                         </button> 
                      </div>
      </div>

  </div>
            <a href="<?php echo base_url() . 'library/addbooktype'; ?>" class="btn btn-primary btn-sm">Add Book</a>
                     
                 
</div>
            

      </div>


<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
<thead>     

<tr>
  <th>ISBN</th>
  <th>BOOK TITLE</th>
  <th>AUTHOR</th>
  <th>COPIES</th>
  <th>Actions</th>


</tr>
</thead>
<tbody>

  <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {
    ?>
<tr>    
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<td><?php echo $row->AUTHOR_NAME;?></td>
<td><?php echo $row->COPIES;?></td>


<td>


<!---Add copies -->
 <a class="btn btn-primary btn-xs glyphicon glyphicon-eye-open" href="<?php echo base_url('library/retrieve_isbn/'.$row->ISBN); ?>"
    data-toggle="tooltip" data-placement="right" title="View Book Copies"></a>&nbsp
<?php
    
      echo '<a class=" btn btn-primary btn-xs glyphicon glyphicon-plus-sign" data-toggle="modal" data-target="#add_copies'.$row->ISBN.'"
         data-toggle="tooltip" data-placement="right" title="Add new book Copies">
      </a>';?>
        

<?php   echo '<div id="add_copies'.$row->ISBN.'" class="modal fade" tabindex="-1" role="dialog" >';?>
          <?php echo form_open('library/add_copies/'.$row->ISBN); ?>
        <?php  echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                 echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h4>';
                  echo '<h4 class="modal-title">Edition:&nbsp '.$row->EDITION.' </h4>';
            echo '</div>';
               echo '<div class="modal-body">';?>
              
           <p>Enter No. Copies</p>
               
        

    <input type="hidden" name="isbn" value="<?php echo $row->ISBN;?>" class="form-control"/>

  
    <input type="text" name="no_of_copies" >
      
  
    
    
  


           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';?>
                  
<?php echo '<button type="submit" class="btn btn-primary btn-sm" onclick="return confirmUpdate() data-dismiss="modal">ADD</button>';?>
            
              <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
              </div>
          </div>

      </div>
       <?php echo form_close();?>
  <?php echo '</div>
      ';
  
?>

<!--/Addd copies--> 

 <!-- Update book type modal-->
<?php

    
      echo '<a href="" class="btn btn-primary btn-xs glyphicon glyphicon-edit"  data-toggle="modal" data-target="#myModal'.$row->ISBN.'"    data-toggle="tooltip" data-placement="right" title="Edit Book Title">
         </a>';?>
<?php echo form_open('library/update_book_type');?>
      
    <?php   echo '<div id="myModal'.$row->ISBN.'" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                 echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h4>';
                  echo '<h4 class="modal-title">Edition:&nbsp '.$row->EDITION.' </h4>';
            echo '</div>';
               echo '<div class="modal-body">';?>
               
               <table class="table" style="background-color:#DDDDDD table-responsive  table-condensed" >
                
                <tr>
    <td></td>
    <td><input type="hidden" name="isbn" id="isbn" value="<?php echo $row->ISBN?>" class="form-control"/><span>
      <?php echo form_error('isbn');?></span></td>
  </tr>
  <tr>
    <td><label>Title</label></td>
    <td><input type="text" name="title" id="title" value="<?php echo $row->BOOK_TITLE ?>" class="form-control" /><span>
      <?php echo form_error('title');?></span></td>
  </tr>
  <tr>
    <td><label>Author</label></td>
    <td><input type="text" name="author" id="author" value="<?php echo $row->AUTHOR_NAME ?>" class="form-control" /><span>
      <?php echo form_error('author');?></span></td>
  </tr>
  <tr>
    <td><label>Edition</label></td>
    <td><input type="text" name="edition" id="edition" value="<?php echo $row->EDITION ?>" class="form-control" /><span>
      <?php echo form_error('edition');?></span></td>
  </tr>
               </table>
            
           

           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';
                   echo '<button type="submit" class="btn btn-primary btn-sm" onclick="return confirmUpdate() data-dismiss="modal">SAVE</button>';
              
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
              </div>
          </div>
      </div>
    </div>
      ';
  
?>

   <?php echo form_close();?>
   <!-- /Update book type modal-->
</td>



</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr><div class="alert alert-dismissible alert-success">
    <?php echo 'No record found!';?>
  </div></tr>
<?php endif;?>
</tbody>
</table>

<!-- /End of table-->

<!-- insert book type modal-->

<?php echo form_open('library/insertbooktype');?>
      
    <?php   echo '<div id="add_type" class="modal fade" tabindex="-1" role="dialog" >';
         echo '<div class="modal-dialog" >
          <!-- Modal content-->';
         echo  '<div class="modal-content" style="background-color:#DDDDDD">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                // echo '<h4 class="modal-title">Title:&nbsp '.$row->BOOK_TITLE.'</h4>';
                 // echo '<h4 class="modal-title">Edition:&nbsp '.$row->EDITION.' </h4>';
            echo '</div>';
               echo '<div class="modal-body">';?>
               
               <table class="table">
  <tr>
    <td><label>ISBN</label></td>
    <td><input type="text" name="isbn" class="form-control" /><span>
      <?php echo form_error('isbn');?></span></td>
  </tr>
  <tr>
    <td><label>Title</label></td>
    <td><input type="text" name="title" class="form-control" /><span>
      <?php echo form_error('title');?></span></td>
  </tr>
  <tr>
    <td><label>Author</label></td>
    <td><input type="text" name="author" class="form-control" /><span>
      <?php echo form_error('author');?></span></td>
  </tr>
  <tr>
    <td><label>Edition</label></td>
    <td><input type="text" name="edition" class="form-control" /><span>
      <?php echo form_error('edition');?></span></td>
  </tr>
  <tr>
    <td><label>Class</label></td>
    <td><select type="text" name="class" class="form-control">
    <option value="--select-- ">--select--</option>
    <?php foreach($classes as $class): ?>
      <option value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
    <?php endforeach?>

    </select><span>
      <?php echo form_error('class');?></span></td>
  </tr>

  

</table>
            
           

           <?php 
              echo '</div>';
              echo '<div class="modal-footer">';
                   echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
              
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
    </div>
      ';
  
?>
 
   <?php echo form_close();?>
 
   <!-- /insert book type modal-->
   <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
 </div> 
 <div class="center">
  <label class="label-control"><?php echo $links;?></label>
</div>
  </div>

  </div>
  </div>
  </div>









