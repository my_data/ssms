<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('library/edit_btype/'.$borrower['blid'], $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="b_type">Borrower Type :</label>
			<div class="col-sm-9">
				<input type="text" value="<?php echo $borrower['b_type']; ?>" name="b_type" class="form-control" readonly/>
				<div class="error_message_color">
					<?php echo form_error('b_type'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">Borrowing Limit :</label>
			<div class="col-sm-9">
				<input type="text" value="<?php echo $borrower['borrowing_limit']; ?>" name="b_limit" class="form-control" />
				<div class="error_message_color">
					<?php echo form_error('b_limit'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>		
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_borrower" value="Add borrower" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>
