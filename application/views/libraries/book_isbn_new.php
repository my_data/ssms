
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<span class="badge"><?php echo $x ;?>&nbsp;<!--  / &nbsp;<?php echo $y; ?> --></span>&nbsp;
    Title-&nbsp;<?php echo $title ;?>&nbsp; ;
    Author-&nbsp;<?php echo $author;?>&nbsp; ; 
    &nbsp;Edition-&nbsp;<?php echo $edition;?> 
</div>
<div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
<?php echo form_open(base_url() . 'library/retrieve_isbn/'.$isbn); ?>
 
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  bbbb..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search sm " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?> 
  </div>
  
               <a href="<?php echo base_url() . 'library/dept_borrow/'.$isbn; ?>" class="btn btn-primary btn-sm" title="department">D</a>&nbsp;
               <a href="<?php echo base_url() . 'library/retrieve_isbn/'.$isbn; ?>" class="btn btn-primary btn-sm" title="individual">I</a>
                       
                
            </div>
 
          </div>

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
  <thead> <tr>
    <th>Code#</th>
	
	  <th class="col-xs-2 text-center">Action</th>
  
    </tr>
       </thead>
<tbody>

	<?php if(count($books)>0):?>
	<?php foreach ($books as $data) {
	
		?>
	<tr>
	    <td><?php echo $data->ID; ?></td>
<td>
   <a href="<?php echo base_url() . 'library/new_borrow_book/'.$data->ISBN.'/'.$data->ID; ?>" title="Borrow this Copy" class="btn btn-primary btn-xs fa fa-shopping-cart"></a>

<?php echo form_open('library/borrow_book');?>
			
		<?php		echo '<div id="borrow_book'.$data->ID.'" class="modal fade" tabindex="-1" role="dialog" >';
  			 echo '<div class="modal-dialog" >
			    <!-- Modal content-->';
			   echo  '<div class="modal-content" style="background-color:#DDDDDD">';
			    	echo '<div class="modal-header">';
			        	echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
			        	 //
			        	  echo '<h4 class="modal-title">Title:&nbsp '.$data->BOOK_TITLE.'</h4>'; 
			    	echo '</div>';
			      	 echo '<div class="modal-body">';?>
			      	 
			      	 <table class="table" style="background-color:#DDDDDD">
	<tbody>
	<tr>
		<td><label>Book #CODE</label></td>
		<td><input type="text" name="code" id="code" value="<?php echo $data->ID ?>" class="form-control">
		<span class="text-danger"><?php echo form_error('code'); ?></span></td>
	</tr>
	<tr>
		<td><label>Book ISBN</label></td>
		<td><input type="text" name="isbn" id="isbn" value="<?php echo $data->ISBN ;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
	</tr>
	<tr>
		<td><label>Borrower ID</label></td>
		<td><input type="text" name="borrower" class="form-control">
		<span class="text-danger"><?php echo form_error('borrower'); ?></span></td>

	</tr>
<tr>
	<td><label>Borrower type</label></td>
	<td><select name="type" class="form-control">
		<?php foreach($type as $ty): ?>
			<option value="<?php echo $ty['SYMBOL']; ?>"><?php echo $ty['TYPE']; ?></option>
		<?php endforeach; ?></select>
		<span class="text-danger"><?php echo form_error('status'); ?></span></td></tr>
	
	<tr>
		<td><label>Date of return</label></td>
		<td><input type="date" name="date" id="date" class="form-control">
		<span class="text-danger"><?php echo form_error('date'); ?></span></td>
	</tr>
	

	</tbody>

	</table>
			      
			     

			     <?php 
			      	echo '</div>';
			      	echo '<div class="modal-footer">';
			      			 echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
			      	
			        	echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      	</div>
			    </div>
		 	</div>
		</div>
			';
	
?>

 	 <?php echo form_close();?>

        </td>


	</tr>
	<?php
	}

	?>
	<?php else:?>
	<tr><td><h4>No record found</h4></td></tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
      &nbsp;

<div class="center">
   <?php echo $links;?>
  </div>


</div>



 
