
<div class="panel panel-default">
<div class="panel-body">
<table class="table">
	<thead>
		<tr>
			<th><span class="glyphicon glyphicon-book">&nbspRETURNED BOOKS</th>

</tr>

	<tr>
		<th><form class="form-inline" role="search" action="<?php echo base_url().'library/search_book';?>" method="POST">
        <div class="form-group">
         <input type="text" name="book" class="form-control" placeholder="Search type" id="search">
      </div>
      <button type="submit" class="btn btn-info">Search</button>
      </form></th>
    </tr>

	</thead>
</table>
<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-condensed table-hover table-responsive table-bordered">
<thead>
<tr class="text-info">
	<th>Code#</th>
	<th>Isbn</th>
	<th>Title</th>
	<th>Librarian code</th>
	<th>Customer#</th>
	<th>Date borrowed</th>
	<th>Expect return</th>
	<th>Date returned</th>
	<th>Status after</th>
	
	
<!-- 	<th style="color:green;">ACTIONS</th> -->

</tr>
</thead>
<tbody>

	<?php if(count($x)):?>
	<?php foreach ($x->result() as $row) {
		?>
<tr>		
<td><?php echo $row->ID;?></td>
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<!-- <td><?php echo $row->AUTHOR_NAME;?></td> -->
<td><?php echo $row->LIBRARIAN_ID;?></td>
<td><?php echo $row->BORROWER_ID;?></td>
<td><?php echo $row->DATE_BORROWED;?></td>
<td><?php echo $row->EXPECT_RETURN;?></td>
<td><?php echo $row->RETURN_DATE;?></td>

<td><?php echo $row->STATUS_AFTER;?></td>

<!-- <td>

<?php echo anchor("library/to_booktransct/{$row->ID}","Borrow");?>&nbsp</a><span>|</span>
<?php echo anchor("library/fill_editbook/{$row->ID}","Edit");?><span class="glyphicon glyphicon-pencil"></span>

</td> -->

</tr>

	<?php
	}

	?>
<?php else:?>
	<tr>No record found</tr>
<?php endif;?>

</tbody>
</table>
  </div>
  </div>
  </div>
  </div>








  