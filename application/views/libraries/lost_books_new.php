
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;List of Lost Copies &nbsp;</span>
        </div>
            <div class="db-header-extra form-inline"> 

<?php echo form_open(base_url() . 'library/lost_books'); ?>

      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                      <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search" ></i>
                         </button> 
                      </div>
      </div>
<?php echo form_close(); ?> 

            </div>
          </div>



<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
<thead >
<tr>
  <th>Code</th>
  <th>Isbn</th>
  <th>Title</th>
  <th>Author</th>
  
  
  
<!--  <th style="color:green;">ACTIONS</th> -->

</tr>
</thead>
<tbody>

  <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {
    ?>
<tr>    
<td><?php echo $row->ID;?></td>
<td><?php echo $row->ISBN;?></td>
<td><?php echo $row->BOOK_TITLE;?></td>
<td><?php echo $row->AUTHOR_NAME;?></td>
<!-- <td><?php echo $row->AUTHOR_NAME;?></td> -->

<!-- <td>

<?php echo anchor("library/to_booktransct/{$row->ID}","Borrow");?>&nbsp</a><span>|</span>
<?php echo anchor("library/fill_editbook/{$row->ID}","Edit");?><span class="glyphicon glyphicon-pencil"></span>

</td> -->

</tr>

  <?php
  }

  ?>
<?php else:?>
  <tr>No record found</tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
    <div class="center">
  <label class="label-control"><?php echo $links;?></label>

  </div> 
  </div>
  </div>
  </div>
  </div>


