
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;Out-dated Books &nbsp;</span>
        </div>
            <div class="db-header-extra form-inline"> 

               
    <?php echo form_open(base_url() . 'library/expired_books'); ?>
                
     <div class="input-group ">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
     <?php echo form_close(); ?> 
                            
                   
            </div>
          </div>
 

 <div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

  <div class="table table-responsive">
  <table class="table table-striped table-hover table-condensed table-responsive table-bordered">
    <tr>
      <th>#Code</th>
      <th>Isbn</th>
      <th>Title</th>
      <th>Cust-ID</th>
      <th>Names</th>
      <th>Expired since</th>
    </tr>
    
    <?php if(count($x)):?>
  <?php foreach ($x->result() as $row) {
    ?>
  <tr>
      <td><?php echo $row->ID;?></td>
      <td><?php echo $row->ISBN;?></td>
      <td><?php echo $row->BOOK_TITLE;?></td>
      <td><?php echo $row->BORROWER_ID;?></td>
      <td><?php echo $row->NAMES;?></td>
      <td><?php echo $row->TIME_LEFT;?>&nbspDays</td>
    </tr>
    <?php
  }

  ?>
<?php else:?>
  <tr><div class="alert alert-dismissible alert-success">
    <?php echo 'No record found!';?>
  </div></tr>
<?php endif;?>
  </table>
</div>

  </div>
  </div>








  






