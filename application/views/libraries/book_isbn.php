
 <div class="panel panel-default">
  <div class="panel-body">
    
 
 <?php if($error=$this->session->flashdata('response')):?>
  <div class="alert alert-dismissible alert-success">
    <?php echo $error;?>
    <a class="close" data-dismiss="alert" aria-label="close">x</a>
  </div>
<?php endif;?>

<table class="table">
<thead>	<tr>

</tr>

</thead>
</table>

<table>
	<tr class="text-info"><th> <h4><span class="badge"><?php echo $x ;?>&nbsp / &nbsp<?php echo $y;?></span>&nbsp</h4>
	</th>

	<th><strong><h4>Isbn &nbsp<?php echo $isbn;?>;&nbsp</h4></strong></th>
	<th><h4>Title-&nbsp<?php echo $title ;?>&nbsp ;</h4></th>
	<th><h4>Author-&nbsp<?php echo $author;?>&nbsp ; </h4></th>
	<th><h4>&nbspEdition-&nbsp<?php echo $edition;?> </h4></th>


	</tr>
</table>

<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
  <thead> <tr>
    <th>Code#</th>
	
	  <th class="col-xs-2 text-center">Action</th>
  
    </tr>
       </thead>
<tbody>

	<?php if(count($books)>0):?>
	<?php foreach ($books as $data) {
	
		?>
	<tr>
	    <td><?php echo $data->ID;?></td>
<td>
   
<?php
		
			echo '<a href="" class="fa fa-shopping-cart" aria-hidden="true" data-toggle="modal" data-target="#borrow_book'.$data->ID.'"
      data-toggle="tooltip" data-placement="right" title="Borrow this copy">
			</a> </span>';?>
<?php echo form_open('library/borrow_book');?>
			
		<?php		echo '<div id="borrow_book'.$data->ID.'" class="modal fade" tabindex="-1" role="dialog" >';
  			 echo '<div class="modal-dialog" >
			    <!-- Modal content-->';
			   echo  '<div class="modal-content" style="background-color:#DDDDDD">';
			    	echo '<div class="modal-header">';
			        	echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
			        	 //
			        	  echo '<h4 class="modal-title">Title:&nbsp '.$data->BOOK_TITLE.'</h4>'; 
			    	echo '</div>';
			      	 echo '<div class="modal-body">';?>
			      	 
			      	 <table class="table" style="background-color:#DDDDDD">
	<tbody>
	<tr>
		<td><label>Book #CODE</label></td>
		<td><input type="text" name="code" id="code" value="<?php echo $data->ID ?>" class="form-control">
		<span class="text-danger"><?php echo form_error('code'); ?></span></td>
	</tr>
	<tr>
		<td><label>Book ISBN</label></td>
		<td><input type="text" name="isbn" id="isbn" value="<?php echo $data->ISBN ;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
	</tr>
	<tr>
		<td><label>Borrower ID</label></td>
		<td><input type="text" name="borrower" class="form-control">
		<span class="text-danger"><?php echo form_error('borrower'); ?></span></td>

	</tr>
<tr>
	<td><label>Borrower type</label></td>
	<td><select name="type" class="form-control">
		<?php foreach($type as $ty): ?>
			<option value="<?php echo $ty['SYMBOL']; ?>"><?php echo $ty['TYPE']; ?></option>
		<?php endforeach; ?></select>
		<span class="text-danger"><?php echo form_error('status'); ?></span></td></tr>
	
	<tr>
		<td><label>Date of return</label></td>
		<td><input type="date" name="date" id="date" class="form-control">
		<span class="text-danger"><?php echo form_error('date'); ?></span></td>
	</tr>
	

	</tbody>

	</table>
			      
			     

			     <?php 
			      	echo '</div>';
			      	echo '<div class="modal-footer">';
			      			 echo '<button type="submit" class="btn btn-success" onclick="return confirmUpdate() data-dismiss="modal">Submit</button>';
			      	
			        	echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      	</div>
			    </div>
		 	</div>
		</div>
			';
	
?>

 	 <?php echo form_close();?>

        </td>


	</tr>
	<?php
	}

	?>
	<?php else:?>
	<tr><td><h4>No record found</h4></td></tr>
<?php endif;?>

</tbody>
</table>

 </div> 
 </div>
</div>
</div>



 