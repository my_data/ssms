
<div class="col-md-2"></div>
<div class="col-md-6" style="background-color:#DDDDDD">
<?php echo form_open('library/return_book');?>
<div class="panel panel-success"style="background-color:#DDDDDD">
<div class="panel-content" >

<div class="panel-heading">
		<h4>Title:&nbsp<?php echo $title;?>,&nbspAuthor:&nbsp<?php echo $author ;?>&nbsp Edition:<?php echo $edition;?></h4>

</div>
<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="panel-body">
 <input type="text" value="<?php echo $transaction_id;?>" name="tra" class="hidden">
 <table class="table">
<tr>
		<td><label>Book #CODE</label></td>
		<td><input type="text" name="code" id="code" value="<?php echo $code;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('code'); ?></span></td>
	</tr>
	<tr>
		<td><label>Book ISBN</label></td>
		<td><input type="text" name="isbn" id="isbn" value="<?php echo $isbn;?>" class="form-control">
		<span class="text-danger"><?php echo form_error('isbn'); ?></span></td>
	</tr>
     <tr>
		<td><label>Status after</label></td>
		<td><label><input type="checkbox" name="status" class="status" value="Normal" >Nomal&nbsp&nbsp<input type="checkbox" name="status" class="status" id="ava" value="bad">Bad</label>
		<span class="text-danger"><?php echo form_error('status'); ?></span></td>
		
	</tr>
	<tr>
		<td><label>Availability</label></td>
		<td><label><input type="checkbox" name="availability" class="ava" value="Returned" checked>Returned&nbsp&nbsp<input type="checkbox" name="availability" class="ava" value="lost">Lost</label>
		<span class="text-danger"><?php echo form_error('availability'); ?></span></td>
		
	</tr>

	</table>
	


</div>


</div>

</div>

<div class="form-group text-right">
 <a class="btn btn-md btn-danger" href="<?php echo base_url('library/borrowed/'); ?>">Cancel 
</a>
			<button type="submit" class="btn btn-success">Submit<br></button>
		</div>
<?php echo form_close();?>
</div>
<div class="col-md-2"></div>

  </div>
  </div>



