
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<span class="badge"><?php echo $x ;?>&nbsp;<!--  / &nbsp;<?php echo $y; ?> --></span>&nbsp;
    Title-&nbsp;<?php echo $title ;?>&nbsp; ;
    Author-&nbsp;<?php echo $author;?>&nbsp; ; 
    &nbsp;Edition-&nbsp;<?php echo $edition;?> 
</div>
<div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
                <?php echo form_open(base_url() . 'library/dept_borrow/'.$isbn); ?>
        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                    <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                    </div>
        </div>
                        <?php echo form_close(); ?> 
                            
</div> 

            	<a href="<?php echo base_url() . 'library/dept_borrow/'.$isbn; ?>" class="btn btn-primary btn-sm" title="department">D</a>&nbsp;&nbsp;
               <a href="<?php echo base_url() . 'library/retrieve_isbn/'.$isbn; ?>" class="btn btn-primary btn-sm" title="individual">I</a>

</div>
           
</div>

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-responsive table-bordered table-condensed">
  <thead> <tr>

  	<!-- START CHECKBOX CODE -->
	<?php $attr = array('role' => 'form',  'id' => 'selectAllForm'); ?>
	<?php echo form_open('library/borrow_book_department/'.$isbn, $attr); ?>
  	<th width="2%" class="text-center"><input type="checkbox" id="selectAllFormCheckBox" onclick="checkAll(selectAllForm)"></th>
    <th width="5%">Code#</th>
	
	  <th width="97%" class="col-xs-2 text-center">

		  <input type="date" name="date" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <select name="dept_id" required >
		  		<option value="">--Select Department--</option>
			  <?php foreach($departments as $dept): ?>
			  	<option value="<?php echo $dept['dept_id']; ?>"><?php echo $dept['dept_name']; ?></option>
			  <?php endforeach; ?>
		  </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input type="submit" name="borrow_books" class="btn btn-primary btn-sm" value="BORROW">
		  
	  </th>
  
    </tr>
       </thead>
<tbody>

	<?php if(count($books) > 0):?>
	<?php $x = 1; foreach ($books as $data) {
	
		?>
	<tr>
	
		<td class="text-center"><input type="checkbox" name="book_copies[]" id="<?php echo $data->ID; ?>" value="<?php echo $data->ID; ?>" ></td>
	
	    <td><?php echo $data->ID; ?></td>


<td class="text-center">

			



        </td>


	</tr>
	<?php
	}

	?>

	<!-- END CHECKBOX CODE -->
	<?php echo form_close(); ?>

	<?php else:?>
	<tr><td><h4>No record found</h4></td></tr>
<?php endif;?>

</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
      &nbsp;
<div class="center">
   <?php echo $links;?>
  </div>
</div>

</div>



 
