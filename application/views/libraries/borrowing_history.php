
<div class="panel panel-default">
  <div class="panel-body">

<table class="table table-striped table-hover table-responsive ">
	 <thead>
		<tr>
			<td><h4>BORROWING HISTORY</h4></td>


<td><form  role="search" action="<?php echo base_url().'library/searchhistory';?>" method="POST">
        <div class="col-md-12">  
    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search " name="searchhi" id="searchhi">
    <div class="input-group-btn">
      <button class="btn btn-primary" type="submit">
        <i class="glyphicon glyphicon-search " ></i>
      </button>
    </div>
  </div>
  </div>
  </form> 
  </td>
      <td>Dictionary:<label class="text-danger">lost=<span class="glyphicon glyphicon-remove"> </span></label>,
       <label class="text-success">returned=<span class="glyphicon glyphicon-ok"> </span> </label>,
       <label class="text-info"> Still out=<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> </label>
      
      </td>


</tr>

	
</thead>	
</table>
<?php if($error=$this->session->flashdata('response')):?>
  <div class="alert alert-dismissible alert-success">
    <?php echo $error;?>
    <a class="close" data-dismiss="alert" aria-label="close">x</a>
  </div>
<?php endif;?>

<div class="scrollable" id="borrowing-report" >

<table class="table table-striped table-hover table-responsive table-bordered table-condensed" >

<tr class="text-info">
	<th>Code#</th>
	<th>Isbn#</th>
	<!-- <th>TITLE</th> -->
	<!-- <th>LIBRARIAN ID</th> -->
	<th>Cus-id#</th>
    <th>Names</th>
	<th>Date_borrowed</th>
	<th>Expected-return</th>
	<th>Date-due back  </th>
	<th>Status after</th>
	
	
	
<!-- 	<th style="color:green;">ACTIONS</th> -->

</tr>



	<?php if(count($x)):?>
	<?php foreach ($x->result() as $row) {
		?>
<tr>		
<td><?php echo $row->ID;?></td>
<td><?php echo $row->ISBN;?></td>
<!-- <td><?php echo $row->BOOK_TITLE;?></td> -->
<!-- <td><?php echo $row->AUTHOR_NAME;?></td> -->
<!-- -->
<td><?php echo $row->BORROWER_ID;?></td>
<td><?php echo $row->NAMES;?></td> 
<td><?php echo $row->DATE_BORROWED;?></td>
<td><?php echo $row->RETURN_DATE;?></td>
<?php if($row->DATE_DUE_BACK==""):?>
	<td class="text-danger">Pending..</td>
		
<?php else:?>

<td><?php echo $row->DATE_DUE_BACK;?></td>
<?php endif;?>
<?php if($row->FLAG=='lost'):?>
<td class="text-danger"><span class="glyphicon glyphicon-remove"></span></td>
<?php elseif($row->FLAG=='returned'):?>
<td class="text-success"><span class="glyphicon glyphicon-ok"></span></td>
<?php else:?>
<td class="text-info"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i></td>
<?php endif;?>


<!-- <td>

<?php echo anchor("library/to_booktransct/{$row->ID}","Borrow");?>&nbsp</a><span>|</span>
<?php echo anchor("library/fill_editbook/{$row->ID}","Edit");?><span class="glyphicon glyphicon-pencil"></span>

</td> -->

</tr>

	<?php
	}

	?>
<?php else:?>
	<tr class="text-info"><td>No record matches</td></tr>
<?php endif;?>

</table>


</div>
<!-- <a href="#" id="print1" onclick="javascript:printreport1('borrowing-report')">Preview print</a>
  --> <div class="center">
   <?php echo $links;?>
  </div>    
</div>
</div>
</div>
</div>








  
