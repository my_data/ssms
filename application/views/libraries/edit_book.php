<div class="col-md-12">
<table class="table">
<thead>
	<tr>
	
	<th><h4><span class="glyphicon glyphicon-briefcase">&nbspNEW BOOK</h4></th>
	
<th><a href="<?php echo base_url('library/retbooktype');?>"><span class="glyphicon glyphicon-briefcase"></span>&nbspGenre Log
</a></th>

<td><a href="<?php echo base_url('library/book1_report');?>"><span class="glyphicon glyphicon-book"></span> &nbspBook Log</a></th>
</tr>
</thead>
</table>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('response')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('response'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class=" col-md-2"></div>
<div class="col-md-6">

	<div class="panel-primary" id="addbpanel">
		<div class="panel-body">
		
		<h3 id="bookheader">Edit Book</h3>
			<?php echo form_open('library/add_book');?>
			<table class="table">
			<tr>
				<td><label>BOOK #CODE</label></td>
				<td><input type="text" name="code" class="form-control" value="<?php echo $y->ID;?>">
				<span><?php echo form_error('code');?></span></td>
			</tr>
			<tr>
				<td><label>ISBN</label></td>
				<td><input type="text" name="isbn" value="<?php echo $y->isbn;?>"class="form-control" readonly/>
				<span><?php echo form_error('isbn');?></span></td>
				
			</tr>
			
				<td><label>BOOK STATUS</label></td>
		<td><label><input type="radio" name="status" value="good" enabled>good&nbsp&nbsp<input type="radio" name="status" value="bad">bad</label>
		<span class="text-danger"><?php echo form_error('status'); ?></span></td>
		
			</tr>
			
		    
			
				</table>
				<div class="group text-right">
				<button class="btn btn-success" type="submit"class="form-control" onclick="return confirmAdd()">Add</button>
				</div>
			
			
			<?php echo form_close();?>
		</div>
		
	</div>
	</div>


<div class="col-md-2"></div>
<!--book type view list-->
 



