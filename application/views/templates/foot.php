</div>
</div>
</div>

    <!-- Global variable to be used in all scripts -->
    <script>var base_url = '<?php echo base_url(); ?>' </script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- jquery -->
    <script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!--Datatables -->
    <script src="<?php echo base_url(); ?>assets/datatables/dataTables.min.js"></script>
    <!-- Bootstrap Multiselect -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/bootstrap-multiselect.js"></script>
     <!-- Custom Javascript -->
    <script src="<?php echo base_url(); ?>assets/global.js"></script>
    <!--  <script src="<?php echo base_url(); ?>custom/js/ckeditor.js"></script> -->
    <script src="http://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <!--BOOTSTRAP SWITCH-->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-switch.min.js"></script>
    <!--END BOOTSTRAP SWITCH-->
    <!-- CHOSEN JS -->
    <script src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.js"></script>
    <!-- END CHOSEN JS -->
    <!--GOOGLE CHARTS-->
   <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <!--END GOOGLE CHARTS-->

     <!-- Code for enabling ckeditor -->
  <script type="text/javascript">
    //CKEDITOR.replace('inci-area');
  </script>
   <!-- End Code for enabling ckeditor -->

   <!-- Code for select chosen -->
  <script type="text/javascript" language="javascript">
    $('.chosen-select').chosen();
    $('button').click(function(){
      $('.chosen-select').val('').trigger("chosen:updated");
    });
  </script>
   <!-- End Code for select chosen -->

   <!--Tooltip MF-->
   <script type="text/javascript">
     $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
     });
   </script>
   <!--Tooltip MF-->

   <!-- PRINTING SCRIPT -->
   <script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    </script>
   <!-- END PRINT SCRIPT -->

   <!--Radio Button Modal-->
  <!--  <script type="text/javascript">
     $('#myModalTransfer').modal('show');
      $('input[type=radio]').click(function() {
        alert($(this).val());
      });
   </script> -->
   <!--Radion Button Modal-->

   <!-- SEARCHING ON KEY UP -->
   <!-- <script type="text/javascript" language="javascript">
     $(document).ready(function() {
        var search = $("#searchy");

            search.keyup(function() {
                if (search.val() != '') {
                    $.post("students/add_selected", { search : search.val()}, function(data) {
                        $(".result").html(data);
                    });
                }
            });
    });
   </script> -->
   <!-- END SEARCHING ON KEY UP -->

</body>
</html>

<!---->
<script>
    checkbox_checked = false;
    function checkAll(selectAllForm){
      var check_id = document.getElementById("selectAllForm");
      if(checkbox_checked == false){
        checkbox_checked = true;
      }
      else{
        checkbox_checked = false;
      }
      for(var i=0; i<check_id.elements.length; i++){
        check_id.elements[i].checked = checkbox_checked;
      }
    }
  </script>
<!---->

<!-- ROLE CHECKBOX-->
<script>
    checkbox_checked = false;
    function selectAll(selectForm){
      var check_id = document.getElementById("selectForm");
      if(checkbox_checked == false){
        checkbox_checked = true;
      }
      else{
        checkbox_checked = false;
      }
      for(var i=0; i<check_id.elements.length; i++){
        check_id.elements[i].checked = checkbox_checked;
      }
    }
  </script>
<!--END ROLE -->

<!-- START TIMETABLE SCRIPTS -->
<script type="text/javascript">
  function concatenate_for_filter(x, y){
    var class_id = document.getElementById('class_name').value;
    var stream = document.getElementById('stream').value;

    return document.getElementById('csid').value = class_id + stream;
  }
</script>

<script type="text/javascript">
  function concatenate(x, y){
    var class_id = document.getElementById('class_id').value;
    var stream_id_name = document.getElementById('stream_id_name').value;

    stream = stream_id_name.substring(0, stream_id_name.indexOf('-'));
    return document.getElementById('concatenate_csid').value = class_id + stream;
  }
</script>


<script type="text/javascript">
  //Special for class_stream_subjects
  function concatenate_x(x, y){
    var class_id = document.getElementById('class_id').value;
    var stream = document.getElementById('stream_id').value;

    return document.getElementById('concatenate_csid').value = class_id + stream;
  }
</script>


<script type="text/javascript">
  function checkTimetableMonday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0); 
    var period_no = id.substring(0, id.indexOf('-'));

    //alert(id.substring(0, id.indexOf('-')));
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetable', {id: new_value}, function(data) {
        $("#mon"+period_no).html(data);
      });
    }
  }


  function checkTimetableTuesday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetable', {id: new_value}, function(data) {
        $("#tue"+period_no).html(data);
      });
    }
  }

  function checkTimetableWednesday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetable', {id: new_value}, function(data) {
        $("#wed"+period_no).html(data);
      });
    }
  }

  function checkTimetableThursday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetable', {id: new_value}, function(data) {
        $("#thu"+period_no).html(data);
      });
    }
  }

  function checkTimetableFriday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetable', {id: new_value}, function(data) {
        $("#fri"+period_no).html(data);
      });
    }
  }



//Check When Editing

  function checkTeacherInTimetableWhenEditingMonday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetableWhenEditing', {id: new_value}, function(data) {
        $("#mon"+period_no).html(data);
      });
    }
  }


  function checkTeacherInTimetableWhenEditingTuesday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetableWhenEditing', {id: new_value}, function(data) {
        $("#tue"+period_no).html(data);
      });
    }
  }

  function checkTeacherInTimetableWhenEditingWednesday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetableWhenEditing', {id: new_value}, function(data) {
        $("#wed"+period_no).html(data);
      });
    }
  }

  function checkTeacherInTimetableWhenEditingThursday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetableWhenEditing', {id: new_value}, function(data) {
        $("#thu"+period_no).html(data);
      });
    }
  }

  function checkTeacherInTimetableWhenEditingFriday(getID){
    var value = document.getElementById(getID).value;
    var id = document.getElementById(getID).id;

    var new_value = id + '-' + value;  
    //var period_no = id.charAt(0);
    var period_no = id.substring(0, id.indexOf('-'));
 

    //alert(new_value);
    if($.trim(id) != ''){
      $.post(base_url+'classes/checkTeacherInTimetableWhenEditing', {id: new_value}, function(data) {
        $("#fri"+period_no).html(data);
      });
    }
  }
//End of check when editing
</script>

<!-- END TIMETABLE SCRIPTS -->

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function() {
      $('#successMessage').fadeOut('fast');
    }, 2000);
  });

  $(document).ready(function(){
    setTimeout(function() {
      $('#errorMessage').fadeOut('fast');
    }, 2000);
  });

  $(document).ready(function(){
    setTimeout(function() {
      $('#warningMessage').fadeOut('fast');
    }, 2000);
  });

 /* $(document).ready(function(){
    setTimeout(function() {
      $('.timetableErrorMessage').fadeOut('fast');
    }, 10000);
  });*/

</script>

<script type="text/javascript" language="javascript">
   /*// Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Borrowed',<?php echo $borrowed;?>],
          ['Available',<?php echo $available;?>],
          ['Lost', <?php echo $lost;?>]
         
        ]);

        // Set chart options
        var options = {'title':'Book Copy transactional analysis (<?php echo $all;?>)',
                       'width':400,
                       'height':400,
                       pieStartAngle: 100,
                       is3D:true
                        
                     };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
//end of general analysis report chat
//start of analysis status

*/


</script>
 
 <script type="text/javascript">
/*
     // Load the Visualization API and the corechart package.
 google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
 google.charts.setOnLoadCallback(drawChart);

function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Poor Copies',<?php echo $poor;?>],
          ['Normal Copies',<?php echo $normal;?>]
         
        ]);

        // Set chart options
        var options = {'title':'Book Copy Conditional analysis (<?php echo $x;?>) :lost excluded',
                       'width':400,
                       'height':400,
                       pieStartAngle: 100,
                       is3D:true
                       
                    };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_cond'));
        chart.draw(data, options);
      }*/

</script>


<!-- CONFIRM PASSWORD -->
<script type="text/javascript" language="javascript">
//KILWANI SCRIPTS
 /*$('.date').datepicker({
       format: 'yyyy-mm-dd'
     });

    $(".status").change(function(){
  $(".status").prop('checked',false);
  $(this).prop('checked',true);
});

$(".ava").change(function(){
  $(".ava").prop('checked',false);
  $(this).prop('checked',true);
});*/
//END KILWANI SCRIPTS
   </script>
<!-- END CODE -->

<!-- CHECKING CODE -->
   <script type="text/javascript" language="javascript">
     $(document).ready(function() {
        var current_password = $("#current_password1");
            current_password.keyup(function() {
                if (current_password.val() != '') {
                    $.post("check_password_accuracy", { current_password : current_password.val()}, function(data) {
                        $(".password_result").html(data);
                    });
                }
            });
    });
   </script>
   <!-- END CODE -->

<!-- SEARCHING ON KEY UP -->
   <script type="text/javascript" language="javascript">
     $(document).ready(function() {
        var search = $("#search");
            search.keyup(function() {
                if (search.val() != '') {
                    $.post("check_existance", { search : search.val()}, function(data) {
                        $(".result").html(data);
                        //$(".result").html(search.val());
                    });
                }
            });
    });
   </script>
   <!-- END SEARCHING ON KEY UP -->

   <!-- SEARCHING ON KEY UP LIBRARY -->
   <script type="text/javascript" language="javascript">
     $(document).ready(function() {
        var search = $("#search");
            search.keyup(function() {
                if (search.val() != '') {
                    $.post(base_url+"library/check_borrower", { search : search.val()}, function(data) {
                        $(".result_borrower").html(data);
                        //$(".result").html(search.val());
                    });
                }
            });
    });
   </script>
   <!-- END SEARCHING ON KEY UP LIBRARY -->


<script type="text/javascript" language="javascript"> 

  //Code for select class and stream dropdowns in new_timetable.php
  function sss(){
    var class_name = document.getElementById('class_name').value;
    var stream = document.getElementById('stream').value;

    //var v = document.getElementById('monday');
    //alert(v.value);


  }

  //Please WORK.. I want to leave this place!!!!!!!!!!++++++++++++++++++++=============
  //Special for checkboxes in student attendance
    /*for (var i = 0; i <= 10; i++) {
      $(".chb"+i).change(function() {
          $(".chb"+i).prop('checked', false);
          $(this).prop('checked', true);
      });
    }*/


    $(".chb"+1).change(function() {
          $(".chb"+1).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+2).change(function() {
          $(".chb"+2).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+3).change(function() {
          $(".chb"+3).prop('checked', false);
          $(this).prop('checked', true);
      });

     $(".chb"+4).change(function() {
          $(".chb"+4).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+5).change(function() {
          $(".chb"+5).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+6).change(function() {
          $(".chb"+6).prop('checked', false);
          $(this).prop('checked', true);
      });

     $(".chb"+7).change(function() {
          $(".chb"+7).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+8).change(function() {
          $(".chb"+8).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+9).change(function() {
          $(".chb"+9).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+10).change(function() {
          $(".chb"+10).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+11).change(function() {
          $(".chb"+11).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+12).change(function() {
          $(".chb"+12).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+13).change(function() {
          $(".chb"+13).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+14).change(function() {
          $(".chb"+14).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+15).change(function() {
          $(".chb"+15).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+16).change(function() {
          $(".chb"+16).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+17).change(function() {
          $(".chb"+17).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+18).change(function() {
          $(".chb"+18).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+19).change(function() {
          $(".chb"+19).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+20).change(function() {
          $(".chb"+20).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+21).change(function() {
          $(".chb"+21).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+22).change(function() {
          $(".chb"+22).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+23).change(function() {
          $(".chb"+23).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+24).change(function() {
          $(".chb"+24).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+24).change(function() {
          $(".chb"+24).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+25).change(function() {
          $(".chb"+25).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+26).change(function() {
          $(".chb"+26).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+27).change(function() {
          $(".chb"+27).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+28).change(function() {
          $(".chb"+28).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+29).change(function() {
          $(".chb"+29).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+30).change(function() {
          $(".chb"+30).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+31).change(function() {
          $(".chb"+31).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+32).change(function() {
          $(".chb"+32).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+33).change(function() {
          $(".chb"+33).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+34).change(function() {
          $(".chb"+34).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+35).change(function() {
          $(".chb"+35).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+36).change(function() {
          $(".chb"+36).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+37).change(function() {
          $(".chb"+37).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+38).change(function() {
          $(".chb"+38).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+40).change(function() {
          $(".chb"+40).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+41).change(function() {
          $(".chb"+41).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+42).change(function() {
          $(".chb"+42).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+43).change(function() {
          $(".chb"+43).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+44).change(function() {
          $(".chb"+44).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+45).change(function() {
          $(".chb"+45).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+46).change(function() {
          $(".chb"+46).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+47).change(function() {
          $(".chb"+47).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+48).change(function() {
          $(".chb"+48).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+49).change(function() {
          $(".chb"+49).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+50).change(function() {
          $(".chb"+50).prop('checked', false);
          $(this).prop('checked', true);
      });

    //Check this OUT,  IT MIGHT NOT WORK
    function display(){
      var class_journal_form = document.getElementById('class_journal');
      var button_show_hide = document.getElementById('hide_button');

      if(class_journal_form.style.display === "none"){
        //class_journal_form.style.display = "block";
        //button_show_hide.style.display = "none";
      }
      else{
        //class_journal_form.style.display = "none";
        //button_show_hide.style.display = "block";
      }
    }

    function hide(){
      var btn = document.getElementById('save_button');
      alert(btn);
    }


</script>


   <!-- FILTER ON CHANGE ALUMNI: NOTE: IT HAS NOT BEEN USED -->
   <script type="text/javascript" language="javascript">
    function filterByYear(){
      var value = document.getElementById("filter_alumni_by_year").value;

      if(value != ''){
        $.post(base_url+'students/kinabo', {filter_year: value}, function(data) {
          $(".xxx").html(data);
        });
      }
    }
   </script>
   <!-- END FILTER ON CHANGE ALUMNI -->


<script type="text/javascript" language="javascript">
   // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Borrowed',<?php echo $borrowed;?>],
          ['Available',<?php echo $available;?>],
          ['Lost', <?php echo $lost;?>]
         
        ]);

        // Set chart options
        var options = {'title':'Book Copy transactional analysis (<?php echo $all;?>)',
                        pieHole: 0.4,
                        
                     };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
//end of general analysis report chat
//start of analysis status




</script>
 
 <script type="text/javascript">

     // Load the Visualization API and the corechart package.
 google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
 google.charts.setOnLoadCallback(drawChart);

function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Poor Copies',<?php echo $poor;?>],
          ['Normal Copies',<?php echo $normal;?>]
         
        ]);

        // Set chart options
        var options = {'title':'Book Copy Conditional analysis (<?php echo $x;?>) :lost excluded',
                         pieHole: 0.4,
                       
                    };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_cond'));
        chart.draw(data, options);
      }

</script>
