</div>
  </div>


  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- jquery -->
    <script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!--Datatables -->
    <script src="<?php echo base_url(); ?>assets/datatables/dataTables.min.js"></script>
    <!-- Bootstrap Multiselect -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/bootstrap-multiselect.js"></script>
     <!-- Custom Javascript -->
    <script src="<?php echo base_url(); ?>assets/global.js"></script>
    <!-- Form Validation Javascript -->
    <script src="<?php echo base_url(); ?>custom/js/form_validation.js"></script>
    <!--BOOTSTRAP SWITCH-->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-switch.min.js"></script>
    <!--END BOOTSTRAP SWITCH-->

    <!-- CHOSEN JS -->
    <script src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.js"></script>
    <!-- END CHOSEN JS -->

    <!-- HTML Editor -->
    <!--  <script src="<?php echo base_url(); ?>custom/js/ckeditor.js"></script> -->
    <script src="http://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

     <!-- Code for enabling ckeditor -->
  <script type="text/javascript">
    CKEDITOR.replace('inci-area');
  </script>
   <!-- End Code for enabling ckeditor -->

   <!-- Code for select chosen -->
  <script type="text/javascript" language="javascript">
    $('.chosen-select').chosen();
    $('button').click(function(){
      $('.chosen-select').val('').trigger("chosen:updated");
    });
  </script>
   <!-- End Code for select chosen -->

   <script type="text/javascript">
     $("[name='my-checkbox']").bootstrapSwitch();
   </script>

   <!--Tooltip MF-->
   <script type="text/javascript">
     $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
     });
   </script>
   <!--Tooltip MF-->

   <!--For Disabled text field-->
   <script type="text/javascript">
     $(document).ready(function(){
        $('input[type="radio"]').click(function(){
            var inputValue = $(this).attr("value");
            var targetBox = $("." + inputValue);
            $(".box").not(targetBox).hide();
            $(targetBox).show();
        });
    });
   </script>

   <script type="text/javascript">
      $(document).ready(function(){
        setTimeout(function() {
          $('#successMessage').fadeOut('fast');
        }, 2000);
      });
    </script>
</body>
</html>

<!-- START CLASS STREAM -->
<script type="text/javascript">
  function concatenate(x, y){
    var class_id = document.getElementById('class_id').value;
    var stream_id_name = document.getElementById('stream_id_name').value;

    stream = stream_id_name.substring(0, stream_id_name.indexOf('-'));
    return document.getElementById('concatenate_csid').value = class_id + stream;
  }
</script>
<!-- END CLASS STREAM -->


<script type="text/javascript" language="javascript">
  //Code for Departments
  $(document).ready(function(){
    var dataTable = $('#department_data').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'departments/fetch_department'; ?>",
        type:"POST"
      },
      "columnDefs":[
        {
          "targets":[0, 2, 3, 4, 5, 6],
          "orderable":false
        }
      ]
    });
  });

  //Code for Staffs
  $(document).ready(function(){
    var dataTable = $('#staff_data').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'staffs/fetch_staff'; ?>",
        type:"POST"
      },
      "columnDefs":[
        {
          "targets":[0, 3, 4, 5],
          "orderable":false
        }
      ]
    });
  });

  //Code for Teachers
  $(document).ready(function(){
    var dataTable = $('#teacher_data').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'teachers/fetch_teacher'; ?>",
        type:"POST"
      },
      "columnDefs":[
        {
          "targets":[0, 3, 4],
          "orderable":false
        }
      ]
    });
  });

  //Code for Subjects
  $(document).ready(function(){
    var dataTable = $('#subject_data').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'subjects/fetch_subject'; ?>",
        type:"POST"
      },
      "columnDefs":[
        {
          "targets":[0, 2, 3],
          "orderable":false
        }
      ]
    });
  });


   //Code for Students
  $(document).ready(function(){
    var dataTable = $('#student_data').DataTable({
      /*"rowCallback": function(row, data, index){
          if (data[3] >= 30) {
             jQuery(row).hide();
          }
       }*/
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'students/fetch_student'; ?>",
        type:"POST"/*,
        data:{
          "WHERE":"admission_no > "+"STD0010"
        }*/
      },
      "columnDefs":[
        {
          "targets":[0, 3, 4, 5],
          "orderable":false
        }
      ]
    });

   /* var names = dataTable.rows( function ( idx, data, node ) {
        return data.firstname.charAt(0) === 'K' ? true : false;
    }).data();*/
  });

  //Code for classes
  $(document).ready(function(){
    var dataTable = $('#class_data').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'classes/fetch_classes'; ?>",
        type:"POST"
      },
      "columnDefs":[
        {
          "targets":[0, 1, 2, 3, 4, 5, 6],
          "orderable":false
        }
      ]
    });
  });

  //Code for class_journal
  $(document).ready(function(){
    var count = 1;
    $('#add').click(function(){
      count = count + 1;
      var html_code = "<tr id='row"+count+"'>";
      html_code += "<td contenteditable='true' class='period_no'></td>";
      html_code += "<td contenteditable='true' class='time'></td>";
      html_code += "<td contenteditable='true' class='subject'></td>";
      html_code += "<td contenteditable='true' class='subject_teacher'></td>";
      html_code += "<td contenteditable='true' class='topic_taught'></td>";
      html_code += "<td contenteditable='true' class='sub_topic'></td>";
      html_code += "<td contenteditable='true' class='absentees'></td>";
      html_code += "<td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove'>-</button></td>";
      html_code += "</tr>";
      $('#class_journal_table').append(html_code);
    });

    $(document).on('click', '.remove', function(){
      var delete_row = $(this).data("row");
      $('#'+delete_row).remove();
    });

    $('#save').click(function(){
      var period_no = [];
      var time = [];
      var subject = [];
      var subject_teacher = [];
      var topic_taught = [];
      var sub_topic = [];
      var absentees = [];

      $('.period_no').each(function(){
        period_no.push($(this).text());
      });
      $('.time').each(function(){
        time.push($(this).text);
      });
      $('.subject').each(function(){
        subject.push($(this).text());
      });
      $('.subject_teacher').each(function(){
        subject_teacher.push($(this).text);
      });
      $('.topic_taught').each(function(){
        topic_taught.push($(this).text());
      });
      $('.sub_topic').each(function(){
        sub_topic.push($(this).text);
      });
      $('.absentees').each(function(){
        absentees.push($(this).text);
      });
      $.ajax({
        url:"<?php echo base_url() . 'classes/insert_class_journal'; ?>",
        method:"POST",
        data:{period_no:period_no, time:time, subject:subject, subject_teacher:subject_teacher, topic_taught:topic_taught, sub_topic:sub_topic, absentees:absentees},
        success:function(data){
          $("td[contenteditable='true']").text("");
          for(var i=2; i<=count; i++){
             $('tr#'+i+'').remove();
          }
        }
      });
    });
  });

  //Code for select class and stream dropdowns in new_timetable.php
  function sss(){
    var class_name = document.getElementById('class_name').value;
    var stream = document.getElementById('stream').value;

    //var v = document.getElementById('monday');
    //alert(v.value);


  }

  $(document).ready(function(){
    for(var i=0; i<10; i++){
      $('#student_name'+i).multiselect({
        nonSelectedText: 'Select Student',
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonWidth: '300px'
      });
    }

   /* $('#class_journal_form').on('submit', function(event){
      event.preventDefault();
      var form_data = $(this).serialize();
      $.ajax({
        url:"classes/save_class_journal",
        method:"POST",
        data:form_data,
        success:function(data){
          $('#student_name1 option:selected').each(function(){
            $(this).prop('selected', false);
          });
          $('#student_name1').multiselect('refresh');
          alert(data);
        }
      })
    });*/
  });

  $(document).ready(function(){
    $('#absent_student').multiselect({
      nonSelectedText: 'Select Student',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px'
    });

    $('#sick_student').multiselect({
      nonSelectedText: 'Select Student',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px'
    });

    $('#permitted_student').multiselect({
      nonSelectedText: 'Select Student',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px'
    });

    $('#home_student').multiselect({
      nonSelectedText: 'Select Student',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px'
      // includeSelectAllOption: true,
      // selectAllValue: 'select-all-value'
    });
  });

  $(document).ready(function(){
    $('#absent_staff').multiselect({
      nonSelectedText: 'Select Staff',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px',
      maxHeight: 200
    });

    $('#sick_staff').multiselect({
      nonSelectedText: 'Select Staff',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px',
      maxHeight: 200
    });

    $('#permitted_staff').multiselect({
      nonSelectedText: 'Select Staff',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '500px',
      maxHeight: 200
    });
  });

  //Discipline dropdown
   $(document).ready(function(){
    $('#discipline').multiselect({
      nonSelectedText: 'Select Student',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '100%',
      maxHeight: 300
    });
  });

   //Used when registering staff (teacher) dropdown
   $(document).ready(function(){
    $('#select_subjects').multiselect({
      nonSelectedText: 'Select Subject',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '190px',
      maxHeight: 200
    });
  });

   //Used in teaching_assignment_form
   $(document).ready(function(){
    $('#stream_teaching_assignment').multiselect({
      nonSelectedText: 'Choose Stream',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '200px',
      maxHeight: 200
    });
  });

   //Used in adding subject to department.. file..: "view_depts.php"

   $(document).ready(function(){
    $('#add_new_subject').multiselect({
      nonSelectedText: 'Choose Subject',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth: '200px',
      maxHeight: 200
    });
  });



   //+++++++++++++++++++++++++++++++++++++++++++++++++//
    //Code for Students
  $(document).ready(function(class_id){
    var dataTable = $('#std_data').DataTable({
      "processing":true,
      "serverSide":true,
     // "dataType":json,
      "order":[],
      "ajax":{
        url:"<?php echo base_url() . 'students/fetch_student/'; ?>'"+class_id,
        type:"POST"//,
        
      },
      "columnDefs":[
        {
          "targets":[0, 3, 4, 5],
          "orderable":false
        }
      ]
    });
  });



  //Try this
  $(document).ready(function(class_id = null){      
    $.ajax({
      url: "<?php echo base_url() . 'students/fetch_student_by_class/'; ?>'"+class_id,
      type:"POST",
      dataType: "json",
      success:function(data){
        $("#result").html(response.html);

        manageStudentTable = $("#manageStudentTable").DataTable({
          'ajax' : 'students/fetch_student_by_class/'+class_id,
          'order' : []
        });
      }
    });
  });

  //Please WORK.. I want to leave this place!!!!!!!!!!++++++++++++++++++++=============
  //Special for checkboxes in student attendance

    $(".chb"+1).change(function() {
          $(".chb"+1).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+2).change(function() {
          $(".chb"+2).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+3).change(function() {
          $(".chb"+3).prop('checked', false);
          $(this).prop('checked', true);
      });

     $(".chb"+4).change(function() {
          $(".chb"+4).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+5).change(function() {
          $(".chb"+5).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+6).change(function() {
          $(".chb"+6).prop('checked', false);
          $(this).prop('checked', true);
      });

     $(".chb"+7).change(function() {
          $(".chb"+7).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+8).change(function() {
          $(".chb"+8).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+9).change(function() {
          $(".chb"+9).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+10).change(function() {
          $(".chb"+10).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+11).change(function() {
          $(".chb"+11).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+12).change(function() {
          $(".chb"+12).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+13).change(function() {
          $(".chb"+13).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+14).change(function() {
          $(".chb"+14).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+15).change(function() {
          $(".chb"+15).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+16).change(function() {
          $(".chb"+16).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+17).change(function() {
          $(".chb"+17).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+18).change(function() {
          $(".chb"+18).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+19).change(function() {
          $(".chb"+19).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+20).change(function() {
          $(".chb"+20).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+21).change(function() {
          $(".chb"+21).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+22).change(function() {
          $(".chb"+22).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+23).change(function() {
          $(".chb"+23).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+24).change(function() {
          $(".chb"+24).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+24).change(function() {
          $(".chb"+24).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+25).change(function() {
          $(".chb"+25).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+26).change(function() {
          $(".chb"+26).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+27).change(function() {
          $(".chb"+27).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+28).change(function() {
          $(".chb"+28).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+29).change(function() {
          $(".chb"+29).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+30).change(function() {
          $(".chb"+30).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+31).change(function() {
          $(".chb"+31).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+32).change(function() {
          $(".chb"+32).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+33).change(function() {
          $(".chb"+33).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+34).change(function() {
          $(".chb"+34).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+35).change(function() {
          $(".chb"+35).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+36).change(function() {
          $(".chb"+36).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+37).change(function() {
          $(".chb"+37).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+38).change(function() {
          $(".chb"+38).prop('checked', false);
          $(this).prop('checked', true);
      });
     $(".chb"+40).change(function() {
          $(".chb"+40).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+41).change(function() {
          $(".chb"+41).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+42).change(function() {
          $(".chb"+42).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+43).change(function() {
          $(".chb"+43).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+44).change(function() {
          $(".chb"+44).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+45).change(function() {
          $(".chb"+45).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+46).change(function() {
          $(".chb"+46).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+47).change(function() {
          $(".chb"+47).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+48).change(function() {
          $(".chb"+48).prop('checked', false);
          $(this).prop('checked', true);
      });
    $(".chb"+49).change(function() {
          $(".chb"+49).prop('checked', false);
          $(this).prop('checked', true);
      });

    $(".chb"+50).change(function() {
          $(".chb"+50).prop('checked', false);
          $(this).prop('checked', true);
      });

    //Check this OUT,  IT MIGHT NOT WORK
    function display(){
      var class_journal_form = document.getElementById('class_journal');
      var button_show_hide = document.getElementById('hide_button');

      if(class_journal_form.style.display === "none"){
        //class_journal_form.style.display = "block";
        //button_show_hide.style.display = "none";
      }
      else{
        //class_journal_form.style.display = "none";
        //button_show_hide.style.display = "block";
      }
    }

    function hide(){
      var btn = document.getElementById('save_button');
      alert(btn);
    }
</script>