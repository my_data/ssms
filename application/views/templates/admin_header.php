<!DOCTYPE html>
<html lang="en">
<head><link rel='icon' href="<?php echo base_url() . 'assets/images/129.jpg'; ?>" width='50%' />
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Admin</title>
      <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" >
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/font-awesome.min.css">
      <!-- Datatables css-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/datatables/dataTables.min.css">
      <!-- Bootstrap Multiselect CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/bootstrap-multiselect.css">

      <!--BOOTSTRAP SWITCH-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-switch.min.css">
    <!--END BOOTSTRAP SWITCH-->

      <!-- CHOSEN CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.min.css">
      <!-- END CHOSEN CSS -->

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      
      <style>
      .nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  display: inline-block;
  top: 0px;
  width: 260px;
  height: 100%;
  color: #e1ffff;
  padding-top: 50px;

}

/*MY CUSTOM*/
.table {}
.table-header { font-weight: 600; background: #f0f0f0; }

.white-area-content { background: #FFFFFF; border-radius: 4px; padding: 15px; border: 1px solid #d4dce2;}
.white-link { color: #FFFFFF; }
.white-link:hover { color: #DCE8E8; text-decoration: none;}


.db-header { border-bottom: 1px solid #d3d3d3; margin-bottom: 20px; padding-bottom: 1px;}
.db-header-title { font-size: 18px; float: left;}
.page-header-title { font-size: 16px; float: left;}
.db-header-extra { float: right; padding: 5px;  }

/*END MY CUSTOM*/

.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: none;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
  padding-left: 20px;
}

@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;

  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
    
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}

@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
     position: relative; 
  }
}

body {
  margin: 0px;
  padding-left: 0px;
  padding-top: 50px;    /*Carefull in padding-top*/
  padding-right: 50px;
}

.success_message_color{
  color: #00FF00;
}

.error_message_color{
  color: #FF0000;
}

.box{
    /*color: #fff;
    padding: 20px;*/
    display: none;
/*    margin-top: 20px;*/
}

.add_score{

}

#form_headers_styles{
  /*text-align: text-center;*/
  font-family: serif;
  /*background-color: #4f5b69;*/
  /*font-weight: bold;*/
}

/*.user_role_button { border: 1px solid #1e8ed0; font-weight: 600; border-radius: 4px; padding: 5px; font-size: 11px; margin: 1px; margin-bottom: 2px; vertical-align: top; display: inline-block;}
.user_role_button:hover { background: #e3efff; }
.user_role_button.admin { border: 1px solid #1bbc42 !important; color: #1bbc42; }
.user_role_button.project { border: 1px solid #1e8ed0 !important; color: #1e8ed0; }
.user_role_button.client { border: 1px solid #e46825 !important; color: #e46825;}
.user_role_button.banned { border: 1px solid #d00003 !important; color: #d00003;}*/

      </style>
      <script type="text/javascript">
        /*function persist_select_element() {
          document.getElementById('monthValue').value = "<?php echo $_POST['monthValue'];?>";
        }
*/      </script>

  </head>
<body onLoad="hide_element('select_element')" >

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-content" id="buton">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <a class="navbar-brand" href="#"> 
        Signed in as: 
        <?php 
           echo $this->session->userdata('user_role');

           //Kamata academic year zote
           $years = $this->session->userdata("miaka");

           //Kamata past academic year zote
           $past_years = $this->session->userdata("past_miaka");
        ?>        
      </a>
      
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url() . 'main/logout/admin'; ?>"><span class="glyphicon glyphicon-log-out">&nbsp;Logout</span></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-bell"></span></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-cog"></span></a></li>

        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="row">
  <div class="col-md-3">
    <div class="nav-side-menu">
    <div class="brand">Brand Logo</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse ">
                <li>
                  <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
                </li>
                 <li  data-toggle="collapse" data-target="#admin_panel" class="collapsed ">
                  <a href="#"><i class="fa fa-cog fa-lg"></i> Admin Settings  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="admin_panel">
                    <li ><a href="">Global Settings</a></li>
                    <!-- <li ><a href="<?php echo base_url() . 'admin/section_settings'; ?>">Section Settings</a></li>
 -->                    <li ><a href="<?php echo base_url() . 'admin/roles'; ?>">Manage Roles</a></li>
                    <!-- <li ><a href="">Staff Roles</a></li> -->
                    <li ><a href="<?php echo base_url() . 'admin/groups'; ?>">Staff Groups</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/admin_groups'; ?>">Admin Groups</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/activity_log_records'; ?>">Log Events</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/groups'; ?>">Audit Trail</a></li>                           
                </ul>
                <li class="active">
                  <a href="#">
                  <i class="fa fa-users fa-lg" ></i> Announcement
                  </a>
                </li>

                 <!-- <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Role
                  </a>
                </li> -->

                <li data-toggle="collapse" data-target="#Exam" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Exams <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="Exam">
                  <!-- <li>Add Exam Type</li> -->
                  <li><a href="<?php echo base_url() . 'admin/exam_types'; ?>">Exam Types</a></li>
                  <li><a href="<?php echo base_url() . 'admin/division'; ?>">Division</a></li>
                  <li><a href="<?php echo base_url() . 'admin/a_grade'; ?>">A Level Grade System</a></li>
                  <li><a href="<?php echo base_url() . 'admin/o_grade'; ?>">O Level Grade System</a></li>
                </ul>

                <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Time Table
                  </a>
                </li>

                <li  data-toggle="collapse" data-target="#academic_year" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Academic Year  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="academic_year">
                   
                    <li ><a href="<?php echo base_url() . 'admin/add_academic_year'; ?>">Create Academic Year</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/academic_years'; ?>">View Academic Year</a></li>
              
                </ul>
                <li  data-toggle="collapse" data-target="#Department" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Department  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Department">
                    <li ><a href="<?php echo base_url() . 'admin/add_department'; ?>">Add Department</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/departments'; ?>">View Departments</a></li>
                    <!-- <li ><a href="#">Shift Student</a></li>
                    <li ><a href="#">Assign Dormitory Leader</a></li>
                    <li><a href="#">Assign Teacher to Dormitory</a></li> -->

                    
                </ul>
                <li  data-toggle="collapse" data-target="#Class" class="collapsed ">
                  <a href="#"><i class="fa fa-graduation-cap fa-lg"></i> Class  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Class">
                   
                    <li ><a href="<?php echo base_url() . 'admin/add_class'; ?>">Add Class</a></li>
                    <li ><a href="<?php echo base_url() . 'all_classes'; ?>">View Classes</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/add_stream'; ?>">Add Stream</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/streams'; ?>">View Streams</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/add_class_stream'; ?>">Add Class Stream</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/class_streams'; ?>">View Class Streams</a></li>
                    <!-- <li><a href="#">Assign Teacher to Class</a></li> -->
                    <!-- <li><a href="#"></a>View Teacher Assigned</li> -->
                    <!-- <li><a href="#">Assign Student to Class</a></li> -->
                    <!-- <li><a href="#"> View Students in Class </a></li>
                    <li ><a href="#">Select Class Monitor</a></li>
                    <li ><a href="#">Take Class Attendance</a></li>
                    <li ><a href="#">View Class Attendance Report</a></li> -->                 
                    
                                    
                    
                    

                </ul>

                <li  data-toggle="collapse" data-target="#Subject" class="collapsed ">
                  <a href="#"><i class="fa fa-book fa-lg"></i> Subject  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Subject">
                   
                    <li ><a href="<?php echo base_url() . 'admin/add_new_subject'; ?>">Add Subject</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/subjects'; ?>">View Subjects</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/assign_subject_to_class'; ?>">Assign Subject to Class</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/class_stream_subjects'; ?>">View to Class Subjects</a></li>
                    <!-- <li ><a href="#">Assign Student to Subject </a></li>   -->             

                </ul>

                <li  data-toggle="collapse" data-target="#Domitory" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Dormitory  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Domitory">
                    <li ><a href="<?php echo base_url() . 'admin/add_dormitory'; ?>"">Add Dormitory</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/dormitories'; ?>">View Dormitory</a></li>
                    <!-- <li ><a href="#">Shift Student</a></li>
                    <li ><a href="#">Assign Dormitory Leader</a></li>
                    <li><a href="#">Assign Teacher to Dormitory</a></li> -->

                    
                </ul>


                <li  data-toggle="collapse" data-target="#Student" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Student <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Student">

                    <li ><a href="<?php echo base_url() . 'admin/register_student'; ?>">Add New Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/students'; ?>">View Registered Students</a></li>
                    <li><a href="<?php echo base_url() . 'admin/promote'; ?>">Promote Students</a></li>
                    <li><a href="<?php echo base_url() . 'students/alumni'; ?>">View Completed Students </a></li>

                    <li >
                    <a href="<?php echo base_url() . 'admin/transferred/IN'; ?>">Transferred IN Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/transferred/OUT'; ?>">Transferred OUT Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/expelled_students'; ?>">Suspended Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/selected_students'; ?>">Selected Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/unreported_students'; ?>">Unreported Students</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/enrolled_students'; ?>">Enrolled Students</a></li>                   
                    
                </ul>
                 <li  data-toggle="collapse" data-target="#results" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Results <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="results">
                    <?php foreach($years as $m): ?>
                      <li><a href="<?php echo base_url() . 'admin/student_results/'.$m['year']; ?>"> Results <?php echo $m['year']; ?></a></li>
                    <?php endforeach; ?>                         
                </ul>

                <li  data-toggle="collapse" data-target="#past_academic_years" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Completed Students <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="past_academic_years">
                    <li><a href="<?php echo base_url(). 'students/alumni'; ?>">All Students </a></li>
                    <?php foreach($past_years as $m): ?>
                      <li><a href="<?php echo base_url().'students/alumnus/'.$m['year']; ?>"> 
                      <?php echo $m['year']; ?></a></li>
                    <?php endforeach; ?>
                </ul>

                <li  data-toggle="collapse" data-target="#Staff" class="collapsed ">
                  <a href="#"><i class="fa fa-user fa-lg"></i> Staff  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Staff">
                    <li ><a href="<?php echo base_url() . 'admin/new_staff'; ?>" >Add Staff</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/staffs'; ?>">View Staff</a></li>
                    <li ><a href="<?php echo base_url() . 'admin/password_reset'; ?>">Manage Staff Account</a></li>
                    <!-- <li ><a href="#">Activate/Deactivate Staff</a></li> -->
                    <!-- <li ><a href="#">Assign Staff</a></li> -->

                </ul>

                <li  data-toggle="collapse" data-target="#Library" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Library  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Library">
                    <li ><a href="#">Add Book</a></li>
                    <li ><a href="#">View Book</a></li>
                    <li><a href="#">View Book Copies</a></li>
                    <li ><a href="#">Borrow a Book</a></li>
                    <li><a href="#">Return a Book</a></li>
                     <li><a href="#">Borrowed Book</a></li>
                    <li><a href="#">Lost Books</a></li>
                    
                </ul>

                <li  data-toggle="collapse" data-target="#Finance" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Finance  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Finance">

                    <li ><a href="#">Create Transaction</a></li>
                    <li ><a href="#">Transactions</a></li>
                    <li ><a href="#">Receive Payment</a></li>
                    <li ><a href="#">Transaction Analysis</a></li>
                    <li ><a href="#">Full Paid</a></li>
                    <li ><a href="#">Uncomplete Paid</a></li>
                    
                    
                </ul>
            
            </ul>
     </div>
</div>
</div>