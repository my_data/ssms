<!DOCTYPE html>
<html lang="en">
<head><link rel='icon' href="<?php echo base_url() . 'assets/images/129.jpg'; ?>" width='50%' />
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>BBTSMS</title>
      <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" >
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/font-awesome.min.css">

      <!--BOOTSTRAP SWITCH-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-switch.min.css">
    <!--END BOOTSTRAP SWITCH-->

      <!-- CHOSEN CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.min.css">
      <!-- END CHOSEN CSS -->

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      
 <style>
  .nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  display: inline-block;
  top: 0px;
  width: 260px;
  height: 100%;
  color: #e1ffff;
  padding-top: 50px;
  padding-bottom: 50px

}

@media print {
.table-bordered th,
.table-bordered td {
  border: 1px solid #000 !important;
  font-size: 13px;
  padding-left: 3px;
 
 
}
body{
  padding-top: 0px;
  padding-bottom: 0px;
  padding-left: 0px;
  padding-right: 0px;
}
}
#nav_bar{
  background-color: #2e353d;
}
#bottom_nav{
  background-color: #fff;
  color: #7f7f7f;
  border-top: 1px solid #fff;
}
/* notification color*/
#bell{
color: #f0ad4e;
}
#notify{
  color: #ff0000;
  background-color: #CBD4DA;
}
#logout{
  color: #9C9C9C;
}
/*MY CUSTOM*/
.table {}
.table-header { font-weight: 600; background: #f0f0f0; }

.white-area-content { background: #FFFFFF; border-radius: 4px; padding: 15px; border: 1px solid #d4dce2;}
.white-link { color: #FFFFFF; }
.white-link:hover { color: #DCE8E8; text-decoration: none;}


.db-header { border-bottom: 1px solid #d3d3d3; margin-bottom: 20px; padding-bottom: 1px;}
.db-header-title { font-size: 18px; float: left;}
.page-header-title { font-size: 15px; float: left;}
.db-header-extra { float: right; padding: 5px;  }
/*.table-bordered th,
.table-bordered td {
  font-size: 15px;
  font-family:"Times New Roman", Times, serif;

}*/

/*END MY CUSTOM*/

.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: none;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
  padding-left: 20px;
}


/*  custom collapse nav-bar start here*/
@media (max-width: 992px) {
    .navbar-header {
        float: none;
    }
    .navbar-toggle {
        display: block;
        
    }

    .navbar-collapse {
        border-top: 2px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
        margin: 7.5px -15px;

    }
    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .navbar-text {
        float: none;
        margin: 15px 0;
    }
    /* since 3.1.0 */
    .navbar-collapse.collapse.in { 
        display: block!important;
    }
    .collapsing {
        overflow: hidden!important;
    }
}

/*custom collapse nav-bar ends here*/
/*responsiveness for all small devices below 992 px  start here*/
@media only screen and (max-device-width : 992px){
.nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding-top: 0px;


 

  }
#img1,#img4-staff,#prof2{
  display: none;
}

  .table-bordered th,
.table-bordered td {

  font-size:1em; 


}

  #menu-content{
  overflow:auto;
  font-family: verdana;
  font-size: 12px;
  background-color: #2e353d;
  position: fixed;
  width:94%;
  height: 100%;
  z-index: 30 !important;
  color: #e1ffff;
  padding-top: 50px;
  padding-bottom: 50px;
  
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: fixed;
    right: 0px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
    padding-top: auto;
    
  }
  .brand {
    text-align: left !important;
    font-size: 18px;
    padding-left: 20px;
    line-height: 50px !important;

  }
   #contents{
  margin: 0px;
  font-size: 12px;
  padding-bottom: 50px;

}
body{
  padding-left: 10px;
  padding-right: 10px;
  padding-bottom: 60px;

}
#user{
  font-size: 14px;
  font-family:"Times New Roman", Times, serif;
  color: #fff;
}
#bottom_nav{
  background-color: #fff;
  color: #7f7f7f;
  border-top: 1px solid #9C9C9C;
}

}

/*responsiveness for all small devices below 992 px  end here*/
/*responsiveness for all  devices above 992 px  start here*/
@media only screen and (min-device-width : 992px){
.nav-side-menu .menu-list .menu-content {
    display: block;
     position: relative; 
     padding-bottom: 100px;
     
  }
 #img2,#img3-staff,#prof1,#notifysmall{
  display: none;

 }
body{
  margin: 0px;
  padding-left: 0px;
  padding-right: 20px;
  padding-top: 50px;
  padding-bottom: 70px;
}
   #contents{
  margin: 0px;
  font-size: 13px;
  padding-top: 20px;
  padding-bottom: 50px;

}
.nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  display: inline-block;
  top: 0px;
  width: 280px;
  height: 100%;
  color: #e1ffff;
  padding-top: 50px;
  padding-bottom: 50px

}

}
#user,#user>a{
  font-size: 16px;
  font-family:"Times New Roman", Times, serif;
    color: #fff;
}

.success_message_color{
  /*background-color: #00FF00;*/
  color: #FFF;
  width: 50%;
  font-size: 14px;
  text-align: center;
  background-color:#2E353D;
}

.error_message_color{
  color: #F00;
  font-size: 14px;
/*  background-color:#FF0000;*/
}

.warning_message_color{
  color: yellow;
  font-size: 14px;
  font-family:"Courier New", Courier, monospace;
}



.box{
    /*color: #fff;
    padding: 20px;*/
    display: none;
/*    margin-top: 20px;*/
}
.badge{
  color: #fff;
  background-color:#2769B1; 
}

/*.disabled{ background: #ff0000; }*/


/* CUSTOM TEXTAREA*/

/*END CUSTOM TEXTAREA*/


/*General Hope Page*/
.panel {

  /*height: 120px;*/
  box-shadow: 0 1px 1px rgba(0,0,0,0.10), 0 3px 6px rgba(0,0,0,0.10);
}
.panel-body{
  color: #FFFFFF;  
  font-size: 18px;
 /* padding-top: 35px;*/
}

/* for calender*/
#calendar{
  width: 100%;
  /*padding-bottom: 500px;*/
/*border: 15px solid #25BAE4;
border-collapse:collapse;
margin-top: 50px;
margin-left: 250px;
*/}
#calendar td{
/*width: 50px;
height: 50px;*/
text-align: center;
border: 2px solid #e2e0e0;
font-size: 12px;
font-weight: bold;
}
#calendar th{
/*height: 50px;*/
/*padding-bottom: 8px;*/
text-align: center;
background:#62ACEC; 

font-size: 14px;
}
.prev_sign a, .next_sign a{
color:white;
text-decoration: none;
}
#calendar tr.week_name{
font-size: 19px;
font-weight:100;
color:#62ACEC;

width: 10px;
background-color:#F0F0F0;
}
#calendar .highlight{
background-color:#62ACEC; 
background-color:#62ACEC;
color:#FFFFFF;
/*height: 27px;*/
/*padding-top: 13px;
padding-bottom: 10px;*/
}
/* for calendar*/



/*General Hope Page*/


/*All table style*/
.table{
   box-shadow: 0 1px 1px rgba(0,0,0,0.10), 0 3px 6px rgba(0,0,0,0.10); 
}

/*.rotate {
    -webkit-transform: rotate(90deg);
    -webkit-transform-origin: left top;
    -moz-transform: rotate(90deg);
    -moz-transform-origin: left top;
    -ms-transform: rotate(90deg);
    -ms-transform-origin: left top;
    -o-transform: rotate(90deg);
    -o-transform-origin: left top;
    transform: rotate(90deg);
    transform-origin: left top;

    position: absolute;
    top: 0;
    left: 100%;
    white-space: nowrap;    
    font-size: 9px;
}*/

th.rotate {
  /* Something you can count on */
  height: 140px;
  white-space: nowrap;
}

th.rotate > div {
  transform: 
    /* Magic Numbers */
    translate(15px, 20px)
    /* 45 is really 360 - 45 */
    rotate(270deg);
  width: 20px;
}
th.rotate > div > span {
  border-bottom: 0px solid #ccc;
  padding: 5px 10px;
	}


/*All table style*/

/*MULPHASE FORM*/

form#staffRegistrationForm{ /*border:#000 1px solid; */padding:24px; width:900px; }
form#staffRegistrationForm > #staffContactInfoPhase, #staffRolePhase, #staffNokInfoPhase, #previewPhase{ display: none; }

form#student_registration_form{ /*border:#000 1px solid; */padding:24px; width:900px; }
form#student_registration_form > #contactInfoPhase, #admissionInfoPhase, #first_guardian_phase, #second_guardian_phase, #review_all_data{ display: none; }

#disability_values{
  display: none;
}

#edit_disability_values{
  display: block;
}

#subject_values{
  display: none;
}

#edit_subject_values{
  display: block;
}



.break{
  page-break-after: always;
}
      </style>

  </head>
<body onLoad="hide_element('select_element')" >
<?php
//ALL LINKS
$add_new_student = FALSE; $registered_students = FALSE; $promote_students = FALSE; $manage_selected_students = FALSE; $students = FALSE;
$transferred_in_students = FALSE; $transferred_out_students = FALSE; $suspended_students = FALSE; $unreported_students = FALSE; $selected_students = FALSE;
$enrolled_students = FALSE; $disabled_students = FALSE; $manage_staff = FALSE; $view_staffs = FALSE;
$manage_dormitory = FALSE; $view_dormitories = FALSE; $view_classes = FALSE; $manage_timetable = FALSE; $school_timetable =FALSE; $indiscipline_record = FALSE; $view_personal_timetable = FALSE;
$view_teachers = FALSE; $view_school_attendance = FALSE; $manage_staff_attendance = FALSE; $staff_attendance = FALSE;
$daily_student_attendance = FALSE; $add_score = FALSE; $view_results = FALSE;
$manage_department = FALSE; $view_departments = FALSE; $new_announcement = FALSE; $manage_posted_announcements= FALSE;
$announcements_link = FALSE; $manage_alumni = FALSE; $manage_tods = FALSE; $tods = FALSE;
$manage_indiscipline_records = FALSE; $change_password = FALSE; $view_own_profile = FALSE; $manage_periods = FALSE; $view_periods = FALSE;
$add_own_qualification = FALSE; $department_staffs = FALSE; $assign_teacher_a_subject = FALSE; $view_subjects_assigned = FALSE;
$department_classes = FALSE; $students_in_dormitory = FALSE; $add_roll_call = FALSE;
$roll_call_in_my_dormitory = FALSE; $class_attendance_report = FALSE; $add_class_attendance = FALSE; $dormitory_assets = FALSE;
$students_in_my_class = FALSE; $teachers_assigned = FALSE; $add_class_journal_record = FALSE; $update_class_journal = FALSE; $school_results = FALSE;
$view_class_journal_report = FALSE; $results_first_term = FALSE; $results_last_term = FALSE; $view_own_roles = FALSE; $myclasses = FALSE; $backup_db = FALSE;
$manage_role = FALSE; $manage_groups = FALSE; $view_activity_log = FALSE; $view_exam_types = FALSE;
$view_division = FALSE; $view_a_grade = FALSE; $view_o_grade = FALSE; $view_academic_year = FALSE; $add_academic_year = FALSE; $view_class_stream_subjects = FALSE; $view_subjects = FALSE; $manage_class_subjects = FALSE; $manage_subjects = FALSE; $manage_staff_accounts = FALSE;
$manage_tod_reports = FALSE; $view_audit_trail = FALSE; $view_own_class_results = FALSE; $manage_alumni = FALSE; $passwords_reset = FALSE; $view_tod_report = FALSE; $logs = FALSE;

?>


<?php 
  //ALL MODULES
  $admission = FALSE; $staff_management = FALSE; $accomodation = FALSE; $administration = FALSE;
  $exams = FALSE; $department_management = FALSE; $announcements_module = FALSE; $manage_alumni = FALSE;
  $tod_management = FALSE; $discipline_management = FALSE; $myaccount = FALSE; $mydepartment = FALSE;
  $mydormitory = FALSE; $myclass = FALSE; $tod = FALSE; $results = FALSE; $class_results = FALSE; $myassigned_classes = FALSE; $admin_settings = FALSE;
  $payment = FALSE; $library_management = FALSE; $manage_finance = FALSE; $reports = FALSE; $inventory = FALSE;
  $class_management = FALSE; $timetable = FALSE; $student_management = FALSE; $role = FALSE;
  $add_new_class = FALSE; $manage_class = FALSE; $view_class_streams = FALSE; $view_streams = FALSE;
  $add_new_stream = FALSE; $academic_year_module = FALSE; $subjects_module = FALSE;
?>

<!-- CODE FOR ALL MODULES THAS A PARTICULAR USER IS ALLOWED TO SEE -->
<?php foreach($this->session->userdata('modules') as $mal): ?>
  <?php
    if($mal['module_name'] === "Admin Settings"){
      $admin_settings = TRUE;
    }
    if($mal['module_name'] === "Academic Year"){
      $academic_year_module = TRUE;
    }
    if($mal['module_name'] === "Student Management"){
      $student_management = TRUE;
    }
    if($mal['module_name'] === "Timetable"){
      $timetable = TRUE;
    }
    if($mal['module_name'] === "Reports"){
      $reports = TRUE;
    }
    if($mal['module_name'] === "Inventory"){
      $inventory = TRUE;
    }
    if($mal['module_name'] === "Subject Management"){
      $subjects_module = TRUE;
    }
    if($mal['module_name'] === "Class Management"){
      $class_management = TRUE;
    }
    if($mal['module_name'] === "Role"){
      $role = TRUE;
    }
    if($mal['module_name'] === "Library Management"){
      $library_management = TRUE;
    }
    if($mal['module_name'] === "My Assigned Classes"){
      $myassigned_classes = TRUE;
    }
    if($mal['module_name'] === "Payment"){
      $payment = TRUE;
    }
    if($mal['module_name'] === "Admission"){
      $admission = TRUE;
    }
    if($mal['module_name'] === "Staff Management"){
      $staff_management = TRUE;
    }
    if($mal['module_name'] === "Accomodation"){
      $accomodation = TRUE;
    }
    if($mal['module_name'] === "Administration"){
      $administration = TRUE;
    }
    if($mal['module_name'] === "Exams"){
      $exams = TRUE;
    }
    if($mal['module_name'] === "Department Management"){
      $department_management = TRUE;
    }
    if($mal['module_name'] === "TOD Management"){
      $tod_management = TRUE;
    }
    if($mal['module_name'] === "Discipline Management"){
      $discipline_management = TRUE;
    }
    if($mal['module_name'] === "My Account"){
      $myaccount = TRUE;
    }
    if($mal['module_name'] === "My Department"){
      $mydepartment = TRUE;
    }
    if($mal['module_name'] === "My Dormitory"){
      $mydormitory = TRUE;
    }
    if($mal['module_name'] === "My Class"){
      $myclass = TRUE;
    }
    /*if($mal['module_name'] === "TOD"){
      $tod = TRUE;
    }*/
    if($mal['module_name'] === "Results"){
      $results = TRUE;
    }
    if($mal['module_name'] === "Announcements"){
      $announcements_module = TRUE;
    }
    if($mal['module_name'] === "Manage Finance"){
      $manage_finance = TRUE;
    }
  ?>
<?php endforeach; ?>
<!-- END CODE FOR ALL MODULES THAS A PARTICULAR USER IS ALLOWED TO SEE -->
<!-- CODE FOR ALL LINKS THAS A PARTICULAR USER IS ALLOWED TO SEE -->
<?php if($this->session->userdata('roles_permission')): //Check for roles if any?>

<?php foreach($this->session->userdata('roles_permission') as $mal): ?>
  <?php 
    if($mal['description'] === "Register Student"){
      $this->session->set_userdata('register_student', 'ok');
      $add_new_student = TRUE;
    }
    if($mal['description'] === "View Registered Students"){
      $this->session->set_userdata('view_registered_students', 'ok');
      $registered_students = TRUE;
    }
    if($mal['description'] === "Promote Students"){
      $this->session->set_userdata('promote_students', 'ok');
      $promote_students = TRUE;
    }
    if($mal['description'] === "Manage Selected Students"){
      $this->session->set_userdata('manage_selected_students', 'ok');
      $manage_selected_students = TRUE;
    }
    if($mal['description'] === "View Selected Students"){
      $this->session->set_userdata('view_selected_students', 'ok');
      $selected_students = TRUE;
    }
    if($mal['description'] === "Students"){
      $students = TRUE;
    }
    if($mal['description'] === "View Transferred IN Students"){
      $this->session->set_userdata('view_transferred_in_students', 'ok');
      $transferred_in_students = TRUE;
    }
    if($mal['description'] === "View Transferred OUT Students"){
      $this->session->set_userdata('view_transferred_out_students', 'ok');
      $transferred_out_students = TRUE;
    }
    if($mal['description'] === "Restore Transferred Students"){
      $this->session->set_userdata('restore_transferred_out_students', 'ok');
    }
    if($mal['description'] === "View Suspended Students"){
      $this->session->set_userdata('view_suspended_students', 'ok');
      $suspended_students = TRUE;
    }
    if($mal['description'] === "View Unreported Students"){
      $this->session->set_userdata('view_unreported_students', 'ok');
      $unreported_students = TRUE;
    }
    if($mal['description'] === "View Enrolled Students"){
      $this->session->set_userdata('view_enrolled_students', 'ok');
      $enrolled_students = TRUE;
    }
    if($mal['description'] === "View Disabled Students"){
      $this->session->set_userdata('view_disabled_students', 'ok');
      $disabled_students = TRUE;
    }
    if($mal['description'] === "Manage Disabled Students"){
      $this->session->set_userdata('manage_disabled_students', 'ok');
    }
    if($mal['description'] === "Manage Staff"){
      $this->session->set_userdata('manage_staff', 'ok');
      $manage_staff = TRUE;
    }
    if($mal['description'] === "View Staffs"){
      $this->session->set_userdata('view_staffs', 'ok');
      $view_staffs = TRUE;
    }
    if($mal['description'] === "Manage Staff Accounts"){
      $this->session->set_userdata('manage_staff_accounts', 'ok');
      $manage_staff_accounts = TRUE;
    }
    if($mal['description'] === "Manage Dormitory"){
      $this->session->set_userdata('manage_dormitory', 'ok');
      $manage_dormitory = TRUE;
    }
    if($mal['description'] === "View Dormitories"){
      $this->session->set_userdata('view_dormitories', 'ok');
      $view_dormitories = TRUE;
    }
    if($mal['description'] === "View Staff Attendance"){
      $this->session->set_userdata('view_staff_attendance', 'ok');
      $staff_attendance = TRUE;
    }
    if($mal['description'] === "View Classes"){
      $this->session->set_userdata('view_classes', 'ok');
      $view_classes = TRUE;
    }
    if($mal['description'] === "Manage Timetable"){
      $this->session->set_userdata('manage_timetable', 'ok');
      $manage_timetable = TRUE;
    }
    if($mal['description'] === "View School Timetable"){
      $this->session->set_userdata('view_class_timetable', 'ok');
      $school_timetable = TRUE;
    }
    if($mal['description'] === "View All Teachers"){
      $this->session->set_userdata('view_all_teachers', 'ok');
      $view_teachers = TRUE;
    }
    if($mal['description'] === "View School Attendance"){
      $this->session->set_userdata('view_school_attendance', 'ok');
      $view_school_attendance = TRUE;
    }
    if($mal['description'] === "Manage Staffs Attendance"){
      $this->session->set_userdata('manage_staff_attendance', 'ok');
      $manage_staff_attendance = TRUE;
    }
    if($mal['description'] === "View Class Attendance"){
      $this->session->set_userdata('view_class_attendance', 'ok');
      $daily_student_attendance = TRUE;
    }
    if($mal['description'] === "Add Score Of Own Subject"){
      $this->session->set_userdata('add_score_of_own_subject', 'ok');
      $add_score = TRUE;
    }
    if($mal['description'] === "Manage Score & Results Of Assigned Subjects"){
      $this->session->set_userdata('manage_score_of_assigned_subjects', 'ok');
      $view_results = TRUE;
    }
    if($mal['description'] === "Manage Department"){
      $this->session->set_userdata('manage_department', 'ok');
      $manage_department = TRUE;
    }
    if($mal['description'] === "View Departments"){
      $this->session->set_userdata('view_departments', 'ok');
      $view_departments = TRUE;
    }
    if($mal['description'] === "View Announcements"){
      $this->session->set_userdata('view_announcements', 'ok');
      $announcements_link = TRUE;
    }
    if($mal['description'] === "Manage Alumni"){
      $this->session->set_userdata('manage_alumni', 'ok');
      $manage_alumni = TRUE;
    }
    if($mal['description'] === "Add Own Qualification"){
      $this->session->set_userdata('add_own_qualification', 'ok');
      $add_own_qualification = TRUE;
    }
    if($mal['description'] === "View Own Profile"){
      $this->session->set_userdata('view_own_profile', 'ok');
      $view_own_profile = TRUE;
    }
    if($mal['description'] === "Assign Teacher To Subject"){
      $this->session->set_userdata('assign_teacher_to_subject', 'ok');
      $assign_teacher_a_subject = TRUE;
    }
    if($mal['description'] === "Change Own Password"){
      $this->session->set_userdata('change_password', 'ok');
      $change_password = TRUE;
    }
    if($mal['description'] === "Manage TOD Reports"){
      $this->session->set_userdata('manage_tod_reports', 'ok');
      $manage_tod_reports = TRUE;
    }
    if($mal['description'] === "View TODs"){
      $this->session->set_userdata('view_tods', 'ok');
      $tods = TRUE;
    }
    if($mal['description'] === "Manage Indiscipline Records"){
      $this->session->set_userdata('manage_indiscipline_records', 'ok');
      $manage_indiscipline_records = TRUE;
    }
    if($mal['description'] === "View Indiscipline Records"){
      $this->session->set_userdata('view_indiscipline_records', 'ok');
      $indiscipline_record = TRUE;
    }
    if($mal['description'] === "View Assinged Teachers In Own Department"){
      $this->session->set_userdata('view_assigned_teachers_in_own_department', 'ok');
      $view_subjects_assigned = TRUE;
    }
    if($mal['description'] === "View Own Department Staffs"){
      $this->session->set_userdata('view_own_departments_staffs', 'ok');
      $department_staffs = TRUE;
    }
    if($mal['description'] === "View Own Department Classes"){
      $this->session->set_userdata('view_department_classes', 'ok');
      $department_classes = TRUE;
    }
    if($mal['description'] === "View Students In Own Dormitory"){
      $this->session->set_userdata('view_students_in_own_dormitory', 'ok');
      $students_in_dormitory = TRUE;
    }
    if($mal['description'] === "View Assets In Own Dormitory"){
      $this->session->set_userdata('view_assets_in_own_dormitory', 'ok');
      $dormitory_assets = TRUE;
    }
    if($mal['description'] === "Add Roll Call"){
      $this->session->set_userdata('add_roll_call', 'ok');
      $add_roll_call = TRUE;
    }
    if($mal['description'] === "View Roll Call In Own Dormitory"){
      $this->session->set_userdata('view_roll_call_in_own_dormitory', 'ok');
      $roll_call_in_my_dormitory = TRUE;
    }
    if($mal['description'] === "View Own Class Attedance"){
      $this->session->set_userdata('view_own_class_attendance', 'ok');
      $class_attendance_report = TRUE;
    }
    if($mal['description'] === "Add Class Attedance"){
      $this->session->set_userdata('add_class_attendance', 'ok');
      $add_class_attendance = TRUE;
    }
    if($mal['description'] === "View Students In Own Class"){
      $this->session->set_userdata('view_students_in_own_class', 'ok');
      $students_in_my_class = TRUE;
    }
    if($mal['description'] === "View Assigned Teachers In Own Class"){
      $this->session->set_userdata('view_assigned_teachers_in_own_class', 'ok');
      $teachers_assigned = TRUE;
    }
    if($mal['description'] === "Add Class Journal Record"){
      $this->session->set_userdata('add_class_journal_record', 'ok');
      $add_class_journal_record = TRUE;
    }
    if($mal['description'] === "Update Class Journal"){
      $this->session->set_userdata('update_class_journal', 'ok');
      $update_class_journal = TRUE;
    }
    if($mal['description'] === "View Class Journal Report"){
      $this->session->set_userdata('view_class_journal_report', 'ok');
      $view_class_journal_report = TRUE;
    }
    if($mal['description'] === "View Own Class Journal Report"){
      $this->session->set_userdata('view_own_class_journal_report', 'ok');
      $view_own_class_journal_report = TRUE;
    }
    if($mal['description'] === "View Own Class Results"){
      $this->session->set_userdata('view_own_class_results', 'ok');
      $view_own_class_results = TRUE;
    }
    if($mal['description'] === "View School Results"){
      $this->session->set_userdata('view_school_results', 'ok');
      $school_results = TRUE;
    }
    if($mal['description'] === "Manage Roles"){
      $this->session->set_userdata('manage_roles', 'ok');
      $manage_role = TRUE;
    }
    if($mal['description'] === "Manage Groups"){
      $this->session->set_userdata('manage_groups', 'ok');
      $manage_groups = TRUE;
    }
    if($mal['description'] === "View Activity Log"){
      $this->session->set_userdata('view_activity_log', 'ok');
      $view_activity_log = TRUE;
    }
    if($mal['description'] === "View Audit Trail"){
      $this->session->set_userdata('view_audit_trail', 'ok');
      $view_audit_trail = TRUE;
    }
    if($mal['description'] === "Perform Database Backup"){
      $this->session->set_userdata('db_backup', 'ok');
      $backup_db = TRUE;
    }
    if($mal['description'] === "View Exam Types"){
      $this->session->set_userdata('view_exam_types', 'ok');
      $view_exam_types = TRUE;
    }
    if($mal['description'] === "View Divisions"){
      $this->session->set_userdata('view_divisions', 'ok');
      $view_division = TRUE;
    }
    if($mal['description'] === "View A'Level Grade System"){
      $this->session->set_userdata('view_a_grade', 'ok');
      $view_a_grade = TRUE;
    }
    if($mal['description'] === "View O'Level Grade System"){
      $this->session->set_userdata('view_o_grade', 'ok');
      $view_o_grade = TRUE;
    }
    if($mal['description'] === "View Academic Years"){
      $this->session->set_userdata('view_academic_years', 'ok');
      $view_academic_year = TRUE;
    }
    if($mal['description'] === "Add Academic Year"){
      $this->session->set_userdata('add_academic_year', 'ok');
      $add_academic_year = TRUE;
    }
    if($mal['description'] === "Add New Class"){
      $this->session->set_userdata('add_new_class', 'ok');
      $add_new_class = TRUE;
    }
    if($mal['description'] === "Manage Class"){
      $this->session->set_userdata('manage_class', 'ok');
      $manage_class = TRUE;
    }
    if($mal['description'] === "View Class Streams"){
      $this->session->set_userdata('view_class_streams', 'ok');
      $view_class_streams = TRUE;
    }
    if($mal['description'] === "View Streams"){
      $this->session->set_userdata('view_streams', 'ok');
      $view_streams = TRUE;
    }
    if($mal['description'] === "Manage Subjects"){
      $this->session->set_userdata('manage_subjects', 'ok');
      $manage_subjects = TRUE;
    }
    if($mal['description'] === "View Subjects"){
      $this->session->set_userdata('view_subjects', 'ok');
      $view_subjects = TRUE;
    }
    if($mal['description'] === "View Class Stream Subjects"){
      $this->session->set_userdata('view_class_stream_subjects', 'ok');
      $view_class_stream_subjects = TRUE;
    }
    if($mal['description'] === "Manage Class Subjects"){
      $this->session->set_userdata('manage_class_subjects', 'ok');
      $manage_class_subjects = TRUE;
    }
    if($mal['description'] === "View Personal Timetable"){
      $this->session->set_userdata('view_personal_timetable', 'ok');
      $view_personal_timetable = TRUE;
    }
    if($mal['description'] === "View Own Roles"){
      $view_own_roles = TRUE;
    }
    if($mal['description'] === "View Assigned Classes"){
      $this->session->set_userdata('view_assigned_classes', 'ok');
      $myclasses = TRUE;
    }
    if($mal['description'] === "Manage Periods"){
      $this->session->set_userdata('manage_periods', 'ok');
      $manage_periods = TRUE;
    }
    if($mal['description'] === "View Periods"){
      $this->session->set_userdata('view_periods', 'ok');
      $view_periods = TRUE;
    }
    if($mal['description'] === "Activate-Deactivate Staff Account"){
      $this->session->set_userdata('activate_deactivate_staff_account', 'ok');
    }
    /*if($mal['description'] === "Add New Role"){
      $this->session->set_userdata('add_new_role', 'ok');
    }
    if($mal['description'] === "Rename Role"){
      $this->session->set_userdata('rename_role', 'ok');
    }
    if($mal['description'] === "Remove Role"){
      $this->session->set_userdata('remove_role', 'ok');
    }*/
    if($mal['description'] === "View Subject Teaching Logs"){
      $this->session->set_userdata('view_teaching_logs', 'ok');
    }
    if($mal['description'] === "Manage Teaching Assignments"){
      $this->session->set_userdata('manage_teaching_assignments', 'ok');
    }
    if($mal['description'] === "Add HOD"){
      $this->session->set_userdata('add_hod', 'ok');
    }
    if($mal['description'] === "Update Academic Year"){
      $this->session->set_userdata('update_academic_year', 'ok');
    }
    if($mal['description'] === "Add New Term"){
      $this->session->set_userdata('add_new_term', 'ok');
    }
    if($mal['description'] === "Update Term"){
      $this->session->set_userdata('update_term', 'ok');
    }
    if($mal['description'] === "Set Current Term"){
      $this->session->set_userdata('set_current_term', 'ok');
    }
    if($mal['description'] === "Manage TODs"){
      $this->session->set_userdata('manage_tods', 'ok');
      $manage_tods = TRUE;
    }
    if($mal['description'] === "Manage O'Level Grade System"){
      $this->session->set_userdata('manage_o_level_grading_system', 'ok');
    }
    if($mal['description'] === "Manage A'Level Grade System"){
      $this->session->set_userdata('manage_a_level_grading_system', 'ok');
    }
    if($mal['description'] === "Manage Dormitory Assets"){
      $this->session->set_userdata('manage_dormitory_assets', 'ok');
    }
    if($mal['description'] === "View TOD Report"){
      $this->session->set_userdata('view_tod_report', 'ok');
      $view_tod_report = TRUE;
    }
    if($mal['description'] === "Add Comment TOD Report"){
      $this->session->set_userdata('add_comment_tod_report', 'ok');
    }
    if($mal['description'] === "Update Exam Type"){
      $this->session->set_userdata('update_exam_type', 'ok');
    }
    if($mal['description'] === "Enable/Disable Exam Types"){
      $this->session->set_userdata('enable_or_disable_exam_type', 'ok');
    }
    if($mal['description'] === "Remove Teaching Assignment In Own Department"){
      $this->session->set_userdata('remove_teaching_assignment_in_own_department', 'ok');
    }
    if($mal['description'] === "Explore School Attendance"){
      $this->session->set_userdata('explore_school_attendance', 'ok');
    }
    if($mal['description'] === "Update Own Profile"){
      $this->session->set_userdata('update_own_profile', 'ok');
    }
    if($mal['description'] === "Change Avatar"){
      $this->session->set_userdata('change_avatar', 'ok');
    }
    if($mal['description'] === "Update Score Of Own Subject"){
      $this->session->set_userdata('update_score_of_own_subject', 'ok');
    }
    if($mal['description'] === "Suspend Student"){
      $this->session->set_userdata('suspend_student', 'ok');
    }
    if($mal['description'] === "View Students In Dormitory"){
      $this->session->set_userdata('students_in_dormitory', 'ok');
    }
    if($mal['description'] === "Transfer Student"){
      $this->session->set_userdata('transfer_student', 'ok');
    }
    /*if($mal['description'] === "Add New Group"){
      $this->session->set_userdata('add_new_group', 'ok');
    }
    if($mal['description'] === "View Group Members"){
      $this->session->set_userdata('view_group_members', 'ok');
    }
    if($mal['description'] === "Edit Group"){
      $this->session->set_userdata('edit_group', 'ok');
    }
    if($mal['description'] === "Remove Group"){
      $this->session->set_userdata('remove_group', 'ok');
    }*/
    if($mal['description'] === "View Staffs Details"){
      $this->session->set_userdata('view_staff_details', 'ok');
    }
    if($mal['description'] === "View Teacher Details"){
      $this->session->set_userdata('view_teacher_details', 'ok');
    }
    if($mal['description'] === "Update Teaching Assignment In Own Department"){
      $this->session->set_userdata('update_teaching_assignment_in_own_department', 'ok');
    }
    /*if($mal['description'] === "Add Staff To Group"){
      $this->session->set_userdata('add_staff_to_group', 'ok');
    }*/
    if($mal['description'] === "View Teaching Log In Own Department"){
      $this->session->set_userdata('view_teaching_log_in_own_department', 'ok');
	$logs = TRUE;
    }
    if($mal['description'] === "View Staff Activity Log"){
      $this->session->set_userdata('view_staff_activity_log', 'ok');
    }
    if($mal['description'] === "View Staff Role"){
      $this->session->set_userdata('view_staff_role', 'ok');
    }
    if($mal['description'] === "Edit Student Results"){
       $this->session->set_userdata('edit_student_results', 'ok');
    }
    if($mal['description'] === "Add Completion Details"){
       $this->session->set_userdata('add_completion_details', 'ok');
    }
    if($mal['description'] === "View Own Class Timetable"){
      $this->session->set_userdata('view_own_class_timetable', 'ok');
    }
    if($mal['description'] === "Add Class Monitor"){
      $this->session->set_userdata('add_class_monitor', 'ok');
    }
    if($mal['description'] === "Add Dormitory Leader"){
      $this->session->set_userdata('add_dormitory_leader', 'ok');
    }
    if($mal['description'] === "Update Student"){
      $this->session->set_userdata('update_student', 'ok');
    }
    if($mal['description'] === "Manage Own Posted Announcements"){
      $manage_posted_announcements = TRUE;
      $this->session->set_userdata('manage_posted_announcements', 'ok');
    }
    /*if($mal['description'] === "Publish Announcement"){
      $this->session->set_userdata('publish_announcement', 'ok');
    }*/
    if($mal['description'] === "Add Dormitory Master"){
      $this->session->set_userdata('add_dormitory_master', 'ok');
    }
    if($mal['description'] === "View Roll Call"){
      $this->session->set_userdata('view_roll_call', 'ok');
    }
    if($mal['description'] === "View Assets In Dormitory"){
      $this->session->set_userdata('view_assets_in_dormitory', 'ok');
    }
    if($mal['description'] === "View Assets In Class"){
      $this->session->set_userdata('view_assets_in_class', 'ok');
    }
    if($mal['description'] === "Manage Class Assets"){
      $this->session->set_userdata('manage_class_assets', 'ok');
    }
    if($mal['description'] === "Reset Password"){
      $this->session->set_userdata('reset_password', 'ok');
      $passwords_reset = TRUE;
    }
    if($mal['description'] === "Add Student To Dormitory"){
      $this->session->set_userdata('add_student_to_dormitory', 'ok');
    }
    if($mal['description'] === "Transfer Student To Another Dormitory"){
      $this->session->set_userdata('transfer_student_to_another_dormitory', 'ok');
    }
    /*if($mal['description'] === "Add User To Role"){
      $this->session->set_userdata('add_user_to_role', 'ok');
    }
    if($mal['description'] === " Update Role Permissions"){
      $this->session->set_userdata('update_role_permission', 'ok');
    }*/
    if($mal['description'] === "Manage Division"){
      $this->session->set_userdata('manage_division', 'ok');
    }
    if($mal['description'] === "Restore Suspended Student"){
      $this->session->set_userdata('restore_suspended_student', 'ok');
    }
    if($mal['description'] === "Manage Staff Qualifications"){
      $this->session->set_userdata('manage_staff_qualification', 'ok');
    }
    if($mal['description'] === "View Students In Own Department"){
      $this->session->set_userdata('view_students_in_own_department', 'ok');
    }
    if($mal['description'] === "View Students In Class"){
      $this->session->set_userdata('view_students_in_class', 'ok');
    }
    if($mal['description'] === "View Student Results In Own Department"){
      //NOT Available kwa DB, will be added if deemed necessary
    }
    if($mal['description'] === "View Assigned Teachers In Class"){
      $this->session->set_userdata('view_assigned_teachers_in_class', 'ok');
    }
    if($mal['description'] === "View Own Log"){
      $this->session->set_userdata('view_own_log', 'ok');
    }
    if($mal['description'] === "Manage Teaching Log"){
      $this->session->set_userdata('manage_teaching_log', 'ok');
    }
    if($mal['description'] === "Assign Class Teacher"){
      $this->session->set_userdata('assign_class_teacher', 'ok');
    }
    if($mal['description'] === "View Notifications"){
      $this->session->set_userdata('view_notifications', 'ok');
    }
    if($mal['description'] === "View Topics"){
      $this->session->set_userdata('view_topics', 'ok');
    }
    if($mal['description'] === "Manage Topics"){
      $this->session->set_userdata('manage_topics', 'ok');
    }
    if($mal['description'] === "View Subtopics"){
      $this->session->set_userdata('view_subtopics', 'ok');
    }
    if($mal['description'] === "Manage Subtopics"){
      $this->session->set_userdata('manage_subtopics', 'ok');
    }
    if($mal['description'] === "Assign Option Subject"){
      $this->session->set_userdata('assign_option_subject', 'ok');
    }
    if($mal['description'] === "Shift Student To Another Stream"){
      $this->session->set_userdata('shift_student_to_another_stream', 'ok');
    }
    if($mal['description'] === "Add Subjects To Teacher"){
      $this->session->set_userdata('add_subjects_to_teacher', 'ok');
    }
    if($mal['description'] === "View Subjects Teachers"){
      $this->session->set_userdata('view_subject_teachers', 'ok');
    }
    if($mal['description'] === "Upload Student's Photo"){
      $this->session->set_userdata('upload_student_photo', 'ok');
    }
  ?>
<?php endforeach; ?>

<?php endif; //End checking roles?>

<!-- END CODE FOR ALL LINKS THAS A PARTICULAR USER IS ALLOWED TO SEE -->

<!-- SPECIAL FOR TOD -->
<?php if($this->session->userdata('user_role') !== "Admin"): ?>
  <?php foreach($this->session->userdata('roles') as $assigned_role): ?>
      <?php 
        if($assigned_role['role_type_id'] == 9){ //Teacher On Duty
          $this->session->set_userdata('current_tod', 'ok');
          $this->session->set_userdata('mwalimu_wa_zamu', $this->session->userdata('staff_id'));
          $tod = TRUE;
        }
      ?>
  <?php endforeach; ?>
<?php endif; ?>


<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="nav_bar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-content" id="buton">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="text-left" id="user">
        Signed in as: 
        <?php 
          if($this->session->userdata('user_role') === "Admin"){
            echo $this->session->userdata('user_role');
          }
          else{
            echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname') . ' ' . $this->session->userdata('smiddlename');
          }
          //echo $this->session->userdata('class_stream')['class_stream_id'];
          //Grab teacher_id session
          $teacher_id = $this->session->userdata('staff_id');
          //Grab class_stream_id session
          $class_stream_id = $this->session->userdata('class_stream')['class_stream_id'];

          //Grab dept_id session
          $dept_id = $this->session->userdata('dept')['dept_id'];

          $dorm_id = $this->session->userdata('dormitory')['dorm_id'];

          $notifications = $this->session->userdata('notifications');

          $announcements = $this->session->userdata('announcements');

          //Kamata academic year zote
           $years = $this->session->userdata("miaka");

           //Kamata past academic year zote
           $past_years = $this->session->userdata("past_miaka");

           //Get terms of current academic year kwa ajili ya matokeo
          $terms = $this->session->userdata("mihula");


          //echo "You have: " . $notifications;

        ?>        
      </a>
        &nbsp;
     <!--  <a href="<?php echo base_url() . 'logout'; ?>"><i class="glyphicon glyphicon-log-out" id="logout"> </i></a>
       -->
      <div class="text-right" id="notifysmall">
       <a href="<?php echo base_url() . 'logout'; ?>"><i class="glyphicon glyphicon-log-out" id="logout"> </i></a>
        &nbsp;
        <a href="<?php echo base_url() . 'notification/view/'.$teacher_id; ?>"><span class="fa fa-bell fa-lg" id="bell"></span>
          <?php
            if(!empty($notifications)){
              echo "<span class='badge' id='notify'>".
               $notifications;
            }
           echo "</span>";
          ?>
        </a>
        <?php
          $notification_messages = $this->session->userdata('notification_messages');
          foreach ($notification_messages as $key => $value) {
        ?>
          <!-- <a href="<?php echo base_url() . 'notification/view/'.$teacher_id; ?>"><?php echo $value['msg']; ?></a><br/> -->
        <?php
          }
        ?>
        &nbsp;&nbsp;
        </div>

      </div>
    </div>
      
 

   <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
      <ul class="nav navbar-nav navbar-right">
       <li>
         <a href="<?php echo base_url() . 'logout'; ?>"><i class="glyphicon glyphicon-log-out" id="logout" data-toggle="tooltip" data-placement="bottom" title="logout"> </i></a>
      
       </li>
        <li>
        <a href="<?php echo base_url() . 'notification/view/'.$teacher_id; ?>"><span class="fa fa-bell fa-lg" id="bell" data-toggle="tooltip" data-placement="bottom" title="notifications"></span>
          <?php
            if(!empty($notifications)){
              echo "<span class='badge' id='notify' data-toggle='tooltip' data-placement='bottom' title='notifications'>".
               $notifications;
            }
           echo "</span>";
          ?>
        </a></li>
        <?php
          $notification_messages = $this->session->userdata('notification_messages');
          foreach ($notification_messages as $key => $value) {
        ?>
          <!-- <a href="<?php echo base_url() . 'notification/view/'.$teacher_id; ?>"><?php echo $value['msg']; ?></a><br/> -->
        <?php
          }
        ?>
        <!-- <li><a href="#"><span class="glyphicon glyphicon-cog"></span></a></li> -->

        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="row" >
  <div class="col-lg-3 col-xl-2 col-md-3">
    <div class="nav-side-menu">
    <div class="brand"> </div>
   <!--  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
   -->
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse ">
               <li>
                  <a href="<?php echo base_url() . 'home'; ?>">
                  <i class="fa fa-dashboard fa-lg"></i>MAIN MENU
                  </a>
                </li>
                <!-- Added Admin Modules -->
              <?php if($admin_settings === TRUE): ?>
                <li  data-toggle="collapse" data-target="#admin_panel" class="collapsed ">
                  <a href="#"><i class="fa fa-cog fa-lg"></i> Admin Settings  <span class="arrow"></span></a>
                </li>
              <?php endif; ?>
                <ul class="sub-menu collapse" id="admin_panel">
                    <li ><a href="">System Settings</a></li>
                <?php if($manage_role === TRUE): ?>
                    <li ><a href="<?php echo base_url() . 'admin/roles'; ?>">Manage Roles</a></li>
                <?php endif; ?>
                <?php if($manage_groups === TRUE): ?>
                    <li ><a href="<?php echo base_url() . 'admin/groups'; ?>">Staff Groups</a></li>
                <?php endif; ?>
                  <!-- <li ><a href="<?php echo base_url() . 'admin/admin_groups'; ?>">Admin Groups</a></li> -->
                <?php if($view_activity_log === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'admin/activity_log_records'; ?>">Log Events</a></li>
                <?php endif; ?>
                <?php if($view_audit_trail === TRUE): ?>
                  <li ><a onClick="authenticateUser()" href="#">Audit Trail</a></li>
                <?php endif; ?>
                <?php if($backup_db === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'admin/db_backup'; ?>">Backup Database</a></li>
                <?php endif; ?>
                </ul>
              <!--END ADMIN SETTINGS-->

              <!-- START ACADEMIC YEAR -->
            <?php if($academic_year_module === TRUE): ?>
              <li  data-toggle="collapse" data-target="#academic_year" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Academic Year  <span class="arrow"></span></a>
              </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="academic_year">
            <?php if($add_academic_year === TRUE): ?>     
                <li ><a href="<?php echo base_url() . 'year/add_academic_year'; ?>">Create Academic Year</a></li>
            <?php endif; ?>
            <?php if($view_academic_year === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'year/academic_years'; ?>">View Academic Year</a></li>
            <?php endif; ?>
              </ul>
              <!-- END ACADEMIC YEAR -->

            <?php if($myaccount === TRUE): // My Account IF STATEMENT?>
                <li data-toggle="collapse" data-target="#myAccount" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> My Account <span class="arrow"></span></a>
                </li>  
            <?php endif; //END My Account Module IF STATEMENT ?>
                <ul class="sub-menu collapse" id="myAccount">
            <?php if($change_password === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                  <li><a href="<?php echo base_url() . 'staffs/change_password/'.$teacher_id; ?>">Change Password</a></li>
            <?php endif; ?>
            <?php if($this->session->userdata('user_role') === "Admin"): ?>
                  <li><a href="<?php echo base_url() . 'admin/change_password/'.$teacher_id; ?>">Change Password</a></li>
            <?php endif; ?>
            <?php if($view_own_profile === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                  <li><a href="<?php echo base_url() . 'staffs/myProfile/'.$teacher_id; ?>">Profile</a></li>
            <?php endif; ?>
            <?php if($add_own_qualification === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                  <li><a href="<?php echo base_url() . 'staffs/add_staff_qualification/'.$teacher_id; ?>">Add Qualification</a></li>
            <?php endif; ?>
                </ul>
            <?php if($announcements_module === TRUE): ?>
                <li data-toggle="collapse" data-target="#announcement" class="collapsed">
                    <a href="#"><i class="fa fa-users fa-lg" ></i> Announcement
                      <?php 
                        if($announcements){
                      ?>
                          <span class="badge">
                            <?php 
                              if($this->session->userdata('user_role') !== "Admin"){
                                echo $announcements;
                              }
                            ?> 
                          </span>
                      <?php
                        }
                        else{
                          if($this->session->userdata('user_role') !== "Admin"){
                            echo 0;
                          }
                        }
                      ?>
                    <span class="arrow"></span>
                    </a>
                  </li>
            <?php endif; //END IF STATEMENT announcement_module?>
                  <ul class="sub-menu collapse" id="announcement">
            <?php if($manage_posted_announcements === TRUE): ?>
                    <li><a href="<?php echo base_url() . 'staffs/new_announcement'; ?>">New Announcement</a></li>
            <?php endif; ?>
            <?php if($manage_posted_announcements === TRUE): ?>
                    <li><a href="<?php echo base_url() . 'staffs/announcements/'.$teacher_id; ?>">Posted Announcement</a></li>
            <?php endif; ?>
            <?php if($announcements_link === TRUE  && $this->session->userdata('user_role') !== "Admin"): ?>
                    <li><a href="<?php echo base_url() . 'staffs/my_announcements/'.$teacher_id; ?>">Announcement
                       <?php if($announcements): ?>
                    <span class="badge"><?php echo $announcements; ?></span></a></li>  
                        <?php endif; ?>  
            <?php endif; ?>          
                  </ul>

            <?php if($view_own_roles === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                 <li>
                  <a href="#" data-toggle="modal" data-target="#myRoleModal<?php echo $teacher_id; ?>" ><i class="fa fa-users fa-lg"></i> Role</a>
                </li>
            <?php endif; ?>
            <?php if($timetable === TRUE): // TIMETABLE Module ?>
                 <!-- <li>
                  <a href="<?php echo base_url() . 'teacher/mytimetable/'.$teacher_id; ?>"><i class="fa fa-users fa-lg"></i> Time Table</a>
                </li> -->
            <li data-toggle="collapse" data-target="#timetable" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Time Table <span class="arrow"></span></a>
                </li>
            <?php endif; // TIMETABLE MODULE ?>
                <ul class="sub-menu collapse" id="timetable">
            <?php if($view_personal_timetable === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                    <li><a href="<?php echo base_url() . 'teacher/mytimetable/'.$teacher_id; ?>">My Timetable</a></li>
            <?php endif; ?>   
            <?php if($manage_timetable === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'classes/create_new_timetable'; ?>">Add Timetable</a></li>
            <?php endif; ?>
            <?php if($school_timetable === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/classes_timetable'; ?>">School Timetable</a></li>
            <?php endif; ?>     
                </ul>


                <!-- <li  data-toggle="collapse" data-target="#products" class="collapsed"> -->
                  <!-- <a href="#"><i class="fa fa-gift fa-lg"></i> Classes  <span class="arrow"></span></a> -->
            <?php if($myclasses === TRUE && $this->session->userdata('user_role') !== "Admin"): //My Assigned Classes Module ?>
                  <li>
                  <a href="<?php echo base_url() . 'teacher/assigned_classes/'.$teacher_id; ?>">
                  <i class="fa fa-users fa-lg"></i> My Assigned Classes
                  </a>
                </li>
            <?php endif; // END My Assigned Classes ?>

            <?php if($exams === TRUE): // Exams Module IF STATEMENT ?>
                <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Exams <span class="arrow"></span></a>
                </li> 
            <?php endif; // END Exams IF STATEMENT ?> 
                <ul class="sub-menu collapse" id="service">
                <!-- START ADMIN EXAMS -->
            <?php if($view_exam_types === TRUE): ?>
                <li><a href="<?php echo base_url() . 'exam/exam_types'; ?>">Exam Types</a></li>
            <?php endif; ?>
            <?php if($view_division === TRUE): ?>
                <li><a href="<?php echo base_url() . 'exam/division'; ?>">Division</a></li>
            <?php endif; ?>
            <?php if($view_a_grade === TRUE): ?>
                <li><a href="<?php echo base_url() . 'exam/a_grade'; ?>">A Level Grade System</a></li>
            <?php endif; ?>
            <?php if($view_o_grade === TRUE): ?>
                <li><a href="<?php echo base_url() . 'exam/o_grade'; ?>">O Level Grade System</a></li>
            <?php endif; ?>
                <!-- END ADMIN EXAMS -->
            <!-- <?php if($add_score === TRUE): ?>
                  <li><a href="<?php echo base_url() . 'mysubjects/add_score/'.$teacher_id; ?>">Add Score</a></li>
            <?php endif; ?> -->
            <?php if($view_results === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
                  <li><a href="<?php echo base_url() . 'students/subject_results/'.$teacher_id; ?>">Score & Results</a></li>
            <?php endif; ?>
                </ul>
                
            <?php if($tod === TRUE && $this->session->userdata('user_role') !== "Admin"): //TOD MOdule?>
              <li  data-toggle="collapse" data-target="#tod" class="collapsed "><a href="#"><i class="fa fa-gift fa-lg"></i> TOD  <span class="arrow"></span></a></li>
            <?php endif; ?>
                <ul class="sub-menu collapse" id="tod">
              <li ><a href="<?php echo base_url() . 'classes/class_attendance'; ?>">Daily Students Attendance</a></li>
              <li ><a href="<?php echo base_url() . 'school_attendance_report/'.date('Y-m-d'); ?>">Students Attendance Report</a></li>
              <li ><a href="<?php echo base_url() . 'teachers/daily_routine/'.$teacher_id; ?>">Add Routines</a></li>
              </ul>
 
            <?php if($myclass === TRUE && $this->session->userdata('user_role') !== "Admin"): //Class Teacher Module?>
                <li  data-toggle="collapse" data-target="#cmanagement" class="collapsed "><a href="#"><i class="fa fa-gift fa-lg"></i> My Class  <span class="arrow"></span></a></li>
            <?php endif; ?>            
              <ul class="sub-menu collapse" id="cmanagement">
            <?php if($add_class_attendance === TRUE): //Class Teacher Module?>
                <li ><a href="<?php echo base_url() . 'classes/add_attendance/'.$class_stream_id; ?>">Add Students Attendance</a></li>
            <?php endif; ?>
            <?php if($class_attendance_report === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'classes/attendance_report/'.$class_stream_id; ?>">Students Attendance Report</a></li>
            <?php endif; ?>
            <?php if($students_in_my_class === TRUE): ?>
                <li><a href="<?php echo base_url() . 'myclass/students/'.$class_stream_id; ?>">Students in My Class </a></li>
            <?php endif; ?>
              <!-- <li><a href="'.base_url().'classes/get_students_by_stream/'.$class_stream_id.'" data-target="'.$class_stream_id.'" class="mkondo_student_class_teacher_results">Students Results Bwiru</a></li>
              <li><a href="'.base_url().'classes/students_by_stream/'.$class_stream_id.'">Students Results</a></li> -->     
            <?php if($teachers_assigned === TRUE): ?>            
              <li><a href="<?php echo base_url() . 'myclass/subject_teachers/'.$class_stream_id; ?>">Teachers Assigned</li></a>
            <?php endif; ?>
            <?php if($add_class_journal_record === TRUE): ?>
              <li><a href="<?php echo base_url() . 'classes/load_class_journal/'.$class_stream_id; ?>">Class Journal</a></li>
            <?php endif; ?>
            <?php if($update_class_journal === TRUE): ?>
              <li><a href="<?php echo base_url() . 'classes/edit_teaching_attendance/'.$class_stream_id; ?>">Edit Class Journal</a></li>
            <?php endif; ?>
            <?php if($view_own_class_journal_report === TRUE): ?>
             <li><a href="<?php echo base_url() . 'classes/class_journal_weekly_report/'.$class_stream_id; ?>">Class Journal Report</a></li> 
            <?php endif; ?>               
            <?php //if($assets_in_my_class === TRUE): ?>
             <li><a href="<?php echo base_url() . 'classes/assets/'.$class_stream_id; ?>">Assets</a></li> 
            <?php //endif; ?>
              </ul>
            <?php if($view_own_class_results === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li  data-toggle="collapse" data-target="#class_result_term" class="collapsed ">
              <a href="#"><i class="fa fa-gift fa-lg"></i> Results Bwiru <span class="arrow"></span></a></li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="class_result_term">
                <?php foreach($terms as $m): ?>
                  <li><a href="<?php echo base_url(). 'classes/results_by_grade/'.$class_stream_id.'/'.$m['term_id']; ?>"> Results
                              <?php echo $m['term_name']; ?></a></li>
                <?php endforeach; ?>
              </ul>
            <?php if($view_own_class_results === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li  data-toggle="collapse" data-target="#class_result_term_musoma" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Results Musoma <span class="arrow"></span></a>
                        </li>
            <?php endif; ?>
                <ul class="sub-menu collapse" id="class_result_term_musoma">
                <?php foreach($terms as $m): ?>
                  <li><a href="<?php echo base_url(). 'classes/students_by_stream/'.$class_stream_id.'/'.$m['term_id']; ?>"> Results <?php echo $m['term_name']; ?></a></li>
                <?php endforeach; ?>
                </ul>                
            <?php if($accomodation === TRUE): ?>
              <li  data-toggle="collapse" data-target="#dommanagement" class="collapsed ">
              <a href="#"><i class="fa fa-gift fa-lg"></i> Dormitory Management  <span class="arrow"></span></a></li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="dommanagement">
            <?php if($manage_dormitory === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/add'; ?>">Add Dormitory</a></li>
            <?php endif; ?>
            <?php if($view_dormitories === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/view_all'; ?>">View Dormitories</a></li> 
            <?php endif; ?>
                </ul>
            <?php if($mydormitory === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li  data-toggle="collapse" data-target="#mydormmanagement" class="collapsed "><a href="#"><i class="fa fa-gift fa-lg"></i> My Dormitory  <span class="arrow"></span></a></li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="mydormmanagement">
            <?php if($students_in_dormitory === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/view_student/'.$dorm_id; ?>">Students in My Domitory</a></li>
            <?php endif; ?>
            <?php if($dormitory_assets === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/assets/'.$dorm_id; ?>">Assets in My Dormitory</a></li>
            <?php endif; ?>
            <?php if($add_roll_call === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/load_roll_call/'.$dorm_id; ?>">Add Roll-call Record</a></li>
            <?php endif; ?>
            <?php if($roll_call_in_my_dormitory === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'dormitory/roll_call_report/'.$dorm_id; ?>">Roll-call Report</a></li>
            <?php endif; ?>
            </ul>

            <?php if($library_management === TRUE/* && $this->session->userdata('user_role') !== "Admin"*/): ?>
              <li  data-toggle="collapse" data-target="#librarymanagement" class="collapsed ">
              <a href="#"><i class="glyphicon glyphicon-transfer"></i> Library Management  <span class="arrow"></span></a>
                </li>
            <?php endif; ?>
                <ul class="sub-menu collapse" id="librarymanagement">
                <li><a href="<?php echo base_url("library/borrower_types"); ?>">Set Borrowing Limit</a></li>
               <li><a href="<?php echo base_url("library/addbooktype"); ?>">Add Book Title</a></li>
                    <li ><a href="<?php echo base_url("library/retbooktype"); ?>">Book Titles</a></li>
                    <li ><a href="<?php echo base_url("library/book1_report"); ?>">Book Copies</a></li>
                    <li><a href="<?php echo base_url("library/available"); ?>">Borrow a Book Copy</a></li>
                    <li><a href="<?php echo base_url("library/borrowed"); ?>">Return a Book Copy</a></li>
                     <li><a href="<?php echo base_url("library/borrowed"); ?>">Borrowed Book Copies</a></li>
                    <li><a href="<?php echo base_url("library/lost_books"); ?>">Lost Copies</a></li>
                     <li><a href="<?php echo base_url("library/borrowing_history"); ?>">Borrowing History</a></li>
                      <li><a href="<?php echo base_url("library/expired_books"); ?>">Expired Books</a></li>
                    
                     <li ><a href="<?php echo base_url("library/title_sum_up"); ?>">Report</a></li>

                </ul>
                   
                
            <?php if($administration === TRUE): ?>
              <li  data-toggle="collapse" data-target="#schoolmanagement" class="collapsed "><a href="#"><i class="fa fa-gift fa-lg"></i> School Administration  <span class="arrow"></span></a></li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="schoolmanagement">
            <?php if($manage_periods === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'periods/new_period'; ?>">Add Period</a></li>
            <?php endif; ?>
            <?php if($view_periods === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'periods'; ?>">Periods</a></li>
            <?php endif; ?>            
            <?php if($view_school_attendance === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'school_attendance_report/'.date('Y-m-d'); ?>">Attendance Report</a></li>
            <?php endif; ?>
            <?php if($view_teachers === TRUE): ?>
              <li><a href="<?php echo base_url() . 'teachers'; ?>">View Teachers</a></li>
            <?php endif; ?>                          
              </ul>
            <?php if($school_results === TRUE): ?>
              <li>
                <a href="<?php echo base_url() . 'school/matokeo'; ?>"><i class="fa fa-users fa-lg"></i> Matokeo </a>
              </li>
            <?php endif; ?>
            <!-- <?php if($school_results === TRUE): ?>
              <li  data-toggle="collapse" data-target="#result_academic" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Results <span class="arrow"></span></a>
              </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="result_academic">
                  <?php foreach($years as $m): ?>
              <li><a href="<?php echo base_url() . 'classes/student_results/'.$m['year']; ?>"> Results
                      <?php echo $m['year']; ?></a></li>                     
                  <?php endforeach; ?>
              </ul> -->
            <?php if($manage_alumni === TRUE): ?>
              <li  data-toggle="collapse" data-target="#past_academic_years_academic" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Alumni <span class="arrow"></span></a>
                </li>
            <?php endif; ?>
                <ul class="sub-menu collapse" id="past_academic_years_academic">
            <?php if($manage_alumni === TRUE): ?>
                <li><a href="<?php echo base_url(). 'students/alumni'; ?>">All Students </a></li>
            <?php endif; ?>
                <!-- <?php foreach($past_years as $m): ?>
                  <li><a href="<?php echo base_url().'students/alumni_year/'.$m['year']; ?>"> 
                  <?php echo $m['year']; ?></a></li>
                <?php endforeach; ?>-->
                </ul>

            <?php if($mydepartment === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li  data-toggle="collapse" data-target="#hod" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> My Department <span class="arrow"></span></a>
                                </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="hod">
            <?php if($department_staffs === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'departments/get_staff_by_department/'.$dept_id; ?>">View Staff</a></li>
            <?php endif; ?>
            <?php if($assign_teacher_a_subject === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'departments/teaching_assignment_form/'.$dept_id; ?>">Assign Subject</a></li>
            <?php endif; ?>
            <?php if($view_subjects_assigned === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'departments/subject_assignment/'.$dept_id; ?>">Assigned Teachers</a></li>
            <?php endif; ?>
            <?php if($department_classes === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'departments/classes/'.$dept_id; ?>">View Classes</a></li><?php endif; ?>
            <?php if($logs === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'subject_teaching_logs/'.$dept_id; ?>">Logs</a></li>
	    <?php endif; ?>
            </ul>

           <?php if($class_management === TRUE): ?>
            <li data-toggle="collapse" data-target="#admin_cmanagement" class="collapsed ">
              <a href="#"><i class="fa fa-gift fa-lg"></i> Class Management  <span class="arrow"></span></a>
                </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="admin_cmanagement">
              <!-- START ADMIN CLASS -->
            <?php if($manage_class === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/add_class'; ?>">Add Class</a></li>
            <?php endif; ?>
            <?php if($view_classes === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/view_classes'; ?>">View Classes</a></li>
            <?php endif; ?>
            <?php if($manage_class === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/add_stream'; ?>">Add Stream</a></li>
            <?php endif; ?>
            <?php if($view_streams === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/streams'; ?>">View Streams</a></li>
            <?php endif; ?>
            <?php if($manage_class === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/add_class_stream'; ?>">Add Class Stream</a></li>
            <?php endif; ?>
              <!-- END ADMIN CLASS -->
            <?php if($view_class_streams === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes'; ?>">Class Streams</a></li>
            <?php endif; ?>
            <!-- <?php if($view_school_attendance === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'school_attendance_report/'.date('Y-m-d'); ?>">Students Attendance Report</a></li>
            <?php endif; ?> -->
              </ul>

            <!-- START SUBJECT  -->
            <?php if($subjects_module === TRUE): ?>
              <li  data-toggle="collapse" data-target="#Subject" class="collapsed ">
                <a href="#"><i class="fa fa-book fa-lg"></i> Subject  <span class="arrow"></span></a>
              </li>
            <?php endif; ?>
            <ul class="sub-menu collapse" id="Subject">
            <?php if($manage_subjects === TRUE): ?>   
                <li ><a href="<?php echo base_url() . 'subjects/add_new_subject'; ?>">Add Subject</a></li>
            <?php endif; ?>
            <?php if($view_subjects === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'subjects'; ?>">View Subjects</a></li>
            <?php endif; ?>
            <?php if($manage_class_subjects === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'subjects/assign_subject_to_class'; ?>">Assign Subject to Class</a></li>
            <?php endif; ?>
            <?php if($view_class_stream_subjects === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'subjects/class_stream_subjects'; ?>">View to Class Subjects</a></li>
            <?php endif; ?>
                <!-- <li ><a href="#">Assign Student to Subject </a></li>   -->             

            </ul>
            <!-- END ADMIN SUBJECTS -->
            <?php if($discipline_management === TRUE): ?>
              <li  data-toggle="collapse" data-target="#displine" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Discipline Management  <span class="arrow"></span></a></li>
            <?php endif; ?>
                <ul class="sub-menu collapse" id="displine">
            <?php if($manage_indiscipline_records === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'discipline/add_record'; ?>">Add Indiscipline Case</a></li>
            <?php endif; ?>
            <?php if($indiscipline_record === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/discipline_records'; ?>">Indiscipline Cases</a></li>
            <?php endif; ?>
              </ul>

            <?php if($tod_management === TRUE): ?>
              <li  data-toggle="collapse" data-target="#tod_management" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> TOD"s Management  <span class="arrow"></span></a>
                </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="tod_management">
            <?php if($manage_tods === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'teachers/assign_new_tod'; ?>">Assign TOD</a></li>
            <?php endif; ?>
            <?php if($tods === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'teachers/teachers_duty_roaster'; ?>">View TODs</a></li>
            <?php endif; ?>
            <?php if($daily_student_attendance === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'classes/class_attendance'; ?>">Daily Students Attendance</a></li>
            <?php endif; ?>
            <?php if($view_school_attendance === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li ><a href="<?php echo base_url() . 'school_attendance_report/'.date('Y-m-d'); ?>">School Attendance Report</a></li>
            <?php endif; ?>
            <?php if($manage_tod_reports === TRUE && $this->session->userdata('user_role') !== "Admin"): ?>
              <li ><a href="<?php echo base_url() . 'teachers/head_tod_info'; ?>">Daily TOD Routine</a></li>
            <?php endif; ?>
              </ul>

            <?php if($student_management === TRUE): ?>
              <li  data-toggle="collapse" data-target="#stmanagement" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Student Management  <span class="arrow"></span></a>
              </li>
            <?php endif; ?>
              <ul class="sub-menu collapse" id="stmanagement">
            <?php if($promote_students === TRUE): ?>
              <li><a href="<?php echo base_url() . 'admin/promote'; ?>">Promote Students</a></li>
            <?php endif; ?>
            <?php if($add_new_student === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/new_student'; ?>">Add New Student</a></li>
            <?php endif; ?>
            <?php if($registered_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/registered'; ?>">Registered Students</a></li>
            <?php endif; ?>
            <?php if($disabled_students === TRUE): ?>
              <li><a href="<?php echo base_url() . 'students/disabled'; ?>">View Disabled Students </a></li>
            <?php endif; ?>
            <!-- <?php if($view_class_streams === TRUE): ?>     
              <li ><a href="<?php echo base_url() . 'classes'; ?>">Classes</a></li>
            <?php endif; ?> -->
              <!-- <li ><a href="'.base_url().'students/remove">Remove Student</a></li> -->
            <?php if($manage_selected_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/add_selected'; ?>">Add Selected Students</a></li>
            <?php endif; ?>
            <?php if($selected_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/selected'; ?>">Selected Students</a></li>
            <?php endif; ?>
            <?php if($transferred_in_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/transferred_in'; ?>" class="transferred-in-students-class" data-target="transferred_in">Transferred IN Students</a></li>
            <?php endif; ?>
            <?php if($transferred_out_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/transferred_out'; ?>" class="transferred-students-class" data-target="transferred_out">Transferred OUT Students</a></li>
            <?php endif; ?>
            <?php if($suspended_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/suspended'; ?>" class="expelled-students-class" data-target="Suspended">Suspended Students</a></li>
            <?php endif; ?>
            <?php if($suspended_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/expelled'; ?>" class="expelled-students-class" data-target="Expelled">Expelled Students</a></li>
            <?php endif; ?>
            <?php if($selected_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/unreported'; ?>">Unreported Students</a></li>
            <?php endif; ?>
            <!-- <?php if($enrolled_students === TRUE): ?>
              <li ><a href="<?php echo base_url() . 'students/enrolled'; ?>">Enrolled Students</a></li>
            <?php endif; ?> -->
                </ul>

              <?php if($staff_management === TRUE): ?>
                <li  data-toggle="collapse" data-target="#Staff" class="collapsed ">
                  <a href="#"><i class="fa fa-user fa-lg"></i> Staff Management <span class="arrow"></span></a>
                </li>
              <?php endif; ?>
                <ul class="sub-menu collapse" id="Staff">
                <!-- START ADMIN STAFF -->
              <!-- <?php if($manage_staff === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'admin/new_staff'; ?>" >Add Staff</a></li>
              <?php endif; ?> -->
              <?php if($manage_staff_accounts === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/manage_accounts'; ?>">Manage Staffs</a></li>
              <?php endif; ?>
              <?php if($passwords_reset === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/password_reset'; ?>">Passwords Reset</a></li>
              <?php endif; ?>
                <!-- END ADMIN STAFF -->
              <?php if($manage_staff === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/new_staff'; ?>">Add Staff</a></li>
              <?php endif; ?>
              <?php if($view_staffs === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/index'; ?>">View Staff</a></li>
              <?php endif; ?>
              <?php if($manage_staff_attendance === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/load_new_attendance'; ?>">Add Attendance</a></li>
              <?php endif; ?>
              <?php if($staff_attendance === TRUE): ?>
                <li ><a href="<?php echo base_url() . 'staffs/attendance_report'; ?>">Staff Attendance</a></li>
              <?php endif; ?>
                </ul>

              <?php if($department_management === TRUE && $view_departments === TRUE): ?>
                <li  data-toggle="collapse" data-target="#department" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Department Management  <span class="arrow"></span></a>
                </li>
              <?php endif; ?>
                <ul class="sub-menu collapse" id="department">
              <?php if($manage_department === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'departments/new_department'; ?>">Add Department</a></li>
              <?php endif; ?>
              <?php if($view_departments === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'departments'; ?>"> Departments</a></li>
              <?php endif; ?>
                </ul>
              <!-- <?php if($accomodation === TRUE): ?>
                <li  data-toggle="collapse" data-target="#domeemanagement" class="collapsed ">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Dormitory Management  <span class="arrow"></span></a>
                </li>
              <?php endif; ?>
                <ul class="sub-menu collapse" id="domeemanagement">
              <?php if($manage_dormitory === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'dormitory/add'; ?>">Add Dormitory</a></li>
              <?php endif; ?>
              <?php if($view_dormitories === TRUE): ?>
                  <li ><a href="<?php echo base_url() . 'dormitory/view_all'; ?>">View Dormitories</a></li>
              <?php endif; ?>
                </ul> -->

              <!-- <?php if($manage_finance === TRUE): ?>
                <li  data-toggle="collapse" data-target="#Finance" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Finance  <span class="arrow"></span></a>
                </li>
              <?php endif; ?>
              <ul class="sub-menu collapse" id="Finance">

                  <li ><a href="#">Create Transaction</a></li>
                  <li ><a href="#">Transactions</a></li>
                  <li ><a href="#">Receive Payment</a></li>
                  <li ><a href="#">Transaction Analysis</a></li>
                  <li ><a href="#">Full Paid</a></li>
                  <li ><a href="#">Uncomplete Paid</a></li>
                  
                  
              </ul> -->
              <!-- END LIBRARY AND FINANCE -->



              <?php //if($reports === TRUE): ?>
                <li  data-toggle="collapse" data-target="#reports" class="collapsed ">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Reports <span class="arrow"></span></a>
                </li>
              <?php //endif; ?>
                <ul class="sub-menu collapse" id="reports">
                <?php if($manage_dormitory): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/dormitories'; ?>">List Of Dormitories</a>
                  </li>
                <?php endif; ?>
                <?php if($manage_class): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/class_streams'; ?>">List Of Class Streams</a>
                  </li> 
                <?php endif; ?> 
                <?php if($manage_department): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/departments'; ?>">List Of Departments</a>
                  </li>
                <?php endif; ?> 
		 <?php if($manage_department): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/assigned_teachers'; ?>">List Of Assigned Teachers</a>
                  </li>
                <?php endif; ?>  
                <?php if($registered_students): ?>  
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/students'; ?>">List Of Students</a>
                  </li>
                <?php endif; ?>
                <?php if($enrolled_students): ?>
                    <li >
                      <a href="<?php echo base_url() . 'reports/enrolled'; ?>">Enrolled Students</a>
                    </li>
                <?php endif; ?>
                <?php if($manage_selected_students): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/selected'; ?>">List Of Selected Students</a>
                  </li> 
                <?php endif; ?>
                <?php if($transferred_in_students): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/transferred_in'; ?>">List Of Transferred IN Students</a>
                  </li>
                <?php endif; ?>
                <?php if($transferred_out_students): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/transferred_out'; ?>">List Of Transferred OUT Students</a>
                  </li>
                <?php endif; ?>
                <?php if($manage_staff): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/staffs'; ?>">List Of Staffs</a>
                  </li>
                <?php endif; ?>
                <?php if($this->session->userdata('manage_class_assets') == 'ok'): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/class_assets'; ?>">List Of Class Assets</a>
                  </li>
                <?php endif; ?>
                <?php if($this->session->userdata('manage_dormitory_assets') == 'ok'): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/assets'; ?>">List Of Dormitory Assets</a>
                  </li>
                <?php endif; ?>
                <?php if($disabled_students): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/disabled_students'; ?>">List Of Disabled Students</a>
                  </li> 
                <?php endif; ?>  
                <?php if($manage_staff_attendance): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/staff_attendance_report/'.date('Y-m'); ?>">Staff Attendance Report</a>
                  </li>
                <?php endif; ?>
                <?php if($registered_students): ?>
                  <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                    <a href="<?php echo base_url() . 'reports/parents'; ?>">List Of All Parents</a>
                  </li> 
                <?php endif; ?> 
                  <?php if($myclass === TRUE && $this->session->userdata('user_role') !== "Admin"): //Class Teacher Module?>
                    <li data-toggle="collapse" data-target="#dailyreports" class="collapsed ">
                      <a href="<?php echo base_url() . 'reports/wazazi/'.$class_stream_id; ?>">Students' Parents</a>
                    </li> 
                  <?php endif; ?>        
                </ul>
                  
            
            </ul>
     </div>
</div>
</div>





















<script type="text/javascript">

$('[data-toggle=collapse]').click(function(){
  
    // toggle icon
    $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
  
});

$('.collapse').on('show', function (e) {
  
    // hide open menus
    $('.collapse').each(function(){
      if ($(this).hasClass('in')) {
          $(this).collapse('toggle');
      }
    });
  
})

</script>






























<?php //print_r($this->session->userdata('roles')); ?>
<!-- START ROLES -->
<div class="modal fade" id="myRoleModal<?php echo $teacher_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "My Current Role(s):"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              <ul>
                <?php foreach($this->session->userdata('roles') as $assigned_role): ?>
                    <li><?php echo $assigned_role['role_name']; ?></li>
                <?php endforeach; ?>
              </ul>                               
            </div>
        </div>
    </div>
</div>
<!--END ROLES-->
<div class="col-lg-9 col-xl-10  col-md-9 " id="contents">
