
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">
<?php echo form_open('admin/groups'); ?>
			
		<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_group" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
         </div>

									<?php echo form_close(); ?>
							        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
						          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
						          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
						          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
						        </ul> -->
					 <!-- /btn-group -->
				</div>
		

		<a href="<?php echo base_url() . 'admin/add_new_group'; ?>" class="btn btn-primary btn-sm">Add New Group</a>

	</div>
</div>




<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Group Name</td>
			<td>Description</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($group_types == FALSE): ?>
    	<tr>
	    	<td colspan="3">
	    		<?php
                    $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Groups";
                    echo $message;
                ?>
	    	</td>
	    </tr>
	<?php else: ?>
		<?php
			foreach ($group_types as $key => $group):
		?>
			<tr>
				<td><?php echo $group['group_name']; ?></td>
				<td><?php echo $group['description']; ?></td>
				<td>
					<a href="<?php echo base_url(); ?>admin/view_members/<?php echo $group['group_id']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="View Members"><span class="fa fa-eye"></span></a>&nbsp;
					<a href="<?php echo base_url(); ?>admin/edit_group/<?php echo $group['group_id']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Group"><span class="fa fa-pencil"></span></a>&nbsp;
					<a href="<?php echo base_url(); ?>admin/delete_group/<?php echo $group['group_id']; ?>" class="btn btn-xs btn-danger" onClick="return confirm('Are you sure you want to delete this group?');" data-toggle="tooltip" data-placement="bottom" title="Remove Group"><span class="glyphicon glyphicon-trash"></span></a>
				</td>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
</div>
</div>
</div>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/roles'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>