
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<?php if($x === "student_results"): ?>
	<?php echo form_open('admin/audit_student_results'); ?>
<?php endif; ?>

<?php if($x === "student_attendance"): ?>
	<?php echo form_open('admin/audit_student_attendance_data'); ?>
<?php endif; ?>

<?php if($x === "staff_attendance"): ?>
	<?php echo form_open('admin/audit_staff_attendance_data'); ?>
<?php endif; ?>

<input type="text" class="form-control input-sm" name="search_record" placeholder="Search ..." id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
<?php echo form_close(); ?>
        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
        </ul> -->
      </div><!-- /btn-group -->
</div>
</div>

</div>
</div>
<div id="successMessage" class="success_message_color">
	<?php 
		if($this->session->flashdata('success_message')){
			echo $this->session->flashdata('success_message');
		}
	?>
</div>
<div id="errorMessage" class="error_message_color">
	<?php
		if($this->session->flashdata('error_message')){
			echo $this->session->flashdata('error_message');
		}
	?>
</div>
<div id="warningMessage" class="error_message_color">
	<?php
		if($this->session->flashdata('exist_message')){
			echo $this->session->flashdata('exist_message');
		}
	?>
</div>

<?php if($x === "student_results"): ?>
	<table class="table table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr class="table-header">
				<th width="20%">Student Names</th>
				<th width="10%">Class</th>
				<th width="8%">Year</th>
				<th width="10%">Term</th>
				<th width="10%">Exam Date</th>
				<td width="5%">Action</td>
				<td width="5%">Marks</td>
				<td width="10%">Source</td>
				<td width="20%">Time Of Change</td>
				<th width="25%">User</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($audit_student_results_data == FALSE): ?>
	        <tr>
	          <td colspan="10">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No records";
	                        echo $message;
	                    ?>
	                </td>
	        </tr>
	    <?php else: ?>
			<?php
				$adm_no = "";	//Flag
				foreach ($audit_student_results_data as $key => $asrd):				
			?>
				<tr>
					<td><?php echo $asrd['student_names']; ?></td>
					<td><?php echo $asrd['class_name']; ?></td>
					<td><?php echo $asrd['year']; ?></td>
					<td><?php echo $asrd['term_name']; ?></td>
					<td><?php echo $asrd['e_date']; ?></td>
					<td><?php echo $asrd['change_type']; ?></td>
					<td>
						<?php 
							if($adm_no != $asrd['admission_no']){
								echo "<strong>" . $asrd['marks'] . "&nbsp;<span title='Currently' data-placement='bottom' style='color:#0000FF';>*</span></strong>"; //Bold for current
							}
							else{
								echo $asrd['marks'];
							}
						?>
					</td>
					<td><?php echo $asrd['ip_address']; ?></td>					
					<td><?php echo $asrd['change_time']; ?></td>
					<td><?php echo "<span style='color:#0000FF';>". $asrd['added_changed_by'] . "</span>"; ?></td>
				</tr>
			<?php
				$adm_no = $asrd['admission_no'];	//Assign for flag
				endforeach;
			?>
			<?php endif; ?>
		</tbody>
	</table>

	    <div align="right">
	      <?php echo $links; ?>      
	    </div>
	  </div>
	</div>
<?php endif; ?>

<?php if($x === "student_attendance"): ?>
	<table class="table table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr class="table-header">
				<th width="20%">Student Names</th>
				<th width="8%">Class</th>
				<th width="8%">Att Desc</th>
				<th width="10%">Att Date</th>
				<th width="10%">Day</th>
				<td width="5%">Action</td>
				<td width="10%">Source</td>
				<td width="20%">Time Of Change</td>
				<th width="25%">User</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($audit_student_attendance_data == FALSE): ?>
	        <tr>
	          <td colspan="9">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No records";
	                        echo $message;
	                    ?>
	                </td>
	        </tr>
	    <?php else: ?>
			<?php
				$adm_no = "";	//Flag
				foreach ($audit_student_attendance_data as $key => $assad):				
			?>
				<tr>
					<td><?php echo $assad['firstname'] . " " . $assad['lastname']; ?></td>
					<td>
						<?php 
							if($adm_no != $assad['admission_no']){
								echo "<strong>" . $assad['att_desc'] . "&nbsp;<span title='Currently' data-placement='bottom' style='color:#0000FF';>*</span></strong>"; //Bold for current
							}
							else{
								echo $assad['att_desc'];
							}
						?>
					</td>
					<td><?php echo $assad['class_name']; ?></td>
					<td><?php echo $assad['date_']; ?></td>
					<td><?php echo $assad['day']; ?></td>
					<td><?php echo $assad['change_type']; ?></td>
					<td><?php echo $assad['ip_address']; ?></td>
					<td><?php echo $assad['change_time']; ?></td>
					<td><?php echo "<span style='color:#0000FF';>". $assad['added_changed_by'] . "</span>"; ?></td>
				</tr>
			<?php
				$adm_no = $assad['admission_no'];	//Assign for flag
				endforeach;
			?>
			<?php endif; ?>
		</tbody>
	</table>

	    <div align="right">
	      <?php echo $links; ?>      
	    </div>
	  </div>
	</div>
<?php endif; ?>

<?php if($x === "staff_attendance"): ?>
	<table class="table table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr class="table-header">
				<th width="20%">Staff Names</th>
				<th width="8%">Att Desc</th>
				<th width="10%">Att Date</th>
				<th width="10%">Day</th>
				<td width="5%">Action</td>
				<td width="10%">Source</td>
				<td width="20%">Time Of Change</td>
				<th width="25%">Add/Changed By</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($audit_staff_attendance_data == FALSE): ?>
	        <tr>
	          <td colspan="8">
	                    <?php
	                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No records";
	                        echo $message;
	                    ?>
	                </td>
	        </tr>
	    <?php else: ?>
			<?php
				$sid = "";	//Flag
				foreach ($audit_staff_attendance_data as $key => $assad):				
			?>
				<tr>
					<td><?php echo $assad['firstname'] . " " . $assad['lastname']; ?></td>
					<td>
						<?php 
							if($sid != $assad['staff_id']){
								echo "<strong>" . $assad['att_desc'] . "&nbsp;<span title='Currently' data-placement='bottom' style='color:#0000FF';>*</span></strong>"; //Bold for current
							}
							else{
								echo $assad['att_desc'];
							}
						?>
					</td>
					<td><?php echo $assad['date_']; ?></td>
					<td><?php echo $assad['day']; ?></td>
					<td><?php echo $assad['change_type']; ?></td>
					<td><?php echo $assad['ip_address']; ?></td>
					<td><?php echo $assad['change_time']; ?></td>
					<td><?php echo "<span style='color:#0000FF';>". $assad['added_changed_by'] . "</span>"; ?></td>
				</tr>
			<?php
				$sid = $assad['staff_id'];	//Assign for flag
				endforeach;
			?>
			<?php endif; ?>
		</tbody>
	</table>

	    <div align="right">
	      <?php echo $links; ?>      
	    </div>
	  </div>
	</div>
<?php endif; ?>


<!-- REPLACE INTO notification (staff_id, msg_id, msg, time) VALUES(tod_id, 9, concat("Your are the current teacher on duty, the duty starts FROM: Today: ", "<b>", today_date, "</b>", "TO: ", "<b>", e_date, "</b>"), NOW()); -->
