
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
                <!-- <?php echo form_open('periods'); ?>
                <input type="text" class="form-control input-sm" name="search_period" placeholder="Search ..." id="form-search-input" /> -->
                  <div class="input-group-btn">
                      <!-- <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?> -->
                            
                    </div>
              </div>
            </div>
          </div>
      </div>

</div>
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td width="5%" align="center" >S/No. </td>
			<td width="85%" align="center">Description</td>
			<td width="10%" align="center">Explore</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td align="center">1.</td>
			<td align="center">Audit Records For Students' Academic Results</td>
			<td align="center"><a href="<?php echo base_url() . 'admin/audit_student_results'; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></td>
		</tr>
		<tr>
			<td align="center">2.</td>
			<td align="center">Audit Records For Staff Attendances</td>
			<td align="center"><a href="<?php echo base_url() . 'admin/audit_staff_attendance_data'; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></td>
		</tr>
		<tr>
			<td align="center">3.</td>
			<td align="center">Audit Records For Student Attendances</td>
			<td align="center"><a href="<?php echo base_url() . 'admin/audit_student_attendance_data'; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></td>
		</tr>
		<tr>
			<td align="center">4.</td>
			<td align="center"></td>
			<td align="center"><a href="" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></td>
		</tr>
	</tbody>
</table>



<!-- SELECT concat(student.firstname, " ", student.lastname) AS student_names, subject_name, exam_name, audit_student_subject_assessment.marks, class_name, e_date, term_name, year, change_type, ip_address, change_time, concat(staff.firstname, " ", staff.lastname) AS added_changed_by  FROM student JOIN student_subject_assessment ON(student.admission_no = student_subject_assessment.admission_no) JOIN audit_student_subject_assessment ON(student_subject_assessment.ssa_id = audit_student_subject_assessment.ssa_id) JOIN subject ON(student_subject_assessment.subject_id = subject.subject_id) JOIN exam_type ON(student_subject_assessment.etype_id = exam_type.id) JOIN class_stream ON(student_subject_assessment.class_stream_id = class_stream.class_stream_id) JOIN class_level ON(class_level.class_id = class_stream.class_id) JOIN term ON(student_subject_assessment.term_id = term.term_id) JOIN academic_year ON(academic_year.id = term.aid) JOIN staff ON(staff.staff_id = audit_student_subject_assessment.change_by)
UNION
SELECT concat(student.firstname, " ", student.lastname) AS student_names, subject_name, exam_name, audit_student_subject_assessment.marks, class_name, e_date, term_name, year, change_type, ip_address, change_time, username as added_changed_by  FROM student JOIN student_subject_assessment ON(student.admission_no = student_subject_assessment.admission_no) JOIN audit_student_subject_assessment ON(student_subject_assessment.ssa_id = audit_student_subject_assessment.ssa_id) JOIN subject ON(student_subject_assessment.subject_id = subject.subject_id) JOIN exam_type ON(student_subject_assessment.etype_id = exam_type.id) JOIN class_stream ON(student_subject_assessment.class_stream_id = class_stream.class_stream_id) JOIN class_level ON(class_level.class_id = class_stream.class_id) JOIN term ON(student_subject_assessment.term_id = term.term_id) JOIN academic_year ON(academic_year.id = term.aid) JOIN admin ON(admin.id = audit_student_subject_assessment.change_by)-->


