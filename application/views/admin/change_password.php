
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline">
        <div class="form-group has-feedback no-margin">
        	<!--ADDED FOR AJAX -->
		<br />
		<div class="password_result"></div>
		<!--ADDED FOR AJAX -->
    	</div>
	</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form') ?>
	<?php echo form_open('admin/change_password/'.$id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="current_password">Current Password :</label>
			<!-- <div class="col-sm-9">
			<div class="success_message_color" id="successMessage">
				<?php echo $this->session->flashdata('success_message'); ?>
			</div>
			<div class="error_message_color" id="errorMessage">
				<?php echo $this->session->flashdata('error_message'); ?>
			</div> -->
			<div class="col-sm-9">
				<input id="current_password" type="password" name="current_password" class="form-control"  />
				<div class="error_message_color">
					<?php echo form_error('current_password'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="new_password">New Password :</label>
			<div class="col-sm-9">
				<input id="new_password" type="password" name="new_password" class="form-control"  />
				<div class="error_message_color">
					<?php echo form_error('new_password'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="confirm_new_password">Confirm New Password :</label>
			<div class="col-sm-9">
				<input id="confirm_new_password" type="password" name="confirm_new_password" class="form-control"  />
				<div class="error_message_color">				
					<?php echo form_error('confirm_new_password'); ?>
				</div>
			</div>
			<div class="confirm_error"></div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_passcode" value="Save" />
		</div>
		<br/>
	<?php echo form_close(); ?>
</div>