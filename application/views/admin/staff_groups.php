
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
</div>

<a href="<?php echo base_url() . 'admin/add_or_remove_staff_to_group/'.$group_id; ?>" data-target="#" data-toggle="modal" class="btn btn-primary btn-sm">Add / Remove Staff To Group</a>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Group Name</td>
			<td>Members</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($staff_group == FALSE): ?>
    	<tr>
	    	<td colspan="4">There are currently No Members in this group</td>
	    </tr>
	<?php else: ?>
		<tr>
			<td><?php echo $staff_group['group_name']; ?></td>
			<td><?php echo $staff_group['group_members']; ?></td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
</div>
</div>
