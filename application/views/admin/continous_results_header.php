
<div class="white-area-content">
<div class="db-header clearfix">
	<div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
	    <div class="db-header-extra form-inline"> 
			<div class="form-group has-feedback no-margin">

			</div>

		<?php foreach($class_names as $cn): ?>
			<a href="<?php echo base_url() . 'classes/continous_results/'.$year.'/'.$term_id.'/'.$etype_id.'/'.$cn['class_id']; ?>" class="btn btn-primary btn-sm"><?php echo $cn['class_name']; ?></a>
			<?php //endif; ?>
		<?php endforeach; ?>
		</div>
</div>
