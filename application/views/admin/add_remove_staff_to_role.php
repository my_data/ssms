
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
</div>

<a href="<?php echo base_url() . 'admin/roles'; ?>" data-target="#" data-toggle="modal" class="btn btn-primary btn-sm">Back To Roles</a>

</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_new_staff_to_role()'); ?>
	 <?php echo form_open('admin/add_user_to_role/'.$role_type_id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staffs">Staff Names :</label>
			<div class="col-sm-10">
				<select name="staff_id[]" class="chosen-select form-control" multiple required>
					<?php foreach($staffs as $staff): ?>
						<option 
							<?php 
								foreach ($staff_role_records as $srr) {
									if($srr['staff_id'] === $staff['staff_id']){
										echo "selected='true'";
									}
								}
							?>
						value="<?php echo $staff['staff_id']; ?>"><?php echo $staff['firstname'] . " " . $staff['lastname']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('staff_id[]'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
		<?php if(!($role_type_id == 3 || $role_type_id == 4 ||$role_type_id == 5 || $role_type_id == 9)): ?>
			<input type="submit" class="form-control btn btn-primary" name="add_or_remove_staff_to_role" value="Update" />
		<?php endif; ?>
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>
