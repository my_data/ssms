
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

<div class="input-group">
<!-- <?php echo form_open('students/edit_results/'.$class_stream_id.'/'.$subject_id); ?>
<input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" /> -->
<div class="input-group-btn">
    <!-- <input type="hidden" id="search_type" value="0">
        <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
<?php echo form_close(); ?> -->
       </div>
</div>
    <!-- <a href="<?php echo base_url() . 'students/print_students_results/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-sm btn-primary">Print All Results</a> -->
</div>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td widht="60%">Subject Name</td>
			<td width="30%" align="center">Score</td>
			<td width="10%" align="center">Action</td>
		</tr>
	</thead>
		<tbody>
			<?php if ($result_records == FALSE): ?>
		        <tr>
		          <td colspan="4">
		                    <?php
		                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "No Results Found";
		                        echo $message;
		                    ?>
		                </td>
		        </tr>
		    <?php else: ?>
			<?php foreach($result_records as $row): ?>
			<tr>
				<td><?php echo $row['subject_name']; ?></td>
				<?php echo form_open('admin/edit_student_results/'.$row['admission_no'].'/'.$etype_id.'/'.$term_id); ?>
					<input type="hidden" name="ssa_id" value="<?php echo $row['ssa_id']; ?>" />
					<input type="hidden" name="subject_id" value="<?php echo $row['subject_id']; ?>" />
					<input type="hidden" name="class_stream_id" value="<?php echo $row['class_stream_id']; ?>" />
					<td align="center">
						<input type="number" name="score" step=".01" value="<?php echo $row['marks']; ?>" />
					</td>
					<td align="center">
						<input type="submit" name="edit_score" title="Update <?php echo $row['subject_name']; ?>" data-placement="bottom" value="UPDATE" class="btn btn-primary btn-xs" onclick="return confirm('Are u sure you want to update this marks?');" />
					</td>
				<?php echo form_close(); ?>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
		</tbody>
</table>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
