
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">

					<?php echo form_open('school/matokeo'); ?>
	
		<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
         </div>
									<?php echo form_close(); ?>
					    
				</div>
			</div>
	</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Year</td>
			<td>Level</td>
			<td>Term</td>
			<td>Monthly</td>
			<td>Midterms</td>
			<td>Monthly</td>
			<td>Terminal / Annual</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($records == FALSE): ?>
    	<tr>
	    	<td colspan="3">
	    		<?php
                    $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Results";
                    echo $message;
                ?>
	    	</td>
	    </tr>
	<?php else: ?>
		<?php
			$x = "";
			foreach ($records as $key => $row):
		?>
			<tr>
				<td><?php if($row['year'] !== $x){ echo "<a href='".base_url(). 'classes/student_results/'.$row['year']. "'>" . $row['year'] . "</a>"; } ?></td>
				<td><?php if($row['year'] !== $x){ echo $row['class_level']; } ?></td>
				<td><?php echo $row['term_name']; ?></td>
				<?php foreach ($exam_types as $key => $etype): ?>
					<td align="center"><a href="<?php echo base_url() . 'classes/continous_results/'.$row['year'].'/'.$row['term_id'].'/'.$etype['id']; ?>"><?php echo $etype['id']; ?></a></td>
				<?php endforeach; ?>
			</tr>
		<?php
			$x = $row['year'];
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
</div>
</div>
</div>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/roles'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>