
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form',  'id' => 'selectForm'); ?>
	<?php echo form_open('admin/manage_role/'.$role_type_id, $attributes); ?>
	<input type="checkbox" id="selectAllCheckBox" onclick="selectAll(selectForm)"> <strong>SELECT ALL</strong>
		<div class="form-group">
			<!-- <label class="col-sm-2 control-label"  for="role_name"><?php //echo $pr['role_name']; ?> :</label> -->
			<div class="col-sm-4">
				<?php foreach($common_permissions as $cp): ?>
					<input id="<?php echo $cp['permission_id']; ?>" type="checkbox" name="permission[]" value="<?php echo $cp['permission_id']; ?>" <?php foreach ($permission_records as $key => $pr) {
							if($pr['permission_id'] === $cp['permission_id']){
								echo "checked";
							}
						} ?>><?php echo $cp['description'] . "<br/>"; ?>
				<?php endforeach; ?>
			</div>
		</div>		
		<div class="form-group">
			<!-- <label class="col-sm-2 control-label"  for="role_name"><?php //echo $pr['role_name']; ?> :</label> -->
			<div class="col-sm-4">
				<?php foreach($other_permissions as $op): ?>
					<input type="checkbox" name="permission[]" value="<?php echo $op['permission_id']; ?>" <?php foreach ($permission_records as $key => $pr) {
							if($pr['permission_id'] === $op['permission_id']){
								echo "checked";
							}
						} ?>>
						<?php
							$x = 0; $z = 0;
							foreach ($modules as $key => $md) {
								if($md['module_id'] === $op['module_id']){
									if($x === 1 && $z === 0) {
										echo '<strong>' . $md['module_name'] . '</strong>';	
										$x = 1;
									}
								}
							}

						?>
						<?php echo $op['description'] . "<br/>"; ?>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="form-group">
			<!-- <label class="col-sm-2 control-label"  for="role_name"><?php //echo $pr['role_name']; ?> :</label> -->
			<div class="col-sm-4">
				<?php if($role_type_id == 4): ?>
					<?php foreach($class_teacher_permission as $ctp): ?>
						<input type="checkbox" name="permission[]" value="<?php echo $ctp['permission_id']; ?>" <?php foreach ($permission_records as $key => $pr) {
								if($pr['permission_id'] === $ctp['permission_id']){
									echo "checked";
								}
							} ?>><?php echo $ctp['description'] . "<br/>"; ?>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if($role_type_id == 5): ?>
					<?php foreach($dorm_master_permission as $dmp): ?>
						<input type="checkbox" name="permission[]" value="<?php echo $dmp['permission_id']; ?>" <?php foreach ($permission_records as $key => $pr) {
								if($pr['permission_id'] === $dmp['permission_id']){
									echo "checked";
								}
							} ?>><?php echo $dmp['description'] . "<br/>"; ?>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if($role_type_id == 3): ?>
					<?php foreach($hod_permission as $hp): ?>
						<input type="checkbox" name="permission[]" value="<?php echo $hp['permission_id']; ?>" <?php foreach ($permission_records as $key => $pr) {
								if($pr['permission_id'] === $hp['permission_id']){
									echo "checked";
								}
							} ?>><?php echo $hp['description'] . "<br/>"; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>

		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="edit_new_role" value="Save Changes" onclick="return confirm('Save Changes?');" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>