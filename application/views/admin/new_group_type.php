
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_group_type()'); ?>
	<?php echo form_open('admin/add_new_group', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="group_name">Group Name :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('group_name'); ?>" type="text" name="group_name" class="form-control" id="group_name" required />
				<div class="error_message_color">
					<?php echo form_error('group_name'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="description">Description :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('description'); ?>" type="text" name="description" class="form-control" id="description" required />
				<div class="error_message_color">
					<?php echo form_error('description'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_new_group" value="Add Record" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>