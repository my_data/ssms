
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">

					<?php echo form_open('admin/roles'); ?>

		<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_role" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
         </div>
									<?php echo form_close(); ?>

				</div>
			

		<a href="<?php echo base_url() . 'admin/add_new_role'; ?>" class="btn btn-primary btn-sm">Add Role</a>

	</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Role Name</td>
			<td>Description</td>
			<td align="center">Action</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($role_types == FALSE): ?>
    	<tr>
	    	<td colspan="4">
	    		<?php
                    $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Roles";
                    echo $message;
                ?>
	    	</td>
	    </tr>
	<?php else: ?>
		<?php
			foreach ($role_types as $key => $role):
		?>
			<tr>
				<td><?php echo $role['role_name']; ?></td>
				<td><?php echo $role['description']; ?></td>
				<td align="center">
					<a href="<?php echo base_url(); ?>admin/add_user_to_role/<?php echo $role['role_type_id']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Add user to <?php echo $role['role_name'] . " Role"; ?>"><span class="fa fa-plus"></span></a>&nbsp;
					<a href="<?php echo base_url(); ?>admin/manage_role/<?php echo $role['role_type_id']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Manage <?php echo $role['role_name'] . " Role"; ?>"><span class="fa fa-cog"></span></a>&nbsp;
					<a href="<?php echo base_url(); ?>admin/edit_role/<?php echo $role['role_type_id']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Rename Role"><span class="fa fa-pencil"></span></a>&nbsp;
					<a href="<?php echo base_url(); ?>admin/remove_role/<?php echo $role['role_type_id']; ?>" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remove Role" onClick="return confirm('Are you sure you want to remove this role?');"><span class="glyphicon glyphicon-trash"></span>
				</td>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/roles'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>