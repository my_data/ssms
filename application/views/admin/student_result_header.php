
<div class="table-responsive">  
<table class="table table-striped table-hover table-condensed table-bordered">
    <tbody>
      <?php if($class_level_array['level'] === "O'Level"): ?>
        <tr class="table-header">
          <td colspan="2">Form One</td>
          <td colspan="2">Form Two</td>
          <td colspan="2">Form Three</td>
          <td colspan="2">Form Four</td>
        </tr>
      <?php endif; ?>
      <?php if($class_level_array['level'] === "A'Level"): ?>
        <tr class="table-header">
          <td colspan="2">Form Five</td>
          <td colspan="2">Form Six</td>
        </tr>
      <?php endif; ?>
        <tr>
          <?php foreach($records as $rec): ?>
            <?php echo form_open('admin/progressive_results/'.$rec['admission_no']); ?>
            <!-- <td><a href="<?php echo base_url() . 'students/'; ?>"><?php echo $rec['term_name']; ?></a></td> -->
            <input type="hidden" name="term_id" value="<?php echo $rec['term_id']; ?>" />
            <td><input type="submit" name="get_results" value="<?php echo $rec['term_name']; ?>" class="btn btn-xs btn-primary"></td>
            <?php echo form_close(); ?>
          <?php endforeach; ?>
        </tr>
    </tbody>
  </table>
</div>  