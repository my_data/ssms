
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline"> 

	        <div class="form-group has-feedback no-margin">
				<div class="input-group">
						<?php if($flag === "NOT_SET"): ?>
							<?php echo form_open('admin/activity_log_records'); ?>
								<input type="text" class="form-control input-sm" name="search_log" placeholder="Search ..." id="form-search-input" />
						<?php endif; ?>
						<?php if($flag === "IS_SET"): ?>
							<?php echo form_open('admin/log_records'); ?>
								<input type="text" class="form-control input-sm" name="user_log_record" placeholder="Search ..." id="form-search-input" />
							<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
						<?php endif; ?>
						<div class="input-group-btn">
						    <input type="hidden" id="search_type" value="0">
						        <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
									<?php echo form_close(); ?>
							        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
						          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
						          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
						          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
						        </ul> -->
					    </div><!-- /btn-group -->
				</div>
			</div>

		<!-- <a href="<?php echo base_url() . 'admin/add_department'; ?>" class="btn btn-primary btn-sm">Add Department</a> -->

	</div>
</div>
<div class="table table-responsive">
		<table class="table table-striped table-hover table-condensed table-bordered">
			<thead>
				<tr class="table-header">
					<td>Activity</td>
					<td>Table Name</td>
					<td>Time</td>
					<td>Source</td>
					<td>Destination</td>
					<td>User</td>
				</tr>
			</thead>
			<tbody>
			<?php if ($activity_log_records == FALSE): ?>
		    	<tr>
			    	<td colspan="4">There are currently No Match for your search</td>
			    </tr>
			<?php else: ?>
				<?php
					foreach ($activity_log_records as $key => $record):
				?>
					<tr>
						<td><?php echo $record['activity']; ?></td>
						<td><?php echo $record['tableName']; ?></td>
						<td><?php echo $record['time']; ?></td>
						<td><?php echo $record['source']; ?></td>
						<td><?php echo $record['destination']; ?></td>
						<td><?php echo $record['username']; ?></td>	
					</tr>
				<?php
					endforeach;
				?>
				<?php endif; ?>
			</tbody>
		</table>
		<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
        </div>
		</div>
			<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/activity_log_records'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>