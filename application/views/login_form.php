<!DOCTYPE html>
<html lang="en">
  <head><link rel='icon' href="<?php echo base_url() . 'assets/images/129.jpg'; ?>" width='50%' />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Datatables css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/datatables/dataTables.min.css">
    <!-- Bootstrap Multiselect CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/bootstrap-multiselect.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
   
    h1{ 
          text-align: center;
          color: #306091;
        text-shadow: 3px 5px 2px rgba(150, 150, 150, 0.50);
        }

    .container{
    padding-top: 30px;
    }

    </style>


  </head>
  <body>
    <div class="container" align="center">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="col-md-12">

                            <div class="col-md-12" align="center">
                                <img src="<?php echo base_url();?>/assets/images/129.jpg" class="img-responsive" alt="...">
                                <h1><!-- Bwiru Boys Technical  -->Secondary School Management System </h1>
                                <h1>(BBSMS).</h1>
                            </div>

                        </div>
                        <br/><br/><br><br/><br/><br/><br/><br/><br/><br/><br/>           
                    <div class="col-md-6 col-md-offset-3 panel panel-default">
                        <div class="panel-body">
                            <div class="form-login" style="padding-top:10px;">
                                <?php $attributes = array('class' =>'form-horizontal', 'role' => 'form'); ?>
                                <?php echo form_open(''.base_url().'login', $attributes); ?>
                                   
                                    <div class="form-group">
                                        <div  class="col-sm-3">
                                            <label for="firstname" class="control-label">Username : </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="username" placeholder="+255756001100" name="username">
                                            <span class="text-danger"><?php echo form_error('username'); ?></span>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <div  class="col-sm-3">
                                            <label for="lastname" class="control-label">Password : </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" id="password" placeholder="Enter your Password" name="password">
                                            <span class="text-danger"><?php echo form_error('password'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div  class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <button type="submit" class="btn btn-primary form-control">Sign in</button>
                                            <div class="flash" style="color:red;"><?php echo $this->session->flashdata('error_message'); ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <div class="checkbox">
                                                <label><input type="checkbox"> <b>Remember me</b></label>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



</body>
</html>