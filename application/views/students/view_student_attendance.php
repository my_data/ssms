
        <div  class="table-responsive" id="exhaustive_table">
         <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr class="table-header">
                <td style="text-align:center;">Academic Year</td>
                <td style="text-align:center;">Term</td>
                <td style="text-align:center;">Present</td>
                <td style="text-align:center;">Absent</td>
                <td style="text-align:center;">Sick</td>
                <td style="text-align:center;">Permitted</td>
                <td style="text-align:center;">Home</td>
                <td style="text-align:center;">Average</td>
              </tr>
            </thead>
            <tbody>
            <?php if ($attendance_records == FALSE): ?>
                <tr>
                  <td colspan="8">
                            <?php
                                $message = "There are currently No Attendance Records for this particular student";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
              <?php $x = 0; foreach($attendance_records as $student_attendance_record): ?>
                <tr>
                  <td>&nbsp;<?php 
                              if($x%2 == 0){
                                echo $student_attendance_record['year'];
                              }
                              $x++;
                            ?>
                  </td>
                  <td>&nbsp;<?php echo $student_attendance_record['term_name']; ?></td>
                  <td>&nbsp;
                    <?php
                      if($student_attendance_record['Present'] !== NULL){
                        echo $student_attendance_record['Present'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td>&nbsp;
                    <?php
                      if($student_attendance_record['Absent'] !== NULL){
                        echo $student_attendance_record['Absent'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td>&nbsp;
                    <?php
                      if($student_attendance_record['Sick'] !== NULL){
                        echo $student_attendance_record['Sick'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td>&nbsp;
                    <?php
                      if($student_attendance_record['Permitted'] !== NULL){
                        echo $student_attendance_record['Permitted'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td>&nbsp;
                    <?php
                      if($student_attendance_record['Home'] !== NULL){
                        echo $student_attendance_record['Home'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td>
                    <?php 
                      $total_days = $student_attendance_record['Present'] + $student_attendance_record['Absent'] + $student_attendance_record['Sick'] + $student_attendance_record['Permitted'] +$student_attendance_record['Home'];
                      if($student_attendance_record['Present'] !== NULL){
                        echo $student_attendance_record['Present'];
                      }
                      else{
                        echo 0;
                      }
                      echo '/'.$total_days; 
                    ?>
                  </td>
              <?php endforeach; ?>
            <?php endif;?>
            </tbody>
        </table>
          
      </div>
    </div>