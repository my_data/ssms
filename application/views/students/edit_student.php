
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<?php //echo validation_errors(); ?>
<?php echo form_open('students/edit/'.$student_record['admission_no']); ?>
<center><h4 class="breadcrumb">Student Personal Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_no">Admission No :</label>
		<div class="col-sm-10">
			<input type="hidden" name="current_admission_no" value="<?php echo $student_record['admission_no']; ?>" />
			<input type="hidden" name="admission_date" value="<?php echo $student_record['date']; ?>" />
			<input type="hidden" name="class_id" value="<?php echo $student_record['class_id']; ?>" readonly="true" />
			<input type="text" name="admission_no" class="form-control" value="<?php echo $student_record['admission_no']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('admission_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
		<div class="col-sm-10">
			<input type="text" name="firstname" class="form-control" value="<?php echo $student_record['student_firstname']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('firstname'); ?>
			</div>
		</div>
	</div>




	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="lastname">Other Name :</label>
		<div class="col-sm-10">
			<input type="text" name="lastname" class="form-control" value="<?php echo $student_record['student_lastname']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('lastname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="dob">Date Of Birth :</label>
		<div class="col-sm-6">
			<input type="date" name="dob" class="form-control" value="<?php echo $student_record['dob']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('dob'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="disability">Disabled :</label>
		<?php if($student_record['disabled'] === "yes"): ?>
			<div class="col-sm-2">
				<input id="disabled" onClick="disabledSelectElement('disabled', 'edit_disability_values')" <?php if($student_record['disabled'] === "yes"){ echo "checked"; } ?> type="radio" name="disabled" value="disabled" /> Yes
				<input id="hideDisability" onClick="disabledSelectElement('hideDisability', 'edit_disability_values')" <?php if($student_record['disabled'] === "no"){ echo "checked"; } ?>  type="radio" name="disabled" value="No" /> No
				<div class="error_message_color">
					<?php echo form_error('disabled'); ?>
				</div>
			</div>
		<?php else: ?>
			<div class="col-sm-2">
				<input id="disabled" onClick="disabledSelectElement('disabled', 'disability_values')" <?php if($student_record['disabled'] === "yes"){ echo "checked"; } ?> type="radio" name="disabled" value="disabled" /> Yes
				<input id="hideDisability" onClick="disabledSelectElement('hideDisability', 'disability_values')" <?php if($student_record['disabled'] === "no"){ echo "checked"; } ?>  type="radio" name="disabled" value="No" /> No
				<div class="error_message_color">
					<?php echo form_error('disabled'); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-sm-6" <?php if($student_record['disabled'] === "yes"){ echo "id='edit_disability_values'"; }else{ echo "id='disability_values'"; } ?>>
			<select name="disability" class="form-control" >
				<option value="">--Choose--</option>
				<option <?php if($student_record['disability'] == "Deaf"){ echo "selected='true'"; } ?> value="Deaf">Deaf</option>
				<option <?php if($student_record['disability'] == "Blind"){ echo "selected='true'"; } ?> value="Blind">Blind</option>
				<option <?php if($student_record['disability'] == "Mute"){ echo "selected='true'"; } ?> value="Mute">Mute</option>
				<option <?php if($student_record['disability'] == "Physical Disability"){ echo "selected='true'"; } ?> value="Physical Disability">Physical Disability</option>		
			</select>
			<div class="error_message_color">
				<?php echo form_error('disability'); ?>
			</div>
		</div>
		<?php //endif; ?>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="tribe_id">Tribe :</label>
		<div class="col-sm-10">
			<select name="tribe_id" class="form-control">
				<option value="">--Choose--</option>
				<?php foreach($tribes as $tribe): ?>
					<option <?php if($tribe['tribe_id'] == $student_record['tribe_id']){ echo "selected='true'"; } ?> value="<?php echo $tribe['tribe_id']; ?>"><?php echo $tribe['tribe_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('tribe_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="religion_id">Religion :</label>
		<div class="col-sm-10">
			<select name="religion_id" class="form-control">
				<option value="">--Choose--</option>
				<?php foreach($religions as $religion): ?>
					<option <?php if($religion['religion_id'] == $student_record['religion_id']){ echo "selected='true'"; } ?> value="<?php echo $religion['religion_id']; ?>"><?php echo $religion['religion_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('religion_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
		<center><h4 class="breadcrumb">Student Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="box">P.O.Box :</label>
		<div class="col-sm-4">
			<input type="text" name="box" class="form-control" value="<?php echo $student_record['home_address']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('box'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<label class="col-sm-2 control-label" for="region">Region :</label>
		<div class="col-sm-4 " >
			<select name="region" class="form-control" required>
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option <?php if($region['region_name'] == $student_record['region']){ echo "selected='true'"; } ?> value="<?php echo $region['region_name']; ?>"><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>		
			</select>
			<div class="error_message_color">
				<?php echo form_error('region'); ?>
			</div>
		</div>
	</div>
	<br/>
	<br/>
		<center><h4 class="breadcrumb">Other Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_date">Admission Date :</label>
		<div class="col-sm-4 " >
			<input type="date" class="form-control" name="admission_date" value="<?php echo date('Y-m-d'); ?>" readonly="true" />
			<div class="error_message_color">
				<?php echo form_error('admission_date'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<!-- <div class="form-group">
		<label class="col-sm-2 control-label" for="">Academic Year :</label>
		<div class="col-sm-4 " >
			<input type="text" class="form-control" value="<?php echo $year; ?>" readonly="true" />
		</div>
	</div>
	<br/><br/> -->
	<div class="form-group">
		<label class="col-sm-2 control-label" for="former_school">Former School :</label>
		<div class="col-sm-4 " >
			<input type="text" name="former_school" class="form-control" value="<?php echo $student_record['former_school']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('former_school'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<label class="col-sm-2 control-label" for="transferred">Transfered :</label>
		<div class="col-sm-4 " >
			<input <?php if($student_record['stte'] === "transferred_in"){ echo "checked='true'"; } ?> type="radio" name="transferred" value="transferred_in" /> Yes
			<input <?php if($student_record['stte'] === "selected"){ echo "checked='true'"; } ?> type="radio" name="transferred" value="selected" /> No
			<div class="error_message_color">
				<?php echo form_error('transferred'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
		<center><h4 class="breadcrumb">Parent Personal Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_firstname">Firstname:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="g_firstname" value="<?php echo $student_record['g_firstname']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_firstname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_lastname">Other Name :</label>
		<div class="col-sm-10">
			<input type="text" name="g_lastname" class="form-control" value="<?php echo $student_record['g_lastname']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_lastname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gender">Gender :</label>
		<div class="col-sm-10">
			<input type="radio" name="gender" value="Male" <?php if($student_record['gender'] == "Male"){ echo "checked='true'"; } ?> /> Male
			<input type="radio" name="gender" value="Female" <?php if($student_record['gender'] == "Female"){ echo "checked='true'"; } ?> /> Female
			<div class="error_message_color">
				<?php echo form_error('gender'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="occupation">Occupation :</label>
		<div class="col-sm-10">
			<input type="text" name="occupation" class="form-control" value="<?php echo $student_record['occupation']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('occupation'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="rel_type">Relationship Type:</label>
		<div class="col-sm-10">
			<select name="rel_type" class="form-control">
				<option value="">--Choose--</option>
				<option <?php if($student_record['rel_type'] == "Father"){ echo "selected='true'"; } ?> value="Father">Father</option>
				<option <?php if($student_record['rel_type'] == "Mother"){ echo "selected='true'"; } ?> value="Mother">Mother</option>
				<option <?php if($student_record['rel_type'] == "Guardian"){ echo "selected='true'"; } ?> value="Guardian">Guardian</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('rel_type'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
		<center><h4 class="breadcrumb">Parent Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="email">Email :</label>
		<div class="col-sm-10">
			<input type="email" name="email" class="form-control" value="<?php echo $student_record['email']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('email'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="phone_no">Phone No. :</label>
		<div class="col-sm-10">
			<input type="text" name="phone_no" class="form-control" value="<?php echo $student_record['phone_no']; ?>" />
			<div class="error_message_color">
				<?php echo form_error('phone_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="p_address">Permanent Address :</label>
		<div class="col-sm-10">
			<input type="text" name="p_address" class="form-control" value="<?php echo $student_record['p_box']; ?>" /> 
			<div class="error_message_color">
				<?php echo form_error('p_address'); ?>
			</div>
		</div>
	</div>	
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="region">Region:</label>
		<div class="col-sm-10">
			<select name="p_region" class="form-control">
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option <?php if($region['region_name'] == $student_record['p_region']){ echo "selected='true'"; } ?> value="<?php echo $region['region_name']; ?>"><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('p_region'); ?>
			</div>
		</div>
	</div>
	<br/><br/><br/>
<div class="form-group">	
	<input type="submit" name="submit" value="Update" class="btn btn-primary form-control" />
</div>
<br/>
	

</form>
