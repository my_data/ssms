
<div class="white-area-content">
<div class="db-header clearfix">

<?php if(!empty($check_enabled)): ?>

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

       
<?php echo form_open('students/edit_results/'.$class_stream_id.'/'.$subject_id); ?>

      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>
 
    <!-- <a href="<?php echo base_url() . 'students/print_students_results/'.$class_stream_id.'/'.$term_id; ?>" class="btn btn-sm btn-primary">Print All Results</a> -->
</div>

</div>



<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td widht="60%">Names</td>
			<td width="30%" align="center">Score</td>
			<td width="10%" align="center">Action</td>
		</tr>
	</thead>
		<tbody>
			<?php if ($mystudents == FALSE): ?>
		        <tr>
		          <td colspan="4">
		                    <?php
		                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Uploaded results to edit";
		                        echo $message;
		                    ?>
		                </td>
		        </tr>
		    <?php else: ?>
			<?php foreach($mystudents as $mystudent): ?>
			<tr>
				<td><?php echo $mystudent['names']; ?></td>
				<?php echo form_open('students/edit_results/'.$class_stream_id.'/'.$subject_id); ?>
					<input type="hidden" name="ssa_id" value="<?php echo $mystudent['ssa_id']; ?>" />
					<!-- THESE HIDDEN VALUES ARE FOR REDIRECTION PURPOSE -->
					<input type="hidden" name="class_stream_id" value="<?php echo $mystudent['class_stream_id']; ?>" />
					<input type="hidden" name="term_id" value="<?php echo $mystudent['term_id']; ?>" />
					<input type="hidden" name="subject_id" value="<?php echo $mystudent['subject_id']; ?>" />
					<input type="hidden" name="class_id" value="<?php echo $mystudent['class_id']; ?>" />
					<td align="center">
						<input type="number" name="score" step=".01" min="0" max="100" value="<?php echo $mystudent['marks']; ?>" />
					</td>
					<td align="center">
						<input type="submit" name="edit_score" value="UPDATE" class="btn btn-primary btn-xs" onclick="return confirm('Are u sure you want to update?');" />
					</td>
				<?php echo form_close(); ?>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
		</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

<?php else: ?>
	There are currently no exam types to add or edit, check with academic master for further info
<?php endif; ?>
