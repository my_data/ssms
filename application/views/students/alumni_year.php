
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">
				
					<?php echo form_open('students/alumni_year/'.$year); ?>

       <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
									<?php echo form_close(); ?>
							   
				</div>
			</div>
      <a href="<?php echo base_url() . 'students/alumni'; ?>" class="btn btn-primary btn-sm">View All</a>
	</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Admission#</td>
			<td>Names</td>
			<!-- <td>Lastname</td> -->
			<td>Level</td>
            <td>Completion Year</td>
			<td>Action</td>
			<!-- <td>Edit</td>
			<td>Tranfer</td>
			<td>Expell</td> -->
		</tr>
	</thead>
	<tbody>
  <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['student_names']; ?></td>
				<td><?php echo $student['class_level']; ?></td>
				<td><?php echo $student['year']; ?></td>
				<td><a href='<?php if($this->session->userdata('user_role') === "admin"): echo base_url() . 'admin/student_profile/' . $student['admission_no']; else: echo base_url() . 'students/get_student_profile/' . $student['admission_no']; endif; ?>' class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="View Profile"><span class="fa fa-eye"></span></a>&nbsp;				
					<a href='#' class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalMoreDetails<?php echo $student['admission_no']; ?>" data-toggle="tooltip" data-placement="bottom" title="Add More Details"><span class="fa fa-plus"></span></a>&nbsp;
                    <a href='#' class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalDetailsCompleted<?php echo $student['admission_no']; ?>" data-toggle="tooltip" data-placement="bottom" title="View Completion Details"><span class="fa fa-eye"></span></a>&nbsp;
                    <a href="#" class="btn btn-primary btn-xs <?php if($student['kashachukua_cheti'] === "hapana"){ echo 'glyphicon glyphicon-certificate'; }else{ echo 'glyphicon glyphicon-ok'; } ?>" data-toggle="modal" data-placement="right" data-toggle="tooltip" title="Kachukua cheti" data-target="#myModalCheti<?php echo $student['admission_no']; ?>"></a>
				</td>				
			</tr>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
				<?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/students'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?>
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>
</div>



<!-- MODAL FOR ADD MATOKEO -->

<?php foreach ($students as $student): ?>

	<div class="modal fade" id="myModalMoreDetails<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Extra Details On: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form')
            	?>            
                <?php echo form_open('students/add_completion_details', $attributes); ?>
                	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="index_no" >Index No: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="index_no" id="index_no" value="<?php if($student['index_no']){ echo $student['index_no']; } ?>" />
                    	</div>
                  	</div>
                  	<div class="form-group">
                    	<label  class="col-sm-4 control-label" for="division">Division: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="division" id="division" value="<?php if($student['division']){ echo $student['division']; } ?>" />
                    	</div>
                 	</div>
                  	<div class="form-group">
                    	<label  class="col-sm-4 control-label" for="points">Points: </label>
                    	<div class="col-sm-8">
                            <input type="text" class="form-control" name="points" id="points" value="<?php if($student['points']){ echo $student['points']; } ?>" />
                        </div>
                 	</div>
                    <!-- <div class="form-group">
                        <label  class="col-sm-4 control-label" for="date">Date: </label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" name="date" id="date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                    </div> -->
                  	<!-- <div class="form-group">
                    	<label class="col-sm-4 control-label" for="self_form" >Self form receipt</label>
                    	<div class="col-sm-8">
                        	<select name="self_form_receipt">
                        		<option value="">--Choose--</option>
                        		<option value="Received">Received</option>
                        		<option value="NOT Received">NOT Received</option>
                        	</select>
                    	</div>
                  	</div>
                  	<div class="form-group">
	                    <label  class="col-sm-4 control-label" for="School">Form: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="form" id="form" value="<?php echo $student['class_id']; ?>" />
                    	</div>
	                </div>
	                <div class="form-group">
	                    <label  class="col-sm-4 control-label" name="admission_no" for="admission_no">Admission_no: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="admission_no" id="admission_no" value="<?php echo $student['admission_no']; ?>" />
                    	</div>
	                </div>

	                <input type="hidden" class="form-control" name="transfer_status" id="transfer_status" value="OUT" /> -->
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden"  name="admission_no" value="<?php echo $student['admission_no']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Details</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR MATOKEO -->

<!-- START MODAL FOR KACHUKUA CHETI -->


<?php foreach ($students as $student): ?>

	<div class="modal fade" id="myModalCheti<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tarehe ya kuchukua cheti: </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form')
            	?>            
                <?php echo form_open('students/update_kachukua_cheti/' . $student['admission_no'], $attributes); ?>
                    <div class="form-group">
	                    <label  class="col-sm-4 control-label" for="date">Date: </label>
                    	<div class="col-sm-8">
                        	<input type="date" class="form-control" name="date" required="true" />
                    	</div>
                  	</div>    
                    <div class="form-group">
                      <label  class="col-sm-4 control-label" for="slip_no">Slip No: </label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control" name="slip_no" required="true" />
                      </div>
                    </div>            
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                 <input type="hidden" name="kashachukua_cheti" value="<?php echo $student['kashachukua_cheti']; ?>" />
                 <input type="hidden" name="year" value="<?php echo $student['year']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR KACHUKUA CHETI -->




<!-- MODAL FOR  MORE DETAILS COMPLETED STUDENT -->

<?php foreach ($students as $student): ?>

  <div class="modal fade" id="myModalDetailsCompleted<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Names: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">                      
                    <table class="table table-condensed table-hover table-striped table-bordered">
                      <tr class="table-header">
                        <td>Title: </td>
                        <td>Description</td>
                      </tr>
                      <tr>
                        <td>Index No: </td>
                        <td><?php echo $student['index_no']; ?></td>
                      </tr>
                      <tr>
                        <td>Division: </td>
                        <td><?php echo $student['division']; ?></td>
                      </tr>
                      <tr>
                        <td>Points: </td>
                        <td><?php echo $student['points']; ?></td>
                      </tr>
                      <tr>
                        <td>Completed In: </td>
                        <td><?php echo $student['year']; ?></td>
                      </tr>
                      <tr>
                        <td>Certificate: </td>
                        <td><?php echo $student['kashachukua_cheti']; ?></td>
                      </tr>
                      <tr>
                        <td>Slip No: </td>
                        <td><?php echo $student['slip_no']; ?></td>
                      </tr>
                      <tr>
                        <td>Date: </td>
                        <td><?php echo $student['tarehe_ya_kuchukua']; ?></td>
                      </tr>
                      <!-- <tr>
                        <td>Orphan: </td>
                        <td><?php echo $student['orphan']; ?></td>
                      </tr>
                      <tr>
                        <td>Guardian Firstname: </td>
                        <td><?php echo $student['g_firstname']; ?></td>
                      </tr>
                      <tr>
                        <td>Guardian Lastname: </td>
                        <td><?php echo $student['g_lastname']; ?></td>
                      </tr>
                      <tr>
                        <td>Disable: </td>
                        <td><?php echo $student['disability']; ?></td>
                      </tr>
                      <tr>
                        <td>Region: </td>
                        <td><?php echo $student['region']; ?></td>
                      </tr>
                      <tr>
                        <td>District: </td>
                        <td><?php echo $student['district']; ?></td>
                      </tr>
                      <tr>
                        <td>Telephone#: </td>
                        <td><?php echo $student['tel_no']; ?></td>
                      </tr>
                      <tr>
                        <td>P.O Box: </td>
                        <td><?php echo $student['box']; ?></td>
                      </tr>
                      <tr>
                        <td>Email: </td>
                        <td><?php echo $student['email']; ?></td>
                      </tr>
                      <tr>
                        <td>Form: </td>
                        <td><?php echo $student['form']; ?></td>
                      </tr> -->
                    </table>
            </div>
          </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR MORE DETAILS COMPLETED STUDENTS -->