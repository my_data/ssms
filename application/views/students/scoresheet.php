
<div class="white-area-content">
<div class="db-header clearfix">
<table style="width:100%">
	<tr>
		<td><h3><strong><?php echo $score_title; ?></strong></h3> </td>
		<td align="right">			
				<?php
					echo $exam_date_and_type;
				?>			
		</td>	
	</tr>
</table>
 
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('students/scoresheet/'.$class_stream_id.'/'.$subject_id); ?>

<?php if(!empty($check_enabled)): ?>
<div class="form-group">
	<div class="col-xs-1">	
		<label>Date: </label> 
	</div>	
	<div class="col-xs-3">
		<input type="date" class="form-control" name="edate" id="edate" value="<?php echo set_value('edate'); ?>" required/>
		<input type="hidden" name="etype" value="<?php echo $examtype_id; ?>">
		<input type="hidden" name="term_id" value="<?php echo $term_id; ?>">
	</div>
</div>
<br/><br/><br/>

<div class="table-responsive">
	<table class="table table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr class="table-header">
				<td width="70%" >Names</td>
				<td width="30%" align="center">Score</td>
			</tr>
		</thead>
			<tbody>
			<!-- <div id="successMessage" class="success_message_color">
				<?php 
					echo $this->session->flashdata('success_message');
				?>
			</div>
			<div class="error_message_color">
				<?php 
					echo $this->session->flashdata('error_message');
				?>
			</div> -->
			<div class="error_message_color">
				<?php echo validation_errors();	?>
			</div>
			<?php if ($students == FALSE): ?>
		        <tr>
		          <td colspan="4">
		                    <?php
		                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students Taking this Subject";
		                        echo $message;
		                    ?>
		                </td>
		        </tr>
		    <?php else: ?>
				<?php $x = 1; ?>
				<?php foreach($students as $student): ?>
				<tr>
					<td>
						<?php echo $student['names']; ?>
						<input type="hidden" name="admission_no[]" value="<?php echo $student['admission_no']; ?>"/>
						<input type="hidden" name="class_stream_id" value="<?php echo $student['class_stream_id']; ?>"/>
						<input type="hidden" name="subject_id" value="<?php echo $student['subject_id']; ?>"/>
						<input type="hidden" name="class_id" value="<?php echo $student['class_id']; ?>" />
					</td>
					<td align="center">
						<input type="number" step=".01" name="score[]" min="0" max="100" id="score<?php echo $x+$p; ?>" value="<?php echo set_value('score[]'); ?>" /><!-- &nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="checkAbs" > -->
					</td>
				</tr>
				<?php $x++; ?>
			<?php endforeach; ?>
		<?php endif; ?>
			</tbody>
	</table>
</div>
	<br/>
	<div align="center">
		<input type="submit" value="Save score" class="btn btn-primary btn-md form-control" >
	</div>
	<br/><br/>
</form>
<?php else: ?>
	There are currently no exam types to add, check with academic master for further info
<?php endif; ?>

<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
