

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('students/get_students_by_class/'.$class_stream_id); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
<?php echo form_close(); ?>

              </div>
           
          <?php //if($this->session->userdata('second_master_role_active') === "on"): ?>
            <a href="<?php echo base_url() . 'students/add_student/'.$class_stream_id; ?>" class="btn btn-primary btn-sm">Add Student</a>
          <?php //endif; ?>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!--IMPORTANT-->
<!-- <div class="mkondo_student_class" data-target="<?php echo $class_stream_id; ?>">
</div> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Admission#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Class</td>
			<td>Stream</td>
			<td>Action</td>
			<!-- <td>View</td>
			<td>Edit</td> -->
		</tr>
	</thead>
	<tbody>
    <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="6">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this class";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; if($student['stte_out'] == "suspended"){ echo "<span style='color: red;' title='Currently Suspended' data-placement='bottom'><strong>***</strong></span>"; } ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>
				<td><a href='<?php echo base_url() . 'students/get_student_profile/' . $student['admission_no']; ?>' class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a>&nbsp;
				<a href='<?php echo base_url() . 'students/edit/' . $student['admission_no']; ?>' class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="Edit Student"><span class="fa fa-pencil"></span></a></td>
				
			</tr>
		<?php endforeach; ?>
    <?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>