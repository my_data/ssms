<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>SarahIkumu</title>
      <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" >
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/font-awesome.min.css">
      <!-- Datatables css-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/datatables/dataTables.min.css">
      <!-- Bootstrap Multiselect CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/bootstrap-multiselect.css">

      <!--BOOTSTRAP SWITCH-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-switch.min.css">
    <!--END BOOTSTRAP SWITCH-->

      <!-- CHOSEN CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/chosen/chosen.min.css">
      <!-- END CHOSEN CSS -->


      <link rel="stylesheet" href="<?php echo base_url() ?>scripts/fullcalendar.min.css" />
               <script src="<?php echo base_url() ?>scripts/lib/moment.min.js"></script>
               <script src="<?php echo base_url() ?>scripts/fullcalendar.min.js"></script>
               <script src="<?php echo base_url() ?>scripts/fullcalendar/gcal.js"></script>

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
      
      <style></style>

  </head>
<body>
<div class="container">
	<div class="row">
			<div class="col-md-9">
				<div id="printableAllArea">