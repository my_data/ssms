<div class="db-header-extra form-inline"> 

				<?php //if($this->session->userdata('user_role') === "admin"): ?>
					<?php //echo form_open('admin/class_results/'.$class_id.'/'.$term_id); ?>
				<?php //endif; ?>
				<?php //if($this->session->userdata('academic_role_active') === "on"): ?>
					<?php echo form_open('classes/continous_results/'.$year.'/'.$term_id.'/'.$etype_id.'/'.$class_id); ?>
				<?php //endif; ?>
						
	 <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
				<?php echo form_close(); ?>
				
			</div>
<div id="exhaustive_table" class="table table-responsive" style="font-size: 12px;">

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Student Names</td>
			<?php foreach($subjects as $subject): ?>
				<td><?php echo $subject['subject_name']; ?></td>
			<?php endforeach; ?>
			<td>Total</td>
		    <td>AVG</td>
		    <td>Grade</td>
		    <!-- <td>Pos</td> -->
		    <?php if($this->session->userdata('edit_student_results') == 'ok'): ?>
		    	<td>Edit</td>
		    <?php endif; ?>
		</tr>
	</thead>
	<tbody>
		<?php if ($continous_results == FALSE || $students == FALSE): ?>
	    	<tr>
		    	<td rowspan="<?php //echo $no_of_subjects; ?>">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are No Results for this term";
                        echo $message;
                    ?>
                </td>
		    </tr>
		<?php else: ?>
		<?php foreach($students as $student): ?>
			<tr>
				<td><?php echo $student['firstname'] . " " . $student['lastname']; ?></td>
				<?php foreach($subjects as $subject): ?>
					<?php $x = 0; ?>
						<?php foreach($continous_results as $key => $row): ?>
							<?php if($row['admission_no'] === $student['admission_no']): ?>
								<?php if($subject['subject_id'] === $row['subject_id']): ?>
									<td><?php echo $row['marks']; ?></td>
								<?php else: ?>
									<?php $x++; ?>
									<?php if($x == $student['number_of_subjects']): ?>
										<td></td>
									<?php endif; ?>
								<?php endif; ?>
								
							<?php endif; ?>
						<?php endforeach; ?>
				<?php endforeach; ?>
				<td>
					<?php $total = 0; $idadi_ya_masomo = 0; ?>
					<?php foreach($continous_results as $cr): ?>
						<?php if($cr['admission_no'] === $student['admission_no']): ?>
							<?php $total = $total + $cr['marks']; ?>
							<?php $idadi_ya_masomo++; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php echo $total; ?>
				</td>
		    	<td><?php echo round($total/$idadi_ya_masomo, 2); ?></td>
		    	<td>
		          <?php foreach($grade_system as $gs): ?>
		            <?php
		              if($gs['start_mark'] <= ($total/$idadi_ya_masomo) && ($total/$idadi_ya_masomo) <= $gs['end_mark']){
		                echo $gs['grade'];
		              }
		            ?>
		          <?php endforeach; ?>
		        </td>
		        <!-- <td></td> -->
		        <?php if($this->session->userdata('edit_student_results') == 'ok'): ?>
			        <td align="center">
			        	<a href="<?php echo base_url() . 'results/edit/'.$student['admission_no'].'/'.$etype_id.'/'.$term_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Results"><span class="fa fa-pencil"></span></a>
			        </td>
			    <?php endif; ?>
			</tr>
		<?php endforeach; ?>
	<?php endif; //End of testing if continous_results zipo ?>
	</tbody>
</table>

</div>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div><!--END <div id="exhaustive_table" class="table-responsive" style="font-size: 12px;"> -->

<?php echo form_close(); //Close the score/position form open ?>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="#" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>

