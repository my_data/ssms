
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
</div>

<!-- <a href="<?php echo base_url() . 'admin/add_new_group'; ?>" class="btn btn-primary btn-sm">Add New Role</a> -->

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php 
	$month = explode(':', date('Y:m:d'))[1];
?>
<?php echo form_open("admin/promote_student/"); ?>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="o_level">Ordinary Level :</label>
		<div class="col-sm-10">
			<!-- <div class="success_message_color" id="successMessage">
				<?php
					if($this->session->flashdata('success_message')){
						echo $this->session->flashdata('success_message');
					}
				?>
			</div>
			<div class="error_message_color" id="successMessage">
				<?php //echo $this->session->flashdata('error_message'); ?>
				<?php 
					if($this->session->flashdata('exist_message')){
						echo $this->session->flashdata('exist_message');
					}
				?>
			</div> -->
			<input type="radio" name="level" value="O'level" required <?php if(!($month === "01")){ echo "disabled"; }?>/>
			<!-- <input type="text" name="firstname" class="form-control" required/> -->
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="a_level">Advance Level :</label>
		<div class="col-sm-10">
			<input type="radio" name="level" value="A'level" required <?php if(!($month === "07")){ echo "disabled"; }?>/>
			<!-- <input type="text" name="lastname" class="form-control" required/> -->
		</div>
	</div>
<div align="center">
	<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('staff_id'); ?>">
	<?php if($month === "01" || $month === "07"): ?>
		<input type="submit" name="promote" value="Promote" class="btn btn-primary" onClick="return confirm('Are you sure you want to promote the students to the next class?');"/>
		&nbsp;&nbsp;&nbsp;
		<!-- <input type="submit" name="restore" value="Restore" class="btn btn-primary"/> -->
		<!-- <input type="submit" name="demote" value="Repeat Class" class="btn btn-primary"/> -->
	<?php else: ?>
		<input type="submit" name="promote" value="Promote" class="btn btn-primary" onClick="return confirm('Are you sure you want to promote the students to the next class?');"  disabled />
		&nbsp;&nbsp;&nbsp;
		<input type="submit" name="restore" value="Restore" class="btn btn-primary"  disabled />
	<?php endif; ?>
	<!-- <input type="submit" name="demote" value="Repeat Class" class="btn btn-primary"/> -->
</div>
<?php echo form_close(); ?>
</div>