
<div class="table-responsive">  
<table class="table table-striped table-hover table-condensed table-bordered">
    <tbody>
        <tr class="table-header">
          <?php foreach($class_names as $cn): ?>
            <td colspan="2"><?php echo $cn['class_name']; ?></td>
          <?php endforeach; ?>          
        </tr>
        <tr>
          <?php foreach($records as $rec): ?>
            <?php echo form_open('students/progressive_results/'.$rec['admission_no']); ?>
            <!-- <td><a href="<?php echo base_url() . 'students/'; ?>"><?php echo $rec['term_name']; ?></a></td> -->
            <input type="hidden" name="term_id" value="<?php echo $rec['term_id']; ?>" />
            <input type="hidden" name="class_id" value="<?php echo $rec['class_id']; ?>" />
            <?php $term_name = ($rec['term_name'] == "first_term") ? "First T" : "Second T"; ?>
            <td><input type="submit" name="get_results" value="<?php echo $term_name; ?>" title="<?php echo $rec['year'] . ", " . $rec['term_name'] . " results"; ?>" class="btn btn-xs btn-primary"></td>
            <?php echo form_close(); ?>
          <?php endforeach; ?>
        </tr>
    </tbody>
  </table>
</div>  

      