<div class="white-area-content">
<div class="db-header clearfix">
  
<div class="page-header-title text-left" >
<div class="col-md-3 col-xs-6" id="img2">
	    <img src="<?php echo $image['image'];?>" class="img-responsive " alt="profile picture" style=" width:55%;" />
	    	<h5 class="text-danger">
	            <?php if($this->session->flashdata('error_upload')): ?>
	              <?php echo $this->session->flashdata('error_upload'); ?>
	            <?php endif; ?>
          </h5>
	    <?php echo form_open_multipart('students/upload_image/'.$adm_no);?>
		  	<div class="form-group">
		  	<input type="file" name="image_file" accept="image/gif, image/jpg, image/png" required /><br/>
		  	<input type="submit" class="btn btn-primary btn-xs col-xs-9" value="Save photo">
		  	</div>
	    <?php echo form_close();?>
	</div><?php echo $title; ?>
 
</div>
<div class="db-header-extra form-inline"> 

        
        <div class="text-left">
<!-- <div class="input-group-btn"> -->

	<a href="<?php echo base_url() . 'students/get_student_profile/'.$adm_no ?>" class="btn btn-primary btn-sm">Student</a>

	<a href="<?php echo base_url() . 'students/progressive_results/'.$adm_no ?>" class="btn btn-primary btn-sm">Results</a>

	<a href="<?php echo base_url() . 'students/discipline/'.$adm_no ?>" class="btn btn-primary btn-sm">Discipline</a>
	<a href="<?php echo base_url() . 'students/attendance/'.$adm_no ?>" class="btn btn-primary btn-sm">Attendance</a>
          
                    <!-- </div> -->
              </div>
    
          
          </div>
      </div>

<div class="row" style="padding-top:30px;">
    <div class="col-md-12">
	<div class="col-md-3 col-xs-6" id="img1">
	    <img src="<?php echo $image['image'];?>" class="img-responsive img-thumbnail " alt="profile picture" style="height:180px; width:150px;" />
	    	<h5 class="text-danger">
	            <?php if($this->session->flashdata('error_upload')): ?>
	              <?php echo $this->session->flashdata('error_upload'); ?>
	            <?php endif; ?>
          </h5>
	    <?php echo form_open_multipart('students/upload_image/'.$adm_no);?>
		  	<div class="form-group">
		  	<input type="file" name="image_file" accept="image/gif, image/jpg, image/png" required /><br/>
		  	<input type="submit" class="btn btn-primary col-xs-9" value="SAVE">
		  	</div>
	    <?php echo form_close();?>
	</div>
	<!-- <div class="success_message_color" id="successMessage">
      <?php
        if($this->session->flashdata('success_message')){
           echo '&nbsp&nbsp<i class="fa fa-check" aria-hidden="true"></i> &nbsp&nbsp' .$this->session->flashdata('success_message');
        }
      ?>
    </div> -->
										
