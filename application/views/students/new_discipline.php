
<div class="form-group">
	<?php if($this->session->flashdata('success_message')): ?> 
		<div class="alert alert-dismissible alert-success text algin-center">
			<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_validation')): ?> 
		<div class="alert alert-dismissible alert-warning text algin-center">
			<?php echo $this->session->flashdata('error_validation'); ?>
		</div>
	<?php endif;?>
	<?php if($this->session->flashdata('error_message')): ?> 
		<div class="alert alert-dismissible alert-danger text algin-center">
			<?php echo $this->session->flashdata('error_message'); ?>
		</div>
	<?php endif;?>
</div>


<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="glyphicon glyphicon-bed"></span>&nbsp;<?php echo $discipline_title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('students/load_new_discipline'); ?>
	Date: <input type="date" name="date" value="<?php echo set_value('date'); ?>" />
	<div class="error_message_color">
      <?php echo form_error('date'); ?>
    </div>


</div>
</div>


<!-- <?php echo form_open('students/load_new_discipline'); ?>
	Date: <input type="date" name="date" value="<?php echo set_value('date'); ?>" />
	<div class="error_message_color">
      <?php echo form_error('date'); ?>
    </div>
 -->	<table class="table table-bordered table-striped table-hover table-condensed">
		<tr>
			<th width="40%">Heading_Description</th>
			<th width="60%">Description</th>
		</tr>
		<tbody>
			<tr>
				<td>Student Names</td>
				<td>
					<select name="admission_no" class="form-control chosen-select" single>
						<?php foreach ($students as $student): ?>
							<option <?php echo set_select('admission_no', $student['names'], $this->input->post('admission_no')); ?> value='<?php echo $student['admission_no']; ?>'><?php echo $student['names']; ?></option>
						?>						
						<?php endforeach; ?>
					</select>
					<div class="error_message_color">
			          <?php echo form_error('admission_no'); ?>
			        </div>
				</td>
			</tr>
			<tr>
				<td>Description</td>
				<td>
					<textarea name="description" class="form-control" rows="10" cols="90" id="inci-area" >
						<?php echo set_value('description'); ?>
					</textarea>
					<div class="error_message_color">
			          <?php echo form_error('description'); ?>
			        </div>
				</td>
			</tr>
			<tr>
				<td>Decision</td>
				<td>
					<input value="<?php echo set_value('decision'); ?>" type="text" class="form-control" name="decision" style="width: 100%"; id="decision" />
					<div class="error_message_color">
			          <?php echo form_error('decision'); ?>
			        </div>
				</td>
			</tr>
		</tbody>
	</table>
	<div align="center">
		<input type="submit" name="add" value="Save Record" class="form-control btn btn-primary btn-sm" />
	</div>
</form>
