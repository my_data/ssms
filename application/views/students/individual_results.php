<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
  </head>
<body>
<div class="container">
	<div class="row">
			<div class="col-md-9">
				<div id="printableArea">
					<table width="100%">
						<tr>
							<td rowspan="7" width="10%"><img src="<?php echo base_url();?>/assets/images/129.jpg" class="img-responsive" alt=""/></td>
						</tr>
						<tr>
							<td align="center"><b>JAMUHURI YA MUUNGANO WA TANZANIA</b></td>
						</tr>
						<tr>
							<td align="center"><b>OFISI YA RAIS, TAWALA ZA MIKOA NA SERIKALI ZA MITAA (TAMISEMI)</b></td>
						</tr>
						<tr>
							<td align="center"><b>HALMASHAURI YA MANISPAA YA ILEMELA</b></td>
						</tr>
						<tr>
							<td align="center"><b>SHULE YA SEKONDARI YA UFUNDI BWIRU BOYS</b></td>
						</tr>
						<tr>
							<td align="center"><b>S.L.P 545 MWANZA</b></td>
						</tr>
						<tr>
							<td align="center"><b><u>E-mail :bwiruboys@bwiruboys.com</u></b></td>
						</tr>
						
					</table>
					
					<table width="100%">
						<tr>
							<td width="10%"></td>
							<td align="center"><b><u>TAARIFA YA MAENDELEO YA MWANAFUNZI</u></b></td>
						</tr>
					</table>
					<br/>
					<table width="100%">	
						<tr>
							<td width="40%"><b>JINA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $record['names']; ?></b></td>
							<td width="25%"><b>KIDATO:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $record['class_name']; ?></b></td>
							<td width="20%"><b>MUHULA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $academic_year['term_name']; ?></b></td>
							<td width="15%"><b>MWAKA:</b>&nbsp;&nbsp;&nbsp;<b><?php echo $academic_year['year']; ?></b></td>
						</tr>
					</table>

					<br/>
					<table width="100%" class="table-bordered">
						<tr >
							<th width="45%">SOMO</th>
							<th width="15%">MONTHLY (1)</th>
							<th width="15%">MID-TERM</th>
							<th width="15%">MONTHLY (2)</th>
							<th width="10%">TERMINAL</th>
							<th width="5%">AVERAGE</th>
							<th width="5%">GRADE</th>
							<th width="10%">POINT/ALAMA</th>
							<th width="10%"></th>
						</tr>
						 <?php foreach($mkeka_single as $mk): ?>
						      <tr>
						        <td><?php echo $mk['subject_name']; ?></td>
						        <td><?php echo $mk['monthly_one']; ?></td>
						        <td><?php echo $mk['midterm']; ?></td>
						        <td><?php echo $mk['monthly_two']; ?></td>
						        <td><?php echo $mk['terminal']; ?></td>
						        <td><?php echo $mk['average']; ?></td>
						        <td><?php echo $mk['grade']; ?></td>
						      </tr>
						    <?php endforeach; ?>
						<tr>
							<td><b>TOTAL POINT :</b></td>
							<td align="center"><b><?php echo $division_and_points['points']; ?></b></td>
							<td></td>
							<td></td>
							<td align="center"><b>DIVISION</b></td>
							<td align="center"><b><?php echo $division_and_points['division_name']; ?></b></td>
							<td>
								<table width="100%" class="table-bordered">
									<tr>
										<td width="60%" align="center"><b>WASTANI</b></td>
										<td width="40%" align="center"><?php echo $record['average']; ?><td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="7">Mwanao amekuwa wa <b><?php echo $record['rank']; ?></b> kati ya wanafunzi <b><?php echo $no_of_students; ?></b> Darasani Kwake</td>
						</tr>
					</table>


					<table width="100%" class="table-bordered">

						<tr>
							<th width="50%" colspan="2">UPIMAJI WA TABIA</th>
							<th width="10%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DARAJA</th>
							<th width="40%" colspan="2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GRADES</th>
						</tr>
						<?php
							foreach ($aaaa as $key => $value) {
						?>
								<tr>
									<td width="10%"><?php echo $value['0']; ?></td>
									<td width="40%"><?php echo $value['1']; ?></td>
									<td align="center">B</td>
									<td align="center" width="20%"><?php echo $value['2'] . " = " . $value['3'] . " - " . $value['4']; ?></td>
									<!-- <td><?php echo $value['3']; ?></td>
									<td><?php echo $value['4']; ?></td> -->
									<td align="center" width="20%"><?php echo $value['5']; ?></td>
								</tr>
						<?php
							}
						?>
						<?php
							$x=1;
							foreach ($bbbb as $key => $value) {
						?>		
								<tr>
									<td><?php echo $value['0']; ?></td>
									<td><?php echo $value['1']; ?></td>
									<td align="center">B</td>
									<?php if($x == 1): ?>
										<td colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIVISIONS</b></td>
										<?php $x++; ?>
									<?php continue; ?>
									<?php endif; ?>
									<td align="center" ><?php echo $value['2'] . ' - ' . $value['3']; ?></td>
									<td align="center" ><?php echo $value['4']; ?></td>
								</tr>
						<?php
							}
						?>					
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td colspan="4">
								<b>MAONI YA MWALIMU WA DARASA:</b>
							</td>
						</tr>
					</table>
					
					<table width="100%">	
						<tr>
							<td width="35%"><b>JINA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $class_teacher['teacher_names']; ?></b></td>
							<td width="15%"><b>SAHIHI:</b>&nbsp;&nbsp;&nbsp;.................</td>
							<td width="25%"><b>SIMU NAMBA:</b>&nbsp;&nbsp;&nbsp;<?php echo $class_teacher['phone_no']; ?></td>
							<td width="25%"><b>TAREHE:</b>&nbsp;&nbsp;&nbsp;<?php echo date('Y-m-d'); ?></td>
						</tr>
					</table>

					<table width="100%">
						<tr>
							<td colspan="4"><b>Tafadhali wasiliana na mwalimu wa darasa kwa tatizo lolote.</b></td>
						</tr>
					</table>
					</br>
					<table width="100%">
						<tr>
							<td colspan="4"><b>MAONI YA MKUU WA SHULE:</b></td>
							<td colspan="4"></td>
						</tr>
					</table>
					</br>
					<table width="100%">
						<tr>
							<td width="40%"><b>JINA:</b>&nbsp;&nbsp;&nbsp;<b>MKUU WA SHULE</b></td>
							<td width="60%"><b>SAHIHI:</b>&nbsp;&nbsp;&nbsp;</td>
						</tr>
					</table>
				

				</div>
			<div>
		<div>
	<div>

	<br/><br>		

	<input type="button" onclick="printDiv('printableArea')" value="Print Student Result" />	


</body>
</html>