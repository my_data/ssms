
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">
          
                <?php echo form_open('students/enrollment'); ?>

    <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
                        <?php echo form_close(); ?>
               
            </div>&nbsp;&nbsp;
            <!-- <button onclick="printDiv('printAH')" class="btn btn-primary btn-xs;" title="Print"/><span class="glyphicon glyphicon-print"></span></button> --> 
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <!-- <td width="2%" align="center">S/No.</td> -->
        <td width="25%" align="center">Student Names</td>
        <td width="28%" align="center">Class</td>
        <td width="15%" align="center">Academic Year</td>
        <td width="15%" align="center">Options</td>
      </tr>      
    </thead>
    <tbody>
    <?php if($enrollment_data == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; $adm_no = ""; ?>
    <?php foreach($enrollment_data as $ed): ?>
        <tr>
            <!-- <td align="center"><?php echo $x+$p."."; ?></td> -->
            <td align="center">
              <?php 
                if($adm_no != $ed['admission_no']){
                  echo $ed['student_names']; 
                }
              ?>
            </td>
            <td align="center"><?php echo $ed['class_name']; ?></td>
            <td align="center"><?php echo $ed['year']; ?></td>
            <td align="center">
              <a href="<?php echo base_url() . 'reports/yearly_results/' . $ed['admission_no'].'/'.$ed['academic_year']; ?>" data-toggle="tooltip" data-placement="bottom" title="View More Details" class="btn btn-primary btn-xs">R<!-- <span class="fa fa-exam"></span >--></a><!-- LINK YA MATOKEO HUO MWAKA-->
              </td>
        </tr>
        <?php $x++; $adm_no = $ed['admission_no']; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
