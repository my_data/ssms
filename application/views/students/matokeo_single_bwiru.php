

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user"></span>&nbsp;
  <?php
 echo $record['names']; // $record['class_name'] . ", " . $record['rank'] . ", " . $record['year'] . ",  " . $record['average'] . ", " . $record['grade'] . ", " . $no_of_students . ", " . $division_and_points['division_name'] . ", " . $division_and_points['points'];
  ?>
 </div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

<div class="input-group">
<div class="input-group-btn">
    
       </div>
</div>
      <a href="<?php echo base_url() . 'students/print_results_by_division/' . $admission_no .'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
            &nbsp;&nbsp;
      <a href="<?php echo base_url() . 'students/print_results_by_grade/' . $admission_no .'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
</div>

</div>
</div>
<div class="form-group">
	<?php if($this->session->flashdata('error_message')): ?>
		<div class="alert alert-dismissible alert-danger text align-center">
			<?php echo $this->session->flashdata('error_message'); ?>
		</div>
	<?php endif; ?>
</div>
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td align="center">Class Name</td>
			<td align="center">Year</td>
			<td align="center">Average</td>
			<td align="center">Grade</td>
			<td align="center">Division</td>
			<td align="center">Points</td>
			<td align="center">Position</td>
			<td align="center">Out Of</td>
		</tr>
	</thead>
	<tbody>
		<td align="center"><?php echo  $record['class_name']; ?></td>
		<td align="center"><?php echo $record['year']; ?></td>
		<td align="center"><?php echo $record['average']; ?></td>
		<td align="center"><?php echo $record['grade']; ?></td>
		<td align="center"><?php echo $division_and_points['division_name']; ?></td>
		<td align="center"><?php echo $division_and_points['points']; ?></td>
		<td align="center"><?php echo $record['rank']; ?></td>
		<td align="center"><?php echo $no_of_students; ?></td>
	</tbody>
</table>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Subject</th>
      <td align="center">Monthly</td>
      <td align="center">Mid-Term</td>
      <td align="center">Monthly</td>
      <td align="center">Terminal</td>
      <td align="center">Average</td>
      <td align="center">Grade</td>
	<td align="center">Rank</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach($mkeka_single as $mk): ?>
      <tr>
        <td><?php echo $mk['subject_name']; ?></td>
        <td align="center"><?php echo $mk['monthly_one']; ?></td>
        <td align="center"><?php echo $mk['midterm']; ?></td>
        <td align="center"><?php echo $mk['monthly_two']; ?></td>
        <td align="center"><?php echo $mk['terminal']; ?></td>
        <td align="center"><?php echo $mk['average']; ?></td>
        <td align="center"><?php echo $mk['grade']; ?></td>
	<td align="center"><?php echo $mk['subject_rank']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
