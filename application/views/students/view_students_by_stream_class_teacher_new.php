
  <div class="white-area-content responsive">
      <div class="db-header clearfix">

  <div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
  </div>
      <div class="db-header-extra form-inline text-right"> 

          <div class="form-group has-feedback no-margin">
         <?php echo form_open('myclass/students/'.$class_stream_id); ?>
       <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
          
            
           <?php echo form_close(); ?>
      </div>

    <a href="<?php echo base_url() . 'myclass/assign_monitor/'.$class_stream_id; ?>" class="btn btn-primary btn-sm">Add Class Monitor</a>

  </div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table style="width:50%;" class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td width="70%">Class Monitor</td>
      <td width="20%">Students#</td>
    </tr>
  </thead>
  <tbody>    
    <tr>
      <td><?php if($class_monitor['class_monitor'] == ""){ echo "None"; }else{ echo $class_monitor['class_monitor']; } ?></td>
      <td><?php if($mystudents == FALSE){ echo '0'; }else{ echo $nos['nos']; } ?></td>
    </tr>
  </tbody>
</table>
<br/>
<!-- <table id="students_class_teacher" class="table table-striped table-hover table-condensed table-bordered"> -->
<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td width="5%">S/No.</td>
      <td>Admission#</td>
      <td>Firstname</td>
      <td>Lastname</td>
      <td>Action</td>
    </tr>
  </thead>
  <tbody>
    <?php if ($mystudents == FALSE): ?>
      <tr>
        <td colspan="6">
              <?php
                  $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are no students in this class";
                  echo $message;
              ?>
          </td>
      </tr>
    <?php else: ?>
    <?php $x = 1; ?>
    <?php foreach($mystudents as $mystudent): ?>
      <tr>
        <td><?php echo $x."."; ?></td>
        <td><?php echo $mystudent['admission_no']; if($mystudent['stte_out'] == "suspended"){ echo "<span style='color: red;' title='Currently Suspended' data-placement='bottom'><strong>***</strong></span>"; } ?></td>
        <td><?php echo $mystudent['firstname']; ?></td>
        <td><?php echo $mystudent['lastname']; ?></td>
        <td>
          &nbsp; &nbsp; &nbsp; &nbsp;
          <a href="<?php echo base_url() . 'students/get_student_profile/' . $mystudent['admission_no']; ?>" data-toggle="tooltip" data-placement="bottom" title="View More Details" class="btn btn-primary btn-xs"><span class="fa fa-user"></span></a> 
        </td>
      </tr>
      <?php $x++; ?>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
  <div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'students/get_students_by_class_stream/'.$class_stream_id.''; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div>