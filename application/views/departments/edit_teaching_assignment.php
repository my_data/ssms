
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"><?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('departments/edit_teaching_assignment/'.$dept_id.'/'.$assignment_record['assignment_id']); ?>
	<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>">

	<br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Subject Name : </label>
		</div>
		<div class="col-xs-10">
			<select class="form-control" name="subject_id" id="subject_id">
				<option value="">--Select--</option>
					<?php foreach($subjects as $subject): ?>
						<option 
							<?php 
								if($subject['subject_id'] === $assignment_record['subject_id']){
									echo "selected='true'";
								}
							?>
						value="<?php  echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<?php echo form_error('subject_id'); ?>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Teacher Name : </label>
		</div>
		<div class="col-xs-10">
			<select class="form-control" name="teacher_id" id="teacher_id">
				<option value="">--Select--</option>
					<?php foreach($records as $record): ?>
						<option
							<?php 
								if($record['staff_id'] === $assignment_record['teacher_id']){
									echo "selected='true'";
								}
							?>
							value="<?php echo $record['staff_id']; ?>"><?php echo $record['names']; ?></option>
					<?php endforeach; ?>
			</select>
			<?php echo form_error('teacher_id'); ?>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Class : </label>
		</div>
		<div class="col-xs-10">
			<select  class="form-control" name="class_id" id="class_id">
				<option value="">--Select--</option>
					<?php foreach($class_names as $class_name): ?>
						<option
							<?php 
								if($class_name['class_id'] === $assignment_record['class_id']){
									echo "selected='true'";
								}
							?>
							 value="<?php echo $class_name['class_id']; ?>"><?php echo $class_name['class_name']; ?></option>
					<?php endforeach; ?>
			</select>
			<?php echo form_error('class_id'); ?>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Stream : </label>
		</div>
		<div class="col-xs-10">
			<select class="form-control" name="stream" id="stream" >
				<option value="">--Select--</option>
					<?php foreach($streams as $stream): ?>
						<option 
							<?php 
								if($stream['stream'] === $assignment_record['stream']){
									echo "selected='true'";
								}
							?>
							value="<?php echo $stream['stream']; ?>"><?php echo $stream['stream']; ?></option>
					<?php endforeach; ?>
			</select>
			<?php echo form_error('stream'); ?>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>From : </label>
		</div>
		<div class="col-xs-10">
			<input type="date" class="form-control" name="start_date" id="start_date" value="<?php echo $assignment_record['start_date']; ?>" />
			<?php echo form_error('start_date'); ?>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>To : </label>
		</div>
		<div class="col-xs-10">
			<input type="date" name="end_date" class="form-control" id="end_date" value="<?php echo $assignment_record['end_date']; ?>" />
			<?php echo form_error('end_date'); ?>
		</div>
	</div>
	<br/><br/>
	<!-- <div class="form-group">
		<div class="col-xs-2">
			<label>Term : </label>
		</div>
		<div class="col-xs-10">
			<select name="term" class="form-control" id="term" >
				<option value="">--Select--</option>
				<option <?php if($assignment_record['term'] === "first_term"){ echo "selected='true'"; } ?> value="first_term">First Term</option>
				<option <?php if($assignment_record['term'] === "second_term"){ echo "selected='true'"; } ?>  value="second_term">Second Term</option>
			</select>
		</div>
	</div>
	<br/><br/> -->
	<div align="center">
		<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>">
		<input type="submit" name="update_teaching_assignment" value="Update" class="btn btn-primary form-control" onClick="return confirm('Save changes');" />
	</div>
	<br/>

</form>
