
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"><?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php echo form_open('departments/teaching_assignment_form/'.$dept_id); ?>
	<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>">
	<br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Subject Name : </label>
		</div>
		<div class="col-xs-10">			
			<?php echo form_error('subject_id'); ?>
			<select class="form-control" name="subject_id" id="subject_id">
				<option value="">--Select--</option>
					<?php foreach($subjects as $subject): ?>
						<option value="<?php  echo $subject['subject_id']; ?>" <?php echo  set_select('subject_id', ''.$subject["subject_id"].'', $this->input->post('subject_id')); ?>><?php echo $subject['subject_name']; ?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Teacher Name : </label>
		</div>
		<div class="col-xs-10">
			<?php echo form_error('teacher_id'); ?>
			<select class="form-control" name="teacher_id" id="teacher_id">
				<option value="">--Select--</option>
					<?php foreach($records as $record): ?>
						<option value="<?php echo $record['staff_id']; ?>" <?php echo  set_select('teacher_id', ''.$record["staff_id"].'', $this->input->post('teacher_id')); ?>><?php echo $record['names']; ?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Class : </label>
		</div>
		<div class="col-xs-10">
			<?php echo form_error('class_id'); ?>
			<select class="form-control" name="class_id" id="class_id">
				<option value="">--Select--</option>
					<?php foreach($class_names as $class_name): ?>
						<option value="<?php echo $class_name['class_id']; ?>" <?php echo  set_select('class_id', ''.$class_name["class_id"].'', $this->input->post('class_id')); ?>><?php echo $class_name['class_name']; ?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>Stream : </label>
		</div>
		<div class="col-xs-10">
			<?php echo form_error('stream'); ?>
			<select class="form-control" name="stream" id="stream" onchange="validate_ta();">
				<option value="">--Select--</option>
					<?php foreach($streams as $stream): ?>
						<option value="<?php echo $stream['stream']; ?>" <?php echo  set_select('stream', ''.$stream["stream"].'', $this->input->post('stream')); ?>><?php echo $stream['stream']; ?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>From : </label>
		</div>
		<div class="col-xs-10">
			<?php echo form_error('start_date'); ?>
			<input type="date" name="start_date" id="start_date" class="form-control" value="<?php echo set_value('start_date'); ?>" />
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-xs-2">
			<label>To : </label>
		</div>
		<div class="col-xs-10">
			<?php echo form_error('end_date'); ?>
			<input type="date" name="end_date" id="end_date" class="form-control" value="<?php echo set_value('end_date'); ?>" />
		</div>
	</div>
	<br/><br/>
	<!-- <div class="form-group">
		<div class="col-xs-2">
			<label>Term : </label>
		</div>
		<div class="col-xs-10">
			<select name="term" id="term" class="form-control" >
				<option value="">--Select--</option>
				<option value="first_term">First Term</option>
				<option value="second_term">Second Term</option>
			</select>
		</div>
	</div>
	<br/><br/> -->
	<div align="center">
		<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>">
		<input type="submit" name="save_teaching_assignment" value="Assign To Subject" class="btn btn-primary form-control" />
	</div>
	<br/>
</form>
