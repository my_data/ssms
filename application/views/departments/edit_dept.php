
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('departments/edit_department/'.$dept_record['dept_id'], $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="dept_name">Department Name :</label>
			<div class="col-sm-9">
				<input type="text" name="dept_name" class="form-control" value="<?php echo $dept_record['dept_name']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('dept_name'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="dept_name">Location :</label>
			<div class="col-sm-9">
				<input type="text" name="dept_loc" class="form-control" value="<?php echo $dept_record['dept_loc']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('dept_loc'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<!-- <div class="form-group">
			<label class="col-sm-3 control-label" for="level">Type :</label>
			<div class="col-sm-9">
				<select name="dept_type" class="form-control" id="dept_type" required>
					<option value="">--Choose Level--</option>
					<option <?php if($dept_record['dept_type'] === 'subject_department'){ echo "selected"; } ?> value="subject_department">Subject Department</option>
					<option <?php if($dept_record['dept_type'] === 'non_subject_department'){ echo "selected"; } ?> value="non_subject_department">Non Subject Deparmtent</option>
				</select>
			</div>
		</div>
		<br/><br/><br/> -->
		<div class="form-group">
			<input type="hidden" name="dept_id" value="<?php echo $dept_record['dept_id']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_department" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>