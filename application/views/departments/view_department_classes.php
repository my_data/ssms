
<div class="white-area-content">
<div class="db-header clearfix">


 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 


<?php echo form_open('departments/classes/'.$dept_id); ?>

 <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>

<?php echo form_close(); ?>

</div>

</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td align="center" width="15%">ClassName</td>
      <td align="center" width="65%">Subject(s) Offered</td>
      <td align="center" width="20%">Action</td>
    </tr>
  </thead>
  <tbody>
    <?php if ($classes == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records to display";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php $x = ""; //Initiaze $x to test if class_id has already echoed?> 
    <?php foreach($classes as $cs): ?>
      <?php if($cs['class_id'] !== $x): //Making sure the subject doesn't echo twice for the same class?>
      <tr>
        <td align="center"><?php echo $cs['class_name']; ?></td>
        <td><?php echo $cs['subjects']; ?></td>
          
        <td align="center">
          <a href="<?php echo base_url() . 'students/students_in_department/'.$dept_id.'/'.$cs['class_id']; ?>" class="btn btn-primary btn-xs" title="View Students" data-placement="bottom" data-toggle="tooltip"><span class="fa fa-eye"></span></a>
         <!-- <a href="<?php echo base_url() . 'subjects/log_book/'.$dept_id.'/'.$cs['class_id']; ?>" class="btn btn-info btn-xs"  title="View Teaching Log" data-placement="bottom" data-toggle="tooltip"><span class="fa fa-book"></span></a>-->
        </td>
      </tr>
      <?php $x = $cs['class_id']; //Assing class_id to $x ?>
      <?php endif; ?>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
</div>
