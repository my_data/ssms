
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title">&nbsp;&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<form role="form" action="<?php echo base_url('departments/add_hod/'.$dept_id); ?>" method="post" class="form-horizontal">
		<br/>
		<div class="form-group">
			<label for="staff_names" class="col-xs-2 text-right label-control">Staff Name:</label>
			<div class="col-xs-10">   
				<select class="form-control " name="staff_id" >
					<option value="">--Select Staff--</option>
					<?php foreach ($staffs as $staff): ?>
						<option <?php if($staff['staff_id'] === $current_hod){ echo "selected"; } ?> value="<?php echo $staff['staff_id']; ?>" ><?php echo $staff['staff_names']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('staff_id'); ?>
				</div>
			</div>
		</div>
		<br/>
		<div class="form-group">
			<label for="submit" class=" label-control"></label>
			<div class="col-xs-12">
				<input type="hidden" value="<?php echo $dept_id; ?>" name="dept_id">
				<input type="Submit" class="btn btn-primary form-control" value="Save Changes">
			</div>
		</div>

	</form>


</div>
