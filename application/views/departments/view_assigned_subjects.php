
<div class="white-area-content">
<div class="db-header clearfix">


 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 
<div class="form-group has-feedback no-margin">
<?php echo form_open('departments/subject_assignment/'.$dept_id); ?>


   <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_assignment" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
    </div>
  
<?php echo form_close(); ?>
</div>

    <a href="<?php echo base_url() . 'departments/teaching_assignment_form/'.$dept_id; ?>" class="btn btn-primary btn-sm">Assign New</a>

</div>

</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">      
      <td width="30%">Staff Names</td>
      <td width="10%" align="center">Gender</td>
      <td width="30%">Subject Name</td>
      <td width="10%" align="center">Class Name</td>
      <td width="10%" align="center">Stream</td>
      <td width="10%" align="center">Action&nbsp;&nbsp;
      <?php if($show == "yes"): ?>
        <a href="<?php echo base_url() . 'departments/assign_all/'.$dept_id; ?>" class="btn btn-primary btn-xs" title="Extend teaching assignment to this current term" onClick="return confirm('Are you sure the same teachers will be teaching the same subjects this term?');"><span class="glyphicon glyphicon-cog"></span></a>
      <?php endif; ?>
      </td>
    </tr>
  </thead>
  <tbody>
    <?php if($assignment_record == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "None have been assigned";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
    <?php foreach($assignment_record as $ar): ?>
      <tr>
        <td><?php echo $ar['firstname'] . " " . $ar['lastname']; ?></td>
        <td align="center"><?php echo $ar['gender']; ?></td>
        <td><?php echo $ar['subject_name']; ?></td>
        <td><?php echo $ar['class_name']; ?></td>
        <td align="center"><?php echo $ar['stream']; ?></td>     
        <td align="center">
          <?php echo form_open('departments/delete_teaching_assignment'); ?>
            <a href="<?php echo base_url(). 'departments/edit_teaching_assignment/'.$ar['dept_id'].'/'.$ar['assignment_id']; ?>" class="btn btn-primary btn-xs" data-toggle="modal tooltip" data-placement="bottom" title="Edit" ><i class="fa fa-pencil"></i></a>
            <input type="hidden" name="teacher_id" value="<?php echo $ar['staff_id']; ?>" />
            <input type="hidden" name="subject_id" value="<?php echo $ar['subject_id']; ?>" />
            <input type="hidden" name="class_stream_id" value="<?php echo $ar['class_stream_id']; ?>" />
            <input type="hidden" name="dept_id" value="<?php echo $ar['dept_id']; ?>" />
            <button type="submit" class="btn btn-xs btn-danger" data-placement="bottom" title="Delete" data-toggle="tooltip" onClick="return confirm('Are you sure you want to delete this record?');"><span class="glyphicon glyphicon-trash"></span></button>
          <?php echo form_close(); ?>
        </td>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
