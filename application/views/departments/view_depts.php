
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

<div class="form-group has-feedback no-margin">
    <?php echo form_open('departments/index'); ?>
      
         <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_department" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
         </div>
      <?php echo form_close(); ?>
      </div>
<?php if($this->session->userdata('manage_department') == 'ok'): ?>
            <a href="<?php echo base_url() . 'departments/new_department'; ?>" class="btn btn-primary btn-sm">Add Department</a>
<?php endif; ?>
            </div>
          <?php //if($this->session->userdata('permission') === 'add_new_department'): ?>
            
          <?php //endif; ?>
          </div>
         
   
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr class="table-header">
            <td width="2%" align="center">No.</td>
            <td width="30%" align="center">Department Name</td>
            <td width="15%" align="center">Location</td>
            <td width="15%" align="center">HOD</td>
            <?php if($this->session->userdata('manage_department') == 'ok' || $this->session->userdata('add_hod') == 'ok' || $this->session->userdata('view_teaching_logs') == 'ok' || $this->session->userdata('manage_teaching_assignments') == 'ok'): ?>
              <td width="18%" align="center">Options
                <?php if($this->session->userdata('manage_department') == 'ok'): ?>
                  <span style="float: right;">
                    <a href="<?php echo base_url() . 'departments/current_and_past_hods'; ?>" class="btn btn-primary btn-xs" title="Past And Current HODs" data-placement="bottom"><strong>HODH</strong></a>
                  </span>
                <?php endif; ?>
              </td>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
    <?php if($departments == FALSE): ?>
      <tr>
        <td colspan="5">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Departments";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
      <?php $x = 1; ?>
    <?php foreach($departments as $dept): ?>
        <tr>
            <td align="center"><?php echo $x+$p."."; ?></td>
            <td align="center"><?php echo $dept['dept_name']; ?></td>
            <td align="center"><?php echo $dept['dept_loc']; ?></td>
            <td align="center"><?php echo $dept['firstname'] . " " . $dept['lastname']; ?></td>
            <?php if($this->session->userdata('manage_department') == 'ok' || $this->session->userdata('add_hod') == 'ok' || $this->session->userdata('view_teaching_logs') == 'ok' || $this->session->userdata('manage_teaching_assignments') == 'ok'): ?>
              <td align="center">
                <?php if($this->session->userdata('manage_department') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>departments/edit_department/<?php echo $dept['dept_id']; ?>" class="btn btn-primary btn-xs" data-target="modal" data-placement="bottom" title="Edit Department"><span class="fa fa-pencil"></span></a>&nbsp;
                  <a href="<?php echo base_url() . 'departments/get_staff_by_department/'.$dept['dept_id']; ?>" data-placement="bottom" title="Department members" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>&nbsp;
                  <a href="<?php echo base_url() . 'subjects/get_subject_by_dept/'.$dept['dept_id']; ?>" data-toggle="modal" data-placement="bottom" title="View Subjects Offerred" data-target="#myModal<?php echo $dept['dept_id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('manage_teaching_assignments') == 'ok'): ?>
                  <a href="<?php echo base_url() . 'departments/subject_assignment/'.$dept['dept_id']; ?>" class="btn btn-xs btn-primary" title="Manage Teaching Assignments" data-placement="bottom" >M</a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('view_teaching_logs') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>subject_teaching_logs/<?php echo $dept['dept_id']; ?>" class="btn btn-primary btn-xs" data-placement="bottom" title="View Logs"><span>L</span></a>
                <?php endif; ?>
                <?php if($this->session->userdata('add_hod') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>departments/add_hod/<?php echo $dept['dept_id']; ?>" class="btn btn-xs btn-primary" title="Add HOD" data-placement="bottom">A</a>&nbsp;
                <?php endif; ?>
                <?php if($this->session->userdata('manage_department') == 'ok'): ?>
                  <a href="<?php echo base_url(); ?>departments/delete_department/<?php echo $dept['dept_id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to delete this department?');"><span class="glyphicon glyphicon-trash"></span></a>
                <?php endif; ?>
              </td>
            <?php endif; ?>
        </tr>
        <?php $x++; ?>
    <?php endforeach; ?>
  <?php endif; ?>
    </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'departments'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
<!-- <div class="col-md-9">

<h3 align="center"><?php echo $title; ?></h3><br />

<table id="department_data" class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th width="10%">DeptID</th>
            <th width="30%" >Name_of_the_Department</th>
            <th width="20%">Department_Location</th>
            <th width="10%">HOD</th>
            <th width="5%">View</th>
            <th width="5%">Edit</th>
            <th width="5%">Delete</th>
        </tr>
    </thead>
</table> -->

<!-- PINGING THE DATABASE: $this->db->reconnect(); -->

<!--SUBJECTS OFFERED -->
<?php foreach ($departments as $dept): ?>

    <div class="modal fade" id="myModal<?php echo $dept['dept_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Subject(s) in: " . $dept['dept_name']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <?php foreach($subjects as $subject): ?>
                    <?php
                        if($dept['dept_id'] === $subject['dept_id']){
                    ?>
                        <ul>
                            <li><?php echo $subject['subject_name']; ?></li>
                        </ul>
                    <?php
                        }
                    ?>
                <?php endforeach; ?>                                    
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!--END SUBJECTS OFFERED -->

<!-- ASSIGN HOD -->


<?php foreach ($departments as $dept): ?>

  <div class="modal fade" id="myModalAssign<?php echo $dept['dept_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Assign HOD To: " . $dept['dept_name']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
              <?php
                $attributes = array('class' => 'form-horizontal', 'role' => 'form')
              ?>            
                <?php echo form_open('departments/assign_hod/'.$dept['dept_id'], $attributes); ?>
                    <div class="form-group">
                      <label class="col-sm-4 control-label" for="teacher_names" >Teacher Name</label>
                      <div class="col-sm-8">
                          <select name="staff_id" class="form-control">
                            <option value="">--Choose--</option>
                            <?php foreach($teachers as $teacher): ?>
                              <option value="<?php echo $teacher['staff_id']; ?>" 
                              <?php 
                                if($teacher['staff_id'] == $dept['staff_id']){
                                  echo "selected";
                                }
                              ?>><?php echo $teacher['firstname'] . " " . $teacher['lastname']; ?></option>
                            <?php endforeach; ?>
                          </select>
                      </div>
                    </div>               
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END ASSIGN HOD -->
