
<div class="white-area-content">
<div class="db-header clearfix">


 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 


<?php echo form_open('departments/get_staff_by_department/'.$dept_id); ?>
       <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_staff" placeholder="Search ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
        </div>

<?php echo form_close(); ?>


</div>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td align="center" width="30%">Staff Names</td>
      <td align="center" width="20%">Gender</td>
      <td align="center" width="25%">Subject</td>
      <td align="center" width="25%">Action</td>
    </tr>
     </thead>
  <tbody>
  <?php if ($staffs == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Staffs in this Department";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php foreach($staffs as $staff): ?>
      <tr>
        <td><?php echo $staff['staff_names']; ?></td>
        <td align="center"><?php echo $staff['gender']; ?></td>
        <td align="center"><?php echo $staff['subjects']; ?></td>
        <td align="center"><a href="<?php echo base_url() . 'staffs/profile/'.$staff['staff_id']; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" title="View More Details"  data-placement="bottom"><span class="glyphicon glyphicon-user"></span></a></td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>

<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
</div>
<?php
  echo $links;
?>