
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
                <!-- <?php echo form_open('periods'); ?>
                <input type="text" class="form-control input-sm" name="search_period" placeholder="Search ..." id="form-search-input" /> -->
                  <div class="input-group-btn">
                      <!-- <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?> -->
                            
                    </div>
              </div>
            </div>
          <?php if($this->session->userdata('manage_periods') == 'ok'): ?>
            <a href="<?php echo base_url() . 'periods/new_period'; ?>" class="btn btn-primary btn-sm">Add Period</a>&nbsp;<a href="<?php echo base_url() . 'periods/new_break'; ?>" class="btn btn-primary btn-sm">Set Breaks</a>
          <?php endif; ?>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td width="5%" align="center">Period#</td>
			<td width="30%" align="center">Start Time</td>
			<td width="30%" align="center">End Time</td>
			<td width="5%" align="center">Duration</td>
      <?php if($this->session->userdata('manage_periods') == 'ok'): ?>
			 <td width="15%" align="center">Action</td>
      <?php endif; ?>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($periods as $period):
		?>
			<tr>
				<td align="center"><?php echo $period['period_no']; ?></td>
				<td align="center"><?php echo $period['start_time']; ?></td>
				<td align="center"><?php echo $period['end_time']; ?></td>
				<td align="center"><?php echo $period['duration']; ?></td>
        <?php if($this->session->userdata('manage_periods') == 'ok'): ?>
  				<td align="center">
            <a href="<?php echo base_url(); ?>periods/edit/<?php echo $period['period_no']; ?>"  class="btn btn-xs btn-primary"><span class="fa fa-pencil"></span></a>&nbsp;
    				<a href="<?php echo base_url(); ?>periods/delete/<?php echo $period['period_no']; ?>" class="btn btn-xs btn-danger" onClick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span></a>
          </td>
        <?php endif; ?>
			</tr>
		<?php
        foreach ($breaks as $br):
          if($period['period_no'] === $br['after_period']):
          ?>
            <tr>
              <td colspan="3" align="center" style="font-size: 16px;"><?php echo $br['description']; ?></td>
              <td align="center"><?php echo $br['duration']; ?></td>
              <?php if($this->session->userdata('manage_periods') == 'ok'): ?>
                <td align="center">
                  <a href="<?php echo base_url(); ?>periods/edit_break/<?php echo $br['bid']; ?>"  class="btn btn-xs btn-primary"><span class="fa fa-pencil"></span></a>&nbsp;
                  <a href="<?php echo base_url(); ?>periods/remove_break/<?php echo $br['bid']; ?>" class="btn btn-xs btn-danger" onClick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
              <?php endif; ?>
            </tr>
          <?php
          endif;
        endforeach;
			endforeach;
		?>
	</tbody>
</table>
</div>