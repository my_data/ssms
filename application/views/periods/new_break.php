
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('periods/new_break', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">After Period No. :</label>
			<div class="col-sm-9">
				<select name="after_period_no" class="form-control" id="after_period_no" required="true">
					<option value="">--Choose--</option>
					<?php foreach($periods as $p): ?>
						<option <?php echo set_select('after_period_no', $p['period_no'], $this->input->post('after_period_no')); ?> value="<?php echo $p['period_no']; ?>"><?php echo $p['period_no']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('after_period_no'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">Description :</label>
			<div class="col-sm-9">
				<input type="text" name="description" class="form-control" value="<?php echo set_value('description'); ?>" />
				<div class="error_message_color">
					<?php echo form_error('description'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-9">
				<input type="submit" class="form-control btn btn-primary" name="add_break" value="Add Break" />
			</div>
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>