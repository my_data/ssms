
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('periods/edit_break/'.$bid, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">Description :</label>
			<div class="col-sm-9">
				<input type="text" name="description" class="form-control" value="<?php echo $break_record['description']; ?>" />
			</div>
		</div>
		<br/><br/><br/>
		<!-- Uvivu wa kutengeneza form_validation nyingine, tumia ile ile ya add-edit_break -->
		<input type="hidden" name="after_period_no" class="form-control" value="<?php echo $break_record['after_period']; ?>" />
		<div class="form-group">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-9">
				<input type="submit" class="form-control btn btn-primary" name="update_break" value="Save Changes" />
			</div>
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>