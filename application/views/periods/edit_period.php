
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('periods/edit/'.$period_no, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">Start Time :</label>
			<div class="col-sm-9">
				<input type="text" name="start_time" class="form-control" value="<?php echo $period_record['start_time']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('start_time'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="location">End Time :</label>
			<div class="col-sm-9">
				<input type="text" name="end_time" class="form-control" value="<?php echo $period_record['end_time']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('end_time'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="period_no" class="form-control" value="<?php echo $period_record['period_no']; ?>" />
			<input type="submit" class="form-control btn btn-primary" value="Update" onClick="return confirm('Save Changes?');"/>
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>