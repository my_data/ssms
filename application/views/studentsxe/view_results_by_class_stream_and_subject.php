
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">

                <?php echo form_open('students/student_results/'.$class_stream_id.'/'.$subject_id); ?>
        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
        </div>
         <?php echo form_close(); ?>
              
              </div>
            </div>

      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Grade</td>
      <?php foreach($grade_system as $gs): ?>
        <td><?php echo $gs["grade"]; ?></td>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
     <tr>
        <td>NO. STUDENTS</td>
        <?php foreach($grade_system as $gs): ?>
          <?php $x = 0; ?>
          <?php foreach($grade_groups as $gg): ?>
            <?php if($gg['grade'] === $gs['grade']): ?>
              <td><?php echo $gg['count']; ?></td>
            <?php else: ?>
              <?php $x++; ?>
              <?php if($x == count($grade_groups)): ?>
                <td><?php echo "0"; ?></td>
              <?php endif; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
    <tr>
  </tbody>
</table>


<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered table-responsive">
  <thead>
    <tr class="table-header">
      <td>Admission#</td>
      <td>Student Names</td>
      <td>Monthly</td>
      <td>Mid-Term</td>
      <td>Monthly</td>
      <td>Terminal</td>
      <td>Average</td>
      <td>Grade</td>
      <td>Rank</td>
    </tr>
  </thead>
  <tbody>
    <?php if ($mkeka == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Uploaded Results fot this subject";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <input type="hidden" name="class_stream_id" value="<?php echo $class_stream_id; ?>" />
    <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
    <?php //$rank = 1; ?>
    <?php foreach($mkeka as $mk): ?>
      <tr>
        <input type="hidden" name="admission_no[]" value="<?php echo $mk['admission_no']; ?>" />
        <input type="hidden" name="monthly_one[]" value="<?php echo $mk['monthly_one']; ?>" />
        <input type="hidden" name="midterm[]" value="<?php echo $mk['midterm']; ?>" />
        <input type="hidden" name="monthly_two[]" value="<?php echo $mk['monthly_two']; ?>" />
        <input type="hidden" name="terminal[]" value="<?php echo $mk['terminal']; ?>" />
        <input type="hidden" name="average[]" value="<?php echo $mk['average']; ?>" />
        <input type="hidden" name="class_id[]" value="<?php echo $mk['class_id']; ?>" />
   
        <td><?php echo $mk['admission_no']; ?></td>
        <td><?php echo $mk['names']; ?></td>
        <td align="center"><?php echo $mk['monthly_one']; ?></td>
        <td align="center"><?php echo $mk['midterm']; ?></td>
        <td align="center"><?php echo $mk['monthly_two']; ?></td>
        <td align="center"><?php echo $mk['terminal']; ?></td>
        <td align="center" align="center"><?php echo $mk['average']; ?></td>
        <td align="center">
      <?php echo $mk['grade'];
        /*foreach ($grade_system as $gs) {
          if($mk['average'] >= $gs['start_mark'] && $mk['average'] <= $gs['end_mark']){
            echo $gs['grade'];

      ?>    <input type="hidden" name="grade[]" value="<?php echo $gs['grade']; ?>" />
      <?php
          }
        }*/
      ?>
        </td>
        <td><?php echo $mk['subject_rank']; //echo $rank; $rank++; ?></td>
      </tr>
    <?php endforeach; ?>
    <!-- <input type="submit" name="submit" value="Compute" class="btn btn-success btn-xs" /> -->
    <?php echo form_open(); ?>
  <?php endif; //End the if of mkeka ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes/classes_timetable'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
