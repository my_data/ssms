
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;
  <?php
  echo $record['names'] . ",  " . $record['class_name'] . ", " . $record['rank'] . ", " . $record['year'] . ",  " . $record['average'] . ", " . $record['grade'] . ", " . $no_of_students;
  ?>
 </div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

<div class="input-group">
<div class="input-group-btn">
    
       </div>
</div>
      <a href="<?php echo base_url() . 'students/print_results/' . $admission_no .'/'.$term_id; ?>" class="btn btn-primary btn-xs" data-placement="bottom" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
</div>

</div>
</div>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr>
      <th>Subject</th>
      <th>Monthly</th>
      <th>Mid-Term</th>
      <th>Monthly</th>
      <th>Terminal</th>
      <th>Average</th>
      <th>Grade</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($mkeka_single as $mk): ?>
      <tr>
        <td><?php echo $mk['subject_name']; ?></td>
        <td><?php echo $mk['monthly_one']; ?></td>
        <td><?php echo $mk['midterm']; ?></td>
        <td><?php echo $mk['monthly_two']; ?></td>
        <td><?php echo $mk['terminal']; ?></td>
        <td><?php echo $mk['average']; ?></td>
        <td><?php echo $mk['grade']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>