
<div class="white-area-content">
<div class="db-header clearfix">


 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('students/students_in_department/'.$dept_id.'/'.$class_id); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
<?php echo form_close(); ?>

</div>
</div>

</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Admission#</td>
      <th>Firstname</td>
      <td>Lastname</td>
      <td>Action</td>
    </tr>
  </thead>
  <tbody>
    <?php if ($mystudents == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in this class for this Department";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php foreach($mystudents as $mystudent): ?>
      <tr>
        <td><?php echo $mystudent['admission_no']; ?></td>
        <td><?php echo $mystudent['firstname']; ?></td>
        <td><?php echo $mystudent['lastname']; ?></td>
        <td align="center">
          <a href="<?php echo base_url() . 'students/get_student_profile/' . $mystudent['admission_no']; ?>" class="btn btn-primary btn-xs" title="View More Details" data-placement="bottom" data-toggle="tooltip"><span class="glyphicon glyphicon-user"></span></a> 
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>