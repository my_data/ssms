
<h3 align="center"><?php echo $title; ?></h3><hr />

<table class="table table-striped table-hover table-condensed">
  <thead>
    <tr>
      <th>Stream</th>
      <th>Form 1</th>
      <th>Form 2</th>
      <th>Form 3</th>
      <th>Form 4</th>
      <th>Form 5</th>
      <th>Form 6</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($no_of_students as $nos): ?>
      <tr>
        <td><?php echo $nos['stream']; ?></td>
        <td>
          <?php
            if($nos['F1']){
              echo $nos['F1'];
            }
            else{
              echo "-";
            }
          ?>
        </td>
        <td>
          <?php
            if($nos['F2']){
              echo $nos['F2'];
            }
            else{
              echo "-";
            }
          ?>
        </td>
        <td>
          <?php
            if($nos['F3']){
              echo $nos['F3'];
            }
            else{
              echo "-";
            }
          ?>
        </td>
        <td>
          <?php
            if($nos['F4']){
              echo $nos['F4'];
            }
            else{
              echo "-";
            }
          ?>         
        </td>
        <td>
          <?php
            if($nos['F5']){
              echo $nos['F5'];
            }
            else{
              echo "-";
            }
          ?>           
        </td>
        <td>
          <?php
            if($nos['F6']){
              echo $nos['F6'];
            }
            else{
              echo "-";
            }
          ?>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
