
        <div class="table-responsive">        
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr class="table-header">
                <td style="width:20%; text-align:center;">Date</td>
                <td style="width:40%; text-align:center;">Description</td>
                <td style="width:40%; text-align:center;">Decision</td>
              </tr>
            </thead>
            <tbody>
            <?php if ($discipline_records == FALSE): ?>
                <tr>
                  <td colspan="3">
                            <?php
                                $message = "There are No Discipline Records for this particular student";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
              <?php foreach($discipline_records as $discipline_record): ?>
              <tr>
                <td><?php echo $discipline_record['date']; ?></td>
                <td><?php echo $discipline_record['description']; ?></td>
                <td><?php echo $discipline_record['decision']; ?></td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
            </tbody>
          </table>
        </div>  
      </div>

  </div>
</div>
