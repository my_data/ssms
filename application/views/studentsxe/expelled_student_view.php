
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">
				
<?php echo form_open('students/expelled'); ?>
			
	<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>
							        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
						          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
						          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
						          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
						        </ul> -->
					 
			</div>

		<!-- <a href="<?php echo base_url() . 'admin/register_student'; ?>" class="btn btn-primary btn-sm">Add Student</a> -->

	</div>
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- <table id="expelled_students" class="table table-striped table-hover table-condensed"> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Admission#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Class</td>
			<td>Stream</td>
			<td align="center">Action</td>
			<!-- <td>Restore</td> -->
		</tr>
	</thead>
	<tbody>
	<?php if ($students == FALSE): ?>
        <tr>
          <td colspan="9">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Suspended Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>
				<td align="center"><?php echo form_open('students/restore_student/'.$student['admission_no']); ?>
					<a href="<?php echo base_url() . 'students/get_student_profile/'.$student['admission_no']; ?>" class="fa fa-eye btn btn-primary btn-xs" data-target="tooltip" data-placement="right" title="View More Details"></a>&nbsp;
					<?php if($flag === "EXPELL"): ?>
						<input type="hidden" name="expell" value="expell" />
					<?php endif; ?>
					<?php if($flag === "SUSPEND"): ?>
						<input type="hidden" name="suspend" value="suspend" />
					<?php endif; ?>
					<?php if($this->session->userdata('restore_suspended_student') == 'ok'): ?>
						<button type="submit" name="restore_student" class="btn btn-primary btn-xs" data-placement="right" data-toggle="tooltip" title="Restore student" onClick="return confirm('Are you sure you want to restore this student?');" /><span class="fa fa-undo"></span></button>
					<?php endif; ?>
				<?php echo form_close(); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students/expelled'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>