
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline">
        <div class="form-group has-feedback no-margin">
        	<!--ADDED FOR AJAX -->
		<input class="form-control" type="text" value="" name="search" id="search"/>
		<br />
		<div class="result"></div>
		<!--ADDED FOR AJAX -->
    	</div>
	</div>
</div>


<?php echo form_open_multipart('students/upload_selected_students'); ?>
<div class="form-group">
	<div class="col-xs-3">
		<label for="exampleInputFile">Upload by CSV file :</label>
	</div>

	<div class="col-xs-3">
		<input type="file" name="file_to_upload" value="Upload" />
	</div>
	
	<div class="col-xs-2">
		<input class="form-control btn btn-primary btn-sm" type="submit" name="upload" value="Upload" />
	</div>
	
</div>
<?php echo form_close(); ?>
<br/><br/><br/>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<?php echo form_open('students/add_selected'); ?>

<div align="center">
	<strong><h4 class="breadcrumb">Student Personal Information:</h4></strong>
</div>
<br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="exam_no">Examination No :</label>
	<div class="col-sm-10">
		<input type="text" name="exam_no" class="form-control" value="<?php echo set_value('exam_no'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('exam_no'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
	<div class="col-sm-10">
		<input type="text" name="firstname" class="form-control" value="<?php echo set_value('firstname'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('firstname'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
	<div class="col-sm-10">
		<input type="text" name="lastname" class="form-control" value="<?php echo set_value('lastname'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('lastname'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="gender">Gender :</label>
	<div class="col-sm-4">
		<input type="radio" name="gender" value="Male" checked="true" /> Male
		<input type="radio" name="gender" value="Female" /> Female
		<div class="error_message_color">
			<?php echo form_error('gender'); ?>
		</div>
	</div>

	<label class="col-sm-2 control-label" for="dob">Date of Birth :</label>
	
	<div class="col-sm-4">
		<input type="date" name="dob"  class="form-control" value="<?php echo set_value('dob'); ?>" required />
		<div class="error_message_color">
			<?php echo form_error('dob'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="nationality">Nationality :</label>
	<div class="col-sm-10">
		<select name="nationality" class="form-control">
			<option value="">--Choose--</option>
			<option <?php echo set_select('nationality', 'Tanzanian', $this->input->post('nationality')); ?> value="Tanzanian">Tanzanian</option>
			<option <?php echo set_select('nationality', 'Other', $this->input->post('nationality')); ?> value="Other">Other</option>
		</select>
		<div class="error_message_color">
			<?php echo form_error('nationality'); ?>
		</div>
	</div>
</div>
<br/><br/>
	<center><h4 class="breadcrumb">Student Contact Information:</h4></center>
<br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="box">P.O.Box :</label>
		<div class="col-sm-4">
			<input type="text" name="box" class="form-control" value="<?php echo set_value('box'); ?>" required/>
			<div class="error_message_color">
			<?php echo form_error('box'); ?>
		</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
	<label class="col-sm-2 control-label" for="region">Region :</label>
		<div class="col-sm-4 " >
			<select name="region" class="form-control" required>
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option value="<?php echo $region['region_name']; ?>" <?php echo set_select('region', ''.$region["region_name"].'', $this->input->post('region')); ?>><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('region'); ?>
			</div>
		</div>
</div>
<br/><br>
<div class="form-group">
	<label class="col-sm-2 control-label" for="tel_no">Phone No :</label>
		<div class="col-sm-4">
			<input type="text" name="tel_no" class="form-control" value="<?php echo set_value('tel_no'); ?>" required/>
			<div class="error_message_color">
				<?php echo form_error('tel_no'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
	<label class="col-sm-2 control-label" for="email">Email :</label>
		<div class="col-sm-4 " >
			<input type="email" name="email" class="form-control" value="<?php echo set_value('email'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('email'); ?>
			</div>
		</div>
</div>
<br/><br>
<div class="form-group">
	<label class="col-sm-2 control-label" for="ward">Ward :</label>
		<div class="col-sm-4">
			<input type="text" name="ward" class="form-control" value="<?php echo set_value('ward'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('ward'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
	<label class="col-sm-2 control-label" for="district">District :</label>
		<div class="col-sm-4 " >
			<input type="text" name="district" class="form-control" value="<?php echo set_value('district'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('district'); ?>
			</div>
		</div>
</div>
<br/><br/>
	<center><h4 class="breadcrumb">Other Information:</h4></center>
<br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="orphan">Orphan :</label>
	<div class="col-sm-10">
		<input type="radio" name="orphan" value="yes" /> Yes
		<input type="radio" name="orphan" value="no" checked="true" /> No
		<div class="error_message_color">
			<?php echo form_error('orphan'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="form">Form :</label>
	<div class="col-sm-10">
		<select class="form-control" name="form" >
				<option value="">--Choose--</option>
				<option <?php echo set_select('form', 'I-O"level', $this->input->post('form')); ?> value="I-O'level">Form One</option>
				<option <?php echo set_select('form', 'V-A"level', $this->input->post('form')); ?> value="V-A'level">Form Five</option>
		</select>
		<div class="error_message_color">
			<?php echo form_error('form'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="former_school">Former School :</label>
	<div class="col-sm-10">
		<input type="text" name="former_school" class="form-control" value="<?php echo set_value('former_school'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('former_school'); ?>
		</div>
	</div>
</div>
<br/><br/>
	<center><h4 class="breadcrumb">Guardian Information:</h4></center>
<br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="g_firstname">First Name :</label>
	<div class="col-sm-10">
		<input type="text" name="g_firstname" class="form-control" value="<?php echo set_value('g_firstname'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('g_firstname'); ?>
		</div>
	</div>
</div>
<br/><br/>
<div class="form-group">
	<label class="col-sm-2 control-label" for="g_lastname">Last Name :</label>
	<div class="col-sm-10">
		<input type="text" name="g_lastname" class="form-control" value="<?php echo set_value('g_lastname'); ?>" required/>
		<div class="error_message_color">
			<?php echo form_error('g_lastname'); ?>
		</div>
	</div>
</div>
<br/><br/>
<br/>
<div class="form-group">	
	<input type="submit" name="submit" value="Add Selected Student" class="btn btn-primary form-control" />
</div>
</form>