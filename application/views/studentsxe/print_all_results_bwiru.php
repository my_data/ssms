<table width="100%">
						<tr>
							<td rowspan="7" width="10%"><img src="<?php echo base_url();?>/assets/images/129.jpg" class="img-responsive" alt=""/></td>
						</tr>
						<tr>
							<td align="center"><b>JAMUHURI YA MUUNGANO WA TANZANIA</b></td>
						</tr>
						<tr>
							<td align="center"><b>OFISI YA RAIS, TAWALA ZA MIKOA NA SERIKALI ZA MITAA (TAMISEMI)</b></td>
						</tr>
						<tr>
							<td align="center"><b>HALMASHAURI YA MANISPAA YA ILEMELA</b></td>
						</tr>
						<tr>
							<td align="center"><b>SHULE YA SEKONDARI YA UFUNDI BWIRU WAVULANA</b></td>
						</tr>
						<tr>
							<td align="center"><b>S.L.P 217 MWANZA</b></td>
						</tr>
						
					</table>

					<table width="100%">
						<!-- <tr>
							<td rowspan="4" width="10%"><img src="<?php echo base_url();?>/asset/images/LOGO.jpg" class="img-responsive" alt=""/></td>
						</tr> -->
						<tr>
							<td><b>KUMBU NA <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></b></td>
						</tr>
						<tr>
							<td><b>ANUANI YA MZAZI</b></td>
						</tr>
						<tr>
							<td><b><u>P.O.Box <?php echo $personal_record['p_box']; ?> ,</u></b></td>
						</tr>
						<tr>
							<td><b><u><?php echo $personal_record['p_region']; ?>.</u></b></td>
						</tr>
						<tr>
							<td><b>Tarehe&nbsp;&nbsp;&nbsp;<u><?php echo date('d-m-Y'); ?></u></b></td>
						</tr>
						
					</table>
					
					<table width="100%">
						<tr>
							<td width="10%"></td>
							<td align="center"><b><u>TAARIFA YA MAENDELEO YA MWANAFUNZI</u></b></td>
						</tr>
					</table>
					<br/>
					<table width="100%">	
						<tr>
							<td width="35%"><b>JINA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $record['names']; ?></b></td>
							<td width="25%"><b>KIDATO:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $record['class_name']; ?></b></td>
							<td width="20%"><b>MUHULA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $academic_year['term_name']; ?></b></td>
							<td width="20%"><b>MWAKA:</b>&nbsp;&nbsp;&nbsp;<b><?php echo $academic_year['year']; ?></b></td>
						</tr>
					</table>

					<br/>
					<table width="100%" class="table-bordered">
						<tr >
							<th style"width:30%;">&nbsp;&nbsp;&nbsp;SOMO</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Zoezi 1</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Zoezi 2 (Midterm)</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Zoezi 3</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Jumla</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Wastani</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Jaribio</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Jumla</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Wastani</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Daraja</th>
							<th width="7%">&nbsp;&nbsp;&nbsp;Nafasi</th>
						</tr>
						<?php $jumla_kuu = 0.00; $number_of_subjects = 0; //Initialiaze special for summing all averages ?>
						<?php foreach($mkeka_single as $mk): ?>
					      <tr>
					        <td style="text-align:center;"><?php echo $mk['subject_name']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['monthly_one']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['midterm']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['monthly_two']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['monthly_one'] + $mk['midterm'] + $mk['monthly_two']; ?></td>
					        <td style="text-align:center;"><?php echo round(($mk['monthly_one'] + $mk['midterm'] + $mk['monthly_two'])/3, 2); ?></td>
					        <td style="text-align:center;"><?php echo $mk['terminal']; ?></td>
					        <td style="text-align:center;">
				        	<?php
				        		$total_ca_and_terminal = round(($mk['monthly_one'] + $mk['midterm'] + $mk['monthly_two'])/3, 2) + $mk['terminal'];
				        		echo $total_ca_and_terminal;
				        	?>
					        </td>
					        <td style="text-align:center;"><?php echo $mk['average']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['grade']; ?></td>
					        <td style="text-align:center;"><?php echo $mk['subject_rank']; ?></td>
					      </tr>
					      <!--SUM ALL THE STUDENT'S SUBJECT AVERAGES-->
					      <?php $jumla_kuu += $mk['average']; ?>
					      <!--INCREMENT THE NUMBER OF SUBJECTS-->
					      <?php $number_of_subjects++; ?>
					    <?php endforeach; ?>
					    <tr>
							<td align="center" colspan="7">
								<b>
								<?php if($compute_by_grade === "OK"): ?>
									JUMLA KUU
								<?php endif; ?>
								</b>
							</td>
							<td align="center">
								<?php if($compute_by_division === "OK"): ?>
									DIVISION
								<?php endif; ?>
							</td>
							<td align="center">
								<b>
								<?php if($compute_by_grade === "OK"): ?>
									<?php echo $jumla_kuu; ?>
								<?php endif; ?>
								<?php if($compute_by_division === "OK"): ?>
									<b><?php echo $division_and_points['division_name']; ?></b>
								<?php endif; ?>
								</b>
							</td>
							<td align="center">
								<?php if($compute_by_division === "OK"): ?>
									POINTS
								<?php endif; ?>
							</td>
							<td align="center">
								<?php if($compute_by_division === "OK"): ?>
									<b><?php echo $division_and_points['points']; ?></b>
								<?php endif; ?>
							</td>
						</tr>
						<?php if($compute_by_grade === "OK"): ?>
						<tr>
							<td align="center" colspan="7"><b>WASTANI KUU</b></td>
							<td align="center"></td>
							<td align="center"><b><?php echo round($jumla_kuu/$number_of_subjects, 2); ?></b></td>
							<td align="center">
								<?php
								//Assign overall average to variable
								$ovarall_avg = round($jumla_kuu/$number_of_subjects, 2);
								foreach ($grade_system as $gs) {
	          						if($ovarall_avg >= $gs['start_mark'] && $ovarall_avg <= $gs['end_mark']):
	            						echo "<strong>" . $gs['grade'] . "</strong>";
	            					endif;
	            				}
								?>
							</td>
							<td align="center"></td>
						</tr>
						<?php endif; ?>
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td width="50%">
								<b>AMEKUWA WA &nbsp;&nbsp;&nbsp;<u><?php echo $record['rank']; ?></u></b>
							</td>
							<td width="50%">
								<b>KATI YA WANAFUNZI &nbsp;&nbsp;&nbsp;<u><?php echo $no_of_students; ?></u></b>
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td>
								<b>SEHEMU B: TABIA NA MWENENDO WA MWANAFUNZI</b>
							</td>
						<tr>
						<tr>
							<td>
								( A = Nzuri sana, B = Nzuri, C = Wastani, D = Dhaifu)
							</td>
						</tr>
						<tr>
							<td>
								Tabia na mwenendo wa mwanafunzi ni&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> 
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td >
								<b>SEHEMU C: MAHUDHURIO DARASANI&nbsp;&nbsp;&nbsp;</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td>
								<b>SEHEMU D: MALIPO/MADENI ANAYODAIWA</b>
							</td>
						</tr>
					</table>
					<table width="100%" class="table-bordered">
						<tr>
							<td align="center">
								<b>MWAKA</b>
							</td>
							<td align="center">
								<b>AINA YA DENI ANALODAIWA</b>
							</td>
							<td align="center">
								<b>THAMANI YA ANACHODAIWA</b>
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
							<td align="center">
								&nbsp;
							</td>
						</tr>
					</table>
					&nbsp;
					<table width="100%">
						<tr>
							<td><b>MAONI YA MWALIMU WA DARASA</b></td>
						</tr>
						<tr>
							<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
						</tr>
					</table>
					&nbsp;
					<table width="100%">	
						<tr>
							<td width="30%"><b>JINA:</b>&nbsp;&nbsp;&nbsp; <b><?php echo $class_teacher['teacher_names']; ?></b></td>
							<td width="25%"><b>SAHIHI:&nbsp;&nbsp;&nbsp;</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
							<td width="25%"><b>SIMU NAMBA:</b>&nbsp;&nbsp; <?php echo $class_teacher['phone_no']; ?></td>
							<td width="20%"><b>TAREHE:</b>&nbsp;&nbsp;&nbsp;<?php echo date('d-m-Y'); ?></td>
						</tr>
					</table>
					&nbsp;
					<table width="100%">
						<tr>
							<td colspan="4"><b>MAONI YA MKUU WA SHULE:</b></td>
						</tr>
						<tr>
							<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
						</tr>
					</table>
					</br>
					<table width="100%">
						<tr>
							<td width="50%">Shule imefungwa tarehe&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
							<td width="50%">itafunguliwa tarehe&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
						</tr>
					</table>&nbsp;	
					<table width="100%">
						<tr>
							<td width="50%">Sahihi ya mkuu wa shule&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
							<td width="50%">Tarehe&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
						</tr>
					</table>
<div class="break" >
</div>
