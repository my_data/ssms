
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <?php echo form_open('students/disabled'); ?>
            
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
                        <?php echo form_close(); ?>
                          
                
            </div>
          </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- <table id="student_data" class="table table-striped table-hover table-condensed table-bordered"> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Admission#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Class</td>
			<td>Stream</td>
      <td>Disability</td>
      <?php if($this->session->userdata('manage_disabled_students') == 'ok'): ?>
			 <td align="center">Action</td>
      <?php endif; ?>
			<!-- <td>View</td>
			<td>Edit</td> -->
		</tr>
	</thead>
	<tbody>
    <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="7">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Disabled Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>
        <td><?php echo $student['disability']; ?></td>
				<td class="text-center"><a href='<?php echo base_url() . 'students/get_student_profile/' . $student['admission_no'];  ?>' class="btn btn-primary btn-xs" aria-hidden="true" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a>&nbsp;
				<?php if($this->session->userdata('manage_disabled_students') == 'ok'): ?>
          <a href='#' class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editDisabilty<?php echo $student['admission_no']; ?>" data-placement="bottom" title="Edit Student's Disability"><span class="fa fa-pencil"></span></a>&nbsp;
				<?php endif; ?>
        </td>
			</tr>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>




<!-- START MODAL EDIT DISABILITY -->
<?php $y = 1; foreach($students as $row): ?>
    <div class="modal fade" id="editDisabilty<?php echo $row['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit Disability: " . $row['firstname'] . " " . $row['lastname']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("students/update_disability", $attributes);
                ?>        
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="disability" >Disabiltity</label>
                    <div class="col-sm-8">                     
                  <select name="disability" class="form-control" required>
                      <option <?php if($row['disability'] == "Physical Disability"){ echo "selected"; } ?> value="Physical Disability">Physical Disability</option>
                      <option <?php if($row['disability'] == "Blind"){ echo "selected"; } ?> value="Blind">Blind</option>
                      <option  <?php if($row['disability'] == "Deaf"){ echo "selected"; } ?> value="Deaf">Deaf</option>
                      <option  <?php if($row['disability'] == "Mute"){ echo "selected"; } ?> value="Mute">Mute</option>
                    </select>
                    </div>
                </div>        
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
              <input type="hidden" name="admission_no" value="<?php echo $row['admission_no']; ?>" />
              <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
              <button type="submit" name="assign" value="assign" class="btn btn-primary">UPDATE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php endforeach; ?>
<!-- END MODAL EDIT DISABILITY -->