
        <div class="table-responsive">
        <table class="table table-striped table-hover table-condensed table-bordered">
            <thead>
              <tr class="table-header">
                <td align="center">Class</td>
                <!-- <td align="center">Term</td> -->
                <td align="center">Average</td>
                <td align="center">Grade</td>
                <td align="center">Academic Year</td>
                <td align="center">Position</td>
                <td align="center">Out of</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td align="center"><?php echo $record['class_name']; ?></td>
                <!-- <td align="center"><?php echo $record['term_name']; ?></td> -->
                <td align="center"><?php echo $record['average']; ?></td>
                <td align="center"><?php echo $record['grade']; ?></td>
                <td align="center"><?php echo $record['year']; ?></td>
                <td align="center"><?php echo $record['rank']; ?></td>
                <td align="center"><?php if($record['rank'] ==! ""){ echo $no_of_students; } ?></td>
              </tr>
            </tbody>
        </table>
      </div>

        <div class="table-responsive">  
 
        <table class="table table-striped table-hover table-condensed table-bordered">
          <?php if ($mkeka_single == FALSE): ?>
                <tr>
                  <td colspan="8">
                            <?php
                                $message = "There are No Results to display";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
          <thead>
            <tr>
              <th>Subject#</th>
              <th>Monthly</th>
              <th>Mid-Term</th>
              <th>Monthly</th>
              <th>Terminal</th>
              <th>Average</th>
              <th>Grade</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($mkeka_single as $mk): ?>
            <tr>
              <td><?php echo $mk['subject_name']; ?></td>
              <td><?php echo $mk['monthly_one']; ?></td>
              <td><?php echo $mk['midterm']; ?></td>
              <td><?php echo $mk['monthly_two']; ?></td>
              <td><?php echo $mk['terminal']; ?></td>
              <td><?php echo $mk['average']; ?></td>
              <td><?php echo $mk['grade']; ?></td>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
        </table>
</div>  
      </div>

  </div>
</div>