
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">

                <?php echo form_open('students/registered'); ?>


        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
                        <?php echo form_close(); ?>
                            <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
                            <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
                            <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
                            <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
                          </ul> -->
<!-- /btn-group -->
              </div>           
          <?php if($this->session->userdata('register_student') == 'ok'): ?>
            <a href="<?php echo base_url() . 'students/new_student'; ?>" class="btn btn-primary btn-sm">Add Student</a>
          <?php endif; ?>
          </div>
      </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- <table id="student_data" class="table table-striped table-hover table-condensed table-bordered"> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td width="10%">Admission#</td>
			<td width="20%">Firstname</td>
			<td width="20%">Lastname</td>
			<td width="20%">Class</td>
			<td width="10%">Stream</td>
			<?php if($this->session->userdata('update_student') == 'ok' || $this->session->userdata('transfer_student') == 'ok' || $this->session->userdata('suspend_student') == 'ok' || $this->session->userdata('assign_option_subject') == 'ok' || $this->session->userdata('staff_id')): ?>
				<td width="20%">Action
        <?php if($this->session->userdata('register_sudent') == 'ok'): ?>
         <span style="float: right;">
            <a href="<?php echo base_url() . 'students/enrollment'; ?>" class="btn btn-primary btn-xs" title="Students Enrollment" data-placement="bottom"><strong>SE</strong></a>
          </span>
        <?php endif; ?>
      </td>
	<?php endif; ?>
			<!-- <td>View</td>
			<td>Edit</td> -->
		</tr>
	</thead>
	<tbody>
    <?php if ($students == FALSE): ?>
        <tr>
          <td colspan="6">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Registered Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach($students as $student): ?>
			<tr>
				<td><?php echo $student['admission_no']; if($student['stte_out'] == "suspended"){ echo "<span style='color: red;' title='Currently Suspended' data-placement='bottom'><strong>***</strong></span>"; } ?></td>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['stream']; ?></td>
        <?php if($this->session->userdata('update_student') == 'ok' || $this->session->userdata('transfer_student') == 'ok' || $this->session->userdata('suspend_student') == 'ok' || $this->session->userdata('assign_option_subject') == 'ok' || $this->session->userdata('staff_id')): ?>
  				<td><a href='<?php echo base_url() . 'students/get_student_profile/' . $student['admission_no'];  ?>' class="btn btn-primary btn-xs" aria-hidden="true" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a>&nbsp;
  				<?php if($this->session->userdata('update_student') == 'ok'): //Start of Transfer session?>
            <a href='<?php echo base_url() . 'students/edit/' . $student['admission_no']; ?>' class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="Edit Student"><span class="fa fa-pencil"></span></a>&nbsp;
  				<?php endif; //End edit ?>
          <?php if($this->session->userdata('transfer_student') == 'ok'): //Start of Transfer session?>
            <a href="#" class="btn btn-primary btn-xs glyphicon glyphicon-transfer" data-toggle="modal" data-placement="right" data-toggle="tooltip" title="Transfer Student" data-target="#myModalTransfer<?php echo $student['admission_no']; ?>"></a>&nbsp;  
          <?php endif; //End transfer?>
          <?php if($this->session->userdata('suspend_student') == 'ok'): //Start of suspend session?>
            <?php if($student['stte_out'] !== "suspended"): ?>        		
    					<a href='#' class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModalExpell<?php echo $student['admission_no']; ?>" data-toggle="tooltip" data-placement="bottom" title="Suspend Student">S</a>&nbsp;
            <?php else: ?>
              <button class="btn btn-danger btn-xs" disabled><span>S</span></button>&nbsp;
            <?php endif; ?>
              <a href='#' class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModalExpellKimoja<?php echo $student['admission_no']; ?>" data-toggle="tooltip" data-placement="bottom" title="Expell Student">E</a>&nbsp;
          <?php endif; //End of suspend session ?>
          <?php if($this->session->userdata('assign_option_subject') == 'ok'): //Start of Assign Optin Subject?>
            <?php if($student['class_level'] === "O'Level"): ?>
              <a href='' class="btn btn-primary btn-xs" aria-hidden="true" data-target="#myModal<?php echo $student['admission_no']; ?>" data-toggle="modal" data-placement="bottom" title="Assign Option Subject">A</a>&nbsp;
            <?php endif; //if($student['class_level'] === "O'Level"):?>
          <?php endif; //if($this->session->userdata('assign_option_subject') == 'ok'):?>
  				</td>
        <?php endif; ?>
			</tr>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>


<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
















<!-- START ASSIGN OPTIONAL SUBJECT MODAL -->

<?php foreach ($students as $student): ?>

	<div class="modal fade" id="myModal<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Assign Optional Subject To: " . $student['firstname'] . " " . $student['lastname']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form');
            	?>   
              <?php echo form_open("students/assign_optional_subject/".$student['admission_no'], $attributes); ?>         
                  	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="teacher_names" >Subject Name</label>
                    	<div class="col-sm-8">
                        
			
				<select name="subject_id" class="form-control">
					<option value="">--Choose--</option>
					<?php foreach($op_subjects as $optional_subject): ?>
						<option value="<?php echo $optional_subject['subject_id']; ?>"><?php echo $optional_subject['subject_name']; ?></option>
					<?php endforeach; ?>
				</select>
                    	</div>
                  	</div>              
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
            	<input type="hidden" name="class_id" value="<?php echo $student['class_id']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END ASSIGN OPTIONAL SUBJECT MODAL -->

<!-- MODAL FOR TRANSFER STUDENT -->

<?php foreach ($students as $student): ?>

	<div class="modal fade" id="myModalTransfer<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Transfer: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form');
            	?>            
                <?php echo form_open('students/transfer_student', $attributes); ?>
                	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="names" >Student Names: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="student_names" id="student_names" value="<?php echo $student['student_names']; ?>" />
                    	</div>
                  	</div>
                  	<div class="form-group">
                    	<label  class="col-sm-4 control-label" for="School">School Name: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="school_name" id="school" placeholder="School Name"/>
                    	</div>
                 	</div>
                  	<div class="form-group">
                    	<label  class="col-sm-4 control-label" for="transter_letter">Transfer Letter</label>
                    	<div class="col-sm-8">
                        	<select name="transfer_letter" class="form-control">
                        		<option value="">--Choose--</option>
                        		<option value="Available">Available</option>
                        		<option value="NOT Available">NOT Available</option>
                        	</select>
                    	</div>
                 	</div>
                  	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="self_form" >Self form receipt</label>
                    	<div class="col-sm-8">
                        	<select name="self_form_receipt" class="form-control">
                        		<option value="">--Choose--</option>
                        		<option value="Received">Received</option>
                        		<option value="NOT Received">NOT Received</option>
                        	</select>
                    	</div>
                  	</div>
                  	<!-- <div class="form-group">
	                    <label  class="col-sm-4 control-label" for="date">Date: </label>
                    	<div class="col-sm-8">
                        	<input type="hidden" class="form-control" name="date" id="date" value="<?php echo date('Y-m-d'); ?>" />
                    	</div>
                  	</div>
                  	<div class="form-group">
	                    <label  class="col-sm-4 control-label" for="School">Form: </label>
                    	<div class="col-sm-8">
                        	<input type="hidden" class="form-control" name="form" id="form" value="<?php echo $student['class_id']; ?>" />
                    	</div>
	                </div>
	                <div class="form-group">
	                    <label  class="col-sm-4 control-label" name="admission_no" for="admission_no">Admission_no: </label>
                    	<div class="col-sm-8">
                        	<input type="hidden" class="form-control" name="admission_no" id="admission_no" value="<?php echo $student['admission_no']; ?>" />
                    	</div>
	                </div> -->

                  <input type="hidden" class="form-control" name="date" id="date" value="<?php echo date('Y-m-d'); ?>" />
                  <input type="hidden" class="form-control" name="form" id="form" value="<?php echo $student['class_id']; ?>" />
	                <input type="hidden" class="form-control" name="transfer_status" id="transfer_status" value="OUT" />
                  <input type="hidden" class="form-control" name="admission_no" id="admission_no" value="<?php echo $student['admission_no']; ?>" />
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR TRANSFER -->

<!-- START MODAL FOR SUSPEND -->


<?php foreach ($students as $student): ?>

	<div class="modal fade" id="myModalExpell<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Reason For Suspending: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form');
            	?>            
                <?php echo form_open('students/suspend_student/' . $student['admission_no'], $attributes); ?>
                  	<div class="form-group">
                    	<label  class="col-sm-4 control-label" for="reason">Reason: </label>
                    	<div class="col-sm-8">
                    		<textarea class="form-control" name="suspension_reason" cols="42" rows="5"></textarea>
                        	<!-- <input type="text" class="form-control" name="reason" id="school" placeholder="Reason"/> -->
                    	</div>
                 	</div>
                    <div class="form-group">
	                    <label  class="col-sm-4 control-label" for="date">From: </label>
                    	<div class="col-sm-8">
                        	<input type="date" class="form-control" name="from_date" id="date_from" />
                    	</div>
                  	</div>
                  	<div class="form-group">
	                    <label  class="col-sm-4 control-label" for="date">To: </label>
                    	<div class="col-sm-8">
                        	<input type="date" class="form-control" name="to_date" id="date_to" />
                    	</div>
                  	</div>
               	    <!-- <div class="form-group">
	                    <label  class="col-sm-4 control-label" name="admission_no" for="admission_no">Admission_no: </label>
                    	<div class="col-sm-8">
                        	
                    	</div>
	                </div> -->
	                <input type="hidden" class="form-control" name="admission_no" id="admission_no" value="<?php echo $student['admission_no']; ?>" />
	                <input type="hidden" class="form-control" name="form" id="form" value="<?php echo $student['class_id']; ?>" />
	                <input type="hidden" class="form-control" name="transfer_status" id="transfer_status" value="OUT" />
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR SUSPEND -->




<!-- START MODAL FOR EXPELL -->


<?php foreach ($students as $student): ?>

  <div class="modal fade" id="myModalExpellKimoja<?php echo $student['admission_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Reason For Expelling: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
              <?php
                $attributes = array('class' => 'form-horizontal', 'role' => 'form');
              ?>            
                <?php echo form_open('students/expel_student/' . $student['admission_no'], $attributes); ?>
                    <div class="form-group">
                      <label  class="col-sm-4 control-label" for="reason">Reason: </label>
                      <div class="col-sm-8">
                        <textarea class="form-control" name="suspension_reason" cols="42" rows="5"></textarea>
                          <!-- <input type="text" class="form-control" name="reason" id="school" placeholder="Reason"/> -->
                      </div>
                  </div>
                    <div class="form-group">
                      <label  class="col-sm-4 control-label" for="date">From: </label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" name="from_date" id="date_from" />
                      </div>
                    </div>
                  <input type="hidden" class="form-control" name="admission_no" id="admission_no" value="<?php echo $student['admission_no']; ?>" />
                  <input type="hidden" class="form-control" name="form" id="form" value="<?php echo $student['class_id']; ?>" />
                  <input type="hidden" class="form-control" name="transfer_status" id="transfer_status" value="OUT" />
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>

<!-- END MODAL FOR EXPELL -->
