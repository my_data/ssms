
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('students/discipline_records'); ?>

        <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>
        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
        </ul> -->
    <!-- /btn-group -->
</div>
<?php if($this->session->userdata('manage_indiscipline_records') == 'ok'): ?>
  <a href="<?php echo base_url() . 'discipline/add_record'; ?>" class="btn btn-primary btn-sm">Add Record</a>
<?php endif; ?>
</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

      <div class="table-responsive">        
        <table class="table table-hover table-condensed table-bordered">
          <thead>
            <tr class="table-header">
              <td style="width:20%; text-align:center;">Date</td>
              <td style="width:40%; text-align:center;">Description</td>
              <td style="width:40%; text-align:center;">Decision</td>
              <?php if($this->session->userdata('manage_indiscipline_records') == 'ok'): ?>
                <td style="width:2%; text-align:center;">Edit</td>
              <?php endif; ?>
            </tr>
          </thead>
          <tbody>
            <?php if ($discipline_records == FALSE): ?>
              <tr>
                <td colspan="5">
                          <?php
                              $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No discipline records";
                              echo $message;
                          ?>
                      </td>
              </tr>
            <?php else: ?>
            <?php foreach($discipline_records as $discipline_record): ?>
            <tr>
              <td><?php echo $discipline_record['date']; ?></td>
              <td><?php echo $discipline_record['description']; ?></td>
              <td><?php echo $discipline_record['decision']; ?></td>
            <?php if($this->session->userdata('manage_indiscipline_records') == 'ok'): ?>
              <td align="center">
                <a href="<?php echo base_url(); ?>students/update_indiscipline_record/<?php echo $discipline_record['id']; ?>" class="btn btn-primary btn-xs" ><span class="fa fa-pencil"></span></a>
              </td>
            <?php endif; ?>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
          </tbody>
        </table>
        <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
      </div>  
    </div>

  </div>
</div>

<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'admin/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div>
</div>
</div>

