
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">

                <?php echo form_open('students/subject_results/'.$teacher_id); ?>
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
        </div>
                        <?php echo form_close(); ?>

            </div>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered table-responsive">
  <thead>
        <tr class="table-header">
          <td align="center" width="25%">Class</td>
          <td align="center" width="25%">Stream</td>
          <td align="center" width="25%">Subject Name</td>
          <td align="center" width="25%">Action</td>
        </tr>
  </thead>
  <tbody>
    <?php if ($myclasses == FALSE): ?>
      <tr>
        <td colspan="4">
              <?php
                  $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "You have not been assigned to teach any class";
                  echo $message;
              ?>
          </td>
      </tr>
    <?php else: ?>
    <?php foreach($myclasses as $myclass): ?>
      <tr>
        <td align="center"><?php echo $myclass['class_name']; ?></td>
        <td align="center"><?php echo $myclass['stream']; ?></td>
        <td align="center"><?php echo $myclass['subject_name']; ?></td>        
        <td align="center">
          <a href="<?php echo base_url() . 'students/scoresheet/' . $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create Scoresheet"></span></a>&nbsp;
          <a href="<?php echo base_url()  .'students/edit_results/' . $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit Scoresheet"></span></a>
          &nbsp;
          <a href="<?php echo base_url() .'students/student_results/'. $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" class="btn btn-xs btn-primary fa fa-clone" data-toggle="tooltip" data-placement="bottom" title="View Results"></a>
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>