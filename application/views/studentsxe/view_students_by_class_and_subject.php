
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
                <?php echo form_open('teacher/myclasses/'.$class_stream_id.'/'.$subject_id); ?>
                <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                      <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?>
                    </div>
              </div>
            </div>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- <div class="class_stream_id_class" data-target="<?php echo $class_stream_id; ?>">
</div>
<div class="subject_id_class" data-target="<?php echo $subject_id; ?>">
</div> -->

<!-- <table id="class_subject_students" class="table table-striped table-hover table-condensed"> -->
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Admission#</td>
      <td>Firstname</td>
      <td>Lastname</td>
      <td align="center">Action</td>
    </tr>
  </thead>
 <tbody>
    <?php if ($mystudents == FALSE): ?>
      <tr>
        <td colspan="4">
              <?php
                  $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students in taking this subject";
                  echo $message;
              ?>
          </td>
      </tr>
    <?php else: ?>
    <?php foreach($mystudents as $mystudent): ?>
      <tr>
        <td><?php echo $mystudent['admission_no']; ?></td>
        <!-- <td><?php echo $mystudent['names']; ?></td> -->
        <td><?php echo $mystudent['firstname']; ?></td>
        <td><?php echo $mystudent['lastname']; ?></td>
        <td align="center">
          <a href="<?php echo base_url() . 'students/get_student_profile/' . $mystudent['admission_no']; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-eye"></span></a> 
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes/classes_timetable'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>