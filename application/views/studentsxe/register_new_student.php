
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline">
        <div class="form-group has-feedback no-margin">
        	<!--ADDED FOR AJAX -->
		<input type="text" value="" name="search" id="search"/>
		<br />
		<div class="result"></div>
		<!--ADDED FOR AJAX -->
    	</div>
	</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<?php
	$date = date('Y-m-d');
	$year = explode('-', $date)[0];
?>

<?php echo form_open('students/new_student'); ?>	
    <!-- <div class="disabled box">You have selected <strong>red radio button</strong> so i am here</div>
	<center><h4>Student Personal Information:</h4></center> -->
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_no">Admission# :</label>
		<div class="col-sm-10">
			<div id="successMessage" class="success_message_color">
				<?php 
					if($this->session->flashdata('success_message')){
						echo $this->session->flashdata('success_message');
					}
				?>
			</div>
			<div id="errorMessage" class="error_message_color">
				<?php
					if($this->session->flashdata('error_message')){
						echo $this->session->flashdata('error_message');
					}
				?>
			</div>
			<div id="warningMessage" class="error_message_color">
				<?php
					if($this->session->flashdata('exist_message')){
						echo $this->session->flashdata('exist_message');
					}
				?>
			</div>
			<input type="text" name="admission_no" class="form-control" value="<?php echo set_value('admission_no'); ?>" required/>
			<div class="error_message_color">
				<?php echo form_error('admission_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	


	<div class="form-group">
		<label class="col-sm-2 control-label" for="middlename">Middle Name :</label>
		<div class="col-sm-10">
			<input type="text" name="middlename" class="form-control" value="<?php if($ssd['middlename']){ echo $ssd['middlename']; }else{ echo set_value('middlename'); } ?>" />
			<div class="error_message_color">
				<?php echo form_error('middlename'); ?>
			</div>
		</div>
	</div>



	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
		<div class="col-sm-10">
			<input type="text" name="lastname" class="form-control" value="<?php if($ssd['firstname']){ echo $ssd['lastname']; }else{ echo set_value('lastname'); } ?>" />
			<div class="error_message_color">
				<?php echo form_error('lastname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="dob">Date Of Birth :</label>
		<div class="col-sm-5">
			<input type="date" name="dob" value="<?php if($ssd['dob']){ echo $ssd['dob']; }else{ echo set_value('dob'); } ?>" class="form-control" />
			<div class="error_message_color">
				<?php echo form_error('dob'); ?>
			</div>
		</div>
		<label class="col-sm-1 control-label" for="disability">Disabled :</label>
		<div class="col-sm-2">
			<input type="radio" name="disabled" value="disabled" /> Yes
			<input type="radio" name="disabled" value="No" checked="true"/> No
			<div class="error_message_color">
				<?php echo form_error('disabled'); ?>
			</div>
		</div>
		<div class="col-sm-2 disabled box">
			<input type="text" placeholder="Add Disabililty" name="disability" class="form-control" value="<?php echo set_value('disability'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('disability'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="tribe_id">Tribe :</label>
		<div class="col-sm-4">
			<select name="tribe_id" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($tribes as $tribe): ?>
					<option value="<?php echo $tribe['tribe_id']; ?>" <?php echo set_select('tribe_id', ''.$tribe["tribe_id"].'', $this->input->post('tribe_id')); ?>><?php echo $tribe['tribe_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('tribe_id'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<label class="col-sm-2 control-label" for="religion_id">Religion :</label>
		<div class="col-sm-4">
			<select name="religion_id" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($religions as $religion): ?>
					<option value="<?php echo $religion['religion_id']; ?>" <?php echo set_select('religion_id', ''.$religion["religion_id"].'', $this->input->post('religion_id')); ?>><?php echo $religion['religion_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('religion_id'); ?>
			</div>
		</div>
	</div>
	<br/>
		<center><h4>Student Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="box">P.O.Box :</label>
		<div class="col-sm-4">
			<input type="text" name="box" class="form-control" value="<?php echo set_value('box'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('box'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<label class="col-sm-2 control-label" for="region">Region :</label>
		<div class="col-sm-4 " >
			<select name="region" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option value="<?php echo $region['region_name']; ?>" <?php echo  set_select('region', ''.$region["region_name"].'', $this->input->post('region')); ?>><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('region'); ?>
			</div>
		</div>
	</div>
	<br/>
		<center><h4>Other Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_date">Admission Date :</label>
		<div class="col-sm-4 " >
			<input type="date" class="form-control" name="admission_date" value="<?php echo date('Y-m-d'); ?>" readonly="true" />
			<div class="error_message_color">
				<?php echo form_error('admission_date'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<!-- <label class="col-sm-2 control-label" for="">Academic Year :</label>
		<div class="col-sm-4 " >
			<input type="text" class="form-control" value="<?php echo $year; ?>" readonly="true" />
		</div>
	</div> -->
	<label class="col-sm-2 control-label" for="transferred">Transfered :</label>
		<div class="col-sm-4 " >
			<input type="radio" name="transferred" value="transferred_in" /> Yes
			<input type="radio" name="transferred" value="selected" checked="true" /> No
			<div class="error_message_color">
				<?php echo form_error('transferred'); ?>
			</div>
		</div>
	</div>
	<br/><br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="former_school">Former School :</label>
		<div class="col-sm-10 " >
			<input type="text" name="former_school" class="form-control" value="<?php if($ssd['former_school']){ echo $ssd['former_school']; }else{ echo set_value('former_school'); } ?>" />
			<div class="error_message_color">
				<?php echo form_error('former_school'); ?>
			</div>
		</div>
	<br/>
		<center><h4>Allocated To:</h4></center>
	<br/>
	<div class="form-group"> 
		<label class="col-sm-2 control-label" for="dorm_id">Dormitory Name :</label>
		<div class="col-sm-10" >
			<select name="dorm_id" class="form-control" >
					<option value="">--Choose--</option>
					<?php foreach($dormitories as $dormitory): ?>
						<option value="<?php echo $dormitory['dorm_id']; ?>" <?php echo  set_select('dorm_id', ''.$dormitory["dorm_id"].'', $this->input->post('dorm_id')); ?>><?php echo $dormitory['dorm_name']; ?></option>
					<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('dorm_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group"> 
		<label class="col-sm-2 control-label" for="">Class Name :</label>
		<div class="col-sm-10" >
			<select name="class_stream_id" class="form-control" >
					<option value="">--Choose--</option>
					<?php foreach($classes as $class): ?>
						<option value="<?php echo $class['class_stream_id'] . "-" . $class['class_id']; ?>" <?php echo  set_select('class_stream_id', ''.$class['class_stream_id'] . "-" . $class['class_id'].'', $this->input->post('class_stream_id')); ?>><?php echo $class['class_name'] . " " . $class['stream']; ?></option>
					<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('class_stream_id'); ?>
			</div>
		</div>
	</div>
	<br/>
		<center><h4>Parent Personal Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_firstname">Firstname:</label>
		<div class="col-sm-10">
			<input type="text" name="g_firstname" class="form-control" value="<?php echo set_value('g_firstname'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_firstname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_middlename">Middlename :</label>
		<div class="col-sm-10">
			<input type="text" name="g_middlename" class="form-control" value="<?php echo set_value('g_middlename'); ?>"/>
			<div class="error_message_color">
				<?php echo form_error('g_middlename'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_lastname">Lastname :</label>
		<div class="col-sm-10">
			<input type="text" name="g_lastname" class="form-control" value="<?php echo set_value('g_lastname'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_lastname'); ?>
			</div>
		</div>
	</div>



	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gender">Gender :</label>
		<div class="col-sm-10">
			<input type="radio" name="gender" value="Male" checked="true" /> Male
			<input type="radio" name="gender" value="Female" /> Female
			<div class="error_message_color">
				<?php echo form_error('gender'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="occupation">Occupation :</label>
		<div class="col-sm-10">
			<input type="text" name="occupation" class="form-control" value="<?php echo set_value('occupation'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('occupation'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="rel_type">Relationship Type:</label>
		<div class="col-sm-10">
			<select name="rel_type" class="form-control" >
				<option value="">--Choose--</option>
				<option <?php echo set_select('rel_type', 'Father', $this->input->post('rel_type')); ?> value="Father">Father</option>
				<option <?php echo set_select('rel_type', 'Mother', $this->input->post('rel_type')); ?> value="Mother">Mother</option>
				<option <?php echo set_select('rel_type', 'Guardian', $this->input->post('rel_type')); ?> value="Guardian">Guardian</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('rel_type'); ?>
			</div>
		</div>
	</div>
	<br/>
		<center><h4>Parent Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="email">Email :</label>
		<div class="col-sm-10">
			<input type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('email'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="phone_no">Phone No. :</label>
		<div class="col-sm-10">
			<input type="text" name="phone_no" class="form-control"  value="<?php echo set_value('phone_no'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('phone_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="p_address">Permanent Address :</label>
		<div class="col-sm-10">
			<input type="text" name="p_address" class="form-control" value="<?php echo set_value('p_address'); ?>" /> 
			<div class="error_message_color">
				<?php echo form_error('p_address'); ?>
			</div>
		</div>
	</div>	
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="region">Region:</label>
		<div class="col-sm-10">
			<select name="p_region" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option value="<?php echo $region['region_name']; ?>" <?php echo  set_select('p_region', ''.$region["region_name"].'', $this->input->post('p_region')); ?>><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('p_region'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<br/>
	<div class="form-group">
		<input type="submit" class="form-control tn btn-primary" name="register_student" value="Register" />
	</div>
	<br/>
	
</form>


