
<div class="col-md-9">

  <!-- <div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div> -->


        <div class="table_responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
           
            <thead>
              <tr class="table-header">
                <td  style="width:30%; text-align:center;">Title</td>
                <td  style="width:70%; text-align:center;">Description
              <?php if($this->session->userdata('permission') === "edit_student"): ?>
                <span style="float: right;"><a href="<?php echo base_url('students/edit/' .$student_profile['admission_no']);?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a></span>
              <?php endif; ?>
                </td>
              </tr>
            </thead>

            <tbody>
            <?php if ($student_profile == FALSE): ?>
                <tr>
                  <td colspan="2">
                            <?php
                                $message = "There are currently Records for this particular student";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
              <tr>
                <td><b>&nbsp;Student Names</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['student_names']; ?></td>
              </tr>
              <tr>
                <td><b>&nbsp;Date of birth</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['dob']; ?></td>
              </tr>
              <tr>
                <td><b>&nbsp;Current Dormitory</b></td>
                <td>&nbsp;&nbsp;<?php echo $current_dormitory['dorm_name']; ?></td>
              </tr>
              <tr>
                <td><b>&nbsp;Current Class</b></td>
                <td>&nbsp;&nbsp;<?php echo $current_class['class_name'] . " " . $current_class['stream']; ?></td>
              </tr>
              <tr>
              <tr>
                <td><b>&nbsp;Status</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['status']; ?></td>
              </tr>
              <tr>
                <td><b>&nbsp;Guardian Names</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['guardian_names']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Gender</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['gender']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Guardian Phone No.</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['phone_no']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Relationship Type</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['rel_type']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Occupation</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['occupation']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Email</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['email']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;<b>Guardian Address</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['p_box'] . ", " . $student_profile['p_region']; ?></td>
              </tr>
              <!-- <tr>
                <td><b>&nbsp;Region</b></td>
                <td>&nbsp;&nbsp;<?php echo $student_profile['p_region']; ?></td>
              </tr> -->
            </tbody>
          <?php endif; ?>
        </table>
      </div>
      </div>


    </div>
  </div>
</div>
