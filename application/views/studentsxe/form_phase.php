
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline">
        <div class="form-group has-feedback no-margin">
        	<div class="input-group">
    			<h6 id="status">Phase 1 of 4</h6>
			</div>
        </div>
        <progress id="progressBar" value="0" max="100" style="width:350px;"></progress>
        	<!--ADDED FOR AJAX -->
		<!-- <input type="text" value="" name="search" id="search"/>
		<br /> -->
		<!-- <div class="result"></div> -->
		<!--ADDED FOR AJAX -->
 
    	
	</div>
</div>

<?php
	$date = date('Y-m-d');
	$year = explode('-', $date)[0];
?>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div id="successMessage" class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('error_validation')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_validation'); ?>
        </div>
    <?php endif; ?>
</div>

<?php $attributes = array('id' => 'student_registration_form', 'onSubmit' => 'return false'); ?>
<?php echo form_open('students/new_student', $attributes); ?>	
    <!-- <div class="disabled box">You have selected <strong>red radio button</strong> so i am here</div>-->


<div id="personalInfoPhase">
	<center><h4>Student Personal Information:</h4></center> 
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_no">Admission# :</label>
		<div class="col-sm-10">
			<input id="admission_no" type="number" name="admission_no" class="form-control" value="<?php echo set_value('admission_no'); ?>" required/>
			<div class="error_message_color">
				<?php echo form_error('admission_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
		<div class="col-sm-10">
			<input id="firstname" type="text" name="firstname" class="form-control" value="<?php if($ssd['firstname']){ echo $ssd['firstname']; }else{ echo set_value('firstname'); } ?>" required/>
			<div class="error_message_color">
				<?php echo form_error('firstname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
		<div class="col-sm-10">
			<input id="lastname" type="text" name="lastname" class="form-control" value="<?php if($ssd['firstname']){ echo $ssd['lastname']; }else{ echo set_value('lastname'); } ?>" required/>
			<div class="error_message_color">
				<?php echo form_error('lastname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="dob">Date Of Birth :</label>
		<div class="col-sm-10">
			<input id="dob" type="date" name="dob" value="<?php if($ssd['dob']){ echo $ssd['dob']; }else{ echo set_value('dob'); } ?>" class="form-control" required/>
			<div class="error_message_color">
				<?php echo form_error('dob'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="tribe_id">Tribe :</label>
		<div class="col-sm-10">
			<select id="tribe" name="tribe_id" class="form-control" required>
				<option value="">--Choose--</option>
				<?php foreach($tribes as $tribe): ?>
					<option value="<?php echo $tribe['tribe_id']; ?>" <?php echo set_select('tribe_id', ''.$tribe["tribe_id"].'', $this->input->post('tribe_id')); ?>><?php echo $tribe['tribe_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('tribe_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="religion_id">Religion :</label>
		<div class="col-sm-10">
			<select id="religion" name="religion_id" class="form-control" required>
				<option value="">--Choose--</option>
				<?php foreach($religions as $religion): ?>
					<option value="<?php echo $religion['religion_id']; ?>" <?php echo set_select('religion_id', ''.$religion["religion_id"].'', $this->input->post('religion_id')); ?>><?php echo $religion['religion_name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('religion_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-sm-2">
		</div>
		<div class="col-sm-10">
			<button onClick="processPersonalInfo()" class="form-control btn btn-primary">Continue</button>
		</div>
	</div>
</div><!-- END OF personalInfoPhase-->

<div id="contactInfoPhase">
		<center><h4>Student Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="box">P.O.Box :</label>
		<div class="col-sm-4">
			<input id="box" type="text" name="box" class="form-control" value="<?php echo set_value('box'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('box'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<label class="col-sm-2 control-label" for="region">Region :</label>
		<div class="col-sm-4 " >
			<select id="region" name="region" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option value="<?php echo $region['region_name']; ?>" <?php echo  set_select('region', ''.$region["region_name"].'', $this->input->post('region')); ?>><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('region'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-sm-2">
		</div>
		<div class="col-sm-5">
			<button onClick="backToPersonalInfo()" class="form-control btn btn-primary">Previous</button>
		</div>
		<div class="col-sm-5">
			<button onClick="processContactInfo()" class="form-control btn btn-primary">Continue</button>
		</div>
	</div>
	<br/>
</div><!-- END OF contactInfoPhase -->

<div id="admissionInfoPhase">
		<center><h4>Other Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="disability">Disabled :</label>
		<div class="col-sm-4">
			<input id="disabled" type="radio" name="disabled" value="disabled" onClick="disabledSelectElement('disabled', 'disability_values')" /> Yes
			<input id="hideDisability" type="radio" name="disabled" value="No" onClick="disabledSelectElement('hideDisability', 'disability_values')" checked="true"/> No
			<div class="error_message_color">
				<?php echo form_error('disabled'); ?>
			</div>
		</div>
		<!-- <div class="col-sm-6 disabled box">
			<input id="disability" type="text" placeholder="Add Disabililty" name="disability" class="form-control" value="<?php echo set_value('disability'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('disability'); ?>
			</div>
		</div> -->
		<!-- <div class="col-sm-6 disabled box"> -->
		<div class="col-sm-6" id="disability_values">
			<select id="disability" name="disability" class="form-control" >
				<option value="">--Choose--</option>
				<option value="Deaf">Deaf</option>
				<option value="Blind">Blind</option>
				<option value="Mute">Mute</option>
				<option value="Physical Disability">Physical Disability</option>		
			</select>
			<div class="error_message_color">
				<?php echo form_error('disability'); ?>
			</div>
		</div>
	</div>
	<br/><br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="admission_date">Admission Date :</label>
		<div class="col-sm-4 " >
			<input id="admission_date" type="date" class="form-control" name="admission_date" value="<?php echo date('Y-m-d'); ?>" readonly="true" />
			<div class="error_message_color">
				<?php echo form_error('admission_date'); ?>
			</div>
		</div>
	<!-- </div>
	<br/><br/>
	<div class="form-group"> -->
		<!-- <label class="col-sm-2 control-label" for="">Academic Year :</label>
		<div class="col-sm-4 " >
			<input type="text" class="form-control" value="<?php echo $year; ?>" readonly="true" />
		</div>
	</div> -->
	<label class="col-sm-2 control-label" for="transferred">Transfered :</label>
		<div class="col-sm-4 " >
			<input id="transferred" type="radio" name="transferred" value="transferred_in" /> Yes
			<input id="transferred" type="radio" name="transferred" value="selected" checked="true" /> No
			<div class="error_message_color">
				<?php echo form_error('transferred'); ?>
			</div>
		</div>
	</div>
	<br/><br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="former_school">Former School :</label>
		<div class="col-sm-10 " >
			<input id="former_school" type="text" name="former_school" class="form-control" value="<?php if($ssd['former_school']){ echo $ssd['former_school']; }else{ echo set_value('former_school'); } ?>" />
			<div class="error_message_color">
				<?php echo form_error('former_school'); ?>
			</div>
		</div>
	</div>
	<br/><br/><br/>
	<div class="form-group"> 
		<label class="col-sm-2 control-label" for="dorm_id">Dormitory Name :</label>
		<div class="col-sm-10" >
			<select id="dormitory" name="dorm_id" class="form-control" >
					<option value="">--Choose--</option>
					<?php foreach($dormitories as $dormitory): ?>
						<option value="<?php echo $dormitory['dorm_id']; ?>" <?php echo  set_select('dorm_id', ''.$dormitory["dorm_id"].'', $this->input->post('dorm_id')); ?>><?php echo $dormitory['dorm_name']; ?></option>
					<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('dorm_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group"> 
		<label class="col-sm-2 control-label" for="">Class Name :</label>
		<div class="col-sm-10" >
			<select id="class_" name="class_stream_id" class="form-control" >
					<option value="">--Choose--</option>
					<?php foreach($classes as $class): ?>
						<option value="<?php echo $class['class_stream_id'] . "-" . $class['class_id']; ?>" <?php echo  set_select('class_stream_id', ''.$class['class_stream_id'] . "-" . $class['class_id'].'', $this->input->post('class_stream_id')); ?>><?php echo $class['class_name'] . " " . $class['stream']; ?></option>
					<?php endforeach; ?>
			</select>
			<div class="error_message_color">
				<?php echo form_error('class_stream_id'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-sm-2">
		</div>
		<div class="col-sm-5">
			<button onClick="backToContactInfo()" class="form-control btn btn-primary">Previous</button>
		</div>
		<div class="col-sm-5">
			<button onClick="processAdmissionInfo()" class="form-control btn btn-primary">Continue</button>
		</div>
	</div>
	<br/><br/>

</div> <!-- END OF admissionInfoPhase -->

<div id="first_guardian_phase">
		<center><h4>Parent Personal Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_firstname">Firstname:</label>
		<div class="col-sm-10">
			<input id="fg_firstname" type="text" name="g_firstname" class="form-control" value="<?php echo set_value('g_firstname'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_firstname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_middlename">Middlename :</label>
		<div class="col-sm-10">
			<input id="fg_middlename" type="text" name="g_middlename" class="form-control" value="<?php echo set_value('g_middlename'); ?>"/>
			<div class="error_message_color">
				<?php echo form_error('g_middlename'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="g_lastname">Lastname :</label>
		<div class="col-sm-10">
			<input id="fg_lastname" type="text" name="g_lastname" class="form-control" value="<?php echo set_value('g_lastname'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('g_lastname'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gender">Gender :</label>
		<div class="col-sm-10">
			<input id="fg_gender" type="radio" name="gender" value="Male" checked="true" /> Male
			<input id="fg_gender" type="radio" name="gender" value="Female" /> Female
			<div class="error_message_color">
				<?php echo form_error('gender'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="occupation">Occupation :</label>
		<div class="col-sm-10">
			<input id="fg_occupation" type="text" name="occupation" class="form-control" value="<?php echo set_value('occupation'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('occupation'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="rel_type">Relationship Type:</label>
		<div class="col-sm-10">
			<select id="fg_relationship" name="rel_type" class="form-control" >
				<option value="">--Choose--</option>
				<option <?php echo set_select('rel_type', 'Father', $this->input->post('rel_type')); ?> value="Father">Father</option>
				<option <?php echo set_select('rel_type', 'Mother', $this->input->post('rel_type')); ?> value="Mother">Mother</option>
				<option <?php echo set_select('rel_type', 'Guardian', $this->input->post('rel_type')); ?> value="Guardian">Guardian</option>
			</select>
			<div class="error_message_color">
				<?php echo form_error('rel_type'); ?>
			</div>
		</div>
	</div>
	<br/>
		<center><h4>Parent Contact Information:</h4></center>
	<br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="email">Email :</label>
		<div class="col-sm-10">
			<input id="fg_email" type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('email'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="phone_no">Phone No. :</label>
		<div class="col-sm-10">
			<input id="fg_phone" type="text" name="phone_no" class="form-control"  value="<?php echo set_value('phone_no'); ?>" />
			<div class="error_message_color">
				<?php echo form_error('phone_no'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="p_address">Address:</label>
		<div class="col-sm-10">
			<input id="fg_address" type="text" name="p_address" class="form-control" value="<?php echo set_value('p_address'); ?>" /> 
			<div class="error_message_color">
				<?php echo form_error('p_address'); ?>
			</div>
		</div>
	</div>	
	<br/><br/><br/>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="region">Region:</label>
		<div class="col-sm-10">
			<select id="fg_region" name="p_region" class="form-control" >
				<option value="">--Choose--</option>
				<?php foreach($regions as $region): ?>
					<option value="<?php echo $region['region_name']; ?>" <?php echo  set_select('p_region', ''.$region["region_name"].'', $this->input->post('p_region')); ?>><?php echo $region['region_name']; ?></option>
				<?php endforeach; ?>			
			</select>
			<div class="error_message_color">
				<?php echo form_error('p_region'); ?>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="form-group">
		<div class="col-sm-2">
		</div>
		<div class="col-sm-5">
			<button onClick="backToAdmissionPhase()" class="form-control btn btn-primary">Previous</button>
		</div>
		<div class="col-sm-5">
			<button onClick="process_first_guardian_phase()" class="form-control btn btn-primary">Continue</button>
		</div>
	</div>
	<br/><br/>
</div><!-- END first_guardian_phase -->

<div id="review_all_data">
	<table class="table table-condensed table-striped table-bordered table-hover">
		<thead>
			<tr class="table-header">
				<td width="40%">Title</td>
				<td width="60%">Description</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><label>Admission# </label></td>
				<td><span id="show_admission_no"></span></td>
			</tr>
			<tr>
				<td><label>Student Names </label></td>
				<td><span id="show_firstname"></span>&nbsp;&nbsp;<span id="show_lastname"></span></td>
			</tr>
			<tr>
				<td><label>Date Of Birth </label></td>
				<td><span id="show_dob"></span></td>
			</tr>
			<tr>
				<td><label>Tribe </label></td>
				<td><span id="show_tribe"></span></td>
			</tr>
			<tr>
				<td><label>Religion </label></td>
				<td><span id="show_religion"></span></td>
			</tr>
			<tr>
				<td><label>Address </label></td>
				<td><span id="show_box"></span>,&nbsp;&nbsp;<span id="show_region"></span></td>
			</tr>
			<tr>
				<td><label>Disabled </label></td>
				<td><span id="show_disabled"></span></td>
			</tr>		
			<tr>
				<td><label>Disability </label></td>
				<td><span id="show_disability"></span></td>
			</tr>
			<tr>
				<td><label>Admission Date </label></td>
				<td><span id="show_admission_date"></span></td>
			</tr>
			<tr>
				<td><label>Transferred </label></td>
				<td><span id="show_transferred"></span></td>
			</tr>
			<tr>
				<td><label>Former School </label></td>
				<td><span id="show_former_school"></span></td>
			</tr>		
			<tr>
				<td><label>Class </label></td>
				<td><span id="show_class_"></span></td>
			</tr>
			<tr>
				<td><label>Dormitory </label></td>
				<td><span id="show_dormitory"></span></td>
			</tr>
			<tr>
				<td><label>Guardian Names </label></td>
				<td><span id="show_fg_firstname"></span>&nbsp;&nbsp;<span id="show_fg_middlename"></span>&nbsp;&nbsp;<span id="show_fg_lastname"></span></td>
			</tr>
			<tr>
				<td><label>Gender </label></td>
				<td><span id="show_fg_gender"></span></td>
			</tr>		
			<tr>
				<td><label>Occupation </label></td>
				<td><span id="show_fg_occupation"></span></td>
			</tr>

			<tr>
				<td><label>Relationhip Type </label></td>
				<td><span id="show_fg_relationship"></span></td>
			</tr>
			<tr>
				<td><label>Phone# </label></td>
				<td><span id="show_fg_phone"></span></td>
			</tr>
			<tr>
				<td><label>Email </label></td>
				<td><span id="show_fg_email"></span></td>
			</tr>
			<tr>
				<td><label>Address </label></td>
				<td><span id="show_fg_address"></span>,&nbsp;&nbsp;<span id="show_fg_region"></span></td>
			</tr>
		</tbody>
	</table>
	<div class="form-group">
		<input type="submit" onClick="submitData()" class="form-control btn btn-primary" name="register_student" value="Register" />
	</div>
	<br/>
</div>
	
<?php echo form_close(); ?>


