
	<div class="white-area-content">
			<div class="db-header clearfix">

	<div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline text-right"> 

	        <div class="form-group has-feedback no-margin">

<?php echo form_open('students/transferred_out'); ?>
	

	<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>
							        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
						          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
						          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
						          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
						        </ul> -->
<!-- /btn-group -->
				</div>
			</div>

		<!-- <a href="<?php echo base_url() . 'admin/register_student'; ?>" class="btn btn-primary btn-sm">Add Student</a> -->

	</div>
<!-- </div> -->

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<!-- <table id="transferred_out_students" class="table table-striped table-hover table-condensed"> -->
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Firstname</td>
			<td>Lastname</td>
			<td>School From</td>
			<!-- <?php
				if($status == 'transferred_out'){
					echo "<th>School From</th>";
				}
				else if($status == 'transferred_out'){
					echo "<th>School To</th>"; 
				}
			?> -->
			<td>Status</td>
			<td>Class Name</td>
			<td>Date of transfer</td>
			<td>Transfer letter</td>
			<td>Self form receipt</td>
			<td align="center">Action</td>
		</tr>
	</thead>
	<tbody>
	<?php if ($students == FALSE): ?>
        <tr>
          <td colspan="9">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Transferred OUT Students";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach ($students as $student): ?>
			<tr>
				<td><?php echo $student['firstname']; ?></td>
				<td><?php echo $student['lastname']; ?></td>
				<td><?php echo $student['school']; ?></td>
				<td><?php echo $student['transfer_status']; ?></td>
				<td><?php echo $student['class_name']; ?></td>
				<td><?php echo $student['date_of_transfer']; ?></td>
				<td><?php echo $student['transfer_letter']; ?></td>
				<td><?php echo $student['self_form_receipt']; ?></td>
				<td align="center">
				<?php echo form_open('students/restore_student/'.$student['admission_no']); ?>
					<a href='<?php echo base_url() . 'students/get_student_profile/' . $student['admission_no']; ?>' title="View More Details" data-placement="bottom" data-target="tooltip" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>&nbsp;
						<input type="hidden" name="transfer" value="transfer" />
					<?php if($this->session->userdata('restore_transferred_out_students') == 'ok'): ?>
						<button type="submit" name="restore_student" class="btn btn-primary btn-xs" data-placement="bottom" data-target="tooltip" title="Restore student" onClick="return confirm('Are you sure you want to restore this student?');" /><span class="fa fa-undo"></span></button>
					<?php endif; ?>
					<?php echo form_close(); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<!-- </table> -->
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students/transferred_out'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>