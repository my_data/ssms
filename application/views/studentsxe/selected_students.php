
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <div class="form-group has-feedback no-margin">
              <div class="input-group">
              <?php //if($this->session->userdata('registrar_role_active') === "on" || $this->session->userdata('second_master_role_active') === "on"): ?>
                <?php echo form_open('students/selected/'.$form); ?>
              <?php //endif; ?>
                <input type="text" class="form-control input-sm" name="search_student" placeholder="Search ..." id="form-search-input" />
                  <div class="input-group-btn">
                      <input type="hidden" id="search_type" value="0">
                          <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        <?php echo form_close(); ?>
                    </div>
              </div>
            </div>
            <a href="<?php echo base_url() . 'students/selected/form_one'; ?>" class="btn btn-primary btn-sm">Form One</a>
            <a href="<?php echo base_url() . 'students/selected/form_five'; ?>" class="btn btn-primary btn-sm">Form Five</a>
            <?php if($this->session->userdata('manage_selected_students') == 'ok'): ?>
              <a href="<?php echo base_url() . 'students/add_selected'; ?>" class="btn btn-primary btn-sm">Add Selected Student</a>
            <?php endif; ?>
          </div>
      </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>STATUS</td>
    <?php foreach($count_reported as $cr): ?>
      <td>
      <?php
        $status = ($cr['reported'] == "yes") ? "REPORTED" : "UNREPORTED";
        echo $status;
       ?>
      </td>
    <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>    
    <td>NUMBER OF STUDENTS</td>
      <?php foreach($count_reported as $cr): ?>
    <td><?php echo $cr['nos']; ?></td>
      <?php endforeach; ?>
    </tr>
  </tbody>
</table>
<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td>Examination#</td>
      <td>Student Names#</td>
      <td>Form</td>
      <td>Reported</td>
      <?php if($this->session->userdata('manage_selected_students') == 'ok'): ?>
        <td align="center">Action</td>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
      <?php if ($students == FALSE): ?>
          <tr>
            <td colspan="5">
                      <?php
                          $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Students";
                          echo $message;
                      ?>
                  </td>
          </tr>
      <?php else: ?>
      <?php foreach($students as $student): ?>
        <tr>
          <td><?php echo $student['examination_no']; ?></td>
          <td><?php echo $student['student_names']; ?></td>
          <td><?php echo $student['form']; ?></td>
          <td><?php echo $student['reported']; ?></td>
          <?php if($this->session->userdata('manage_selected_students') == 'ok'): ?>
            <td align="center"><a href="<?php echo base_url() . 'students/edit_selected/'.$student['examination_no']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit" data-placement="bottom"><span class="fa fa-pencil"></span></a>&nbsp;
            <a href="<?php echo base_url() . 'students/new_student/'.$student['examination_no']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Add to registered" data-placement="bottom"><span class="fa fa-plus"></span></a>&nbsp;
            <a href="<?php echo base_url() . 'students/mark_reported/'.$student['examination_no'].'/'.$student['reported']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip"  data-placement="bottom">
            <span <?php if($student['reported'] === 'yes'){
                      echo 'class="glyphicon glyphicon-ok" title="Mark as unreported"';
                    }
                    else{
                      echo 'class="fa fa-spinner fa-pulse fa-1x fa-fw" title="Mark as reported"';
                    }  
              ?> onClick="return confirm('Are you sure you this student has <?php if($student['reported'] === 'yes'){ echo 'un'; } ?>reported?');">
            </span>
            </a>&nbsp;
            <a href="#" class="btn btn-xs btn-primary" data-target="#myModalSelected<?php echo $student['examination_no']; ?>" data-toggle="modal" title="View More Details" data-placement="bottom"><span class="fa fa-eye"></span></a>
            </td>
          <?php endif; ?>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>


<!-- MODAL FOR SELECTED STUDENT -->

<?php foreach ($students as $student): ?>

  <div class="modal fade" id="myModalSelected<?php echo $student['examination_no']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Names: " . $student['student_names']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">                      
                    <table class="table table-condensed table-hover table-striped table-bordered">
                      <tr class="table-header">
                        <td>Title: </td>
                        <td>Description</td>
                      </tr>
                      <tr>
                        <td>Exam#: </td>
                        <td><?php echo $student['examination_no']; ?></td>
                      </tr>
                      <tr>
                        <td>Date Of Birth: </td>
                        <td><?php echo $student['dob']; ?></td>
                      </tr>
                      <tr>
                        <td>Former School: </td>
                        <td><?php echo $student['former_school']; ?></td>
                      </tr>
                      <tr>
                        <td>Sex: </td>
                        <td><?php echo $student['gender']; ?></td>
                      </tr>
                      <tr>
                        <td>Nationality: </td>
                        <td><?php echo $student['nationality']; ?></td>
                      </tr>
                      <tr>
                        <td>Ward: </td>
                        <td><?php echo $student['ward']; ?></td>
                      </tr>
                      <tr>
                        <td>Orphan: </td>
                        <td><?php echo $student['orphan']; ?></td>
                      </tr>
                      <tr>
                        <td>Guardian Firstname: </td>
                        <td><?php echo $student['g_firstname']; ?></td>
                      </tr>
                      <tr>
                        <td>Guardian Lastname: </td>
                        <td><?php echo $student['g_lastname']; ?></td>
                      </tr>
                      <tr>
                        <td>Disable: </td>
                        <td><?php echo $student['disability']; ?></td>
                      </tr>
                      <tr>
                        <td>Region: </td>
                        <td><?php echo $student['region']; ?></td>
                      </tr>
                      <tr>
                        <td>District: </td>
                        <td><?php echo $student['district']; ?></td>
                      </tr>
                      <tr>
                        <td>Telephone#: </td>
                        <td><?php echo $student['tel_no']; ?></td>
                      </tr>
                      <tr>
                        <td>P.O Box: </td>
                        <td><?php echo $student['box']; ?></td>
                      </tr>
                      <tr>
                        <td>Email: </td>
                        <td><?php echo $student['email']; ?></td>
                      </tr>
                      <tr>
                        <td>Form: </td>
                        <td><?php echo $student['form']; ?></td>
                      </tr>
                    </table>
            </div>
          </div>
    </div>
</div>


<?php endforeach; ?>

<!-- END MODAL FOR SELECTED STUDENTS -->