
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_academic_year();'); ?>
	<?php echo form_open('year/update_academic_year', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="start_date">Begin Date :</label>
			<div class="col-sm-10">
				<input type="date" name="start_date" class="form-control" id="start_date" required value="<?php echo $academic_year_record['start_date']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('start_date'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="end_date">End Date :</label>
			<div class="col-sm-10">
				<input type="date" name="end_date" class="form-control" id="end_date" required value="<?php echo $academic_year_record['end_date']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('end_date'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="level">Level :</label>
			<div class="col-sm-10">
				<input value="<?php echo $academic_year_record['class_level']; ?>" type="text" name="level" class="form-control" id="year" required readonly/>
				<div class="error_message_color">
					<?php echo form_error('level'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="year">Year :</label>
			<div class="col-sm-10">
				<input value="<?php echo $academic_year_record['year']; ?>" type="text" name="year" class="form-control" id="year" required readonly/>
				<div class="error_message_color">
					<?php echo form_error('year'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="id" value="<?php echo $academic_year_record['id']; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_academic_year" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>