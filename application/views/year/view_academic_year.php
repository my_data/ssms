
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $academic_year_title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

</div>
<?php if($this->session->userdata('update_academic_year') == 'ok'): ?>
    <a href="<?php echo base_url() . 'year/add_academic_year'; ?>" class="btn btn-primary btn-sm pull-right">Add New</a>
<?php endif; ?>
</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Level</td>	
			<td>Academic year</td>
			<td>Begin Date</td>
			<td>End Date</td>	
            <?php if($this->session->userdata('update_academic_year') == 'ok'): ?>	
			 <td>Action</td>
            <?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($academic_years == FALSE): ?>
    	<tr>
	    	<td colspan="4">There is currently no academic year added</td>
	    </tr>
	<?php else: ?>
		<?php
			foreach ($academic_years as $key => $academic_year):
		?>
			<tr>
				<td><?php echo $academic_year['class_level']; ?></td>
				<td><?php echo $academic_year['year']; ?></td>
				<td><?php echo $academic_year['start_date']; ?></td>
				<td><?php echo $academic_year['end_date']; ?></td>
                <?php if($this->session->userdata('update_academic_year') == 'ok'): ?>
    				<td>&nbsp;&nbsp;&nbsp;
    					<a href="<?php echo base_url(); ?>year/edit_academic_year/<?php echo $academic_year['id']; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;
    					<!-- <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-toggle="tooltip" data-placement="bottom" title="New Term" data-target="#<?php echo $academic_year['id']; ?>"><span class="fa fa-plus"></span></a> -->
    				</td>
                <?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
</div> <!-- END WHITE AREA CONTENT -->

<div style="padding-top: 50px;">

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $term_title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<strong>
    <button type="submit" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-off"></span></button>&nbsp;Current
    &nbsp; &nbsp;
    <button type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-off"></span></button>&nbsp;Noncurrent
</strong>
</div>


</div>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr class="table-header"> 
            <td>Class Level</td>
            <td>Academic year</td>
            <td>Term</td>
            <td>Begin Date</td>
            <td>End Date</td>     
            <?php if($this->session->userdata('update_term') == 'ok'): ?> 
                <td>Edit</td>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
    <?php if ($terms == FALSE): ?>
        <tr>
            <td colspan="4">There are currently No terms added</td>
        </tr>
    <?php else: ?>
        <?php
            $level = ""; $yr = "";
            foreach ($terms as $key => $term):                
        ?>
            <tr>
                <td><?php if($term['class_level'] != $level){ echo $term['class_level']; } ?></td>
                <td><?php if($term['year'] != $yr){ echo $term['year']; } ?></td>
                <td><?php echo $term['term_name']; ?></td>
                <td><?php echo $term['term_begin_date']; ?></td>
                <td><?php echo $term['term_end_date']; ?></td>
                <?php if($this->session->userdata('update_term') == 'ok'): ?>
                    <td>
                    <?php echo form_open('year/set_current_term'); ?>
                        &nbsp;&nbsp;&nbsp;
                        <a href="#" data-target="#term<?php echo $term['term_id']; ?>" data-toggle="modal" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="academic_year" value="<?php echo $term['id']; ?>">
                        <input type="hidden" name="term_id" value="<?php echo $term['term_id']; ?>" />
                        <input type="hidden" name="is_current" value="<?php echo $term['is_current']; ?>" />
                        <button type="submit" name="set_current" 
                        <?php
                            if($term['is_current'] === 'yes'){
                        ?>
                                class="btn btn-xs btn-primary" 
                        <?php
                            }
                            else{
                        ?>
                                class="btn btn-xs btn-danger" 
                        <?php
                            }
                        ?>
                        onClick="return confirm('Are you sure this is the current term? Make sure before you confirm?');" disabled>
                        <span class="glyphicon glyphicon-off"></span></button>
                    <?php echo form_close(); ?>
                    </td>
                <?php endif; ?>
            </tr>
        <?php
            //Special kwa Class Level na Term zizijirudie katika ku_echo 
            $level = $term['class_level'];
            $yr = $term['year'];
            endforeach;
        ?>
        <?php endif; ?>
    </tbody>
</table>

<!--START ADD TERM MODAL-->
<?php foreach ($academic_years as $key => $academic_year): ?>
<div class="modal fade" id="<?php echo $academic_year['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title"><?php echo "Add Term to: ". $academic_year['year']; ?></h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <!-- <form id="loginForm" method="post" class="form-horizontal"> -->
                <?php $attributes = array('class' => 'form-horizontal'); ?>
                <?php echo form_open('year/add_new_term', $attributes); ?>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Term</label>
                        <div class="col-xs-9">
                            <select name="term_name" class="form-control" id="term_name" >
                               <option value="">--Choose--</option>
                            	<option value="first_term">First Term</option>
                            	<option value="second_term">Second Term</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Begin Date</label>
                        <div class="col-xs-9">
                            <input type="date" name="begin_date" class="form-control" id="begin_date" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">End Date</label>
                        <div class="col-xs-9">
                            <input type="date" name="end_date" class="form-control" id="end_date" />
                        </div>
                    </div>
                    <input type="hidden" name="academic_year" class="form-control" value="<?php echo $academic_year['id']; ?>" />

                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-3">
                            <input type="submit" class="btn btn-primary form-control" name="add_new_term" value="Add Record" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!--END ADD TERM MODAL-->

<!--START EDIT TERM MODAL-->
<?php foreach ($terms as $key => $term): ?>
<div class="modal fade" id="term<?php echo $term['term_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title"><?php echo "Edit: " . $term['term_name'] . ", " . $term['year']; ?></h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <!-- <form id="loginForm" method="post" class="form-horizontal"> -->
                <?php $attributes = array('class' => 'form-horizontal'); ?>
                <?php echo form_open('year/update_term', $attributes); ?>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Begin Date</label>
                        <div class="col-xs-9">
                            <input  value="<?php echo $term['term_begin_date']; ?>" type="date" name="begin_date" class="form-control" id="begin_date" readonly />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">End Date</label>
                        <div class="col-xs-9">
                            <input value="<?php echo $term['term_end_date']; ?>" type="date" name="end_date" class="form-control" id="end_date" />
                        </div>
                    </div>
                    <input type="hidden" name="aid" class="form-control" value="<?php echo $term['id']; ?>" />
                    <input type="hidden" name="term_name" class="form-control" value="<?php echo $term['term_name']; ?>" />

                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-3">
                            <input type="submit" class="btn btn-primary form-control" name="update_term" value="Update" onClick="return alert('Save Changes?');" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!--END EDIT TERM MODAL-->