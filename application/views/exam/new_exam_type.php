
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-pencil-square"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('exam/add_exam_type', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="exam_name">Exam Name :</label>
			<div class="col-sm-10">
				<select class="form-control" name="exam_name_and_exam_types" id="exam_name" required="true">
					<option value="">--Choose--</option>
					<option value="E01/First Monthly Examinations/E05">First Monthly Examinations</option>
					<option value="E02/Midterm Examinations/E06">Midterm Examinations</option>
					<option value="E03/Second Monthly Examinations/E07">Second Monthly Examinations</option>
					<option value="E04/End of term Examinations/E08">End of term Examinations</option>
				</select>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="academic_year">Academic year :</label>
			<div class="col-sm-10">
				<select name="academic_year" class="form-control" id="academic_year" required="true">
					<option value="">--Choose--</option>
					<?php
						foreach ($academic_years as $key => $academic_year) {
					?>
							<option value="<?php echo $academic_year['id']; ?>"><?php echo $academic_year['year']; ?></option>
					<?php
						}
					?>
				</select>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="level">Level :</label>
			<div class="col-sm-10">
				<select name="level" class="form-control" id="level" required="true">
					<option value="">--Choose--</option>
					<option value="A">A'Level</option>
					<option value="O">O'Level</option>
				</select>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="new_exam_type" value="Add Exam Type" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>