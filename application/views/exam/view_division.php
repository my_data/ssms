
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">

</div>
<?php if($this->session->userdata('manage_division') == 'ok'): ?>
  <a href="<?php echo base_url() . 'exam/add_division'; ?>" class="btn btn-primary btn-sm">Add New Division</a>
<?php endif; ?>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-responsive table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td align="center">Level</td>
			<td align="center">No. Of Subjects</td>
			<td align="center">Penalty</td>
			<td align="center">Penalty Grade</td>
			<td align="center">Penalty Divisions</td>	
			<td align="center">Assigned Division</td>	
      <?php if($this->session->userdata('manage_division') == 'ok'): ?>
        <td align="center">Action</td>		
      <?php endif; ?>
		</tr>
	</thead>
	<tbody>
		<?php if ($compute_division): ?>
			<?php foreach ($compute_division as $key => $row): ?>
				<tr>
					<td align="center"><strong><?php echo $row['level']; ?></strong></td>
					<td align="center"><strong><?php echo $row['nos']; ?></strong></td>
					<td align="center"><strong><?php echo $row['penalty']; ?></strong></td>
					<td align="center"><strong><?php if($row['penalty'] == "yes"){ echo $row['penalty_grade']; }else{ echo "N/A"; } ?></strong></td>
					<td align="center"><strong><?php if($row['penalty'] == "yes"){ echo $row['penalty_divs']; }else{ echo "N/A"; } ?></strong></td>
					<td align="center"><strong><?php if($row['penalty'] == "yes"){ echo $row['assignedDiv']; }else{ echo "N/A"; } ?></strong></td>
          <?php if($this->session->userdata('manage_division') == 'ok'): ?>
				    <td align="center"><a href="#" data-toggle='modal' data-target='#addCD<?php echo $row['level']; ?>' title="Add / Edit Division Computation" class="btn btn-primary btn-xs"><span class="fa fa-cog"></td>
          <?php endif; ?>
        </tr>	
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>

</table>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Level</td>
			<td align="center">Min Points</td>
			<td align="center">Max Points</td>
			<td align="center">Division</td>
      <?php if($this->session->userdata('manage_division') == 'ok'): ?>
			 <td align="center">Action</td>
      <?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($divisions == FALSE): ?>
    	<tr>
	    	<td colspan="5">There are currently No Division</td>
	    </tr>
	<?php else: ?>
		<?php
			$x = ""; //For checking level ili i_echo only once
			foreach ($divisions as $key => $division):
		?>
			<tr>
				<td>
					<?php if($division['level'] !== $x): ?>
						<strong><?php echo $division['level'] . "'Level"; ?></strong>
							<?php $x = $division['level']; ?>
					<?php endif; ?>
				</td>
				<td align="center"><?php echo $division['starting_points']; ?></td>
				<td align="center"><?php echo $division['ending_points']; ?></td>
				<td align="center"><?php echo $division['div_name']; ?></td>
        <?php if($this->session->userdata('manage_division') == 'ok'): ?>
  				<td align="center">
  					<a href="<?php echo base_url(); ?>exam/edit_division/<?php echo $division['id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
  					<a href="<?php echo base_url(); ?>exam/delete_division/<?php echo $division['id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to delete this record?');"><span class="glyphicon glyphicon-trash"></span></a>
  				</td>
        <?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
	<!-- <div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'exam/streams'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div> -->
	<div align="right">
		<?php //echo $links; ?>
	</div>
</div>
</div>


<!-- START MODAL ADD COMPUTE DIVISION -->
<?php foreach ($divisions as $key => $division): ?>
<div class="modal fade" id="addCD<?php echo $division['level']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add CD: " . $division['level'] . " Level"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("exam/add_nos_for_computing_division", $attributes);
                ?> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="nos" >Number of Subjects</label>
                    <div class="col-sm-8">
                      <input type="number" name="nos" min="3" class="form-control" required />
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="penalty" >Penalty</label>
                    <div class="col-sm-8">
                      <input type="radio" name="penalty" required value="yes" />Yes&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="penalty" required value="no" checked/>No
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="division" >Divisions</label>
                    <div class="col-sm-8">
                      	<?php foreach ($divisions as $key => $div):
                      		if($div['level'] == $division['level']): ?>
                      			<input type="checkbox" name="penalty_divs[]" value="<?php echo $div['div_name']; ?>" />
                      			<strong><?php echo $div['div_name']; ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      		<?php endif; ?>
                      	<?php endforeach; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="assignedDiv" >Assigned Division</label>
                    <div class="col-sm-8">
                      <?php foreach ($divisions as $key => $div):
                      		if($div['level'] == $division['level']): ?>
                      			<input type="radio" name="assignedDiv" value="<?php echo $div['div_name']; ?>" />
                      			<strong><?php echo $div['div_name']; ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      		<?php endif; ?>
                      	<?php endforeach; ?>

                    </div>
                </div>    
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="assignedDiv" >Penalty Grade</label>
                    <div class="col-sm-8">
                    	<?php if($division['level'] == "O"): ?>
                    		<select name="penalty_grade" class="form-control">
                    			<option value="">--Choose--</option>
                    			<?php foreach ($o_grades as $key => $og): ?>                     		                      			
	                      			<option value="<?php echo $og['grade']; ?>"><?php echo $og['grade']; ?></option>
	                      		<?php endforeach; ?>
                    		</select>	                      		
                      	<?php endif; ?>
                      	<?php if($division['level'] == "A"): ?>
                      		<select name="penalty_grade" class="form-control">
                      			<option value="">--Choose--</option>
                    			<?php foreach ($a_grades as $key => $ag): ?>                     		                      			
	                      			<option value="<?php echo $ag['grade']; ?>"><?php echo $ag['grade']; ?></option>
	                      		<?php endforeach; ?>
                    		</select>
                      	<?php endif; ?>
                    </div>
                </div>                
            </div>            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="level" value="<?php echo $division['level']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">ADD</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php $x++; //Kwa ajili ya modal ?>
<?php endforeach; ?>
<!-- END MODAL ADD COMPUTE DIVISION -->