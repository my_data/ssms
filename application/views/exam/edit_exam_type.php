
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-pencil-square"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	<?php
		echo form_open('exam/edit_exam_type/'.$etid, $attributes);
	?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="exam_name">Exam Name :</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="exam_name" id="exam_name" required="true" value="<?php echo $exam_type['exam_name']; ?>">	
				<div class="error_message_color">
					<?php echo form_error('exam_name'); ?>
				</div>				
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="start_date">Start Date :</label>
			<div class="col-sm-10">
				<input type="date" class="form-control" name="start_date" id="start_date" required="true" value="<?php echo $exam_type['start_date']; ?>">
				<div class="error_message_color">
					<?php echo form_error('start_date'); ?>
				</div>									
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="end_date">End Date :</label>
			<div class="col-sm-10">
				<input type="date" class="form-control" name="end_date" id="end_date" required="true" value="<?php echo $exam_type['end_date']; ?>">
				<div class="error_message_color">
					<?php echo form_error('end_date'); ?>
				</div>								
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="id" required="true" value="<?php echo $exam_type['id']; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_exam_type" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>