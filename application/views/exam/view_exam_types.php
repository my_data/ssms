
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

</div>

<?php if($this->session->userdata('enable_or_disable_exam_type') == 'ok'): ?>
	<?php echo form_open('exam/enable_exam_type'); ?>
	    <input type="submit" name="disable" value="Disable All" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to disable all exams?')"/>
	<?php echo form_close(); ?>
<?php endif; ?>

</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Exam Type</td>
			<td>Start Date</td>
			<td>End Date</td>
			<?php if($this->session->userdata('update_exam_type') == 'ok' || $this->session->userdata('enable_or_disable_exam_type') == 'ok'): ?>
				<td align="center">Action</td>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($exam_types == FALSE): ?>
    	<tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Exam Types Added";
                        echo $message;
                    ?>
                </td>
        </tr>
	<?php else: ?>
		<?php
			foreach ($exam_types as $key => $exam_type):
		?>
			<tr>
				<td>
					<?php 
	    				if($exam_type['add_score_enabled'] == 1){
	    					echo "<strong><em>" . $exam_type['exam_name'] . "</em></strong>";
	    				}
	    				else{
	    					echo $exam_type['exam_name'];
	    				}
	    			?>
				</td>
				<td><?php echo $exam_type['start_date']; ?></td>
				<td><?php echo $exam_type['end_date']; ?></td>
				<?php if($this->session->userdata('update_exam_type') == 'ok' || $this->session->userdata('enable_or_disable_exam_type') == 'ok'): ?>
					<td align="center">
						<?php echo form_open('exam/enable_exam_type/'.$exam_type['id']); ?>
							<?php if($this->session->userdata('update_exam_type') == 'ok'): ?>
								<a href="<?php echo base_url(); ?>exam/edit_exam_type/<?php echo $exam_type['id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
							<?php endif; ?>
							<?php if($this->session->userdata('enable_or_disable_exam_type') == 'ok'): ?>
								<!-- <a href="<?php echo base_url(); ?>exam/delete_exam_type/<?php echo $exam_type['id']; ?>" class="glyphicon glyphicon-trash" style="color: #f00;" onClick="return confirm('Are you sure you want to delete this exam_type?');"></a> -->
								<input type="hidden" name="add_score_enabled" value="1" />
								<input type="submit" name="submit" value="Enable" class="btn btn-primary btn-xs" onClick="return confirm('Are you sure you want to enabled this exam type?')"/>
							<?php endif; ?>
						<?php echo form_close(); ?>
					</td>
				<?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
	<!-- <div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'exam/streams'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div> -->
	<div align="right">
		<?php //echo $links; ?>
	</div>
</div>
</div>