
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_division()'); ?>
	<?php echo form_open('exam/add_division', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="start_mark">Minimum Points :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('min_points'); ?>" type="number" min="0" max="50" name="min_points" class="form-control" id="min_points" required />
				<div class="error_message_color">
					<?php echo form_error('min_points'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="ending_points">Maximum Points :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('max_points'); ?>" type="number" min="0" max="50" name="max_points" class="form-control" id="min_points" required />
				<div class="error_message_color">
					<?php echo form_error('max_points'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="level">Level :</label>
			<div class="col-sm-10">
				<select name="level" class="form-control" id="level" required>
					<option value="">--Choose--</option>
					<option <?php echo set_select('level', "A", $this->input->post('level')); ?> value="A">A'Level</option>
					<option <?php echo set_select('level', "O", $this->input->post('level')); ?> value="O">O'Level</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('level'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="grade">Division :</label>
			<div class="col-sm-10">
				<select name="division" class="form-control chosen-select" id="division">
					<option value="">--Choose--</option>
					<option <?php echo set_select('level', "I", $this->input->post('division')); ?> value="I">I</option>
					<option <?php echo set_select('level', "II", $this->input->post('division')); ?> value="II">II</option>
					<option <?php echo set_select('level', "III", $this->input->post('division')); ?> value="III">III</option>
					<option <?php echo set_select('level', "IV", $this->input->post('division')); ?> value="IV">IV</option>
					<option <?php echo set_select('level', "0", $this->input->post('division')); ?> value="0">0</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('division'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_division" value="Add Record" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>