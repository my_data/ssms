
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_division()'); ?>
	<?php echo form_open('exam/edit_division/'.$id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="min_points">Minimum Points :</label>
			<div class="col-sm-10">
				<input type="number" min="0" max="50" name="min_points" class="form-control" id="min_points" required value="<?php echo $division_record['starting_points']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('min_points'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="max_points">Maximum Points :</label>
			<div class="col-sm-10">
				<input type="number" min="0" max="50" name="max_points" class="form-control" id="min_points" required value="<?php echo $division_record['ending_points']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('max_points'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="level">Level :</label>
			<div class="col-sm-10">
				<select name="level" class="form-control" id="level" required>
					<option value="">--Choose--</option>
					<option <?php if($division_record['level'] === "A"){ echo "selected"; } ?> value="A">A'Level</option>
					<option <?php if($division_record['level'] === "O"){ echo "selected"; } ?> value="O">O'Level</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('level'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="division">Division :</label>
			<div class="col-sm-10">
				<select name="division" class="form-control chosen-select" id="division" required>
					<option value="">--Choose--</option>
					<option <?php if($division_record['div_name'] === "I"){ echo "selected"; } ?> value="I">I</option>
					<option <?php if($division_record['div_name'] === "II"){ echo "selected"; } ?> value="II">II</option>
					<option <?php if($division_record['div_name'] === "III"){ echo "selected"; } ?> value="III">III</option>
					<option <?php if($division_record['div_name'] === "IV"){ echo "selected"; } ?> value="IV">IV</option>
					<option <?php if($division_record['div_name'] === "0"){ echo "selected"; } ?> value="0">0</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('division'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="id" value="<?php echo $division_record['id']; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_division" value="Update" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>