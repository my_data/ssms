
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'/*, 'onSubmit' => 'return validate_o_grade_system()'*/); ?>
	<?php echo form_open('exam/add_o_grade', $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="start_mark">From :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('start_mark'); ?>" type="number" step=".01" min="0" max="100" name="start_mark" class="form-control" id="start_mark" required />
				<div class="error_message_color">
					<?php echo form_error('start_mark'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="end_mark">To :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('end_mark'); ?>" type="number" step=".01" min="0" max="100" name="end_mark" class="form-control" id="end_mark" required />
				<div class="error_message_color">
					<?php echo form_error('end_mark'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="grade">Grade :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('grade'); ?>" type="text" name="grade" class="form-control" id="grade" required />
				<div class="error_message_color">
					<?php echo form_error('grade'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="unit">Unit :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('unit'); ?>" type="number" min="0" max="10" name="unit" class="form-control" id="unit" required />
				<div class="error_message_color">
					<?php echo form_error('unit'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label"  for="end_mark">Remarks :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('remarks'); ?>" type="text" name="remarks" class="form-control" id="remarks" required />
				<div class="error_message_color">
					<?php echo form_error('remarks'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_o_grade" value="Add Record" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>