
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
</div>
<?php if($this->session->userdata('manage_a_level_grading_system') == 'ok'): ?>
	<a href="<?php echo base_url() . 'exam/add_a_grade'; ?>" class="btn btn-primary btn-sm">Add New</a>
<?php endif; ?>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>From</td>
			<td>To</td>
			<td>Grade</td>
			<td>Unit</td>
			<td>Remarks</td>
			<?php if($this->session->userdata('manage_a_level_grading_system') == 'ok'): ?>
				<td align="center">Action</td>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($a_grades == FALSE): ?>
    	<tr>
	    	<td colspan="6">There are currently Records</td>
	    </tr>
	<?php else: ?>
		<?php
			foreach ($a_grades as $key => $a_grade):
		?>
			<tr>
				<td><?php echo $a_grade['start_mark']; ?></td>
				<td><?php echo $a_grade['end_mark']; ?></td>
				<td><?php echo $a_grade['grade']; ?></td>
				<td><?php echo $a_grade['points']; ?></td>
				<td><?php echo $a_grade['remarks']; ?></td>
				<?php if($this->session->userdata('manage_a_level_grading_system') == 'ok'): ?>
					<td align="center">
						<a href="<?php echo base_url(); ?>exam/edit_a_grade/<?php echo $a_grade['id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
						<a href="<?php echo base_url(); ?>exam/delete_a_grade/<?php echo $a_grade['id']; ?>" class="btn btn-xs btn-danger" onClick="return confirm('Are you sure you want to delete this record?');"><span class="glyphicon glyphicon-trash"></span></a>
					</td>
				<?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
	<!-- <div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'exam/streams'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div> -->
	<div align="right">
		<?php //echo $links; ?>
	</div>
</div>
</div>