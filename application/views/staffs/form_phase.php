
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user-plus" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>

 	<!-- <a href="#" onClick="hide_element('select_element')">Try</a> -->
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('id' => 'staffRegistrationForm', 'role' => 'form', 'onSubmit' => 'return false'); ?>
	<?php //echo form_open('admin/new_staff', $attributes); ?>
	<?php
		echo form_open('staffs/new_staff', $attributes); 		
	?>

<div id="staffPersonalInfoPhase">
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Personal Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staff_id">Staff ID :</label>
			<div class="col-sm-10">
				<div id="successMessage" class="success_message_color">
					<?php 
						if($this->session->flashdata('success_message')){
							echo $this->session->flashdata('success_message');
						}
					?>
				</div>
				<div id="errorMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('error_message')){
							echo $this->session->flashdata('error_message');
						}
					?>
				</div>
				<div id="warningMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('exist_message')){
							echo $this->session->flashdata('exist_message');
						}
					?>
				</div>
				<input id="staff_id" value="<?php echo set_value('staff_id'); ?>" type="text" name="staff_id" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('staff_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
			<div class="col-sm-10">
				<input id="firstname" value="<?php echo set_value('firstname'); ?>" type="text" name="firstname" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('firstname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="middlename">Middlename : </label>
			<div class="col-sm-10">
				<input id="middlename" value="<?php echo set_value('middlename'); ?>" type="text" name="middlename" class="form-control" />
				<div class="error_message_color">
					<?php echo form_error('middlename'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
			<div class="col-sm-10">
				<input id="lastname" value="<?php echo set_value('lastname'); ?>" type="text" name="lastname" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('lastname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="dob">Date of birth :</label>
			<div class="col-sm-10">
				<input id="dob" value="<?php echo set_value('dob'); ?>" type="date" name="dob" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('dob'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gender">Gender :</label>
			<div class="col-sm-10">
				<input id="gender" type="radio" name="gender" value="Male" checked /> Male
				<input type="radio" name="gender" value="Female" /> Female
				<div class="error_message_color">
					<?php echo form_error('gender'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="marital_status">Marital Status :</label>
			<div class="col-sm-10">
				<select id="marital_status" name="marital_status" class="form-control" required >
					<option value="">--Choose--</option>
					<option <?php echo set_select('marital_status', 'Single', $this->input->post('marital_status')); ?> value="Single">Single</option>
					<option <?php echo set_select('marital_status', 'Married', $this->input->post('marital_status')); ?> value="Married">Married</option>
					<option <?php echo set_select('marital_status', 'Divorced', $this->input->post('marital_status')); ?> value="Divorced">Divorced</option>
					<option <?php echo set_select('marital_status', 'Widowed', $this->input->post('marital_status')); ?> value="Widowed">Widowed</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('marital_status'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-10">
				<button onClick="processStaffPersonalInfo()" class="form-control btn btn-primary">Continue</button>
			</div>
		</div>
</div><!-- END Personal Phase -->
<div id="staffContactInfoPhase">
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Contact Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="email">Email :</label>
			<div class="col-sm-10">
				<input id="email" value="<?php echo set_value('email'); ?>" type="text" name="email" class="form-control" />
				<div class="error_message_color">
					<?php echo form_error('email'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="phone_no">Phone No :</label>
			<div class="col-sm-10">
				<input id="phone_no" value="<?php echo set_value('phone_no'); ?>" type="text" name="phone_no" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('phone_no'); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-5">
				<button onClick="backToStaffPersonalInfo()" class="form-control btn btn-primary">Previous</button>
			</div>
			<div class="col-sm-5">
				<button onClick="processStaffContactInfo()" class="form-control btn btn-primary">Continue</button>
			</div>
		</div>
		<br/>
</div> <!-- END Contact Phase -->
<div id="staffRolePhase">
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Staff Role</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staff_type">Staff Type :</label>
			<div class="col-sm-10">
				<select id="staff_type" name="staff_type" onchange="show_element()" class="form-control" required>
					<option value="">--Choose Type--</option>
					<option <?php //echo set_select('staff_type', 'T', $this->input->post('staff_type')); ?> value="T">Teacher</option>
					<option <?php echo set_select('staff_type', 'S', $this->input->post('staff_type')); ?> value="S">Secretary</option>
					<option <?php echo set_select('staff_type', 'L', $this->input->post('staff_type')); ?> value="L">Librarian</option>
					<option <?php echo set_select('staff_type', 'A', $this->input->post('staff_type')); ?> value="A">Accountant</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('staff_type'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<!-- <div id="select_element" > -->
			<div class="form-group">
				<label class="col-sm-2 control-label" for="subjects">Subjects :</label>
				<div class="col-sm-10">
					<select name="subject_id[]" class="chosen-select form-control" multiple >
						<?php foreach($subjects as $subject): ?>
							<option value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
						<?php endforeach; ?>
					</select>
					<div class="error_message_color">
						<?php echo form_error('subject_id[]'); ?>
					</div>
				</div>
			</div>
		<!-- </div> -->
		<br/><br/>
		<div class="form-group">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-5">
				<button onClick="backToStaffContactInfo()" class="form-control btn btn-primary">Previous</button>
			</div>
			<div class="col-sm-5">
				<button onClick="processStaffRoleInfo()" class="form-control btn btn-primary">Continue</button>
			</div>
		</div>
		<br/>
</div><!-- END Staff Role Phase -->

<div id="staffNokInfoPhase">
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Next Of Kin Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_firstname">Firstname :</label>
			<div class="col-sm-10">
				<input id="nok_fn" value="<?php echo set_value('r_firstname'); ?>" type="text" name="r_firstname" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('r_firstname'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_lastname">Lastname :</label>
			<div class="col-sm-10">
				<input id="nok_ln" value="<?php echo set_value('r_lastname'); ?>" type="text" name="r_lastname" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('r_lastname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_gender">Gender :</label>
			<div class="col-sm-10">
				<input id="nok_gender" type="radio" name="r_gender" value="Male" checked /> Male
				<input type="radio" name="r_gender" value="Female" /> Female
				<div class="error_message_color">
					<?php echo form_error('r_gender'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="rel_type">Relationship Type :</label>
			<div class="col-sm-10">
				<select id="nok_rel_type" name="r_relationship" class="form-control" required >
					<option <?php echo set_select('r_relationship', 'Father', $this->input->post('r_relationship')); ?> value="">--Choose--</option>
					<option <?php echo set_select('r_relationship', 'Husband', $this->input->post('r_relationship')); ?> value="Husband">Husband</option>
					<option <?php echo set_select('r_relationship', 'Wife', $this->input->post('r_relationship')); ?> value="Wife">Wife</option>
					<option <?php echo set_select('r_relationship', 'Brother', $this->input->post('r_relationship')); ?> value="Brother">Brother</option>
					<option <?php echo set_select('r_relationship', 'Sister', $this->input->post('r_relationship')); ?> value="Sister">Sister</option>
					<option <?php echo set_select('r_relationship', 'Other', $this->input->post('r_relationship')); ?> value="Other">Other</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('r_relationship'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_phone">Phone No :</label>
			<div class="col-sm-10">
				<input id="nok_phone" value="<?php echo set_value('r_phone'); ?>" type="text" name="r_phone" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('r_phone'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_box">P.O. Box :</label>
			<div class="col-sm-10">
				<input id="nok_box" value="<?php echo set_value('r_box'); ?>" type="text" name="r_box" class="form-control" required />
				<div class="error_message_color">
					<?php echo form_error('r_box'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_region">Region :</label>
			<div class="col-sm-10">
				<select id="nok_region" name="r_region" class="form-control" required>
					<option value="">--Choose--</option>
					<?php foreach($regions as $region): ?>
						<option value="<?php echo $region['region_name']; ?>" <?php echo  set_select('r_region', ''.$region["region_name"].'', $this->input->post('r_region')); ?>><?php echo $region['region_name']; ?></option>
					<?php endforeach; ?>			
				</select>
				<div class="error_message_color">
					<?php echo form_error('r_region'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-5">
				<button onClick="backToStaffRoleInfo()" class="form-control btn btn-primary">Previous</button>
			</div>
			<div class="col-sm-5">
				<button onClick="processStaffNokInfoPhase()" class="form-control btn btn-primary">Continue</button>
			</div>
		</div>
</div><!-- END staffNokInfoPhase -->

<div id="previewPhase">
		<table class="table table-condensed table-striped table-bordered table-hover">
			<thead>
				<tr class="table-header">
					<td width="40%">Title</td>
					<td width="60%">Description</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><label>Staff ID </label></td>
					<td><span id="show_staff_id"></span></td>
				</tr>
				<tr>
					<td><label>Staff Names </label></td>
					<td><span id="show_firstname"></span>&nbsp;&nbsp;<span id="show_middlename"></span>&nbsp;&nbsp;<span id="show_lastname"></span></td>
				</tr>
				<tr>
					<td><label>Date Of Birth </label></td>
					<td><span id="show_staff_dob"></span></td>
				</tr>
				<tr>
					<td><label>Gender </label></td>
					<td><span id="show_staff_gender"></span></td>
				</tr>
				<tr>
					<td><label>Marital Status </label></td>
					<td><span id="show_marital_status"></span></td>
				</tr>
				<tr>
					<td><label>Phone# </label></td>
					<td><span id="show_phone_no"></span></td>
				</tr>
				<tr>
					<td><label>Email </label></td>
					<td><span id="show_staff_email"></span></td>
				</tr>		
				<tr>
					<td><label>Staff Type </label></td>
					<td><span id="show_staff_type"></span></td>
				</tr>
				<tr>
					<td><label>NOK Firstname </label></td>
					<td><span id="show_nok_fn"></span></td>
				</tr>
				<tr>
					<td><label>NOK Lastname </label></td>
					<td><span id="show_nok_ln"></span></td>
				</tr>
				<tr>
					<td><label>NOK Gender </label></td>
					<td><span id="show_nok_gender"></span></td>
				</tr>		
				<tr>
					<td><label>Relationship Type </label></td>
					<td><span id="show_nok_rel_type"></span></td>
				</tr>
				<tr>
					<td><label>NOK Phone# </label></td>
					<td><span id="show_nok_phone"></span></td>
				</tr>
				<tr>
					<td><label>Address </label></td>
					<td><span id="show_nok_box"></span>,&nbsp;&nbsp;<span id="show_nok_region"></span></td>
				</tr>
			</tbody>
		</table>
		<br/><br/>
		<div class="form-group">
			<input type="submit" onClick="submitStaffData()" class="form-control btn btn-primary" name="add_staff" value="Add Staff" />
		</div>
		<br/>
</div>
<?php echo form_close(); ?>