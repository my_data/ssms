
<!-- DERICK FORM -->


<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> 

 &nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 


<?php if($this->session->userdata('update_own_profile') == 'ok'): ?>
  <a href="<?php echo base_url() . 'staffs/edit_profile/'.$staff_id; ?>" class="btn btn-primary btn-xs">Edit Profile</a>
<?php endif; ?>

</div>

</div>

<div class="text-left" id="prof1">
  <img src="<?php echo $image['staff_image'];?>" class="img-responsive  " alt="profile picture" style="width:55px" />
          <h5 class="text-danger">
            <?php if($this->session->flashdata('error_upload')): ?>
              <?php echo $this->session->flashdata('error_upload'); ?>
            <?php endif; ?>
          </h5>

<?php //if($this->session->userdata('change_avatar') == 'ok'): ?>
  <?php echo form_open_multipart('staffs/upload_photo/'.$staff_id);?>
      <div class="form-group text-left" >
      <input type="file" name="image_file"  accept="image/gif, image/jpg, image/png" required/><br/>

      <input type="submit" class="btn btn-primary btn-xs" value="Save photo">
      </div>
      

    <?php echo form_close();?>
<?php //endif; ?>
    <!-- Result message-->
      <div class="success_message_color" id="successMessage">
      <?php
        if($this->session->flashdata('success_message')){
           echo '&nbsp&nbsp<i class="fa fa-check" aria-hidden="true"></i> &nbsp&nbsp' .$this->session->flashdata('success_message');
        }
      ?>
    </div>
</div>

<div class="clearfix text-left">

<div class="row">
<div class="col-xs-3" id="prof2">
  <img src="<?php echo $image['staff_image'];?>" class="img-responsive img-thumbnail " alt="profile picture" style="height:180px;width:150px"/>
          <h5 class="text-danger">
            <?php if($this->session->flashdata('error_upload')): ?>
              <?php echo $this->session->flashdata('error_upload'); ?>
            <?php endif; ?>
          </h5>
<?php if($this->session->userdata('change_avatar') == 'ok'): ?>
  <?php echo form_open_multipart('staffs/upload_photo/'.$staff_id);?>
      <div class="form-group">
      <input type="file" name="image_file"  accept="image/gif, image/jpg, image/png" required/><br/>

      <input type="submit" class="btn btn-primary col-xs-9" value="SAVE">
      </div>
<?php endif; ?>
      

    <?php echo form_close();?>
    <!-- Result message-->
      <div class="success_message_color" id="successMessage">
      <?php
        if($this->session->flashdata('success_message')){
           echo '&nbsp&nbsp<i class="fa fa-check" aria-hidden="true"></i> &nbsp&nbsp' .$this->session->flashdata('success_message');
        }
      ?>
    </div>
</div>
<div class="col-md-9">
<div class="col-md-12">
  <div class="col-md-3">
    <label> Full Name :</label>
  </div>
  <div class="col-md-9">
    <?php echo $staff_info['firstname'] . " " . $staff_info['middlename'] . " " . $staff_info['lastname']; ?>
  </div>
</div>

<div class="col-md-12">  
  <div class="col-md-3">
    <label>Phone Number :</label>
  </div>
  <div class="col-md-9">
    <?php echo $staff_info['phone_no']; ?>
  </div>
</div>
<br/>
<div class="col-md-12">  
  <div class="col-md-3">
    <label>Gender :</label>
  </div>
  <div class="col-md-9">
    <?php echo $staff_info['gender']; ?>
  </div>
</div>
<br/>
<div class="col-md-12">  
  <div class="col-md-3">
    <label>Email :</label>
  </div>
  <div class="col-md-9">
    <?php
          if($staff_info['email'] !== ""){
             echo $staff_info['email'];
          }
          else{
            echo "None";
          }
        ?>
  </div>
</div>
<br/>
<div class="col-md-12">  
  <div class="col-md-3">
    <label>Date of Birth :</label>
  </div>
  <div class="col-md-9">
    <?php echo $staff_info['dob']; ?>
  </div>
</div>
<br/>
<div class="col-md-12">  
  <div class="col-md-3">
    <label><b>Role(s) :</b></label>
  </div>
  <div class="col-md-9">
    <?php
      if($staff_info['roles']){
        echo $staff_info['roles'];
      }
      else{
        echo "None";
      }
    ?>
  </div>
</div>
<br/><br/>
<!-- <div class="col-md-12">  
  <div class="col-md-3">
    <label>Admin Group :</b></label>
  </div>
  <div class="col-md-9">
    <?php 
      if($staff_admin_info['group_name']){
        echo $staff_admin_info['group_name'];
      }
      else{
        echo "None";
      }
    ?>
  </div>
</div>
<br/><br/> -->
<div class="col-md-12">  
  <div class="col-md-3">
    <label><b>User Group(s) :</b></label>
  </div>
  <div class="col-md-9">
    <?php if($staff_group_info['staff_groups']){
      echo $staff_group_info['staff_groups'];
    }
    else{
      echo "None";
    }
    ?> 
  </div>
</div>
<br/><br/>
<div class="col-md-12">  
  <div class="col-md-3">
    <label>Qualification :</label>
  </div>
  <div class="col-md-9">
    <?php 
          if($certifications == FALSE){
            echo "None";
          }
          else{
            echo '<table class="table table-condensed table-bordered table-striped table-hover">';
            $x = 0; //Special for testing so as to echo None Uploaded
            foreach ($certifications as $key => $value) {
        ?>
              
                  <?php 
                    if($value['certified'] === "yes"){
                      $x++;
                      echo "<tr><td>";
                      echo $value['description'] . ", " . $value['university'] . ", " . $value['completion_date'];
                      echo "</td></tr>";
                    }
                  ?>                  
        <?php
            }
            if($x === 0){
              echo "<tr><td>None</td></tr>";
            }
            echo "</table>";
          }
        ?>
  </div>
</div>
</div>  <!-- END DIV YA col-xs-9 -->
</div> <!-- END DIV YA ROW -->