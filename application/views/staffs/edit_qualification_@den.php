
<div class="white-area-content">
<div class="db-header clearfix">
<div class="page-header-title"> <span class="fa fa-pencil" style="font-size: 25px;"></span>&nbsp &nbsp<?php echo $names['firstname']." ".$names['lastname'];?>

</div>
</div>
<div class="clearfix">

<!-- <div class="col-sm-10 "> -->
<!-- <h4><?php echo $title;?></h4> -->

<?php echo form_open('staffs/save_edit_qualification');?>

 <input type="hidden" name="staff_id"  class="form-control" value="<?php echo $staff['staff_id'];?>"><br>
 
<div class="form-group">
 <label class="col-sm-2">Certification</label>
 <div class="col-sm-10">
 	<select name="certificate" class="form-control">
 
 		<option value="">--option--</option>
 		<option <?php if ($staff['certification']==="Masters") {echo "selected='true'";}?> value="Masters">Masters</option>
 		<option <?php if ($staff['certification']==="Postgraduate") {echo "selected='true'";}?> value="Postgraduate">Postgraduate</option>
 		<option <?php if ($staff['certification']==="Undergraduate") {echo "selected='true'";}?> value="Undergraduate">Undergraduate</option>
 		<option <?php if ($staff['certification']==="Advanced diploma") {echo "selected='true'";}?>  value="Advanced diploma">Advanced diploma</option>
 		<option <?php if ($staff['certification']==="Diploma") {echo "selected='true'";}?>  value="Diploma">Diploma</option>
 		<option <?php if ($staff['certification']==="Certificate") {echo "selected='true'";}?>  value="Certificate">Certificate</option>
 	</select><br>
 </div>	
</div>

<div class="form-group">
 <label class="col-sm-2">Description</label>
 <div class="col-sm-10">
 	<input type="text" name="description"  class="form-control" value="<?php echo $staff['description'];?>"><br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Completion date</label>
 <div class="col-sm-10">
 	<input type="text" name="year" value="<?php echo $staff['completion_date'];?>" class="form-control"><br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Institute/University</label>
 <div class="col-sm-10">
 	<input type="text" name="university" value="<?php echo $staff['university'];?>" class="form-control"><br>
 </div>	
</div>


<div class="form-group">
 <label class="col-sm-2"> </label>
 <div class="col-sm-10">
 	<input type="submit" name="submit" class="form-control btn-primary" value=" Save qualification"><br>
 </div>	
</div>


<?php echo form_close();?>
 <!-- </div> -->
 </div>
 </div>