
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user-plus" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form', 'onSubmit' => 'return validate_new_class()'); ?>
	<?php echo form_open('staffs/edit_staff/'.$staff_id, $attributes); ?>
		<div class="form-group">
			<!-- <label for="staff_id">Staff#</label> -->
			<div align="center" id="form_headers_styles">
				<h3 class="breadcrumb">Personal Details</h3>
			</div>
			<input type="hidden" name="staff_id" class="form-control" value="<?php echo $staff_record['staff_id']; ?>" required />
		</div>
		<br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
			<div class="col-sm-10">
				<input type="text" name="firstname" class="form-control" value="<?php echo $staff_record['firstname']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('firstname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="middlename">Middlename :</label>
			<div class="col-sm-10">
				<input type="text" name="middlename" class="form-control" value="<?php echo $staff_record['middlename']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('middlename'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
			<div class="col-sm-10">
				<input type="text" name="lastname" class="form-control" value="<?php echo $staff_record['lastname']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('lastname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="dob">Date of birth :</label>
			<div class="col-sm-10">
				<input type="date" name="dob" class="form-control" value="<?php echo $staff_record['dob']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('dob'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gender">Gender :</label>
			<div class="col-sm-10">
				<input type="radio" name="gender" value="Male" <?php if($staff_record['gender'] === "Male"){ echo "checked"; } ?> /> Male
				<input type="radio" name="gender" value="Female" <?php if($staff_record['gender'] === "Female"){ echo "checked"; } ?> /> Female
				<div class="error_message_color">
					<?php echo form_error('gender'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="marital_status">Marital Status :</label>
			<div class="col-sm-10">
				<select name="marital_status" class="form-control" required >
					<option <?php if($staff_record['marital_status'] === "Single"){ echo "selected='true'"; } ?> value="Single">Single</option>
					<option <?php if($staff_record['marital_status'] === "Married"){ echo "selected='true'"; } ?> value="Married">Married</option>
					<option <?php if($staff_record['marital_status'] === "Divorced"){ echo "selected='true'"; } ?> value="Divorced">Divorced</option>
					<option <?php if($staff_record['marital_status'] === "Widowed"){ echo "selected='true'"; } ?> value="Widowed">Widowed</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('marital_status'); ?>
				</div>
			</div>
		</div>
		<br/>
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Contact Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="email">Email :</label>
			<div class="col-sm-10">
				<input type="text" name="email" class="form-control" value="<?php echo $staff_record['email']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('email'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="phone_no">Phone No :</label>
			<div class="col-sm-10">
				<input type="text" name="phone_no" class="form-control" value="<?php echo $staff_record['phone_no']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('phone_no'); ?>
				</div>
			</div>
		</div>
		<br/>
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Staff Role</h3>
		</div>
		<!-- <div class="form-group">
			<label class="col-sm-2 control-label" for="staff_type">Staff Type :</label>
			<?php if($staff_record['staff_type'] === "T"): ?>
				<div class="col-sm-10">
					<select id="staff_type" name="staff_type" onchange="disabledSelectElementStaff('staff_type', 'edit_subject_values')" class="form-control" required>
						<option <?php if($staff_record['staff_type'] === "T"){ echo "selected='true'"; } ?> value="T">Teacher</option>
						<option <?php if($staff_record['staff_type'] === "S"){ echo "selected='true'"; } ?> value="S">Secretary</option>
						<option <?php if($staff_record['staff_type'] === "L"){ echo "selected='true'"; } ?> value="L">Librarian</option>
						<option <?php if($staff_record['staff_type'] === "A"){ echo "selected='true'"; } ?> value="A">Accountant</option>
					</select>
					<div class="error_message_color">
						<?php echo form_error('staff_type'); ?>
					</div>
				</div>
			<?php else: ?>
				<div class="col-sm-10">
					<select name="staff_type" onchange="disabledSelectElementStaff('staff_type', 'subject_values')" class="form-control" required>
						<option <?php if($staff_record['staff_type'] === "T"){ echo "selected='true'"; } ?> value="T">Teacher</option>
						<option <?php if($staff_record['staff_type'] === "S"){ echo "selected='true'"; } ?> value="S">Secretary</option>
						<option <?php if($staff_record['staff_type'] === "L"){ echo "selected='true'"; } ?> value="L">Librarian</option>
						<option <?php if($staff_record['staff_type'] === "A"){ echo "selected='true'"; } ?> value="A">Accountant</option>
					</select>
					<div class="error_message_color">
						<?php echo form_error('staff_type'); ?>
					</div>
				</div>
			<?php endif; ?>
		</div> -->
		
		<br/>
		<?php //if($staff_record['staff_type'] === "T"): ?>
		<!-- <div <?php if($staff_record['staff_type'] === "T"){ echo "id='edit_subject_values'"; }else{ echo "id='subject_values'"; } ?> >
			<div class="form-group">
			<br/>
				<label class="col-sm-2 control-label" for="subjects">Subjects :</label>
				<div class="col-sm-10">
					<select name="subject_id[]" class="chosen-select form-control" multiple>
						<?php foreach($subjects as $subject): ?>
							<option 
								<?php 
									foreach ($subject_teacher_record as $str) {
										if($str['subject_id'] === $subject['subject_id']){
											echo "selected='true'";
										}
									}
								?>
							value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div> -->
		<?php //endif; ?>	
		<br/>
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Next Of Kin Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_firstname">Firstname :</label>
			<div class="col-sm-10">
				<input type="text" name="r_firstname" class="form-control" required value="<?php echo $staff_record['nok_fn']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('r_firstname'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_lastname">Lastname :</label>
			<div class="col-sm-10">
				<input type="text" name="r_lastname" class="form-control" required value="<?php echo $staff_record['nok_ln']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('r_lastname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label for="r_gender">Gender:</label>
			<div class="col-sm-10">
				<input type="radio" name="r_gender" value="Male" checked /> Male
				<input type="radio" name="r_gender" value="Female" /> Female
				<div class="error_message_color">
					<?php echo form_error('r_gender'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label for="rel_type">Relationship Type :</label>
			<div class="col-sm-10">
				<select name="r_relationship" class="form-control" required >
					<option value="">--Choose--</option>
					<option <?php if($staff_record['relation_type'] === "Husband"){ echo "selected='true'"; } ?> value="Husband">Husband</option>
					<option <?php if($staff_record['relation_type'] === "Wife"){ echo "selected='true'"; } ?> value="Wife">Wife</option>
					<option <?php if($staff_record['relation_type'] === "Brother"){ echo "selected='true'"; } ?> value="Brother">Brother</option>
					<option <?php if($staff_record['relation_type'] === "Sister"){ echo "selected='true'"; } ?> value="Sister">Sister</option>
					<option <?php if($staff_record['relation_type'] === "Other"){ echo "selected='true'"; } ?> value="Other">Other</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('r_relationship'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_phone">PhoneNo :</label>
			<div class="col-sm-10">
				<input type="text" name="r_phone" class="form-control" value="<?php echo $staff_record['nok_phone_no']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('r_phone'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_box">P.O. Box :</label>
			<div class="col-sm-10">
				<input type="text" name="r_box" class="form-control" value="<?php echo $staff_record['nok_box']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('r_box'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="r_region">Region :</label>
			<div class="col-sm-10">
				<select name="region" class="form-control" required >
					<option value="">--Choose--</option>
					<?php foreach($regions as $region): ?>
						<option <?php if($region['region_name'] == $staff_record['nok_region']){ echo "selected='true'"; } ?> value="<?php echo $region['region_name']; ?>"><?php echo $region['region_name']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('region'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="old_staff_type" value="<?php echo $staff_record['staff_type']; ?>">
			<input type="submit" class="form-control btn btn-primary" name="update_staff" value="Update" />
		</div>
		<br/>
	<?php echo form_close(); ?>
</div>
