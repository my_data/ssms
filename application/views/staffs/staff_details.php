
        <div class="table-responsive">

            <table class="table table-striped table-hover table-condensed table-bordered">
                
                <thead>
                  <tr class="table-header">
                    <td  style="width:30%; text-align:center;">Title</td>
                    <td  style="width:70%; text-align:center;">Description</td>
                  </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>&nbsp;Staff Names</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['firstname'] . " " . $staff_info['middlename'] . " " . $staff_info['lastname']; ?></td>
                </tr>
                <!-- <tr>
                  <td><b>&nbsp;Phone No.</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['phone_no']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Gender</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['gender']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Email</b></td>
                  <td>&nbsp;&nbsp;
                    <?php
                      if($staff_info['email'] !== ""){
                         echo $staff_info['email'];
                      }
                      else{
                        echo "None";
                      }
                    ?>
                  </td>
                </tr> -->
                <tr>
                  <td><b>&nbsp;Status</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['status']; ?></td>
                </tr>
                <?php 
                  if($staff_info['status'] === "inactive"){
                ?>
                  <tr>
                    <td><b>&nbsp;Reason. </b></td>
                    <td>&nbsp;&nbsp;<?php echo $staff_info['reason']; ?></td>
                  </tr>
                  <tr>
                    <td><b>&nbsp;Deactivated On.</b></td>
                    <td>&nbsp;&nbsp;<?php echo $staff_info['deactivated_date']; ?></td>
                  </tr>
                <?php
                  }
                ?>
                <?php if($staff_info['status'] === "active"): ?>
                  <tr>
                    <td><b>&nbsp;Role(s)</b></td>
                    <td>
                    &nbsp;&nbsp;
                    <?php
                      if($staff_info['roles']){
                        echo $staff_info['roles'];
                      }
                      else{
                        echo "None";
                      }
                    ?>
                    </td>
                  </tr>                
                 <!--  <tr>
                    <td><b>&nbsp;Admin Group</b></td>
                    <td>
                    &nbsp;&nbsp;
                    <?php 
                      if($staff_admin_info['group_name']){
                        echo $staff_admin_info['group_name'];
                      }
                      else{
                        echo "None";
                      }
                    ?>                    
                    </td>
                  </tr> -->
                  <tr>
                    <td><b>&nbsp;User Group(s)</b></td>
                    <td>
                    &nbsp;&nbsp;
                    <?php if($staff_group_info['staff_groups']){
                      echo $staff_group_info['staff_groups'];
                    }
                    else{
                      echo "None";
                    }
                    ?>                    
                    </td>
                  </tr>
                <?php endif; ?>
                  <tr>
                  <td><b>&nbsp;Last Login</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['last_log']; ?></td>
                </tr>
              </tbody>
            </table>
          </div>