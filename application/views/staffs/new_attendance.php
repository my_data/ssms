
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<?php echo form_open('staffs/save_staff_attendance'); ?>

<label for="date">Date</label>
<input type="date" name="date" id="date" required />

<div class="input-group-btn">
   
      </div>
</div>
</div>


</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr class="table-header">
			<td>Names</td>
			<td>Present</td>			
			<td>Sick</td>
			<td>Permitted</td>
			<td>Absent</td>
		</tr>
	</thead>

		<tbody >
		<?php if ($staffs == FALSE): ?>
	      	<tr>
		        <td colspan="5">
		            <?php
		                $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Staffs";
		                echo $message;
		            ?>
		         </td>
	     	</tr>
		<?php else: ?>
			<?php $x = 1; ?> 
			<?php foreach($staffs as $staff): ?>
				<tr>
					<td>
						<?php echo $staff['names']; ?>
						<input type="hidden" name="staff_id[]" value="<?php echo $staff['staff_id']; ?>">
					</td>
					<!-- CHECKBOX -->
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="2" checked="true" />
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="1">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="3">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<td>
						<input type="checkbox" name="attendance[]" class="chb<?php echo $x; ?>" value="4">
						<input type="hidden" name="" value="chb<?php echo $x; ?>">
					</td>
					<!-- END CHECKBOX -->
				</tr>
				<?php $x++; ?>
			<?php endforeach; ?>
		</tbody>
	
</table>
<div align="center">
	<input type="submit" name="submit" value="Save Attendance Record" class="btn btn-primary form-control" onClick="return confirm('Are you sure you want to add this attendance record, it cannot be modified');" />
<?php endif; //The save button should not be displayed if there are bo students in class ?>
</div>
<?php echo form_close(); ?>