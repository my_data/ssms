
          <div class="table-responsive" id="exhaustive_table">
           <table class="table table-striped table-hover table-condensed table-bordered">
              <thead>

                <tr class="table-header">
                  <td style="text-align:center;">Year</td>
                  <td style="text-align:center;">Present</td>
                  <td style="text-align:center;">Absent</td>
                  <td style="text-align:center;">Sick</td>
                  <td style="text-align:center;">Permitted</td>
                  <td style="text-align:center;">Average</td>
                </tr>
              </thead>
              <tbody>
               <?php if ($staff_attendance_record == FALSE): ?>
                <tr>
                  <td colspan="8">
                            <?php
                                $message = "There are currently No Attendance Records for this particular student";
                                echo $message;
                            ?>
                        </td>
                </tr>
            <?php else: ?>
              <?php foreach($staff_attendance_record as $sar): ?>
                <tr>
                  <td style="text-align:center;">&nbsp;<?php
                              echo $sar['year'];
                            ?>
                  </td>
                  <td style="text-align:center;">&nbsp;
                    <?php
                      if($sar['Present'] !== NULL){
                        echo $sar['Present'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td style="text-align:center;">&nbsp;
                    <?php
                      if($sar['Absent'] !== NULL){
                        echo $sar['Absent'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td style="text-align:center;">&nbsp;
                    <?php
                      if($sar['Sick'] !== NULL){
                        echo $sar['Sick'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td style="text-align:center;">&nbsp;
                    <?php
                      if($sar['Permitted'] !== NULL){
                        echo $sar['Permitted'];
                      }
                      else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <td style="text-align:center;">
                    <?php 
                      $total_days = $sar['Present'] + $sar['Absent'] + $sar['Sick'] + $sar['Permitted'];
                      if($sar['Present'] !== NULL){
                        echo $sar['Present'];
                      }
                      else{
                        echo 0;
                      }
                      echo '/'.$total_days; 
                    ?>
                  </td>
              <?php endforeach; ?>
            <?php endif;?>
            </tbody>
        </table>
          
      </div>
    </div>