
<div class="white-area-content">
<div class="db-header clearfix">
<div class="page-header-title"> <!-- <span class="fa fa-user" style="font-size: 30px;"></span> --><?php echo $title; ?></div>
</div>
<div class="clearfix">

<!-- <div class="col-sm-10 "> -->
<!-- <h4><?php echo $title;?></h4> -->

      <div class="error_message_color" id="error_Message">
        <?php //echo $this->session->flashdata('error_message'); ?>
        <?php 
          if($this->session->flashdata('aqua_errors')){
            echo '&nbsp<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp'.  $this->session->flashdata('qua_errors');
          }
        ?>
      </div>
        <!-- Result message-->

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>



<?php echo form_open('staffs/add_staff_qualification/'.$staff_id); ?>

 <input type="hidden" name="staff_id"  class="form-control" value="<?php echo $staff['staff_id'];?>"><br>
 
<div class="form-group">
 <label class="col-sm-2">Certification</label>
 <div class="col-sm-10">
<!--  <div id="successMessage" class="success_message_color">
  <?php 
    if($this->session->flashdata('success_message')){
      echo $this->session->flashdata('success_message');
    }
  ?>
</div>
<div id="errorMessage" class="error_message_color">
  <?php
    if($this->session->flashdata('error_message')){
      echo $this->session->flashdata('error_message');
    }

    if($this->session->flashdata('errors')){
      echo $this->session->flashdata('errors');
    }
  ?>
</div> -->
 	<select name="certification" class="form-control" >
 		<option value="">--option--</option>
 		<option <?php echo set_select('certificate', 'Masters', $this->input->post('certificate')); ?> value="Masters">Masters</option>
 		<option <?php echo set_select('certificate', 'Postgraduate', $this->input->post('certificate')); ?> value="Postgraduate">Postgraduate</option>
 		<option <?php echo set_select('certificate', 'Undergraduate', $this->input->post('certificate')); ?> value="Undergraduate">Undergraduate</option>
 		<option <?php echo set_select('certificate', 'Advanced diploma', $this->input->post('certificate')); ?> value="Advanced diploma">Advanced diploma</option>
 		<option <?php echo set_select('certificate', 'Diploma', $this->input->post('certificate')); ?> value="Diploma">Diploma</option>
 		<option <?php echo set_select('certificate', 'Certificate', $this->input->post('certificate')); ?> value="Certificate">Certificate</option>
 	</select>
  <div class="error_message_color"><?php echo form_error('certificate'); ?></div>
 </div>	
</div>
<br><br>
<div class="form-group">
 <label class="col-sm-2">Description</label>
 <div class="col-sm-10">
 	<input type="text" name="description" placeholder="eg Bachelor of art with ed" class="form-control" value="<?php echo set_value('description'); ?>" />
    <div class="error_message_color"><?php echo form_error('description'); ?></div>
 </div>	
</div>
<br><br>
<div class="form-group">
 <label class="col-sm-2">Completion date</label>
 <div class="col-sm-10">
 	<input type="text" name="year" placeholder="eg 2017" class="form-control" value="<?php echo set_value('year'); ?>" />
    <div class="error_message_color"><?php echo form_error('year'); ?></div>
 </div>	
</div>
<br><br>
<div class="form-group">
 <label class="col-sm-2">Institute/University</label>
 <div class="col-sm-10">
 	<input type="text" name="university" placeholder="eg. Mzumbe University" class="form-control" value="<?php echo set_value('university'); ?>" />
    <div class="error_message_color"><?php echo form_error('university'); ?></div>
 </div>	
</div>
<br><br>

<div class="form-group">
 <label class="col-sm-2"> </label>
 <div class="col-sm-10">
 	<?php //if($this->session->userdata('second') === "A" && $x == TRUE): ?>
 		<!-- <input type="hidden" name="posted_by" value="second_master" /> -->	<!-- For Redirection -->
 		<!-- <input type="hidden" name="certify" value="yes" /> -->
 	<?php //else: ?>
 		<input type="hidden" name="posted_by" value="account_owner" />	<!-- For Redirection -->
 		<input type="hidden" name="certify" value="no" />
 	<?php //endif; ?>
 	<input type="submit" name="submit" class="form-control btn-primary" value="Save qualification"><br>
 </div>	
</div>


<?php echo form_close();?>
 <!-- </div> -->
 </div>
 </div>