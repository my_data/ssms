
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline text-right"> 

                <div class="form-group has-feedback no-margin">
            
              	<?php if($is_monthly === "YES"): ?>
                	<?php echo form_open('staffs/monthly_attendance_report/'.$date); ?>
                <?php endif; ?>
                <?php if($is_daily === "YES"): ?>
                	<?php echo form_open('staffs/daily_attendance/'.$date); ?>
                <?php endif; ?>
              <!-- <input type="text" class="form-control input-sm" name="search_staff" placeholder="Search ..." id="form-search-input" />-->
 
      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_staff" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
                        <?php echo form_close(); ?>
                            
                 
            
            </div>
            &nbsp;
            <a href="<?php echo base_url() . 'staffs/daily_attendance/'.date('Y-m-d'); ?>" class="btn btn-primary btn-sm">Today</a>
            &nbsp;
            <a href="<?php echo base_url() . 'staffs/monthly_attendance_report/'.explode('-', date('Y-m-d'))[0].'-'.explode('-', date('Y-m-d'))[1]; ?>" class="btn btn-primary btn-sm">Monthly</a>
            
          </div>
      </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

