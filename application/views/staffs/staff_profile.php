


        <div class="table-responsive">
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
            <table class="table table-striped table-hover table-condensed table-bordered">
                
                <thead>
                  <tr class="table-header">
                    <td  style="width:30%; text-align:center;">Title</td>
                    <td  style="width:70%; text-align:center;">Description</td>
                  </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>&nbsp;Staff Names</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['firstname'] . " " . $staff_info['middlename'] . " " . $staff_info['lastname']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Phone No.</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['phone_no']; ?></td>
                </tr>
                <?php if($this->session->userdata('manage_staff_qualification') == 'ok'): ?>
                  <tr>
                    <td><b>&nbsp;Qualification(s)</b></td>
                    <td><table class="table table-condensed table-bordered table-striped table-hover">
                      <?php 
                        if($certifications == FALSE){
                          echo "&nbsp;&nbsp;None";
                        }
                        else{
                      ?>
                      <tr>
                        <td></td>
                        <td align="right"><a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myAddQualificationModal" data-placement="bottom" title="Add qualification" ><span class="fa fa-plus"></span></a></td>
                      </tr>
                      <?php
                          foreach ($certifications as $key => $x) {
                      ?>
                      <?php if($x['description'] == ""): ?>
                            <tr>
                              <td>The are currently no qualification added</td>
                            </tr>
                      <?php else: ?>
                            <tr>
                              <td width="55%">
                                <?php 
                                  echo $x['description'] . ", " . $x['university'] . ", " . $x['completion_date'];
                                ?>
                              </td>
                              <td width="15%">
                                <a href="#" data-target="#myEditModal<?php echo $x['q_id']; ?>" data-toggle="modal" title="Edit" data-placement="bottom" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
                                <a href="<?php echo base_url() . 'staffs/certify/'.$x['staff_id'].'/'.$x['q_id']; ?>" title="Certify" data-placement="bottom" 
                                <?php 
                                if($x['certified'] == "yes"){ 
                                  echo 'class="btn btn-primary btn-xs"'; 
                                }
                                else{
                                  echo 'class="btn btn-info btn-xs"'; 
                                } 
                                ?>
                                onClick="return confirm('Certify qualification?');">C</a>
                                <a href="<?php echo base_url() . 'staffs/remove_qualification/'.$x['staff_id'].'/'.$x['q_id']; ?>" title="Delete" data-placement="bottom" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to remove this qualification?');"><span class="glyphicon glyphicon-trash" onClick="return confirm('Are you sure you want to remove this qualification?');"></span></a>
                                
                              </td>
                            </tr>
                    <?php endif; ?>
                      <?php
                          }
                        }
                      ?>
                      </table>
                    </td>
                  </tr>
                <?php endif; ?>
                <tr>
                  <td><b>&nbsp;Gender</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['gender']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Email</b></td>
                  <td>
                    <?php
                      if($staff_info['email'] !== ""){
                         echo "&nbsp;&nbsp;".$staff_info['email'];
                      }
                      else{
                        echo "&nbsp;&nbsp;None";
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td><b>&nbsp;Next Of Kin</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['nok_fn'] . " " . $staff_info['nok_ln']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;NOK Phone No. </b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['nok_phone_no']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Relationship Type</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['relation_type']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;NOK Email</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['nok_email']; ?></td>
                </tr>
                
                <tr>
                  <td><b>&nbsp;Address</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['nok_box']; ?></td>
                </tr>
                <tr>
                  <td><b>&nbsp;Region</b></td>
                  <td>&nbsp;&nbsp;<?php echo $staff_info['nok_region']; ?></td>
                </tr>
              </tbody>
            </table>
          </div>




<!-- START MODAL EDIT QUALIFICATION -->
<?php foreach ($certifications as $x): ?>

    <div class="modal fade" id="myEditModal<?php echo $x['q_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Edit Qualification: : " . $x['firstname'] . " " . $x['lastname']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("staffs/save_edit_qualification", $attributes);
                ?>            
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="qualification" >Qualification</label>
                        <div class="col-sm-8">
                        
            
                <select name="certification" class="form-control" required>
                  <option value="">--option--</option>
                  <option <?php if($x['certification'] === "Masters"){ echo "selected"; } ?> value="Masters">Masters</option>
                  <option <?php if($x['certification'] === "Postgraduate"){ echo "selected"; } ?> value="Postgraduate">Postgraduate</option>
                  <option <?php if($x['certification'] === "Undergraduate"){ echo "selected"; } ?> value="Undergraduate">Undergraduate</option>
                  <option <?php if($x['certification'] === "Advanced Diploma"){ echo "selected"; } ?> value="Advanced Diploma">Advanced Diploma</option>
                  <option <?php if($x['certification'] === "Diploma"){ echo "selected"; } ?> value="Diploma">Diploma</option>
                  <option <?php if($x['certification'] === "Certificate"){ echo "selected"; } ?> value="Certificate">Certificate</option>
                </select>
                        </div>
                    </div>    
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="description" >Description</label>
                    <div class="col-sm-8">
                      <input type="text" name="description" class="form-control" value="<?php echo $x['description']; ?>" required>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="completion_date" >Completion date</label>
                    <div class="col-sm-8">
                      <input type="text" name="year" class="form-control" value="<?php echo $x['completion_date']; ?>" required>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="university" >Institute/University</label>
                    <div class="col-sm-8">
                      <input type="text" name="university" class="form-control" value="<?php echo $x['university']; ?>" required>
                    </div>
                  </div>          
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="q_id" value="<?php echo $x['q_id']; ?>" />
                <input type="hidden" name="staff_id" value="<?php echo $x['staff_id']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">UPDATE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php endforeach; ?>
<!-- END MODAL EDIT QUALIFICATION -->


<!-- START MODAL ADD QUALIFICATION -->

    <div class="modal fade" id="myAddQualificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Qualification: : " . $x['firstname'] . " " . $x['lastname']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("staffs/save_qualification", $attributes);
                ?>            
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="qualification" >Qualification</label>
                        <div class="col-sm-8">
                        
            
                <select name="certification" class="form-control" required>
                  <option value="">--option--</option>
                  <option value="Masters">Masters</option>
                  <option value="Postgraduate">Postgraduate</option>
                  <option value="Undergraduate">Undergraduate</option>
                  <option value="Advanced Diploma">Advanced Diploma</option>
                  <option value="Diploma">Diploma</option>
                  <option value="Certificate">Certificate</option>
                </select>
                        </div>
                    </div>    
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="description" >Description</label>
                    <div class="col-sm-8">
                      <input type="text" name="description" class="form-control" required>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="completion_date" >Completion date</label>
                    <div class="col-sm-8">
                      <input type="text" name="year" class="form-control" required>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="university" >Institute/University</label>
                    <div class="col-sm-8">
                      <input type="text" name="university" class="form-control" required>
                    </div>
                  </div>          
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="staff_id" value="<?php echo $x['staff_id']; ?>" />
                <input type="hidden" name="certify" value="yes" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" name="assign" value="assign" class="btn btn-primary">ADD</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- END MODAL ADD QUALIFICATION -->
