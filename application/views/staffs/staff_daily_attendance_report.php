<?php echo form_open('staffs/daily_attendance'); ?>
<div class="form-group">

<div class="col-xs-1">
  <label>Date: </label>
</div>

<div class="col-xs-3">
  <input class="form-control" type="date" name="tarehe" required />
</div>

<div class="col-xs-1">
<input type="submit" name="search_record" value="Search" class="btn btn-primary btn-sm"/>
</div>
</div>
<?php echo form_close(); ?>
<br/><br/><br/>
<div class="row">

<h4 align="center">Summary</h4>
<div id="summary_table" class="table-responsive">
 <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <!-- <tr class="table-header">
        <td width="50%" align="center">Description</td>
        <td width="50%" align="center">No of staffs</td>
      </tr> -->
    </thead>
    <tbody>
      <!-- <?php foreach($no_of_staffs as $nos): ?>
        <tr>
          <td align="center"><?php echo $nos['description']; ?></td>
          <td align="center"><?php echo $nos['no_of_staffs']; ?></td>
        </tr>
      <?php endforeach; ?> -->
      <tr  class="table-header">
        <td width="20%" align="center">Description</td>
        <?php foreach($no_of_staffs as $nos): ?>
            <td align="center"><?php echo $nos['description']; ?></td>
        <?php endforeach; ?>
      </tr>
      <tr>
        <td width="20%" align="center"><strong>No of Staffs</strong></td>
        <?php foreach($no_of_staffs as $nos): ?>
            <td align="center"><?php echo $nos['no_of_staffs']; ?></td>
        <?php endforeach; ?>
      </tr>
    </tbody>
  </table>
</div>

<h4 align="center">Attendance</h4>

<div id="exhaustive_table" class="table-responsive">
 <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <td align="center" width="50%">Student Names</td>
        <td align="center" width="35%">Status</td>
        <?php if($this->session->userdata('manage_department') == 'ok'): ?>
          <td align="center" width="15%"></td>
          <td align="center" width="5%">Edit</td>
        <?php endif; ?>
      </tr>
    </thead>
    <tbody>
    <?php if ($staffs == FALSE): ?>
      <tr>
        <td colspan="2">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Attendance Records";
                      echo $message;
                  ?>
              </td>
      </tr>
  <?php else: ?>
      <?php foreach($staffs as $staff): ?>
        <tr>
          <td align="center"><?php echo $staff['names']; ?></td>
          <td align="center"><?php echo $staff['description']; ?></td>
          <?php if($this->session->userdata('manage_staff_attendance') == 'ok'): ?>
            <td align="center">
              <?php echo form_open('staffs/update_staff_attendance'); ?>
                <select name="att_id" class="form-control">
                  <option value="">--Choose--</option>
                  <?php foreach($attendance_records as $ar): ?>
                    <option value="<?php echo $ar['att_id']; ?>"><?php echo $ar['description']; ?></option>
                  <?php endforeach; ?>
                </select>
                <div class="error_message_color">
                  <?php echo form_error('att_id'); ?>
                </div>
            </td>
            <td align="center">              
                <input type="hidden" name="v" value="<?php echo $v; ?>" />
                <input type="hidden" name="offset" value="<?php echo $offset; ?>" />

                <input type="hidden" name="staff_id" value="<?php echo $staff['staff_id']; ?>" />
                <input type="hidden" name="date_" value="<?php echo $staff['date_']; ?>" />         
                <input type="submit" name="update_staff_attendance" value="UPDATE" class="btn btn-primary btn-xs" />
              <?php echo form_close(); ?>
            </td>
          <?php endif; ?>
      <?php endforeach; ?>
    </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
<?php endif; ?>