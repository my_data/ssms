<div class="col-md-9">
	<div class="white-area-content">
	<div class="db-header clearfix">

	<div class="page-header-title"><!--  <span class="fa fa-book"></span>&nbsp; --><?php //echo $title; ?>
	</div>
	    <div class="db-header-extra form-inline"> 

	        <div class="form-group has-feedback no-margin">

					<?php echo form_open('staffs/user_log_records/'.$user_id); ?>

   <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
					<?php echo form_close(); ?>

				</div>
			</div>

		<!-- <a href="<?php echo base_url() . 'staffs/add_department'; ?>" class="btn btn-primary btn-sm">Add Department</a> -->

</div>
<div class="table table-responsive">
		<table class="table table-striped table-hover table-condensed table-bordered">
			<thead>
				<tr class="table-header">
					<td>Activity</td>
					<!-- <td>Table Name</td> -->
					<td>Time</td>
					<td>Source</td>
					<!-- <td>Destination</td> -->
					<td>Action</td>
					<!-- <td>User</td> -->
				</tr>
			</thead>
			<tbody>
			<?php if ($activity_log_records == FALSE): ?>
		    	<tr>
			    	<td colspan="4">There are currently No Match for your search</td>
			    </tr>
			<?php else: ?>
				<?php
					foreach ($activity_log_records as $key => $record):
				?>
					<tr>
						<td><?php echo $record['activity']; ?></td>
						<!-- <td><?php echo $record['tableName']; ?></td> -->
						<td><?php echo $record['time']; ?></td>
						<td><?php echo $record['source']; ?></td>
						<!-- <td><?php echo $record['destination']; ?></td> -->
						<!-- <td><?php echo $record['user_id']; ?></td>	 -->
						<td align="center"><a href="#" data-toggle="modal" data-target="#myModalLogEvent<?php echo $record['id']; ?>" title="View More" data-placement="bottom" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></td>
					</tr>
				<?php
					endforeach;
				?>
				<?php endif; ?>
			</tbody>
		</table>
		<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
		</div>
			<div align="left">
				<!-- <?php
					if($display_back === "OK"){
				?>
					<a href="<?php echo base_url() . 'admin/activity_log_records'; ?>" class="btn btn-primary btn-xs">Back</a>
				<?php
					}
				?> -->
			</div>
		<div align="right">
			<?php echo $links; ?>
		</div>
	</div>





<!-- START MODAL -->
<?php foreach($activity_log_records as $record): ?>
  <div class="modal fade" id="myModalLogEvent<?php echo $record['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Names: " . $record['username']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">                      
                    <table class="table table-condensed table-hover table-striped table-bordered">
                      <tr class="table-header">
                        <td>Title: </td>
                        <td>Description</td>
                      </tr>
                      <tr>
                        <td>Activity: </td>
                        <td><?php echo $record['activity']; ?></td>
                      </tr>
                      <tr>
                        <td>User ID: </td>
                        <td><?php echo $record['user_id']; ?></td>
                      </tr>
                      <!-- <tr>
                        <td>Names: </td>
                        <td><?php echo $record['username']; ?></td>
                      </tr> -->
                        <td>Table Name: </td>
                        <td><?php echo $record['tableName']; ?></td>
                      </tr>
                      <tr>
                        <td>Source: </td>
                        <td><?php echo $record['source']; ?></td>
                      </tr>
                      <tr>
                        <td>Destination: </td>
                        <td><?php echo $record['destination']; ?></td>
                      </tr>
                    </table>
            </div>
          </div>
    </div>
</div>
<?php endforeach; ?>
<!-- END MODAL -->
