
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <!-- <span class="fa fa-user" style="font-size: 30px;"></span> --><?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<?php echo form_open('staffs/my_announcements/'.$staff_id); ?>
<input type="text" class="form-control input-sm" name="search_announcement" placeholder="Search ..." id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="submit" class="btn btn-primary btn-sm dropdown-toggle" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
<?php echo form_close(); ?>
        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> Like</a></li>
          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> Exact</a></li>
          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> Name</a></li>
        </ul> -->
      </div><!-- /btn-group -->
</div>
</div>

</div>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td width="30%">Time</td>
      <td width="55%">Title</th>
      <td width="15%">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php if($my_announcements == FALSE): ?>
      <tr>
        <td colspan="3">
                  <?php
                      $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Announcements";
                      echo $message;
                  ?>
              </td>
      </tr>
    <?php else: ?>
    <?php
      foreach ($my_announcements as $key => $my_announcement):
    ?>
      <tr>
        <td><?php echo $my_announcement['time']; ?></td>
        <td><?php echo $my_announcement['heading']; ?></td>
        <td>        
          <?php echo form_open('staffs/mark_as_read'); ?>
          <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#announcementModal<?php echo $my_announcement['announcement_id']; ?>" data-placement="bottom" title="Read More">R</a>
            &nbsp;&nbsp;
            <input type="hidden" name="announcement_id" value="<?php echo $my_announcement['announcement_id']; ?>" /> 
            <input type="hidden" name="staff_id" value="<?php echo $my_announcement['staff_id']; ?>" /> 
            <?php
              if($my_announcement['is_read'] === "unread"){
            ?>
                <input type="submit" name="" value="<?php echo $my_announcement['is_read']; ?>" class="btn btn-primary btn-xs" onClick="alert('Marked as read');" data-placement="bottom" title="Mark as read" />
            <?php
              }
              else{
            ?>
                <input type="submit" name="" value="<?php echo $my_announcement['is_read']; ?>" class="btn btn-default btn-xs" disabled />
            <?php
              }
            ?>
          <?php echo form_close(); ?>     
        </td>
      </tr>
    <?php
      endforeach;
      endif;
    ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'admin/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div>
</div>
</div>




<!-- START ANNOUNCEMENT -->
<?php foreach ($my_announcements as $key => $my_announcement): ?>
<div class="modal fade" id="announcementModal<?php echo $my_announcement['announcement_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Glory Glory Man Utd:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              <?php echo $my_announcement['msg']; ?>                             
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<!--END ANNOUNCEMENT-->
