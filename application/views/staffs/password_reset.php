
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('staffs/password_reset'); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_staff" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>

</div>

<?php if($this->session->userdata('reset_password') == 'ok'): ?>
	<a href="<?php echo base_url() . 'staffs/reset_all'; ?>" class="btn btn-danger btn-sm" onClick="return confirm('Are u you sure you want to reset all passwords, all users will have a single default password');">Reset All</a>
<?php endif; ?>
</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Firstname</td>
			<td>Middlename</th>
			<td>Lastname</th>
			<td>Gender</td>
			<td>Phone#</td>
			<td>Email</td>
			<td>Role</td>
			<td>Status</td>
			<?php if($this->session->userdata('reset_password') == 'ok'): ?>
				<td align="center">Reset</td>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($staffs == FALSE): ?>
        <tr>
          <td colspan="9">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Staffs";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php
			foreach ($staffs as $key => $staff):
		?>
			<tr>
				<td><?php echo $staff['firstname']; ?></td>
				<td><?php echo $staff['middlename']; ?></td>
				<td><?php echo $staff['lastname']; ?></td>
				<td><?php echo $staff['gender']; ?></td>
				<td><?php echo $staff['phone_no']; ?></td>
				<td><?php echo $staff['email']; ?></td>
				<td><?php echo $staff['user_role']; ?></td>
				<td><?php echo $staff['status']; ?></td>
				<?php if($this->session->userdata('reset_password') == 'ok'): ?>
					<td align="center">
						<?php echo form_open('staffs/reset/'.$staff['staff_id']); ?>
							<input type="submit" name="reset" class="btn btn-primary btn-xs" onClick="return confirm('Are you sure you want to do this?');" data-target="tooltip" data-placement="bottom" title="Reset Password" value="R">
						<?php echo form_close(); ?>
					</td>
				<?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
	<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'staffs/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>
