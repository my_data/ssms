
<div class="white-area-content">
<div class="db-header clearfix">
<div class="page-header-title"><?php echo $title ; ?>
</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<?php echo form_open('staffs/edit_announcement/'.$ar['announcement_id']); ?>
	<!-- <input type="text" name="date" id="date" value="<?php echo date('Y-m-d-H-m-s'); ?>" /> -->
	<div class="form-group">
			<label class="col-sm-2 control-label" for="group_id">Group Name :</label>
			<div class="col-sm-10">
				<!-- <div id="successMessage" class="success_message_color">
					<?php 
						if($this->session->flashdata('success_message')){
							echo $this->session->flashdata('success_message');
						}
					?>
				</div>
				<div id="errorMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('error_message')){
							echo $this->session->flashdata('error_message');
						}
					?>
				</div> -->
				<input type="text" class="form-control" name="group_id" value="<?php echo $ar['group_name']; ?>" readonly />
				<div class="error_message_color">
					<?php echo form_error('group_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="heading">Heading :</label>
			<div class="col-sm-10">
				<input value="<?php echo $ar['heading']; ?>" type="text" name="heading" class="form-control" />
				<div class="error_message_color">
					<?php echo form_error('heading'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="announcement">Announcement :</label>
			<div class="col-sm-10">
				<textarea name="msg" rows="10" cols="90" id="inci-area" >
						<?php echo $ar['msg']; ?>
				</textarea>
				<div class="error_message_color">
					<?php echo form_error('msg'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="staff_id" value="<?php echo $this->session->userdata('staff_id'); ?>" />
			<input type="hidden" name="announcement_id" value="<?php echo $ar['announcement_id']; ?>" />
			<input type="submit" class="form-control btn btn-primary" name="update_announcement" value="Update Announcement" onClick="Save Changes?" />
		</div>
		<br/><br/>

</form>
