
<div class="white-area-content">
<div class="db-header clearfix">

 <!--success and error message-->
 <h5 class="text-info text-center"><?php if($this->session->flashdata('qua')): ?>
    <p><?php echo $this->session->flashdata('qua'); ?><?php endif; ?></p>
</h5>
<h5 class="text-info text-danger text-center"><?php if($this->session->flashdata('qua_errors')): ?>
    <p><?php echo $this->session->flashdata('qua_errors'); ?></p>
<?php endif; ?></h5>
 <div class="page-header-title"> <span class="fa fa-user" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 


        <div class="form-group has-feedback no-margin">

<?php echo form_open('staffs/index'); ?>

   <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_staff" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
    </div>
 <?php echo form_close(); ?>

</div>
<?php if($this->session->userdata('manage_staff') == 'ok'): ?>
	<a href="<?php echo base_url() . 'staffs/new_staff'; ?>" class="btn btn-primary btn-sm">Add Staff</a>
<?php endif; ?>
</div>



</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Names</td>
			<td>Gender</td>			
			<td>Phone#</td>
			<td>Email</td>
			<td>Role</td>			
			<td align="center">Status</td>
			<?php if($this->session->userdata('view_staff_details') == 'ok'): ?>
				<td align="center">Actions</td>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
	<?php if ($staffs == FALSE): ?>
    	<tr>
	    	<td colspan="4">There are currently No Match for your search</td>
	    </tr>
	<?php else: ?>
		<?php
			$x = 0;
			foreach ($staffs as $key => $staff):
		?>
			<tr>
				<td><?php echo $staff['firstname'] . ' ' .$staff['middlename']." ". $staff['lastname']; ?></td>
				<td><?php echo $staff['gender']; ?>
				<td><?php echo $staff['phone_no']; ?>
				<td><?php echo $staff['email']; ?>
				<td><?php echo $staff['user_role']; ?></td>

				<td align="center"><?php echo $staff['status']; ?></td>
			<?php if($this->session->userdata('view_staff_details') == 'ok' || $this->session->userdata('manage_staff') == 'ok'): ?>
				<td align="center">
	
				<?php if($this->session->userdata('view_staff_details') == 'ok'): ?>	
                	<a href="<?php echo base_url() . 'staffs/profile/'.$staff['staff_id']; ?>" data-toggle="tooltip" data-placement="bottom" title="View More Details" class="btn btn-primary btn-xs" ><span class="fa fa-user"></span></a>
				<?php endif; ?>
				&nbsp;
				<?php if($this->session->userdata('manage_staff') == 'ok'): ?>	
					<a href="<?php echo base_url() . 'staffs/edit_staff/'.$staff['staff_id']; ?>" class="btn btn-xs btn-primary" data-target="tooltip" data-placement="bottom" title="Edit <?php echo $staff['firstname'] . " " . $staff['lastname'];?>"><span class="fa fa-pencil"></span></a>
				<?php endif; ?>
				</td>
			<?php endif; ?>	
			</tr>
		<?php
			$x++;
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
	<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'staffs/index'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>