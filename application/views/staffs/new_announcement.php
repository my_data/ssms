<div class="white-area-content">
<div class="db-header clearfix">
<div class="page-header-title"><?php echo $title ; ?>
</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<?php echo form_open('staffs/new_announcement'); ?>
	<!-- <input type="text" name="date" id="date" value="<?php echo date('Y-m-d-H-m-s'); ?>" /> -->
	<div class="form-group">
			<label class="col-sm-2 control-label" for="group_id">Group Name :</label>
			<div class="col-sm-10">
				<!-- <div id="successMessage" class="success_message_color">
					<?php 
						if($this->session->flashdata('success_message')){
							echo $this->session->flashdata('success_message');
						}
					?>
				</div>
				<div id="errorMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('error_message')){
							echo $this->session->flashdata('error_message');
						}
					?>
				</div>
				<div id="warningMessage" class="error_message_color">
					<?php
						if($this->session->flashdata('exist_message')){
							echo $this->session->flashdata('exist_message');
						}
					?>
				</div> -->

				<select name="group_id" class="form-control" id="group_id" >
					<option value="">--Choose Group--</option>
					<?php foreach($groups as $group): ?>
						<option value="<?php echo $group['group_id']; ?>" <?php echo  set_select('group_id', ''.$group["group_id"].'', $this->input->post('group_id')); ?>><?php echo $group['group_name']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('group_id'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="heading">Heading :</label>
			<div class="col-sm-10">
				<input value="<?php echo set_value('heading'); ?>" type="text" name="heading" class="form-control" />
				<div class="error_message_color">
					<?php echo form_error('heading'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="announcement">Announcement :</label>
			<div class="col-sm-10">
				<textarea name="msg" rows="10" cols="90" id="inci-area" class="form-control">
						<?php echo set_value('msg'); ?>
				</textarea>
				<div class="error_message_color">
					<?php echo form_error('msg'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<div class="form-group">
			<input type="hidden" name="staff_id" value="<?php echo $this->session->userdata('staff_id'); ?>" />
			<input type="submit" class="form-control btn btn-primary" name="post_announcement" value="Post Announcement" />
		</div>
		<br/><br/>

</form>
