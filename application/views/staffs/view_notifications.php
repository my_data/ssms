
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <!-- <span class="fa fa-user" style="font-size: 30px;"></span> --><?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">

        
      </div>
</div>
</div>


</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Time</td>
			<td>Message</th>
			<td>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($notification_msgs as $key => $msg):
		?>
			<tr>
				<td><?php echo $msg['time_']; ?></td>
				<td><?php echo $msg['msg']; ?></td>
				<!-- <td><?php echo $msg['notification_status']; ?></td> -->
				<td>				
 					<?php echo form_open('notification/mark_read'); ?>
	 					&nbsp;&nbsp;
						<input type="hidden" name="notification_id" value="<?php echo $msg['notification_id']; ?>" />	
						<input type="hidden" name="staff_id" value="<?php echo $msg['staff_id']; ?>" />	
						<?php
							if($msg['notification_status'] === "unread"){
						?>
								<input type="submit" name="" value="<?php echo $msg['notification_status']; ?>" class="btn btn-primary btn-xs" onClick="return confirm('Marked as read');" data-toggle="tooltip" data-placement="bottom" title="Mark as read" />
						<?php
							}
							else{
						?>
								<input type="submit" name="" value="<?php echo $msg['notification_status']; ?>" class="btn btn-default btn-xs" disabled />
						<?php
							}
						?>
					<?php echo form_close(); ?>			
				</td>
			</tr>
		<?php
			endforeach;
		?>
	</tbody>
</table>
	
</div>
</div>