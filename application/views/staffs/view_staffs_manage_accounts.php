
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-user" style="font-size: 30px;"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('staffs/manage_accounts'); ?>


   <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_staff" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>

</div>



</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Firstname</td>
			<td>Middlename</th>
			<td>Lastname</th>
			<td>Gender</td>
			<td>Phone#</td>
			<td>Email</td>
			<td>Role</td>
			<td>Status</td>
			<?php if($this->session->userdata('activate-deactivate_staff_account') == 'ok' || $this->session->userdata('manage_staff_accounts') == 'ok'): ?>
				<td align="center">Option</td>
			<?php endif; ?>
			<!-- <td>P</td> -->
		</tr>
	</thead>
	<tbody>
	<?php if ($staffs == FALSE): ?>
        <tr>
          <td colspan="10">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Staffs";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php
			foreach ($staffs as $key => $staff):
		?>
			<tr>
				<td><?php echo $staff['firstname']; ?></td>
				<td><?php echo $staff['middlename']; ?></td>
				<td><?php echo $staff['lastname']; ?></td>
				<td><?php echo $staff['gender']; ?></td>
				<td><?php echo $staff['phone_no']; ?></td>
				<td><?php echo $staff['email']; ?></td>
				<td><?php echo $staff['user_role']; ?></td>
				<td><?php echo $staff['status']; ?></td>
				<?php if($this->session->userdata('activate-deactivate_staff_account') == 'ok' || $this->session->userdata('manage_staff_accounts') == 'ok'): ?>
					<td align="center">
						<?php if($this->session->userdata('activate_deactivate_staff_account') == 'ok'): ?>
							<?php if($staff['status'] === 'inactive'): ?>
								<?php echo form_open('staffs/delete_staff/'.$staff['staff_id']); ?>
							<?php endif; ?>
								<input type="hidden" name="status" value="inactive" />
								<input type="submit" name="activate_deactivate_staff" onClick="return confirm('Are you sure you want to do this?');" 
									<?php
										if($staff['status'] === 'active'){
									?>
											class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $staff['staff_id']; ?>"
									<?php
										}
										else{
									?>
											class="btn btn-danger btn-xs"
									<?php
										}
									?>
								 value="
									<?php
										if($staff['status'] === 'active'){
											echo 'A';
										}
										else{
											echo 'D';
										}
									?>
								" data-target="tooltip" data-placement="bottom" title="<?php if($staff['status'] === 'active'){ echo "Deactivate this account"; }else{ echo "Activate this account"; } ?>">		
							<?php endif; ?>		
						<?php if($this->session->userdata('manage_staff_accounts') == 'ok'): ?>
							<a href="<?php echo base_url() . 'staffs/staff_details/'.$staff['staff_id'].''; ?>" class="btn btn-primary btn-xs" data-placement="bottom" title="View More Details">P</a>
						<?php endif; ?>
					</td>
					<?php echo form_close(); ?>

					<?php endif; ?>
			</tr>
		<?php
			endforeach;
		?>
		<?php endif; ?>
	</tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
	<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>



<!--MODAL FOR deacitvating staff-->
<?php foreach ($staffs as $staff): ?>

	<div class="modal fade" id="myModal<?php echo $staff['staff_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Enter Reason For Deactivation</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
            	<?php
            		$attributes = array('class' => 'form-horizontal', 'role' => 'form');
            		echo form_open('staffs/delete_staff/'.$staff['staff_id'], $attributes);
            	?>
                	<div class="form-group">
                    	<label class="col-sm-4 control-label" for="names" >Reason: </label>
                    	<div class="col-sm-8">
                        	<input type="text" class="form-control" name="reason" id="deactivation_reason" />
                    	</div>
                  	</div>
	                <input type="hidden" class="form-control" name="time_" id="staff_id" value="<?php echo date('Y-m-d h:i:s'); ?>" />                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
            	<input type="hidden" name="status" value="active" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach; ?>