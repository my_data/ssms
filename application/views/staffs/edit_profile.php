
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <!-- <span class="fa fa-pencil" style="font-size: 30px;"></span> --><?php echo $title; ?></div>
    
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


	<?php $attributes = array('role' => 'form'); ?>
	<?php echo form_open('staffs/edit_profile/'.$staff_record['staff_id'], $attributes); ?>
		<div class="form-group">
			<!-- <label for="staff_id">Staff#</label> -->
			<div align="center" id="form_headers_styles">
				<h3 class="breadcrumb">Personal Details</h3>
			</div>
			<input type="hidden" name="staff_id" class="form-control" value="<?php echo $staff_record['staff_id']; ?>" required />
		</div>
		<br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="firstname">Firstname :</label>
			<div class="col-sm-10">
				<!-- <div class="success_message_color" id="successMessage">
					<?php echo $this->session->flashdata('success_message'); ?>
				</div>
				<div class="error_message_color" id="errorMessage">
					<?php echo $this->session->flashdata('error_message'); ?>
				</div> -->
				<input type="text" name="firstname" class="form-control" value="<?php echo $staff_record['firstname']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('firstname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="middlename">Middlename :</label>
			<div class="col-sm-10">
				<input type="text" name="middlename" class="form-control" value="<?php echo $staff_record['middlename']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('middlename'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="lastname">Lastname :</label>
			<div class="col-sm-10">
				<input type="text" name="lastname" class="form-control" value="<?php echo $staff_record['lastname']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('lastname'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="dob">Date of birth :</label>
			<div class="col-sm-10">
				<input type="date" name="dob" class="form-control" value="<?php echo $staff_record['dob']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('dob'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gender">Gender :</label>
			<div class="col-sm-10">
				<input type="radio" name="gender" value="Male" <?php if($staff_record['gender'] === "Male"){ echo "checked"; } ?> /> Male
				<input type="radio" name="gender" value="Female" <?php if($staff_record['gender'] === "Female"){ echo "checked"; } ?> /> Female
				<div class="error_message_color">
					<?php echo form_error('gender'); ?>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="marital_status">Marital Status :</label>
			<div class="col-sm-10">
				<select name="marital_status" class="form-control" required >
					<option <?php if($staff_record['marital_status'] === "Single"){ echo "selected='true'"; } ?> value="Single">Single</option>
					<option <?php if($staff_record['marital_status'] === "Married"){ echo "selected='true'"; } ?> value="Married">Married</option>
					<option <?php if($staff_record['marital_status'] === "Divorced"){ echo "selected='true'"; } ?> value="Divorced">Divorced</option>
					<option <?php if($staff_record['marital_status'] === "Widowed"){ echo "selected='true'"; } ?> value="Widowed">Widowed</option>
				</select>
				<div class="error_message_color">
					<?php echo form_error('marital_status'); ?>
				</div>
			</div>
		</div>
		<br/>
		<div align="center" id="form_headers_styles">
			<h3 class="breadcrumb">Contact Details</h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="email">Email :</label>
			<div class="col-sm-10">
				<input type="text" name="email" class="form-control" value="<?php echo $staff_record['email']; ?>" />
				<div class="error_message_color">
					<?php echo form_error('email'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="phone_no">Phone No :</label>
			<div class="col-sm-10">
				<input type="text" name="phone_no" class="form-control" value="<?php echo $staff_record['phone_no']; ?>" required />
				<div class="error_message_color">
					<?php echo form_error('phone_no'); ?>
				</div>
			</div>
		</div>
		<br/>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="update_profile" value="Update" onClick="return confirm('Save Changes?');" />
		</div>
		<br/>
	<?php echo form_close(); ?>
</div>
