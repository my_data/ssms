
<div class="white-area-content">
<div class="db-header clearfix">
  
<strong><div class="page-header-title" style="font-size:16px;"> <span class="" style="font-size:30px;"></span>&nbsp;<?php echo $title; ?></div></strong>
<div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">
        <div class="input-group">
<!-- <div class="input-group-btn"> -->
<?php if($this->session->userdata('view_staff_details') == 'ok'): ?>
  <a href="<?php echo base_url() . 'staffs/profile/'.$staff_id ?>" class="btn btn-primary btn-sm" title="Personal Details" data-placement="bottom">PROFILE</a>
<?php endif; ?>
  &nbsp;
<?php if($this->session->userdata('view_staff_attendance') == 'ok'): ?>
  <a href="<?php echo base_url() . 'staffs/attendance/'.$staff_id ?>" class="btn btn-primary btn-sm" title="Staff Attendance" data-placement="bottom">ATTENDANCE</a>
<?php endif; ?>
<?php if($this->session->userdata('view_staff_role') == 'ok'): ?>
  &nbsp;
  <a href="<?php echo base_url() . 'staffs/staff_details/'.$staff_id ?>" class="btn btn-primary btn-sm" title="Administrative Details" data-placement="bottom">OSD</a>
<?php endif; ?>
<?php if($this->session->userdata('view_staff_activity_log') == 'ok'): ?>
  &nbsp;
  <a href="<?php echo base_url() . 'staffs/user_log_records/'.$staff_id ?>" class="btn btn-primary btn-sm" title="Staff's Activities" data-placement="bottom">ACTIVITY LOG</a>
<?php endif; ?>
 <!-- </div> -->
              </div>
            </div>
          
          </div>
      </div>

<div class="row" style="padding-top:30px;">
    <div class="col-md-12">    	
		<div class="col-md-3 col-xs-6">
	        <img src="<?php echo $image['staff_image'];?>" class="img-responsive img-thumbnail " alt="profile picture" style="height:180px; width:150px;" />
	    </div>
                   
			    

