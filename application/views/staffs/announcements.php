
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <!-- <span class="fa fa-user" style="font-size: 30px;"></span> --><?php echo $title; ?></div>
    <div class="db-header-extra form-inline text-right"> 

        <div class="form-group has-feedback no-margin">

<?php echo form_open('staffs/announcements/'.$staff_id); ?>

   <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_announcement" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
      </div>
<?php echo form_close(); ?>

</div>
<a href="<?php echo base_url() . 'staffs/new_announcement'; ?>" class="btn btn-primary btn-sm">Add Announcement</a>

</div>
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered">
  <thead>
    <tr class="table-header">
      <td align="center" width="55%">Announcement</td>
      <td align="center" width="15%">Group Name</td>
      <td align="center" width="20%">Posted At</td>
      <td align="center" width="10%">Action</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach($announcement_records as $ar): ?>
      <tr>
        <td><?php echo $ar['msg']; ?></td>
        <td align="center"><?php echo $ar['group_name']; ?></td>
        <td align="center"><?php echo $ar['time']; ?></td>
        <td align="center">
        <?php echo form_open('staffs/publish_announcement/'.$ar['announcement_id']); ?>
          <a href="<?php echo base_url() . 'staffs/edit_announcement/'.$ar['announcement_id']; ?>" class="btn btn-primary btn-xs glyphicon glyphicon-pencil" title="Edit Announcement" data-target="tooltip" data-placement="bottom"></a>
          <button type="submit"
              <?php
                  if($ar['status'] === 'show'){
                    $hide_show = "hide";
              ?>
                      class="btn btn-xs btn-success" title="unpublish" data-target="tooltip" data-placement="bottom"
              <?php
                  }
                  else{
                    $hide_show = "show";
              ?>  
                      class="btn btn-xs btn-danger" title="publish" data-target="tooltip" data-placement="bottom"
              <?php
                  }
              ?>
              onClick="return confirm('Are you sure you want to <?php echo $hide_show; ?> this announcement?');">
              <span class="glyphicon glyphicon-off"></span>
          </button>
          <input type="hidden" name="status" value="<?php echo $ar['status']; ?>" />
          <input type="hidden" name="user_id" value="<?php echo $ar['posted_by']; ?>" />
        <?php echo form_close(); ?>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>
<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'admin/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div>
</div>
</div>
