
<div class="white-area-content">
<div class="db-header clearfix">

<div class="page-header-title">
 <?php echo $title; ?>
</div>
    
</div>
<div class="clearfix">
<div class="col-md-12">
      <div class="col-md-6" id="summary_table">
            <h3 align="center">No. of Periods Per Day</h3>
          <div class="table-responsive">
            <table class="table table-striped table-condensed table-bordered">
                <thead>
                  <tr class="table-header">
                    <td width="60%" align="center">Weekday</td>
                    <td width="40%" align="center">No. of Periods</td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($teacher_periods as $teacher_period): ?>
                    <tr>
                      <td align="center"><?php echo $teacher_period['weekday']; ?></td>
                      <td align="center"><?php echo $teacher_period['no_of_periods']; ?></td>
                  <?php endforeach; ?>
                </tbody>
            </table>
          </div>
      </div>

      <div class="col-md-6" id="">
        <h3 align="center"><?php echo $myclasses_title; ?></h3>
          <div class="table-responsive">
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                  <tr class="table-header">
                    <td align="center">Class</td>
                    <td align="center">Stream</td>
                    <td align="center">Timetable</td>
                  </tr>
                </thead>
                <tbody>
                <?php if($class_streams == FALSE): ?>
                  <tr>
                      <td colspan="3">
                            <?php
                                $message = "You have not been assigned to teach any class";
                                echo $message;
                            ?>
                        </td>
                    </tr>
                <?php else: ?>
                  <?php foreach($class_streams as $cs): ?>
                      <tr>
                        <td align="center"><?php echo $cs['class_name']; ?></td>
                        <td align="center"><?php echo $cs['stream']; ?></td>
                        <td align="center">
                        <?php echo form_open('classes/get_class_stream_timetable/'.$cs['class_stream_id']); ?>
                          <input type="hidden" name="ind_tt" value="personal_timetable" />
                          <input type="submit" name="fetch_timetable" value="VIEW" class="btn btn-primary btn-xs">
                        <?php echo form_close(); ?>
                      </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>
          </div> 
      </div>


        <div class="col-md-12">
          <h3 align="center">Weekly Timetable</h3>
          <div id="exhaustive_table">
            <div class="table-responsive">
          <table class="table table-striped table-hover table-condensed table-bordered">
            <thead>
              <tr class="table-header">
                <td align="center">Period#</td>
                <td align="center">Start</td>
                <td align="center">End</td>
                <td align="center">Monday</td>
                <td align="center">Tuesday</td>
                <td align="center">Wednesday</td>
                <td align="center">Thursday</td>
                <td align="center">Friday</td>
              </tr>
            </thead>
            <tbody>
            <?php if($timetable_records == FALSE): ?>
              <tr>
                  <td colspan="8">
                        <?php
                            $message = "You do not have any timetable";
                            echo $message;
                        ?>
                    </td>
                </tr>
            <?php else: ?>
              <?php foreach($timetable_records as $timetable_record): ?>
                  <tr>
                    <td align="center"><?php echo $timetable_record['period_no']; ?></td>
                    <td><?php echo $timetable_record['start_time']; ?></td>
                    <td><?php echo $timetable_record['end_time']; ?></td>
                    <td><?php echo $timetable_record['Monday']; ?></td>
                    <td><?php echo $timetable_record['Tuesday']; ?></td>
                    <td><?php echo $timetable_record['Wednesday']; ?></td>
                    <td><?php echo $timetable_record['Thursday']; ?></td>
                    <td><?php echo $timetable_record['Friday']; ?></td>
                  </tr>
              <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

</div>




