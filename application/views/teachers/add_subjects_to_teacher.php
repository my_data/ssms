
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
</div>

<a href="<?php echo base_url() . 'teachers'; ?>" data-target="#" data-toggle="modal" class="btn btn-primary btn-sm">Back To Teachers</a>

</div>
</div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

	<?php $attributes = array('role' => 'form'); ?>
	 <?php echo form_open('teachers/add_subject_teacher/'.$teacher_id, $attributes); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staffs">Subjects :</label>
			<div class="col-sm-10">
				<select name="subject_id[]" class="chosen-select form-control" multiple required>
					<?php foreach($subject_records as $subject): ?>
						<option 
							<?php 
								foreach ($subject_teacher_records as $str) {
									if($str['subject_id'] === $subject['subject_id']){
										echo "selected='true'";
									}
								}
							?>
						value="<?php echo $subject['subject_id']; ?>"><?php echo $subject['subject_name']; ?></option>
					<?php endforeach; ?>
				</select>
				<div class="error_message_color">
					<?php echo form_error('subject_id[]'); ?>
				</div>
			</div>
		</div>
		<br/><br/><br/>
		<div class="form-group">
			<input type="submit" class="form-control btn btn-primary" name="add_subjects_to_teacher" value="SAVE" />
		</div>
		<br/><br/>
	<?php echo form_close(); ?>
</div>
