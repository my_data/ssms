
  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 


                <?php echo form_open('teacher/assigned_classes/'.$teacher_id); ?>
              
                      <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_student" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                        <?php echo form_close(); ?>

            </div>
          </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table-responsive">
<table class="table table-striped table-hover table-condensed table-bordered ">
  <thead>
    <tr class="table-header">
      <td style="width:30%; text-align:center;">Subject Name</td>
      <td style="width:10%; text-align:center;">Class</td>
      <td style="width:10%; text-align:center;">Stream</td>
      <td style="width:15%; text-align:center;">From</td>
      <td style="width:15%; text-align:center;">To</td>
	<?php if($this->session->userdata('manage_teaching_log') == 'ok' || $this->session->userdata('view_topics') == 'ok'): ?>
      <td style="width:20%; text-align:center;">Action
	<?php if($this->session->userdata('manage_teaching_log') == 'ok'): ?>
          <a href="<?php echo base_url() . 'subjects/get_log_by_teacher_id/' . $teacher_id; ?>" class="btn btn-xs btn-primary fa fa-book pull-right" data-placement="bottom" title="View Log"></a>
	<?php endif; ?>
      </td>
	<?php endif; ?>
    </tr>
  </thead>
  <tbody>
  <?php if($myclasses == FALSE): ?>
    <tr>
        <td colspan="6">
              <?php
                  $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "You have not been assigned to teach any class";
                  echo $message;
              ?>
          </td>
      </tr>
  <?php else: ?>
    <?php $x = 1; //Special for modalID ?>
    <?php foreach($myclasses as $myclass): ?>
      <tr>
        <td style="text-align:center;"><?php echo $myclass['subject_name']; ?></td>
        <td style="text-align:center;"><?php echo $myclass['class_name']; ?></td>
        <td style="text-align:center;"><?php echo $myclass['stream']; ?></td>
        <td style="text-align:center;"><?php echo $myclass['start_date']; ?></td>
        <td style="text-align:center;"><?php echo $myclass['end_date']; ?></td>
<?php if($this->session->userdata('manage_teaching_log') == 'ok' || $this->session->userdata('view_topics') == 'ok'): ?>
        <td style="text-align:center;">
          <a href="<?php echo base_url() . 'teacher/myclasses/' . $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" data-placement="right" title="View Students" class="btn btn-primary btn-xs fa fa-eye"></a> 
          <?php if($this->session->userdata('view_topics') == 'ok'): ?>
            &nbsp;<!--  &nbsp; -->
            <a href="<?php echo base_url() . 'subjects/topics/' . $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" class="btn btn-xs btn-primary fa fa-cog" data-placement="right" title="View Topics"></a> 
          <?php endif; ?>
          <?php if($this->session->userdata('manage_teaching_log') == 'ok'): ?>
            &nbsp; <!-- &nbsp; -->
         <!-- <a href="#" class="btn btn-xs btn-primary fa fa-plus" data-target="#addLog<?php echo $myclass['class_stream_id']; ?>" data-toggle="modal" data-placement="right" title="Add Subject Log"></a>&nbsp;-->
	  <a href="<?php echo base_url() . 'subjects/addLog/'.$myclass['class_stream_id'].'/'.$myclass['subject_id']; ?>" class="btn btn-xs btn-primary fa fa-plus" data-placement="right" title="Add Subject Log"></a>
          <?php endif; ?>
          &nbsp; <!-- &nbsp; -->
          <!-- <a href="<?php echo base_url() . 'subjects/view_log/' . $myclass['class_stream_id'] . '/' . $myclass['subject_id']; ?>" class="btn btn-xs btn-primary fa fa-book" data-target="#viewLog<?php echo $x; ?>" data-toggle="modal" data-placement="right" title="View Log"></a> -->
        </td>
<?php endif; ?>
      </tr>
      <?php $x++; //For Modal ID ?>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
  <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>

<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'students'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>




<!-- START MODAL ADD NEW LOG -->
<?php $x = 1; // Kwa ajili ya Modal ?>
<?php foreach($myclasses as $myclass): ?>
<div class="modal fade" id="addLog<?php echo $myclass['class_stream_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Log:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <?php
                    $attributes = array('class' => 'form-horizontal', 'role' => 'form');
                    echo form_open("subjects/log_topic", $attributes);
                ?>        
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="topic" >Topic Name</label>
                    <div class="col-sm-8">                     
                  <select name="topic_id" class="form-control" required>
                      <option value="">--Choose Topic--</option>
                      <?php foreach ($topics as $key => $topic) {
                      // if($myclass['class_stream_id'] === $topic['class_stream_id'] && $myclass['subject_id'] === $topic['subject_id']){
                      ?>
                          <option value="<?php echo $topic['topic_id']; ?>"><?php echo $topic['topic_name']; ?></option>
                      <?php
                        //}
                      }
                      ?>
                    </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="start_date" >Start Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="start_date" class="form-control" required />
                    </div>
                </div> 
                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label" for="end_date" >End Date</label>
                    <div class="col-sm-8">
                      <input type="date" name="end_date" class="form-control" />
                    </div>
                </div>  -->                      
            </div>            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>" />
                <input type="hidden" name="subject_id" value="<?php echo $myclass['subject_id']; ?>" />
                <input type="hidden" name="class_stream_id" value="<?php echo $myclass['class_stream_id']; ?>" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">SAVE</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php $x++; //Kwa ajili ya modal ?>
<?php endforeach; ?>
<!-- END MODAL ADD NEW LOG -->


logs

<!-- START MODAL VIEW LOG -->
<?php $x = 1; // Kwa ajili ya Modal ?>
<?php foreach($myclasses as $myclass): ?>
<div class="modal fade" id="viewLog<?php echo $x; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "My Log Records:"; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">    
                <table class="table table-condensed table-hover table-striped table-bordered">
                      <tr class="table-header">
                        <!-- <td>No</td> -->
                        <td>Class Name</td>
                        <td>Stream</td>
                        <td>Subject</td>
                        <td>Topic</td>
                        <td>Start date</td>
                        <td>End Date</td>
                        <td>Action</td>
                      </tr>
                      <?php if($logs == FALSE): ?>
                        <tr><td>The are no log records</td></tr>
                      <?php else: ?>
                        <?php foreach($logs as $log): ?>
                          <?php if($log['class_stream_id'] === $myclass['class_stream_id'] && $log['subject_id'] === $myclass['subject_id'] && $log['term_id'] === $myclass['term_id']): ?>
                            <tr>
                            <!-- <td><?php echo $x; ?></td> -->
                            <td><?php echo $log['class_name']; ?></td>
                            <td><?php echo $log['stream']; ?></td>
                            <td><?php echo $log['subject_name']; ?></td>
                            <td><?php echo $log['topic_name']; ?></td>
                            <td><?php echo $log['start_date']; ?></td>
                            <td><?php echo $log['end_date']; ?></td>
                            <td><a href="">??</a></td>
                          </tr>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </table>
            </div>
        </div>
    </div>
</div>
<?php $x++; //Kwa ajili ya modal ?>
<?php endforeach; ?>
<!-- END MODAL VIEW LOG -->
