 <div class="page-header-title"> <h4><strong><i class=" btn btn-default fa fa-clock-o text-success" aria-hidden="true"></i>&nbsp;<?php echo  'Duty from &nbsp&nbsp' .$staff['start_date']. '&nbsp&nbspto&nbsp&nbsp' .$staff['end_date']; ?>&nbsp;&nbsp;: TODAY REPORT</strong></h4></div>
    

</div>
<div class="clearfix">
<div class="jumbotron">
	<div style="float: right;">
		<!-- <a href="" class="btn btn-primary btn-xs" title="Edit Report" data-placement="bottom"><span class="fa fa-pencil"></span></a>&nbsp;<a href="" class="btn btn-primary btn-xs" title="Submit Report" data-placement="bottom">P</a> -->
	</div>
	<div class="table table-responsive">
	<table class="table table-bordered">	
		<tr class="table-header">
			<th>Date</th>
			<th>Day</th>
			<th>Wake up time</th>
			<th>Parade time</th>
            <th>Class start time</th>
		</tr>
		<?php if ($daily == FALSE): ?>
	        <tr>
	          <td colspan="2">
	                    <?php
	                        $message = "Today's report is yet to be sumbitted";
	                        echo $message;
	                    ?>
	                </td>
	        </tr>
	    <?php else: ?>
		<tr style="background-color:#fff;">
			<td><?php echo $daily['date_'];?></td>
			<td><?php echo $daily['day'];?></td>
			<td><?php echo $daily['wake_up_time'];?></td>
			<td><?php echo $daily['parade_time'];?></td>
			<td><?php echo $daily['start_classes_time'];?></td>
		
		</tr>

	</table>
	</div>
	<div class="table table-responsive">
	<table class="table table-responsive table-bordered" >
		<tr><th width="20%">Meals type</th>
         <th >Menu</th>
		</tr>
		<tr>
		    <td>Breakfast </td>
			<td ><?php echo $daily['breakfast'];?></td>
		</tr>
		<tr>
		    <td>Lunch </td>
			<td ><?php echo $daily['lunch'];?></td>
		</tr>
		<tr>
		    <td>Dinner </td>
			<td ><?php echo $daily['dinner'];?></td>
		</tr>
	</table>
	</div>
	<div class="table table-responsive">
	<table class="table table-responsive table-bordered" >
		<tr><th width="20%">Others</th>
         <th >Descriptions</th>
		</tr>
		<tr>
		    <td>Security </td>
			<td ><?php echo $daily['security'];?></td>
		</tr>
		<tr>
		    <td>Events </td>
			<td ><?php echo $daily['events'];?></td>
		</tr>
		
	</table>
	</div>
	<table class="table table-responsive table-bordered" >
		<tr><th><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;Comments</th>
         
		<tr>
		    
			<td style="background-color:#fff;"><?php echo $daily['comments'];?></td>
		</tr>
		
	</table>
	<?php endif; //END IF ya kecheck ifthe report is available ?>
</div>
 </div>
 </div>