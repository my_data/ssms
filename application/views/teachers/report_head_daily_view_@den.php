<h5 class="text-info text-center success_message_color" id="successMessage"><?php if($this->session->flashdata('comm')): ?>
<?php echo $this->session->flashdata('comm'); ?>&nbsp;<i class="fa fa-check" aria-hidden="true"></i><?php endif; ?>
</h5>
<h5 class="text-info text-danger text-center"><?php if($this->session->flashdata('com_error')): ?>
<?php echo $this->session->flashdata('com_error'); ?>&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
<?php endif; ?></h5>
<div class="jumbotron" style="background-color:#fff;">
	<table class="table table-bordered">
	
		<tr>
			<th>Date</th>
			<th>Day</th>
			<th>Wake up time</th>
			<th>Parade time</th>
            <th>Class start time</th>
		</tr>
		<tr style="background-color:#fff;">
			<td><?php echo $daily['date_'];?></td>
			<td><?php echo $daily['day'];?></td>
			<td><?php echo $daily['wake_up_time'];?></td>
			<td><?php echo $daily['parade_time'];?></td>
			<td><?php echo $daily['start_classes_time'];?></td>
		
		</tr>

	</table>
	
	<table class="table table-responsive table-bordered" >
		<tr><th width="20%">Meals type</th>
         <th >Menu</th>
		</tr>
		<tr>
		    <td>Breakfast </td>
			<td ><?php echo $daily['breakfast'];?></td>
		</tr>
		<tr>
		    <td>Lunch </td>
			<td ><?php echo $daily['lunch'];?></td>
		</tr>
		<tr>
		    <td>Dinner </td>
			<td ><?php echo $daily['dinner'];?></td>
		</tr>
	</table>
	<table class="table table-responsive table-bordered" >
		<tr><th width="20%">Others</th>
         <th >Descriptions</th>
		</tr>
		<tr>
		    <td>Security </td>
			<td ><?php echo $daily['security'];?></td>
		</tr>
		<tr>
		    <td>Events </td>
			<td ><?php echo $daily['events'];?></td>
		</tr>
		
	</table>
	<table class="table table-responsive table-bordered" >
		<?php if($this->session->userdata('manage_tod_reports') == 'ok'): ?>
			<tr><th><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;Comments
			&nbsp;&nbsp;<a href="<?php //echo base_url() . 'teachers/head_day_comment/'.$daily['date_']?>" class="fa fa-plus" aria-hidden="true" data-toggle="modal" data-target="#myModal<?php echo $modalID; ?>" data-placement="bottom" title="Add comments" >&nbsp;<strong>Add/Edit comments</strong></a></th>
	         
			<tr>
		<?php endif; ?>
		    
			<td><?php echo $daily['comments'];?>&nbsp;<i class="fa fa-comments-o text-info" aria-hidden="true"></i></td>
		</tr>
		
	</table>
	
</div>
</div>
 </div>



<div class="modal fade" id="myModal<?php echo $modalID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Add Comment : " . $date . '&nbsp'.$day; ?>&nbsp;<i class="fa fa-comments-o text-success"></i></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <?php echo form_open('teachers/add_comment/'.$daily['date_']);?>
					<input type="hidden" value="<?php echo $date;?>">
					<textarea name="msg" cols="54" rows="4" class="form-control" ><?php echo $daily['comments'] ; ?>&nbsp;
					</textarea>               
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
            	<input type="hidden" name="status" value="active" />
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">Edit / Add Comment</button>
            </div>
            <?php echo form_close();?>   
        </div>
    </div>
</div>
