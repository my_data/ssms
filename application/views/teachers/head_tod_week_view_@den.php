<table class="table table-striped table-hover table-condensed table-bordered" >
    <thead>
      <tr class="table-header">
        <td>Day</td>
        <td>Date</td>
        <?php if($this->session->userdata('manage_tod_reports') == 'ok'): ?>
            <td align="center">Action</td>
        <?php endif; ?>
      </tr>
    </thead>

<tbody>
<?php if($routine_records == FALSE): ?>
      <td rowspan="4">
        There are currently no records for teacher on duty reports
      </td>
    <?php else: ?>
<?php $x = 1; foreach ($routine_records as $row) {
        ?>
    <tr>
        <td><?php echo $row['day']; ?></td>
        <td><?php echo $row['date_']; ?></td>
        <?php if($this->session->userdata('manage_tod_reports') == 'ok'): ?>
            <td align="center">
                <a href="<?php echo base_url('teachers/report_head_tod_info/'.$row['date_']); ?>"
             data-placement="right" title="View report" class="btn btn-primary btn-xs"><span  class="glyphicon glyphicon-eye-open"></span></a>&nbsp;
                <a href="#<?php //echo $row['date_']; ?>"
        aria-hidden="true" data-target="#myModal<?php echo $x; ?>" data-toggle="modal" data-placement="bottom" title="Headmaster comments" class="btn btn-primary btn-xs"><span  class="fa fa-comments"></span></a>
            </td>
        <?php endif; ?>
    </tr>
    <?php
      $x++;
    }
    ?>
<?php endif; ?>
</tbody>
</table>
 </div>
 </div>


 <?php $a = 1; foreach ($routine_records as $row): ?>

    <div class="modal fade" id="myModal<?php echo $a; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo "Comments : " . $row['date_']; ?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                  <?php echo $row['comments']; ?>                    
            </div>
        </div>
    </div>
</div>

<?php $a++; endforeach; ?>