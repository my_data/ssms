
<h3 align="center"><?php echo $title; ?></h3><br />

<table id="teacher_data" class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th>Firstname</th>
			<th>Lastname</th>
			<th>Subject</th>
			<th>Class</th>
			<th>Stream</th>
		</tr>
	</thead>
</table>