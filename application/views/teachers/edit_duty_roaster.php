
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

  <?php $attributes = array('role' => 'form'); ?>
  <?php if($record == FALSE): ?>
    <?php echo "No roaster selected to editing"; ?>
  <?php else: ?>
  <?php echo form_open('teachers/edit_duty_roaster/'.$record['id'].'/'.$sid, $attributes); ?>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="teacher_names">Teacher Name :</label>
      <div class="col-sm-10">

        <select class="form-control" name="t_id" id="t_id">
                <option value="">-- Select Teacher --</option>
            <?php foreach ($teacher_names as $dataa): ?>
                <option <?php if($dataa['staff_id'] === $record['teacher_id']){ echo "selected"; } ?> value="<?php echo $dataa['staff_id']; ?>" ><?php echo $dataa['firstname']; ?>&nbsp;<?php echo $dataa['lastname']; ?></option>
            <?php endforeach; ?>
        </select>
        <div class="error_message_color">
          <?php echo form_error('t_id'); ?>
        </div>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="level">Start Date :</label>
      <div class="col-sm-10">
        <input type="date" class="form-control" value="<?php echo $record['start_date']; ?>" name="start_date" placeholder="yyyy-mm-dd" readonly>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="level">End Date :</label>
      <div class="col-sm-10">
        <input type="date" class="form-control" value="<?php echo $record['end_date']; ?>" name="end_date" placeholder="yyyy-mm-dd" readonly>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <input type="hidden" name="id" value="<?php echo $record['id']; ?>" />
      <input type="hidden" name="teacher_id" value="<?php echo $sid; ?>" />
      <input type="submit" class="form-control btn btn-primary" name="update_record" value="Update" />
    </div>
    <br/><br/>
  <?php echo form_close(); ?>
<?php endif; ?>
</div>