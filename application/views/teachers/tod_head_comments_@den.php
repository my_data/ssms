 <div class="page-header-title"> <h4><i class="fa fa-calendar text-success" aria-hidden="true"></i>&nbsp;<?php echo  'Duty from &nbsp&nbsp' .$staff['start_date']. '&nbsp&nbspto&nbsp&nbsp' .$staff['end_date']; ?>&nbsp&nbsp: This week H/Comments</h4></div>
  
</div>
<div class="clearfix">

<table class="table">
	<thead>
		<tr class="table-header">
			<td width="10%">Date</td>
			<td>Comments</td>
		</tr>
	</thead>
	<tbody>	
		<?php $comment = ""; ?>
		<?php if ($daily == FALSE): ?>
	        <tr>
	          <td colspan="2">
	                    <?php
	                        $message = "There are currently No Comments to display for this week";
	                        echo $message;
	                    ?>
	                </td>
	        </tr>
	    <?php else: ?>
		<?php foreach ($daily as $row): ?>
			<?php 
				$comment = ($row['comments'] == "") ? "No comment has been added" : $row['comments']; 
			?>
			<tr>
				<td><?php echo $row['date_']; ?></td>	
				<td><?php echo $comment; ?>&nbsp;...<i class="fa fa-comment-o text-info"></i></td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
</div>
</div>