<div class="white-area-content">
<div class="db-header cliarfix">
<div class="page-header-title">
  
<div class="page-header-title"><?php echo $title; ?></div>
</div>



<?php //echo form_open('teachers/search_attendance_report_summary'); ?>
<div class="text-right">

<?php echo form_open('teachers/school_attendance_report'); ?>


 		 <input type="date" name="date" />


  		<input type="submit" name="search_attendance_report" value="Search" class="btn btn-primary btn-xs" />

<?php echo form_close(); ?>
</div>

&nbsp;&nbsp;&nbsp;
</div>

<br/>
<div class="table table-responsive">

<table class="table table-striped table-hover table-condensed">
  <thead>
    <tr>
      <th>Class Name#</th>
      <th>Present</th>
      <th>Absent</th>
      <th>Sick</th>
      <th>Permitted</th>
      <th>Home</th>
      <th>Total Per Stream</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php if ($att_records == 0 ): ?>
        <tr>
          <td colspan="8">
                    <?php
                      $message = $this->session->flashdata('att_record_message');
                      echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
    <?php foreach($school_attendance_records as $record): ?>
          <?php
            if($record['class_name'] == "Total"){
          ?>
              <tr style="background: grey; font-weight: bold;">
                <td><?php echo $record['class_name'] . " " . $record['stream']; ?></td>
                <td>
                  <?php  
                    if($record['Present']){
                      echo $record['Present'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Absent']){
                      echo $record['Absent'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Sick']){
                      echo $record['Sick'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Permitted']){
                      echo $record['Permitted'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Home']){
                      echo $record['Home'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php echo "<strong>" . $record['total_per_stream'] . "</strong>"; ?>
                </td>
              </tr>
          <?php
            }
            else{
          ?>
              <tr>
                <td><?php echo $record['class_name'] . " " . $record['stream']; ?></td>
                <td>
                  <?php  
                    if($record['Present']){
                      echo $record['Present'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Absent']){
                      echo $record['Absent'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Sick']){
                      echo $record['Sick'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Permitted']){
                      echo $record['Permitted'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td>
                  <?php  
                    if($record['Home']){
                      echo $record['Home'];
                    }
                    else{
                      echo 0;
                    }
                  ?>          
                </td>
                <td style="background: grey; font-weight: bold;">
                  <?php echo "<strong>" . $record['total_per_stream'] . "</strong>"; ?>
                </td>
                <!-- <td>
                  <?php echo form_open('classes/attendance_report'); ?>
                    <input type="text" name="csid" value="<?php echo $record['class_stream_id']; ?>" />
                    <input type="text" name="date" value="<?php echo $record['date_']; ?>" />
                    <input type="submit" name="submit" value="Explore" class="btn btn-primary btn-xs" />
                  <?php echo form_close(); ?>
                </td> -->
                <td><a href='<?php echo base_url() . 'classes/attendance/' . $record['class_stream_id'] . '/' . $record['date_']; ?>' class="btn btn-primary btn-xs" data-target="tooltip" title="Explore More">E</span></a>&nbsp;
                <a href='<?php echo base_url() . 'classes/edit_attendance/' . $record['class_stream_id'] . '/' . $record['date_']; ?>' class="btn btn-primary btn-xs" data-placement="bottom" title="Edit Attendance"><span class="fa fa-pencil"></span></a>
                </td>
              </tr>
          <?php
            }
          ?>
    <?php endforeach; ?>
  <?php endif; ?>
  </tbody>
</table>
</div>
