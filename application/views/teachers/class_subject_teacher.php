
  <div class="white-area-content">
      <div class="db-header clearfix">

  <div class="page-header-title"> <!-- <span class="fa fa-book"></span> -->&nbsp;<?php echo $title; ?>
  </div>
  <div class="db-header-extra form-inline text-right"> 
  <div class="form-group has-feedback no-margin">


          <?php echo form_open('myclass/subject_teachers/'.$class_stream_id); ?>

                <div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_record" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
               <?php echo form_close(); ?>
</div> 

    <a href="<?php echo base_url() . 'classes/get_class_stream_timetable/'.$class_stream_id; ?>" class="btn btn-primary btn-sm">Class Timetable</a> 


      </div>

  </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="table-responsive">
    <table width="100%" class="table table-striped table-hover table-condensed table-bordered">
      <thead>
        <tr class="table-header">
          <td width="40%">Subject Names</td>
          <td width="60%" align="center">Teacher Names</td>
        </tr>
      </thead>
      <tbody>
      <?php if ($teachers == FALSE): ?>
        <tr>
          <td colspan="2">
                <?php
                    $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "No teacher assigned to teach this class";
                    echo $message;
                ?>
            </td>
        </tr>
      <?php else: ?>
        <?php foreach($teachers as $teacher): ?>
          <tr>
            <td>&nbsp;&nbsp;<?php echo $teacher['subject_name']; ?></td>
            <td>&nbsp;&nbsp;<?php echo $teacher['names']; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
      </tbody>
    </table>
    <div style="float: left;">
        <?php echo $x_of_y_entries; ?>
      </div>
</div>

<div align="left">
    <?php
      if($display_back === "OK"){
    ?>
      <a href="<?php echo base_url() . 'students/get_students_by_class_stream/'.$class_stream_id.''; ?>" class="btn btn-primary btn-xs">Back</a>
    <?php
      }
    ?>
  </div>
  <div align="right">
    <?php echo $links; ?>
  </div>