
<div class="white-area-content">
<div class="db-header clearfix">
	
 <div class="page-header-title"> <i class="fa fa-comments-o text-success" aria-hidden="true"></i>&nbsp;</div>
    

</div>
<div class="clearfix">

<table class="table table-responsive table-condensed table-striped table-bordered">
<thead>
	<tr>
		<th>
			Date
		</th>
		<th>Overall day status</th>
		<th>Your Comments</th>

	</tr>
</thead>
<tbody>

	<tr>
		<td><?php echo $daily['date_'];?></td>
		<td>
		
		
		<p><strong> Student wakeup:</strong> &nbsp 
		<?php echo $daily['wake_up_time'];?><br>
           <strong>Parade time:</strong> &nbsp
           <?php echo $daily['parade_time'];?><br>
             <strong>Classes start time:</strong>&nbsp
             <?php echo $daily['start_classes_time'];?><br>
              <strong>Breakfast:</strong>&nbsp<?php echo $daily['breakfast'];?><br>
               <strong>Lunch menu:</strong>&nbsp<?php echo $daily['lunch'];?><br>
               <strong>Dinner menu:</strong>&nbsp<?php echo $daily['dinner'];?><br>
               <strong>Security:</strong>&nbsp<?php echo $daily['security'];?><br>
               <strong>Events:</strong>&nbsp<?php echo $daily['events'];?><br>
		</p>

		
		</td>
		<td><?php echo $daily['comments'];?>&nbsp<i class="fa fa-comments tex-info" aria-hidden="true"></i></td>
	</tr>
	
</tbody>

	
</table>

 </div>
 </div>