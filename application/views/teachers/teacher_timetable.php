
<h3 align="center"><?php echo $title; ?></h3><hr />

<div id="summary_table">
  <table class="table table-striped table-hover table-condensed">
    <thead>
      <tr>
        <th>Weekday</th>
        <th>Periods</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($teacher_periods as $teacher_period): ?>
        <tr>
          <td><?php echo $teacher_period['weekday']; ?></td>
          <td><?php echo $teacher_period['no_of_periods']; ?></td>
      <?php endforeach; ?>
    </tbody>
</table>
</div>

<div id="exhaustive_table" class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
      <tr>
        <th>Day</th>
        <th>Period#</th>
        <th>Start</th>
        <th>End</th>
        <th>Subject</th>
        <th>Class</th>
        <th>Stream</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($timetable_records as $timetable_record): ?>
        <tr>
          <td><?php echo $timetable_record['weekday']; ?></td>
          <td><?php echo $timetable_record['period_no']; ?></td>
          <td><?php echo $timetable_record['start_time']; ?></td>
          <td><?php echo $timetable_record['end_time']; ?></td>
          <td><?php echo $timetable_record['subject_name']; ?></td>
          <td><?php echo $timetable_record['class_name']; ?></td>
          <td><?php echo $timetable_record['stream']; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>