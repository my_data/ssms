
<div class="white-area-content">
<div class="db-header clearfix">

 <h3><?php echo $title;  ?></h3>
</div>
<br/>

<div class="form-group">
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<div class="clearfix">

<div class="col-md-12">

	<div class="col-md-4">
		<div class="panel panel-default" style="background:#62ACEC;">
			<div class="panel-body">
				<span class="fa fa-bullhorn" style="font-size:40px;"></span><br/>
			    	&nbsp;&nbsp;<strong><?php if($this->session->userdata('announcements')){
			    						echo $this->session->userdata('announcements');
			    					}
			    					else{
			    						echo 0;
			    						} ?> Announcement</strong>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default" style="background:#5CB85C;">
			<div class="panel-body" style="font-color:#ffffff;">
				<span class="fa fa-contao" style="font-size:40px;"></span><br/>
			    	<strong><?php if($this->session->userdata('teaching_classes')){
			    						echo $this->session->userdata('teaching_classes');
			    					}
			    					else{
			    						echo 0;
			    						} ?> Teaching Classes
			    	</strong>

			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default" style="background:#F0AD4E;">
			<div class="panel-body" style="font-color:#ffffff;">
				<span class="fa fa-bell-o" style="font-size:40px;"></span><br/>
			    	<strong><?php echo $this->session->userdata('notifications'); ?> Notifications</strong>
			 </div>
		</div>
	</div>

	<div class="col-md-4">
		<h4><b>Calendar</b></h4>
		<?php
		// Generate calendar
		echo $this->calendar->generate($year, $month);
		?>
	</div>

	<div class="col-md-4">
		
	</div>


</div>

<!-- 
<div class="col-md-12">
<h1>Academic Homepage<h1>
	<div class="col-md-4">
		<div class="panel panel-default" style="background:#62ACEC;">
			<div class="panel-body">
				<span class="fa fa-bullhorn" style="font-size:40px;"></span><br/>
			    	&nbsp;&nbsp;<strong>42 Announcement</strong>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default" style="background:#5CB85C;">
			<div class="panel-body" style="font-color:#ffffff;">
				<span class="fa fa-contao" style="font-size:40px;"></span><br/>
			    	<strong>12 Teaching Classes</strong>

			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default" style="background:#F0AD4E;">
			<div class="panel-body" style="font-color:#ffffff;">
				<span class="fa fa-bell-o" style="font-size:40px;"></span><br/>
			    	<strong>12 Notifications</strong>
			 </div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="col-md-12">

		<h3 style="text-align:center;">School Attendance Day:Monday Date:27/12/2016</h3>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed" style="
  box-shadow: 0 1px 1px rgba(0,0,0,0.10), 0 3px 6px rgba(0,0,0,0.10);">
			    <thead>
			        <tr class="table-header" style="background-color:#D9534F;">
			            <td style="width:30%; text-align:center; color:#FFFFFF;">Class Name</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Present</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Absent</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Sick</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Permitted</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Home</td>
			            <td style="width:20%; text-align:center; color:#FFFFFF;">Total Per-Stream</td>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;Form 1</td>
			            <td style="text-align:center;">10</td>
			            <td style="text-align:center;">2</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">0</td>
			            <td style="text-align:center;">3</td>
			            <td style="text-align:center;"><b>16</b></td>
			        </tr>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;Form 2</td>
			            <td style="text-align:center;">10</td>
			            <td style="text-align:center;">2</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">3</td>
			            <td style="text-align:center;"><b>17</b></td>
			        </tr>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;<b>Total</b></td>
			            <td style="text-align:center;"><b>20</b></td>
			            <td style="text-align:center;"><b>4</b></td>
			            <td style="text-align:center;"><b>2</b></td>
			            <td style="text-align:center;"><b>1</b></td>
			            <td style="text-align:center;"><b>6</b></td>
			            <td style="text-align:center;"><b>33</b></td>
			        </tr>
			    </tbody>
			</table>
		</div>


		<h3 style="text-align:center;">Roll-call, Day:Monday Date:27/12/2016</h3>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed" 
			style="box-shadow: 0 1px 1px rgba(0,0,0,0.10), 0 3px 6px rgba(0,0,0,0.10);">
			    <thead>
			        <tr class="table-header" style="background-color:#5CB85C;">
			            <td style="width:30%; text-align:center; color:#FFFFFF;">Dormitory Name</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Present</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Absent</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Sick</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Permitted</td>
			            <td style="width:10%; text-align:center; color:#FFFFFF;">Home</td>
			            <td style="width:20%; text-align:center; color:#FFFFFF;">Total Per-Dormitory</td>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;Kimwecha</td>
			            <td style="text-align:center;">10</td>
			            <td style="text-align:center;">2</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">0</td>
			            <td style="text-align:center;">3</td>
			            <td style="text-align:center;"><b>16</b></td>
			        </tr>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;Henga</td>
			            <td style="text-align:center;">10</td>
			            <td style="text-align:center;">2</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">1</td>
			            <td style="text-align:center;">3</td>
			            <td style="text-align:center;"><b>17</b></td>
			        </tr>
			        <tr>
			            <td>&nbsp;&nbsp;&nbsp;<b>Total</b></td>
			            <td style="text-align:center;"><b>20</b></td>
			            <td style="text-align:center;"><b>4</b></td>
			            <td style="text-align:center;"><b>2</b></td>
			            <td style="text-align:center;"><b>1</b></td>
			            <td style="text-align:center;"><b>6</b></td>
			            <td style="text-align:center;"><b>33</b></td>
			        </tr>
			    </tbody>
			</table>
		</div>



	</div>
</div> -->