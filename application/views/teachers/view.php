
<?php echo $title; ?>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

<table class="table table-striped table-hover table-condensed">
	<?php foreach($teachers as $teacher): ?>
		<tr>
			<td><?php echo $teacher['staff_id']; ?></td>
			<td><?php echo $teacher['firstname']; ?></td>
			<td><?php echo $teacher['lastname']; ?></td>
			<td><a href="/app/teacher/edit/<?php echo $teacher['staff_id']; ?>" class="btn btn-primary btn-xs">Edit</a></td>
			<td><a href="/app/teacher/assign/<?php echo $teacher['staff_id']; ?>" class="btn btn-success btn-xs" data-toggle="modal" data-target="#assignTeacherModel">Assign</a></td>
			<td><a href="/app/teacher/view/<?php echo $teacher['staff_id']; ?>" class="btn btn-primary btn-xs" >View</a></td>
			<?php echo form_open('teacher/delete/'.$teacher['staff_id']); ?>
				<td><input type="submit" value="Delele" class="btn btn-danger btn-xs"></td>
			</form>
		</tr>
		<!-- Modal -->
		<div class="modal fade" id="assignTeacherModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <!-- Modal Header -->
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">
		                    <span aria-hidden="true">&times;</span>
		                    <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
		            </div>
		            
		            <!-- Modal Body -->
		            <div class="modal-body">                
		                <form class="form-horizontal" role="form">
		                  	<div class="form-group">
		                    	<label  class="col-sm-2 control-label" for="inputEmail3">Email</label>
		                    	<div class="col-sm-10">
		                        	<input type="email" value="<?php echo $teacher['firstname']; ?>" class="form-control" id="inputEmail3" placeholder="Email"/>
		                    	</div>
		                 	</div>
		                  	<div class="form-group">
		                    	<label class="col-sm-2 control-label" for="inputPassword3" >Password</label>
		                    	<div class="col-sm-10">
		                        	<input type="password" class="form-control" id="inputPassword3" placeholder="Password"/>
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
			                    <div class="col-sm-offset-2 col-sm-10">
			                      	<div class="checkbox">
				                        <label>
				                            <input type="checkbox"/> Remember me
				                        </label>
			                      	</div>
			                    </div>
		                  	</div>
		                  	<div class="form-group">
			                    <div class="col-sm-offset-2 col-sm-10">
			                      <button type="submit" class="btn btn-default">Sign in</button>
			                    </div>
			                  </div>
		                </form>
		            </div>
		            
		            <!-- Modal Footer -->
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="button" class="btn btn-primary">Save changes</button>
		            </div>
		        </div>
		    </div>
		</div>
	<?php endforeach; ?>
</table>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalHorizontal">Open Modal</button>

<!-- Modal -->
<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">                
                <form class="form-horizontal" role="form">
                  	<div class="form-group">
                    	<label  class="col-sm-2 control-label" for="inputEmail3">Email</label>
                    	<div class="col-sm-10">
                        	<input type="email" class="form-control" id="inputEmail3" placeholder="Email"/>
                    	</div>
                 	</div>
                  	<div class="form-group">
                    	<label class="col-sm-2 control-label" for="inputPassword3" >Password</label>
                    	<div class="col-sm-10">
                        	<input type="password" class="form-control" id="inputPassword3" placeholder="Password"/>
                    	</div>
                  	</div>
                  	<div class="form-group">
	                    <div class="col-sm-offset-2 col-sm-10">
	                      	<div class="checkbox">
		                        <label>
		                            <input type="checkbox"/> Remember me
		                        </label>
	                      	</div>
	                    </div>
                  	</div>
                  	<div class="form-group">
	                    <div class="col-sm-offset-2 col-sm-10">
	                      <button type="submit" class="btn btn-default">Sign in</button>
	                    </div>
	                  </div>
                </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>