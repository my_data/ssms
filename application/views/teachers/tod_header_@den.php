
<div class="white-area-content">

<div class="db-header clearfix">

<div  align="right">
    <a href="<?php echo base_url() . 'teachers/display_week_comments/'.$this->session->userdata('mwalimu_wa_zamu'); ?>" class=" btn btn-primary fa fa-comments text-info" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Headmaster comments" >&nbsp;<strong>Comments</strong></a>&nbsp;
 <a href="<?php echo base_url() . 'teachers/daily_routine/'.$this->session->userdata('mwalimu_wa_zamu'); ?>" class=" btn btn-primary fa fa-wpforms text-info" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Duty routine form">&nbsp;<strong>Routine form</strong></a>&nbsp;
  &nbsp;
  <a href="<?php echo base_url() . 'teachers/today_routine_report/'.$this->session->userdata('mwalimu_wa_zamu').'/'.date('Y-m-d'); ?>" class=" btn btn-primary fa fa-clock-o text-info" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Todays report" >&nbsp;<strong>Today report</a></strong>&nbsp;
  <a href="<?php echo base_url() . 'teachers/display_week_routine/'.$this->session->userdata('mwalimu_wa_zamu'); ?>" class=" btn btn-primary fa fa-table text-info" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Week report" >&nbsp;<strong>Week report</strong></a>
</div>

