	
 <div class="page-header-title"> <h4><strong><i class=" btn btn-default fa fa-clock-o text-success" aria-hidden="true"></i>&nbsp;<?php echo  'Duty from &nbsp&nbsp' .$staff['start_date']. '&nbsp&nbspto&nbsp&nbsp' .$staff['end_date']; ?>&nbsp;&nbsp;: DAILY ROUTINE FORM</strong></h4></div>
    

</div>
<div class="clearfix">
<!--success and error message-->
<!--  <h4 class="text-info text-center"><?php if($this->session->flashdata('nor_error')): ?>
    <p><?php echo $this->session->flashdata('no_error'); ?>&nbsp;<i class="fa fa-check" aria-hidden="true"></i><?php endif; ?></p>
</h4>
<h4 class="text-info text-danger text-center"><?php if($this->session->flashdata('erroe')): ?>
    <p><?php echo $this->session->flashdata('check_error'); ?>&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true"></i></p>
<?php endif; ?></h4> -->

<div id="successMessage" class="success_message_color">
	<?php 
		if($this->session->flashdata('success_message')){
			echo $this->session->flashdata('success_message');
		}
	?>
</div>
<div id="errorMessage" class="error_message_color">
	<?php
		if($this->session->flashdata('error_message')){
			echo $this->session->flashdata('error_message');
		}
	?>
</div>

<?php echo form_open('teachers/daily_routine/'.$teacher_id);?>
<div class="form-group">
 <label class="col-sm-2">Date</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('date'); ?>" type="date" name="date" placeholder="Today's date" class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('date'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Wake-up time</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('wakeup'); ?>" type="text" name="wakeup" placeholder="wake-up time" class="form-control">
 		<div class="error_message_color">
          <?php echo form_error('wakeup'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Parade time</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('parade'); ?>" type="text" name="parade" placeholder="parade time" class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('parade'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Classes start</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('class'); ?>" type="text" name="class" placeholder="class time" class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('class'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Break fast</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('break'); ?>" type="text" name="break" placeholder="food1,food2,.." class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('break'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Lunch</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('lunch'); ?>" type="text" name="lunch" placeholder="food1,food2,.." class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('lunch'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Dinner</label>
 <div class="col-sm-10">
 	<input value="<?php echo set_value('dinner'); ?>" type="text" name="dinner" placeholder="food1,food2,.." class="form-control" required>
 		<div class="error_message_color">
          <?php echo form_error('dinner'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Events</label>
 <div class="col-sm-10">
 	<textarea rows="4" cols="54"  name="events" placeholder=".Events ." class="form-control">
 		 <?php echo set_value('events'); ?>
 	</textarea>
 		<div class="error_message_color">
          <?php echo form_error('events'); ?>
        </div>
 	<br>
 </div>	
</div>
<div class="form-group">
 <label class="col-sm-2">Security status</label>
 <div class="col-sm-10">
 	 	<textarea rows="4" name="security" cols="54" placeholder="Security explanations..." class="form-control">
 	 		 <?php echo set_value('security'); ?>
 	 	</textarea>
 	 	<div class="error_message_color">
          <?php echo form_error('security'); ?>
        </div>
 	<br>
 </div>	
</div>

<input type="hidden" name="id" class="form-control" value="<?php echo $staff['did'];?>"><br>

<div class="form-group">
 <label class="col-sm-2"> </label>
 <div class="col-sm-10">
 	<input type="submit" name="submit" class="form-control btn-primary" value="SAVE" onclick="return confirm('Are you sure you want to submit report? Once submitted, it cannot be changed. Please confirm the values before pressing ok');"><br>
 </div>	
</div>


<?php echo form_close();?>
 <!-- </div> -->
 </div>
 </div>


