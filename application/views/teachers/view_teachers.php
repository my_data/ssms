

  <div class="white-area-content">
      <div class="db-header clearfix">

        <div class="page-header-title"> <span class="fa fa-book"></span>&nbsp;<?php echo $title; ?>
        </div>
            <div class="db-header-extra form-inline"> 

                <?php echo form_open('teachers'); ?>

<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_teacher" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                        <?php echo form_close(); ?>
     
            </div>

            <!-- <a href="<?php echo base_url() . 'admin/add_class_stream'; ?>" class="btn btn-primary btn-sm">Add Class Stream</a>
 -->
          </div>


<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>
<!-- <table id="teacher_data" class="table table-striped table-hover table-condensed table-bordered"> -->
<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr class="table-header">
			<td>Staff#</td>
			<td>Firstname</td>
			<td>Lastname</td>
			<td>Subjects</td>
      <?php if($this->session->userdata('add_subjects_to_teacher') == 'ok' || $this->session->userdata('view_teacher_details') == 'ok'): ?>
			 <td align="center">Action</td>
      <?php endif; ?>
		</tr>
	</thead>
	<tbody>
  <?php if ($teachers == FALSE): ?>
        <tr>
          <td colspan="4">
                    <?php
                        $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Teachers";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
		<?php foreach($teachers as $teacher): ?>
			<tr>
				<td><?php echo $teacher['staff_id']; ?></td>
				<td><?php echo $teacher['firstname']; ?></td>
				<td><?php echo $teacher['lastname']; ?></td>
				<td><?php echo $teacher['subject_names']; ?></td>
        <?php if($this->session->userdata('add_subjects_to_teacher') == 'ok' || $this->session->userdata('view_teacher_details') == 'ok'): ?>
  				<td align="center">
            <?php if($this->session->userdata('view_teacher_details') == 'ok'): ?>
  					 <a href="<?php echo base_url() . 'staffs/profile/'.$teacher['staff_id'].''; ?>" class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="View More Details"><span class="fa fa-user"></span></a>&nbsp;
            <?php endif; ?>
            <?php if($this->session->userdata('add_subjects_to_teacher') == 'ok'): ?>
              <a href="<?php echo base_url() . 'teachers/add_subject_teacher/'.$teacher['staff_id'].''; ?>" class="btn btn-primary btn-xs" data-target="tooltip" data-placement="bottom" title="Add / Edit Subjects"><span class="fa fa-plus"></span></a>
  				  <?php endif; ?>
          </td>
        <?php endif; ?>
			</tr>
		<?php endforeach; ?>
  <?php endif; ?>
	</tbody>
</table>

<div align="left">
		<?php
			if($display_back === "OK"){
		?>
			<a href="<?php echo base_url() . 'admin/staffs'; ?>" class="btn btn-primary btn-xs">Back</a>
		<?php
			}
		?>
	</div>
	<div align="right">
		<?php echo $links; ?>
	</div>
</div>
</div>