<!-- <div class="col-md-9" style="padding-top: 50px;">

<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<br/> -->

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>


  <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <td>Firstname</td>
        <td>Lastname</td>
        <td>From</td>
        <td>To</td>
        <!-- <td>Action</td> -->
      </tr>
    </thead>
    <tbody>
    <?php if($teacher_records == FALSE): ?>
      <td rowspan="4">
        There is currently no teacher on duty assigned for this week
      </td>
    <?php else: ?>
      <?php foreach($teacher_records as $row): ?>
        <tr>
          <td><?php echo $row['firstname']; ?></td>
          <td><?php echo $row['lastname']; ?></td>
          <td><?php echo $row['start_date']; ?></td>
          <td><?php echo $row['end_date']; ?></td>
          <!-- <td>
            <a href="<?php echo base_url() . 'teachers/edit_duty_roaster/'.$row['id']; ?>" class="btn btn-primary btn-xs">Edit</a>&nbsp;&nbsp;&nbsp;
            <a onClick="return confirm('Are you sure you want todelete this record');" href="<?php echo base_url() . 'teachers/delete_duty_roaster/'.$row['id']; ?>" class="btn btn-danger btn-xs">Delete</a>
          </td> -->
        </tr> 
      <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
  </table>
<!-- </div> -->