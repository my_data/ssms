 <div class="page-header-title"> <h4><strong><i class=" btn btn-default fa fa-clock-o text-info" aria-hidden="true"></i>&nbsp;<?php echo  'Duty from &nbsp&nbsp' .$staff['start_date']. '&nbsp&nbspto&nbsp&nbsp' .$staff['end_date']; ?>&nbsp;&nbsp;: THIS WEEK REPORT</strong></h4></div>
    

</div>
<div class="clearfix">
<table class="table table-responsive table-condensed table-striped table-bordered">
<thead>
	<tr>
		<th>Day</th>
		<th>Date</th>		
		<th>View report</th>
	</tr>
</thead>
<tbody>
	<?php if ($daily == FALSE): ?>
        <tr>
          <td colspan="2">
                    <?php
                        $message = "No report found for this week";
                        echo $message;
                    ?>
                </td>
        </tr>
    <?php else: ?>
	<?php foreach ($daily as $daily): ?>
  		<tr>
	        <td><?php echo $daily['day'];?></td>
	        <td><?php echo $daily['date_'];?></td>
	        <td><a class="glyphicon glyphicon-eye-open" href="<?php echo base_url('teachers/today_routine_report/'.$staff['teacher_id'].'/'.$daily['date_']); ?>" data-toggle="tooltip" data-placement="right" title="View report"></a></td>
    	</tr>
	<?php endforeach; ?>
<?php endif; ?>
</tbody>	
</table>

 </div>
 </div>