
<div class="white-area-content">
<div class="db-header clearfix">

 <div class="page-header-title"> <span class="fa fa-graduation-cap"></span>&nbsp;<?php echo $title; ?></div>
    
</div>
<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

  <?php $attributes = array('role' => 'form'); ?>
  <?php echo form_open('teachers/assign_new_tod', $attributes); ?>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="teacher_names">Teacher Name :</label>
      <div class="col-sm-10">
        <!-- <div id="successMessage" class="success_message_color">
          <?php 
            if($this->session->flashdata('success_message')){
              echo $this->session->flashdata('success_message');
            }
          ?>
        </div>
        <div id="errorMessage" class="error_message_color">
          <?php
            if($this->session->flashdata('error_message')){
              echo $this->session->flashdata('error_message');
            }
          ?>
        </div>
        <div id="warningMessage" class="error_message_color">
          <?php
            if($this->session->flashdata('exist_message')){
              echo $this->session->flashdata('exist_message');
            }
          ?>
        </div> -->
        <select class="form-control" name="t_id" id="t_name">
                <option value="">-- Select Teacher --</option>
            <?php foreach ($teacher_names as $dataa): ?>
                <option value="<?php echo $dataa['staff_id']; ?>" <?php echo  set_select('t_id', ''.$dataa["staff_id"].'', $this->input->post('t_id')); ?>><?php echo $dataa['firstname']; ?>&nbsp;<?php echo $dataa['lastname']; ?></option>
            <?php endforeach; ?>
        </select>
        <div class="error_message_color">
          <?php echo form_error('t_id'); ?>
        </div>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="level">Start Date :</label>
      <div class="col-sm-10">
        <input type="date" class="form-control" name="start_date" placeholder="yyyy-mm-dd" value="<?php echo set_value('start_date'); ?>">
      </div>
      <div class="error_message_color">
        <?php echo form_error('start_date'); ?>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="level">End Date :</label>
      <div class="col-sm-10">
        <input type="date" class="form-control" name="end_date" placeholder="yyyy-mm-dd" value="<?php echo set_value('end_date'); ?>">
      </div>
      <div class="error_message_color">
        <?php echo form_error('end_date'); ?>
      </div>
    </div>
    <br/><br/><br/>
    <div class="form-group">
      <input type="submit" class="form-control btn btn-primary" name="assign_tod" value="Assign TOD" />
    </div>
    <br/><br/>
  <?php echo form_close(); ?>
</div>