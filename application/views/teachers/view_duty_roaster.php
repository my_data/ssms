<div class="white-area-content">
<div class="db-header clearfix">  

<div class="page-header-title"><span class="fa fa-graduation-cap"></span>&nbsp;
<?php echo $title; ?></div>
<div class="db-header-extra form-inline text-right">

<div class="form-group has-feedback no-margin">


                <?php echo form_open('teachers/teachers_duty_roaster'); ?>
<div class="input-group">
                      <input type="text" class="form-control input-xs" name="search_roaster" placeholder="Search  ..." id="form-search-input" />
                       <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" aria-haspopup="true" aria-expanded="false">
                          <i class="glyphicon glyphicon-search " ></i>
                        </button>
                        </div>
</div>
                        <?php echo form_close(); ?>
                        </div>
    
    <?php if($this->session->userdata('manage_tods') == 'ok'): ?>
      <a href="<?php echo base_url() . 'teachers/assign_new_tod'; ?>" class="btn btn-primary btn-sm">Add New</a>
    <?php endif; ?>  

            </div>
 </div>

<div class="form-group">
    <?php if($this->session->flashdata('success_message')): ?> 
        <div class="alert alert-dismissible alert-success text algin-center">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('errors')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('error_message')): ?> 
        <div class="alert alert-dismissible alert-danger text algin-center">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php endif;?>
</div>

  <table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
      <tr class="table-header">
        <?php if($this->session->userdata('manage_tods') == 'ok'): ?>
          <td align="center">Options</td>
        <?php endif; ?>
        <td>From</td>
        <td>To</td>
        <td>Firstname</td>
        <td>Lastname</td>
        <?php if($this->session->userdata('manage_tods') == 'ok'): ?>
          <td align="center">Edit</td>
        <?php endif; ?>
      </tr>
    </thead>
    <tbody>
      <?php $date = ""; ?>
      <?php if ($duty_roaster_record == FALSE): ?>
          <tr>
            <td colspan="6">
                <?php
                    $message = ($this->session->flashdata('search_message')) ? $this->session->flashdata('search_message') : "There are currently No Records";
                    echo $message;
                ?>
            </td>
          </tr>
      <?php else: ?>
      <?php foreach($duty_roaster_record as $row): ?>
        <tr>
          <?php if($this->session->userdata('manage_tods') == 'ok'): // Check if authorized?>
            <td align="center">
              <?php if($row['is_current'] === "T" && $date !== $row['start_date']): ?>
                <?php //echo "<strong>C</strong>"; ?>
                <a href="#" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-star" title="Current Week" data-placement="bottom"></span></a>
                &nbsp;
              <a href="<?php echo base_url() . 'teachers/view_week/'.$row['id']; ?>" class="btn btn-primary btn-xs" title="View Week Report" data-placement="bottom"><span class="fa fa-eye"></span></a>
              &nbsp;
              <a onClick="return confirm('Are you sure you want remove this teacher from the duty roaster?');" href="<?php echo base_url() . 'teachers/delete_duty_roaster/'.$row['id']; ?>" class="btn btn-danger btn-xs" title="Delete duty roaster" data-placement="bottom"><span class="glyphicon glyphicon-trash"></span></a>
              <?php endif; ?>
              <?php if($row['is_current'] === "F" && $date !== $row['start_date']): ?>
                <a href="<?php echo base_url() . 'teachers/set_current_duty_roaster/'.$row['id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure you want to set this as the current roaster?');" title="Set Current Duty Roaster" data-placement="bottom"><span class="glyphicon glyphicon-off"></span></a>
                &nbsp;
              <a href="<?php echo base_url() . 'teachers/view_week/'.$row['id']; ?>" class="btn btn-primary btn-xs" t title="View Week Report" data-placement="bottom"><span class="fa fa-eye"></span></a>
              &nbsp;
              <a onClick="return confirm('Are you sure you want remove this teacher from the duty roaster?');" href="<?php echo base_url() . 'teachers/delete_duty_roaster/'.$row['id'].'/'.$row['teacher_id']; ?>" class="btn btn-danger btn-xs" title="Delete from duty roaster" data-placement="bottom"><span class="glyphicon glyphicon-trash"></span></a>
              <?php endif; ?>
            </td>
          <?php endif;  //End check if authorized?>
          <td>
          <?php if($date !== $row['start_date']): ?>
<?php if($row['is_current'] === "T"): ?>
            <?php echo "<strong><span style='color: green;'>". $row['start_date']."</span></strong>"; ?>
<?php else: ?>
	     <?php echo $row['start_date']; ?>
<?php endif; ?>
          <?php endif; ?>
          </td>
          <td>
          <?php if($date !== $row['start_date']): ?>
<?php if($row['is_current'] === "T"): ?>
            <?php echo "<strong><span style='color: green;'>".$row['end_date']."</span></strong>"; ?>
<?php else: ?>
	     <?php echo $row['end_date']; ?>
<?php endif; ?>
          <?php endif; ?>
          </td>
          <td>
<?php if($row['is_current'] === "T"): ?>
		<?php echo "<strong>". $row['firstname']. "</strong"; ?>
<?php else: ?>
		<?php echo $row['firstname']; ?>
<?php endif; ?>
	  </td>
          <td>
<?php if($row['is_current'] === "T"): ?>
		<?php echo "<strong>". $row['lastname']. "</strong>"; ?>
<?php else: ?>
		<?php echo $row['lastname']; ?>
<?php endif; ?>
	  </td>
          <?php if($this->session->userdata('manage_tods') == 'ok'): ?>
            <td align="center">
              <a href="<?php echo base_url() . 'teachers/edit_duty_roaster/'.$row['id'].'/'.$row['teacher_id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>&nbsp;
              <!-- <a onClick="return confirm('Are you sure you want remove this teacher from the duty roaster?');" href="<?php echo base_url() . 'teachers/delete_duty_roaster/'.$row['id'].'/'.$row['teacher_id']; ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete from duty roaster" data-placement="bottom"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
              <a href="<?php echo base_url() . 'teachers/view_week/'.$row['id']; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>&nbsp; -->
            </td>
          <?php endif; ?>
        </tr>
        <?php $date = $row['start_date']; //Kwa ajili ya date kuto_print zaidi ya mara moja ?>
      <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
  </table>
<div align="left">
        <?php
          if($display_back === "OK"){
        ?>
          <a href="<?php echo base_url() . 'classes/classes_timetable'; ?>" class="btn btn-primary btn-xs">Back</a>
        <?php
          }
        ?>
      </div>
    <div align="right">
      <?php echo $links; ?>
    </div>
  </div>
</div>
