-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2020 at 07:18 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `macha`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_year`
--

CREATE TABLE `academic_year` (
  `id` int(5) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `year` varchar(10) NOT NULL,
  `class_level` enum('A''Level','O''Level') DEFAULT NULL,
  `status` enum('current_academic_year','past_academic_year') NOT NULL DEFAULT 'current_academic_year'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`id`, `start_date`, `end_date`, `year`, `class_level`, `status`) VALUES
(1, '2018-01-08', '2018-12-07', '2018', 'O\'Level', 'current_academic_year'),
(3, '2018-07-01', '2019-06-23', '2018-2019', 'A\'Level', 'current_academic_year');

--
-- Triggers `academic_year`
--
DELIMITER $$
CREATE TRIGGER `before_adding_academic_year_trigger` BEFORE INSERT ON `academic_year` FOR EACH ROW BEGIN  DECLARE current_year VARCHAR(4);  DECLARE a_level_academic_year VARCHAR(10);  DECLARE current_month VARCHAR(20);  SELECT YEAR((select current_date())) INTO current_year;  SELECT concat((SELECT year((SELECT current_date()))),"-",(SELECT YEAR((SELECT DATE_ADD(current_date(), INTERVAL 1 YEAR))))) INTO a_level_academic_year;  SELECT MONTHNAME((select current_date())) INTO current_month;  IF (NEW.class_level = "O'Level") THEN  IF !(NEW.year = current_year && current_month = "January") THEN  SIGNAL SQLSTATE '45000' set message_text = "The current year do not correspond with the O'LEVEL academic year you are trying to create. Check your date settings";  END IF;  END IF;  IF (NEW.class_level = "A'Level") THEN  IF !(NEW.year = a_level_academic_year && current_month = "July") THEN  SIGNAL SQLSTATE '45000' set message_text = "The current year do not correspond with the A'LEVEL academic year you are trying to create. Check your date settings";  END IF;  END IF;  END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `academic_year_term_view`
-- (See below for the actual view)
--
CREATE TABLE `academic_year_term_view` (
`term_id` int(11)
,`is_current` enum('yes','no')
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`term_name` enum('first_term','second_term')
,`term_begin_date` date
,`term_end_date` date
);

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_history`
--

CREATE TABLE `accomodation_history` (
  `admission_no` varchar(10) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accomodation_history`
--

INSERT INTO `accomodation_history` (`admission_no`, `dorm_id`, `start_date`, `end_date`) VALUES
('1031456', 1, '2018-06-01', NULL),
('1234567', 4, '2018-06-04', NULL),
('1000120', 5, '2018-06-04', NULL),
('1000121', 5, '2018-06-04', NULL),
('1000122', 1, '2018-06-04', '2018-06-05'),
('1000123', 4, '2018-06-04', NULL),
('1000124', 2, '2018-06-04', '2018-06-05'),
('1000125', 2, '2018-06-04', '2018-06-05'),
('1000126', 2, '2018-06-04', '2018-06-05'),
('1000127', 3, '2018-06-04', NULL),
('7656777', 1, '2018-06-05', '2018-06-05'),
('1563242', 1, '2018-06-05', '2018-06-05'),
('0987654', 1, '2018-06-05', '2018-06-05'),
('5555555', 5, '2018-06-05', NULL),
('1647366', 2, '2018-06-05', '2018-06-05'),
('1647366', 5, '2018-06-05', '2018-06-05'),
('1000125', 5, '2018-06-05', NULL),
('1647366', 2, '2018-06-05', '2018-06-05'),
('1000124', 2, '2018-06-05', '2018-06-05'),
('1000124', 2, '2018-06-05', '2018-06-05'),
('1000124', 4, '2018-06-05', NULL),
('1000126', 5, '2018-06-05', NULL),
('1000122', 4, '2018-06-05', NULL),
('1563242', 5, '2018-06-05', NULL),
('7656777', 2, '2018-06-05', NULL),
('0987654', 4, '2018-06-05', NULL),
('1647366', 5, '2018-06-05', NULL),
('4352111', 4, '2018-06-05', NULL),
('6754678', 3, '2018-06-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_no` varchar(30) NOT NULL,
  `available_amount` bigint(20) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `month` varchar(10) NOT NULL,
  `action` enum('none','debit','credit') NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `accountant`
--

CREATE TABLE `accountant` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `active_students_view`
-- (See below for the actual view)
--
CREATE TABLE `active_students_view` (
`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) NOT NULL,
  `activity` varchar(50) NOT NULL,
  `tableName` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `source` varchar(30) NOT NULL,
  `destination` varchar(10) DEFAULT NULL,
  `user_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `activity_log_view`
-- (See below for the actual view)
--
CREATE TABLE `activity_log_view` (
`id` bigint(20)
,`activity` varchar(50)
,`tableName` varchar(50)
,`time` timestamp
,`source` varchar(30)
,`destination` varchar(10)
,`user_id` varchar(10)
,`username` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(1) UNSIGNED NOT NULL,
  `user_role` enum('Admin') DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `last_log` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_role`, `username`, `password`, `last_log`) VALUES
(1, 'Admin', 'Administrator', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2020-08-24 04:04:08');

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_announcements_view`
-- (See below for the actual view)
--
CREATE TABLE `all_announcements_view` (
`group_id` int(11)
,`announcement_id` int(11)
,`heading` varchar(255)
,`msg` longtext
,`time` timestamp
,`posted_by` varchar(10)
,`status` enum('show','hide')
,`group_name` varchar(100)
,`description` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_class_streams_view`
-- (See below for the actual view)
--
CREATE TABLE `all_class_streams_view` (
`teacher_id` varchar(10)
,`teacher_names` varchar(61)
,`id` int(5)
,`year` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`level` enum('O''Level','A''Level')
,`enrolled` bigint(21)
,`capacity` int(3)
,`description` varchar(255)
,`id_display` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_results_view`
-- (See below for the actual view)
--
CREATE TABLE `all_results_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`class_name` varchar(30)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`term_id` int(11)
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`aid` int(5)
,`term_name` enum('first_term','second_term')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_teachers_view`
-- (See below for the actual view)
--
CREATE TABLE `all_teachers_view` (
`staff_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`user_role` varchar(30)
,`staff_type` char(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announcement_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `msg` longtext NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `posted_by` varchar(10) NOT NULL,
  `status` enum('show','hide') NOT NULL DEFAULT 'hide'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announcement_id`, `group_id`, `heading`, `msg`, `time`, `posted_by`, `status`) VALUES
(9, 1, 'Oyooo', 'Liverpool were deservedly beaten by Real Madrid!!!', '2018-06-05 16:49:45', '1', 'show'),
(10, 1, 'Holla', 'Sorry Salah.. u did not have a chance to help your teammates', '2018-06-05 17:19:28', '1', 'show');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_status`
--

CREATE TABLE `announcement_status` (
  `staff_id` varchar(10) NOT NULL,
  `group_id` int(11) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `is_read` enum('read','unread') NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement_status`
--

INSERT INTO `announcement_status` (`staff_id`, `group_id`, `announcement_id`, `is_read`) VALUES
('STAFF0001', 1, 9, 'read'),
('STAFF0001', 1, 10, 'read');

-- --------------------------------------------------------

--
-- Stand-in structure for view `assigned_user_roles`
-- (See below for the actual view)
--
CREATE TABLE `assigned_user_roles` (
`role_name` varchar(30)
,`description` mediumtext
,`role_type_id` int(2)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`active` enum('on','off')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `assign_hod_class_teacher_view`
-- (See below for the actual view)
--
CREATE TABLE `assign_hod_class_teacher_view` (
`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `att_id` int(2) NOT NULL,
  `att_type` char(1) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`att_id`, `att_type`, `description`) VALUES
(1, 'A', 'Absent'),
(2, 'P', 'Present'),
(3, 'S', 'Sick'),
(4, 'T', 'Permitted'),
(5, 'H', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `audit_password_reset_tbl`
--

CREATE TABLE `audit_password_reset_tbl` (
  `staff_id` varchar(10) NOT NULL,
  `reset_by` int(1) UNSIGNED NOT NULL,
  `reset_to` varchar(50) NOT NULL,
  `reset_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `user_changed` enum('T','F') NOT NULL DEFAULT 'F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_password_reset_tbl`
--

INSERT INTO `audit_password_reset_tbl` (`staff_id`, `reset_by`, `reset_to`, `reset_time`, `user_changed`) VALUES
('STAFF0001', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF0002', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-06-01 07:10:49', 'F'),
('STAFF0003', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF0004', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF0005', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 09:23:18', 'F'),
('STAFF0008', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 08:02:02', 'F'),
('STAFF0035', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF0339', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF0987', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF1111', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF2020', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF2222', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF3211', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T'),
('STAFF3333', 1, '7c222fb2927d828af22f592134e8932480637c0d', '2018-06-05 12:18:28', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `audit_register_student_tbl`
--

CREATE TABLE `audit_register_student_tbl` (
  `id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `change_type` enum('NEW','EDIT','DELETE') NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_register_student_tbl`
--

INSERT INTO `audit_register_student_tbl` (`id`, `admission_no`, `class_stream_id`, `change_by`, `change_type`, `time`, `source`) VALUES
(1, '9649531', 'C05PCB', 'STAFF0001', 'NEW', '2018-05-31 16:36:55', '192.168.100.12'),
(2, '1111111', 'C05PGM', 'STAFF0002', 'NEW', '2018-05-31 16:38:11', '192.168.100.11'),
(3, '6905452', 'C05PCB', 'STAFF0001', 'NEW', '2018-05-31 16:38:51', '192.168.100.12'),
(4, '2222222', 'C05PGM', 'STAFF0002', 'NEW', '2018-05-31 16:50:41', '192.168.100.11'),
(5, '3546572', 'C05PGM', 'STAFF0001', 'NEW', '2018-05-31 16:52:04', '192.168.100.12'),
(6, '3333333', 'C05PCB', 'STAFF0002', 'NEW', '2018-05-31 16:54:10', '192.168.100.11'),
(7, '8967755', 'C05PGM', 'STAFF0001', 'NEW', '2018-05-31 16:54:25', '192.168.100.12'),
(8, '3449343', 'C05PCB', 'STAFF0001', 'NEW', '2018-05-31 16:58:31', '192.168.100.12'),
(9, '1031456', 'C05EGM', 'STAFF0004', 'NEW', '2018-06-01 13:48:27', '192.168.100.15'),
(10, '1234567', 'C05PCB', '1', 'NEW', '2018-06-04 11:49:23', '192.168.100.12'),
(11, '1000120', 'C05EGM', '1', 'NEW', '2018-06-04 11:58:20', '192.168.100.11'),
(12, '1000121', 'C05PCB', '1', 'NEW', '2018-06-04 12:00:22', '192.168.100.11'),
(13, '1000122', 'C05EGM', '1', 'NEW', '2018-06-04 12:30:27', '192.168.100.11'),
(14, '1000123', 'C05PCB', '1', 'NEW', '2018-06-04 12:32:36', '192.168.100.11'),
(15, '1000124', 'C05EGM', '1', 'NEW', '2018-06-04 12:35:15', '192.168.100.11'),
(16, '1000125', 'C05PCB', '1', 'NEW', '2018-06-04 12:40:33', '192.168.100.11'),
(17, '1000126', 'C05EGM', '1', 'NEW', '2018-06-04 12:43:06', '192.168.100.11'),
(18, '1000127', 'C05PCB', '1', 'NEW', '2018-06-04 12:46:34', '192.168.100.11'),
(19, '7656777', 'C05EGM', 'STAFF2222', 'NEW', '2018-06-05 06:57:48', '192.168.100.13'),
(20, '1563242', 'C05EGM', 'STAFF3333', 'NEW', '2018-06-05 07:01:31', '192.168.100.12'),
(21, '0987654', 'C05PCB', 'STAFF1111', 'NEW', '2018-06-05 07:08:07', '192.168.100.17'),
(22, '5555555', 'C05EGM', 'STAFF0003', 'NEW', '2018-06-05 07:08:18', '192.168.100.11'),
(23, '1647366', 'C05PCB', 'STAFF3333', 'NEW', '2018-06-05 07:11:44', '192.168.100.12'),
(24, '4352111', 'C05EGM', '1', 'NEW', '2018-06-05 11:59:38', '192.168.100.14'),
(25, '6754678', 'C05PCB', 'STAFF2222', 'NEW', '2018-06-05 12:10:42', '192.168.100.13');

-- --------------------------------------------------------

--
-- Table structure for table `audit_staff_attendance`
--

CREATE TABLE `audit_staff_attendance` (
  `id` int(11) NOT NULL,
  `sar_id` int(11) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `att_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_staff_attendance`
--

INSERT INTO `audit_staff_attendance` (`id`, `sar_id`, `change_type`, `change_by`, `ip_address`, `change_time`, `att_id`) VALUES
(1, 1, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 1),
(2, 2, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(3, 3, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(4, 4, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 3),
(5, 5, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(6, 6, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 4),
(7, 7, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(8, 8, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(9, 9, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(10, 10, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(11, 11, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:32:27', 2),
(12, 1, 'UPDATE', 'STAFF3333', '192.168.100.12', '2018-06-05 12:33:11', 2),
(13, 11, 'UPDATE', '1', '192.168.100.14', '2018-06-05 12:34:46', 3),
(14, 12, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(15, 13, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(16, 14, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(17, 15, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(18, 16, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(19, 17, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(20, 18, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 1),
(21, 19, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(22, 20, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 4),
(23, 21, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 2),
(24, 22, 'ADD', 'STAFF3333', '192.168.100.12', '2018-06-05 12:35:15', 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_staff_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `audit_staff_attendance_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`date_` date
,`day` varchar(9)
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` varchar(6)
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
,`added_changed_by` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `audit_student_attendance`
--

CREATE TABLE `audit_student_attendance` (
  `id` int(11) NOT NULL,
  `sar_id` int(11) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `att_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_student_attendance`
--

INSERT INTO `audit_student_attendance` (`id`, `sar_id`, `change_type`, `change_by`, `ip_address`, `change_time`, `att_id`) VALUES
(1, 1, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2),
(2, 2, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2),
(3, 3, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2),
(4, 4, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 3),
(5, 5, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2),
(6, 6, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 1),
(7, 7, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2),
(8, 8, 'ADD', 'STAFF0001', '192.168.100.11', '2018-06-06 16:16:09', 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_student_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `audit_student_attendance_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`admission_no` varchar(10)
,`date_` date
,`day` varchar(9)
,`csid` varchar(10)
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` varchar(6)
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
,`class_name` varchar(30)
,`added_changed_by` varchar(61)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_student_results_view`
-- (See below for the actual view)
--
CREATE TABLE `audit_student_results_view` (
`admission_no` varchar(10)
,`student_names` varchar(41)
,`subject_name` varchar(50)
,`exam_name` varchar(30)
,`marks` float(5,2)
,`class_name` varchar(30)
,`e_date` date
,`term_name` varchar(11)
,`year` varchar(10)
,`change_type` varchar(6)
,`ip_address` varchar(255)
,`change_time` timestamp
,`added_changed_by` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `audit_student_subject_assessment`
--

CREATE TABLE `audit_student_subject_assessment` (
  `id` bigint(20) NOT NULL,
  `ssa_id` bigint(20) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `marks` float(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_student_subject_assessment`
--

INSERT INTO `audit_student_subject_assessment` (`id`, `ssa_id`, `change_type`, `change_by`, `ip_address`, `change_time`, `marks`) VALUES
(326, 616, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', 90.00),
(327, 617, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', NULL),
(328, 618, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', 90.00),
(329, 619, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', 88.00),
(330, 620, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', NULL),
(331, 621, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', 67.00),
(332, 622, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', NULL),
(333, 623, 'ADD', 'STAFF2222', '192.168.100.11', '2018-06-07 11:42:03', 87.00);

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `author_id` int(3) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `available_books_view`
-- (See below for the actual view)
--
CREATE TABLE `available_books_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`BOOK_TITLE` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `a_level_grade_system_tbl`
--

CREATE TABLE `a_level_grade_system_tbl` (
  `id` int(3) NOT NULL,
  `start_mark` float(5,2) NOT NULL,
  `end_mark` float(5,2) NOT NULL,
  `grade` enum('A','B+','B','C','D','E','S','F') NOT NULL,
  `points` int(1) NOT NULL,
  `remarks` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `a_level_grade_system_tbl`
--

INSERT INTO `a_level_grade_system_tbl` (`id`, `start_mark`, `end_mark`, `grade`, `points`, `remarks`) VALUES
(1, 0.00, 30.00, 'F', 7, 'Fail'),
(2, 30.01, 40.00, 'S', 6, 'Satisfactory'),
(3, 40.01, 50.00, 'E', 5, 'Pass'),
(4, 50.10, 60.00, 'D', 4, 'Fair'),
(5, 60.01, 70.00, 'C', 3, 'Good'),
(6, 80.01, 100.00, 'A', 1, 'Excellent'),
(7, 70.01, 80.00, 'B', 2, 'Very Good');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `isbn` varchar(17) NOT NULL,
  `ID` varchar(50) NOT NULL DEFAULT '',
  `LAST_EDIT` date DEFAULT NULL,
  `STATUS` enum('normal','lost','bad') DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`isbn`, `ID`, `LAST_EDIT`, `STATUS`) VALUES
('04-002-5', '1', '2018-05-23', 'normal'),
('04-002-5', '10', '2018-05-23', 'normal'),
('04-002-5', '11', '2018-05-23', 'normal'),
('04-002-5', '12', '2018-05-23', 'normal'),
('04-002-5', '13', '2018-05-23', 'normal'),
('04-002-5', '14', '2018-05-23', 'normal'),
('04-002-5', '15', '2018-05-23', 'normal'),
('04-002-5', '16', '2018-05-23', 'normal'),
('04-002-5', '17', '2018-05-23', 'normal'),
('04-002-5', '18', '2018-05-23', 'normal'),
('04-002-5', '19', '2018-05-23', 'normal'),
('04-002-5', '2', '2018-05-23', 'normal'),
('04-002-5', '20', '2018-05-23', 'normal'),
('04-002-5', '21', '2018-05-23', 'normal'),
('04-002-5', '22', '2018-05-23', 'normal'),
('04-002-5', '23', '2018-05-23', 'normal'),
('04-002-5', '24', '2018-05-23', 'normal'),
('04-002-5', '25', '2018-05-23', 'normal'),
('04-002-5', '26', '2018-05-23', 'normal'),
('04-002-5', '27', '2018-05-23', 'normal'),
('04-002-5', '28', '2018-05-23', 'normal'),
('04-002-5', '29', '2018-05-23', 'normal'),
('04-002-5', '3', '2018-05-23', 'normal'),
('04-002-5', '30', '2018-05-23', 'normal'),
('04-002-5', '31', '2018-05-23', 'normal'),
('04-002-5', '32', '2018-05-23', 'normal'),
('04-002-5', '33', '2018-05-23', 'normal'),
('04-002-5', '34', '2018-05-23', 'normal'),
('04-002-5', '35', '2018-05-23', 'normal'),
('04-002-5', '36', '2018-05-23', 'normal'),
('04-002-5', '37', '2018-05-23', 'normal'),
('04-002-5', '38', '2018-05-23', 'normal'),
('04-002-5', '39', '2018-05-23', 'normal'),
('04-002-5', '4', '2018-05-23', 'normal'),
('04-002-5', '40', '2018-05-23', 'normal'),
('04-002-5', '41', '2018-05-23', 'normal'),
('04-002-5', '42', '2018-05-23', 'normal'),
('04-002-5', '43', '2018-05-23', 'normal'),
('04-002-5', '44', '2018-05-23', 'normal'),
('04-002-5', '45', '2018-05-23', 'normal'),
('04-002-5', '46', '2018-05-23', 'normal'),
('04-002-5', '47', '2018-05-23', 'normal'),
('04-002-5', '48', '2018-05-23', 'normal'),
('04-002-5', '49', '2018-05-23', 'normal'),
('04-002-5', '5', '2018-05-23', 'normal'),
('04-002-5', '50', '2018-05-23', 'normal'),
('04-002-5', '51', '2018-05-23', 'normal'),
('04-002-5', '52', '2018-05-23', 'normal'),
('04-002-5', '53', '2018-05-23', 'normal'),
('04-002-5', '54', '2018-05-23', 'normal'),
('04-002-5', '55', '2018-05-23', 'normal'),
('04-002-5', '56', '2018-05-23', 'normal'),
('04-002-5', '57', '2018-05-23', 'normal'),
('04-002-5', '58', '2018-05-23', 'normal'),
('04-002-5', '59', '2018-05-23', 'normal'),
('04-002-5', '6', '2018-05-23', 'normal'),
('04-002-5', '60', '2018-05-23', 'normal'),
('04-002-5', '61', '2018-05-23', 'normal'),
('04-002-5', '62', '2018-05-23', 'normal'),
('04-002-5', '63', '2018-05-23', 'normal'),
('04-002-5', '64', '2018-05-23', 'normal'),
('04-002-5', '65', '2018-05-23', 'normal'),
('04-002-5', '66', '2018-05-23', 'normal'),
('04-002-5', '67', '2018-05-23', 'normal'),
('04-002-5', '68', '2018-05-23', 'normal'),
('04-002-5', '69', '2018-05-23', 'normal'),
('04-002-5', '7', '2018-05-23', 'normal'),
('04-002-5', '70', '2018-05-23', 'normal'),
('04-002-5', '71', '2018-05-23', 'normal'),
('04-002-5', '72', '2018-05-23', 'normal'),
('04-002-5', '73', '2018-05-23', 'normal'),
('04-002-5', '74', '2018-05-23', 'normal'),
('04-002-5', '75', '2018-05-23', 'normal'),
('04-002-5', '76', '2018-05-23', 'normal'),
('04-002-5', '77', '2018-05-23', 'normal'),
('04-002-5', '78', '2018-05-23', 'normal'),
('04-002-5', '79', '2018-05-23', 'normal'),
('04-002-5', '8', '2018-05-23', 'normal'),
('04-002-5', '80', '2018-05-23', 'normal'),
('04-002-5', '81', '2018-05-23', 'normal'),
('04-002-5', '82', '2018-05-23', 'normal'),
('04-002-5', '83', '2018-05-23', 'normal'),
('04-002-5', '84', '2018-05-23', 'normal'),
('04-002-5', '9', '2018-05-23', 'normal'),
('61-057-2', '1', '2018-05-23', 'normal'),
('61-057-2', '10', '2018-05-23', 'normal'),
('61-057-2', '11', '2018-05-23', 'normal'),
('61-057-2', '12', '2018-05-23', 'normal'),
('61-057-2', '13', '2018-05-23', 'normal'),
('61-057-2', '14', '2018-05-23', 'normal'),
('61-057-2', '15', '2018-05-23', 'normal'),
('61-057-2', '16', '2018-05-23', 'normal'),
('61-057-2', '17', '2018-05-23', 'normal'),
('61-057-2', '18', '2018-05-23', 'normal'),
('61-057-2', '19', '2018-05-23', 'normal'),
('61-057-2', '2', '2018-05-23', 'normal'),
('61-057-2', '20', '2018-05-23', 'normal'),
('61-057-2', '21', '2018-05-23', 'normal'),
('61-057-2', '22', '2018-05-23', 'normal'),
('61-057-2', '23', '2018-05-23', 'normal'),
('61-057-2', '24', '2018-05-23', 'normal'),
('61-057-2', '25', '2018-05-23', 'normal'),
('61-057-2', '26', '2018-05-23', 'normal'),
('61-057-2', '27', '2018-05-23', 'normal'),
('61-057-2', '28', '2018-05-23', 'normal'),
('61-057-2', '29', '2018-05-23', 'normal'),
('61-057-2', '3', '2018-05-23', 'normal'),
('61-057-2', '30', '2018-05-23', 'normal'),
('61-057-2', '31', '2018-05-23', 'normal'),
('61-057-2', '32', '2018-05-23', 'normal'),
('61-057-2', '33', '2018-05-23', 'normal'),
('61-057-2', '34', '2018-05-23', 'normal'),
('61-057-2', '35', '2018-05-23', 'normal'),
('61-057-2', '36', '2018-05-23', 'normal'),
('61-057-2', '37', '2018-05-23', 'normal'),
('61-057-2', '38', '2018-05-23', 'normal'),
('61-057-2', '39', '2018-05-23', 'normal'),
('61-057-2', '4', '2018-05-23', 'normal'),
('61-057-2', '40', '2018-05-23', 'normal'),
('61-057-2', '41', '2018-05-23', 'normal'),
('61-057-2', '42', '2018-05-23', 'normal'),
('61-057-2', '43', '2018-05-23', 'normal'),
('61-057-2', '44', '2018-05-23', 'normal'),
('61-057-2', '45', '2018-05-23', 'normal'),
('61-057-2', '46', '2018-05-23', 'normal'),
('61-057-2', '47', '2018-05-23', 'normal'),
('61-057-2', '48', '2018-05-23', 'normal'),
('61-057-2', '49', '2018-05-23', 'normal'),
('61-057-2', '5', '2018-05-23', 'normal'),
('61-057-2', '50', '2018-05-23', 'normal'),
('61-057-2', '51', '2018-05-23', 'normal'),
('61-057-2', '52', '2018-05-23', 'normal'),
('61-057-2', '6', '2018-05-23', 'normal'),
('61-057-2', '7', '2018-05-23', 'normal'),
('61-057-2', '8', '2018-05-23', 'normal'),
('61-057-2', '9', '2018-05-23', 'normal'),
('61-457-2', '1', '2018-05-23', 'normal'),
('61-457-2', '10', '2018-05-23', 'normal'),
('61-457-2', '100', '2018-05-23', 'normal'),
('61-457-2', '101', '2018-05-23', 'normal'),
('61-457-2', '102', '2018-05-23', 'normal'),
('61-457-2', '103', '2018-05-23', 'normal'),
('61-457-2', '104', '2018-05-23', 'normal'),
('61-457-2', '105', '2018-05-23', 'normal'),
('61-457-2', '106', '2018-05-23', 'normal'),
('61-457-2', '107', '2018-05-23', 'normal'),
('61-457-2', '108', '2018-05-23', 'normal'),
('61-457-2', '109', '2018-05-23', 'normal'),
('61-457-2', '11', '2018-05-23', 'normal'),
('61-457-2', '110', '2018-05-23', 'normal'),
('61-457-2', '111', '2018-05-23', 'normal'),
('61-457-2', '112', '2018-05-23', 'normal'),
('61-457-2', '113', '2018-05-23', 'normal'),
('61-457-2', '114', '2018-05-23', 'normal'),
('61-457-2', '115', '2018-05-23', 'normal'),
('61-457-2', '116', '2018-05-23', 'normal'),
('61-457-2', '117', '2018-05-23', 'normal'),
('61-457-2', '118', '2018-05-23', 'normal'),
('61-457-2', '119', '2018-05-23', 'normal'),
('61-457-2', '12', '2018-05-23', 'normal'),
('61-457-2', '120', '2018-05-23', 'normal'),
('61-457-2', '13', '2018-05-23', 'normal'),
('61-457-2', '14', '2018-05-23', 'normal'),
('61-457-2', '15', '2018-05-23', 'normal'),
('61-457-2', '16', '2018-05-23', 'normal'),
('61-457-2', '17', '2018-05-23', 'normal'),
('61-457-2', '18', '2018-05-23', 'normal'),
('61-457-2', '19', '2018-05-23', 'normal'),
('61-457-2', '2', '2018-05-23', 'normal'),
('61-457-2', '20', '2018-05-23', 'normal'),
('61-457-2', '21', '2018-05-23', 'normal'),
('61-457-2', '22', '2018-05-23', 'normal'),
('61-457-2', '23', '2018-05-23', 'normal'),
('61-457-2', '24', '2018-05-23', 'normal'),
('61-457-2', '25', '2018-05-23', 'normal'),
('61-457-2', '26', '2018-05-23', 'normal'),
('61-457-2', '27', '2018-05-23', 'normal'),
('61-457-2', '28', '2018-05-23', 'normal'),
('61-457-2', '29', '2018-05-23', 'normal'),
('61-457-2', '3', '2018-05-23', 'normal'),
('61-457-2', '30', '2018-05-23', 'normal'),
('61-457-2', '31', '2018-05-23', 'normal'),
('61-457-2', '32', '2018-05-23', 'normal'),
('61-457-2', '33', '2018-05-23', 'normal'),
('61-457-2', '34', '2018-05-23', 'normal'),
('61-457-2', '35', '2018-05-23', 'normal'),
('61-457-2', '36', '2018-05-23', 'normal'),
('61-457-2', '37', '2018-05-23', 'normal'),
('61-457-2', '38', '2018-05-23', 'normal'),
('61-457-2', '39', '2018-05-23', 'normal'),
('61-457-2', '4', '2018-05-23', 'normal'),
('61-457-2', '40', '2018-05-23', 'normal'),
('61-457-2', '41', '2018-05-23', 'normal'),
('61-457-2', '42', '2018-05-23', 'normal'),
('61-457-2', '43', '2018-05-23', 'normal'),
('61-457-2', '44', '2018-05-23', 'normal'),
('61-457-2', '45', '2018-05-23', 'normal'),
('61-457-2', '46', '2018-05-23', 'normal'),
('61-457-2', '47', '2018-05-23', 'normal'),
('61-457-2', '48', '2018-05-23', 'normal'),
('61-457-2', '49', '2018-05-23', 'normal'),
('61-457-2', '5', '2018-05-23', 'normal'),
('61-457-2', '50', '2018-05-23', 'normal'),
('61-457-2', '51', '2018-05-23', 'normal'),
('61-457-2', '52', '2018-05-23', 'normal'),
('61-457-2', '53', '2018-05-23', 'normal'),
('61-457-2', '54', '2018-05-23', 'normal'),
('61-457-2', '55', '2018-05-23', 'normal'),
('61-457-2', '56', '2018-05-23', 'normal'),
('61-457-2', '57', '2018-05-23', 'normal'),
('61-457-2', '58', '2018-05-23', 'normal'),
('61-457-2', '59', '2018-05-23', 'normal'),
('61-457-2', '6', '2018-05-23', 'normal'),
('61-457-2', '60', '2018-05-23', 'normal'),
('61-457-2', '61', '2018-05-23', 'normal'),
('61-457-2', '62', '2018-05-23', 'normal'),
('61-457-2', '63', '2018-05-23', 'normal'),
('61-457-2', '64', '2018-05-23', 'normal'),
('61-457-2', '65', '2018-05-23', 'normal'),
('61-457-2', '66', '2018-05-23', 'normal'),
('61-457-2', '67', '2018-05-23', 'normal'),
('61-457-2', '68', '2018-05-23', 'normal'),
('61-457-2', '69', '2018-05-23', 'normal'),
('61-457-2', '7', '2018-05-23', 'normal'),
('61-457-2', '70', '2018-05-23', 'normal'),
('61-457-2', '71', '2018-05-23', 'normal'),
('61-457-2', '72', '2018-05-23', 'normal'),
('61-457-2', '73', '2018-05-23', 'normal'),
('61-457-2', '74', '2018-05-23', 'normal'),
('61-457-2', '75', '2018-05-23', 'normal'),
('61-457-2', '76', '2018-05-23', 'normal'),
('61-457-2', '77', '2018-05-23', 'normal'),
('61-457-2', '78', '2018-05-23', 'normal'),
('61-457-2', '79', '2018-05-23', 'normal'),
('61-457-2', '8', '2018-05-23', 'normal'),
('61-457-2', '80', '2018-05-23', 'normal'),
('61-457-2', '81', '2018-05-23', 'normal'),
('61-457-2', '82', '2018-05-23', 'normal'),
('61-457-2', '83', '2018-05-23', 'normal'),
('61-457-2', '84', '2018-05-23', 'normal'),
('61-457-2', '85', '2018-05-23', 'normal'),
('61-457-2', '86', '2018-05-23', 'normal'),
('61-457-2', '87', '2018-05-23', 'normal'),
('61-457-2', '88', '2018-05-23', 'normal'),
('61-457-2', '89', '2018-05-23', 'normal'),
('61-457-2', '9', '2018-05-23', 'normal'),
('61-457-2', '90', '2018-05-23', 'normal'),
('61-457-2', '91', '2018-05-23', 'normal'),
('61-457-2', '92', '2018-05-23', 'normal'),
('61-457-2', '93', '2018-05-23', 'normal'),
('61-457-2', '94', '2018-05-23', 'normal'),
('61-457-2', '95', '2018-05-23', 'normal'),
('61-457-2', '96', '2018-05-23', 'normal'),
('61-457-2', '97', '2018-05-23', 'normal'),
('61-457-2', '98', '2018-05-23', 'normal'),
('61-457-2', '99', '2018-05-23', 'normal'),
('61-458-9', '1', '2018-05-23', 'normal'),
('61-458-9', '10', '2018-05-23', 'normal'),
('61-458-9', '11', '2018-05-23', 'normal'),
('61-458-9', '12', '2018-05-23', 'normal'),
('61-458-9', '13', '2018-05-23', 'normal'),
('61-458-9', '14', '2018-05-23', 'normal'),
('61-458-9', '15', '2018-05-23', 'normal'),
('61-458-9', '16', '2018-05-23', 'normal'),
('61-458-9', '17', '2018-05-23', 'normal'),
('61-458-9', '18', '2018-05-23', 'normal'),
('61-458-9', '19', '2018-05-23', 'normal'),
('61-458-9', '2', '2018-05-23', 'normal'),
('61-458-9', '20', '2018-05-23', 'normal'),
('61-458-9', '21', '2018-05-23', 'normal'),
('61-458-9', '22', '2018-05-23', 'normal'),
('61-458-9', '23', '2018-05-23', 'normal'),
('61-458-9', '24', '2018-05-23', 'normal'),
('61-458-9', '25', '2018-05-23', 'normal'),
('61-458-9', '26', '2018-05-23', 'normal'),
('61-458-9', '27', '2018-05-23', 'normal'),
('61-458-9', '28', '2018-05-23', 'normal'),
('61-458-9', '29', '2018-05-23', 'normal'),
('61-458-9', '3', '2018-05-23', 'normal'),
('61-458-9', '30', '2018-05-23', 'normal'),
('61-458-9', '31', '2018-05-23', 'normal'),
('61-458-9', '32', '2018-05-23', 'normal'),
('61-458-9', '33', '2018-05-23', 'normal'),
('61-458-9', '34', '2018-05-23', 'normal'),
('61-458-9', '35', '2018-05-23', 'normal'),
('61-458-9', '36', '2018-05-23', 'normal'),
('61-458-9', '37', '2018-05-23', 'normal'),
('61-458-9', '38', '2018-05-23', 'normal'),
('61-458-9', '39', '2018-05-23', 'normal'),
('61-458-9', '4', '2018-05-23', 'normal'),
('61-458-9', '40', '2018-05-23', 'normal'),
('61-458-9', '41', '2018-05-23', 'normal'),
('61-458-9', '42', '2018-05-23', 'normal'),
('61-458-9', '43', '2018-05-23', 'normal'),
('61-458-9', '44', '2018-05-23', 'normal'),
('61-458-9', '45', '2018-05-23', 'normal'),
('61-458-9', '46', '2018-05-23', 'normal'),
('61-458-9', '47', '2018-05-23', 'normal'),
('61-458-9', '48', '2018-05-23', 'normal'),
('61-458-9', '49', '2018-05-23', 'normal'),
('61-458-9', '5', '2018-05-23', 'normal'),
('61-458-9', '50', '2018-05-23', 'normal'),
('61-458-9', '51', '2018-05-23', 'normal'),
('61-458-9', '52', '2018-05-23', 'normal'),
('61-458-9', '53', '2018-05-23', 'normal'),
('61-458-9', '54', '2018-05-23', 'normal'),
('61-458-9', '55', '2018-05-23', 'normal'),
('61-458-9', '56', '2018-05-23', 'normal'),
('61-458-9', '57', '2018-05-23', 'normal'),
('61-458-9', '58', '2018-05-23', 'normal'),
('61-458-9', '59', '2018-05-23', 'normal'),
('61-458-9', '6', '2018-05-23', 'normal'),
('61-458-9', '60', '2018-05-23', 'normal'),
('61-458-9', '61', '2018-05-23', 'normal'),
('61-458-9', '7', '2018-05-23', 'normal'),
('61-458-9', '8', '2018-05-23', 'normal'),
('61-458-9', '9', '2018-05-23', 'normal'),
('61-462-6', '1', '2018-05-23', 'normal'),
('61-462-6', '10', '2018-05-23', 'bad'),
('61-462-6', '11', '2018-05-23', 'bad'),
('61-462-6', '12', '2018-05-23', 'bad'),
('61-462-6', '13', '2018-05-23', 'normal'),
('61-462-6', '14', '2018-05-23', 'normal'),
('61-462-6', '15', '2018-05-23', 'normal'),
('61-462-6', '16', '2018-05-23', 'normal'),
('61-462-6', '17', '2018-05-23', 'normal'),
('61-462-6', '18', '2018-05-23', 'normal'),
('61-462-6', '19', '2018-05-23', 'normal'),
('61-462-6', '2', '2018-05-23', 'normal'),
('61-462-6', '20', '2018-05-23', 'normal'),
('61-462-6', '21', '2018-05-23', 'normal'),
('61-462-6', '22', '2018-05-23', 'normal'),
('61-462-6', '23', '2018-05-23', 'normal'),
('61-462-6', '24', '2018-05-23', 'normal'),
('61-462-6', '25', '2018-05-23', 'normal'),
('61-462-6', '26', '2018-05-23', 'normal'),
('61-462-6', '27', '2018-05-23', 'normal'),
('61-462-6', '28', '2018-05-23', 'normal'),
('61-462-6', '29', '2018-05-23', 'normal'),
('61-462-6', '3', '2018-05-23', 'normal'),
('61-462-6', '30', '2018-05-23', 'normal'),
('61-462-6', '31', '2018-05-23', 'normal'),
('61-462-6', '32', '2018-05-23', 'normal'),
('61-462-6', '33', '2018-05-23', 'normal'),
('61-462-6', '34', '2018-05-23', 'normal'),
('61-462-6', '35', '2018-05-23', 'normal'),
('61-462-6', '36', '2018-05-23', 'normal'),
('61-462-6', '37', '2018-05-23', 'normal'),
('61-462-6', '38', '2018-05-23', 'normal'),
('61-462-6', '39', '2018-05-23', 'normal'),
('61-462-6', '4', '2018-05-23', 'normal'),
('61-462-6', '40', '2018-05-23', 'normal'),
('61-462-6', '41', '2018-05-23', 'normal'),
('61-462-6', '42', '2018-05-23', 'normal'),
('61-462-6', '43', '2018-05-23', 'normal'),
('61-462-6', '44', '2018-05-23', 'normal'),
('61-462-6', '45', '2018-05-23', 'normal'),
('61-462-6', '46', '2018-05-23', 'normal'),
('61-462-6', '47', '2018-05-23', 'normal'),
('61-462-6', '48', '2018-05-23', 'normal'),
('61-462-6', '49', '2018-05-23', 'normal'),
('61-462-6', '5', '2018-05-23', 'normal'),
('61-462-6', '50', '2018-05-23', 'normal'),
('61-462-6', '6', '2018-05-23', 'normal'),
('61-462-6', '7', '2018-05-23', 'normal'),
('61-462-6', '8', '2018-05-23', 'normal'),
('61-462-6', '9', '2018-05-23', 'normal'),
('61-463-3', '1', '2018-05-23', 'normal'),
('61-463-3', '10', '2018-05-23', 'normal'),
('61-463-3', '11', '2018-05-23', 'normal'),
('61-463-3', '12', '2018-05-23', 'normal'),
('61-463-3', '13', '2018-05-23', 'normal'),
('61-463-3', '14', '2018-05-23', 'normal'),
('61-463-3', '15', '2018-05-23', 'normal'),
('61-463-3', '16', '2018-05-23', 'normal'),
('61-463-3', '17', '2018-05-23', 'normal'),
('61-463-3', '18', '2018-05-23', 'normal'),
('61-463-3', '19', '2018-05-23', 'normal'),
('61-463-3', '2', '2018-05-23', 'normal'),
('61-463-3', '20', '2018-05-23', 'normal'),
('61-463-3', '21', '2018-05-23', 'normal'),
('61-463-3', '22', '2018-05-23', 'normal'),
('61-463-3', '23', '2018-05-23', 'normal'),
('61-463-3', '24', '2018-05-23', 'normal'),
('61-463-3', '25', '2018-05-23', 'normal'),
('61-463-3', '26', '2018-05-23', 'normal'),
('61-463-3', '27', '2018-05-23', 'normal'),
('61-463-3', '28', '2018-05-23', 'normal'),
('61-463-3', '29', '2018-05-23', 'normal'),
('61-463-3', '3', '2018-05-23', 'normal'),
('61-463-3', '30', '2018-05-23', 'normal'),
('61-463-3', '31', '2018-05-23', 'normal'),
('61-463-3', '32', '2018-05-23', 'normal'),
('61-463-3', '33', '2018-05-23', 'normal'),
('61-463-3', '34', '2018-05-23', 'normal'),
('61-463-3', '35', '2018-05-23', 'normal'),
('61-463-3', '36', '2018-05-23', 'normal'),
('61-463-3', '37', '2018-05-23', 'normal'),
('61-463-3', '38', '2018-05-23', 'normal'),
('61-463-3', '39', '2018-05-23', 'normal'),
('61-463-3', '4', '2018-05-23', 'normal'),
('61-463-3', '40', '2018-05-23', 'normal'),
('61-463-3', '5', '2018-05-23', 'normal'),
('61-463-3', '6', '2018-05-23', 'normal'),
('61-463-3', '7', '2018-05-23', 'normal'),
('61-463-3', '8', '2018-05-23', 'normal'),
('61-463-3', '9', '2018-05-23', 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `books_borrowing_limit`
--

CREATE TABLE `books_borrowing_limit` (
  `borrowing_limit` int(3) NOT NULL,
  `b_type` varchar(10) NOT NULL,
  `blid` smallint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books_borrowing_limit`
--

INSERT INTO `books_borrowing_limit` (`borrowing_limit`, `b_type`, `blid`) VALUES
(2, 'STUDENT', 1),
(3, 'STAFF', 2),
(20, 'DEPARTMENT', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `book_copy_report`
-- (See below for the actual view)
--
CREATE TABLE `book_copy_report` (
`ISBN` varchar(17)
,`CODE` varchar(50)
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
);

-- --------------------------------------------------------

--
-- Table structure for table `book_loan_info`
--

CREATE TABLE `book_loan_info` (
  `TRANSACTION_ID` int(11) NOT NULL,
  `ID` varchar(50) DEFAULT NULL,
  `ISBN` varchar(17) DEFAULT NULL,
  `LIBRARIAN_ID` varchar(10) DEFAULT NULL,
  `DATE_BORROWED` date NOT NULL,
  `RETURN_DATE` date NOT NULL,
  `DATE_DUE_BACK` date DEFAULT NULL,
  `DATE_OVERDUE` date DEFAULT NULL,
  `status_before` enum('good','bad') DEFAULT 'good',
  `status_after` enum('good','bad') DEFAULT 'good',
  `BORROWER_ID` varchar(10) NOT NULL,
  `BORROWER_TYPE` varchar(100) DEFAULT NULL,
  `LAST_EDIT` date DEFAULT NULL,
  `FLAG` enum('borrowed','lost','returned') DEFAULT 'borrowed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_loan_info`
--

INSERT INTO `book_loan_info` (`TRANSACTION_ID`, `ID`, `ISBN`, `LIBRARIAN_ID`, `DATE_BORROWED`, `RETURN_DATE`, `DATE_DUE_BACK`, `DATE_OVERDUE`, `status_before`, `status_after`, `BORROWER_ID`, `BORROWER_TYPE`, `LAST_EDIT`, `FLAG`) VALUES
(29, '1', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(30, '10', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(31, '11', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(32, '12', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(33, '13', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(34, '14', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(35, '15', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(36, '16', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(37, '17', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(38, '18', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(39, '2', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(40, '20', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(41, '21', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(42, '22', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(43, '23', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(44, '24', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(45, '25', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(46, '37', '61-463-3', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '1000000', 'STUDENT', '2018-05-23', 'borrowed');

-- --------------------------------------------------------

--
-- Table structure for table `book_type`
--

CREATE TABLE `book_type` (
  `ISBN` varchar(17) NOT NULL,
  `BOOK_TITLE` varchar(100) NOT NULL,
  `AUTHOR_NAME` varchar(100) NOT NULL,
  `EDITION` varchar(15) NOT NULL,
  `CLASS_ID` varchar(10) DEFAULT NULL,
  `A_ID` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `LAST_EDIT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_type`
--

INSERT INTO `book_type` (`ISBN`, `BOOK_TITLE`, `AUTHOR_NAME`, `EDITION`, `CLASS_ID`, `A_ID`, `year`, `LAST_EDIT`) VALUES
('04-002-5', 'MATHEMATICS FOR SECONDARY SCHOOLS FORM TWO', 'SCSU & MoEVT-ZANZIBAR,Rev,Ed,2010', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-057-2', 'ENGLISH FOR SECONDARY SCHOOLS FORM ONE', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-457-2', 'PHYSICS FOR SECONDARY SCHOOLS FORM SIX', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-458-9', 'ENGLISH FOR SECONDARY SCHOOLS FORM TWO', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-462-6', 'BIOLOGY FOR SECONDARY SCHOOLS FORM FIVE', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-463-3', 'BIOLOGY FOR SECONDARY SCHOOLS FORM SIX', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23');

-- --------------------------------------------------------

--
-- Stand-in structure for view `book_type_report`
-- (See below for the actual view)
--
CREATE TABLE `book_type_report` (
`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`LAST_EDIT` date
,`COPIES` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowable_view`
-- (See below for the actual view)
--
CREATE TABLE `borrowable_view` (
`ID` varchar(50)
,`ISBN` varchar(17)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowed_report`
-- (See below for the actual view)
--
CREATE TABLE `borrowed_report` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`EDITION` varchar(15)
,`BORROWER_DESCRIPTION` varchar(61)
,`BORROWER_ID` varchar(10)
,`BORROWER_TYPE` varchar(100)
,`STATUS_BEFORE` enum('good','bad')
,`FLAG` enum('borrowed','lost','returned')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowed_report_view`
-- (See below for the actual view)
--
CREATE TABLE `borrowed_report_view` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BORROWER_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`NAMES` varchar(61)
,`FLAG` enum('borrowed','lost','returned')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowers`
-- (See below for the actual view)
--
CREATE TABLE `borrowers` (
`borrower_id` varchar(10)
,`borrower_description` varchar(61)
,`b_type` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `borrower_type`
--

CREATE TABLE `borrower_type` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(30) NOT NULL,
  `SYMBOL` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowing_report`
-- (See below for the actual view)
--
CREATE TABLE `borrowing_report` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`DATE_DUE_BACK` date
,`DATE_OVERDUE` date
,`status_before` enum('good','bad')
,`status_after` enum('good','bad')
,`BORROWER_ID` varchar(10)
,`BORROWER_TYPE` varchar(100)
,`LAST_EDIT` date
,`FLAG` enum('borrowed','lost','returned')
,`NAMES` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `break_time`
--

CREATE TABLE `break_time` (
  `bid` int(2) NOT NULL,
  `after_period` int(2) NOT NULL,
  `description` varchar(100) NOT NULL,
  `duration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `break_time`
--

INSERT INTO `break_time` (`bid`, `after_period`, `description`, `duration`) VALUES
(2, 8, 'Lunch Break', 20),
(3, 5, 'Breakfast Break', 20);

-- --------------------------------------------------------

--
-- Stand-in structure for view `calculate_a_level_units_view`
-- (See below for the actual view)
--
CREATE TABLE `calculate_a_level_units_view` (
`term_id` int(11)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`points` int(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `calculate_o_level_units_view`
-- (See below for the actual view)
--
CREATE TABLE `calculate_o_level_units_view` (
`term_id` int(11)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`unit_point` int(2) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1c120fed1f3133e64270be376eb35b1afeeb5a68', '::1', 1597935942, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933353934323b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d6572726f725f6d6573736167657c733a32383a22496e76616c696420557365726e616d65206f722050617373776f7264223b5f5f63695f766172737c613a313a7b733a31333a226572726f725f6d657373616765223b733a333a226f6c64223b7d),
('1d01d26845e43a3c628dda3fad813ad9dfad8b00', '::1', 1597992336, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373939323333363b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('2a07e62b831e14cd246f2214c646dea8aed380c6', '::1', 1598288641, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383238383634313b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('33de8509e532154d795c5531a4ebbb0074c7d37b', '::1', 1597933837, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933333833373b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('382ca2ad44b7c4fcaf1d8a5a2caa2eecd7d43792', '::1', 1598004065, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383030343036353b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('4064fd58a34262a7a277128ad904311b7f4f9be9', '::1', 1597934177, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933343137373b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d);
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('47b0eb38184f33cd3529834578b3a6714757a26f', '::1', 1598288649, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383238383634313b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a33303a7b693a303b613a333a7b733a393a226d6f64756c655f6964223b733a313a2231223b733a31313a226d6f64756c655f6e616d65223b733a393a2241646d697373696f6e223b733a323a226964223b733a313a2231223b7d693a313b613a333a7b733a393a226d6f64756c655f6964223b733a313a2232223b733a31313a226d6f64756c655f6e616d65223b733a31343a224d616e6167652046696e616e6365223b733a323a226964223b733a313a2231223b7d693a323b613a333a7b733a393a226d6f64756c655f6964223b733a313a2233223b733a31313a226d6f64756c655f6e616d65223b733a31363a225374616666204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a333b613a333a7b733a393a226d6f64756c655f6964223b733a313a2234223b733a31313a226d6f64756c655f6e616d65223b733a31383a2253747564656e74204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a343b613a333a7b733a393a226d6f64756c655f6964223b733a313a2235223b733a31313a226d6f64756c655f6e616d65223b733a31383a224c696272617279204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a353b613a333a7b733a393a226d6f64756c655f6964223b733a313a2236223b733a31313a226d6f64756c655f6e616d65223b733a31323a224163636f6d6f646174696f6e223b733a323a226964223b733a313a2231223b7d693a363b613a333a7b733a393a226d6f64756c655f6964223b733a313a2237223b733a31313a226d6f64756c655f6e616d65223b733a31343a2241646d696e697374726174696f6e223b733a323a226964223b733a313a2231223b7d693a373b613a333a7b733a393a226d6f64756c655f6964223b733a313a2238223b733a31313a226d6f64756c655f6e616d65223b733a353a224578616d73223b733a323a226964223b733a313a2231223b7d693a383b613a333a7b733a393a226d6f64756c655f6964223b733a313a2239223b733a31313a226d6f64756c655f6e616d65223b733a373a225265706f727473223b733a323a226964223b733a313a2231223b7d693a393b613a333a7b733a393a226d6f64756c655f6964223b733a323a223130223b733a31313a226d6f64756c655f6e616d65223b733a393a22496e76656e746f7279223b733a323a226964223b733a313a2231223b7d693a31303b613a333a7b733a393a226d6f64756c655f6964223b733a323a223131223b733a31313a226d6f64756c655f6e616d65223b733a32313a224465706172746d656e74204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a31313b613a333a7b733a393a226d6f64756c655f6964223b733a323a223132223b733a31313a226d6f64756c655f6e616d65223b733a31363a22436c617373204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a31323b613a333a7b733a393a226d6f64756c655f6964223b733a323a223133223b733a31313a226d6f64756c655f6e616d65223b733a393a2254696d657461626c65223b733a323a226964223b733a313a2231223b7d693a31333b613a333a7b733a393a226d6f64756c655f6964223b733a323a223134223b733a31313a226d6f64756c655f6e616d65223b733a31333a22416e6e6f756e63656d656e7473223b733a323a226964223b733a313a2231223b7d693a31343b613a333a7b733a393a226d6f64756c655f6964223b733a323a223135223b733a31313a226d6f64756c655f6e616d65223b733a31393a224d792041737369676e656420436c6173736573223b733a323a226964223b733a313a2231223b7d693a31353b613a333a7b733a393a226d6f64756c655f6964223b733a323a223136223b733a31313a226d6f64756c655f6e616d65223b733a31383a22436f6d706c657465642053747564656e7473223b733a323a226964223b733a313a2231223b7d693a31363b613a333a7b733a393a226d6f64756c655f6964223b733a323a223137223b733a31313a226d6f64756c655f6e616d65223b733a31343a22544f44204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a31373b613a333a7b733a393a226d6f64756c655f6964223b733a323a223138223b733a31313a226d6f64756c655f6e616d65223b733a32313a224469736369706c696e65204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a31383b613a333a7b733a393a226d6f64756c655f6964223b733a323a223139223b733a31313a226d6f64756c655f6e616d65223b733a31303a224d79204163636f756e74223b733a323a226964223b733a313a2231223b7d693a31393b613a333a7b733a393a226d6f64756c655f6964223b733a323a223230223b733a31313a226d6f64756c655f6e616d65223b733a31333a224d79204465706172746d656e74223b733a323a226964223b733a313a2231223b7d693a32303b613a333a7b733a393a226d6f64756c655f6964223b733a323a223231223b733a31313a226d6f64756c655f6e616d65223b733a31323a224d7920446f726d69746f7279223b733a323a226964223b733a313a2231223b7d693a32313b613a333a7b733a393a226d6f64756c655f6964223b733a323a223232223b733a31313a226d6f64756c655f6e616d65223b733a383a224d7920436c617373223b733a323a226964223b733a313a2231223b7d693a32323b613a333a7b733a393a226d6f64756c655f6964223b733a323a223233223b733a31313a226d6f64756c655f6e616d65223b733a333a22544f44223b733a323a226964223b733a313a2231223b7d693a32333b613a333a7b733a393a226d6f64756c655f6964223b733a323a223234223b733a31313a226d6f64756c655f6e616d65223b733a373a22526573756c7473223b733a323a226964223b733a313a2231223b7d693a32343b613a333a7b733a393a226d6f64756c655f6964223b733a323a223235223b733a31313a226d6f64756c655f6e616d65223b733a343a22526f6c65223b733a323a226964223b733a313a2231223b7d693a32353b613a333a7b733a393a226d6f64756c655f6964223b733a323a223236223b733a31313a226d6f64756c655f6e616d65223b733a31333a22436c61737320526573756c7473223b733a323a226964223b733a313a2231223b7d693a32363b613a333a7b733a393a226d6f64756c655f6964223b733a323a223237223b733a31313a226d6f64756c655f6e616d65223b733a31383a225375626a656374204d616e6167656d656e74223b733a323a226964223b733a313a2231223b7d693a32373b613a333a7b733a393a226d6f64756c655f6964223b733a323a223238223b733a31313a226d6f64756c655f6e616d65223b733a31333a2241636164656d69632059656172223b733a323a226964223b733a313a2231223b7d693a32383b613a333a7b733a393a226d6f64756c655f6964223b733a323a223239223b733a31313a226d6f64756c655f6e616d65223b733a31343a2241646d696e2053657474696e6773223b733a323a226964223b733a313a2231223b7d693a32393b613a333a7b733a393a226d6f64756c655f6964223b733a323a223330223b733a31313a226d6f64756c655f6e616d65223b733a31333a224e6f74696669636174696f6e73223b733a323a226964223b733a313a2231223b7d7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a3134313a7b693a303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a313a2231223b733a31313a226465736372697074696f6e223b733a31323a224d616e616765205374616666223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223735223b733a323a226964223b733a313a2231223b7d693a313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a313a2233223b733a31313a226465736372697074696f6e223b733a33333a2241637469766174652d44656163746976617465205374616666204163636f756e74223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313735223b733a323a226964223b733a313a2231223b7d693a323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a313a2234223b733a31313a226465736372697074696f6e223b733a31363a2252656769737465722053747564656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a313a2235223b733a31313a226465736372697074696f6e223b733a32343a225669657720526567697374657265642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a313a2236223b733a31313a226465736372697074696f6e223b733a31363a2250726f6d6f74652053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2236223b733a323a226964223b733a313a2231223b7d693a353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223131223b733a31313a226465736372697074696f6e223b733a31373a224d616e616765204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223131223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223137223b733a323a226964223b733a313a2231223b7d693a363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223132223b733a31313a226465736372697074696f6e223b733a32363a2256696577205375626a656374205465616368696e67204c6f6773223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223131223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223137223b733a323a226964223b733a313a2231223b7d693a373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223133223b733a31313a226465736372697074696f6e223b733a32373a224d616e616765205465616368696e672041737369676e6d656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223131223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223137223b733a323a226964223b733a313a2231223b7d693a383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223135223b733a31313a226465736372697074696f6e223b733a373a2241646420484f44223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223131223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223137223b733a323a226964223b733a313a2231223b7d693a393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223137223b733a31313a226465736372697074696f6e223b733a31363a2256696577204465706172746d656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223131223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223137223b733a323a226964223b733a313a2231223b7d693a31303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223138223b733a31313a226465736372697074696f6e223b733a31373a224164642041636164656d69632059656172223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223139223b733a31313a226465736372697074696f6e223b733a31393a22566965772041636164656d6963205965617273223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223230223b733a31313a226465736372697074696f6e223b733a32303a225570646174652041636164656d69632059656172223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223231223b733a31313a226465736372697074696f6e223b733a31323a22416464204e6577205465726d223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223232223b733a31313a226465736372697074696f6e223b733a31313a22557064617465205465726d223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223233223b733a31313a226465736372697074696f6e223b733a31363a225365742043757272656e74205465726d223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223139223b733a323a226964223b733a313a2231223b7d693a31363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223234223b733a31313a226465736372697074696f6e223b733a393a225669657720544f4473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223137223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223234223b733a323a226964223b733a313a2231223b7d693a31373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223235223b733a31313a226465736372697074696f6e223b733a31313a224d616e61676520544f4473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223137223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223234223b733a323a226964223b733a313a2231223b7d693a31383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223237223b733a31313a226465736372697074696f6e223b733a31363a224d616e61676520446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a31393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223330223b733a31313a226465736372697074696f6e223b733a32373a224d616e616765204f274c6576656c2047726164652053797374656d223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223334223b733a323a226964223b733a313a2231223b7d693a32303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223331223b733a31313a226465736372697074696f6e223b733a32373a224d616e6167652041274c6576656c2047726164652053797374656d223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223335223b733a323a226964223b733a313a2231223b7d693a32313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223334223b733a31313a226465736372697074696f6e223b733a32353a2256696577204f274c6576656c2047726164652053797374656d223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223334223b733a323a226964223b733a313a2231223b7d693a32323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223335223b733a31313a226465736372697074696f6e223b733a32353a22566965772041274c6576656c2047726164652053797374656d223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223335223b733a323a226964223b733a313a2231223b7d693a32333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223336223b733a31313a226465736372697074696f6e223b733a31353a224d616e616765205375626a65637473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223339223b733a323a226964223b733a313a2231223b7d693a32343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223339223b733a31313a226465736372697074696f6e223b733a31333a2256696577205375626a65637473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223237223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223339223b733a323a226964223b733a313a2231223b7d693a32353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223430223b733a31313a226465736372697074696f6e223b733a32363a225669657720436c6173732053747265616d205375626a65637473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223237223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223430223b733a323a226964223b733a313a2231223b7d693a32363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223432223b733a31313a226465736372697074696f6e223b733a32363a2256696577204f776e204465706172746d656e7420537461666673223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223432223b733a323a226964223b733a313a2231223b7d693a32373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223433223b733a31313a226465736372697074696f6e223b733a31363a225669657720446f726d69746f72696573223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a32383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223434223b733a31313a226465736372697074696f6e223b733a32333a224d616e61676520446f726d69746f727920417373657473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313335223b733a323a226964223b733a313a2231223b7d693a32393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223435223b733a31313a226465736372697074696f6e223b733a31323a224d616e61676520436c617373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223435223b733a323a226964223b733a313a2231223b7d693a33303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223438223b733a31313a226465736372697074696f6e223b733a31353a225669657720544f44205265706f7274223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223137223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223438223b733a323a226964223b733a313a2231223b7d693a33313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223439223b733a31313a226465736372697074696f6e223b733a31383a224d616e61676520544f44205265706f727473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223137223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223438223b733a323a226964223b733a313a2231223b7d693a33323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223530223b733a31313a226465736372697074696f6e223b733a31323a225669657720436c6173736573223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223530223b733a323a226964223b733a313a2231223b7d693a33333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223535223b733a31313a226465736372697074696f6e223b733a31323a22566965772053747265616d73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223535223b733a323a226964223b733a313a2231223b7d693a33343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223538223b733a31313a226465736372697074696f6e223b733a31353a2256696577204578616d205479706573223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223538223b733a323a226964223b733a313a2231223b7d693a33353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223539223b733a31313a226465736372697074696f6e223b733a31363a22557064617465204578616d2054797065223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223538223b733a323a226964223b733a313a2231223b7d693a33363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223631223b733a31313a226465736372697074696f6e223b733a32353a22456e61626c652f44697361626c65204578616d205479706573223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223538223b733a323a226964223b733a313a2231223b7d693a33373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223632223b733a31313a226465736372697074696f6e223b733a31393a224368616e6765204f776e2050617373776f7264223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223139223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223632223b733a323a226964223b733a313a2231223b7d693a33383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223633223b733a31313a226465736372697074696f6e223b733a32313a225669657720436c61737320417474656e64616e6365223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223137223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223633223b733a323a226964223b733a313a2231223b7d693a33393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223634223b733a31313a226465736372697074696f6e223b733a32323a2256696577205363686f6f6c20417474656e64616e6365223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223634223b733a323a226964223b733a313a2231223b7d693a34303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223635223b733a31313a226465736372697074696f6e223b733a34333a224d616e6167652053636f7265202620526573756c7473204f662041737369676e6564205375626a65637473223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223635223b733a323a226964223b733a313a2231223b7d693a34313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223636223b733a31313a226465736372697074696f6e223b733a32343a224164642053636f7265204f66204f776e205375626a656374223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223635223b733a323a226964223b733a313a2231223b7d693a34323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223637223b733a31313a226465736372697074696f6e223b733a32343a224d616e6167652053656c65637465642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223833223b733a323a226964223b733a313a2231223b7d693a34333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223638223b733a31313a226465736372697074696f6e223b733a32373a2256696577204f776e204465706172746d656e7420436c6173736573223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223638223b733a323a226964223b733a313a2231223b7d693a34343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223639223b733a31313a226465736372697074696f6e223b733a32353a2241737369676e205465616368657220546f205375626a656374223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313030223b733a323a226964223b733a313a2231223b7d693a34353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223730223b733a31313a226465736372697074696f6e223b733a34343a2252656d6f7665205465616368696e672041737369676e6d656e7420496e204f776e204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313030223b733a323a226964223b733a313a2231223b7d693a34363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223731223b733a31313a226465736372697074696f6e223b733a32353a224578706c6f7265205363686f6f6c20417474656e64616e6365223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223634223b733a323a226964223b733a313a2231223b7d693a34373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223733223b733a31313a226465736372697074696f6e223b733a31363a224d616e6167652054696d657461626c65223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223133223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223830223b733a323a226964223b733a313a2231223b7d693a34383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223735223b733a31313a226465736372697074696f6e223b733a31313a225669657720537461666673223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223735223b733a323a226964223b733a313a2231223b7d693a34393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223737223b733a31313a226465736372697074696f6e223b733a32313a22416464204f776e205175616c696669636174696f6e223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223139223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223737223b733a323a226964223b733a313a2231223b7d693a35303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223738223b733a31313a226465736372697074696f6e223b733a31363a2256696577204f776e2050726f66696c65223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223139223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223738223b733a323a226964223b733a313a2231223b7d693a35313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223830223b733a31313a226465736372697074696f6e223b733a32313a2256696577205363686f6f6c2054696d657461626c65223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223133223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223830223b733a323a226964223b733a313a2231223b7d693a35323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223831223b733a31313a226465736372697074696f6e223b733a31383a22557064617465204f776e2050726f66696c65223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223139223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223738223b733a323a226964223b733a313a2231223b7d693a35333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223832223b733a31313a226465736372697074696f6e223b733a32373a225570646174652053636f7265204f66204f776e205375626a656374223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223635223b733a323a226964223b733a313a2231223b7d693a35343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223833223b733a31313a226465736372697074696f6e223b733a32323a22566965772053656c65637465642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223833223b733a323a226964223b733a313a2231223b7d693a35353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223834223b733a31313a226465736372697074696f6e223b733a32383a2256696577205472616e7366657272656420494e2053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223834223b733a323a226964223b733a313a2231223b7d693a35363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223835223b733a31313a226465736372697074696f6e223b733a32393a2256696577205472616e73666572726564204f55542053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223835223b733a323a226964223b733a313a2231223b7d693a35373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223836223b733a31313a226465736372697074696f6e223b733a32333a22566965772053757370656e6465642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223836223b733a323a226964223b733a313a2231223b7d693a35383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223838223b733a31313a226465736372697074696f6e223b733a32323a225669657720456e726f6c6c65642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223838223b733a323a226964223b733a313a2231223b7d693a35393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223839223b733a31313a226465736372697074696f6e223b733a32323a22566965772044697361626c65642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223839223b733a323a226964223b733a313a2231223b7d693a36303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223930223b733a31313a226465736372697074696f6e223b733a31353a2253757370656e642053747564656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a36313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223931223b733a31313a226465736372697074696f6e223b733a31383a225669657720436c6173732053747265616d73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a36323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223932223b733a31313a226465736372697074696f6e223b733a32363a22566965772053747564656e747320496e20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31353a22726f6f745f6368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a36333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223933223b733a31313a226465736372697074696f6e223b733a31363a225472616e736665722053747564656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a36343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223935223b733a31313a226465736372697074696f6e223b733a31333a224d616e6167652047726f757073223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223239223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223935223b733a323a226964223b733a313a2231223b7d693a36353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a323a223939223b733a31313a226465736372697074696f6e223b733a31393a2256696577205374616666732044657461696c73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223735223b733a323a226964223b733a313a2231223b7d693a36363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313030223b733a31313a226465736372697074696f6e223b733a34303a225669657720417373696e67656420546561636865727320496e204f776e204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313030223b733a323a226964223b733a313a2231223b7d693a36373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313031223b733a31313a226465736372697074696f6e223b733a34343a22557064617465205465616368696e672041737369676e6d656e7420496e204f776e204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313030223b733a323a226964223b733a313a2231223b7d693a36383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313033223b733a31313a226465736372697074696f6e223b733a33353a2256696577205465616368696e67204c6f6720496e204f776e204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223638223b733a323a226964223b733a313a2231223b7d693a36393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313034223b733a31313a226465736372697074696f6e223b733a31373a2256696577204163746976697479204c6f67223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223239223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313034223b733a323a226964223b733a313a2231223b7d693a37303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313035223b733a31313a226465736372697074696f6e223b733a32333a2256696577205374616666204163746976697479204c6f67223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223939223b733a323a226964223b733a313a2231223b7d693a37313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313036223b733a31313a226465736372697074696f6e223b733a31363a225669657720417564697420547261696c223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223239223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313036223b733a323a226964223b733a313a2231223b7d693a37323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313037223b733a31313a226465736372697074696f6e223b733a32303a22456469742053747564656e7420526573756c7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223136223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313739223b733a323a226964223b733a313a2231223b7d693a37333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313038223b733a31313a226465736372697074696f6e223b733a32323a2241646420436f6d706c6574696f6e2044657461696c73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223136223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313734223b733a323a226964223b733a313a2231223b7d693a37343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313039223b733a31313a226465736372697074696f6e223b733a32323a2256696577204f776e20436c61737320526573756c7473223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313039223b733a323a226964223b733a313a2231223b7d693a37353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313131223b733a31313a226465736372697074696f6e223b733a31393a2241646420436c617373204174746564616e6365223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313132223b733a323a226964223b733a313a2231223b7d693a37363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313132223b733a31313a226465736372697074696f6e223b733a32343a2256696577204f776e20436c617373204174746564616e6365223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313132223b733a323a226964223b733a313a2231223b7d693a37373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313133223b733a31313a226465736372697074696f6e223b733a32363a22566965772053747564656e747320496e204f776e20436c617373223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313133223b733a323a226964223b733a313a2231223b7d693a37383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313134223b733a31313a226465736372697074696f6e223b733a33353a22566965772041737369676e656420546561636865727320496e204f776e20436c617373223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313134223b733a323a226964223b733a313a2231223b7d693a37393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313135223b733a31313a226465736372697074696f6e223b733a32343a2256696577204f776e20436c6173732054696d657461626c65223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313135223b733a323a226964223b733a313a2231223b7d693a38303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313136223b733a31313a226465736372697074696f6e223b733a32343a2241646420436c617373204a6f75726e616c205265636f7264223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313138223b733a323a226964223b733a313a2231223b7d693a38313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313137223b733a31313a226465736372697074696f6e223b733a32303a2255706461746520436c617373204a6f75726e616c223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313138223b733a323a226964223b733a313a2231223b7d693a38323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313138223b733a31313a226465736372697074696f6e223b733a32393a2256696577204f776e20436c617373204a6f75726e616c205265706f7274223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313138223b733a323a226964223b733a313a2231223b7d693a38333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313139223b733a31313a226465736372697074696f6e223b733a31373a2241646420436c617373204d6f6e69746f72223b733a393a22636f6d6d6f6e5f746f223b733a31343a22636c6173735f7465616368657273223b733a393a226d6f64756c655f6964223b733a323a223232223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313133223b733a323a226964223b733a313a2231223b7d693a38343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313230223b733a31313a226465736372697074696f6e223b733a33303a22566965772053747564656e747320496e204f776e20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a31373a22646f726d69746f72795f6d617374657273223b733a393a226d6f64756c655f6964223b733a323a223231223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313230223b733a323a226964223b733a313a2231223b7d693a38353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313231223b733a31313a226465736372697074696f6e223b733a32303a2241646420446f726d69746f7279204c6561646572223b733a393a22636f6d6d6f6e5f746f223b733a31373a22646f726d69746f72795f6d617374657273223b733a393a226d6f64756c655f6964223b733a323a223231223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313230223b733a323a226964223b733a313a2231223b7d693a38363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313232223b733a31313a226465736372697074696f6e223b733a32383a22566965772041737365747320496e204f776e20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a31373a22646f726d69746f72795f6d617374657273223b733a393a226d6f64756c655f6964223b733a323a223231223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313232223b733a323a226964223b733a313a2231223b7d693a38373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313235223b733a31313a226465736372697074696f6e223b733a31343a225570646174652053747564656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a38383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313237223b733a31313a226465736372697074696f6e223b733a33313a224d616e616765204f776e20506f7374656420416e6e6f756e63656d656e7473223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223134223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313237223b733a323a226964223b733a313a2231223b7d693a38393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313238223b733a31313a226465736372697074696f6e223b733a31383a225669657720416e6e6f756e63656d656e7473223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223134223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313238223b733a323a226964223b733a313a2231223b7d693a39303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313331223b733a31313a226465736372697074696f6e223b733a31343a2256696577204f776e20526f6c6573223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223235223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313331223b733a323a226964223b733a313a2231223b7d693a39313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313332223b733a31313a226465736372697074696f6e223b733a32313a22566965772041737369676e656420436c6173736573223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313332223b733a323a226964223b733a313a2231223b7d693a39323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313333223b733a31313a226465736372697074696f6e223b733a32303a2241646420446f726d69746f7279204d6173746572223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a39333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313334223b733a31313a226465736372697074696f6e223b733a31343a225669657720526f6c6c2043616c6c223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a39343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313335223b733a31313a226465736372697074696f6e223b733a32343a22566965772041737365747320496e20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31353a22726f6f745f6368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223433223b733a323a226964223b733a313a2231223b7d693a39353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313337223b733a31313a226465736372697074696f6e223b733a31343a2252657365742050617373776f7264223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313337223b733a323a226964223b733a313a2231223b7d693a39363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313338223b733a31313a226465736372697074696f6e223b733a32343a224d616e6167652053746166667320417474656e64616e6365223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313635223b733a323a226964223b733a313a2231223b7d693a39373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313432223b733a31313a226465736372697074696f6e223b733a32313a224d616e61676520436c617373205375626a65637473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223430223b733a323a226964223b733a313a2231223b7d693a39383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313436223b733a31313a226465736372697074696f6e223b733a31333a2241646420526f6c6c2043616c6c223b733a393a22636f6d6d6f6e5f746f223b733a31373a22646f726d69746f72795f6d617374657273223b733a393a226d6f64756c655f6964223b733a323a223231223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313437223b733a323a226964223b733a313a2231223b7d693a39393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313437223b733a31313a226465736372697074696f6e223b733a33313a225669657720526f6c6c2043616c6c20496e204f776e20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a31373a22646f726d69746f72795f6d617374657273223b733a393a226d6f64756c655f6964223b733a323a223231223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313437223b733a323a226964223b733a313a2231223b7d693a3130303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313530223b733a31313a226465736372697074696f6e223b733a32343a224164642053747564656e7420546f20446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223932223b733a323a226964223b733a313a2231223b7d693a3130313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313534223b733a31313a226465736372697074696f6e223b733a31323a224d616e61676520526f6c6573223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a323a223235223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313534223b733a323a226964223b733a313a2231223b7d693a3130323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313535223b733a31313a226465736372697074696f6e223b733a31353a224d616e616765204469766973696f6e223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313538223b733a323a226964223b733a313a2231223b7d693a3130333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313538223b733a31313a226465736372697074696f6e223b733a31343a2256696577204469766973696f6e73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2238223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313538223b733a323a226964223b733a313a2231223b7d693a3130343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313631223b733a31313a226465736372697074696f6e223b733a32353a22526573746f72652053757370656e6465642053747564656e74223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223836223b733a323a226964223b733a313a2231223b7d693a3130353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313635223b733a31313a226465736372697074696f6e223b733a32313a225669657720537461666620417474656e64616e6365223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313635223b733a323a226964223b733a313a2231223b7d693a3130363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313639223b733a31313a226465736372697074696f6e223b733a32373a224d616e616765205374616666205175616c696669636174696f6e73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223939223b733a323a226964223b733a313a2231223b7d693a3130373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313730223b733a31313a226465736372697074696f6e223b733a33313a22566965772053747564656e747320496e204f776e204465706172746d656e74223b733a393a22636f6d6d6f6e5f746f223b733a343a22686f6473223b733a393a226d6f64756c655f6964223b733a323a223230223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223638223b733a323a226964223b733a313a2231223b7d693a3130383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313732223b733a31313a226465736372697074696f6e223b733a32333a225669657720506572736f6e616c2054696d657461626c65223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223133223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313732223b733a323a226964223b733a313a2231223b7d693a3130393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313734223b733a31313a226465736372697074696f6e223b733a31333a224d616e61676520416c756d6e69223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223136223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313734223b733a323a226964223b733a313a2231223b7d693a3131303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313735223b733a31313a226465736372697074696f6e223b733a32313a224d616e616765205374616666204163636f756e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a313a2233223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313735223b733a323a226964223b733a313a2231223b7d693a3131313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313737223b733a31313a226465736372697074696f6e223b733a31393a224d616e616765205465616368696e67204c6f67223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313839223b733a323a226964223b733a313a2231223b7d693a3131323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313739223b733a31313a226465736372697074696f6e223b733a31393a2256696577205363686f6f6c20526573756c7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223234223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313739223b733a323a226964223b733a313a2231223b7d693a3131333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313830223b733a31313a226465736372697074696f6e223b733a32373a224d616e61676520496e6469736369706c696e65205265636f726473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223138223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313831223b733a323a226964223b733a313a2231223b7d693a3131343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313831223b733a31313a226465736372697074696f6e223b733a32353a225669657720496e6469736369706c696e65205265636f726473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223138223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313831223b733a323a226964223b733a313a2231223b7d693a3131353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313832223b733a31313a226465736372697074696f6e223b733a31373a225669657720416c6c205465616368657273223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313832223b733a323a226964223b733a313a2231223b7d693a3131363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313833223b733a31313a226465736372697074696f6e223b733a31343a224d616e61676520506572696f6473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313834223b733a323a226964223b733a313a2231223b7d693a3131373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313834223b733a31313a226465736372697074696f6e223b733a31323a225669657720506572696f6473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313834223b733a323a226964223b733a313a2231223b7d693a3131383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313835223b733a31313a226465736372697074696f6e223b733a32333a22506572666f726d204461746162617365204261636b7570223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223239223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313835223b733a323a226964223b733a313a2231223b7d693a3131393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313836223b733a31313a226465736372697074696f6e223b733a31333a224368616e676520417661746172223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223139223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223738223b733a323a226964223b733a313a2231223b7d693a3132303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313839223b733a31313a226465736372697074696f6e223b733a31313a225669657720546f70696373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a31353a22726f6f745f6368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313332223b733a323a226964223b733a313a2231223b7d693a3132313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313930223b733a31313a226465736372697074696f6e223b733a31333a224d616e61676520546f70696373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313839223b733a323a226964223b733a313a2231223b7d693a3132323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313933223b733a31313a226465736372697074696f6e223b733a31343a225669657720537562746f70696373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a31353a22726f6f745f6368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313839223b733a323a226964223b733a313a2231223b7d693a3132333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313934223b733a31313a226465736372697074696f6e223b733a31363a224d616e61676520537562746f70696373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223135223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313933223b733a323a226964223b733a313a2231223b7d693a3132343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313937223b733a31313a226465736372697074696f6e223b733a32303a2241737369676e20436c6173732054656163686572223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3132353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313938223b733a31313a226465736372697074696f6e223b733a32323a22566965772053747564656e747320496e20436c617373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31353a22726f6f745f6368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3132363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22313939223b733a31313a226465736372697074696f6e223b733a31383a2256696577204e6f74696669636174696f6e73223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a323a223330223b733a31353a227065726d697373696f6e5f74797065223b733a393a22726f6f745f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313939223b733a323a226964223b733a313a2231223b7d693a3132373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323030223b733a31313a226465736372697074696f6e223b733a33373a225472616e736665722053747564656e7420546f20416e6f7468657220446f726d69746f7279223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2236223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223932223b733a323a226964223b733a313a2231223b7d693a3132383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323031223b733a31313a226465736372697074696f6e223b733a32353a225669657720436c617373204a6f75726e616c205265706f7274223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3132393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323032223b733a31313a226465736372697074696f6e223b733a32343a224d616e6167652044697361626c65642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223839223b733a323a226964223b733a313a2231223b7d693a3133303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323033223b733a31313a226465736372697074696f6e223b733a32383a22526573746f7265205472616e736665727265642053747564656e7473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223835223b733a323a226964223b733a313a2231223b7d693a3133313b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323035223b733a31313a226465736372697074696f6e223b733a32313a2241737369676e204f7074696f6e205375626a656374223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a3133323b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323036223b733a31313a226465736372697074696f6e223b733a33313a2253686966742053747564656e7420546f20416e6f746865722053747265616d223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313938223b733a323a226964223b733a313a2231223b7d693a3133333b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323037223b733a31313a226465736372697074696f6e223b733a32333a22416464205375626a6563747320546f2054656163686572223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313832223b733a323a226964223b733a313a2231223b7d693a3133343b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323038223b733a31313a226465736372697074696f6e223b733a32323a2256696577205375626a65637473205465616368657273223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223339223b733a323a226964223b733a313a2231223b7d693a3133353b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323131223b733a31313a226465736372697074696f6e223b733a32323a2255706c6f61642053747564656e7427732050686f746f223b733a393a22636f6d6d6f6e5f746f223b733a333a22616c6c223b733a393a226d6f64756c655f6964223b733a313a2234223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a313a2235223b733a323a226964223b733a313a2231223b7d693a3133363b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323132223b733a31313a226465736372697074696f6e223b733a31393a224d616e61676520436c61737320417373657473223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3133373b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323135223b733a31313a226465736372697074696f6e223b733a32303a22566965772041737365747320496e20436c617373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3133383b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323136223b733a31313a226465736372697074696f6e223b733a33313a22566965772041737369676e656420546561636865727320496e20436c617373223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a323a223132223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223931223b733a323a226964223b733a313a2231223b7d693a3133393b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323137223b733a31313a226465736372697074696f6e223b733a32303a225669657720546561636865722044657461696c73223b733a393a22636f6d6d6f6e5f746f223b733a353a226f74686572223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a333a22313832223b733a323a226964223b733a313a2231223b7d693a3134303b613a373a7b733a31333a227065726d697373696f6e5f6964223b733a333a22323138223b733a31313a226465736372697074696f6e223b733a31353a225669657720537461666620526f6c65223b733a393a22636f6d6d6f6e5f746f223b733a353a2261646d696e223b733a393a226d6f64756c655f6964223b733a313a2237223b733a31353a227065726d697373696f6e5f74797065223b733a31303a226368696c645f6e6f6465223b733a31323a22726f6f745f6e6f64655f6964223b733a323a223735223b733a323a226964223b733a313a2231223b7d7d7065726d616e656e745f726f6c657c733a31333a2241646d696e6973747261746f72223b73746166665f69647c733a313a2231223b757365725f726f6c657c733a353a2241646d696e223b6c6f676765645f696e7c623a313b706173745f6d69616b617c613a303a7b7d6d616e6167655f73746166667c733a323a226f6b223b61637469766174655f646561637469766174655f73746166665f6163636f756e747c733a323a226f6b223b72656769737465725f73747564656e747c733a323a226f6b223b766965775f726567697374657265645f73747564656e74737c733a323a226f6b223b70726f6d6f74655f73747564656e74737c733a323a226f6b223b6d616e6167655f6465706172746d656e747c733a323a226f6b223b766965775f7465616368696e675f6c6f67737c733a323a226f6b223b6d616e6167655f7465616368696e675f61737369676e6d656e74737c733a323a226f6b223b6164645f686f647c733a323a226f6b223b766965775f6465706172746d656e74737c733a323a226f6b223b6164645f61636164656d69635f796561727c733a323a226f6b223b766965775f61636164656d69635f79656172737c733a323a226f6b223b7570646174655f61636164656d69635f796561727c733a323a226f6b223b6164645f6e65775f7465726d7c733a323a226f6b223b7570646174655f7465726d7c733a323a226f6b223b7365745f63757272656e745f7465726d7c733a323a226f6b223b766965775f746f64737c733a323a226f6b223b6d616e6167655f746f64737c733a323a226f6b223b6d616e6167655f646f726d69746f72797c733a323a226f6b223b6d616e6167655f6f5f6c6576656c5f67726164696e675f73797374656d7c733a323a226f6b223b6d616e6167655f615f6c6576656c5f67726164696e675f73797374656d7c733a323a226f6b223b766965775f6f5f67726164657c733a323a226f6b223b766965775f615f67726164657c733a323a226f6b223b6d616e6167655f7375626a656374737c733a323a226f6b223b766965775f7375626a656374737c733a323a226f6b223b766965775f636c6173735f73747265616d5f7375626a656374737c733a323a226f6b223b766965775f6f776e5f6465706172746d656e74735f7374616666737c733a323a226f6b223b766965775f646f726d69746f726965737c733a323a226f6b223b6d616e6167655f646f726d69746f72795f6173736574737c733a323a226f6b223b6d616e6167655f636c6173737c733a323a226f6b223b766965775f746f645f7265706f72747c733a323a226f6b223b6d616e6167655f746f645f7265706f7274737c733a323a226f6b223b766965775f636c61737365737c733a323a226f6b223b766965775f73747265616d737c733a323a226f6b223b766965775f6578616d5f74797065737c733a323a226f6b223b7570646174655f6578616d5f747970657c733a323a226f6b223b656e61626c655f6f725f64697361626c655f6578616d5f747970657c733a323a226f6b223b6368616e67655f70617373776f72647c733a323a226f6b223b766965775f636c6173735f617474656e64616e63657c733a323a226f6b223b766965775f7363686f6f6c5f617474656e64616e63657c733a323a226f6b223b6d616e6167655f73636f72655f6f665f61737369676e65645f7375626a656374737c733a323a226f6b223b6164645f73636f72655f6f665f6f776e5f7375626a6563747c733a323a226f6b223b6d616e6167655f73656c65637465645f73747564656e74737c733a323a226f6b223b766965775f6465706172746d656e745f636c61737365737c733a323a226f6b223b61737369676e5f746561636865725f746f5f7375626a6563747c733a323a226f6b223b72656d6f76655f7465616368696e675f61737369676e6d656e745f696e5f6f776e5f6465706172746d656e747c733a323a226f6b223b6578706c6f72655f7363686f6f6c5f617474656e64616e63657c733a323a226f6b223b6d616e6167655f74696d657461626c657c733a323a226f6b223b766965775f7374616666737c733a323a226f6b223b6164645f6f776e5f7175616c696669636174696f6e7c733a323a226f6b223b766965775f6f776e5f70726f66696c657c733a323a226f6b223b766965775f636c6173735f74696d657461626c657c733a323a226f6b223b7570646174655f6f776e5f70726f66696c657c733a323a226f6b223b7570646174655f73636f72655f6f665f6f776e5f7375626a6563747c733a323a226f6b223b766965775f73656c65637465645f73747564656e74737c733a323a226f6b223b766965775f7472616e736665727265645f696e5f73747564656e74737c733a323a226f6b223b766965775f7472616e736665727265645f6f75745f73747564656e74737c733a323a226f6b223b766965775f73757370656e6465645f73747564656e74737c733a323a226f6b223b766965775f656e726f6c6c65645f73747564656e74737c733a323a226f6b223b766965775f64697361626c65645f73747564656e74737c733a323a226f6b223b73757370656e645f73747564656e747c733a323a226f6b223b766965775f636c6173735f73747265616d737c733a323a226f6b223b73747564656e74735f696e5f646f726d69746f72797c733a323a226f6b223b7472616e736665725f73747564656e747c733a323a226f6b223b6d616e6167655f67726f7570737c733a323a226f6b223b766965775f73746166665f64657461696c737c733a323a226f6b223b766965775f61737369676e65645f74656163686572735f696e5f6f776e5f6465706172746d656e747c733a323a226f6b223b7570646174655f7465616368696e675f61737369676e6d656e745f696e5f6f776e5f6465706172746d656e747c733a323a226f6b223b766965775f7465616368696e675f6c6f675f696e5f6f776e5f6465706172746d656e747c733a323a226f6b223b766965775f61637469766974795f6c6f677c733a323a226f6b223b766965775f73746166665f61637469766974795f6c6f677c733a323a226f6b223b766965775f61756469745f747261696c7c733a323a226f6b223b656469745f73747564656e745f726573756c74737c733a323a226f6b223b6164645f636f6d706c6574696f6e5f64657461696c737c733a323a226f6b223b766965775f6f776e5f636c6173735f726573756c74737c733a323a226f6b223b6164645f636c6173735f617474656e64616e63657c733a323a226f6b223b766965775f6f776e5f636c6173735f617474656e64616e63657c733a323a226f6b223b766965775f73747564656e74735f696e5f6f776e5f636c6173737c733a323a226f6b223b766965775f61737369676e65645f74656163686572735f696e5f6f776e5f636c6173737c733a323a226f6b223b766965775f6f776e5f636c6173735f74696d657461626c657c733a323a226f6b223b6164645f636c6173735f6a6f75726e616c5f7265636f72647c733a323a226f6b223b7570646174655f636c6173735f6a6f75726e616c7c733a323a226f6b223b766965775f6f776e5f636c6173735f6a6f75726e616c5f7265706f72747c733a323a226f6b223b6164645f636c6173735f6d6f6e69746f727c733a323a226f6b223b766965775f73747564656e74735f696e5f6f776e5f646f726d69746f72797c733a323a226f6b223b6164645f646f726d69746f72795f6c65616465727c733a323a226f6b223b766965775f6173736574735f696e5f6f776e5f646f726d69746f72797c733a323a226f6b223b7570646174655f73747564656e747c733a323a226f6b223b6d616e6167655f706f737465645f616e6e6f756e63656d656e74737c733a323a226f6b223b766965775f616e6e6f756e63656d656e74737c733a323a226f6b223b766965775f61737369676e65645f636c61737365737c733a323a226f6b223b6164645f646f726d69746f72795f6d61737465727c733a323a226f6b223b766965775f726f6c6c5f63616c6c7c733a323a226f6b223b766965775f6173736574735f696e5f646f726d69746f72797c733a323a226f6b223b72657365745f70617373776f72647c733a323a226f6b223b6d616e6167655f73746166665f617474656e64616e63657c733a323a226f6b223b6d616e6167655f636c6173735f7375626a656374737c733a323a226f6b223b6164645f726f6c6c5f63616c6c7c733a323a226f6b223b766965775f726f6c6c5f63616c6c5f696e5f6f776e5f646f726d69746f72797c733a323a226f6b223b6164645f73747564656e745f746f5f646f726d69746f72797c733a323a226f6b223b6d616e6167655f726f6c65737c733a323a226f6b223b6d616e6167655f6469766973696f6e7c733a323a226f6b223b766965775f6469766973696f6e737c733a323a226f6b223b726573746f72655f73757370656e6465645f73747564656e747c733a323a226f6b223b766965775f73746166665f617474656e64616e63657c733a323a226f6b223b6d616e6167655f73746166665f7175616c696669636174696f6e7c733a323a226f6b223b766965775f73747564656e74735f696e5f6f776e5f6465706172746d656e747c733a323a226f6b223b766965775f706572736f6e616c5f74696d657461626c657c733a323a226f6b223b6d616e6167655f616c756d6e697c733a323a226f6b223b6d616e6167655f73746166665f6163636f756e74737c733a323a226f6b223b6d616e6167655f7465616368696e675f6c6f677c733a323a226f6b223b766965775f7363686f6f6c5f726573756c74737c733a323a226f6b223b6d616e6167655f696e6469736369706c696e655f7265636f7264737c733a323a226f6b223b766965775f696e6469736369706c696e655f7265636f7264737c733a323a226f6b223b766965775f616c6c5f74656163686572737c733a323a226f6b223b6d616e6167655f706572696f64737c733a323a226f6b223b766965775f706572696f64737c733a323a226f6b223b64625f6261636b75707c733a323a226f6b223b6368616e67655f6176617461727c733a323a226f6b223b766965775f746f706963737c733a323a226f6b223b6d616e6167655f746f706963737c733a323a226f6b223b766965775f737562746f706963737c733a323a226f6b223b6d616e6167655f737562746f706963737c733a323a226f6b223b61737369676e5f636c6173735f746561636865727c733a323a226f6b223b766965775f73747564656e74735f696e5f636c6173737c733a323a226f6b223b766965775f6e6f74696669636174696f6e737c733a323a226f6b223b7472616e736665725f73747564656e745f746f5f616e6f746865725f646f726d69746f72797c733a323a226f6b223b766965775f636c6173735f6a6f75726e616c5f7265706f72747c733a323a226f6b223b6d616e6167655f64697361626c65645f73747564656e74737c733a323a226f6b223b726573746f72655f7472616e736665727265645f6f75745f73747564656e74737c733a323a226f6b223b61737369676e5f6f7074696f6e5f7375626a6563747c733a323a226f6b223b73686966745f73747564656e745f746f5f616e6f746865725f73747265616d7c733a323a226f6b223b6164645f7375626a656374735f746f5f746561636865727c733a323a226f6b223b766965775f7375626a6563745f74656163686572737c733a323a226f6b223b75706c6f61645f73747564656e745f70686f746f7c733a323a226f6b223b6d616e6167655f636c6173735f6173736574737c733a323a226f6b223b766965775f6173736574735f696e5f636c6173737c733a323a226f6b223b766965775f61737369676e65645f74656163686572735f696e5f636c6173737c733a323a226f6b223b766965775f746561636865725f64657461696c737c733a323a226f6b223b766965775f73746166665f726f6c657c733a323a226f6b223b);
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('590b35a6fa50afe4a597297a942c2035bb681e09', '::1', 1597950708, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373935303730383b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('614f3d869e1f75e6bc2f797759df8259cf8c50c9', '::1', 1597949888, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373934393838383b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('682a2e30182c46dbd116840467ed4e9002285925', '::1', 1597930510, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933303531303b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('7d25705417d8cc96d72b34001d45c7d812d45cb0', '::1', 1597951351, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373935313335313b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d6572726f725f6d6573736167657c733a32383a22496e76616c696420557365726e616d65206f722050617373776f7264223b5f5f63695f766172737c613a313a7b733a31333a226572726f725f6d657373616765223b733a333a226f6c64223b7d),
('81bc0124ef976cf52ff8428fa81c05b308e8af54', '::1', 1597932726, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933323732363b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('980c6a0c9a2fb179ffd473766614dcc5c4a75c2c', '::1', 1597934628, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933343632383b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('a8e3fc26d24a8bec4b36545b36e136896fd85401', '::1', 1597932278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373933323237383b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('b25a12b9fbf386e6ef7e03d2fb2de492bb48d3e2', '::1', 1597953802, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373935333830323b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('b94b13d77ddf31ff2754a1528fcb0d14019e7e3d', '::1', 1597949421, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373934393432313b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d6572726f725f6d6573736167657c733a32383a22496e76616c696420557365726e616d65206f722050617373776f7264223b5f5f63695f766172737c613a313a7b733a31333a226572726f725f6d657373616765223b733a333a226f6c64223b7d),
('e304ce2c605e9995b14ecfcc2f89c7fd33a6e587', '::1', 1597929461, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373932393436313b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('ebceddf33280142eebf6cb8c362988b810f8d655', '::1', 1597991934, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373939313933343b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d),
('ef28e32bce55c15072abe1dbafc7deaaa6569f9a', '::1', 1597981980, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539373938313938303b6e6f74696669636174696f6e737c733a313a2230223b6e6f74696669636174696f6e5f6d657373616765737c613a303a7b7d616e6e6f756e63656d656e74737c733a313a2230223b7465616368696e675f636c61737365737c733a313a2230223b636c61737365735f61737369676e65645f746f5f74656163687c613a303a7b7d636c6173735f73747265616d7c613a31313a7b733a31353a22636c6173735f73747265616d5f6964223b733a363a22433035504342223b733a383a22636c6173735f6964223b733a333a22433035223b733a363a2273747265616d223b733a333a22504342223b733a393a2273747265616d5f6964223b733a313a2239223b733a31303a22746561636865725f6964223b4e3b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a383a226361706163697479223b733a323a223530223b733a31383a226e756d6265725f6f665f7375626a65637473223b733a313a2235223b733a31303a2269645f646973706c6179223b4e3b733a32303a226e6578745f636c6173735f73747265616d5f6964223b733a363a22433036504342223b7d646570747c613a363a7b733a373a22646570745f6964223b733a323a223235223b733a393a22646570745f6e616d65223b733a393a2245434f4e4f4d494353223b733a383a22646570745f6c6f63223b733a373a22426c6f636b2042223b733a383a2273746166665f6964223b4e3b733a393a22646570745f74797065223b733a31383a227375626a6563745f6465706172746d656e74223b733a363a22625f74797065223b733a31303a224445504152544d454e54223b7d646f726d69746f72797c613a363a7b733a373a22646f726d5f6964223b733a313a2235223b733a393a22646f726d5f6e616d65223b733a393a224d414b4f4e474f524f223b733a383a226c6f636174696f6e223b733a31333a226e656172206d657373726f6f6d223b733a383a226361706163697479223b733a323a223738223b733a31323a2261646d697373696f6e5f6e6f223b4e3b733a31303a22746561636865725f6964223b4e3b7d6d6f64756c65737c613a303a7b7d726f6c65737c613a303a7b7d726f6c65735f7065726d697373696f6e7c613a303a7b7d);

-- --------------------------------------------------------

--
-- Stand-in structure for view `classes_assigned_view`
-- (See below for the actual view)
--
CREATE TABLE `classes_assigned_view` (
`class_stream_id` varchar(10)
,`teacher_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`start_date` date
,`end_date` date
);

-- --------------------------------------------------------

--
-- Table structure for table `class_asset`
--

CREATE TABLE `class_asset` (
  `id` int(11) NOT NULL,
  `asset_no` varchar(50) NOT NULL,
  `asset_name` varchar(20) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `status` mediumtext NOT NULL,
  `description` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_asset_view`
-- (See below for the actual view)
--
CREATE TABLE `class_asset_view` (
`id` int(11)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`asset_name` varchar(20)
,`asset_no` varchar(50)
,`status` mediumtext
,`description` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_journal_view`
-- (See below for the actual view)
--
CREATE TABLE `class_journal_view` (
`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`timetable_class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);

-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE `class_level` (
  `class_id` varchar(10) NOT NULL,
  `class_name` varchar(30) NOT NULL,
  `level` enum('O''Level','A''Level') NOT NULL,
  `next_class_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` (`class_id`, `class_name`, `level`, `next_class_id`) VALUES
('C01', 'Form One', 'O\'Level', 'C02'),
('C02', 'Form Two', 'O\'Level', 'C03'),
('C05', 'Form Five', 'A\'Level', 'C06'),
('C06', 'Form Six', 'A\'Level', 'C07');

-- --------------------------------------------------------

--
-- Table structure for table `class_stream`
--

CREATE TABLE `class_stream` (
  `class_stream_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `stream` varchar(5) NOT NULL,
  `stream_id` int(2) NOT NULL,
  `teacher_id` varchar(10) DEFAULT NULL,
  `admission_no` varchar(10) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `capacity` int(3) DEFAULT NULL,
  `number_of_subjects` int(2) NOT NULL,
  `id_display` varchar(30) DEFAULT NULL,
  `next_class_stream_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_stream`
--

INSERT INTO `class_stream` (`class_stream_id`, `class_id`, `stream`, `stream_id`, `teacher_id`, `admission_no`, `description`, `capacity`, `number_of_subjects`, `id_display`, `next_class_stream_id`) VALUES
('C05EGM', 'C05', 'EGM', 13, 'STAFF0001', NULL, '', 66, 4, NULL, 'C06EGM'),
('C05PCB', 'C05', 'PCB', 9, NULL, NULL, '', 50, 5, NULL, 'C06PCB'),
('C05PGM', 'C05', 'PGM', 12, NULL, NULL, '', 60, 4, NULL, 'C06PGM');

-- --------------------------------------------------------

--
-- Table structure for table `class_stream_subject`
--

CREATE TABLE `class_stream_subject` (
  `class_stream_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_stream_subject`
--

INSERT INTO `class_stream_subject` (`class_stream_id`, `subject_id`) VALUES
('C05EGM', 'SUB01'),
('C05EGM', 'SUB04'),
('C05EGM', 'SUB05'),
('C05EGM', 'SUB08'),
('C05PCB', 'SUB01'),
('C05PCB', 'SUB02'),
('C05PCB', 'SUB03'),
('C05PCB', 'SUB07'),
('C05PCB', 'SUB09');

--
-- Triggers `class_stream_subject`
--
DELIMITER $$
CREATE TRIGGER `after_update_on_class_stream_subject` BEFORE UPDATE ON `class_stream_subject` FOR EACH ROW BEGIN
UPDATE student_subject SET class_stream_id="", subject_id="" WHERE class_stream_id=OLD.class_stream_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_stream_subject_display_view`
-- (See below for the actual view)
--
CREATE TABLE `class_stream_subject_display_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`subject_id` varchar(10)
,`stream` varchar(5)
,`class_subjects` mediumtext
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_stream_subject_view`
-- (See below for the actual view)
--
CREATE TABLE `class_stream_subject_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`subject_choice` enum('optional','compulsory')
,`subject_category` enum('T','N')
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_subject_teachers_view`
-- (See below for the actual view)
--
CREATE TABLE `class_subject_teachers_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`names` varchar(61)
,`subject_name` varchar(50)
,`class_stream_id` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `class_teacher_history`
--

CREATE TABLE `class_teacher_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_teacher_history`
--

INSERT INTO `class_teacher_history` (`id`, `class_stream_id`, `teacher_id`, `start_date`, `end_date`) VALUES
(1, 'C05EGM', 'STAFF0001', '2018-06-04', '2018-06-05'),
(2, 'C05EGM', 'STAFF1111', '2018-06-05', '2018-06-05'),
(3, 'C05EGM', 'STAFF0001', '2018-06-05', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `completed_students_view`
-- (See below for the actual view)
--
CREATE TABLE `completed_students_view` (
`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`id` int(11)
,`index_no` varchar(30)
,`admission_no` varchar(10)
,`division` varchar(2)
,`points` int(2)
,`kashachukua_cheti` enum('ndiyo','hapana')
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`academic_year_id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `compute_division`
--

CREATE TABLE `compute_division` (
  `id` int(1) NOT NULL,
  `level` enum('O','A') NOT NULL,
  `nos` int(2) NOT NULL,
  `penalty` enum('yes','no') NOT NULL DEFAULT 'no',
  `penalty_divs` varchar(50) DEFAULT NULL,
  `assignedDiv` varchar(10) DEFAULT NULL,
  `penalty_grade` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `compute_division`
--

INSERT INTO `compute_division` (`id`, `level`, `nos`, `penalty`, `penalty_divs`, `assignedDiv`, `penalty_grade`) VALUES
(24, 'O', 7, 'yes', 'I,II', 'III', 'F'),
(25, 'A', 3, 'no', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `copy_duty_roaster`
--

CREATE TABLE `copy_duty_roaster` (
  `teacher_id` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `count_borrowed_per_person_view`
-- (See below for the actual view)
--
CREATE TABLE `count_borrowed_per_person_view` (
`copies` bigint(21)
,`names` varchar(61)
,`borrower_type` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `current_term_academic_year_view`
-- (See below for the actual view)
--
CREATE TABLE `current_term_academic_year_view` (
`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`class_level` enum('A''Level','O''Level')
,`id` int(5)
,`year` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_attendance_report_view`
-- (See below for the actual view)
--
CREATE TABLE `daily_attendance_report_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`att_type` char(1)
,`nos` bigint(21)
,`date_` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_school_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `daily_school_attendance_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`att_type` char(1)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`date_` date
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_student_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `daily_student_attendance_view` (
`admission_no` varchar(10)
,`names` varchar(41)
,`att_type` char(1)
,`description` varchar(50)
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`date_` date
,`class_stream_id` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_student_roll_call_view`
-- (See below for the actual view)
--
CREATE TABLE `daily_student_roll_call_view` (
`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`att_type` char(1)
,`description` varchar(50)
,`date_` date
,`dorm_id` int(3)
,`att_id` int(2)
);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dept_id` int(3) UNSIGNED NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `dept_loc` varchar(50) DEFAULT NULL,
  `staff_id` varchar(10) DEFAULT NULL,
  `dept_type` enum('subject_department','non_subject_department') NOT NULL DEFAULT 'subject_department',
  `b_type` enum('DEPARTMENT') DEFAULT 'DEPARTMENT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `dept_name`, `dept_loc`, `staff_id`, `dept_type`, `b_type`) VALUES
(18, 'PHYSICS', 'Block A', 'STAFF0001', 'subject_department', 'DEPARTMENT'),
(19, 'GEOGRAPHY', 'BLOCK B', 'STAFF3333', 'subject_department', 'DEPARTMENT'),
(20, 'MATHEMATICS', 'Block B', 'STAFF2020', 'subject_department', 'DEPARTMENT'),
(21, 'GS and CIVICS', 'Block B', 'STAFF2222', 'subject_department', 'DEPARTMENT'),
(22, 'CHEMISTRY', 'BLOCK F', 'STAFF0035', 'subject_department', 'DEPARTMENT'),
(23, 'BIOLOGY', 'Block C', 'STAFF3211', 'subject_department', 'DEPARTMENT'),
(25, 'ECONOMICS', 'Block B', NULL, 'subject_department', 'DEPARTMENT');

-- --------------------------------------------------------

--
-- Stand-in structure for view `department_class_stream_subject_view`
-- (See below for the actual view)
--
CREATE TABLE `department_class_stream_subject_view` (
`class_name` varchar(30)
,`subject_name` varchar(50)
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`class_stream_id` varchar(10)
,`class_id` varchar(10)
,`stream` varchar(5)
,`stream_id` int(2)
,`teacher_id` varchar(10)
,`admission_no` varchar(10)
,`description` varchar(255)
,`capacity` int(3)
,`id_display` varchar(30)
,`subject_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`staff_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `department_staff_view`
-- (See below for the actual view)
--
CREATE TABLE `department_staff_view` (
`staff_id` varchar(10)
,`staff_names` varchar(92)
,`dept_id` int(3) unsigned
,`gender` varchar(6)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `department_view`
-- (See below for the actual view)
--
CREATE TABLE `department_view` (
`staff_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`dept_type` enum('subject_department','non_subject_department')
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `disabled_students`
-- (See below for the actual view)
--
CREATE TABLE `disabled_students` (
`disabled` enum('yes','no')
,`statasi` enum('active','inactive')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `discipline`
--

CREATE TABLE `discipline` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `description` mediumtext NOT NULL,
  `decision` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discipline`
--

INSERT INTO `discipline` (`id`, `admission_no`, `description`, `decision`, `date`) VALUES
(1, '1000000', 'katorokashuleni usiku nakwenda kuangalia mpirA', 'KURUDI NYUMBANI KWA WIKI MOJA', '2018-05-24'),
(2, '1234567', 'Kakutwa town bila uniform', 'SUSPENDED FOR 7 Days', '2018-05-24'),
(3, '1000345', 'Mlevi				Sanaaaa					fkhfkhfkf', 'EXPELLED FROM STUDIES FFFFFF', '2018-05-31'),
(4, '9999999', 'Kalevi		Hakaa', 'EXPELLED FROM STUDIES HHH', '2018-06-07'),
(5, '1000001', 'Oyooo', 'wtrh', '2018-05-30'),
(6, '1000003', 'Holla', 'EXPELLED FROM STUDIES', '2018-05-30'),
(7, '5552231', 'Kakutwa town', 'SUSPENDED FOR 13 Days', '2018-05-30'),
(8, '5552231', 'Holaaa', 'SUSPENDED FOR 8 Days', '2018-05-31'),
(9, '0987654', 'Hallo', 'Suspended', '2018-06-06'),
(10, '0987654', 'fighting academia', 'SUSPENDED FOR 21 Days', '2018-06-05'),
(11, '1000127', 'mwizi', 'EXPELLED FROM STUDIES', '2018-06-05'),
(12, '1031456', 'mwizi', 'EXPELLED FROM STUDIES', '2018-06-05'),
(13, '0987654', 'stole mashauris property and used abusive language', 'for sunspension', '2018-06-05');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(3) NOT NULL,
  `level` enum('O','A') NOT NULL,
  `starting_points` int(2) NOT NULL,
  `ending_points` int(2) NOT NULL,
  `div_name` enum('I','II','III','IV','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `level`, `starting_points`, `ending_points`, `div_name`) VALUES
(1, '', 1, 1, ''),
(2, 'O', 1, 17, 'I'),
(3, 'O', 18, 21, 'II'),
(4, 'O', 22, 26, 'III'),
(5, 'O', 27, 33, 'IV'),
(6, 'O', 34, 35, '0'),
(7, 'A', 3, 9, 'I'),
(14, 'A', 10, 12, 'II'),
(9, 'A', 13, 16, 'III'),
(10, 'A', 17, 19, 'IV'),
(13, 'A', 20, 21, '0');

--
-- Triggers `division`
--
DELIMITER $$
CREATE TRIGGER `before_delete_division_trigger` BEFORE DELETE ON `division` FOR EACH ROW BEGIN DECLARE x INT DEFAULT 0; SELECT id FROM division WHERE id=OLD.id INTO x; IF x = 1 THEN SIGNAL SQLSTATE '45000' SET message_text="Permission Denied, Cannot delete this value"; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `division_bk`
--

CREATE TABLE `division_bk` (
  `id` int(3) NOT NULL DEFAULT 0,
  `level` enum('O','A') NOT NULL,
  `starting_points` int(2) NOT NULL,
  `ending_points` int(2) NOT NULL,
  `div_name` enum('I','II','III','IV','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division_bk`
--

INSERT INTO `division_bk` (`id`, `level`, `starting_points`, `ending_points`, `div_name`) VALUES
(1, '', 1, 1, ''),
(2, 'O', 1, 17, 'I'),
(3, 'O', 18, 21, 'II'),
(4, 'O', 22, 26, 'III'),
(5, 'O', 27, 33, 'IV'),
(6, 'O', 34, 35, '0'),
(7, 'A', 3, 9, 'I'),
(8, 'A', 10, 12, 'II'),
(9, 'A', 13, 16, 'III'),
(10, 'A', 17, 19, 'IV'),
(11, 'A', 20, 21, '0');

-- --------------------------------------------------------

--
-- Table structure for table `dormitory`
--

CREATE TABLE `dormitory` (
  `dorm_id` int(3) NOT NULL,
  `dorm_name` varchar(30) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `capacity` int(11) NOT NULL,
  `admission_no` varchar(10) DEFAULT NULL,
  `teacher_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dormitory`
--

INSERT INTO `dormitory` (`dorm_id`, `dorm_name`, `location`, `capacity`, `admission_no`, `teacher_id`) VALUES
(1, 'MKWAWA', 'south west', 66, NULL, 'STAFF1111'),
(2, 'SAMORA', 'far east', 100, NULL, 'STAFF0003'),
(3, 'MANDELA', 'near messroom', 120, NULL, 'STAFF0001'),
(4, 'KENYATTA', 'behind f v classes', 65, NULL, 'STAFF3211'),
(5, 'MAKONGORO', 'near messroom', 78, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dorm_asset`
--

CREATE TABLE `dorm_asset` (
  `id` int(11) NOT NULL,
  `asset_no` varchar(50) NOT NULL,
  `asset_name` varchar(20) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `status` mediumtext NOT NULL,
  `description` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dorm_asset_view`
-- (See below for the actual view)
--
CREATE TABLE `dorm_asset_view` (
`id` int(11)
,`dorm_id` int(3)
,`dorm_name` varchar(30)
,`asset_name` varchar(20)
,`asset_no` varchar(50)
,`status` mediumtext
,`description` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dorm_capacity_view`
-- (See below for the actual view)
--
CREATE TABLE `dorm_capacity_view` (
`dorm_id` int(3)
,`no_of_students` bigint(21)
,`dorm_name` varchar(30)
,`capacity` int(11)
,`remaining` bigint(22)
);

-- --------------------------------------------------------

--
-- Table structure for table `dorm_master_history`
--

CREATE TABLE `dorm_master_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dorm_master_history`
--

INSERT INTO `dorm_master_history` (`id`, `dorm_id`, `teacher_id`, `start_date`, `end_date`) VALUES
(1, 1, 'STAFF0001', '2018-06-01', '2018-06-01'),
(2, 1, 'STAFF1111', '2018-06-01', NULL),
(3, 2, 'STAFF0035', '2018-06-01', '2018-06-01'),
(4, 2, 'STAFF0003', '2018-06-01', NULL),
(5, 4, 'STAFF0001', '2018-06-05', '2018-06-05'),
(6, 4, 'STAFF3211', '2018-06-05', NULL),
(7, 3, 'STAFF0001', '2018-06-05', '2018-06-06'),
(8, 3, 'STAFF0001', '2018-06-06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `duty_roaster`
--

CREATE TABLE `duty_roaster` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_current` enum('T','F') NOT NULL DEFAULT 'F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `duty_roaster`
--

INSERT INTO `duty_roaster` (`id`, `start_date`, `end_date`, `is_current`) VALUES
(1, '2017-12-18', '2017-12-24', 'F'),
(4, '2017-12-25', '2017-12-31', 'F'),
(5, '2018-01-01', '2018-01-07', 'F'),
(7, '2018-03-19', '2018-03-25', 'F'),
(8, '2018-01-22', '2018-01-28', 'F'),
(9, '2018-02-05', '2018-02-11', 'F'),
(12, '2016-08-01', '2016-08-07', 'F'),
(17, '2019-01-07', '2019-01-13', 'F'),
(23, '2021-01-25', '2021-01-31', 'F'),
(26, '2018-05-28', '2018-06-03', 'T'),
(27, '2018-06-04', '2018-06-10', 'F');

--
-- Triggers `duty_roaster`
--
DELIMITER $$
CREATE TRIGGER `before_delete_duty_roaster` BEFORE DELETE ON `duty_roaster` FOR EACH ROW BEGIN DECLARE is_cur ENUM('T','F') DEFAULT 'F'; SELECT is_current FROM duty_roaster WHERE id=OLD.id INTO is_cur; IF is_cur = 'T' THEN SIGNAL SQLSTATE '45000' SET message_text='Cannot delete this record while currently set as on duty'; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `edit_teaching_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `edit_teaching_attendance_view` (
`subTopicID` int(11)
,`subtopic_name` varchar(255)
,`ta_id` bigint(20)
,`no_of_absentees` int(2)
,`status` enum('taught','untaught','none')
,`date` date
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `edit_teaching_attendance_view_mpya`
-- (See below for the actual view)
--
CREATE TABLE `edit_teaching_attendance_view_mpya` (
`subTopicID` int(11)
,`subtopic_name` varchar(255)
,`ta_id` bigint(20)
,`no_of_absentees` int(2)
,`status` enum('taught','untaught','none')
,`date` date
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

CREATE TABLE `enrollment` (
  `admission_no` varchar(10) NOT NULL DEFAULT '',
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL DEFAULT '',
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enrollment`
--

INSERT INTO `enrollment` (`admission_no`, `class_id`, `class_stream_id`, `academic_year`) VALUES
('0987654', 'C05', 'C05PCB', 3),
('1000120', 'C05', 'C05EGM', 3),
('1000121', 'C05', 'C05PCB', 3),
('1000122', 'C05', 'C05EGM', 3),
('1000123', 'C05', 'C05PCB', 3),
('1000124', 'C05', 'C05EGM', 3),
('1000125', 'C05', 'C05PCB', 3),
('1000126', 'C05', 'C05EGM', 3),
('1000127', 'C05', 'C05PCB', 3),
('1031456', 'C05', 'C05EGM', 3),
('1234567', 'C05', 'C05PCB', 3),
('1563242', 'C05', 'C05EGM', 3),
('1647366', 'C05', 'C05PCB', 3),
('4352111', 'C05', 'C05EGM', 3),
('5555555', 'C05', 'C05EGM', 3),
('6754678', 'C05', 'C05PCB', 3),
('7656777', 'C05', 'C05EGM', 3);

-- --------------------------------------------------------

--
-- Table structure for table `exam_type`
--

CREATE TABLE `exam_type` (
  `id` varchar(10) NOT NULL,
  `exam_name` varchar(30) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `academic_year` int(5) DEFAULT NULL,
  `a_level_academic_year` int(5) DEFAULT NULL,
  `add_score_enabled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exam_type`
--

INSERT INTO `exam_type` (`id`, `exam_name`, `start_date`, `end_date`, `academic_year`, `a_level_academic_year`, `add_score_enabled`) VALUES
('E01', 'First Monthly Examinations', '2018-04-11', '2018-04-16', NULL, 3, 0),
('E02', 'Midterm Examinations', '2018-08-31', '2018-09-04', NULL, 3, 0),
('E03', 'Second Monthly Examinations', '2018-10-28', '2018-10-30', NULL, 3, 0),
('E04', 'End of term Examinations', '2018-12-01', '2018-12-10', NULL, 3, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `expelled_students_view`
-- (See below for the actual view)
--
CREATE TABLE `expelled_students_view` (
`admission_no` varchar(10)
,`date` date
,`academic_year` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`dob` date
,`tribe_id` int(5)
,`religion_id` int(5)
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`dorm_id` int(3)
,`status` enum('active','inactive')
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `expired_book_view`
-- (See below for the actual view)
--
CREATE TABLE `expired_book_view` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BORROWER_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`NAMES` varchar(61)
,`FLAG` enum('borrowed','lost','returned')
);

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE `fee` (
  `fee_id` varchar(10) NOT NULL,
  `fee_name` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fee_details`
--

CREATE TABLE `fee_details` (
  `id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `fee_id` varchar(10) NOT NULL,
  `date_paid` date NOT NULL,
  `amount_paid` int(11) NOT NULL,
  `amount_remaining` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fee_details`
--

INSERT INTO `fee_details` (`id`, `admission_no`, `fee_id`, `date_paid`, `amount_paid`, `amount_remaining`) VALUES
(1, 'STD0081', 'F1', '2016-07-20', 20000, 0),
(2, 'STD0081', 'F2', '2016-07-20', 10000, 0),
(3, 'STD0081', 'F3', '2016-07-20', 20000, 0),
(4, 'STD0081', 'F4', '2016-07-20', 15000, 0),
(5, 'STD0081', 'F5', '2016-07-20', 5000, 0),
(6, 'STD0081', 'F6', '2016-07-20', 3000, 0),
(7, 'STD0081', 'F7', '2016-07-20', 10000, 0),
(8, 'STD0082', 'F1', '2016-07-21', 20000, 0),
(9, 'STD0082', 'F2', '2016-07-21', 10000, 0),
(10, 'STD0082', 'F3', '2016-07-21', 20000, 0),
(11, 'STD0082', 'F4', '2016-07-21', 10000, 0),
(12, 'STD0082', 'F5', '2016-07-21', 5000, 0),
(13, 'STD0082', 'F6', '2016-07-21', 3000, 0),
(14, 'STD0082', 'F7', '2016-07-21', 5000, 0),
(15, 'STD0083', 'F1', '2016-07-21', 20000, 0),
(16, 'STD0083', 'F2', '2016-07-21', 10000, 0),
(17, 'STD0083', 'F3', '2016-07-21', 20000, 0),
(18, 'STD0083', 'F4', '2016-07-21', 15000, 0),
(19, 'STD0083', 'F5', '2016-07-21', 5000, 0),
(20, 'STD0083', 'F6', '2016-07-21', 3000, 0),
(21, 'STD0083', 'F7', '2016-07-21', 10000, 0),
(22, 'STD0084', 'F1', '2016-07-10', 20000, 0),
(23, 'STD0084', 'F2', '2016-07-10', 10000, 0),
(24, 'STD0084', 'F3', '2016-07-10', 20000, 0),
(25, 'STD0084', 'F4', '2016-07-10', 15000, 0),
(26, 'STD0084', 'F5', '2016-07-10', 5000, 0),
(27, 'STD0084', 'F6', '2016-07-10', 3000, 0),
(28, 'STD0084', 'F7', '2016-07-10', 10000, 0),
(29, 'STD0085', 'F1', '2016-07-10', 20000, 0),
(30, 'STD0085', 'F2', '2016-07-10', 10000, 0),
(31, 'STD0085', 'F3', '2016-07-10', 20000, 0),
(32, 'STD0085', 'F4', '2016-07-10', 15000, 0),
(33, 'STD0085', 'F5', '2016-07-10', 5000, 0),
(34, 'STD0085', 'F6', '2016-07-10', 3000, 0),
(35, 'STD0085', 'F7', '2016-07-10', 10000, 0),
(36, 'STD0086', 'F1', '2016-07-11', 20000, 0),
(37, 'STD0086', 'F2', '2016-07-11', 10000, 0),
(38, 'STD0086', 'F3', '2016-07-11', 20000, 0),
(39, 'STD0086', 'F4', '2016-07-11', 15000, 0),
(40, 'STD0086', 'F5', '2016-07-11', 5000, 0),
(41, 'STD0086', 'F6', '2016-07-11', 0, 0),
(42, 'STD0086', 'F7', '2016-07-11', 0, 0),
(43, 'STD0087', 'F1', '2016-07-17', 20000, 0),
(44, 'STD0087', 'F2', '2016-07-17', 10000, 0),
(45, 'STD0087', 'F3', '2016-07-17', 20000, 0),
(46, 'STD0087', 'F4', '2016-07-17', 15000, 0),
(47, 'STD0087', 'F5', '2016-07-17', 2000, 0),
(48, 'STD0087', 'F6', '2016-07-17', 0, 0),
(49, 'STD0087', 'F7', '2016-07-17', 0, 0),
(50, 'STD0088', 'F1', '2016-07-17', 20000, 0),
(51, 'STD0088', 'F2', '2016-07-17', 10000, 0),
(52, 'STD0088', 'F3', '2016-07-17', 20000, 0),
(53, 'STD0088', 'F4', '2016-07-17', 15000, 0),
(54, 'STD0088', 'F5', '2016-07-17', 5000, 0),
(55, 'STD0088', 'F6', '2016-07-17', 3000, 0),
(56, 'STD0088', 'F7', '2016-07-17', 10000, 0),
(57, 'STD0089', 'F1', '2016-07-17', 20000, 0),
(58, 'STD0089', 'F2', '2016-07-17', 10000, 0),
(59, 'STD0089', 'F3', '2016-07-17', 20000, 0),
(60, 'STD0089', 'F4', '2016-07-17', 15000, 0),
(61, 'STD0089', 'F5', '2016-07-17', 5000, 0),
(62, 'STD0089', 'F6', '2016-07-17', 3000, 0),
(63, 'STD0089', 'F7', '2016-07-17', 10000, 0),
(64, 'STD0090', 'F1', '2016-07-18', 20000, 0),
(65, 'STD0090', 'F2', '2016-07-18', 10000, 0),
(66, 'STD0090', 'F3', '2016-07-18', 20000, 0),
(67, 'STD0090', 'F4', '2016-07-18', 0, 0),
(68, 'STD0090', 'F5', '2016-07-18', 0, 0),
(69, 'STD0090', 'F6', '2016-07-18', 0, 0),
(70, 'STD0090', 'F7', '2016-07-18', 0, 0),
(71, 'STD0091', 'F1', '2015-07-08', 20000, 0),
(72, 'STD0091', 'F2', '2015-07-08', 10000, 0),
(73, 'STD0091', 'F3', '2015-07-08', 20000, 0),
(74, 'STD0091', 'F4', '2015-07-08', 15000, 0),
(75, 'STD0091', 'F5', '2015-07-08', 5000, 0),
(76, 'STD0091', 'F6', '2015-07-08', 3000, 0),
(77, 'STD0091', 'F7', '2015-07-08', 10000, 0),
(78, 'STD0091', 'F8', '2015-07-08', 10000, 0),
(79, 'STD0091', 'F9', '2015-07-08', 25000, 0),
(80, 'STD0092', 'F1', '2015-07-08', 20000, 0),
(81, 'STD0092', 'F2', '2015-07-08', 10000, 0),
(82, 'STD0092', 'F3', '2015-07-08', 20000, 0),
(83, 'STD0092', 'F4', '2015-07-08', 15000, 0),
(84, 'STD0092', 'F5', '2015-07-08', 5000, 0),
(85, 'STD0092', 'F6', '2015-07-08', 3000, 0),
(86, 'STD0092', 'F7', '2015-07-08', 10000, 0),
(87, 'STD0092', 'F8', '2015-07-08', 10000, 0),
(88, 'STD0092', 'F9', '2015-07-08', 25000, 0),
(89, 'STD0093', 'F1', '2015-07-08', 20000, 0),
(90, 'STD0093', 'F2', '2015-07-08', 10000, 0),
(91, 'STD0093', 'F3', '2015-07-08', 20000, 0),
(92, 'STD0093', 'F4', '2015-07-08', 15000, 0),
(93, 'STD0093', 'F5', '2015-07-08', 5000, 0),
(94, 'STD0093', 'F6', '2015-07-08', 3000, 0),
(95, 'STD0093', 'F7', '2015-07-08', 0, 0),
(96, 'STD0093', 'F8', '2015-07-08', 0, 0),
(97, 'STD0093', 'F9', '2015-07-08', 0, 0),
(98, 'STD0094', 'F1', '2015-07-09', 20000, 0),
(99, 'STD0094', 'F2', '2015-07-09', 10000, 0),
(100, 'STD0094', 'F3', '2015-07-09', 20000, 0),
(101, 'STD0094', 'F4', '2015-07-09', 15000, 0),
(102, 'STD0094', 'F5', '2015-07-09', 5000, 0),
(103, 'STD0094', 'F6', '2015-07-09', 3000, 0),
(104, 'STD0094', 'F7', '2015-07-09', 10000, 0),
(105, 'STD0094', 'F8', '2015-07-09', 10000, 0),
(106, 'STD0094', 'F9', '2015-07-09', 25000, 0),
(107, 'STD0095', 'F1', '2015-07-09', 20000, 0),
(108, 'STD0095', 'F2', '2015-07-09', 10000, 0),
(109, 'STD0095', 'F3', '2015-07-09', 20000, 0),
(110, 'STD0095', 'F4', '2015-07-09', 15000, 0),
(111, 'STD0095', 'F5', '2015-07-09', 5000, 0),
(112, 'STD0095', 'F6', '2015-07-09', 3000, 0),
(113, 'STD0095', 'F7', '2015-07-09', 10000, 0),
(114, 'STD0095', 'F8', '2015-07-09', 10000, 0),
(115, 'STD0095', 'F9', '2015-07-09', 25000, 0),
(116, 'STD0096', 'F1', '2015-07-09', 20000, 0),
(117, 'STD0096', 'F2', '2015-07-09', 10000, 0),
(118, 'STD0096', 'F3', '2015-07-09', 20000, 0),
(119, 'STD0096', 'F4', '2015-07-09', 15000, 0),
(120, 'STD0096', 'F5', '2015-07-09', 5000, 0),
(121, 'STD0096', 'F6', '2015-07-09', 3000, 0),
(122, 'STD0096', 'F7', '2015-07-09', 10000, 0),
(123, 'STD0096', 'F8', '2015-07-09', 10000, 0),
(124, 'STD0096', 'F9', '2015-07-09', 25000, 0),
(125, 'STD0097', 'F1', '2015-07-09', 20000, 0),
(126, 'STD0097', 'F2', '2015-07-09', 10000, 0),
(127, 'STD0097', 'F3', '2015-07-09', 0, 0),
(128, 'STD0097', 'F4', '2015-07-09', 0, 0),
(129, 'STD0097', 'F5', '2015-07-09', 0, 0),
(130, 'STD0097', 'F6', '2015-07-09', 0, 0),
(131, 'STD0097', 'F7', '2015-07-09', 0, 0),
(132, 'STD0097', 'F8', '2015-07-09', 10000, 0),
(133, 'STD0097', 'F9', '2015-07-09', 25000, 0),
(134, 'STD0098', 'F1', '2015-07-09', 20000, 0),
(135, 'STD0098', 'F2', '2015-07-09', 10000, 0),
(136, 'STD0098', 'F3', '2015-07-09', 20000, 0),
(137, 'STD0098', 'F4', '2015-07-09', 15000, 0),
(138, 'STD0098', 'F5', '2015-07-09', 5000, 0),
(139, 'STD0098', 'F6', '2015-07-09', 3000, 0),
(140, 'STD0098', 'F7', '2015-07-09', 10000, 0),
(141, 'STD0098', 'F8', '2015-07-09', 10000, 0),
(142, 'STD0098', 'F9', '2015-07-09', 25000, 0),
(143, 'STD0099', 'F1', '2015-07-09', 20000, 0),
(144, 'STD0099', 'F2', '2015-07-09', 10000, 0),
(145, 'STD0099', 'F3', '2015-07-09', 20000, 0),
(146, 'STD0099', 'F4', '2015-07-09', 15000, 0),
(147, 'STD0099', 'F5', '2015-07-09', 5000, 0),
(148, 'STD0099', 'F6', '2015-07-09', 0, 0),
(149, 'STD0099', 'F7', '2015-07-09', 0, 0),
(150, 'STD0099', 'F8', '2015-07-09', 10000, 0),
(151, 'STD0099', 'F9', '2015-07-09', 25000, 0),
(152, 'STD0100', 'F1', '2015-07-09', 20000, 0),
(153, 'STD0100', 'F2', '2015-07-09', 10000, 0),
(154, 'STD0100', 'F3', '2015-07-09', 20000, 0),
(155, 'STD0100', 'F4', '2015-07-09', 15000, 0),
(156, 'STD0100', 'F5', '2015-07-09', 0, 0),
(157, 'STD0100', 'F6', '2015-07-09', 0, 0),
(158, 'STD0100', 'F7', '2015-07-09', 0, 0),
(159, 'STD0100', 'F8', '2015-07-09', 10000, 0),
(160, 'STD0100', 'F9', '2015-07-09', 25000, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `filtered_teaching_assignment`
-- (See below for the actual view)
--
CREATE TABLE `filtered_teaching_assignment` (
`assignment_id` int(11)
,`teacher_id` varchar(10)
,`subject_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`term_id` int(11)
,`filter_flag` tinyint(1)
,`dept_id` int(3) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `first_join_staff_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `first_join_staff_attendance_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`date_` date
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` enum('ADD','UPDATE','REMOVE')
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `group_type`
--

CREATE TABLE `group_type` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_type`
--

INSERT INTO `group_type` (`group_id`, `group_name`, `description`) VALUES
(1, 'Walimu Wa Darasa', 'Wasimamizi wa shughuli zote za darasa lake'),
(2, 'HoD', 'Head of Department');

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE `guardian` (
  `admission_no` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `rel_type` enum('Father','Mother','Guardian') NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `p_box` int(5) NOT NULL,
  `p_region` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guardian`
--

INSERT INTO `guardian` (`admission_no`, `firstname`, `middlename`, `lastname`, `gender`, `rel_type`, `email`, `occupation`, `phone_no`, `p_box`, `p_region`) VALUES
('0987654', 'Julias', 'Mathias', 'Msukuma', 'Male', 'Father', '', 'Mkulima', '+255789300400', 34, 'Geita'),
('1000120', 'Patrick', '', 'Lazaro', 'Male', 'Father', 'patrick@yahoo.com', 'Farmer', '+255765234500', 56745, 'Katavi'),
('1000121', 'Mathayo', '', 'Anthony', 'Male', 'Father', 'patrick11@yahoo.com', 'Farmer', '+255765234511', 56745, 'Arusha'),
('1000122', 'Anaset', '', 'Clement', 'Male', 'Father', 'anaset@yahoo.com', 'Farmer', '+255765274523', 3429, 'Dodoma'),
('1000123', 'Kapanga', '', 'Mayawa', 'Male', 'Father', '', 'Bussnessman', '+255765237902', 34291, 'Dodoma'),
('1000124', 'Abbakari', '', 'Twaha', 'Male', 'Father', '', 'Bussnessman', '+255765234999', 34123, 'Mwanza'),
('1000125', 'Abbakari', '', 'Zuberi', 'Male', 'Guardian', '', 'Farmer', '+255765234441', 65789, 'Morogoro'),
('1000126', 'Zahdin', '', 'Yunusu', 'Male', 'Guardian', '', 'Farmer', '+255765239876', 65767, 'Mbeya'),
('1000127', 'Swarehe', '', 'Mdhawala', 'Male', 'Guardian', '', 'Farmer', '+255765278674', 45328, 'Kigoma'),
('1031456', 'Juma', 'Umaa', 'Tommy', 'Male', 'Father', '', 'Peasant', '+255745234567', 213, 'Shinyanga'),
('1234567', 'Edwin', 'James', 'Mwaiaya', 'Male', 'Father', '', 'Teacher', '+255717909808', 234, 'Katavi'),
('1563242', 'Matatizo', 'Shida', 'Sikujua', 'Male', 'Father', '', 'Peasant', '+255765234669', 217, 'Mwanza'),
('1647366', 'Mwakifwamba', 'Mwaipopo', 'Mwakapumbu', 'Male', 'Father', '', 'Peasant', '+255765098789', 227, 'Mbeya'),
('4352111', 'Ghasia', '', 'Mohammed', 'Male', 'Guardian', '', 'Teacher', '+255781232145', 32, 'Mara'),
('5555555', 'Kivuyo', 'Esso', 'Mchungaji', 'Male', 'Guardian', '', 'Mnyama', '+255784858143', 87, 'Manyara'),
('6754678', 'opo', '', 'popo', 'Male', 'Guardian', '', 'mkulima', '+255717237889', 34, 'Ruvuma'),
('7656777', 'Eso', 'Shamande', 'Nailon', 'Male', 'Father', '', 'farmer', '+255787654343', 503, 'Dar-es-salaam');

-- --------------------------------------------------------

--
-- Table structure for table `hod_history`
--

CREATE TABLE `hod_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `dept_id` int(3) UNSIGNED NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hod_history`
--

INSERT INTO `hod_history` (`id`, `dept_id`, `staff_id`, `start_date`, `end_date`) VALUES
(17, 21, 'STAFF2222', '2018-06-01', '2018-06-01'),
(18, 21, 'STAFF0001', '2018-06-01', '2018-06-01'),
(19, 18, 'STAFF0001', '2018-06-01', '2018-06-01'),
(20, 19, 'STAFF3333', '2018-06-01', '2018-06-01'),
(21, 20, 'STAFF2020', '2018-06-01', NULL),
(22, 21, 'STAFF2222', '2018-06-01', NULL),
(23, 22, 'STAFF0035', '2018-06-01', '2018-06-01'),
(24, 23, 'STAFF3211', '2018-06-01', '2018-06-01'),
(25, 25, 'STAFF1111', '2018-06-01', '2018-06-01'),
(26, 19, 'STAFF3211', '2018-06-01', '2018-06-01'),
(27, 22, 'STAFF0001', '2018-06-01', '2018-06-01'),
(28, 18, 'STAFF1111', '2018-06-01', '2018-06-01'),
(29, 23, 'STAFF0035', '2018-06-01', '2018-06-01'),
(30, 25, 'STAFF3333', '2018-06-01', '2018-06-01'),
(31, 25, 'STAFF1111', '2018-06-01', '2018-06-05'),
(32, 18, 'STAFF0001', '2018-06-01', '2018-06-05'),
(33, 22, 'STAFF0035', '2018-06-01', NULL),
(34, 23, 'STAFF3211', '2018-06-01', NULL),
(35, 19, 'STAFF3333', '2018-06-01', NULL),
(36, 18, 'STAFF1111', '2018-06-05', '2018-06-05'),
(37, 18, 'STAFF0001', '2018-06-05', '2018-06-05'),
(38, 18, 'STAFF1111', '2018-06-05', '2018-06-05'),
(39, 18, 'STAFF0001', '2018-06-05', '2018-06-06'),
(40, 18, 'STAFF1111', '2018-06-06', '2018-06-06'),
(41, 18, 'STAFF0001', '2018-06-06', '2018-06-06'),
(42, 18, 'STAFF0001', '2018-06-06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inactive_staff`
--

CREATE TABLE `inactive_staff` (
  `staff_id` varchar(10) NOT NULL,
  `deactivated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inactive_staff`
--

INSERT INTO `inactive_staff` (`staff_id`, `deactivated_date`, `reason`) VALUES
('STAFF0035', '2018-06-05 12:26:21', 'Maternity Leave');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `s_no` int(11) NOT NULL,
  `source` varchar(50) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `received_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE `librarian` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `librarian`
--

INSERT INTO `librarian` (`staff_id`) VALUES
('STAFF0339');

-- --------------------------------------------------------

--
-- Table structure for table `lock_account_tbl`
--

CREATE TABLE `lock_account_tbl` (
  `username` varchar(13) NOT NULL,
  `account_locked` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `count_failed_attempt` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `lost_book_view`
-- (See below for the actual view)
--
CREATE TABLE `lost_book_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`LAST_EDIT` date
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `module_id` int(3) UNSIGNED NOT NULL,
  `module_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `module_name`) VALUES
(1, 'Admission'),
(2, 'Manage Finance'),
(3, 'Staff Management'),
(4, 'Student Management'),
(5, 'Library Management'),
(6, 'Accomodation'),
(7, 'Administration'),
(8, 'Exams'),
(9, 'Reports'),
(10, 'Inventory'),
(11, 'Department Management'),
(12, 'Class Management'),
(13, 'Timetable'),
(14, 'Announcements'),
(15, 'My Assigned Classes'),
(16, 'Completed Students'),
(17, 'TOD Management'),
(18, 'Discipline Management'),
(19, 'My Account'),
(20, 'My Department'),
(21, 'My Dormitory'),
(22, 'My Class'),
(23, 'TOD'),
(24, 'Results'),
(25, 'Role'),
(26, 'Class Results'),
(27, 'Subject Management'),
(28, 'Academic Year'),
(29, 'Admin Settings'),
(30, 'Notifications');

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `month_id` varchar(2) NOT NULL,
  `month_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`month_id`, `month_name`) VALUES
('01', 'January'),
('02', 'February'),
('03', 'March'),
('04', 'April'),
('05', 'May'),
('06', 'June'),
('07', 'July'),
('08', 'August'),
('09', 'September'),
('10', 'October'),
('11', 'November'),
('12', 'December');

-- --------------------------------------------------------

--
-- Stand-in structure for view `my_announcements_view`
-- (See below for the actual view)
--
CREATE TABLE `my_announcements_view` (
`gid` int(11)
,`announcement_id` int(11)
,`group_id` int(11)
,`heading` varchar(255)
,`msg` longtext
,`time` timestamp
,`posted_by` varchar(10)
,`status` varchar(4)
,`staff_id` varchar(10)
,`is_read` varchar(6)
,`username` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `msg_id` int(2) NOT NULL,
  `msg` mediumtext NOT NULL,
  `time_` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `notification_status` enum('read','unread') DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`notification_id`, `staff_id`, `msg_id`, `msg`, `time_`, `notification_status`) VALUES
(342, 'STAFF0001', 5, 'You have been assigned as dormitory master of: MANDELA', '2018-06-06 16:41:38', 'read'),
(407, 'STAFF1111', 3, 'You have been assigned as class teacher of: PHYSICS', '2018-06-06 16:06:15', 'unread'),
(409, 'STAFF0001', 3, 'You have been assigned as HOD of: PHYSICS', '2018-06-06 16:41:35', 'read'),
(428, 'STAFF0001', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(429, 'STAFF0003', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(430, 'STAFF0004', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(431, 'STAFF0035', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(432, 'STAFF1111', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(433, 'STAFF2020', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(434, 'STAFF2222', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(435, 'STAFF3211', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread'),
(436, 'STAFF3333', 9, 'Current exams: End of term Examinations', '2018-06-07 08:52:01', 'unread');

-- --------------------------------------------------------

--
-- Stand-in structure for view `out_book`
-- (See below for the actual view)
--
CREATE TABLE `out_book` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`DATE_DUE_BACK` date
,`DATE_OVERDUE` date
,`STATUS_BEFORE` enum('good','bad')
,`STATUS_AFTER` enum('good','bad')
,`BORROWER_ID` varchar(10)
,`BORROWER_TYPE` varchar(100)
,`LAST_EDIT` date
,`FLAG` enum('borrowed','lost','returned')
);

-- --------------------------------------------------------

--
-- Table structure for table `o_level_grade_system_tbl`
--

CREATE TABLE `o_level_grade_system_tbl` (
  `id` int(3) NOT NULL,
  `start_mark` float(5,2) NOT NULL,
  `end_mark` float(5,2) NOT NULL,
  `grade` enum('A','B+','B','C','D','F') NOT NULL,
  `unit_point` int(2) UNSIGNED NOT NULL,
  `remarks` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o_level_grade_system_tbl`
--

INSERT INTO `o_level_grade_system_tbl` (`id`, `start_mark`, `end_mark`, `grade`, `unit_point`, `remarks`) VALUES
(11, 30.01, 45.00, 'D', 5, 'Pass'),
(12, 45.01, 60.00, 'C', 4, 'Credit'),
(13, 60.01, 70.00, 'B', 3, 'Good'),
(14, 70.01, 80.00, 'B+', 2, 'Very Good'),
(15, 80.01, 100.00, 'A', 1, 'Excellent'),
(16, 0.00, 30.00, 'F', 6, 'Fail');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tbl`
--

CREATE TABLE `password_reset_tbl` (
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `password_reset_tbl`
--

INSERT INTO `password_reset_tbl` (`password`) VALUES
('a96af5cb3ae3b0e034f1c597133307de25068490');

-- --------------------------------------------------------

--
-- Table structure for table `payee`
--

CREATE TABLE `payee` (
  `s_no` int(11) NOT NULL,
  `cheque_no` varchar(20) NOT NULL,
  `amount_paid` bigint(20) NOT NULL,
  `payee_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `approved` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE `period` (
  `period_no` int(2) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `duration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`period_no`, `start_time`, `end_time`, `duration`) VALUES
(1, '08:00:00', '08:40:00', 40),
(2, '08:40:00', '09:20:00', 40),
(3, '09:20:00', '10:00:00', 40),
(4, '10:00:00', '10:40:00', 40),
(5, '10:40:00', '11:20:00', 40),
(6, '11:40:00', '12:20:00', 40),
(7, '12:20:00', '13:00:00', 40),
(8, '13:00:00', '13:20:00', 20),
(9, '13:40:00', '14:20:00', 40),
(10, '14:20:00', '15:00:00', 40);

--
-- Triggers `period`
--
DELIMITER $$
CREATE TRIGGER `before_insert_on_period` BEFORE INSERT ON `period` FOR EACH ROW BEGIN DECLARE min_start_time time; DECLARE max_end_time time; SELECT MIN(start_time) FROM period INTO min_start_time; SELECT MAX(end_time) FROM period INTO max_end_time; IF (min_start_time <= NEW.start_time && max_end_time >= NEW.start_time) THEN SIGNAL SQLSTATE '45000' SET message_text="Invalid input, the time you are trying to enter overlaps with the available period"; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(5) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `common_to` enum('class_teachers','hods','dormitory_masters','all','admin','other') NOT NULL DEFAULT 'other',
  `module_id` int(3) UNSIGNED NOT NULL,
  `permission_type` enum('root_child_node','root_node','child_node') NOT NULL,
  `root_node_id` int(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `description`, `common_to`, `module_id`, `permission_type`, `root_node_id`) VALUES
(1, 'Manage Staff', 'other', 3, 'child_node', 75),
(3, 'Activate-Deactivate Staff Account', 'admin', 3, 'child_node', 175),
(4, 'Register Student', 'other', 4, 'child_node', 5),
(5, 'View Registered Students', 'other', 4, 'root_node', 5),
(6, 'Promote Students', 'other', 4, 'root_node', 6),
(11, 'Manage Department', 'other', 11, 'child_node', 17),
(12, 'View Subject Teaching Logs', 'other', 11, 'child_node', 17),
(13, 'Manage Teaching Assignments', 'other', 11, 'child_node', 17),
(15, 'Add HOD', 'other', 11, 'child_node', 17),
(17, 'View Departments', 'other', 11, 'root_node', 17),
(18, 'Add Academic Year', 'admin', 28, 'child_node', 19),
(19, 'View Academic Years', 'other', 28, 'root_node', 19),
(20, 'Update Academic Year', 'admin', 28, 'child_node', 19),
(21, 'Add New Term', 'admin', 28, 'child_node', 19),
(22, 'Update Term', 'admin', 28, 'child_node', 19),
(23, 'Set Current Term', 'admin', 28, 'child_node', 19),
(24, 'View TODs', 'other', 17, 'root_node', 24),
(25, 'Manage TODs', 'other', 17, 'child_node', 24),
(27, 'Manage Dormitory', 'other', 6, 'child_node', 43),
(30, 'Manage O\'Level Grade System', 'other', 8, 'child_node', 34),
(31, 'Manage A\'Level Grade System', 'other', 8, 'child_node', 35),
(34, 'View O\'Level Grade System', 'other', 8, 'root_node', 34),
(35, 'View A\'Level Grade System', 'other', 8, 'root_node', 35),
(36, 'Manage Subjects', 'other', 27, 'child_node', 39),
(39, 'View Subjects', 'other', 27, 'root_node', 39),
(40, 'View Class Stream Subjects', 'other', 27, 'root_node', 40),
(42, 'View Own Department Staffs', 'hods', 20, 'root_node', 42),
(43, 'View Dormitories', 'other', 6, 'root_node', 43),
(44, 'Manage Dormitory Assets', 'other', 6, 'child_node', 135),
(45, 'Manage Class', 'other', 12, 'root_node', 45),
(48, 'View TOD Report', 'other', 17, 'root_node', 48),
(49, 'Manage TOD Reports', 'other', 17, 'child_node', 48),
(50, 'View Classes', 'other', 12, 'root_node', 50),
(52, 'Remove Stream', 'other', 12, 'child_node', 55),
(54, 'Update Stream', 'other', 12, 'child_node', 55),
(55, 'View Streams', 'other', 12, 'root_node', 55),
(58, 'View Exam Types', 'other', 8, 'root_node', 58),
(59, 'Update Exam Type', 'other', 8, 'child_node', 58),
(61, 'Enable/Disable Exam Types', 'other', 8, 'child_node', 58),
(62, 'Change Own Password', 'all', 19, 'root_node', 62),
(63, 'View Class Attendance', 'other', 17, 'root_node', 63),
(64, 'View School Attendance', 'other', 7, 'root_node', 64),
(65, 'Manage Score & Results Of Assigned Subjects', 'all', 8, 'root_node', 65),
(66, 'Add Score Of Own Subject', 'all', 8, 'child_node', 65),
(67, 'Manage Selected Students', 'other', 4, 'child_node', 83),
(68, 'View Own Department Classes', 'hods', 20, 'root_node', 68),
(69, 'Assign Teacher To Subject', 'hods', 20, 'child_node', 100),
(70, 'Remove Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(71, 'Explore School Attendance', 'other', 7, 'child_node', 64),
(73, 'Manage Timetable', 'other', 13, 'child_node', 80),
(75, 'View Staffs', 'other', 3, 'root_node', 75),
(77, 'Add Own Qualification', 'all', 19, 'root_node', 77),
(78, 'View Own Profile', 'all', 19, 'root_node', 78),
(80, 'View School Timetable', 'other', 13, 'root_node', 80),
(81, 'Update Own Profile', 'all', 19, 'child_node', 78),
(82, 'Update Score Of Own Subject', 'all', 8, 'child_node', 65),
(83, 'View Selected Students', 'other', 4, 'root_node', 83),
(84, 'View Transferred IN Students', 'other', 4, 'root_node', 84),
(85, 'View Transferred OUT Students', 'other', 4, 'root_node', 85),
(86, 'View Suspended Students', 'other', 4, 'root_node', 86),
(88, 'View Enrolled Students', 'other', 4, 'root_node', 88),
(89, 'View Disabled Students', 'other', 4, 'root_node', 89),
(90, 'Suspend Student', 'other', 4, 'child_node', 5),
(91, 'View Class Streams', 'other', 12, 'root_node', 91),
(92, 'View Students In Dormitory', 'other', 6, 'root_child_node', 43),
(93, 'Transfer Student', 'other', 4, 'child_node', 5),
(95, 'Manage Groups', 'admin', 29, 'root_node', 95),
(99, 'View Staffs Details', 'other', 3, 'child_node', 75),
(100, 'View Assinged Teachers In Own Department', 'hods', 20, 'root_node', 100),
(101, 'Update Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(103, 'View Teaching Log In Own Department', 'hods', 20, 'child_node', 68),
(104, 'View Activity Log', 'admin', 29, 'root_node', 104),
(105, 'View Staff Activity Log', 'admin', 3, 'child_node', 99),
(106, 'View Audit Trail', 'admin', 29, 'root_node', 106),
(107, 'Edit Student Results', 'other', 16, 'child_node', 179),
(108, 'Add Completion Details', 'other', 16, 'child_node', 174),
(109, 'View Own Class Results', 'class_teachers', 24, 'root_node', 109),
(111, 'Add Class Attedance', 'class_teachers', 22, 'child_node', 112),
(112, 'View Own Class Attedance', 'class_teachers', 22, 'root_node', 112),
(113, 'View Students In Own Class', 'class_teachers', 22, 'root_node', 113),
(114, 'View Assigned Teachers In Own Class', 'class_teachers', 22, 'root_node', 114),
(115, 'View Own Class Timetable', 'class_teachers', 22, 'root_node', 115),
(116, 'Add Class Journal Record', 'class_teachers', 22, 'child_node', 118),
(117, 'Update Class Journal', 'class_teachers', 22, 'child_node', 118),
(118, 'View Own Class Journal Report', 'class_teachers', 22, 'root_node', 118),
(119, 'Add Class Monitor', 'class_teachers', 22, 'child_node', 113),
(120, 'View Students In Own Dormitory', 'dormitory_masters', 21, 'root_node', 120),
(121, 'Add Dormitory Leader', 'dormitory_masters', 21, 'child_node', 120),
(122, 'View Assets In Own Dormitory', 'dormitory_masters', 21, 'root_node', 122),
(125, 'Update Student', 'other', 4, 'child_node', 5),
(127, 'Manage Own Posted Announcements', 'all', 14, 'root_node', 127),
(128, 'View Announcements', 'all', 14, 'root_node', 128),
(131, 'View Own Roles', 'all', 25, 'root_node', 131),
(132, 'View Assigned Classes', 'all', 15, 'root_node', 132),
(133, 'Add Dormitory Master', 'other', 6, 'child_node', 43),
(134, 'View Roll Call', 'other', 6, 'child_node', 43),
(135, 'View Assets In Dormitory', 'other', 6, 'root_child_node', 43),
(137, 'Reset Password', 'admin', 3, 'root_node', 137),
(138, 'Manage Staffs Attendance', 'other', 3, 'child_node', 165),
(142, 'Manage Class Subjects', 'other', 27, 'child_node', 40),
(146, 'Add Roll Call', 'dormitory_masters', 21, 'child_node', 147),
(147, 'View Roll Call In Own Dormitory', 'dormitory_masters', 21, 'root_node', 147),
(150, 'Add Student To Dormitory', 'other', 6, 'child_node', 92),
(154, 'Manage Roles', 'admin', 25, 'root_node', 154),
(155, 'Manage Division', 'other', 8, 'child_node', 158),
(158, 'View Divisions', 'other', 8, 'root_node', 158),
(161, 'Restore Suspended Student', 'other', 4, 'child_node', 86),
(165, 'View Staff Attendance', 'other', 3, 'root_node', 165),
(169, 'Manage Staff Qualifications', 'other', 3, 'child_node', 99),
(170, 'View Students In Own Department', 'hods', 20, 'child_node', 68),
(172, 'View Personal Timetable', 'all', 13, 'root_node', 172),
(174, 'Manage Alumni', 'other', 16, 'root_node', 174),
(175, 'Manage Staff Accounts', 'admin', 3, 'root_node', 175),
(177, 'Manage Teaching Log', 'all', 15, 'child_node', 189),
(179, 'View School Results', 'other', 24, 'root_node', 179),
(180, 'Manage Indiscipline Records', 'other', 18, 'child_node', 181),
(181, 'View Indiscipline Records', 'other', 18, 'root_node', 181),
(182, 'View All Teachers', 'other', 7, 'root_node', 182),
(183, 'Manage Periods', 'other', 7, 'child_node', 184),
(184, 'View Periods', 'other', 7, 'root_node', 184),
(185, 'Perform Database Backup', 'all', 29, 'root_node', 185),
(186, 'Change Avatar', 'all', 19, 'child_node', 78),
(189, 'View Topics', 'other', 15, 'root_child_node', 132),
(190, 'Manage Topics', 'other', 15, 'child_node', 189),
(193, 'View Subtopics', 'other', 15, 'root_child_node', 189),
(194, 'Manage Subtopics', 'other', 15, 'child_node', 193),
(197, 'Assign Class Teacher', 'other', 12, 'child_node', 91),
(198, 'View Students In Class', 'other', 12, 'root_child_node', 91),
(199, 'View Notifications', 'all', 30, 'root_node', 199),
(200, 'Transfer Student To Another Dormitory', 'other', 6, 'child_node', 92),
(201, 'View Class Journal Report', 'other', 12, 'child_node', 91),
(202, 'Manage Disabled Students', 'other', 4, 'child_node', 89),
(203, 'Restore Transferred Students', 'other', 4, 'child_node', 85),
(205, 'Assign Option Subject', 'other', 4, 'child_node', 5),
(206, 'Shift Student To Another Stream', 'other', 12, 'child_node', 198),
(207, 'Add Subjects To Teacher', 'other', 7, 'child_node', 182),
(208, 'View Subjects Teachers', 'other', 27, 'child_node', 39),
(211, 'Upload Student\'s Photo', 'all', 4, 'child_node', 5),
(212, 'Manage Class Assets', 'other', 12, 'child_node', 91),
(215, 'View Assets In Class', 'other', 12, 'child_node', 91),
(216, 'View Assigned Teachers In Class', 'other', 12, 'child_node', 91),
(217, 'View Teacher Details', 'other', 7, 'child_node', 182),
(218, 'View Staff Role', 'admin', 7, 'child_node', 75);

-- --------------------------------------------------------

--
-- Table structure for table `permission_bk`
--

CREATE TABLE `permission_bk` (
  `permission_id` int(5) UNSIGNED NOT NULL DEFAULT 0,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `common_to` enum('class_teachers','hods','dormitory_masters','all','other') CHARACTER SET utf8 NOT NULL DEFAULT 'other',
  `module_id` int(3) UNSIGNED NOT NULL,
  `permission_type` enum('root_child_node','root_node','child_node') CHARACTER SET utf8 NOT NULL,
  `root_node_id` int(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_bk`
--

INSERT INTO `permission_bk` (`permission_id`, `description`, `common_to`, `module_id`, `permission_type`, `root_node_id`) VALUES
(1, 'Add New Staff', 'other', 3, 'child_node', 175),
(2, 'Update Staff', 'other', 3, 'child_node', 175),
(3, 'Activate Staff Account', 'other', 3, 'child_node', 175),
(4, 'Register Student', 'other', 4, 'child_node', 5),
(5, 'View Registered Students', 'other', 4, 'root_node', 5),
(6, 'Promote Students', 'other', 4, 'root_node', 6),
(11, 'Add New Department', 'other', 11, 'child_node', 17),
(12, 'Update Department', 'other', 11, 'child_node', 17),
(13, 'Remove Department', 'other', 11, 'child_node', 17),
(14, 'View Department Members', 'other', 11, 'child_node', 17),
(15, 'Add HOD', 'other', 11, 'child_node', 17),
(16, 'View Subjects Offered By Department', 'other', 11, 'child_node', 17),
(17, 'View Departments', 'other', 11, 'root_node', 17),
(18, 'Add Academic Year', 'other', 28, 'child_node', 19),
(19, 'View Academic Years', 'other', 28, 'root_node', 19),
(20, 'Update Academic Year', 'other', 28, 'child_node', 19),
(21, 'Add New Term', 'other', 28, 'child_node', 19),
(22, 'Update Term', 'other', 28, 'child_node', 19),
(23, 'Set Current Term', 'other', 28, 'child_node', 19),
(24, 'View TODs', 'other', 17, 'root_node', 24),
(25, 'Add New TOD', 'other', 17, 'child_node', 24),
(26, 'Update TOD', 'other', 17, 'child_node', 24),
(27, 'Add New Dormitory', 'other', 6, 'child_node', 43),
(28, 'Update Dormitory', 'other', 6, 'child_node', 43),
(29, 'Remove Dormitory', 'other', 6, 'child_node', 43),
(30, 'Add O\'Level Grade System', 'other', 8, 'child_node', 34),
(31, 'Add A\'Level Grade System', 'other', 8, 'child_node', 35),
(32, 'Update A\'Level Grade System', 'other', 8, 'child_node', 35),
(33, 'Update O\'Level Grade System', 'other', 8, 'child_node', 34),
(34, 'View O\'Level Grade System', 'other', 8, 'root_node', 34),
(35, 'View A\'Level Grade System', 'other', 8, 'root_node', 35),
(36, 'Add New Subject', 'other', 27, 'child_node', 39),
(37, 'Update Subject', 'other', 27, 'child_node', 39),
(38, 'Remove Subject', 'other', 27, 'child_node', 39),
(39, 'View Subjects', 'other', 27, 'root_node', 39),
(40, 'View Class Stream Subjects', 'other', 27, 'root_node', 40),
(41, 'Remove TOD', 'other', 17, 'child_node', 24),
(42, 'View Own Department Staffs', 'hods', 20, 'root_node', 42),
(43, 'View Dormitories', 'other', 6, 'root_node', 43),
(44, 'Add Asset In Dormitory', 'other', 6, 'child_node', 135),
(45, 'Add New Class', 'other', 12, 'root_node', 45),
(46, 'Update Class', 'other', 12, 'child_node', 50),
(47, 'Set Current TOD', 'other', 17, 'child_node', 24),
(48, 'View TOD Report', 'other', 17, 'root_child_node', 72),
(49, 'Add Comment TOD Report', 'other', 17, 'child_node', 48),
(50, 'View Classes', 'other', 12, 'root_node', 50),
(51, 'Remove Class', 'other', 12, 'child_node', 50),
(52, 'Remove Stream', 'other', 12, 'child_node', 55),
(53, 'Add New Stream', 'other', 12, 'child_node', 55),
(54, 'Update Stream', 'other', 12, 'child_node', 55),
(55, 'View Streams', 'other', 12, 'root_node', 55),
(56, 'Remove A\'Level Grade System', 'other', 8, 'child_node', 35),
(57, 'Remove O\'Level Grade System', 'other', 8, 'child_node', 34),
(58, 'View Exam Types', 'other', 8, 'root_node', 58),
(59, 'Update Exam Type', 'other', 8, 'child_node', 58),
(61, 'Enable/Disable Exam Types', 'other', 8, 'child_node', 58),
(62, 'Change Own Password', 'all', 19, 'root_node', 62),
(63, 'View Class Attendance', 'other', 17, 'root_node', 63),
(64, 'View School Attendance', 'other', 7, 'root_node', 64),
(65, 'Manage Score & Results Of Assigned Subjects', 'all', 8, 'root_node', 65),
(66, 'Add Score Of Own Subject', 'all', 8, 'child_node', 65),
(67, 'Add Selected Student', 'other', 4, 'child_node', 83),
(68, 'View Own Department Classes', 'hods', 20, 'root_node', 68),
(69, 'Assign Teacher To Subject', 'hods', 20, 'child_node', 100),
(70, 'Remove Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(71, 'Explore School Attendance', 'other', 7, 'child_node', 64),
(72, 'TOD Routine', 'other', 17, 'root_node', 72),
(73, 'Add New Timetable', 'other', 13, 'child_node', 80),
(74, 'Deactivate Staff Account', 'other', 3, 'child_node', 175),
(75, 'View Staffs', 'other', 3, 'root_node', 75),
(77, 'Add Own Qualification', 'all', 19, 'root_node', 77),
(78, 'View Own Profile', 'all', 19, 'root_node', 78),
(79, 'Update Timetable', 'other', 13, 'child_node', 80),
(80, 'View School Timetable', 'other', 13, 'root_node', 80),
(81, 'Update Own Profile', 'all', 19, 'child_node', 78),
(82, 'Update Score Of Own Subject', 'all', 8, 'child_node', 65),
(83, 'View Selected Students', 'other', 4, 'root_node', 83),
(84, 'View Transferred IN Students', 'other', 4, 'root_node', 84),
(85, 'View Transferred OUT Students', 'other', 4, 'root_node', 85),
(86, 'View Suspended Students', 'other', 4, 'root_node', 86),
(87, 'View Unreported Students', 'other', 4, 'root_node', 87),
(88, 'View Enrolled Students', 'other', 4, 'root_node', 88),
(89, 'View Disabled Students', 'other', 4, 'root_node', 89),
(90, 'Suspend Student', 'other', 4, 'child_node', 5),
(91, 'View Class Streams', 'other', 12, 'root_node', 91),
(92, 'View Students In Dormitory', 'other', 6, 'root_child_node', 43),
(93, 'Transfer Student', 'other', 4, 'child_node', 5),
(95, 'Manage Groups', 'other', 29, 'root_node', 95),
(99, 'View Staffs Details', 'other', 3, 'root_child_node', 175),
(100, 'View Assinged Teachers In Own Department', 'hods', 20, 'root_node', 100),
(101, 'Update Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(103, 'View Teaching Log In Own Department', 'hods', 20, 'child_node', 68),
(104, 'View Activity Log', 'other', 29, 'root_node', 104),
(105, 'View Staff Activity Log', 'other', 3, 'child_node', 99),
(106, 'View Audit Trail', 'other', 29, 'root_node', 106),
(107, 'Edit Student Results', 'other', 16, 'child_node', 179),
(108, 'Add Completion Details', 'other', 16, 'child_node', 174),
(109, 'View Own Class Results', 'class_teachers', 24, 'root_node', 109),
(111, 'Add Class Attedance', 'class_teachers', 22, 'child_node', 112),
(112, 'View Own Class Attedance', 'class_teachers', 22, 'root_node', 112),
(113, 'View Students In Own Class', 'class_teachers', 22, 'root_node', 113),
(114, 'View Assigned Teachers In Own Class', 'class_teachers', 22, 'root_node', 114),
(115, 'View Own Class Timetable', 'class_teachers', 22, 'root_node', 115),
(116, 'Add Class Journal Record', 'class_teachers', 22, 'child_node', 118),
(117, 'Update Class Journal', 'class_teachers', 22, 'child_node', 118),
(118, 'View Own Class Journal Report', 'class_teachers', 22, 'root_node', 118),
(119, 'Add Class Monitor', 'class_teachers', 22, 'child_node', 113),
(120, 'View Students In Own Dormitory', 'dormitory_masters', 21, 'root_node', 120),
(121, 'Add Dormitory Leader', 'dormitory_masters', 21, 'child_node', 120),
(122, 'View Assets In Own Dormitory', 'dormitory_masters', 21, 'root_node', 122),
(125, 'Update Student', 'other', 4, 'child_node', 5),
(126, 'Post New Announcement', 'all', 14, 'child_node', 127),
(127, 'View Own Posted Announcements', 'all', 14, 'root_node', 127),
(128, 'View Announcements', 'all', 14, 'root_node', 128),
(129, 'Update Announcement', 'all', 14, 'child_node', 127),
(131, 'View Own Roles', 'all', 25, 'root_node', 131),
(132, 'View Assigned Classes', 'all', 15, 'root_node', 132),
(133, 'Add Dormitory Master', 'other', 6, 'child_node', 43),
(134, 'View Roll Call', 'other', 6, 'child_node', 43),
(135, 'View Assets In Dormitory', 'other', 6, 'root_child_node', 43),
(137, 'Reset Password', 'other', 3, 'root_node', 137),
(138, 'Add Staff Attendance', 'other', 3, 'child_node', 165),
(139, 'Update Class Stream', 'other', 12, 'child_node', 91),
(140, 'Add New Class Stream', 'other', 12, 'child_node', 91),
(141, 'Remove Class Stream', 'other', 12, 'child_node', 91),
(142, 'Add Class Stream Subject', 'other', 27, 'child_node', 40),
(143, 'Update Class Stream Subject', 'other', 27, 'child_node', 40),
(144, 'Remove Class Stream Subject', 'other', 27, 'child_node', 40),
(146, 'Add Roll Call', 'dormitory_masters', 21, 'child_node', 147),
(147, 'View Roll Call In Own Dormitory', 'dormitory_masters', 21, 'root_node', 147),
(148, 'Remove Asset From Dormitory', 'other', 6, 'child_node', 135),
(149, 'Update Asset In Dormitory', 'other', 6, 'child_node', 135),
(150, 'Add Student To Dormitory', 'other', 6, 'child_node', 92),
(151, 'Edit Period', 'other', 7, 'child_node', 184),
(154, 'Manage Roles', 'other', 25, 'root_node', 154),
(155, 'Add Division', 'other', 8, 'child_node', 158),
(156, 'Update Division', 'other', 8, 'child_node', 158),
(157, 'Remove Division', 'other', 8, 'child_node', 158),
(158, 'View Divisions', 'other', 8, 'root_node', 158),
(161, 'Restore Suspended Student', 'other', 4, 'child_node', 86),
(162, 'View Student Profile', 'all', 4, '', 5),
(163, 'Mark Student As Reported', 'other', 4, 'child_node', 83),
(164, 'Mark Student As Unreported', 'other', 4, 'child_node', 83),
(165, 'View Staff Attendance', 'other', 3, 'root_node', 165),
(169, 'View Staff Qualification', 'other', 3, 'child_node', 99),
(170, 'View Students In Own Department', 'hods', 20, 'child_node', 68),
(172, 'View Personal Timetable', 'all', 13, 'root_node', 172),
(174, 'View All Completed Students', 'other', 16, 'root_node', 174),
(175, 'Manage Staff Accounts', 'other', 3, 'root_node', 175),
(176, 'View Own Log', 'all', 15, 'child_node', 189),
(177, 'Add Teaching Log', 'all', 15, 'child_node', 189),
(179, 'View School Results', 'other', 24, 'root_node', 179),
(180, 'Add Indiscipline Record', 'other', 18, 'child_node', 181),
(181, 'View Indiscipline Records', 'other', 18, 'root_node', 181),
(182, 'View All Teachers', 'other', 7, 'root_node', 182),
(183, 'Add New Period', 'other', 7, 'child_node', 184),
(184, 'View Periods', 'other', 7, 'root_node', 184),
(185, 'Perform Database Backup', 'all', 29, 'root_node', 185),
(186, 'Change Avatar', 'all', 19, 'child_node', 78),
(188, 'Erase Timetable', 'other', 13, 'child_node', 80),
(189, 'View Topics', 'other', 15, 'root_child_node', 132),
(190, 'Add Topic', 'other', 15, 'child_node', 189),
(191, 'Update Topic', 'other', 15, 'child_node', 189),
(192, 'Remove Topic', 'other', 15, 'child_node', 189),
(193, 'View Subtopics', 'other', 15, 'root_child_node', 189),
(194, 'Add Subtopic', 'other', 15, 'child_node', 193),
(195, 'Update Subtopic', 'other', 15, 'child_node', 193),
(196, 'Remove Subtopic', 'other', 15, 'child_node', 193),
(197, 'Assign Class Teacher', 'other', 12, 'child_node', 91),
(198, 'View Students In Class', 'other', 12, 'root_child_node', 91),
(199, 'View Notifications', 'all', 30, 'root_node', 199),
(200, 'Transfer Student To Another Dormitory', 'other', 6, 'child_node', 92),
(201, 'View Class Journal Report', 'other', 12, 'child_node', 91),
(202, 'Add Staff Qualification', 'other', 3, 'child_node', 99),
(205, 'Assign Option Subject', 'other', 4, 'child_node', 5),
(206, 'Shift Student To Another Stream', 'other', 12, 'child_node', 198),
(207, 'Add Subjects To Teacher', 'other', 7, 'child_node', 182),
(208, 'View Subjects Teachers', 'other', 27, 'child_node', 39),
(209, 'Edit Selected Students', 'other', 4, 'child_node', 83),
(210, 'Update Staff Attendance', 'all', 3, 'child_node', 165),
(211, 'Upload Student\'s Photo', 'all', 4, 'child_node', 162),
(212, 'Add Asset In Class', 'other', 12, 'child_node', 91),
(213, 'Update Asset In Class', 'other', 12, 'child_node', 91),
(214, 'Remove Asset From Class', 'other', 12, 'child_node', 91),
(215, 'View Assets In Class', 'other', 12, 'child_node', 91);

-- --------------------------------------------------------

--
-- Table structure for table `promote_student_audit_table`
--

CREATE TABLE `promote_student_audit_table` (
  `id` int(4) NOT NULL,
  `promotion_level` varchar(10) NOT NULL,
  `promoted_by` varchar(10) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `promote_student_audit_table`
--
DELIMITER $$
CREATE TRIGGER `promote_students_trigger` BEFORE INSERT ON `promote_student_audit_table` FOR EACH ROW BEGIN 
DECLARE role VARCHAR(20); 
SELECT user_role FROM admin WHERE id=1 INTO role; 
IF(role = "admin") THEN 
CALL promote_class_stream_id_procedure_new(NEW.promotion_level); 
CALL promote_class_id_procedure_new(NEW.promotion_level);  
CALL promote_academic_year_procedure(NEW.promotion_level); 
CALL copy_to_students_enrollment_procedure(NEW.promotion_level); 
ELSE 
SIGNAL SQLSTATE '45000' set message_text = "You do not have the privilege to promote students"; 
END IF; 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `certification` varchar(30) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `university` varchar(100) NOT NULL,
  `completion_date` year(4) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `q_id` int(11) NOT NULL,
  `certified` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `region_id` int(2) NOT NULL,
  `region_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`region_id`, `region_name`) VALUES
(1, 'Arusha'),
(2, 'Dar-es-salaam'),
(3, 'Dodoma'),
(4, 'Geita'),
(5, 'Iringa'),
(6, 'Kagera'),
(7, 'Katavi'),
(8, 'Kigoma'),
(9, 'Kilimanjaro'),
(10, 'Manyara'),
(11, 'Mara'),
(12, 'Mbeya'),
(13, 'Morogoro'),
(14, 'Mtwara'),
(15, 'Mwanza'),
(16, 'Pwani'),
(17, 'Rukwa'),
(18, 'Ruvuma'),
(19, 'Shinyanga'),
(20, 'Simiyu'),
(21, 'Singida'),
(22, 'Tabora'),
(23, 'Tanga'),
(24, 'Tunduma');

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE `religion` (
  `religion_id` int(5) NOT NULL,
  `religion_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`religion_id`, `religion_name`) VALUES
(1, 'Christian'),
(2, 'Muslim');

-- --------------------------------------------------------

--
-- Stand-in structure for view `remained_book_view`
-- (See below for the actual view)
--
CREATE TABLE `remained_book_view` (
`isbn` varchar(17)
,`ID` varchar(50)
,`LAST_EDIT` date
,`STATUS` enum('normal','lost','bad')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `reminder`
-- (See below for the actual view)
--
CREATE TABLE `reminder` (
`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`ID` varchar(50)
,`ISBN` varchar(17)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `remove_borrowed_view`
-- (See below for the actual view)
--
CREATE TABLE `remove_borrowed_view` (
`ID` varchar(50)
,`ISBN` varchar(17)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `returned_book_view`
-- (See below for the actual view)
--
CREATE TABLE `returned_book_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`EXPECT_RETURN` date
,`RETURN_DATE` date
,`STATUS_AFTER` enum('good','bad')
,`BORROWER_ID` varchar(10)
,`BORROWER_DESCRIPTION` varchar(61)
,`BORROWER_TYPE` varchar(100)
,`FLAG` enum('borrowed','lost','returned')
);

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `role_type_id` int(2) NOT NULL,
  `module_id` int(3) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`role_type_id`, `module_id`) VALUES
(3, 20),
(3, 30),
(4, 13),
(4, 14),
(4, 19),
(4, 22),
(4, 24),
(5, 21),
(19, 3),
(19, 4),
(19, 7),
(19, 8),
(19, 11),
(19, 12),
(19, 13),
(19, 14),
(19, 15),
(19, 17),
(19, 19),
(19, 30),
(20, 3),
(20, 4),
(20, 8),
(20, 12),
(20, 13),
(20, 14),
(20, 15),
(20, 19),
(20, 25);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_type_id` int(2) NOT NULL,
  `permission_id` int(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_type_id`, `permission_id`) VALUES
(3, 42),
(3, 199),
(4, 78),
(4, 81),
(4, 109),
(4, 111),
(4, 112),
(4, 113),
(4, 114),
(4, 115),
(4, 116),
(4, 117),
(4, 118),
(4, 119),
(4, 128),
(4, 172),
(4, 186),
(5, 120),
(5, 121),
(5, 122),
(5, 146),
(5, 147),
(19, 1),
(19, 4),
(19, 5),
(19, 11),
(19, 12),
(19, 13),
(19, 15),
(19, 17),
(19, 30),
(19, 31),
(19, 34),
(19, 35),
(19, 45),
(19, 50),
(19, 52),
(19, 54),
(19, 55),
(19, 58),
(19, 59),
(19, 61),
(19, 62),
(19, 63),
(19, 64),
(19, 65),
(19, 66),
(19, 71),
(19, 73),
(19, 75),
(19, 77),
(19, 78),
(19, 80),
(19, 81),
(19, 82),
(19, 83),
(19, 84),
(19, 85),
(19, 86),
(19, 88),
(19, 89),
(19, 91),
(19, 99),
(19, 127),
(19, 128),
(19, 132),
(19, 155),
(19, 158),
(19, 182),
(19, 184),
(19, 189),
(19, 190),
(19, 193),
(19, 194),
(19, 197),
(19, 198),
(19, 199),
(19, 201),
(19, 205),
(19, 207),
(19, 211),
(20, 1),
(20, 4),
(20, 5),
(20, 50),
(20, 55),
(20, 62),
(20, 65),
(20, 66),
(20, 67),
(20, 75),
(20, 77),
(20, 78),
(20, 81),
(20, 83),
(20, 91),
(20, 127),
(20, 128),
(20, 131),
(20, 132),
(20, 172),
(20, 177),
(20, 186),
(20, 189),
(20, 198),
(20, 211);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission_dummy_table`
--

CREATE TABLE `role_permission_dummy_table` (
  `role_type_id` int(2) DEFAULT NULL,
  `permission_id` int(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission_dummy_table`
--

INSERT INTO `role_permission_dummy_table` (`role_type_id`, `permission_id`) VALUES
(9, 64),
(9, 71),
(5, 120),
(5, 121),
(5, 122),
(5, 146),
(5, 147),
(3, 199),
(3, 42),
(4, 172),
(4, 128),
(4, 78),
(4, 81),
(4, 186),
(4, 111),
(4, 112),
(4, 113),
(4, 114),
(4, 115),
(4, 116),
(4, 117),
(4, 118),
(4, 119),
(4, 109),
(19, 211),
(19, 65),
(19, 82),
(19, 66),
(19, 127),
(19, 128),
(19, 132),
(19, 77),
(19, 78),
(19, 81),
(19, 62),
(19, 199),
(19, 1),
(19, 75),
(19, 99),
(19, 205),
(19, 89),
(19, 88),
(19, 86),
(19, 85),
(19, 84),
(19, 83),
(19, 4),
(19, 5),
(19, 207),
(19, 184),
(19, 182),
(19, 64),
(19, 71),
(19, 158),
(19, 61),
(19, 59),
(19, 155),
(19, 30),
(19, 31),
(19, 34),
(19, 35),
(19, 58),
(19, 15),
(19, 11),
(19, 13),
(19, 17),
(19, 12),
(19, 198),
(19, 55),
(19, 54),
(19, 197),
(19, 45),
(19, 52),
(19, 201),
(19, 50),
(19, 91),
(19, 80),
(19, 73),
(19, 189),
(19, 190),
(19, 193),
(19, 194),
(19, 63),
(20, 211),
(20, 66),
(20, 172),
(20, 127),
(20, 128),
(20, 132),
(20, 177),
(20, 77),
(20, 78),
(20, 81),
(20, 186),
(20, 62),
(20, 131),
(20, 1),
(20, 75),
(20, 83),
(20, 4),
(20, 67),
(20, 5),
(20, 198),
(20, 55),
(20, 50);

--
-- Triggers `role_permission_dummy_table`
--
DELIMITER $$
CREATE TRIGGER `before_insert_in_role_permission_trigger` BEFORE INSERT ON `role_permission_dummy_table` FOR EACH ROW BEGIN 
DECLARE rnid INT DEFAULT 0;
DECLARE p_type VARCHAR(30);
DECLARE rnid2 INT DEFAULT 0;

SELECT root_node_id FROM permission WHERE permission_id=NEW.permission_id INTO rnid; 
SELECT permission_type FROM permission WHERE permission_id=NEW.permission_id INTO p_type;

IF p_type = "child_node" THEN 
REPLACE INTO role_permission VALUES(NEW.role_type_id, NEW.permission_id);
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid);

SELECT root_node_id FROM permission WHERE permission_id=rnid INTO rnid2; 
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid2);
ELSE
REPLACE INTO role_permission VALUES(NEW.role_type_id, NEW.permission_id);
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid);
END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role_type`
--

CREATE TABLE `role_type` (
  `role_type_id` int(2) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  `description` mediumtext NOT NULL,
  `active` enum('on','off') DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_type`
--

INSERT INTO `role_type` (`role_type_id`, `role_name`, `description`, `active`) VALUES
(3, 'Head Of Department', 'Responsible for department', 'on'),
(4, 'Class Teacher', 'Responsible for class', 'on'),
(5, 'Dormitory Master', 'Responsible for dormitory', 'on'),
(9, 'Teacher On Duty', 'Resposible for all activities this week', 'on'),
(19, 'Academic Master', 'Responsible for all Academic issues', 'on'),
(20, 'Registrar', 'Registering Students', 'on'),
(21, 'Teacher', 'Responsible for teaching', 'on');

--
-- Triggers `role_type`
--
DELIMITER $$
CREATE TRIGGER `before_delete_role_type` BEFORE DELETE ON `role_type` FOR EACH ROW BEGIN IF (OLD.role_type_id = 3 || OLD.role_type_id = 4 || OLD.role_type_id = 5 || OLD.role_type_id = 9) THEN SIGNAL SQLSTATE '45000' SET message_text="Permission Denied. Cannot delete this role"; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `secretary`
--

CREATE TABLE `secretary` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `secretary`
--

INSERT INTO `secretary` (`staff_id`) VALUES
('STAFF0987');

-- --------------------------------------------------------

--
-- Table structure for table `selected_students`
--

CREATE TABLE `selected_students` (
  `examination_no` varchar(20) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date NOT NULL,
  `former_school` varchar(255) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `ward` varchar(30) NOT NULL,
  `orphan` enum('yes','no') DEFAULT NULL,
  `g_firstname` varchar(30) DEFAULT NULL,
  `g_lastname` varchar(30) DEFAULT NULL,
  `disability` enum('yes','no') NOT NULL DEFAULT 'no',
  `region` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `tel_no` varchar(15) NOT NULL,
  `box` varchar(10) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `form` enum('I','V') NOT NULL,
  `reported` enum('yes','no') NOT NULL DEFAULT 'no',
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selected_students`
--

INSERT INTO `selected_students` (`examination_no`, `firstname`, `lastname`, `dob`, `former_school`, `gender`, `nationality`, `ward`, `orphan`, `g_firstname`, `g_lastname`, `disability`, `region`, `district`, `tel_no`, `box`, `email`, `form`, `reported`, `academic_year`) VALUES
('S123-123', 'Haruna', 'Bakaru', '2018-05-08', 'Nkurumahia Secondary School', 'Male', 'Tanzanian', 'Ilemela', 'no', 'Grace', 'Makei', 'no', 'Kilimanjaro', 'Ilemela', '+255717808090', '123', '', 'V', 'no', 3);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date NOT NULL,
  `marital_status` enum('Married','Single','Divorced','Widowed') NOT NULL,
  `gender` varchar(6) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone_no` varchar(13) NOT NULL,
  `staff_image` varchar(255) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `staff_type` char(1) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `user_role` varchar(30) NOT NULL,
  `last_log` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `b_type` enum('STAFF') DEFAULT 'STAFF'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `firstname`, `middlename`, `lastname`, `dob`, `marital_status`, `gender`, `email`, `phone_no`, `staff_image`, `password`, `staff_type`, `status`, `user_role`, `last_log`, `b_type`) VALUES
('STAFF0001', 'Andrew', '', 'Lyagunga', '1982-06-16', 'Single', 'Male', 'andrea@yahoo.com', '+255717112233', 'http://192.168.100.5/assets/images/andrew_lyagunga.jpg', '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-06 16:14:30', 'STAFF'),
('STAFF0003', 'JOVINARY', 'MATHAYO', 'BAYASABE', '1973-10-07', 'Married', 'Male', '', '+255784858140', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-05 12:09:07', 'STAFF'),
('STAFF0004', 'MUSSA', '', 'MUSEGETE', '1987-05-04', 'Single', 'Male', '', '+255784858143', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-05 08:54:00', 'STAFF'),
('STAFF0013', 'DAUDI', 'GABRIEL', 'LUMAZI', '1969-12-31', 'Married', 'Male', '', '+255767469261', NULL, 'f8fa7e31eb49935123b97e31f715bd1b0c36b41f', 'T', 'active', 'teacher', '2018-07-11 08:13:51', 'STAFF'),
('STAFF0014', 'IRENE', 'EVANCE', 'MBISHI', '1989-12-27', 'Married', 'Male', 'irenevance89@gmail.com', '+255752613321', NULL, '0af4d2eea5d26a6d0638356b232ae13a6cb60d0a', 'T', 'active', 'teacher', '2018-07-11 08:35:41', 'STAFF'),
('STAFF0015', 'SOSPETER', 'MARTINE', 'LUSAYA', '1981-06-16', 'Married', 'Male', 'martinesospeter70@gmail.com', '+255753225928', NULL, '4e2c71561e889ba1a39fca957f65e176a49c60ae', 'T', 'active', 'teacher', '2018-07-11 08:42:07', 'STAFF'),
('STAFF0016', 'ISMAIL', 'MJENGI', 'HAMIS', '1987-12-25', 'Married', 'Male', 'ismailhamisi4@gmail.com', '+255756539880', NULL, 'd33e6535cf3902ae77e02982558b79a213320401', 'T', 'active', 'teacher', '2018-07-11 08:50:55', 'STAFF'),
('STAFF0018', 'EMMANUEL', 'KINENEKE', 'MADAHA', '1973-02-11', 'Married', 'Male', 'kineneke@gmail.com', '+255787297657', NULL, 'a449a64d29470895e0621dfcd55df2a2d35c4b39', 'T', 'active', 'teacher', '2018-07-11 09:01:52', 'STAFF'),
('STAFF0019', 'HEARTMAN', 'MNEMA', 'MGANDA', '1987-10-17', 'Married', 'Male', 'heartmanmnema@yahoo.com', '+255754489670', NULL, 'c896f2bda2f7ec66be76cbdfed778eb8672263f5', 'T', 'active', 'teacher', '0000-00-00 00:00:00', 'STAFF'),
('STAFF0020', 'BAHATI', 'AMOS', 'JINASA', '1989-11-21', 'Married', 'Female', 'bahatiamos@yahoo.com', '+255764308624', NULL, '15f6af808f53338d64af40042d73cc9c635373f9', 'T', 'active', 'teacher', '2018-07-11 09:16:17', 'STAFF'),
('STAFF0021', 'ZAITUNI', '', 'IBRAHIM', '1984-07-02', 'Married', 'Female', 'mamatiphaa@gmail.com', '+255758300526', NULL, '57a8ba606f774cb1163f1ddf567090d36d86bc16', 'T', 'active', 'teacher', '2018-07-11 09:22:44', 'STAFF'),
('STAFF0022', 'YIBA', 'GISAYI', 'MADOSHI', '1965-11-18', 'Married', 'Male', 'yibamadoshi@yahoo.com', '+255752888932', NULL, '4c52bfe11257bc5f2c62a2e998b44128c4ed508e', 'T', 'active', 'teacher', '2018-07-11 09:28:47', 'STAFF'),
('STAFF0023', 'MARIAM', 'JUSTINE', 'LAWI', '1987-11-30', 'Married', 'Male', 'lawimariam@yahoo.com', '+255782018622', NULL, 'b1a3ca0e196d3ac04ba8d871725f92f31cbc69e7', 'T', 'active', 'teacher', '2018-07-11 09:57:41', 'STAFF'),
('STAFF0024', 'SIRAJI', 'SADICK', 'MUTTAZAA', '1964-06-06', 'Married', 'Male', 'sirajisadickmuttazaa19640@gmail.com', '+255765914607', NULL, '49c522afea48bd7109178b4d2f35d77b63b03c29', 'T', 'active', 'teacher', '2018-07-11 10:23:48', 'STAFF'),
('STAFF0026', 'Sophia', 'Gunje', 'Ndungile', '1986-08-15', 'Married', 'Female', 'ndungileeddy15@gmail.com', '+255768610136', NULL, 'a0d2ed6a98aabbc7c1bf1cb06f453c8dd7906e1c', 'T', 'active', 'teacher', '2018-07-11 10:45:29', 'STAFF'),
('STAFF0027', 'DEMITRIUS', 'DAMIAN', 'RUTTA', '1983-01-01', 'Married', 'Male', 'ruttademitrius@gmail.com', '+255754010701', NULL, 'af00d1e49c969ba5eacddc62e6d4bdadad1eb030', 'T', 'active', 'teacher', '0000-00-00 00:00:00', 'STAFF'),
('STAFF0028', 'MUSSA', 'ELIAS', 'MSEGETE', '1987-01-15', 'Married', 'Male', 'msegetemussa87@gmail.com', '+255766059748', NULL, '28dc5c6b0f0b5de5ccec2fdea526d81e5a7257a0', 'T', 'active', 'teacher', '2018-07-11 11:10:48', 'STAFF'),
('STAFF0030', 'KALUNDE', 'SAID', 'SALUM', '1978-06-03', 'Married', 'Female', 'salumkalunde1978@yahoo.com', '+255752572382', NULL, 'e0d1f93538598878bb00ae065c6ac13849a36f15', 'T', 'active', 'teacher', '2018-07-12 07:48:03', 'STAFF'),
('STAFF0031', 'ELTON', 'NYACHERI', 'WARIOBA', '1983-08-20', 'Married', 'Male', 'eltonwarioba@gmail.com', '+255765155552', NULL, 'e3f1c6c68b592a767cf6437a365faf3286906b8b', 'T', 'active', 'teacher', '2018-07-18 09:16:47', 'STAFF'),
('STAFF0035', 'Byaombe', 'Mjanja', 'Matika', '2018-10-26', 'Single', 'Male', 'touni@mjini.com', '+255777889956', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'inactive', 'teacher', '2018-06-05 09:38:15', 'STAFF'),
('STAFF0339', 'Theresia', 'Richard', 'Joseph', '2018-06-01', 'Married', 'Male', 'theresiajoseph@gmail.com', '+255756775352', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'L', 'active', 'library', '0000-00-00 00:00:00', 'STAFF'),
('STAFF0987', 'DEMITRIUS', 'RUTTA', 'JAMES', '2018-05-31', 'Married', 'Male', 'mnene@hihi.com', '+255745234567', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'S', 'active', 'secretary', '2018-06-04 14:58:38', 'STAFF'),
('STAFF1111', 'Albert', 'Madoshi', 'Mahuyu', '1982-01-04', 'Married', 'Male', 'mahuyualbert@yahoo.com', '+255713313203', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-05 12:49:53', 'STAFF'),
('STAFF2020', 'Sitta', 'Yassin', 'Kerawe', '2018-06-01', 'Single', 'Male', 'skerawe@yahoo.com', '+255789701777', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-04 14:55:49', 'STAFF'),
('STAFF2222', 'Kanyika', 'Matiti', 'Dasmas', '2001-02-01', 'Divorced', 'Female', 'sakaramatitijr@gmail.com', '+255717237878', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-06 14:23:58', 'STAFF'),
('STAFF3211', 'utune', '', 'krizanti', '1990-06-11', 'Married', 'Male', '', '+255787879000', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-04 14:53:04', 'STAFF'),
('STAFF3333', 'ELIAS', 'CHARLES', 'KABOJA', '2018-06-01', 'Divorced', 'Male', 'sophiandungile@yahoo.com', '+255765234669', NULL, '7c222fb2927d828af22f592134e8932480637c0d', 'T', 'active', 'teacher', '2018-06-05 12:28:28', 'STAFF');

--
-- Triggers `staff`
--
DELIMITER $$
CREATE TRIGGER `before_update_on_staff_password_trigger` BEFORE UPDATE ON `staff` FOR EACH ROW BEGIN 
IF (NEW.password = (SELECT password FROM password_reset_tbl) && @user_id = (SELECT id FROM admin)) THEN 
REPLACE INTO audit_password_reset_tbl (staff_id, reset_by, reset_to, reset_time) VALUES(NEW.staff_id, @user_id, (SELECT password FROM password_reset_tbl), NOW()); REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(NEW.staff_id, 8, "Your password has been reset, make sure you change the default password", NOW()); 
END IF;  
IF NEW.password <> 'a96af5cb3ae3b0e034f1c597133307de25068490' THEN UPDATE audit_password_reset_tbl SET user_changed="T", reset_to=NEW.password WHERE staff_id=NEW.staff_id; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_activity_log_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_activity_log_view` (
`id` bigint(20)
,`activity` varchar(50)
,`tableName` varchar(50)
,`time` timestamp
,`source` varchar(30)
,`destination` varchar(10)
,`user_id` varchar(10)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`user_role` varchar(30)
,`last_log` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `staff_attendance_record`
--

CREATE TABLE `staff_attendance_record` (
  `id` int(11) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `att_id` int(2) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `date_` date NOT NULL,
  `reason` mediumtext DEFAULT NULL,
  `aid` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_attendance_record`
--

INSERT INTO `staff_attendance_record` (`id`, `staff_id`, `att_id`, `day`, `date_`, `reason`, `aid`) VALUES
(1, 'STAFF0001', 2, 'Tuesday', '2018-06-05', NULL, 1),
(2, 'STAFF0003', 2, 'Tuesday', '2018-06-05', NULL, 1),
(3, 'STAFF0004', 2, 'Tuesday', '2018-06-05', NULL, 1),
(4, 'STAFF0035', 3, 'Tuesday', '2018-06-05', NULL, 1),
(5, 'STAFF0339', 2, 'Tuesday', '2018-06-05', NULL, 1),
(6, 'STAFF0987', 4, 'Tuesday', '2018-06-05', NULL, 1),
(7, 'STAFF1111', 2, 'Tuesday', '2018-06-05', NULL, 1),
(8, 'STAFF2020', 2, 'Tuesday', '2018-06-05', NULL, 1),
(9, 'STAFF2222', 2, 'Tuesday', '2018-06-05', NULL, 1),
(10, 'STAFF3211', 2, 'Tuesday', '2018-06-05', NULL, 1),
(11, 'STAFF3333', 3, 'Tuesday', '2018-06-05', NULL, 1),
(12, 'STAFF0001', 2, 'Monday', '2018-06-04', NULL, 1),
(13, 'STAFF0003', 2, 'Monday', '2018-06-04', NULL, 1),
(14, 'STAFF0004', 2, 'Monday', '2018-06-04', NULL, 1),
(15, 'STAFF0035', 2, 'Monday', '2018-06-04', NULL, 1),
(16, 'STAFF0339', 2, 'Monday', '2018-06-04', NULL, 1),
(17, 'STAFF0987', 2, 'Monday', '2018-06-04', NULL, 1),
(18, 'STAFF1111', 1, 'Monday', '2018-06-04', NULL, 1),
(19, 'STAFF2020', 2, 'Monday', '2018-06-04', NULL, 1),
(20, 'STAFF2222', 4, 'Monday', '2018-06-04', NULL, 1),
(21, 'STAFF3211', 2, 'Monday', '2018-06-04', NULL, 1),
(22, 'STAFF3333', 4, 'Monday', '2018-06-04', NULL, 1);

--
-- Triggers `staff_attendance_record`
--
DELIMITER $$
CREATE TRIGGER `after_insert_staff_att_trigger` AFTER INSERT ON `staff_attendance_record` FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM staff_attendance_record WHERE staff_id=NEW.staff_id AND date_=NEW.date_ AND aid=NEW.aid INTO att_record_id; INSERT INTO audit_staff_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "ADD", @user_id, @ip_address, NOW(), NEW.att_id); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_staff_att_trigger` AFTER UPDATE ON `staff_attendance_record` FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM staff_attendance_record WHERE staff_id=NEW.staff_id AND date_=NEW.date_ AND aid=NEW.aid INTO att_record_id; INSERT INTO audit_staff_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.att_id); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_staff_attendance` BEFORE DELETE ON `staff_attendance_record` FOR EACH ROW BEGIN END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_attendance_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_attendance_view` (
`staff_id` varchar(10)
,`names` varchar(61)
,`gender` varchar(6)
,`att_type` char(1)
,`description` varchar(50)
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`date_` date
);

-- --------------------------------------------------------

--
-- Table structure for table `staff_department`
--

CREATE TABLE `staff_department` (
  `staff_id` varchar(10) NOT NULL,
  `dept_id` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_department`
--

INSERT INTO `staff_department` (`staff_id`, `dept_id`) VALUES
('STAFF0001', 18),
('STAFF0001', 21),
('STAFF0001', 22),
('STAFF0003', 20),
('STAFF0004', 20),
('STAFF0035', 22),
('STAFF0035', 23),
('STAFF1111', 18),
('STAFF1111', 25),
('STAFF2020', 20),
('STAFF2020', 25),
('STAFF2222', 21),
('STAFF3211', 19),
('STAFF3211', 23),
('STAFF3333', 19),
('STAFF3333', 25);

-- --------------------------------------------------------

--
-- Table structure for table `staff_department_dummy_table`
--

CREATE TABLE `staff_department_dummy_table` (
  `staff_id` varchar(10) NOT NULL,
  `dept_id` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_department_dummy_table`
--

INSERT INTO `staff_department_dummy_table` (`staff_id`, `dept_id`) VALUES
('STAFF0001', 1),
('STAFF0002', 4),
('STAFF0003', 3),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0005', 3),
('STAFF0001', 1),
('STAFF0008', 1),
('STAFF0008', 4),
('STAFF0008', 3),
('STAFF1111', 4),
('STAFF2222', 1),
('STAFF0008', 6),
('STAFF0008', 6),
('STAFF0008', 5),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0005', 7),
('STAFF0005', 3),
('STAFF0003', 6),
('STAFF0003', 3),
('STAFF0003', 3),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0002', 4),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0002', 1),
('STAFF1111', 1),
('STAFF1111', 4),
('STAFF1111', 1),
('STAFF1111', 4),
('STAFF0003', 3),
('STAFF0003', 3),
('STAFF0003', 7),
('STAFF0005', 4),
('STAFF0005', 5),
('STAFF0005', 4),
('STAFF0005', 1),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0002', 9),
('STAFF0002', 15),
('STAFF0002', 14),
('STAFF0002', 12),
('STAFF0001', 15),
('STAFF0001', 14),
('STAFF0001', 12),
('STAFF0002', 11),
('STAFF0002', 16),
('STAFF0002', 12),
('STAFF0002', 11),
('STAFF0001', 23),
('STAFF0001', 22),
('STAFF3333', 25),
('STAFF3333', 19),
('STAFF0003', 20),
('STAFF0003', 20),
('STAFF2222', 21),
('STAFF1111', 22),
('STAFF0004', 20),
('STAFF0004', 20),
('STAFF0035', 23),
('STAFF0035', 22),
('STAFF1111', 22),
('STAFF1111', 25),
('STAFF3211', 23),
('STAFF3211', 19),
('STAFF2020', 20),
('STAFF2020', 25),
('STAFF1111', 25),
('STAFF1111', 18),
('STAFF0001', 22),
('STAFF0001', 18),
('STAFF0001', 22),
('STAFF0001', 21),
('STAFF0001', 18);

--
-- Triggers `staff_department_dummy_table`
--
DELIMITER $$
CREATE TRIGGER `before_insert_on_staff_department_trigger` BEFORE INSERT ON `staff_department_dummy_table` FOR EACH ROW BEGIN 
REPLACE INTO staff_department VALUES(NEW.staff_id, NEW.dept_id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_group`
--

CREATE TABLE `staff_group` (
  `staff_id` varchar(10) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_group`
--

INSERT INTO `staff_group` (`staff_id`, `group_id`) VALUES
('STAFF0001', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_group_members_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_group_members_view` (
`group_id` int(11)
,`group_name` varchar(100)
,`group_members` mediumtext
);

-- --------------------------------------------------------

--
-- Table structure for table `staff_nok`
--

CREATE TABLE `staff_nok` (
  `staff_id` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `relation_type` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone_no` varchar(13) NOT NULL,
  `p_box` int(10) DEFAULT NULL,
  `p_region` varchar(30) DEFAULT NULL,
  `current_box` int(10) DEFAULT NULL,
  `current_region` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_nok`
--

INSERT INTO `staff_nok` (`staff_id`, `firstname`, `lastname`, `gender`, `relation_type`, `email`, `phone_no`, `p_box`, `p_region`, `current_box`, `current_region`) VALUES
('STAFF0001', 'Lyagunga', 'Thomas', 'Male', 'Wife', NULL, '+255754231234', 5, 'Mara', NULL, NULL),
('STAFF0003', 'BRAVO', 'BAYASABE', 'Male', 'Brother', NULL, '+255784858141', 35, 'Kagera', NULL, NULL),
('STAFF0004', 'KENNY', 'MUSEGETE', 'Male', 'Other', NULL, '+255784858146', 88, 'Morogoro', NULL, NULL),
('STAFF0013', 'EVER', 'LUMAZI', 'Male', 'Other', NULL, '+255767469261', 217, 'Mwanza', NULL, NULL),
('STAFF0014', 'AARON', 'KWIZERA', 'Male', 'Husband', NULL, '+255757867945', 217, 'Mwanza', NULL, NULL),
('STAFF0015', 'MARYCIANA', 'PAUL', 'Female', 'Wife', NULL, '+255759707353', 217, 'Mwanza', NULL, NULL),
('STAFF0016', 'PENDO', 'RAPHAEL', 'Female', 'Wife', NULL, '+255757370825', 217, 'Mwanza', NULL, NULL),
('STAFF0018', 'LINDA', 'ONINGO', 'Male', 'Wife', NULL, '+255787177326', 735, 'Mwanza', NULL, NULL),
('STAFF0019', 'NEEMA', 'BUKOMBE', 'Female', 'Wife', NULL, '+255765196514', 4094, 'Mwanza', NULL, NULL),
('STAFF0020', 'STEVEN', 'NTARE', 'Male', 'Husband', NULL, '+255758408996', 217, 'Mwanza', NULL, NULL),
('STAFF0021', 'ZUHURA', 'BUNDUKI', 'Female', 'Other', NULL, '+255784375082', 321, 'Tabora', NULL, NULL),
('STAFF0022', 'MONICA', 'ISAAC', 'Female', 'Sister', NULL, '+255768838455', 217, 'Mwanza', NULL, NULL),
('STAFF0023', 'DASMAS', 'MATITI', 'Male', 'Other', NULL, '+255752818454', 217, 'Mwanza', NULL, NULL),
('STAFF0024', 'SADICK', 'SIRAJI', 'Male', 'Other', NULL, '+255656999565', 217, 'Mwanza', NULL, NULL),
('STAFF0026', 'Mathias', 'Bujiku', 'Male', 'Husband', NULL, '+255762695553', 217, 'Mwanza', NULL, NULL),
('STAFF0027', 'JONESIA', 'RUTTA', 'Female', 'Wife', NULL, '+255653331087', 217, 'Mwanza', NULL, NULL),
('STAFF0028', 'WINNIE', 'MWAKIPUNDA', 'Female', 'Wife', NULL, '+255654252565', 217, 'Mwanza', NULL, NULL),
('STAFF0030', 'ASHA', 'JUMANNE', 'Female', 'Sister', NULL, '+255754345509', 217, 'Mwanza', NULL, NULL),
('STAFF0031', 'FROLIDA', 'WARIOBA', 'Female', 'Sister', NULL, '+255756675755', 182, 'Mara', NULL, NULL),
('STAFF0035', 'KIKOJOZI', 'KWAKE', 'Male', 'Brother', NULL, '+255684721441', 5647, 'Ruvuma', NULL, NULL),
('STAFF0339', 'Charles', 'Maduhu', 'Male', 'Husband', NULL, '+255785568466', 217, 'Mwanza', NULL, NULL),
('STAFF0987', 'Zizi', 'James', 'Male', 'Other', NULL, '+255764442312', 217, 'Mwanza', NULL, NULL),
('STAFF1111', 'Edita', 'Rutayuga', 'Female', 'Wife', NULL, '+255757517328', 217, 'Mwanza', NULL, NULL),
('STAFF2020', 'Godfrey', 'Kerawe', 'Male', 'Other', NULL, '+255755704903', 217, 'Mwanza', NULL, NULL),
('STAFF2222', 'elton', 'nyacheri', 'Male', 'Husband', NULL, '+255717237878', 217, 'Iringa', NULL, NULL),
('STAFF3211', 'ana', 'mayaya', 'Male', 'Wife', NULL, '+255717564534', 45, 'Shinyanga', NULL, NULL),
('STAFF3333', 'YUSUPH', 'KABOJA', 'Male', 'Other', NULL, '+255768610136', 217, 'Mwanza', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_plus_nok_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_plus_nok_view` (
`user_role` varchar(30)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`last_log` timestamp
,`nok_fn` varchar(30)
,`nok_ln` varchar(30)
,`nok_gender` varchar(6)
,`relation_type` varchar(30)
,`nok_email` varchar(50)
,`nok_phone_no` varchar(13)
,`nok_box` int(10)
,`nok_region` varchar(30)
,`current_box` int(10)
,`current_region` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_qualification_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_qualification_view` (
`q_id` int(11)
,`certified` enum('yes','no')
,`user_role` varchar(30)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`last_log` timestamp
,`certification` varchar(30)
,`completion_date` year(4)
,`description` mediumtext
,`university` varchar(100)
,`nok_fn` varchar(30)
,`nok_ln` varchar(30)
,`nok_gender` varchar(6)
,`relation_type` varchar(30)
,`nok_email` varchar(50)
,`nok_phone_no` varchar(13)
,`nok_box` int(10)
,`nok_region` varchar(30)
,`current_box` int(10)
,`current_region` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `staff_role`
--

CREATE TABLE `staff_role` (
  `role_type_id` int(2) NOT NULL,
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_role`
--

INSERT INTO `staff_role` (`role_type_id`, `staff_id`) VALUES
(3, 'STAFF0001'),
(3, 'STAFF0035'),
(3, 'STAFF2020'),
(3, 'STAFF2222'),
(3, 'STAFF3211'),
(3, 'STAFF3333'),
(4, 'STAFF0001'),
(5, 'STAFF0001'),
(5, 'STAFF0003'),
(5, 'STAFF1111'),
(5, 'STAFF3211'),
(9, 'STAFF2020'),
(9, 'STAFF2222'),
(19, 'STAFF0013'),
(20, 'STAFF0030'),
(21, 'STAFF0014');

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_role_permission_view`
-- (See below for the actual view)
--
CREATE TABLE `staff_role_permission_view` (
`staff_id` varchar(10)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`phone_no` varchar(13)
,`status` enum('active','inactive')
,`permission_id` int(5) unsigned
,`description` varchar(255)
,`role_type_id` int(2)
,`role_name` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `stream`
--

CREATE TABLE `stream` (
  `stream_id` int(2) NOT NULL,
  `stream_name` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stream`
--

INSERT INTO `stream` (`stream_id`, `stream_name`) VALUES
(1, 'A'),
(3, 'B'),
(4, 'C'),
(11, 'E1'),
(13, 'EGM'),
(9, 'PCB'),
(12, 'PGM');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `admission_no` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `academic_year` int(5) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `class_id` varchar(10) DEFAULT NULL,
  `class_stream_id` varchar(10) DEFAULT NULL,
  `dob` date NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `tribe_id` int(5) NOT NULL,
  `religion_id` int(5) NOT NULL,
  `home_address` varchar(50) DEFAULT NULL,
  `region` varchar(50) NOT NULL,
  `former_school` varchar(100) NOT NULL,
  `dorm_id` int(3) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `stte` enum('selected','transferred_in') NOT NULL DEFAULT 'selected',
  `stte_out` enum('transferred_out','expelled','completed','suspended','') DEFAULT NULL,
  `disabled` enum('yes','no') NOT NULL DEFAULT 'no',
  `disability` enum('Deaf','Mute','Blind','Physical Disability') DEFAULT NULL,
  `b_type` enum('STUDENT') DEFAULT 'STUDENT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`admission_no`, `date`, `academic_year`, `firstname`, `lastname`, `class_id`, `class_stream_id`, `dob`, `image`, `tribe_id`, `religion_id`, `home_address`, `region`, `former_school`, `dorm_id`, `status`, `stte`, `stte_out`, `disabled`, `disability`, `b_type`) VALUES
('0987654', '2018-06-05', 3, 'Mirembe', 'Julias', 'C05', 'C05PCB', '2018-06-05', 'http://192.168.100.5/assets/images/mirembe_julias.gif', 62, 1, '567', 'Geita', 'geita secondary school', 4, 'active', 'selected', 'suspended', 'no', '', 'STUDENT'),
('1000120', '2018-06-04', 3, 'Ezron', 'Patrick', 'C05', 'C05EGM', '1994-07-12', NULL, 10, 1, '34557', 'Katavi', 'Karagwe primary school', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000121', '2018-06-04', 3, 'Edson', 'Mathayo', 'C05', 'C05PCB', '1993-07-12', NULL, 15, 1, '23190', 'Arusha', 'Jamhuri primary school', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000122', '2018-06-04', 3, 'Anaset', 'Evansy', 'C05', 'C05EGM', '1991-03-20', NULL, 13, 1, '56342', 'Arusha', 'Malangwa primary school', 4, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000123', '2018-06-04', 3, 'Samson', 'Edward', 'C05', 'C05PCB', '1991-03-20', NULL, 11, 1, '56311', 'Arusha', 'Malangwa primary school', 4, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000124', '2018-06-04', 3, 'Muzamiru', 'Abbakari', 'C05', 'C05EGM', '1990-11-11', NULL, 16, 2, '12390', 'Kigoma', 'Kashasha primary school', 4, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000125', '2018-06-04', 3, 'Munawala', 'Abbakari', 'C05', 'C05PCB', '1994-09-11', NULL, 13, 2, '99778', 'Morogoro', 'Msamvu primary school', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000126', '2018-06-04', 3, 'Swarehe', 'Sadamu', 'C05', 'C05EGM', '1992-08-02', NULL, 13, 2, '99723', 'Morogoro', 'Musabe Internation Sec school', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1000127', '2018-06-04', 3, 'Swaumuni', 'Swadikta', 'C05', 'C05PCB', '1989-05-23', NULL, 8, 2, '23416', 'Manyara', 'Musoun Islamic Sec school', 3, 'inactive', 'selected', 'expelled', 'no', '', 'STUDENT'),
('1031456', '2018-06-01', 3, 'Demitrius', 'Rutta', 'C05', 'C05EGM', '0000-00-00', NULL, 7, 1, '217', 'Kagera', 'Kaitaba  sm', 1, 'inactive', 'selected', 'expelled', 'no', '', 'STUDENT'),
('1234567', '2018-06-04', 3, 'Juma', 'Mwaipaya', 'C05', 'C05PCB', '2018-03-05', NULL, 14, 1, '21', 'Geita', 'Azania Secondary School', 4, 'active', 'transferred_in', NULL, 'yes', 'Physical Disability', 'STUDENT'),
('1563242', '2018-06-05', 3, 'Majaliwa', 'Matatizo', 'C05', 'C05EGM', '2002-06-22', NULL, 135, 2, '217', 'Mwanza', 'Zogimlole', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('1647366', '2018-06-05', 3, 'Ambakisye', 'Mwakifwamba', 'C05', 'C05PCB', '1997-06-27', NULL, 103, 1, '227', 'Mbeya', 'Kolomije', 5, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('4352111', '2018-06-05', 3, 'Hawa', 'Ghasia', 'C05', 'C05EGM', '2000-02-02', NULL, 105, 2, '21', 'Manyara', 'Joomla Secondary School', 4, 'active', 'selected', NULL, 'yes', 'Physical Disability', 'STUDENT'),
('5555555', '2018-06-05', 3, 'Vbog', 'Vasa', 'C05', 'C05EGM', '2008-01-09', NULL, 18, 2, '78', 'Manyara', 'Nyasaka', 5, 'active', 'transferred_in', NULL, 'yes', 'Deaf', 'STUDENT'),
('6754678', '2018-06-05', 3, 'Haruna', 'Bakaru', 'C05', 'C05PCB', '2018-05-08', NULL, 18, 1, '98', 'Dodoma', 'Nkurumahia Secondary School', 3, 'active', 'selected', NULL, 'no', '', 'STUDENT'),
('7656777', '2018-06-05', 3, 'Mashauri', 'Boaz', 'C05', 'C05EGM', '1988-06-10', NULL, 36, 1, '502', 'Dar-es-salaam', 'nyasaka', 2, 'active', 'selected', NULL, 'no', '', 'STUDENT');

--
-- Triggers `student`
--
DELIMITER $$
CREATE TRIGGER `before_insert_into_student_trigger` BEFORE INSERT ON `student` FOR EACH ROW BEGIN DECLARE a INT DEFAULT 0;  DECLARE lvl VARCHAR(10); DECLARE nos INT DEFAULT 0;
SELECT COUNT(*) FROM class_stream_subject WHERE class_stream_id=NEW.class_stream_id INTO a;
SELECT number_of_subjects FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO nos;

IF (a != 0 && a = nos) THEN
INSERT INTO audit_register_student_tbl (admission_no, change_by, class_stream_id, change_type, time, source) VALUES(NEW.admission_no, @user_id, NEW.class_stream_id, "NEW", NOW(), @ip_address);
ELSE
SIGNAL SQLSTATE '45000' set message_text = "This class does not have any subjects at the moment";  
END IF; 
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_student_subject_trigger` AFTER INSERT ON `student` FOR EACH ROW BEGIN INSERT INTO enrollment (admission_no, class_id, class_stream_id, academic_year) VALUES(NEW.admission_no, NEW.class_id, NEW.class_stream_id, (SELECT id FROM academic_year where class_level=(SELECT level FROM class_level WHERE class_id=NEW.class_id) AND status="current_academic_year")); CALL select_class_stream_subjects(NEW.admission_no, NEW.class_stream_id); INSERT INTO accomodation_history(admission_no, dorm_id, start_date) VALUES(NEW.admission_no, NEW.dorm_id, NOW()); END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `students_by_class_and_subject_view`
-- (See below for the actual view)
--
CREATE TABLE `students_by_class_and_subject_view` (
`level` enum('O''Level','A''Level')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`names` varchar(41)
,`class_stream_id` varchar(10)
,`class_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `students_in_dormitory_view`
-- (See below for the actual view)
--
CREATE TABLE `students_in_dormitory_view` (
`class_name` varchar(30)
,`stream` varchar(5)
,`admission_no` varchar(10)
,`names` varchar(41)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`status` enum('active','inactive')
,`dorm_id` int(3)
);

-- --------------------------------------------------------

--
-- Table structure for table `student_assessment`
--

CREATE TABLE `student_assessment` (
  `admission_no` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `average` float(5,2) DEFAULT 0.00,
  `grade` varchar(3) DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `class_rank` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_assessment`
--

INSERT INTO `student_assessment` (`admission_no`, `class_id`, `class_stream_id`, `average`, `grade`, `term_id`, `class_rank`) VALUES
('0987654', 'C05', 'C05PCB', NULL, NULL, 2, 13),
('1000120', 'C05', 'C05EGM', 90.00, 'A', 2, 1),
('1000121', 'C05', 'C05PCB', 78.00, 'B', 2, 5),
('1000122', 'C05', 'C05EGM', 76.00, 'B', 2, 10),
('1000123', 'C05', 'C05PCB', 76.00, 'B', 2, 10),
('1000124', 'C05', 'C05EGM', 90.00, 'A', 2, 1),
('1000125', 'C05', 'C05PCB', 78.00, 'B', 2, 5),
('1000126', 'C05', 'C05EGM', 88.00, 'A', 2, 5),
('1234567', 'C05', 'C05PCB', 90.00, 'A', 2, 1),
('1563242', 'C05', 'C05EGM', 89.00, 'A', 2, 4),
('1647366', 'C05', 'C05PCB', 67.00, 'C', 2, 12),
('4352111', 'C05', 'C05EGM', 67.00, 'C', 2, 12),
('5555555', 'C05', 'C05EGM', 86.00, 'A', 2, 7),
('6754678', 'C05', 'C05PCB', 65.00, 'C', 2, 13),
('7656777', 'C05', 'C05EGM', 87.00, 'A', 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance_record`
--

CREATE TABLE `student_attendance_record` (
  `id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `att_id` int(2) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `date_` date NOT NULL,
  `month` enum('January','February','March','April','May','June','July','August','September','October','November','December') DEFAULT NULL,
  `term_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_attendance_record`
--

INSERT INTO `student_attendance_record` (`id`, `admission_no`, `class_stream_id`, `att_id`, `day`, `date_`, `month`, `term_id`) VALUES
(1, '1000120', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2),
(2, '1000122', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2),
(3, '1000124', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2),
(4, '1000126', 'C05EGM', 3, 'Wednesday', '2018-06-06', 'June', 2),
(5, '1563242', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2),
(6, '4352111', 'C05EGM', 1, 'Wednesday', '2018-06-06', 'June', 2),
(7, '5555555', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2),
(8, '7656777', 'C05EGM', 2, 'Wednesday', '2018-06-06', 'June', 2);

--
-- Triggers `student_attendance_record`
--
DELIMITER $$
CREATE TRIGGER `after_insert_student_attendance` AFTER INSERT ON `student_attendance_record` FOR EACH ROW BEGIN
DECLARE att_record_id INT(2) DEFAULT 0;
SELECT id FROM student_attendance_record WHERE admission_no=NEW.admission_no AND date_=NEW.date_ AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO att_record_id;
INSERT INTO audit_student_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "ADD", @user_id, @ip_address, NOW(), NEW.att_id);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_student_attendance` AFTER UPDATE ON `student_attendance_record` FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM student_attendance_record WHERE admission_no=NEW.admission_no AND date_=NEW.date_ AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO att_record_id; INSERT INTO audit_student_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.att_id); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_student_attendance` BEFORE DELETE ON `student_attendance_record` FOR EACH ROW BEGIN  END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_enrollment_academic_year_view`
-- (See below for the actual view)
--
CREATE TABLE `student_enrollment_academic_year_view` (
`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`is_current` enum('yes','no')
,`aid` int(5)
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`admission_no` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`academic_year` int(5)
,`class_name` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_guardian_view`
-- (See below for the actual view)
--
CREATE TABLE `student_guardian_view` (
`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`stte` enum('selected','transferred_in')
,`tribe_id` int(5)
,`religion_id` int(5)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`student_firstname` varchar(20)
,`student_lastname` varchar(20)
,`g_firstname` varchar(30)
,`g_lastname` varchar(30)
,`g_middlename` varchar(30)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`date` date
,`dob` date
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`status` enum('active','inactive')
,`guardian_names` varchar(92)
,`gender` enum('Male','Female')
,`rel_type` enum('Father','Mother','Guardian')
,`email` varchar(50)
,`occupation` varchar(50)
,`phone_no` varchar(30)
,`p_box` int(5)
,`p_region` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_progressive_results_view`
-- (See below for the actual view)
--
CREATE TABLE `student_progressive_results_view` (
`subject_rank` int(3)
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`terminal` float(5,2)
,`average` float(5,2)
,`grade` varchar(3)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_result_view`
-- (See below for the actual view)
--
CREATE TABLE `student_result_view` (
`subject_rank` int(3)
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`terminal` float(5,2)
,`average` float(5,2)
,`grade` varchar(3)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`academic_year` int(5)
,`aid` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Table structure for table `student_roll_call_record`
--

CREATE TABLE `student_roll_call_record` (
  `id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `att_id` int(2) NOT NULL,
  `date_` date NOT NULL,
  `month` enum('January','February','March','April','May','June','July','August','September','October','November','December') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE `student_subject` (
  `admission_no` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_subject`
--

INSERT INTO `student_subject` (`admission_no`, `subject_id`, `status`, `class_stream_id`) VALUES
('0987654', 'SUB01', 'active', 'C05PCB'),
('0987654', 'SUB02', 'active', 'C05PCB'),
('0987654', 'SUB03', 'active', 'C05PCB'),
('0987654', 'SUB07', 'active', 'C05PCB'),
('0987654', 'SUB09', 'active', 'C05PCB'),
('1000120', 'SUB01', 'active', 'C05EGM'),
('1000120', 'SUB04', 'active', 'C05EGM'),
('1000120', 'SUB05', 'active', 'C05EGM'),
('1000120', 'SUB08', 'active', 'C05EGM'),
('1000121', 'SUB01', 'active', 'C05PCB'),
('1000121', 'SUB02', 'active', 'C05PCB'),
('1000121', 'SUB03', 'active', 'C05PCB'),
('1000121', 'SUB07', 'active', 'C05PCB'),
('1000121', 'SUB09', 'active', 'C05PCB'),
('1000122', 'SUB01', 'active', 'C05EGM'),
('1000122', 'SUB04', 'active', 'C05EGM'),
('1000122', 'SUB05', 'active', 'C05EGM'),
('1000122', 'SUB08', 'active', 'C05EGM'),
('1000123', 'SUB01', 'active', 'C05PCB'),
('1000123', 'SUB02', 'active', 'C05PCB'),
('1000123', 'SUB03', 'active', 'C05PCB'),
('1000123', 'SUB07', 'active', 'C05PCB'),
('1000123', 'SUB09', 'active', 'C05PCB'),
('1000124', 'SUB01', 'active', 'C05EGM'),
('1000124', 'SUB04', 'active', 'C05EGM'),
('1000124', 'SUB05', 'active', 'C05EGM'),
('1000124', 'SUB08', 'active', 'C05EGM'),
('1000125', 'SUB01', 'active', 'C05PCB'),
('1000125', 'SUB02', 'active', 'C05PCB'),
('1000125', 'SUB03', 'active', 'C05PCB'),
('1000125', 'SUB07', 'active', 'C05PCB'),
('1000125', 'SUB09', 'active', 'C05PCB'),
('1000126', 'SUB01', 'active', 'C05EGM'),
('1000126', 'SUB04', 'active', 'C05EGM'),
('1000126', 'SUB05', 'active', 'C05EGM'),
('1000126', 'SUB08', 'active', 'C05EGM'),
('1000127', 'SUB01', 'active', 'C05PCB'),
('1000127', 'SUB02', 'active', 'C05PCB'),
('1000127', 'SUB03', 'active', 'C05PCB'),
('1000127', 'SUB07', 'active', 'C05PCB'),
('1000127', 'SUB09', 'active', 'C05PCB'),
('1031456', 'SUB01', 'active', 'C05EGM'),
('1031456', 'SUB04', 'active', 'C05EGM'),
('1031456', 'SUB05', 'active', 'C05EGM'),
('1031456', 'SUB08', 'active', 'C05EGM'),
('1234567', 'SUB01', 'active', 'C05PCB'),
('1234567', 'SUB02', 'active', 'C05PCB'),
('1234567', 'SUB03', 'active', 'C05PCB'),
('1234567', 'SUB07', 'active', 'C05PCB'),
('1234567', 'SUB09', 'active', 'C05PCB'),
('1563242', 'SUB01', 'active', 'C05EGM'),
('1563242', 'SUB04', 'active', 'C05EGM'),
('1563242', 'SUB05', 'active', 'C05EGM'),
('1563242', 'SUB08', 'active', 'C05EGM'),
('1647366', 'SUB01', 'active', 'C05PCB'),
('1647366', 'SUB02', 'active', 'C05PCB'),
('1647366', 'SUB03', 'active', 'C05PCB'),
('1647366', 'SUB07', 'active', 'C05PCB'),
('1647366', 'SUB09', 'active', 'C05PCB'),
('4352111', 'SUB01', 'active', 'C05EGM'),
('4352111', 'SUB04', 'active', 'C05EGM'),
('4352111', 'SUB05', 'active', 'C05EGM'),
('4352111', 'SUB08', 'active', 'C05EGM'),
('5555555', 'SUB01', 'active', 'C05EGM'),
('5555555', 'SUB04', 'active', 'C05EGM'),
('5555555', 'SUB05', 'active', 'C05EGM'),
('5555555', 'SUB08', 'active', 'C05EGM'),
('6754678', 'SUB01', 'active', 'C05PCB'),
('6754678', 'SUB02', 'active', 'C05PCB'),
('6754678', 'SUB03', 'active', 'C05PCB'),
('6754678', 'SUB07', 'active', 'C05PCB'),
('6754678', 'SUB09', 'active', 'C05PCB'),
('7656777', 'SUB01', 'active', 'C05EGM'),
('7656777', 'SUB04', 'active', 'C05EGM'),
('7656777', 'SUB05', 'active', 'C05EGM'),
('7656777', 'SUB08', 'active', 'C05EGM');

-- --------------------------------------------------------

--
-- Table structure for table `student_subject_assessment`
--

CREATE TABLE `student_subject_assessment` (
  `ssa_id` bigint(20) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `etype_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `e_date` date NOT NULL,
  `marks` float(5,2) DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `status` enum('Pre','Abs') NOT NULL DEFAULT 'Pre'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_subject_assessment`
--

INSERT INTO `student_subject_assessment` (`ssa_id`, `admission_no`, `subject_id`, `etype_id`, `class_stream_id`, `e_date`, `marks`, `term_id`, `status`) VALUES
(616, '1000120', 'SUB01', 'E04', 'C05EGM', '2018-12-05', 90.00, 2, 'Pre'),
(617, '1000122', 'SUB01', 'E04', 'C05EGM', '2018-12-05', NULL, 2, 'Pre'),
(618, '1000124', 'SUB01', 'E04', 'C05EGM', '2018-12-05', 90.00, 2, 'Pre'),
(619, '1000126', 'SUB01', 'E04', 'C05EGM', '2018-12-05', 88.00, 2, 'Pre'),
(620, '1563242', 'SUB01', 'E04', 'C05EGM', '2018-12-05', NULL, 2, 'Pre'),
(621, '4352111', 'SUB01', 'E04', 'C05EGM', '2018-12-05', 67.00, 2, 'Pre'),
(622, '5555555', 'SUB01', 'E04', 'C05EGM', '2018-12-05', NULL, 2, 'Pre'),
(623, '7656777', 'SUB01', 'E04', 'C05EGM', '2018-12-05', 87.00, 2, 'Pre');

--
-- Triggers `student_subject_assessment`
--
DELIMITER $$
CREATE TRIGGER `after_insert_on_student_subject_assessment_trigger` AFTER INSERT ON `student_subject_assessment` FOR EACH ROW BEGIN
DECLARE check_exist INT DEFAULT 0;
DECLARE check_e4_exist INT DEFAULT 0;
DECLARE darasa_id VARCHAR(10);
DECLARE exam_type VARCHAR(10);
DECLARE avg FLOAT(5, 2);
DECLARE overall_avg FLOAT(5, 2);
DECLARE grd VARCHAR(2);
DECLARE overall_grade VARCHAR(2);
DECLARE lvl VARCHAR(10);
DECLARE e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE sum_non_e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE get_ssa_id BIGINT(20) DEFAULT 0;

SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id INTO check_exist;
SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id AND terminal IS NOT NULL INTO check_e4_exist;

#USED FOR COMPUTINN ONLY
SELECT SUM(marks) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id INTO e4_marks;
SELECT SUM(marks)/COUNT(etype_id) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO sum_non_e4_marks;
IF e4_marks IS NULL THEN
SET e4_marks = 0;
END IF;
IF sum_non_e4_marks IS NULL THEN
SET sum_non_e4_marks = 0;
END IF;
#END USED FOR COMPUTING

IF check_exist = 0 THEN	#IF YA INSERT FOR THE FIRST TIME
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF NEW.etype_id = "E04" && NEW.marks IS NOT NULL THEN	#START IF OF TERM EXAM SHOULD NOT BE COMPUTED IF IS ONLY ETYPE DONE
SELECT ROUND(e4_marks, 2) INTO avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
ELSE
SET avg = NULL;
SET grd = NULL;
END IF;	#END IF OF TERM EXAM SHOULD NOT BE COMPUTED IF IS ONLY EXAM_TYPE DONE



IF NEW.etype_id = "E01" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_one, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E02" THEN
INSERT INTO student_subject_score_position (admission_no, midterm, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E03" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_two, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E04" && NEW.marks IS NOT NULL THEN
INSERT INTO student_subject_score_position (admission_no, terminal, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
REPLACE INTO student_assessment (admission_no, class_id, class_stream_id, average, grade, term_id) VALUES (NEW.admission_no, darasa_id, NEW.class_stream_id, avg, grd, NEW.term_id);
#IF NEW.marks IS NOT NULL THEN
CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id, NEW.class_stream_id, darasa_id);
#END IF;
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

END IF;
END IF;	#END IF OF TERM EXAM SHOULD NOT BE COMPUTED IF IS ONLY EXAM_TYPE DONE


IF check_exist > 0 THEN	#START IF THERE IS AN EXAM_TYPE ALREADY ADDED THAT TERM

IF NEW.etype_id = "E01" && check_e4_exist = 0 THEN	#START IF AN EXAM_TYPE IS E01 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;	#END IF AN EXAM_TYPE IS E01 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM

IF NEW.etype_id = "E02" && check_e4_exist = 0 THEN	#START IF AN EXAM_TYPE IS E02 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;	#END IF AN EXAM_TYPE IS E02 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM

IF NEW.etype_id = "E03" && check_e4_exist = 0 THEN	#START IF AN EXAM_TYPE IS E03 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;	#END IF AN EXAM_TYPE IS E03 AND THERE IS A TERMINAL EXAM ADDED IN THAT TERM

IF NEW.etype_id = "E04" THEN	#START IF AN EXAM_TYPE IS E04 AND THERE IS ANOTHER EXAM_TYPE ADDED IN THAT TERM
UPDATE student_subject_score_position SET terminal=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;

SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;

SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;

UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(subject_id), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;

CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id, NEW.class_stream_id, darasa_id);
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

END IF;	#END IF AN EXAM_TYPE IS E04 AND THERE IS ANOTHER EXAM_TYPE ADDED IN THAT TERM

IF ((check_e4_exist = 1 && NEW.etype_id <> "E04")) THEN	#START IF AN EXAM_TYPE IS E04 OR (THE TERMINAL EXAM EXISTS AND IS NOT 'E04') THAT TERM

IF NEW.etype_id = "E01" THEN
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;	
IF NEW.etype_id = "E02" THEN
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E03" THEN
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;


SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;

SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;

UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(subject_id), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;

CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id, NEW.class_stream_id, darasa_id);
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

END IF;	#END IF AN EXAM_TYPE IS E04 OR (THE TERMINAL EXAM EXISTS AND IS NOT 'E04') THAT TERM
END IF;	#END IF THERE IS AN EXAM_TYPE ALREADY ADDED THAT TERM

SELECT ssa_id FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id=NEW.etype_id AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO get_ssa_id;
INSERT INTO audit_student_subject_assessment (ssa_id, change_type, change_by, ip_address, change_time, marks) VALUES(get_ssa_id, "ADD", @user_id, @ip_address, NOW(), NEW.marks);

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_on_student_subject_assessment_trigger` AFTER UPDATE ON `student_subject_assessment` FOR EACH ROW BEGIN DECLARE check_exist INT DEFAULT 0;
DECLARE darasa_id VARCHAR(10);
DECLARE exam_type VARCHAR(10);
DECLARE avg FLOAT(5, 2);
DECLARE overall_avg FLOAT(5, 2);
DECLARE grd VARCHAR(2);
DECLARE overall_grade VARCHAR(2);
DECLARE lvl VARCHAR(10);
DECLARE e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE sum_non_e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE get_ssa_id BIGINT(20) DEFAULT 0;

SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id INTO check_exist;

SELECT SUM(marks) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id INTO e4_marks;
SELECT SUM(marks)/COUNT(etype_id) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO sum_non_e4_marks;
IF e4_marks IS NULL THEN
SET e4_marks = 0;
END IF;
IF sum_non_e4_marks IS NULL THEN
SET sum_non_e4_marks = 0;
END IF;

IF NEW.etype_id = "E01" THEN
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E02" THEN
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E03" THEN
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E04" THEN
UPDATE student_subject_score_position SET terminal=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
#CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id, NEW.class_stream_id, darasa_id);
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

SELECT ssa_id FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id=NEW.etype_id AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO get_ssa_id;
INSERT INTO audit_student_subject_assessment (ssa_id, change_type, change_by, ip_address, change_time, marks) VALUES(get_ssa_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.marks);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_results_trigger` BEFORE DELETE ON `student_subject_assessment` FOR EACH ROW BEGIN END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_insert_results_trigger` BEFORE INSERT ON `student_subject_assessment` FOR EACH ROW BEGIN 
DECLARE g VARCHAR(2) DEFAULT "";
DECLARE lvl VARCHAR(10) DEFAULT NULL;
DECLARE min_mark FLOAT(5,2) DEFAULT 0.00;
DECLARE max_mark FLOAT(5,2) DEFAULT 100.00;

IF NEW.marks IS NOT NULL THEN

IF(NEW.marks < min_mark || NEW.marks > max_mark) THEN 
SIGNAL SQLSTATE '45000' SET message_text="Error: Marks should NOT be less that 0.00 OR greater than 100";
END IF; 
SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id) INTO lvl;
IF lvl = "O'Level" THEN
SELECT grade FROM o_level_grade_system_tbl WHERE NEW.marks >= start_mark && NEW.marks <= end_mark INTO g;
IF g = "" THEN
SIGNAL SQLSTATE '45000' SET message_text="Error: The O'Level grade system table in not properly set, Check with your Administrator";
END IF;
END IF;

IF lvl = "A'Level" THEN
SELECT grade FROM a_level_grade_system_tbl WHERE NEW.marks >= start_mark && NEW.marks <= end_mark INTO g;
IF g = "" THEN
SIGNAL SQLSTATE '45000' SET message_text="Error: The A'Level grade system table in not properly set, Check with your Administrator";
END IF;
END IF;

END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_update_results_trigger` BEFORE UPDATE ON `student_subject_assessment` FOR EACH ROW BEGIN 
DECLARE g VARCHAR(2) DEFAULT "";
DECLARE lvl VARCHAR(10) DEFAULT NULL;
DECLARE min_mark FLOAT(5,2) DEFAULT 0.00; 
DECLARE max_mark FLOAT(5,2) DEFAULT 100.00; 
IF(NEW.marks < min_mark || NEW.marks > max_mark) THEN 
SIGNAL SQLSTATE '45000' SET message_text="Error: Marks should NOT be less that 0.00 OR greater than 100"; 
END IF;

SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id) INTO lvl;
IF lvl = "O'Level" THEN
SELECT grade FROM o_level_grade_system_tbl WHERE NEW.marks >= start_mark && NEW.marks <= end_mark INTO g;
IF g = "" THEN
SIGNAL SQLSTATE '45000' SET message_text="Error: The O'Level grade system table in not properly set, Check with Administrator";
END IF;
END IF;

IF lvl = "A'Level" THEN
SELECT grade FROM a_level_grade_system_tbl WHERE NEW.marks >= start_mark && NEW.marks <= end_mark INTO g;
IF g = "" THEN
SIGNAL SQLSTATE '45000' SET message_text="Error: The A'Level grade system table in not properly set, Check with your Administrator";
END IF;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `student_subject_score_position`
--

CREATE TABLE `student_subject_score_position` (
  `admission_no` varchar(10) NOT NULL,
  `monthly_one` float(5,2) DEFAULT NULL,
  `midterm` float(5,2) DEFAULT NULL,
  `monthly_two` float(5,2) DEFAULT NULL,
  `average` float(5,2) DEFAULT NULL,
  `terminal` float(5,2) DEFAULT NULL,
  `subject_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `grade` varchar(3) DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `subject_rank` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_subject_score_position`
--

INSERT INTO `student_subject_score_position` (`admission_no`, `monthly_one`, `midterm`, `monthly_two`, `average`, `terminal`, `subject_id`, `class_id`, `class_stream_id`, `grade`, `term_id`, `subject_rank`) VALUES
('1000120', NULL, NULL, NULL, 90.00, 90.00, 'SUB01', 'C05', 'C05EGM', 'A', 2, 1),
('1000124', NULL, NULL, NULL, 90.00, 90.00, 'SUB01', 'C05', 'C05EGM', 'A', 2, 1),
('1000126', NULL, NULL, NULL, 88.00, 88.00, 'SUB01', 'C05', 'C05EGM', 'A', 2, 3),
('4352111', NULL, NULL, NULL, 67.00, 67.00, 'SUB01', 'C05', 'C05EGM', 'C', 2, 5),
('7656777', NULL, NULL, NULL, 87.00, 87.00, 'SUB01', 'C05', 'C05EGM', 'A', 2, 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_transfer_in_view`
-- (See below for the actual view)
--
CREATE TABLE `student_transfer_in_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`stte` enum('selected','transferred_in')
,`status` enum('active','inactive')
,`form` varchar(10)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`school` varchar(255)
,`class_name` varchar(30)
,`date_of_transfer` date
,`transfer_letter` enum('Available','NOT Available')
,`self_form_receipt` enum('Received','NOT Received')
,`transfer_status` enum('IN','OUT')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_transfer_out_view`
-- (See below for the actual view)
--
CREATE TABLE `student_transfer_out_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`status` enum('active','inactive')
,`form` varchar(10)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`school` varchar(255)
,`class_name` varchar(30)
,`date_of_transfer` date
,`transfer_letter` enum('Available','NOT Available')
,`self_form_receipt` enum('Received','NOT Received')
,`transfer_status` enum('IN','OUT')
);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `subject_id` varchar(10) NOT NULL,
  `subject_name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `dept_id` int(3) UNSIGNED DEFAULT NULL,
  `subject_category` enum('T','N') NOT NULL,
  `subject_choice` enum('optional','compulsory') NOT NULL DEFAULT 'compulsory',
  `studied_by` enum('O','A','BOTH') NOT NULL,
  `subject_type` enum('Core','Core_With_Penalty','Supplement') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subject_id`, `subject_name`, `description`, `dept_id`, `subject_category`, `subject_choice`, `studied_by`, `subject_type`) VALUES
('SUB01', 'GENERAL STUDY', NULL, 21, 'N', 'compulsory', 'A', 'Supplement'),
('SUB02', 'CHEMISTRY', NULL, 22, 'N', 'compulsory', 'BOTH', 'Core'),
('SUB03', 'BIOLOGY', NULL, 23, 'N', 'compulsory', 'BOTH', 'Core'),
('SUB04', 'ECONOMICS', NULL, 25, 'N', 'compulsory', 'A', 'Core'),
('SUB05', 'GEOGRAPHY', NULL, 19, 'N', 'compulsory', 'BOTH', 'Core'),
('SUB07', 'BASIC APPLIED MATHEMATICS', NULL, 20, 'N', 'compulsory', 'A', 'Supplement'),
('SUB08', 'ADVANCED MATHEMATICS', NULL, 20, 'N', 'compulsory', 'A', 'Core'),
('SUB09', 'PHYSICS', NULL, 18, 'N', 'compulsory', 'A', 'Core');

-- --------------------------------------------------------

--
-- Stand-in structure for view `subjects_subtopic_view`
-- (See below for the actual view)
--
CREATE TABLE `subjects_subtopic_view` (
`class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`subtopic_id` int(11)
,`subtopic_name` varchar(255)
,`topic_name` varchar(100)
,`topic_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `subject_department_view`
-- (See below for the actual view)
--
CREATE TABLE `subject_department_view` (
`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`staff_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`subject_type` enum('Core','Core_With_Penalty','Supplement')
);

-- --------------------------------------------------------

--
-- Table structure for table `subject_teacher`
--

CREATE TABLE `subject_teacher` (
  `subject_id` varchar(10) NOT NULL,
  `teacher_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject_teacher`
--

INSERT INTO `subject_teacher` (`subject_id`, `teacher_id`) VALUES
('SUB01', 'STAFF0001'),
('SUB01', 'STAFF2222'),
('SUB02', 'STAFF0001'),
('SUB02', 'STAFF0035'),
('SUB03', 'STAFF0035'),
('SUB03', 'STAFF3211'),
('SUB04', 'STAFF1111'),
('SUB04', 'STAFF2020'),
('SUB04', 'STAFF3333'),
('SUB05', 'STAFF3211'),
('SUB05', 'STAFF3333'),
('SUB07', 'STAFF0003'),
('SUB07', 'STAFF0004'),
('SUB07', 'STAFF2020'),
('SUB08', 'STAFF0003'),
('SUB08', 'STAFF0004'),
('SUB09', 'STAFF0001'),
('SUB09', 'STAFF1111');

-- --------------------------------------------------------

--
-- Stand-in structure for view `subject_teacher_view`
-- (See below for the actual view)
--
CREATE TABLE `subject_teacher_view` (
`staff_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`user_role` varchar(30)
,`staff_type` char(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `sub_topic`
--

CREATE TABLE `sub_topic` (
  `subtopic_id` int(11) NOT NULL,
  `subtopic_name` varchar(255) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_topic`
--

INSERT INTO `sub_topic` (`subtopic_id`, `subtopic_name`, `subject_id`, `topic_id`, `class_stream_id`) VALUES
(11, 'Woyooooo', 'SUB05', 30, 'C05EGM'),
(12, 'Jamamaaa', 'SUB05', 30, 'C05EGM'),
(13, 'Hoyoooo', 'SUB05', 31, 'C05EGM'),
(14, 'James', 'SUB05', 31, 'C05EGM'),
(15, 'Hurrey', 'SUB04', 27, 'C05EGM'),
(16, 'Vladimir', 'SUB04', 27, 'C05EGM'),
(17, 'waao', 'SUB07', 32, 'C05PCB'),
(18, 'mwalaye', 'SUB07', 32, 'C05PCB'),
(19, 'meaning and types of democracy', 'SUB01', 34, 'C05EGM'),
(20, 'Slave trade', 'SUB04', 26, 'C05EGM'),
(21, 'nana', 'SUB07', 33, 'C05PCB'),
(22, 'jembe', 'SUB07', 33, 'C05PCB'),
(23, 'pallarel', 'SUB09', 37, 'C05PCB'),
(24, 'series', 'SUB09', 37, 'C05PCB'),
(25, 'radio', 'SUB09', 39, 'C05PCB'),
(26, 'telecom', 'SUB09', 39, 'C05PCB'),
(27, 'wrsef', 'SUB02', 42, 'C05PCB');

--
-- Triggers `sub_topic`
--
DELIMITER $$
CREATE TRIGGER `before_insert_on_subtopic_trigger` BEFORE INSERT ON `sub_topic` FOR EACH ROW BEGIN DECLARE a ENUM('yes','no') DEFAULT 'no'; SELECT approved FROM teaching_log WHERE topic_id=NEW.topic_id AND term_id IN(SELECT term_id FROM term WHERE is_current="yes") LIMIT 1 INTO a; IF a = "yes" THEN SIGNAL SQLSTATE '45000' SET message_text="Cannot add a new subtopic, the Topic is already commented as taught"; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `super_admin_module`
--

CREATE TABLE `super_admin_module` (
  `id` int(1) UNSIGNED NOT NULL,
  `module_id` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin_module`
--

INSERT INTO `super_admin_module` (`id`, `module_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin_permission`
--

CREATE TABLE `super_admin_permission` (
  `id` int(1) UNSIGNED NOT NULL,
  `permission_id` int(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin_permission`
--

INSERT INTO `super_admin_permission` (`id`, `permission_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 11),
(1, 12),
(1, 13),
(1, 15),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 27),
(1, 30),
(1, 31),
(1, 34),
(1, 35),
(1, 36),
(1, 39),
(1, 40),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 48),
(1, 49),
(1, 50),
(1, 55),
(1, 58),
(1, 59),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 73),
(1, 75),
(1, 77),
(1, 78),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 95),
(1, 99),
(1, 100),
(1, 101),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(1, 109),
(1, 111),
(1, 112),
(1, 113),
(1, 114),
(1, 115),
(1, 116),
(1, 117),
(1, 118),
(1, 119),
(1, 120),
(1, 121),
(1, 122),
(1, 125),
(1, 127),
(1, 128),
(1, 131),
(1, 132),
(1, 133),
(1, 134),
(1, 135),
(1, 137),
(1, 138),
(1, 142),
(1, 146),
(1, 147),
(1, 150),
(1, 154),
(1, 155),
(1, 158),
(1, 161),
(1, 165),
(1, 169),
(1, 170),
(1, 172),
(1, 174),
(1, 175),
(1, 177),
(1, 179),
(1, 180),
(1, 181),
(1, 182),
(1, 183),
(1, 184),
(1, 185),
(1, 186),
(1, 189),
(1, 190),
(1, 193),
(1, 194),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(1, 201),
(1, 202),
(1, 203),
(1, 205),
(1, 206),
(1, 207),
(1, 208),
(1, 211),
(1, 212),
(1, 215),
(1, 216),
(1, 217),
(1, 218);

-- --------------------------------------------------------

--
-- Table structure for table `suspended_students`
--

CREATE TABLE `suspended_students` (
  `id` int(5) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `suspension_reason` mediumtext NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suspended_students`
--

INSERT INTO `suspended_students` (`id`, `admission_no`, `suspension_reason`, `from_date`, `to_date`) VALUES
(1, '0987654', 'fighting', '2018-06-05', '2018-06-26'),
(2, '1000127', 'mwizi', '2018-06-06', '2070-01-01'),
(3, '1031456', 'mwizi', '2018-06-07', '2070-01-01');

-- --------------------------------------------------------

--
-- Stand-in structure for view `suspended_students_view`
-- (See below for the actual view)
--
CREATE TABLE `suspended_students_view` (
`admission_no` varchar(10)
,`date` date
,`academic_year` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`dob` date
,`tribe_id` int(5)
,`religion_id` int(5)
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`dorm_id` int(3)
,`status` enum('active','inactive')
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`class_name` varchar(30)
,`stream` varchar(5)
);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`staff_id`) VALUES
('STAFF0001'),
('STAFF0003'),
('STAFF0004'),
('STAFF0013'),
('STAFF0014'),
('STAFF0015'),
('STAFF0016'),
('STAFF0018'),
('STAFF0019'),
('STAFF0020'),
('STAFF0021'),
('STAFF0022'),
('STAFF0023'),
('STAFF0024'),
('STAFF0026'),
('STAFF0027'),
('STAFF0028'),
('STAFF0030'),
('STAFF0031'),
('STAFF0035'),
('STAFF1111'),
('STAFF2020'),
('STAFF2222'),
('STAFF3211'),
('STAFF3333');

-- --------------------------------------------------------

--
-- Stand-in structure for view `teachers_timetable_view`
-- (See below for the actual view)
--
CREATE TABLE `teachers_timetable_view` (
`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`subject_name` varchar(50)
,`class_name` varchar(30)
,`stream` varchar(5)
,`teacher_id` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_duty_roaster`
--

CREATE TABLE `teacher_duty_roaster` (
  `teacher_id` varchar(10) NOT NULL DEFAULT '',
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_duty_roaster`
--

INSERT INTO `teacher_duty_roaster` (`teacher_id`, `did`) VALUES
('STAFF0003', 27),
('STAFF0004', 27),
('STAFF2020', 26),
('STAFF2222', 26);

-- --------------------------------------------------------

--
-- Table structure for table `teaching_assignment`
--

CREATE TABLE `teaching_assignment` (
  `assignment_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `term_id` int(11) NOT NULL,
  `filter_flag` tinyint(1) NOT NULL DEFAULT 0,
  `dept_id` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teaching_assignment`
--

INSERT INTO `teaching_assignment` (`assignment_id`, `teacher_id`, `subject_id`, `class_stream_id`, `start_date`, `end_date`, `term_id`, `filter_flag`, `dept_id`) VALUES
(9, 'STAFF0001', 'SUB02', 'C05PCB', '2018-07-02', '2019-05-31', 2, 1, 22),
(10, 'STAFF3333', 'SUB05', 'C05EGM', '2018-07-02', '2019-05-31', 2, 0, 19),
(11, 'STAFF0001', 'SUB09', 'C05PCB', '2018-06-01', '2018-08-08', 2, 1, 18),
(12, 'STAFF0003', 'SUB07', 'C05PCB', '2018-07-02', '2019-05-31', 2, 1, 20),
(13, 'STAFF0004', 'SUB08', 'C05EGM', '2018-07-02', '2019-05-31', 2, 0, 20),
(14, 'STAFF0035', 'SUB03', 'C05PCB', '2018-07-02', '2019-05-31', 2, 1, 23),
(15, 'STAFF2222', 'SUB01', 'C05PCB', '2018-06-01', '2018-06-08', 2, 1, 21),
(16, 'STAFF3333', 'SUB04', 'C05EGM', '2018-07-01', '2019-05-31', 2, 0, 25),
(17, 'STAFF2222', 'SUB01', 'C05EGM', '2018-07-02', '2019-05-31', 2, 0, 21);

--
-- Triggers `teaching_assignment`
--
DELIMITER $$
CREATE TRIGGER `before_insert_on_teaching_assignment` BEFORE INSERT ON `teaching_assignment` FOR EACH ROW BEGIN DECLARE x INT DEFAULT 0; SELECT COUNT(*) FROM class_stream_subject WHERE subject_id=NEW.subject_id AND class_stream_id=NEW.class_stream_id INTO x;
IF x = 0 THEN
SIGNAL SQLSTATE '45000' SEt message_text="Error, The subject you are attempting to assign is NOT available in the class";
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `teaching_assignment_department_view`
-- (See below for the actual view)
--
CREATE TABLE `teaching_assignment_department_view` (
`subject_name` varchar(50)
,`term_id` int(11)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`gender` varchar(6)
,`staff_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`hod_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
,`assignment_id` int(11)
,`teacher_id` varchar(10)
,`subject_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`filter_flag` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `teaching_attendance`
--

CREATE TABLE `teaching_attendance` (
  `ta_id` bigint(20) NOT NULL,
  `t_id` int(5) UNSIGNED DEFAULT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `status` enum('taught','untaught','none') NOT NULL,
  `subTopicID` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `no_of_absentees` int(2) NOT NULL,
  `period_no` int(2) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teaching_log`
--

CREATE TABLE `teaching_log` (
  `log_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `dept_id` int(3) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `approved` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teaching_log`
--

INSERT INTO `teaching_log` (`log_id`, `teacher_id`, `subject_id`, `topic_id`, `class_stream_id`, `dept_id`, `start_date`, `end_date`, `term_id`, `comment`, `approved`) VALUES
(1, 'STAFF3333', 'SUB04', 27, 'C05EGM', 25, '2018-06-07', '2018-06-27', 2, 'Holla', 'no'),
(2, 'STAFF3333', 'SUB05', 31, 'C05EGM', 19, '2018-06-28', '2018-06-29', 2, 'Finitoo', 'yes'),
(3, 'STAFF2222', 'SUB01', 34, 'C05EGM', 21, '2018-06-05', '2018-06-05', 2, 'prepare for examination', 'no'),
(4, 'STAFF0003', 'SUB07', 32, 'C05PCB', 20, '2018-07-03', '2018-08-08', 2, 'WELL COVERED', 'no'),
(5, 'STAFF3333', 'SUB04', 26, 'C05EGM', 25, '2018-06-06', '2018-06-15', 2, 'well taught and understood', 'no'),
(6, 'STAFF0003', 'SUB07', 33, 'C05PCB', 20, '2018-08-02', '2018-09-06', 2, 'NOT TAUGHT', 'no'),
(7, 'STAFF1111', 'SUB09', 40, 'C05PCB', 18, '2018-07-02', NULL, 2, '', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `teaching_log_view`
--

CREATE TABLE `teaching_log_view` (
  `subject_id` varchar(10) DEFAULT NULL,
  `subject_name` varchar(50) DEFAULT NULL,
  `subject_category` enum('T','N') DEFAULT NULL,
  `subject_choice` enum('optional','compulsory') DEFAULT NULL,
  `dept_id` int(3) UNSIGNED DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `topic_name` varchar(100) DEFAULT NULL,
  `log_id` int(11) DEFAULT NULL,
  `subtopic_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `subtopic_name` varchar(255) DEFAULT NULL,
  `class_stream_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `term`
--

CREATE TABLE `term` (
  `term_id` int(11) NOT NULL,
  `aid` int(5) NOT NULL,
  `term_name` enum('first_term','second_term') NOT NULL DEFAULT 'first_term',
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_current` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `term`
--

INSERT INTO `term` (`term_id`, `aid`, `term_name`, `begin_date`, `end_date`, `is_current`) VALUES
(1, 1, 'first_term', '2018-01-08', '2018-06-08', 'no'),
(2, 3, 'first_term', '2018-07-01', '2018-12-31', 'yes'),
(3, 1, 'second_term', '2018-07-01', '2018-12-31', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `timetable`
--

CREATE TABLE `timetable` (
  `t_id` int(5) UNSIGNED NOT NULL,
  `period_no` int(2) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timetable`
--

INSERT INTO `timetable` (`t_id`, `period_no`, `assignment_id`, `teacher_id`, `class_stream_id`, `weekday`) VALUES
(51, 1, 10, 'STAFF3333', 'C05EGM', 'Monday'),
(52, 2, 10, 'STAFF3333', 'C05EGM', 'Monday'),
(53, 3, 13, 'STAFF0004', 'C05EGM', 'Monday'),
(54, 4, 13, 'STAFF0004', 'C05EGM', 'Monday'),
(55, 5, -1, 'T-C05EGM', 'C05EGM', 'Monday'),
(56, 6, -1, 'T-C05EGM', 'C05EGM', 'Monday'),
(57, 7, 16, 'STAFF3333', 'C05EGM', 'Monday'),
(58, 8, 16, 'STAFF3333', 'C05EGM', 'Monday'),
(59, 9, -1, 'T-C05EGM', 'C05EGM', 'Monday'),
(60, 10, -1, 'T-C05EGM', 'C05EGM', 'Monday'),
(61, 1, 17, 'STAFF2222', 'C05EGM', 'Tuesday'),
(62, 2, 17, 'STAFF2222', 'C05EGM', 'Tuesday'),
(63, 3, 13, 'STAFF0004', 'C05EGM', 'Tuesday'),
(64, 4, 10, 'STAFF3333', 'C05EGM', 'Tuesday'),
(65, 5, 13, 'STAFF0004', 'C05EGM', 'Tuesday'),
(66, 6, -1, 'T-C05EGM', 'C05EGM', 'Tuesday'),
(67, 7, -1, 'T-C05EGM', 'C05EGM', 'Tuesday'),
(68, 8, -1, 'T-C05EGM', 'C05EGM', 'Tuesday'),
(69, 9, 16, 'STAFF3333', 'C05EGM', 'Tuesday'),
(70, 10, -1, 'T-C05EGM', 'C05EGM', 'Tuesday'),
(71, 1, 13, 'STAFF0004', 'C05EGM', 'Wednesday'),
(72, 2, 13, 'STAFF0004', 'C05EGM', 'Wednesday'),
(73, 3, -1, 'T-C05EGM', 'C05EGM', 'Wednesday'),
(74, 4, 17, 'STAFF2222', 'C05EGM', 'Wednesday'),
(75, 5, -1, 'T-C05EGM', 'C05EGM', 'Wednesday'),
(76, 6, -1, 'T-C05EGM', 'C05EGM', 'Wednesday'),
(77, 7, 16, 'STAFF3333', 'C05EGM', 'Wednesday'),
(78, 8, 16, 'STAFF3333', 'C05EGM', 'Wednesday'),
(79, 9, 10, 'STAFF3333', 'C05EGM', 'Wednesday'),
(80, 10, 10, 'STAFF3333', 'C05EGM', 'Wednesday'),
(81, 1, 13, 'STAFF0004', 'C05EGM', 'Thursday'),
(82, 2, 13, 'STAFF0004', 'C05EGM', 'Thursday'),
(83, 3, 13, 'STAFF0004', 'C05EGM', 'Thursday'),
(84, 4, 10, 'STAFF3333', 'C05EGM', 'Thursday'),
(85, 5, 10, 'STAFF3333', 'C05EGM', 'Thursday'),
(86, 6, -1, 'T-C05EGM', 'C05EGM', 'Thursday'),
(87, 7, 16, 'STAFF3333', 'C05EGM', 'Thursday'),
(88, 8, 16, 'STAFF3333', 'C05EGM', 'Thursday'),
(89, 9, -1, 'T-C05EGM', 'C05EGM', 'Thursday'),
(90, 10, 10, 'STAFF3333', 'C05EGM', 'Thursday'),
(91, 1, 17, 'STAFF2222', 'C05EGM', 'Friday'),
(92, 2, 10, 'STAFF3333', 'C05EGM', 'Friday'),
(93, 3, 10, 'STAFF3333', 'C05EGM', 'Friday'),
(94, 4, 16, 'STAFF3333', 'C05EGM', 'Friday'),
(95, 5, 16, 'STAFF3333', 'C05EGM', 'Friday'),
(96, 6, 13, 'STAFF0004', 'C05EGM', 'Friday'),
(97, 7, 13, 'STAFF0004', 'C05EGM', 'Friday'),
(98, 8, -1, 'T-C05EGM', 'C05EGM', 'Friday'),
(99, 9, -1, 'T-C05EGM', 'C05EGM', 'Friday'),
(100, 10, 17, 'STAFF2222', 'C05EGM', 'Friday'),
(101, 1, -1, 'T-C05PCB', 'C05PCB', 'Monday'),
(102, 2, 12, 'STAFF0003', 'C05PCB', 'Monday'),
(103, 3, 11, 'STAFF1111', 'C05PCB', 'Monday'),
(104, 4, 15, 'STAFF2222', 'C05PCB', 'Monday'),
(105, 5, 12, 'STAFF0003', 'C05PCB', 'Monday'),
(106, 6, 12, 'STAFF0003', 'C05PCB', 'Monday'),
(107, 7, 14, 'STAFF0035', 'C05PCB', 'Monday'),
(108, 8, 14, 'STAFF0035', 'C05PCB', 'Monday'),
(109, 9, -1, 'T-C05PCB', 'C05PCB', 'Monday'),
(110, 10, -1, 'T-C05PCB', 'C05PCB', 'Monday'),
(111, 1, 14, 'STAFF0035', 'C05PCB', 'Tuesday'),
(112, 2, 9, 'STAFF0001', 'C05PCB', 'Tuesday'),
(113, 3, 12, 'STAFF0003', 'C05PCB', 'Tuesday'),
(114, 4, 9, 'STAFF0001', 'C05PCB', 'Tuesday'),
(115, 5, 11, 'STAFF1111', 'C05PCB', 'Tuesday'),
(116, 6, 11, 'STAFF1111', 'C05PCB', 'Tuesday'),
(117, 7, 9, 'STAFF0001', 'C05PCB', 'Tuesday'),
(118, 8, 9, 'STAFF0001', 'C05PCB', 'Tuesday'),
(119, 9, 14, 'STAFF0035', 'C05PCB', 'Tuesday'),
(120, 10, 14, 'STAFF0035', 'C05PCB', 'Tuesday'),
(121, 1, 15, 'STAFF2222', 'C05PCB', 'Wednesday'),
(122, 2, 11, 'STAFF1111', 'C05PCB', 'Wednesday'),
(123, 3, 14, 'STAFF0035', 'C05PCB', 'Wednesday'),
(124, 4, 14, 'STAFF0035', 'C05PCB', 'Wednesday'),
(125, 5, 9, 'STAFF0001', 'C05PCB', 'Wednesday'),
(126, 6, 9, 'STAFF0001', 'C05PCB', 'Wednesday'),
(127, 7, 12, 'STAFF0003', 'C05PCB', 'Wednesday'),
(128, 8, 12, 'STAFF0003', 'C05PCB', 'Wednesday'),
(129, 9, 11, 'STAFF1111', 'C05PCB', 'Wednesday'),
(130, 10, 11, 'STAFF1111', 'C05PCB', 'Wednesday'),
(131, 1, 11, 'STAFF1111', 'C05PCB', 'Thursday'),
(132, 2, 14, 'STAFF0035', 'C05PCB', 'Thursday'),
(133, 3, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(134, 4, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(135, 5, 14, 'STAFF0035', 'C05PCB', 'Thursday'),
(136, 6, 14, 'STAFF0035', 'C05PCB', 'Thursday'),
(137, 7, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(138, 8, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(139, 9, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(140, 10, 9, 'STAFF0001', 'C05PCB', 'Thursday'),
(141, 1, 9, 'STAFF0001', 'C05PCB', 'Friday'),
(142, 2, 15, 'STAFF2222', 'C05PCB', 'Friday'),
(143, 3, 11, 'STAFF1111', 'C05PCB', 'Friday'),
(144, 4, 9, 'STAFF0001', 'C05PCB', 'Friday'),
(145, 5, 15, 'STAFF2222', 'C05PCB', 'Friday'),
(146, 6, 15, 'STAFF2222', 'C05PCB', 'Friday'),
(147, 7, 11, 'STAFF1111', 'C05PCB', 'Friday'),
(148, 8, 11, 'STAFF1111', 'C05PCB', 'Friday'),
(149, 9, 12, 'STAFF0003', 'C05PCB', 'Friday'),
(150, 10, 12, 'STAFF0003', 'C05PCB', 'Friday');

-- --------------------------------------------------------

--
-- Stand-in structure for view `timetable_view`
-- (See below for the actual view)
--
CREATE TABLE `timetable_view` (
`teacher_id` varchar(10)
,`class_stream_id` varchar(10)
,`csid` varchar(28)
,`start_date` date
,`end_date` date
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`assignment_id` int(11)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`t_id` int(5) unsigned
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`duration` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `title_sum_up_view`
-- (See below for the actual view)
--
CREATE TABLE `title_sum_up_view` (
`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`COPIES` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `tod_daily_routine_tbl`
--

CREATE TABLE `tod_daily_routine_tbl` (
  `date_` date NOT NULL,
  `wake_up_time` varchar(20) NOT NULL,
  `parade_time` varchar(20) NOT NULL,
  `start_classes_time` varchar(20) NOT NULL,
  `breakfast` varchar(100) DEFAULT NULL,
  `lunch` varchar(100) DEFAULT NULL,
  `dinner` varchar(100) DEFAULT NULL,
  `events` longtext DEFAULT NULL,
  `security` longtext DEFAULT NULL,
  `comments` longtext DEFAULT NULL,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tod_daily_routine_tbl`
--

INSERT INTO `tod_daily_routine_tbl` (`date_`, `wake_up_time`, `parade_time`, `start_classes_time`, `breakfast`, `lunch`, `dinner`, `events`, `security`, `comments`, `did`) VALUES
('2018-06-03', '05:40:00', '06:50:00', '07:30:00', 'poridge buns', 'kande', 'ugali beans', 'visit from pm', 'safe', 'Well done!', 26);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `topic_id` int(11) NOT NULL,
  `topic_name` varchar(100) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topic_id`, `topic_name`, `subject_id`, `class_stream_id`) VALUES
(26, 'Fundamental of Java Programming', 'SUB04', 'C05EGM'),
(27, 'Introduction to Networking', 'SUB04', 'C05EGM'),
(28, 'Loops', 'SUB04', 'C05EGM'),
(29, 'Security', 'SUB05', 'C05EGM'),
(30, 'Gender', 'SUB05', 'C05EGM'),
(31, 'Hawa', 'SUB05', 'C05EGM'),
(32, 'Hahahha', 'SUB07', 'C05PCB'),
(33, 'Oyooooo', 'SUB07', 'C05PCB'),
(34, 'democracy', 'SUB01', 'C05EGM'),
(35, 'Magnetic', 'SUB09', 'C05PCB'),
(36, 'Induction', 'SUB09', 'C05PCB'),
(37, 'Electricity', 'SUB09', 'C05PCB'),
(38, 'Mechanical', 'SUB09', 'C05PCB'),
(39, 'Electronic', 'SUB09', 'C05PCB'),
(40, 'radiation', 'SUB09', 'C05PCB'),
(41, 'Sdffhj', 'SUB02', 'C05PCB'),
(42, 'dsygh', 'SUB02', 'C05PCB');

-- --------------------------------------------------------

--
-- Table structure for table `transferred_students`
--

CREATE TABLE `transferred_students` (
  `id` int(5) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `transfer_status` enum('IN','OUT') NOT NULL,
  `school` varchar(255) NOT NULL,
  `form` varchar(10) NOT NULL,
  `date_of_transfer` date NOT NULL,
  `transfer_letter` enum('Available','NOT Available') NOT NULL DEFAULT 'Available',
  `self_form_receipt` enum('Received','NOT Received') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transferred_students`
--

INSERT INTO `transferred_students` (`id`, `admission_no`, `transfer_status`, `school`, `form`, `date_of_transfer`, `transfer_letter`, `self_form_receipt`) VALUES
(3, '5552121', 'IN', 'Respect Secondary School', 'C05', '2018-05-30', 'Available', 'Received'),
(11, '1000003', 'IN', 'Katoro primary school', 'C01', '2018-05-30', 'Available', 'Received'),
(16, '5552231', 'IN', 'Nkurumah Secondary School', 'C05', '2018-05-30', 'Available', 'Received'),
(23, '6743321', 'IN', 'Aljazeera Secondary School', 'C05', '2018-05-31', 'Available', 'Received'),
(24, '1000012', 'OUT', 'Nyakabungo Sec', 'C05', '2018-05-31', 'Available', 'Received'),
(25, '1000000', 'IN', 'Kibaigwa primary school', 'C01', '2018-05-31', 'Available', 'Received'),
(27, '5552231', 'OUT', 'Nyakahoja Secondary School', 'C05', '2018-05-31', 'Available', 'Received'),
(28, '1234567', 'IN', 'Azania Secondary School', 'C05', '2018-06-04', 'Available', 'Received'),
(29, '5555555', 'IN', 'Nyasaka', 'C05', '2018-06-05', 'Available', 'Received');

-- --------------------------------------------------------

--
-- Table structure for table `tribe`
--

CREATE TABLE `tribe` (
  `tribe_name` varchar(100) NOT NULL,
  `tribe_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tribe`
--

INSERT INTO `tribe` (`tribe_name`, `tribe_id`) VALUES
('Akiek', 1),
('Akie Northern Tanzania', 2),
('Arusha', 3),
('Assa', 4),
('Barabaig', 5),
('Balouch Coastal Tanzania', 6),
('Bembe', 7),
('Bena', 8),
('Bende', 9),
('Bondei', 10),
('Bungu', 11),
('Burunge', 12),
('Chaga', 13),
('Datooga', 14),
('Dhaiso', 15),
('Digo', 16),
('Doe', 17),
('Fipa', 18),
('Gogo', 19),
('Goa Coastal Tanzania', 20),
('Gorowa', 21),
('Gujirati Coastal Tanzania', 22),
('Gweno', 23),
('Ha', 24),
('Hadza', 26),
('Hangaza', 27),
('Haya', 28),
('Hehe', 29),
('Holoholo people', 30),
('Ikizu', 31),
('Ikoma', 32),
('Iraqw', 33),
('Isanzu', 34),
('Jiji', 35),
('Jita', 36),
('Kabwa', 37),
('Kagura', 38),
('Kaguru', 39),
('Kahe', 40),
('Kami', 41),
('Kamba Northern Tanzania', 42),
('Kara (also called Regi)', 43),
('Kerewe', 44),
('Kikuyu', 45),
('Kimbu', 46),
('Kinga', 47),
('Kisankasa', 48),
('Kisi', 49),
('Konongo', 50),
('Kuria', 51),
('Kutu', 52),
('Kwâ€™adza', 53),
('Kwavi', 54),
('Kwaya', 55),
('Kwere', 56),
('Kwifa', 57),
('Lagwa', 58),
('Lambya', 59),
('Luguru', 60),
('Luo', 61),
('Maasai', 62),
('Machinga', 63),
('Magoma', 64),
('Mbulu (in Arusha)', 65),
('Makonde', 66),
('Makua', 67),
('Makwe', 68),
('Malila', 69),
('Mambwe', 70),
('Manyema', 71),
('Manda', 72),
('Mahara', 73),
('Mediak', 74),
('Matengo', 75),
('Matumbi', 76),
('Maviha', 77),
('Mbugwe', 78),
('Mbunga', 79),
('Mbugu', 80),
('Mosiro', 82),
('Mpoto', 83),
('Msur Zanzibar', 84),
('Mwanga', 85),
('Mwera', 86),
('Ndali', 87),
('Ndamba', 88),
('Ndendeule', 89),
('Ndengereko', 90),
('Ndonde', 91),
('Ngas Northern Tanzania', 93),
('Ngasa', 94),
('Ngindo', 95),
('Ngoni', 96),
('Ngulu', 97),
('Ngazija (Zanzibar island)', 98),
('Ngurimi', 99),
('Ngwele', 100),
('Nilamba', 101),
('Nindi', 102),
('Nyakyusa', 103),
('Nyasa people in Mbeya', 104),
('Nyambo', 105),
('Nyamwanga', 106),
('Nyamwezi', 107),
('Nyanyembe', 108),
('Nyaturu', 109),
('Nyiha', 110),
('Nyiramba', 111),
('Omotik', 112),
('Okiek people', 113),
('Pangwa', 114),
('Pare', 115),
('Pimbwe', 116),
('Pogolo', 117),
('Rangi', 118),
('Rufiji', 119),
('Rungi', 120),
('Rungu', 121),
('Rungwa', 122),
('Rwa', 123),
('Safwa', 124),
('Sagara', 125),
('Sandawe', 126),
('Sangu', 127),
('Segeju', 128),
('Swengwear', 129),
('Shambaa', 130),
('Shirazi', 131),
('Shubi', 132),
('Sizaki', 133),
('Suba', 134),
('Sukuma', 135),
('Sumbwa', 136),
('Sungu Tanga', 137),
('Swahili', 138),
('Temi', 139),
('Tongwe', 140),
('Twa Western Tanzania', 141),
('Tutsi Western Tanzania', 142),
('Tumbuka', 143),
('Vidunda', 144),
('Vinza', 145),
('Wanda', 146),
('Washihiri Zanzibar', 147),
('Wanji', 149),
('Wangarenaro Arusha', 150),
('Ware', 151),
('Yao', 153),
('Zanaki', 154),
('Zaramo', 155),
('Zigula', 156),
('Zinza', 157),
('Zyoba', 158);

-- --------------------------------------------------------

--
-- Stand-in structure for view `used_to_edit_timetable_view`
-- (See below for the actual view)
--
CREATE TABLE `used_to_edit_timetable_view` (
`teacher_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`assignment_id` int(11)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`t_id` int(5) unsigned
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`duration` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `users`
-- (See below for the actual view)
--
CREATE TABLE `users` (
`id` varchar(10)
,`names` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `waliomaliza_tbl`
--

CREATE TABLE `waliomaliza_tbl` (
  `id` int(11) NOT NULL,
  `index_no` varchar(30) DEFAULT NULL,
  `slip_no` varchar(30) DEFAULT NULL,
  `admission_no` varchar(10) NOT NULL,
  `division` varchar(2) DEFAULT NULL,
  `points` int(2) DEFAULT NULL,
  `kashachukua_cheti` enum('ndiyo','hapana') DEFAULT 'hapana',
  `tarehe_ya_kuchukua` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `completion_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `academic_year_term_view`
--
DROP TABLE IF EXISTS `academic_year_term_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `academic_year_term_view`  AS  select `term`.`term_id` AS `term_id`,`term`.`is_current` AS `is_current`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`term`.`term_name` AS `term_name`,`term`.`begin_date` AS `term_begin_date`,`term`.`end_date` AS `term_end_date` from (`academic_year` join `term` on(`academic_year`.`id` = `term`.`aid`)) where `academic_year`.`status` = 'current_academic_year' ;

-- --------------------------------------------------------

--
-- Structure for view `active_students_view`
--
DROP TABLE IF EXISTS `active_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `active_students_view`  AS  select `student`.`stte_out` AS `stte_out`,`student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from (((`student` join `class_level` on(`student`.`class_id` = `class_level`.`class_id`)) left join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `academic_year` on(`student`.`academic_year` = `academic_year`.`id`)) where `student`.`status` = 'active' and (`student`.`stte` = 'transferred_in' or `student`.`stte` = 'selected') ;

-- --------------------------------------------------------

--
-- Structure for view `activity_log_view`
--
DROP TABLE IF EXISTS `activity_log_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `activity_log_view`  AS  select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `username` from (`activity_log` join `staff` on(`staff`.`staff_id` = `activity_log`.`user_id`)) union select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,`admin`.`username` AS `username` from (`activity_log` join `admin` on(`admin`.`id` = `activity_log`.`user_id`)) order by `time` desc ;

-- --------------------------------------------------------

--
-- Structure for view `all_announcements_view`
--
DROP TABLE IF EXISTS `all_announcements_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_announcements_view`  AS  select `announcement`.`group_id` AS `group_id`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`group_type`.`group_name` AS `group_name`,`group_type`.`description` AS `description` from (`announcement` join `group_type` on(`announcement`.`group_id` = `group_type`.`group_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `all_class_streams_view`
--
DROP TABLE IF EXISTS `all_class_streams_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_class_streams_view`  AS  select `class_stream`.`teacher_id` AS `teacher_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `teacher_names`,`academic_year`.`id` AS `id`,`academic_year`.`year` AS `year`,`class_level`.`class_id` AS `class_id`,`class_stream`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`class_level`.`level` AS `level`,(select count(`student`.`admission_no`) from `student` where `student`.`status` = 'active' and `student`.`class_stream_id` = `class_stream`.`class_stream_id`) AS `enrolled`,`class_stream`.`capacity` AS `capacity`,`class_stream`.`description` AS `description`,`class_stream`.`id_display` AS `id_display` from ((((`class_level` join `class_stream` on(`class_level`.`class_id` = `class_stream`.`class_id`)) left join `student` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `academic_year` on(`class_level`.`level` = `academic_year`.`class_level`)) left join `staff` on(`staff`.`staff_id` = `class_stream`.`teacher_id`)) where `academic_year`.`status` = 'current_academic_year' group by `class_level`.`class_name`,`class_stream`.`stream` order by `class_stream`.`class_id`,`class_stream`.`class_stream_id` ;

-- --------------------------------------------------------

--
-- Structure for view `all_results_view`
--
DROP TABLE IF EXISTS `all_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_results_view`  AS  select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`class_level`.`class_name` AS `class_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`term_id` AS `term_id`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`term`.`aid` AS `aid`,`term`.`term_name` AS `term_name` from ((((`term` join `student_subject_score_position` on(`term`.`term_id` = `student_subject_score_position`.`term_id`)) join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) join `student` on(`student`.`admission_no` = `student_subject_score_position`.`admission_no`)) join `class_level` on(`class_level`.`class_id` = `student_subject_score_position`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `all_teachers_view`
--
DROP TABLE IF EXISTS `all_teachers_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_teachers_view`  AS  select `staff`.`staff_id` AS `staff_id`,`subject_teacher`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`user_role` AS `user_role`,`staff`.`staff_type` AS `staff_type` from (`staff` left join (`subject` join `subject_teacher` on(`subject_teacher`.`subject_id` = `subject`.`subject_id`)) on(`subject_teacher`.`teacher_id` = `staff`.`staff_id`)) where `staff`.`staff_type` = 'T' ;

-- --------------------------------------------------------

--
-- Structure for view `assigned_user_roles`
--
DROP TABLE IF EXISTS `assigned_user_roles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assigned_user_roles`  AS  select `role_type`.`role_name` AS `role_name`,`role_type`.`description` AS `description`,`staff_role`.`role_type_id` AS `role_type_id`,`staff_role`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`role_type`.`active` AS `active` from ((`staff_role` join `role_type` on(`staff_role`.`role_type_id` = `role_type`.`role_type_id`)) join `staff` on(`staff`.`staff_id` = `staff_role`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `assign_hod_class_teacher_view`
--
DROP TABLE IF EXISTS `assign_hod_class_teacher_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assign_hod_class_teacher_view`  AS  select `staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type` from (`staff` join `teacher` on(`staff`.`staff_id` = `teacher`.`staff_id`)) where `staff`.`status` = 'active' ;

-- --------------------------------------------------------

--
-- Structure for view `audit_staff_attendance_view`
--
DROP TABLE IF EXISTS `audit_staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_staff_attendance_view`  AS  select `first_join_staff_attendance_view`.`firstname` AS `firstname`,`first_join_staff_attendance_view`.`lastname` AS `lastname`,`first_join_staff_attendance_view`.`staff_id` AS `staff_id`,`first_join_staff_attendance_view`.`date_` AS `date_`,`first_join_staff_attendance_view`.`day` AS `day`,`first_join_staff_attendance_view`.`at` AS `at`,`first_join_staff_attendance_view`.`att_desc` AS `att_desc`,`first_join_staff_attendance_view`.`change_type` AS `change_type`,`first_join_staff_attendance_view`.`change_by` AS `change_by`,`first_join_staff_attendance_view`.`ip_address` AS `ip_address`,`first_join_staff_attendance_view`.`change_time` AS `change_time`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from (`staff` join `first_join_staff_attendance_view` on(`staff`.`staff_id` = convert(`first_join_staff_attendance_view`.`change_by` using utf8))) union select `first_join_staff_attendance_view`.`firstname` AS `firstname`,`first_join_staff_attendance_view`.`lastname` AS `lastname`,`first_join_staff_attendance_view`.`staff_id` AS `staff_id`,`first_join_staff_attendance_view`.`date_` AS `date_`,`first_join_staff_attendance_view`.`day` AS `day`,`first_join_staff_attendance_view`.`at` AS `at`,`first_join_staff_attendance_view`.`att_desc` AS `att_desc`,`first_join_staff_attendance_view`.`change_type` AS `change_type`,`first_join_staff_attendance_view`.`change_by` AS `change_by`,`first_join_staff_attendance_view`.`ip_address` AS `ip_address`,`first_join_staff_attendance_view`.`change_time` AS `change_time`,`admin`.`username` AS `username` from (`admin` join `first_join_staff_attendance_view` on(`admin`.`id` = `first_join_staff_attendance_view`.`change_by`)) order by `staff_id`,`change_time` desc ;

-- --------------------------------------------------------

--
-- Structure for view `audit_student_attendance_view`
--
DROP TABLE IF EXISTS `audit_student_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_student_attendance_view`  AS  select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_attendance_record`.`admission_no` AS `admission_no`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`class_stream_id` AS `csid`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_student_attendance`.`change_type` AS `change_type`,`audit_student_attendance`.`change_by` AS `change_by`,`audit_student_attendance`.`ip_address` AS `ip_address`,`audit_student_attendance`.`change_time` AS `change_time`,`class_level`.`class_name` AS `class_name`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from ((((((`student_attendance_record` join `student` on(`student_attendance_record`.`admission_no` = `student`.`admission_no`)) join `audit_student_attendance` on(`student_attendance_record`.`id` = `audit_student_attendance`.`sar_id`)) join `attendance` on(`audit_student_attendance`.`att_id` = `attendance`.`att_id`)) join `class_stream` on(`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_stream`.`class_id` = `class_level`.`class_id`)) join `staff` on(`audit_student_attendance`.`change_by` = `staff`.`staff_id`)) union select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_attendance_record`.`admission_no` AS `admission_no`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`class_stream_id` AS `csid`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_student_attendance`.`change_type` AS `change_type`,`audit_student_attendance`.`change_by` AS `change_by`,`audit_student_attendance`.`ip_address` AS `ip_address`,`audit_student_attendance`.`change_time` AS `change_time`,`class_level`.`class_name` AS `class_name`,`admin`.`username` AS `added_changed_by` from ((((((`student_attendance_record` join `student` on(`student_attendance_record`.`admission_no` = `student`.`admission_no`)) join `audit_student_attendance` on(`student_attendance_record`.`id` = `audit_student_attendance`.`sar_id`)) join `attendance` on(`audit_student_attendance`.`att_id` = `attendance`.`att_id`)) join `class_stream` on(`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_stream`.`class_id` = `class_level`.`class_id`)) join `admin` on(`audit_student_attendance`.`change_by` = `admin`.`id`)) order by `admission_no`,`change_time` desc ;

-- --------------------------------------------------------

--
-- Structure for view `audit_student_results_view`
--
DROP TABLE IF EXISTS `audit_student_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_student_results_view`  AS  select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`subject`.`subject_name` AS `subject_name`,`exam_type`.`exam_name` AS `exam_name`,`audit_student_subject_assessment`.`marks` AS `marks`,`class_level`.`class_name` AS `class_name`,`student_subject_assessment`.`e_date` AS `e_date`,`term`.`term_name` AS `term_name`,`academic_year`.`year` AS `year`,`audit_student_subject_assessment`.`change_type` AS `change_type`,`audit_student_subject_assessment`.`ip_address` AS `ip_address`,`audit_student_subject_assessment`.`change_time` AS `change_time`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from (((((((((`student` join `student_subject_assessment` on(`student`.`admission_no` = `student_subject_assessment`.`admission_no`)) join `audit_student_subject_assessment` on(`student_subject_assessment`.`ssa_id` = `audit_student_subject_assessment`.`ssa_id`)) join `subject` on(`student_subject_assessment`.`subject_id` = `subject`.`subject_id`)) join `exam_type` on(`student_subject_assessment`.`etype_id` = `exam_type`.`id`)) join `class_stream` on(`student_subject_assessment`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) join `term` on(`student_subject_assessment`.`term_id` = `term`.`term_id`)) join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) join `staff` on(`staff`.`staff_id` = `audit_student_subject_assessment`.`change_by`)) union select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`subject`.`subject_name` AS `subject_name`,`exam_type`.`exam_name` AS `exam_name`,`audit_student_subject_assessment`.`marks` AS `marks`,`class_level`.`class_name` AS `class_name`,`student_subject_assessment`.`e_date` AS `e_date`,`term`.`term_name` AS `term_name`,`academic_year`.`year` AS `year`,`audit_student_subject_assessment`.`change_type` AS `change_type`,`audit_student_subject_assessment`.`ip_address` AS `ip_address`,`audit_student_subject_assessment`.`change_time` AS `change_time`,`admin`.`username` AS `added_changed_by` from (((((((((`student` join `student_subject_assessment` on(`student`.`admission_no` = `student_subject_assessment`.`admission_no`)) join `audit_student_subject_assessment` on(`student_subject_assessment`.`ssa_id` = `audit_student_subject_assessment`.`ssa_id`)) join `subject` on(`student_subject_assessment`.`subject_id` = `subject`.`subject_id`)) join `exam_type` on(`student_subject_assessment`.`etype_id` = `exam_type`.`id`)) join `class_stream` on(`student_subject_assessment`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) join `term` on(`student_subject_assessment`.`term_id` = `term`.`term_id`)) join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) join `admin` on(`admin`.`id` = `audit_student_subject_assessment`.`change_by`)) order by `student_names`,`change_time` desc ;

-- --------------------------------------------------------

--
-- Structure for view `available_books_view`
--
DROP TABLE IF EXISTS `available_books_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `available_books_view`  AS  select distinct `borrowable_view`.`ISBN` AS `ISBN`,`borrowable_view`.`ID` AS `ID`,`borrowable_view`.`BOOK_TITLE` AS `BOOK_TITLE` from `borrowable_view` where !((`borrowable_view`.`ISBN`,`borrowable_view`.`ID`) in (select `lost_book_view`.`ISBN`,`lost_book_view`.`ID` from `lost_book_view`)) ;

-- --------------------------------------------------------

--
-- Structure for view `book_copy_report`
--
DROP TABLE IF EXISTS `book_copy_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `book_copy_report`  AS  select `book_type`.`ISBN` AS `ISBN`,`book`.`ID` AS `CODE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`EDITION` AS `EDITION`,`book`.`STATUS` AS `STATUS`,`book`.`LAST_EDIT` AS `LAST_EDIT` from (`book` join `book_type`) where `book_type`.`ISBN` = `book`.`isbn` order by `book`.`isbn`,`book`.`ID` ;

-- --------------------------------------------------------

--
-- Structure for view `book_type_report`
--
DROP TABLE IF EXISTS `book_type_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `book_type_report`  AS  select `book_type`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`EDITION` AS `EDITION`,`book_type`.`LAST_EDIT` AS `LAST_EDIT`,(select count(`book`.`ID`) from `book` where `book`.`isbn` = `book_type`.`ISBN`) AS `COPIES` from `book_type` order by `book_type`.`LAST_EDIT` desc ;

-- --------------------------------------------------------

--
-- Structure for view `borrowable_view`
--
DROP TABLE IF EXISTS `borrowable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowable_view`  AS  select `remove_borrowed_view`.`ID` AS `ID`,`remove_borrowed_view`.`ISBN` AS `ISBN`,`remove_borrowed_view`.`STATUS` AS `STATUS`,`remove_borrowed_view`.`LAST_EDIT` AS `LAST_EDIT`,`remove_borrowed_view`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`remove_borrowed_view`.`BOOK_TITLE` AS `BOOK_TITLE`,`remove_borrowed_view`.`EDITION` AS `EDITION` from `remove_borrowed_view` where !((`remove_borrowed_view`.`ID`,`remove_borrowed_view`.`ISBN`) in (select `book`.`ID`,`book`.`isbn` from `book` where `book`.`STATUS` = 'lost')) ;

-- --------------------------------------------------------

--
-- Structure for view `borrowed_report`
--
DROP TABLE IF EXISTS `borrowed_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowed_report`  AS  select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,timestampdiff(DAY,current_timestamp(),`book_loan_info`.`RETURN_DATE`) AS `TIME_LEFT`,`book_type`.`EDITION` AS `EDITION`,`borrowers`.`borrower_description` AS `BORROWER_DESCRIPTION`,`borrowers`.`borrower_id` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`status_before` AS `STATUS_BEFORE`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `borrowers`) where `book_type`.`ISBN` = `book_loan_info`.`ISBN` and `book_loan_info`.`BORROWER_ID` = convert(`borrowers`.`borrower_id` using utf8) and `book_loan_info`.`FLAG` = 'BORROWED' order by timestampdiff(DAY,current_timestamp(),`book_loan_info`.`RETURN_DATE`) ;

-- --------------------------------------------------------

--
-- Structure for view `borrowed_report_view`
--
DROP TABLE IF EXISTS `borrowed_report_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowed_report_view`  AS  select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,timestampdiff(DAY,current_timestamp(),`book_loan_info`.`RETURN_DATE`) AS `TIME_LEFT`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`EDITION` AS `EDITION`,`users`.`names` AS `NAMES`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `users`) where `book_loan_info`.`ISBN` = `book_type`.`ISBN` and `book_loan_info`.`BORROWER_ID` = convert(`users`.`id` using utf8) and `book_loan_info`.`FLAG` = 'borrowed' order by timestampdiff(DAY,current_timestamp(),`book_loan_info`.`RETURN_DATE`) ;

-- --------------------------------------------------------

--
-- Structure for view `borrowers`
--
DROP TABLE IF EXISTS `borrowers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowers`  AS  select `student`.`admission_no` AS `borrower_id`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `borrower_description`,`student`.`b_type` AS `b_type` from `student` where `student`.`status` = 'active' and (`student`.`stte_out` <> 'suspended' or `student`.`stte_out` is null) union select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `concat(firstname," ",lastname)`,`staff`.`b_type` AS `b_type` from `staff` where `staff`.`status` = 'active' union select `department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`b_type` AS `b_type` from `department` ;

-- --------------------------------------------------------

--
-- Structure for view `borrowing_report`
--
DROP TABLE IF EXISTS `borrowing_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowing_report`  AS  select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`DATE_DUE_BACK` AS `DATE_DUE_BACK`,`book_loan_info`.`DATE_OVERDUE` AS `DATE_OVERDUE`,`book_loan_info`.`status_before` AS `status_before`,`book_loan_info`.`status_after` AS `status_after`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`LAST_EDIT` AS `LAST_EDIT`,`book_loan_info`.`FLAG` AS `FLAG`,`users`.`names` AS `NAMES` from (`book_loan_info` join `users` on(convert(`users`.`id` using utf8) = `book_loan_info`.`BORROWER_ID`)) order by `book_loan_info`.`LAST_EDIT` ;

-- --------------------------------------------------------

--
-- Structure for view `calculate_a_level_units_view`
--
DROP TABLE IF EXISTS `calculate_a_level_units_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `calculate_a_level_units_view`  AS  select `student_subject_score_position`.`term_id` AS `term_id`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`a_level_grade_system_tbl`.`points` AS `points` from (`student_subject_score_position` join `a_level_grade_system_tbl` on(`a_level_grade_system_tbl`.`grade` = `student_subject_score_position`.`grade`)) where !(`student_subject_score_position`.`subject_id` in (select `subject`.`subject_id` from `subject` where `subject`.`subject_type` = 'Supplement')) order by `a_level_grade_system_tbl`.`grade` ;

-- --------------------------------------------------------

--
-- Structure for view `calculate_o_level_units_view`
--
DROP TABLE IF EXISTS `calculate_o_level_units_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `calculate_o_level_units_view`  AS  select `student_subject_score_position`.`term_id` AS `term_id`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`o_level_grade_system_tbl`.`unit_point` AS `unit_point` from (`student_subject_score_position` join `o_level_grade_system_tbl` on(`o_level_grade_system_tbl`.`grade` = `student_subject_score_position`.`grade`)) order by `o_level_grade_system_tbl`.`grade` ;

-- --------------------------------------------------------

--
-- Structure for view `classes_assigned_view`
--
DROP TABLE IF EXISTS `classes_assigned_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `classes_assigned_view`  AS  select distinct `class_stream`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date` from ((`teaching_assignment` join `class_stream` on(`class_stream`.`class_stream_id` = `teaching_assignment`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `class_asset_view`
--
DROP TABLE IF EXISTS `class_asset_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_asset_view`  AS  select `class_asset`.`id` AS `id`,`class_asset`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_asset`.`asset_name` AS `asset_name`,`class_asset`.`asset_no` AS `asset_no`,`class_asset`.`status` AS `status`,`class_asset`.`description` AS `description` from ((`class_asset` join `class_stream` on(`class_asset`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `class_journal_view`
--
DROP TABLE IF EXISTS `class_journal_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_journal_view`  AS  select `subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`timetable`.`class_stream_id` AS `timetable_class_stream_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from ((`timetable` left join (((`teaching_assignment` join `teacher` on(`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`)) join `staff` on(`teacher`.`staff_id` = `staff`.`staff_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) on(`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`)) join `period` on(`timetable`.`period_no` = `period`.`period_no`)) ;

-- --------------------------------------------------------

--
-- Structure for view `class_stream_subject_display_view`
--
DROP TABLE IF EXISTS `class_stream_subject_display_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_stream_subject_display_view`  AS  select `class_stream_subject_view`.`class_id` AS `class_id`,`class_stream_subject_view`.`class_stream_id` AS `class_stream_id`,`class_stream_subject_view`.`class_name` AS `class_name`,`class_stream_subject_view`.`subject_id` AS `subject_id`,`class_stream_subject_view`.`stream` AS `stream`,group_concat(' ',`class_stream_subject_view`.`subject_name` separator ',') AS `class_subjects` from `class_stream_subject_view` group by `class_stream_subject_view`.`class_stream_id` ;

-- --------------------------------------------------------

--
-- Structure for view `class_stream_subject_view`
--
DROP TABLE IF EXISTS `class_stream_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_stream_subject_view`  AS  select `class_level`.`class_id` AS `class_id`,`class_stream_subject`.`class_stream_id` AS `class_stream_id`,`class_stream_subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`subject_choice` AS `subject_choice`,`subject`.`subject_category` AS `subject_category`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from (((`class_stream_subject` join `class_stream` on(`class_stream_subject`.`class_stream_id` = convert(`class_stream`.`class_stream_id` using utf8))) join `subject` on(`class_stream_subject`.`subject_id` = convert(`subject`.`subject_id` using utf8))) join `class_level` on(`class_stream`.`class_id` = `class_level`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `class_subject_teachers_view`
--
DROP TABLE IF EXISTS `class_subject_teachers_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_subject_teachers_view`  AS  select `staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names`,`subject`.`subject_name` AS `subject_name`,`teaching_assignment`.`class_stream_id` AS `class_stream_id` from (((`teaching_assignment` join `teacher` on(`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`)) join `staff` on(`teacher`.`staff_id` = `staff`.`staff_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `completed_students_view`
--
DROP TABLE IF EXISTS `completed_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `completed_students_view`  AS  select `student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`waliomaliza_tbl`.`id` AS `id`,`waliomaliza_tbl`.`index_no` AS `index_no`,`waliomaliza_tbl`.`admission_no` AS `admission_no`,`waliomaliza_tbl`.`division` AS `division`,`waliomaliza_tbl`.`points` AS `points`,`waliomaliza_tbl`.`kashachukua_cheti` AS `kashachukua_cheti`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `academic_year_id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from ((((`student` join `class_level` on(`student`.`class_id` = `class_level`.`class_id`)) left join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `academic_year` on(`student`.`academic_year` = `academic_year`.`id`)) join `waliomaliza_tbl` on(`waliomaliza_tbl`.`admission_no` = `student`.`admission_no`)) where `student`.`stte_out` = 'completed' and `student`.`status` = 'inactive' and (`student`.`stte` = 'transferred_in' or `student`.`stte` = 'selected') ;

-- --------------------------------------------------------

--
-- Structure for view `count_borrowed_per_person_view`
--
DROP TABLE IF EXISTS `count_borrowed_per_person_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `count_borrowed_per_person_view`  AS  select count(`borrowing_report`.`BORROWER_ID`) AS `copies`,`borrowing_report`.`NAMES` AS `names`,`borrowing_report`.`BORROWER_TYPE` AS `borrower_type` from `borrowing_report` where `borrowing_report`.`FLAG` = 'borrowed' group by `borrowing_report`.`BORROWER_ID` ;

-- --------------------------------------------------------

--
-- Structure for view `current_term_academic_year_view`
--
DROP TABLE IF EXISTS `current_term_academic_year_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `current_term_academic_year_view`  AS  select `term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`id` AS `id`,`academic_year`.`year` AS `year` from (`term` join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) where `term`.`is_current` = 'yes' and `academic_year`.`status` = 'current_academic_year' ;

-- --------------------------------------------------------

--
-- Structure for view `daily_attendance_report_view`
--
DROP TABLE IF EXISTS `daily_attendance_report_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_attendance_report_view`  AS  select `class_stream`.`class_id` AS `class_id`,`student_attendance_record`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`attendance`.`att_type` AS `att_type`,count(0) AS `nos`,`student_attendance_record`.`date_` AS `date_` from (((`student_attendance_record` join `attendance` on(`student_attendance_record`.`att_id` = `attendance`.`att_id`)) join `class_stream` on(`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_stream`.`class_id` = `class_level`.`class_id`)) group by `student_attendance_record`.`date_`,`student_attendance_record`.`class_stream_id`,`attendance`.`att_type` ;

-- --------------------------------------------------------

--
-- Structure for view `daily_school_attendance_view`
--
DROP TABLE IF EXISTS `daily_school_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_school_attendance_view`  AS  select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`attendance`.`att_type` AS `att_type`,`class_level`.`class_id` AS `class_id`,`class_stream`.`class_stream_id` AS `class_stream_id`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day` from ((((`student_attendance_record` join `student` on(`student_attendance_record`.`admission_no` = `student`.`admission_no`)) join `class_stream` on(`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `attendance` on(`student_attendance_record`.`att_id` = `attendance`.`att_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `daily_student_attendance_view`
--
DROP TABLE IF EXISTS `daily_student_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_student_attendance_view`  AS  select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`class_stream_id` AS `class_stream_id` from ((`student` join `student_attendance_record` on(`student`.`admission_no` = `student_attendance_record`.`admission_no`)) join `attendance` on(`student_attendance_record`.`att_id` = `attendance`.`att_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `daily_student_roll_call_view`
--
DROP TABLE IF EXISTS `daily_student_roll_call_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_student_roll_call_view`  AS  select `student_roll_call_record`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`student_roll_call_record`.`date_` AS `date_`,`student_roll_call_record`.`dorm_id` AS `dorm_id`,`student_roll_call_record`.`att_id` AS `att_id` from ((`student` join `student_roll_call_record` on(`student`.`admission_no` = `student_roll_call_record`.`admission_no`)) join `attendance` on(`student_roll_call_record`.`att_id` = `attendance`.`att_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `department_class_stream_subject_view`
--
DROP TABLE IF EXISTS `department_class_stream_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_class_stream_subject_view`  AS  select `class_level`.`class_name` AS `class_name`,`subject`.`subject_name` AS `subject_name`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`class_stream`.`class_stream_id` AS `class_stream_id`,`class_stream`.`class_id` AS `class_id`,`class_stream`.`stream` AS `stream`,`class_stream`.`stream_id` AS `stream_id`,`class_stream`.`teacher_id` AS `teacher_id`,`class_stream`.`admission_no` AS `admission_no`,`class_stream`.`description` AS `description`,`class_stream`.`capacity` AS `capacity`,`class_stream`.`id_display` AS `id_display`,`class_stream_subject`.`subject_id` AS `subject_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `staff_id`,`department`.`dept_type` AS `dept_type` from ((((`class_stream_subject` join `subject` on(`class_stream_subject`.`subject_id` = convert(`subject`.`subject_id` using utf8))) join `class_stream` on(`class_stream_subject`.`class_stream_id` = convert(`class_stream`.`class_stream_id` using utf8))) join `department` on(`subject`.`dept_id` = `department`.`dept_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `department_staff_view`
--
DROP TABLE IF EXISTS `department_staff_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_staff_view`  AS  select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`middlename`,' ',`staff`.`lastname`) AS `staff_names`,`subject`.`dept_id` AS `dept_id`,`staff`.`gender` AS `gender`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name` from (((`subject_teacher` join `subject` on(`subject`.`subject_id` = `subject_teacher`.`subject_id`)) join `teacher` on(`subject_teacher`.`teacher_id` = `teacher`.`staff_id`)) join `staff` on(`staff`.`staff_id` = `teacher`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `department_view`
--
DROP TABLE IF EXISTS `department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_view`  AS  select `department`.`staff_id` AS `staff_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`dept_type` AS `dept_type`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type` from (`department` left join `staff` on(`department`.`staff_id` = `staff`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `disabled_students`
--
DROP TABLE IF EXISTS `disabled_students`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disabled_students`  AS  select `student`.`disabled` AS `disabled`,`student`.`status` AS `statasi`,`student`.`disability` AS `disability`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from (((`student` join `class_level` on(`student`.`class_id` = `class_level`.`class_id`)) left join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `academic_year` on(`student`.`academic_year` = `academic_year`.`id`)) where `student`.`disabled` = 'yes' and `student`.`status` = 'active' and (`student`.`stte` = 'transferred_in' or `student`.`stte` = 'selected') ;

-- --------------------------------------------------------

--
-- Structure for view `dorm_asset_view`
--
DROP TABLE IF EXISTS `dorm_asset_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dorm_asset_view`  AS  select `dorm_asset`.`id` AS `id`,`dorm_asset`.`dorm_id` AS `dorm_id`,`dormitory`.`dorm_name` AS `dorm_name`,`dorm_asset`.`asset_name` AS `asset_name`,`dorm_asset`.`asset_no` AS `asset_no`,`dorm_asset`.`status` AS `status`,`dorm_asset`.`description` AS `description` from (`dorm_asset` join `dormitory` on(`dorm_asset`.`dorm_id` = `dormitory`.`dorm_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `dorm_capacity_view`
--
DROP TABLE IF EXISTS `dorm_capacity_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dorm_capacity_view`  AS  select `dormitory`.`dorm_id` AS `dorm_id`,(select count(`student`.`admission_no`) from `student` where `student`.`status` = 'active' and `student`.`dorm_id` = `dormitory`.`dorm_id`) AS `no_of_students`,`dormitory`.`dorm_name` AS `dorm_name`,`dormitory`.`capacity` AS `capacity`,`dormitory`.`capacity` - (select count(`student`.`admission_no`) from `student` where `student`.`status` = 'active' and `student`.`dorm_id` = `dormitory`.`dorm_id`) AS `remaining` from (`dormitory` left join `student` on(`dormitory`.`dorm_id` = `student`.`dorm_id`)) group by `dormitory`.`dorm_id` ;

-- --------------------------------------------------------

--
-- Structure for view `edit_teaching_attendance_view`
--
DROP TABLE IF EXISTS `edit_teaching_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `edit_teaching_attendance_view`  AS  select `teaching_attendance`.`subTopicID` AS `subTopicID`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`teaching_attendance`.`ta_id` AS `ta_id`,`teaching_attendance`.`no_of_absentees` AS `no_of_absentees`,`teaching_attendance`.`status` AS `status`,`teaching_attendance`.`date` AS `date`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from (((((((`teaching_assignment` join `teacher` on(`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`)) join `staff` on(`teacher`.`staff_id` = `staff`.`staff_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) join `timetable` on(`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`)) join `period` on(`timetable`.`period_no` = `period`.`period_no`)) join `teaching_attendance` on(`teaching_attendance`.`t_id` = `timetable`.`t_id`)) join `sub_topic` on(`teaching_attendance`.`subTopicID` = `sub_topic`.`subtopic_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `edit_teaching_attendance_view_mpya`
--
DROP TABLE IF EXISTS `edit_teaching_attendance_view_mpya`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `edit_teaching_attendance_view_mpya`  AS  select `teaching_attendance`.`subTopicID` AS `subTopicID`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`teaching_attendance`.`ta_id` AS `ta_id`,`teaching_attendance`.`no_of_absentees` AS `no_of_absentees`,`teaching_attendance`.`status` AS `status`,`teaching_attendance`.`date` AS `date`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from ((`teaching_attendance` left join (((((`teaching_assignment` join `teacher` on(`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`)) join `staff` on(`teacher`.`staff_id` = `staff`.`staff_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) join `timetable` on(`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`)) join `period` on(`timetable`.`period_no` = `period`.`period_no`)) on(`teaching_attendance`.`t_id` = `timetable`.`t_id`)) left join `sub_topic` on(`teaching_attendance`.`subTopicID` = `sub_topic`.`subtopic_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `expelled_students_view`
--
DROP TABLE IF EXISTS `expelled_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expelled_students_view`  AS  select `student`.`admission_no` AS `admission_no`,`student`.`date` AS `date`,`student`.`academic_year` AS `academic_year`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`dob` AS `dob`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`dorm_id` AS `dorm_id`,`student`.`status` AS `status`,`student`.`stte_out` AS `stte_out`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((`student` join `class_level` on(`student`.`class_id` = `class_level`.`class_id`)) join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) where `student`.`stte_out` = 'expelled' and `student`.`status` = 'inactive' ;

-- --------------------------------------------------------

--
-- Structure for view `expired_book_view`
--
DROP TABLE IF EXISTS `expired_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expired_book_view`  AS  select `borrowed_report_view`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`borrowed_report_view`.`ID` AS `ID`,`borrowed_report_view`.`ISBN` AS `ISBN`,`borrowed_report_view`.`BORROWER_ID` AS `BORROWER_ID`,`borrowed_report_view`.`DATE_BORROWED` AS `DATE_BORROWED`,`borrowed_report_view`.`RETURN_DATE` AS `RETURN_DATE`,`borrowed_report_view`.`TIME_LEFT` AS `TIME_LEFT`,`borrowed_report_view`.`BOOK_TITLE` AS `BOOK_TITLE`,`borrowed_report_view`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`borrowed_report_view`.`EDITION` AS `EDITION`,`borrowed_report_view`.`NAMES` AS `NAMES`,`borrowed_report_view`.`FLAG` AS `FLAG` from `borrowed_report_view` where `borrowed_report_view`.`TIME_LEFT` < 0 order by `borrowed_report_view`.`TIME_LEFT` ;

-- --------------------------------------------------------

--
-- Structure for view `filtered_teaching_assignment`
--
DROP TABLE IF EXISTS `filtered_teaching_assignment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `filtered_teaching_assignment`  AS  select `teaching_assignment`.`assignment_id` AS `assignment_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`teaching_assignment`.`term_id` AS `term_id`,`teaching_assignment`.`filter_flag` AS `filter_flag`,`teaching_assignment`.`dept_id` AS `dept_id` from `teaching_assignment` where `teaching_assignment`.`term_id` = '2' and `teaching_assignment`.`filter_flag` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `first_join_staff_attendance_view`
--
DROP TABLE IF EXISTS `first_join_staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `first_join_staff_attendance_view`  AS  select `staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff_attendance_record`.`staff_id` AS `staff_id`,`staff_attendance_record`.`date_` AS `date_`,`staff_attendance_record`.`day` AS `day`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_staff_attendance`.`change_type` AS `change_type`,`audit_staff_attendance`.`change_by` AS `change_by`,`audit_staff_attendance`.`ip_address` AS `ip_address`,`audit_staff_attendance`.`change_time` AS `change_time` from (((`staff_attendance_record` join `staff` on(`staff_attendance_record`.`staff_id` = `staff`.`staff_id`)) join `audit_staff_attendance` on(`staff_attendance_record`.`id` = `audit_staff_attendance`.`sar_id`)) join `attendance` on(`audit_staff_attendance`.`att_id` = `attendance`.`att_id`)) order by `staff_attendance_record`.`staff_id`,`audit_staff_attendance`.`change_time` desc ;

-- --------------------------------------------------------

--
-- Structure for view `lost_book_view`
--
DROP TABLE IF EXISTS `lost_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lost_book_view`  AS  select distinct `book`.`isbn` AS `ISBN`,`book`.`ID` AS `ID`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME` from (`book` join `book_type`) where `book`.`isbn` = `book_type`.`ISBN` and `book`.`STATUS` = 'LOST' ;

-- --------------------------------------------------------

--
-- Structure for view `my_announcements_view`
--
DROP TABLE IF EXISTS `my_announcements_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `my_announcements_view`  AS  select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `username` from ((`announcement` join `announcement_status` on(`announcement`.`announcement_id` = `announcement_status`.`announcement_id`)) join `staff` on(`staff`.`staff_id` = `announcement`.`posted_by`)) where `announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where `staff_group`.`staff_id` = 'STAFF011') and `announcement_status`.`staff_id` = 'STAFF011' union select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,`admin`.`username` AS `username` from ((`announcement` join `announcement_status` on(`announcement`.`announcement_id` = `announcement_status`.`announcement_id`)) join `admin` on(`admin`.`id` = `announcement`.`posted_by`)) where `announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where `staff_group`.`staff_id` = 'STAFF011') and `announcement_status`.`staff_id` = 'STAFF011' ;

-- --------------------------------------------------------

--
-- Structure for view `out_book`
--
DROP TABLE IF EXISTS `out_book`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `out_book`  AS  select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`DATE_DUE_BACK` AS `DATE_DUE_BACK`,`book_loan_info`.`DATE_OVERDUE` AS `DATE_OVERDUE`,`book_loan_info`.`status_before` AS `STATUS_BEFORE`,`book_loan_info`.`status_after` AS `STATUS_AFTER`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`LAST_EDIT` AS `LAST_EDIT`,`book_loan_info`.`FLAG` AS `FLAG` from `book_loan_info` where `book_loan_info`.`FLAG` = 'borrowed' ;

-- --------------------------------------------------------

--
-- Structure for view `remained_book_view`
--
DROP TABLE IF EXISTS `remained_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `remained_book_view`  AS  select `book`.`isbn` AS `isbn`,`book`.`ID` AS `ID`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book`.`STATUS` AS `STATUS` from `book` where !((`book`.`ID`,`book`.`isbn`) in (select `book`.`ID`,`book`.`isbn` from `book` where `book`.`STATUS` = 'lost')) ;

-- --------------------------------------------------------

--
-- Structure for view `reminder`
--
DROP TABLE IF EXISTS `reminder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reminder`  AS  select `book`.`STATUS` AS `STATUS`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book`.`ID` AS `ID`,`book`.`isbn` AS `ISBN` from `book` where !((`book`.`ID`,`book`.`isbn`) in (select `borrowed_report`.`ID`,`borrowed_report`.`ISBN` from `borrowed_report`)) ;

-- --------------------------------------------------------

--
-- Structure for view `remove_borrowed_view`
--
DROP TABLE IF EXISTS `remove_borrowed_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `remove_borrowed_view`  AS  select `reminder`.`ID` AS `ID`,`reminder`.`ISBN` AS `ISBN`,`reminder`.`STATUS` AS `STATUS`,`reminder`.`LAST_EDIT` AS `LAST_EDIT`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`EDITION` AS `EDITION` from (`book_type` join `reminder`) where `reminder`.`ISBN` = `book_type`.`ISBN` order by `reminder`.`LAST_EDIT` desc ;

-- --------------------------------------------------------

--
-- Structure for view `returned_book_view`
--
DROP TABLE IF EXISTS `returned_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `returned_book_view`  AS  select `book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`ID` AS `ID`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `EXPECT_RETURN`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`status_after` AS `STATUS_AFTER`,`borrowers`.`borrower_id` AS `BORROWER_ID`,`borrowers`.`borrower_description` AS `BORROWER_DESCRIPTION`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `borrowers`) where `book_loan_info`.`ISBN` = `book_type`.`ISBN` and `book_loan_info`.`BORROWER_ID` = `borrowers`.`borrower_id` order by `book_loan_info`.`DATE_DUE_BACK` desc ;

-- --------------------------------------------------------

--
-- Structure for view `staff_activity_log_view`
--
DROP TABLE IF EXISTS `staff_activity_log_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_activity_log_view`  AS  select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`user_role` AS `user_role`,`staff`.`last_log` AS `last_log` from (`activity_log` join `staff` on(`staff`.`staff_id` = `activity_log`.`user_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `staff_attendance_view`
--
DROP TABLE IF EXISTS `staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_attendance_view`  AS  select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names`,`staff`.`gender` AS `gender`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`staff_attendance_record`.`day` AS `day`,`staff_attendance_record`.`date_` AS `date_` from ((`staff` join `staff_attendance_record` on(`staff`.`staff_id` = `staff_attendance_record`.`staff_id`)) join `attendance` on(`staff_attendance_record`.`att_id` = `attendance`.`att_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `staff_group_members_view`
--
DROP TABLE IF EXISTS `staff_group_members_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_group_members_view`  AS  select `staff_group`.`group_id` AS `group_id`,`group_type`.`group_name` AS `group_name`,group_concat(concat(`staff`.`firstname`,' ',`staff`.`lastname`) separator ',  ') AS `group_members` from ((`staff_group` join `staff` on(`staff_group`.`staff_id` = `staff`.`staff_id`)) join `group_type` on(`group_type`.`group_id` = `staff_group`.`group_id`)) group by `staff_group`.`group_id` ;

-- --------------------------------------------------------

--
-- Structure for view `staff_plus_nok_view`
--
DROP TABLE IF EXISTS `staff_plus_nok_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_plus_nok_view`  AS  select `staff`.`user_role` AS `user_role`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`last_log` AS `last_log`,`staff_nok`.`firstname` AS `nok_fn`,`staff_nok`.`lastname` AS `nok_ln`,`staff_nok`.`gender` AS `nok_gender`,`staff_nok`.`relation_type` AS `relation_type`,`staff_nok`.`email` AS `nok_email`,`staff_nok`.`phone_no` AS `nok_phone_no`,`staff_nok`.`p_box` AS `nok_box`,`staff_nok`.`p_region` AS `nok_region`,`staff_nok`.`current_box` AS `current_box`,`staff_nok`.`current_region` AS `current_region` from (`staff` left join `staff_nok` on(`staff_nok`.`staff_id` = `staff`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `staff_qualification_view`
--
DROP TABLE IF EXISTS `staff_qualification_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_qualification_view`  AS  select `qualification`.`q_id` AS `q_id`,`qualification`.`certified` AS `certified`,`staff`.`user_role` AS `user_role`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`last_log` AS `last_log`,`qualification`.`certification` AS `certification`,`qualification`.`completion_date` AS `completion_date`,`qualification`.`description` AS `description`,`qualification`.`university` AS `university`,`staff_nok`.`firstname` AS `nok_fn`,`staff_nok`.`lastname` AS `nok_ln`,`staff_nok`.`gender` AS `nok_gender`,`staff_nok`.`relation_type` AS `relation_type`,`staff_nok`.`email` AS `nok_email`,`staff_nok`.`phone_no` AS `nok_phone_no`,`staff_nok`.`p_box` AS `nok_box`,`staff_nok`.`p_region` AS `nok_region`,`staff_nok`.`current_box` AS `current_box`,`staff_nok`.`current_region` AS `current_region` from ((`staff` left join `qualification` on(`staff`.`staff_id` = `qualification`.`staff_id`)) left join `staff_nok` on(`staff_nok`.`staff_id` = `staff`.`staff_id`)) where `staff`.`status` = 'active' order by `qualification`.`completion_date` desc ;

-- --------------------------------------------------------

--
-- Structure for view `staff_role_permission_view`
--
DROP TABLE IF EXISTS `staff_role_permission_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_role_permission_view`  AS  select `staff_role`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`phone_no` AS `phone_no`,`staff`.`status` AS `status`,`permission`.`permission_id` AS `permission_id`,`permission`.`description` AS `description`,`role_permission`.`role_type_id` AS `role_type_id`,`role_type`.`role_name` AS `role_name` from ((((`role_permission` join `permission` on(`permission`.`permission_id` = `role_permission`.`permission_id`)) join `role_type` on(`role_type`.`role_type_id` = `role_permission`.`role_type_id`)) join `staff_role` on(`role_type`.`role_type_id` = `staff_role`.`role_type_id`)) join `staff` on(`staff_role`.`staff_id` = `staff`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `students_by_class_and_subject_view`
--
DROP TABLE IF EXISTS `students_by_class_and_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students_by_class_and_subject_view`  AS  select `class_level`.`level` AS `level`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`class_stream`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_id` AS `class_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((`student` join `student_subject` on(`student`.`admission_no` = `student_subject`.`admission_no`)) join `subject` on(`student_subject`.`subject_id` = `subject`.`subject_id`)) join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) where `student`.`status` = 'active' ;

-- --------------------------------------------------------

--
-- Structure for view `students_in_dormitory_view`
--
DROP TABLE IF EXISTS `students_in_dormitory_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students_in_dormitory_view`  AS  select `class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`status` AS `status`,`dormitory`.`dorm_id` AS `dorm_id` from (((`student` left join `dormitory` on(`student`.`dorm_id` = `dormitory`.`dorm_id`)) left join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `student`.`class_id`)) where `student`.`status` = 'active' order by `class_level`.`class_id` ;

-- --------------------------------------------------------

--
-- Structure for view `student_enrollment_academic_year_view`
--
DROP TABLE IF EXISTS `student_enrollment_academic_year_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_enrollment_academic_year_view`  AS  select `term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`term`.`is_current` AS `is_current`,`term`.`aid` AS `aid`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`enrollment`.`admission_no` AS `admission_no`,`enrollment`.`class_id` AS `class_id`,`enrollment`.`class_stream_id` AS `class_stream_id`,`enrollment`.`academic_year` AS `academic_year`,`class_level`.`class_name` AS `class_name` from (((`academic_year` join `term` on(`academic_year`.`id` = `term`.`aid`)) join `enrollment` on(`academic_year`.`id` = `enrollment`.`academic_year`)) join `class_level` on(`enrollment`.`class_id` = `class_level`.`class_id`)) order by `enrollment`.`class_id`,`term`.`term_name` ;

-- --------------------------------------------------------

--
-- Structure for view `student_guardian_view`
--
DROP TABLE IF EXISTS `student_guardian_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_guardian_view`  AS  select `student`.`stte_out` AS `stte_out`,`student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`student`.`stte` AS `stte`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`firstname` AS `student_firstname`,`student`.`lastname` AS `student_lastname`,`guardian`.`firstname` AS `g_firstname`,`guardian`.`lastname` AS `g_lastname`,`guardian`.`middlename` AS `g_middlename`,`student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`date` AS `date`,`student`.`dob` AS `dob`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`status` AS `status`,concat(`guardian`.`firstname`,' ',`guardian`.`middlename`,' ',`guardian`.`lastname`) AS `guardian_names`,`guardian`.`gender` AS `gender`,`guardian`.`rel_type` AS `rel_type`,`guardian`.`email` AS `email`,`guardian`.`occupation` AS `occupation`,`guardian`.`phone_no` AS `phone_no`,`guardian`.`p_box` AS `p_box`,`guardian`.`p_region` AS `p_region` from (`student` left join `guardian` on(`student`.`admission_no` = `guardian`.`admission_no`)) ;

-- --------------------------------------------------------

--
-- Structure for view `student_progressive_results_view`
--
DROP TABLE IF EXISTS `student_progressive_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_progressive_results_view`  AS  select `student_subject_score_position`.`subject_rank` AS `subject_rank`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((((`student_subject_score_position` join `student` on(`student_subject_score_position`.`admission_no` = `student`.`admission_no`)) join `class_level` on(`student_subject_score_position`.`class_id` = `class_level`.`class_id`)) join `class_stream` on(`class_stream`.`class_stream_id` = `student_subject_score_position`.`class_stream_id`)) join `term` on(`student_subject_score_position`.`term_id` = `term`.`term_id`)) join `subject` on(`student_subject_score_position`.`subject_id` = `subject`.`subject_id`)) join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) ;

-- --------------------------------------------------------

--
-- Structure for view `student_result_view`
--
DROP TABLE IF EXISTS `student_result_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_result_view`  AS  select `student_subject_score_position`.`subject_rank` AS `subject_rank`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`student`.`academic_year` AS `academic_year`,`term`.`aid` AS `aid`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((((`student_subject_score_position` join `student` on(`student_subject_score_position`.`admission_no` = `student`.`admission_no`)) join `class_level` on(`student_subject_score_position`.`class_id` = `class_level`.`class_id`)) join `class_stream` on(`class_stream`.`class_stream_id` = `student_subject_score_position`.`class_stream_id`)) join `term` on(`student_subject_score_position`.`term_id` = `term`.`term_id`)) join `subject` on(`student_subject_score_position`.`subject_id` = `subject`.`subject_id`)) join `academic_year` on(`academic_year`.`id` = `term`.`aid`)) ;

-- --------------------------------------------------------

--
-- Structure for view `student_transfer_in_view`
--
DROP TABLE IF EXISTS `student_transfer_in_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_transfer_in_view`  AS  select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`stte` AS `stte`,`student`.`status` AS `status`,`transferred_students`.`form` AS `form`,`transferred_students`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`transferred_students`.`school` AS `school`,`class_level`.`class_name` AS `class_name`,`transferred_students`.`date_of_transfer` AS `date_of_transfer`,`transferred_students`.`transfer_letter` AS `transfer_letter`,`transferred_students`.`self_form_receipt` AS `self_form_receipt`,`transferred_students`.`transfer_status` AS `transfer_status` from ((`transferred_students` join `student` on(`transferred_students`.`admission_no` = `student`.`admission_no`)) join `class_level` on(`class_level`.`class_id` = `transferred_students`.`form`)) where `transferred_students`.`transfer_status` = 'IN' ;

-- --------------------------------------------------------

--
-- Structure for view `student_transfer_out_view`
--
DROP TABLE IF EXISTS `student_transfer_out_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_transfer_out_view`  AS  select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`stte_out` AS `stte_out`,`student`.`status` AS `status`,`transferred_students`.`form` AS `form`,`transferred_students`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`transferred_students`.`school` AS `school`,`class_level`.`class_name` AS `class_name`,`transferred_students`.`date_of_transfer` AS `date_of_transfer`,`transferred_students`.`transfer_letter` AS `transfer_letter`,`transferred_students`.`self_form_receipt` AS `self_form_receipt`,`transferred_students`.`transfer_status` AS `transfer_status` from ((`transferred_students` join `student` on(`transferred_students`.`admission_no` = `student`.`admission_no`)) join `class_level` on(`class_level`.`class_id` = `transferred_students`.`form`)) where `transferred_students`.`transfer_status` = 'OUT' ;

-- --------------------------------------------------------

--
-- Structure for view `subjects_subtopic_view`
--
DROP TABLE IF EXISTS `subjects_subtopic_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subjects_subtopic_view`  AS  select `sub_topic`.`class_stream_id` AS `class_stream_id`,`sub_topic`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`sub_topic`.`subtopic_id` AS `subtopic_id`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`topic`.`topic_name` AS `topic_name`,`sub_topic`.`topic_id` AS `topic_id` from ((`sub_topic` join `topic` on(`sub_topic`.`topic_id` = `topic`.`topic_id`)) join `subject` on(`subject`.`subject_id` = `sub_topic`.`subject_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `subject_department_view`
--
DROP TABLE IF EXISTS `subject_department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subject_department_view`  AS  select `department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `staff_id`,`department`.`dept_type` AS `dept_type`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`subject`.`subject_type` AS `subject_type` from (`subject` join `department` on(`subject`.`dept_id` = `department`.`dept_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `subject_teacher_view`
--
DROP TABLE IF EXISTS `subject_teacher_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subject_teacher_view`  AS  select `staff`.`staff_id` AS `staff_id`,`subject_teacher`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`user_role` AS `user_role`,`staff`.`staff_type` AS `staff_type` from ((`subject` join `subject_teacher` on(`subject_teacher`.`subject_id` = `subject`.`subject_id`)) join `staff` on(`subject_teacher`.`teacher_id` = `staff`.`staff_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `suspended_students_view`
--
DROP TABLE IF EXISTS `suspended_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `suspended_students_view`  AS  select `student`.`admission_no` AS `admission_no`,`student`.`date` AS `date`,`student`.`academic_year` AS `academic_year`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`dob` AS `dob`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`dorm_id` AS `dorm_id`,`student`.`status` AS `status`,`student`.`stte_out` AS `stte_out`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((`student` join `class_level` on(`student`.`class_id` = `class_level`.`class_id`)) join `class_stream` on(`student`.`class_stream_id` = `class_stream`.`class_stream_id`)) where `student`.`stte_out` = 'suspended' and `student`.`status` = 'active' ;

-- --------------------------------------------------------

--
-- Structure for view `teachers_timetable_view`
--
DROP TABLE IF EXISTS `teachers_timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teachers_timetable_view`  AS  select `timetable`.`weekday` AS `weekday`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`subject`.`subject_name` AS `subject_name`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`teaching_assignment`.`teacher_id` AS `teacher_id` from (((((`timetable` join `teaching_assignment` on(`timetable`.`assignment_id` = `teaching_assignment`.`assignment_id`)) join `period` on(`timetable`.`period_no` = `period`.`period_no`)) join `subject` on(`subject`.`subject_id` = `teaching_assignment`.`subject_id`)) join `class_stream` on(`teaching_assignment`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_level`.`class_id` = `class_stream`.`class_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `teaching_assignment_department_view`
--
DROP TABLE IF EXISTS `teaching_assignment_department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teaching_assignment_department_view`  AS  select `subject`.`subject_name` AS `subject_name`,`teaching_assignment`.`term_id` AS `term_id`,`class_level`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`gender` AS `gender`,`staff`.`staff_id` AS `staff_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `hod_id`,`department`.`dept_type` AS `dept_type`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`teaching_assignment`.`filter_flag` AS `filter_flag` from ((((((`teaching_assignment` join `staff` on(`teaching_assignment`.`teacher_id` = `staff`.`staff_id`)) join `staff_department` on(`staff`.`staff_id` = `staff_department`.`staff_id`)) join `department` on(`staff_department`.`dept_id` = `department`.`dept_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) join `class_stream` on(`teaching_assignment`.`class_stream_id` = `class_stream`.`class_stream_id`)) join `class_level` on(`class_stream`.`class_id` = `class_level`.`class_id`)) where `teaching_assignment`.`term_id` in (select `term`.`term_id` from `term` where `term`.`is_current` = 'yes') ;

-- --------------------------------------------------------

--
-- Structure for view `timetable_view`
--
DROP TABLE IF EXISTS `timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `timetable_view`  AS  select `teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,substr(`teaching_assignment`.`class_stream_id`,3) AS `csid`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`timetable`.`assignment_id` AS `assignment_id`,`timetable`.`weekday` AS `weekday`,`timetable`.`t_id` AS `t_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`period`.`duration` AS `duration` from ((((`timetable` join `period` on(`timetable`.`period_no` = `period`.`period_no`)) join `teaching_assignment` on(`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) join `staff` on(`staff`.`staff_id` = `teaching_assignment`.`teacher_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `title_sum_up_view`
--
DROP TABLE IF EXISTS `title_sum_up_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `title_sum_up_view`  AS  select distinct `book_type`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,(select count(0) from `book` where `book_type`.`ISBN` = `book`.`isbn`) AS `COPIES` from `book_type` ;

-- --------------------------------------------------------

--
-- Structure for view `used_to_edit_timetable_view`
--
DROP TABLE IF EXISTS `used_to_edit_timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `used_to_edit_timetable_view`  AS  select `teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`timetable`.`assignment_id` AS `assignment_id`,`timetable`.`weekday` AS `weekday`,`timetable`.`t_id` AS `t_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`period`.`duration` AS `duration` from ((((`timetable` join `period` on(`timetable`.`period_no` = `period`.`period_no`)) join `teaching_assignment` on(`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`)) join `subject` on(`teaching_assignment`.`subject_id` = `subject`.`subject_id`)) join `staff` on(`staff`.`staff_id` = `teaching_assignment`.`teacher_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `users`
--
DROP TABLE IF EXISTS `users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users`  AS  select `student`.`admission_no` AS `id`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names` from `student` union select `department`.`dept_id` AS `id`,`department`.`dept_name` AS `names` from `department` union select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names` from `staff` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year`
--
ALTER TABLE `academic_year`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `year` (`year`);

--
-- Indexes for table `accomodation_history`
--
ALTER TABLE `accomodation_history`
  ADD KEY `fk_ah_dorm` (`dorm_id`),
  ADD KEY `fk_ah_admission_no` (`admission_no`);

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_no`);

--
-- Indexes for table `accountant`
--
ALTER TABLE `accountant`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announcement_id`),
  ADD KEY `fk_group_announcement` (`group_id`);

--
-- Indexes for table `announcement_status`
--
ALTER TABLE `announcement_status`
  ADD UNIQUE KEY `staff_id` (`staff_id`,`group_id`,`announcement_id`),
  ADD KEY `fk_announcement` (`announcement_id`),
  ADD KEY `fk_group_announce` (`group_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `audit_password_reset_tbl`
--
ALTER TABLE `audit_password_reset_tbl`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `fk_reset_by` (`reset_by`);

--
-- Indexes for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_audit_csid` (`class_stream_id`);

--
-- Indexes for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_audit_staff_att` (`att_id`),
  ADD KEY `fk_audit_staff_att1` (`sar_id`);

--
-- Indexes for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_att_type` (`att_id`),
  ADD KEY `fk_audit_student_attendance` (`sar_id`);

--
-- Indexes for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_audit_results` (`ssa_id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `a_level_grade_system_tbl`
--
ALTER TABLE `a_level_grade_system_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `grade` (`grade`),
  ADD UNIQUE KEY `grade_2` (`grade`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`isbn`,`ID`);

--
-- Indexes for table `books_borrowing_limit`
--
ALTER TABLE `books_borrowing_limit`
  ADD PRIMARY KEY (`blid`),
  ADD UNIQUE KEY `b_type` (`b_type`);

--
-- Indexes for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
  ADD PRIMARY KEY (`TRANSACTION_ID`),
  ADD KEY `fk_book` (`ISBN`,`ID`),
  ADD KEY `fk_library` (`LIBRARIAN_ID`);

--
-- Indexes for table `book_type`
--
ALTER TABLE `book_type`
  ADD PRIMARY KEY (`ISBN`),
  ADD KEY `CLASS_ID` (`CLASS_ID`),
  ADD KEY `FK_booktype_aid` (`A_ID`);

--
-- Indexes for table `borrower_type`
--
ALTER TABLE `borrower_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `break_time`
--
ALTER TABLE `break_time`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `fk_brk` (`after_period`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `class_asset`
--
ALTER TABLE `class_asset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `asset_no` (`asset_no`),
  ADD KEY `fk_class_asset` (`class_stream_id`);

--
-- Indexes for table `class_level`
--
ALTER TABLE `class_level`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `class_stream`
--
ALTER TABLE `class_stream`
  ADD PRIMARY KEY (`class_stream_id`),
  ADD UNIQUE KEY `class_id` (`class_id`,`stream`,`stream_id`),
  ADD KEY `fk_cteacher` (`teacher_id`),
  ADD KEY `fk_monitor` (`admission_no`),
  ADD KEY `fk_str` (`stream_id`),
  ADD KEY `fk_stream_name` (`stream`);

--
-- Indexes for table `class_stream_subject`
--
ALTER TABLE `class_stream_subject`
  ADD PRIMARY KEY (`class_stream_id`,`subject_id`),
  ADD KEY `fk_css_subject` (`subject_id`);

--
-- Indexes for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cth_tid` (`teacher_id`),
  ADD KEY `fk_cth_csid` (`class_stream_id`);

--
-- Indexes for table `compute_division`
--
ALTER TABLE `compute_division`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level` (`level`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dept_id`),
  ADD UNIQUE KEY `dept_name` (`dept_name`),
  ADD KEY `fk_hods` (`staff_id`);

--
-- Indexes for table `discipline`
--
ALTER TABLE `discipline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level` (`level`,`starting_points`,`ending_points`,`div_name`),
  ADD UNIQUE KEY `level_2` (`level`,`div_name`);

--
-- Indexes for table `dormitory`
--
ALTER TABLE `dormitory`
  ADD PRIMARY KEY (`dorm_id`),
  ADD KEY `fk_d_ms` (`teacher_id`),
  ADD KEY `fk_dp` (`admission_no`);

--
-- Indexes for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `asset_no` (`asset_no`),
  ADD KEY `fk_dorm_asset` (`dorm_id`);

--
-- Indexes for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dmh_dorm` (`dorm_id`),
  ADD KEY `fk_dmh_tid` (`teacher_id`);

--
-- Indexes for table `duty_roaster`
--
ALTER TABLE `duty_roaster`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `start_date` (`start_date`),
  ADD UNIQUE KEY `end_date` (`end_date`);

--
-- Indexes for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD PRIMARY KEY (`admission_no`,`class_id`,`class_stream_id`,`academic_year`),
  ADD KEY `fk_year_enrollment` (`academic_year`),
  ADD KEY `fk_ecid` (`class_id`);

--
-- Indexes for table `exam_type`
--
ALTER TABLE `exam_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_year` (`academic_year`),
  ADD KEY `fk_et_alvl` (`a_level_academic_year`);

--
-- Indexes for table `fee`
--
ALTER TABLE `fee`
  ADD PRIMARY KEY (`fee_id`);

--
-- Indexes for table `fee_details`
--
ALTER TABLE `fee_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_type`
--
ALTER TABLE `group_type`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `guardian`
--
ALTER TABLE `guardian`
  ADD PRIMARY KEY (`admission_no`,`firstname`,`lastname`),
  ADD UNIQUE KEY `phone_no` (`phone_no`);

--
-- Indexes for table `hod_history`
--
ALTER TABLE `hod_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_hodh_staff` (`staff_id`),
  ADD KEY `fk_hodh_department` (`dept_id`);

--
-- Indexes for table `inactive_staff`
--
ALTER TABLE `inactive_staff`
  ADD PRIMARY KEY (`staff_id`,`deactivated_date`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`s_no`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `lock_account_tbl`
--
ALTER TABLE `lock_account_tbl`
  ADD KEY `fk_lock_account` (`username`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`month_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`),
  ADD UNIQUE KEY `staff_id` (`staff_id`,`msg_id`);

--
-- Indexes for table `o_level_grade_system_tbl`
--
ALTER TABLE `o_level_grade_system_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `grade` (`grade`),
  ADD UNIQUE KEY `grade_2` (`grade`);

--
-- Indexes for table `payee`
--
ALTER TABLE `payee`
  ADD PRIMARY KEY (`s_no`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`period_no`),
  ADD UNIQUE KEY `start_time` (`start_time`),
  ADD UNIQUE KEY `end_time` (`end_time`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pr_aid` (`academic_year`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`q_id`),
  ADD KEY `fk_qualification` (`staff_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`region_id`),
  ADD UNIQUE KEY `region_name` (`region_name`);

--
-- Indexes for table `religion`
--
ALTER TABLE `religion`
  ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`role_type_id`,`module_id`),
  ADD KEY `fk_mod_id` (`module_id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`role_type_id`,`permission_id`),
  ADD KEY `fk_rp_p` (`permission_id`);

--
-- Indexes for table `role_permission_dummy_table`
--
ALTER TABLE `role_permission_dummy_table`
  ADD KEY `fk_rpdt_role` (`role_type_id`),
  ADD KEY `fk_rpdt_p` (`permission_id`);

--
-- Indexes for table `role_type`
--
ALTER TABLE `role_type`
  ADD PRIMARY KEY (`role_type_id`);

--
-- Indexes for table `secretary`
--
ALTER TABLE `secretary`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `selected_students`
--
ALTER TABLE `selected_students`
  ADD PRIMARY KEY (`examination_no`),
  ADD UNIQUE KEY `tel_no` (`tel_no`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_ss_aid` (`academic_year`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD UNIQUE KEY `phone_no` (`phone_no`);

--
-- Indexes for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_id` (`staff_id`,`date_`),
  ADD KEY `fk_staff_att_rec` (`att_id`),
  ADD KEY `fk_staff_att_aid` (`aid`);

--
-- Indexes for table `staff_department`
--
ALTER TABLE `staff_department`
  ADD PRIMARY KEY (`staff_id`,`dept_id`),
  ADD KEY `fk_department_staff` (`dept_id`);

--
-- Indexes for table `staff_group`
--
ALTER TABLE `staff_group`
  ADD PRIMARY KEY (`staff_id`,`group_id`),
  ADD KEY `fk_group_id` (`group_id`);

--
-- Indexes for table `staff_nok`
--
ALTER TABLE `staff_nok`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `staff_role`
--
ALTER TABLE `staff_role`
  ADD PRIMARY KEY (`role_type_id`,`staff_id`),
  ADD KEY `fk_st_role` (`staff_id`);

--
-- Indexes for table `stream`
--
ALTER TABLE `stream`
  ADD PRIMARY KEY (`stream_id`),
  ADD UNIQUE KEY `stream_name` (`stream_name`),
  ADD UNIQUE KEY `stream_name_2` (`stream_name`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`admission_no`),
  ADD UNIQUE KEY `image` (`image`),
  ADD KEY `fk_student_class_id` (`class_id`),
  ADD KEY `fk_student_class_stream_id` (`class_stream_id`),
  ADD KEY `fk_student_dormitory_id` (`dorm_id`),
  ADD KEY `fk_student_academic_year_id` (`academic_year`),
  ADD KEY `fk_tr` (`tribe_id`),
  ADD KEY `fk_rel` (`religion_id`);

--
-- Indexes for table `student_assessment`
--
ALTER TABLE `student_assessment`
  ADD PRIMARY KEY (`admission_no`,`class_id`,`class_stream_id`,`term_id`),
  ADD KEY `fk_c` (`class_id`),
  ADD KEY `fk_cs` (`class_stream_id`),
  ADD KEY `fk_t` (`term_id`);

--
-- Indexes for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admission_no` (`admission_no`,`class_stream_id`,`date_`),
  ADD KEY `fk_att_csid` (`class_stream_id`),
  ADD KEY `fk_std_att_rec` (`att_id`),
  ADD KEY `fk_term_att` (`term_id`);

--
-- Indexes for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admission_no` (`admission_no`,`dorm_id`,`date_`),
  ADD KEY `fk_atid` (`att_id`),
  ADD KEY `fk_dorm_roll_call` (`dorm_id`);

--
-- Indexes for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD PRIMARY KEY (`admission_no`,`subject_id`),
  ADD KEY `fk_student_subject` (`admission_no`),
  ADD KEY `class_stream_id` (`class_stream_id`,`subject_id`);

--
-- Indexes for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
  ADD PRIMARY KEY (`ssa_id`),
  ADD UNIQUE KEY `admission_no` (`admission_no`,`subject_id`,`etype_id`,`class_stream_id`,`term_id`),
  ADD KEY `fk_ssa_cs` (`class_stream_id`),
  ADD KEY `fk_ssa_s` (`subject_id`),
  ADD KEY `fk_etype` (`etype_id`),
  ADD KEY `fk_tssa` (`term_id`);

--
-- Indexes for table `student_subject_score_position`
--
ALTER TABLE `student_subject_score_position`
  ADD PRIMARY KEY (`admission_no`,`subject_id`,`class_stream_id`,`term_id`),
  ADD KEY `fk_tssp` (`term_id`),
  ADD KEY `fk_cssssp` (`class_stream_id`),
  ADD KEY `fk_csssp` (`class_id`),
  ADD KEY `fk_tssub` (`subject_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`subject_id`),
  ADD UNIQUE KEY `subject_name` (`subject_name`),
  ADD KEY `fk_subject_department` (`dept_id`);

--
-- Indexes for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
  ADD PRIMARY KEY (`subject_id`,`teacher_id`),
  ADD KEY `fk_subject_teacher` (`teacher_id`);

--
-- Indexes for table `sub_topic`
--
ALTER TABLE `sub_topic`
  ADD PRIMARY KEY (`subtopic_id`),
  ADD KEY `fk_subtopic_subject` (`subject_id`),
  ADD KEY `fk_subtopic_csid` (`class_stream_id`),
  ADD KEY `fk_subtopic_topic` (`topic_id`);

--
-- Indexes for table `super_admin_module`
--
ALTER TABLE `super_admin_module`
  ADD PRIMARY KEY (`id`,`module_id`),
  ADD KEY `fk_am` (`module_id`);

--
-- Indexes for table `super_admin_permission`
--
ALTER TABLE `super_admin_permission`
  ADD PRIMARY KEY (`id`,`permission_id`),
  ADD KEY `fk_admin_permission` (`permission_id`);

--
-- Indexes for table `suspended_students`
--
ALTER TABLE `suspended_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_suspension` (`admission_no`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `teacher_duty_roaster`
--
ALTER TABLE `teacher_duty_roaster`
  ADD PRIMARY KEY (`teacher_id`,`did`),
  ADD KEY `fk_tod_roaster` (`did`);

--
-- Indexes for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
  ADD PRIMARY KEY (`assignment_id`),
  ADD UNIQUE KEY `teacher_id` (`teacher_id`,`subject_id`,`class_stream_id`),
  ADD UNIQUE KEY `subject_id` (`subject_id`,`class_stream_id`),
  ADD KEY `fk_ta_csid` (`class_stream_id`),
  ADD KEY `fk_ta_term` (`term_id`),
  ADD KEY `fk_ta_department` (`dept_id`);

--
-- Indexes for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
  ADD PRIMARY KEY (`ta_id`),
  ADD UNIQUE KEY `unique_ta_record` (`period_no`,`date`,`class_stream_id`),
  ADD KEY `fk_journal_subtopic` (`subTopicID`),
  ADD KEY `fk_tatt_csid` (`class_stream_id`);

--
-- Indexes for table `teaching_log`
--
ALTER TABLE `teaching_log`
  ADD PRIMARY KEY (`log_id`),
  ADD UNIQUE KEY `subject_id` (`subject_id`,`topic_id`,`class_stream_id`,`term_id`),
  ADD KEY `fk_tlog_csid` (`class_stream_id`),
  ADD KEY `fk_tlog_term` (`term_id`),
  ADD KEY `fk_tlog_teacher` (`teacher_id`),
  ADD KEY `fk_tlog_topic` (`topic_id`),
  ADD KEY `fk_log_dept` (`dept_id`);

--
-- Indexes for table `term`
--
ALTER TABLE `term`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `fk_aid` (`aid`);

--
-- Indexes for table `timetable`
--
ALTER TABLE `timetable`
  ADD PRIMARY KEY (`t_id`),
  ADD UNIQUE KEY `check_timetable_unique` (`period_no`,`teacher_id`,`weekday`),
  ADD KEY `fk_tt_csid` (`class_stream_id`);

--
-- Indexes for table `tod_daily_routine_tbl`
--
ALTER TABLE `tod_daily_routine_tbl`
  ADD PRIMARY KEY (`date_`),
  ADD KEY `fk_duty_roaster` (`did`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `fk_topic_subject` (`subject_id`),
  ADD KEY `fk_topic_csid` (`class_stream_id`);

--
-- Indexes for table `transferred_students`
--
ALTER TABLE `transferred_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transfer_out` (`admission_no`);

--
-- Indexes for table `tribe`
--
ALTER TABLE `tribe`
  ADD PRIMARY KEY (`tribe_id`);

--
-- Indexes for table `waliomaliza_tbl`
--
ALTER TABLE `waliomaliza_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ly` (`completion_year`),
  ADD KEY `fk_completed_students` (`admission_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year`
--
ALTER TABLE `academic_year`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `att_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=334;

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `author_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_level_grade_system_tbl`
--
ALTER TABLE `a_level_grade_system_tbl`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `books_borrowing_limit`
--
ALTER TABLE `books_borrowing_limit`
  MODIFY `blid` smallint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
  MODIFY `TRANSACTION_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `borrower_type`
--
ALTER TABLE `borrower_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `break_time`
--
ALTER TABLE `break_time`
  MODIFY `bid` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_asset`
--
ALTER TABLE `class_asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `compute_division`
--
ALTER TABLE `compute_division`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dept_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `discipline`
--
ALTER TABLE `discipline`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `dormitory`
--
ALTER TABLE `dormitory`
  MODIFY `dorm_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `duty_roaster`
--
ALTER TABLE `duty_roaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `fee_details`
--
ALTER TABLE `fee_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `group_type`
--
ALTER TABLE `group_type`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hod_history`
--
ALTER TABLE `hod_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `s_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `module_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=437;

--
-- AUTO_INCREMENT for table `o_level_grade_system_tbl`
--
ALTER TABLE `o_level_grade_system_tbl`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `payee`
--
ALTER TABLE `payee`
  MODIFY `s_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `region_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `religion`
--
ALTER TABLE `religion`
  MODIFY `religion_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_type`
--
ALTER TABLE `role_type`
  MODIFY `role_type_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `stream`
--
ALTER TABLE `stream`
  MODIFY `stream_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
  MODIFY `ssa_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=624;

--
-- AUTO_INCREMENT for table `sub_topic`
--
ALTER TABLE `sub_topic`
  MODIFY `subtopic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `suspended_students`
--
ALTER TABLE `suspended_students`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
  MODIFY `assignment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
  MODIFY `ta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teaching_log`
--
ALTER TABLE `teaching_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `term`
--
ALTER TABLE `term`
  MODIFY `term_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `timetable`
--
ALTER TABLE `timetable`
  MODIFY `t_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `transferred_students`
--
ALTER TABLE `transferred_students`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tribe`
--
ALTER TABLE `tribe`
  MODIFY `tribe_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `waliomaliza_tbl`
--
ALTER TABLE `waliomaliza_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accomodation_history`
--
ALTER TABLE `accomodation_history`
  ADD CONSTRAINT `fk_ah_admission_no` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ah_dorm` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE;

--
-- Constraints for table `accountant`
--
ALTER TABLE `accountant`
  ADD CONSTRAINT `fk_accountant` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `fk_group_announcement` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`);

--
-- Constraints for table `announcement_status`
--
ALTER TABLE `announcement_status`
  ADD CONSTRAINT `fk_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`announcement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_group_announce` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `audit_password_reset_tbl`
--
ALTER TABLE `audit_password_reset_tbl`
  ADD CONSTRAINT `fk_reset_by` FOREIGN KEY (`reset_by`) REFERENCES `admin` (`id`);

--
-- Constraints for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
  ADD CONSTRAINT `fk_audit_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE;

--
-- Constraints for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
  ADD CONSTRAINT `fk_audit_staff_att` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
  ADD CONSTRAINT `fk_audit_staff_att1` FOREIGN KEY (`sar_id`) REFERENCES `staff_attendance_record` (`id`);

--
-- Constraints for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
  ADD CONSTRAINT `fk_att_type` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
  ADD CONSTRAINT `fk_audit_student_attendance` FOREIGN KEY (`sar_id`) REFERENCES `student_attendance_record` (`id`);

--
-- Constraints for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
  ADD CONSTRAINT `fk_audit_results` FOREIGN KEY (`ssa_id`) REFERENCES `student_subject_assessment` (`ssa_id`);

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `fk_book_type` FOREIGN KEY (`isbn`) REFERENCES `book_type` (`ISBN`);

--
-- Constraints for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
  ADD CONSTRAINT `fk_book` FOREIGN KEY (`ISBN`,`ID`) REFERENCES `book` (`isbn`, `ID`),
  ADD CONSTRAINT `fk_library` FOREIGN KEY (`LIBRARIAN_ID`) REFERENCES `librarian` (`staff_id`);

--
-- Constraints for table `break_time`
--
ALTER TABLE `break_time`
  ADD CONSTRAINT `fk_brk` FOREIGN KEY (`after_period`) REFERENCES `period` (`period_no`);

--
-- Constraints for table `class_asset`
--
ALTER TABLE `class_asset`
  ADD CONSTRAINT `fk_class_asset` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE;

--
-- Constraints for table `class_stream`
--
ALTER TABLE `class_stream`
  ADD CONSTRAINT `fk_cteacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_monitor` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_str` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_stream_name` FOREIGN KEY (`stream`) REFERENCES `stream` (`stream_name`) ON UPDATE CASCADE;

--
-- Constraints for table `class_stream_subject`
--
ALTER TABLE `class_stream_subject`
  ADD CONSTRAINT `fk_css_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_css_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
  ADD CONSTRAINT `fk_cth_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cth_tid` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `fk_hods` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `dormitory`
--
ALTER TABLE `dormitory`
  ADD CONSTRAINT `fk_d_ms` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dp` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE;

--
-- Constraints for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
  ADD CONSTRAINT `fk_dorm_asset` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE;

--
-- Constraints for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
  ADD CONSTRAINT `fk_dmh_dorm` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dmh_tid` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD CONSTRAINT `fk_ecid` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`),
  ADD CONSTRAINT `fk_student_enrollment` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
  ADD CONSTRAINT `fk_year_enrollment` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`);

--
-- Constraints for table `exam_type`
--
ALTER TABLE `exam_type`
  ADD CONSTRAINT `fk_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_et_alvl` FOREIGN KEY (`a_level_academic_year`) REFERENCES `academic_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `guardian`
--
ALTER TABLE `guardian`
  ADD CONSTRAINT `fk_std_guardian` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`);

--
-- Constraints for table `hod_history`
--
ALTER TABLE `hod_history`
  ADD CONSTRAINT `fk_hodh_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`),
  ADD CONSTRAINT `fk_hodh_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `inactive_staff`
--
ALTER TABLE `inactive_staff`
  ADD CONSTRAINT `fk_inactive_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `librarian`
--
ALTER TABLE `librarian`
  ADD CONSTRAINT `fk_librarian` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lock_account_tbl`
--
ALTER TABLE `lock_account_tbl`
  ADD CONSTRAINT `fk_lock_account` FOREIGN KEY (`username`) REFERENCES `staff` (`phone_no`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `fk_staff_notification` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
  ADD CONSTRAINT `fk_pr_aid` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `qualification`
--
ALTER TABLE `qualification`
  ADD CONSTRAINT `fk_qualification` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `fk_mod_id` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rl_mod` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `fk_rp_p` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rp_role` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permission_dummy_table`
--
ALTER TABLE `role_permission_dummy_table`
  ADD CONSTRAINT `fk_rpdt_p` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rpdt_role` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `secretary`
--
ALTER TABLE `secretary`
  ADD CONSTRAINT `fk_secretary` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `selected_students`
--
ALTER TABLE `selected_students`
  ADD CONSTRAINT `fk_ss_aid` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`);

--
-- Constraints for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
  ADD CONSTRAINT `fk_staff_att` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `fk_staff_att_aid` FOREIGN KEY (`aid`) REFERENCES `academic_year` (`id`),
  ADD CONSTRAINT `fk_staff_att_rec` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`);

--
-- Constraints for table `staff_department`
--
ALTER TABLE `staff_department`
  ADD CONSTRAINT `fk_department_staff` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_staff_department` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_group`
--
ALTER TABLE `staff_group`
  ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_staff_group` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_nok`
--
ALTER TABLE `staff_nok`
  ADD CONSTRAINT `fk_staff_nok` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_role`
--
ALTER TABLE `staff_role`
  ADD CONSTRAINT `fk_rtid` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_st_role` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_clid` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`),
  ADD CONSTRAINT `fk_rel` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`religion_id`),
  ADD CONSTRAINT `fk_std_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_academic_year_id` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_dormitory_id` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tr` FOREIGN KEY (`tribe_id`) REFERENCES `tribe` (`tribe_id`);

--
-- Constraints for table `student_assessment`
--
ALTER TABLE `student_assessment`
  ADD CONSTRAINT `fk_c` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cs` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sa` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_t` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
  ADD CONSTRAINT `fk_adm_no_std_att` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
  ADD CONSTRAINT `fk_att_adm_no` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
  ADD CONSTRAINT `fk_att_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`),
  ADD CONSTRAINT `fk_std_att_rec` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
  ADD CONSTRAINT `fk_term_att` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`);

--
-- Constraints for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
  ADD CONSTRAINT `fk_adm_no_roll_call` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
  ADD CONSTRAINT `fk_atid` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
  ADD CONSTRAINT `fk_dorm_roll_call` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`);

--
-- Constraints for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD CONSTRAINT `fk_student_subject` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usumbufu` FOREIGN KEY (`class_stream_id`,`subject_id`) REFERENCES `class_stream_subject` (`class_stream_id`, `subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
  ADD CONSTRAINT `fk_etype` FOREIGN KEY (`etype_id`) REFERENCES `exam_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ssa` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ssa_cs` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ssa_s` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tssa` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_subject_score_position`
--
ALTER TABLE `student_subject_score_position`
  ADD CONSTRAINT `fk_csssp` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cssssp` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sssp` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tssp` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tssub` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE;

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `fk_subject_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`);

--
-- Constraints for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
  ADD CONSTRAINT `fk_subject_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_teacher_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_topic`
--
ALTER TABLE `sub_topic`
  ADD CONSTRAINT `fk_subtopic_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subtopic_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subtopic_topic` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `super_admin_module`
--
ALTER TABLE `super_admin_module`
  ADD CONSTRAINT `fk_am` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`),
  ADD CONSTRAINT `fk_am_id` FOREIGN KEY (`id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `super_admin_permission`
--
ALTER TABLE `super_admin_permission`
  ADD CONSTRAINT `fk_admin_id` FOREIGN KEY (`id`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `fk_admin_permission` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suspended_students`
--
ALTER TABLE `suspended_students`
  ADD CONSTRAINT `fk_suspension` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `fk_teacher` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher_duty_roaster`
--
ALTER TABLE `teacher_duty_roaster`
  ADD CONSTRAINT `fk_teacher_duty` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tod_roaster` FOREIGN KEY (`did`) REFERENCES `duty_roaster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
  ADD CONSTRAINT `fk_ta_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ta_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ta_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ta_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ta_term` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
  ADD CONSTRAINT `fk_journal_subtopic` FOREIGN KEY (`subTopicID`) REFERENCES `sub_topic` (`subtopic_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ta_period_no` FOREIGN KEY (`period_no`) REFERENCES `period` (`period_no`),
  ADD CONSTRAINT `fk_tatt_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_log`
--
ALTER TABLE `teaching_log`
  ADD CONSTRAINT `fk_log_dept` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tod_daily_routine_tbl`
--
ALTER TABLE `tod_daily_routine_tbl`
  ADD CONSTRAINT `fk_duty_roaster` FOREIGN KEY (`did`) REFERENCES `teacher_duty_roaster` (`did`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
