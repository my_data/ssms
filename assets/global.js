/*//Global Variable to be used by scripts
var base_url = window.location.origin;


*/
$('input#id1').on('keyup', function() {
    var id = $('input#id1').val();
    //alert(id);
    
    if($.trim(id) != ''){
      $.post('classes/get_class_name', {id: id}, function(data) {
        $('div#name-data').text(data);
        document.getElementById('assignment1').value = data;
      });
    }
  });

  //Try this
  $('input#id2').on('keyup', function() {
    var id = $('input#id2').val();
    //alert(id);
    
    if($.trim(id) != ''){
      $.post('classes/get_class_name', {id: id}, function(data) {
        $('div#name-data').text(data);
        document.getElementById('assignment2').value = data;
      });
    }
  });

  $('input#id3').on('keyup', function() {
    var id = $('input#id3').val();
    //alert(id);
    
    if($.trim(id) != ''){
      $.post('classes/get_class_name', {id: id}, function(data) {
        $('div#name-data').text(data);
        document.getElementById('assignment3').value = data;
      });
    }
  });

//****************************************************************************

  $("#class_name").on("change", function(){ //listen when the option change   
      var optionValue = $('#class_name').val();   //get the new value

      if($.trim(optionValue) != ''){
        $.post('http://localhost/app/classes/get_class_name', {id: optionValue}, function(data) {
          //$('div#name-data').text(data);
          document.getElementById('class_name_value').value = data;
          //alert(data);
        });
      }
  });

  $("#stream").on("change", function(){ //listen when the option change   
      var optionValue = $('#stream').val();   //get the new value

      if($.trim(optionValue) != ''){
        $.post('http://localhost/app/classes/get_stream', {id: optionValue}, function(data) {
          //$('div#name-data').text(data);
          document.getElementById('stream_name_value').value = data;
          //alert(data);
        });
      }
  });



//****************************************************************************


  function show_element(){
    
    var staff_type = document.getElementById('staff_type').value;

    var select_element = document.getElementById('select_element');

    if(staff_type == 'T'){       
       select_element.style.display = "block";
       //alert($staff_type);
    }
    else{
      select_element.style.display = "none";
    }
  }

  function hide_element(target) {
    //alert(target);
    document.getElementById(target).style.display = 'none';
  }




  //START FORM PHASE
  function _(x){
    return document.getElementById(x);
  }

  var adm_no, fn, mn, ln, sex, dob, tribe, religion, disabled, disability, transferred, admission_date, former_school, dormitory, class_, region, box, fg_firstname, fg_middlename, fg_lastname, fg_gender, fg_occupation, fg_relationship, fg_phone, fg_email, fg_address, fg_region;
  function processPersonalInfo(){
    adm_no = _("admission_no").value;
    fn = _("firstname").value;
    nameRegEx = /[A-Z][a-z]{2,}/;
    //mn = _("middlename").value;
    //sex = _("gender").value;
    ln = _("lastname").value;
    dob = _("dob").value;
    tribe = _("tribe").value;
    religion = _("religion").value;

    if(adm_no.length == 7){
      if(nameRegEx.test(fn)){
        if(nameRegEx.test(ln)){
          if(tribe.length > 0 && religion.length > 0 && dob.length > 0){
            _("personalInfoPhase").style.display = "none";
            _("contactInfoPhase").style.display = "block";
            //Progress Bar
            _("progressBar").value = 25;
            _("status").innerHTML = /*"Phase 2 of 4"*/"25%";
          }
          else{
            alert("Please fill in the fields");
          }
        }
        else{
          alert("Lastname should be atleast 3 letters");
        }
      }
      else{
        alert("firstname should be atleast 3 letters");
      }
    }
    else{
      alert("admisson_no should be a seven digit natural number");
    }
  }

  function processContactInfo(){
    region = _("region").value;
    box = _("box").value;
    boxRegEx = /[1-9]/;

    if(region.length > 0 && boxRegEx.test(box)){
      _("contactInfoPhase").style.display = "none";
      _("admissionInfoPhase").style.display = "block";
      //Progress Bar
      _("progressBar").value = 50;
      _("status").innerHTML = /*"Phase 2 of 4"*/"50%";
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToPersonalInfo(){
    _("contactInfoPhase").style.display = "none";
    _("personalInfoPhase").style.display = "block";
  }

  function processAdmissionInfo(){
    disabled = (_("disabled").checked == true) ? "YES" : "NO";
    transferred = (_("transferred").checked == true) ? "YES" : "NO";
    admission_date = _("admission_date").value;
    disability = (disabled == "NO") ? "NONE" : _("disability").value;
    former_school = _("former_school").value;
    dormitory = _("dormitory").value;
    class_ = _("class_").value;

    if(former_school.length > 0){
      _("admissionInfoPhase").style.display = "none";
      _("first_guardian_phase").style.display = "block";
      //Progress Bar
      _("progressBar").value = 75;
      _("status").innerHTML = /*"Phase 2 of 4"*/"75%";
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToContactInfo(){
    _("admissionInfoPhase").style.display = "none";
    _("contactInfoPhase").style.display = "block";
  }

  function process_first_guardian_phase(){
    fg_firstname = _("fg_firstname").value;
    fg_middlename = _("fg_middlename").value;
    fg_lastname = _("fg_lastname").value;
    fg_gender = (_("fg_gender").checked == "Male") ? "Male" : "Female";
    fg_occupation = _("fg_occupation").value;
    fg_relationship = _("fg_relationship").value;
    fg_phone = _("fg_phone").value;
    fg_email = _("fg_email").value;
    fg_address = _("fg_address").value;
    fg_region = _("fg_region").value;

    if(fg_firstname.length > 0 && fg_lastname.length > 0 && fg_gender.length > 0 && fg_occupation.length > 0 && fg_relationship.length > 0 && fg_phone.length > 0 && fg_address.length > 0 && fg_region.length > 0){
      _("first_guardian_phase").style.display = "none";
      _("review_all_data").style.display = "block";
      //Progress Bar
      _("progressBar").value = 100;
      _("status").innerHTML = /*"Overview"*/"100%";

      _("show_admission_no").innerHTML = adm_no;
      _("show_firstname").innerHTML = fn;
      //_("show_middlename").innerHTML = mn;
      _("show_lastname").innerHTML = ln;
      _("show_dob").innerHTML = dob;
      _("show_tribe").innerHTML = tribe;
      _("show_religion").innerHTML = religion;
      _("show_region").innerHTML = region;
      _("show_box").innerHTML = box;
      _("show_disabled").innerHTML = disabled;
      _("show_disability").innerHTML = disability;
      _("show_transferred").innerHTML = transferred;
      _("show_former_school").innerHTML = former_school;
      _("show_dormitory").innerHTML = dormitory;
      _("show_class_").innerHTML = class_;
      _("show_admission_date").innerHTML = admission_date;
      _("show_fg_firstname").innerHTML = fg_firstname;
      _("show_fg_middlename").innerHTML = fg_middlename;
      _("show_fg_lastname").innerHTML = fg_lastname;
      _("show_fg_gender").innerHTML = fg_gender;
      _("show_fg_occupation").innerHTML = fg_occupation;
      _("show_fg_relationship").innerHTML = fg_relationship;
      _("show_fg_phone").innerHTML = fg_phone;
      _("show_fg_email").innerHTML = fg_email;
      _("show_fg_address").innerHTML = fg_address;
      _("show_fg_region").innerHTML = fg_region;
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToAdmissionPhase(){
    _("first_guardian_phase").style.display = "none";
    _("admissionInfoPhase").style.display = "block";
  }

  /*function process_second_guardian_phase(){
    sg_firstname = _("sg_firstname").value;
    sg_middlename = _("sg_middlename").value;
    sg_lastname = _("sg_lastname").value;
    sg_occupation = _("sg_occupation").value;
    sg_relationship = _("sg_relationship").value;
    sg_phone = _("sg_phone").value;
    sg_email = _("sg_email").value;
    sg_address = _("sg_address").value;
    sg_region = _("sg_region").value;

    if(sg_firstname.length > 4){
      _("second_guardian_phase").style.display = "none";
      _("review_all_data").style.display = "block";
    }
    else{
      alert("Please fill in the fields");
    }
  }*/

    function submitData(){
      _("student_registration_form").method = "post";
      _("student_registration_form").action = base_url+"students/new_student";
      _("student_registration_form").submit();
    }

    function disabledSelectElement(disabled, select_element){
      status = _(disabled).value;
      if(status == "No"){
        _(select_element).style.display = "none";
      }
      else{
        _(select_element).style.display = "block";
      }
    }


  //START STAFF CODE
  var staff_id, staff_fn, staff_mn, staff_ln, gender, staff_dob, marital_status, staff_email, phone_no, staff_type, subject_teaching, nok_fn, nok_ln, nok_gender, nok_rel_type, nok_phone, nok_box, nok_region;
  function processStaffPersonalInfo(){
    staff_id = _("staff_id").value;
    staff_fn = _("firstname").value;
    nameRegEx = /[A-Z][a-z]{3,}/;

    staff_mn = _("middlename").value;
    gender = (_("gender").checked == "Male") ? "Male" : "Female";
    staff_ln = _("lastname").value;
    staff_dob = _("dob").value;
    marital_status = _("marital_status").value;

    if(staff_id.length == 9){
      if(nameRegEx.test(staff_fn)){
        if(nameRegEx.test(staff_ln)){
          if(gender.length > 0 && marital_status.length > 0 && staff_dob.length > 0){
            _("staffPersonalInfoPhase").style.display = "none";
            _("staffContactInfoPhase").style.display = "block";
          }
          else{
            alert("Please fill in the fields");
          }
        }
        else{
          alert("lastname error");
        }
      }
      else{
        alert("firstname error");
      }
    }
    else{
      alert("Staff ID error");
    }
  }

  function processStaffContactInfo(){
    phone_no = _("phone_no").value;
    staff_email = _("email").value;

    if(phone_no.length > 0){
      _("staffContactInfoPhase").style.display = "none";
      _("staffRolePhase").style.display = "block";
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToStaffPersonalInfo(){
    _("staffContactInfoPhase").style.display = "none";
    _("staffPersonalInfoPhase").style.display = "block";
  }

  function processStaffRoleInfo(){
    subject_teaching = "";
    staff_type = _("staff_type").value;

    if(staff_type.length > 0){
      _("staffRolePhase").style.display = "none";
      _("staffNokInfoPhase").style.display = "block";
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToStaffContactInfo(){
    _("staffRolePhase").style.display = "none";
    _("staffContactInfoPhase").style.display = "block";
  }

  function processStaffNokInfoPhase(){
    nok_fn = _("nok_fn").value;
    nok_ln = _("nok_ln").value;

    nok_gender = (_("nok_gender").checked == "Male") ? "Male" : "Female";
    nok_rel_type = _("nok_rel_type").value;
    nok_phone = _("nok_phone").value;
    nok_box = _("nok_box").value;    
    nok_region = _("nok_region").value;

    if(nok_fn.length > 0){
      _("staffNokInfoPhase").style.display = "none";
      _("previewPhase").style.display = "block";


      _("show_staff_id").innerHTML = staff_id;
      _("show_firstname").innerHTML = staff_fn;
      _("show_middlename").innerHTML = staff_mn;
      _("show_lastname").innerHTML = staff_ln;
      _("show_staff_dob").innerHTML = staff_dob;
      _("show_staff_gender").innerHTML = gender;
      _("show_marital_status").innerHTML = marital_status;
      _("show_staff_email").innerHTML = staff_email;
      _("show_phone_no").innerHTML = phone_no;
      _("show_staff_type").innerHTML = staff_type;
      //_("show_subject_teaching").innerHTML = subject_teaching;
      _("show_nok_fn").innerHTML = nok_fn;
      _("show_nok_ln").innerHTML = nok_ln;
      _("show_nok_gender").innerHTML = nok_gender;
      _("show_nok_rel_type").innerHTML = nok_rel_type;
      _("show_nok_phone").innerHTML = nok_phone;
      _("show_nok_box").innerHTML = nok_box;
      _("show_nok_region").innerHTML = nok_region;
    }
    else{
      alert("Please fill in the fields");
    }
  }

  function backToStaffRoleInfo(){
    _("staffNokInfoPhase").style.display = "none";
    _("staffRolePhase").style.display = "block";
  }


  function submitStaffData(){
    _("staffRegistrationForm").method = "post";
    _("staffRegistrationForm").action = base_url+"staffs/new_staff";
    _("staffRegistrationForm").submit();
  }


  function disabledSelectElementStaff(disabled, select_element){
    status = _(disabled).value;
    if(status == "T"){
      _(select_element).style.display = "block";
    }
    else{
      _(select_element).style.display = "none";
    }
  }


  //######################################################
  function authenticateUser(){
    var password = prompt('Enter Password', "");
    var asterisks = (new Array(password.length+1).join("*"));
    alert(asterisks);
    if(password == 'vladimir'){
      location.href = base_url + 'admin/audit';
    }
    else if(password.length > 0 && password != 'vladimir'){
      alert('Invalid Password');
      location.href = base_url + 'home';
    }   
  }

