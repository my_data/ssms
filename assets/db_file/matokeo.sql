DROP TRIGGER if exists after_insert_on_student_subject_assessment_trigger//
CREATE TRIGGER after_insert_on_student_subject_assessment_trigger AFTER INSERT ON student_subject_assessment FOR EACH ROW
BEGIN DECLARE check_exist INT DEFAULT 0;
DECLARE darasa_id VARCHAR(10);
DECLARE exam_type VARCHAR(10);
DECLARE avg FLOAT(5, 2);
DECLARE overall_avg FLOAT(5, 2);
DECLARE grd VARCHAR(2);
DECLARE overall_grade VARCHAR(2);
DECLARE lvl VARCHAR(10);
DECLARE e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE sum_non_e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE get_ssa_id BIGINT(20) DEFAULT 0;

SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id INTO check_exist;

SELECT SUM(marks) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id INTO e4_marks;
SELECT SUM(marks)/COUNT(etype_id) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO sum_non_e4_marks;
IF e4_marks IS NULL THEN
SET e4_marks = 0;
END IF;
IF sum_non_e4_marks IS NULL THEN
SET sum_non_e4_marks = 0;
END IF;

IF check_exist = 0 THEN
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;
#SELECT ROUND(((SELECT marks FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id) + SUM(marks)/COUNT(etype_id))/2, 2) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO avg;
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF NEW.etype_id = "E01" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_one, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E02" THEN
INSERT INTO student_subject_score_position (admission_no, midterm, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E03" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_two, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E04" THEN
INSERT INTO student_subject_score_position (admission_no, terminal, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
REPLACE INTO student_assessment (admission_no, class_id, class_stream_id, average, grade, term_id) VALUES (NEW.admission_no, darasa_id, NEW.class_stream_id, overall_avg, overall_grade, NEW.term_id);
END IF;
IF check_exist > 0 THEN
IF NEW.etype_id = "E01" THEN
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E02" THEN
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E03" THEN
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E04" THEN
UPDATE student_subject_score_position SET terminal=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;
#SELECT ROUND(((SELECT marks FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id) + SUM(marks)/COUNT(etype_id))/2, 2) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN	SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id);

SELECT ssa_id FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id=NEW.etype_id AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO get_ssa_id;
INSERT INTO audit_student_subject_assessment (ssa_id, change_type, change_by, change_time, marks) VALUES(get_ssa_id, "ADD", @user_id, NOW(), NEW.marks);
END IF;














BEGIN DECLARE v_finished INTEGER DEFAULT 0; DECLARE i INT DEFAULT 0; DECLARE adm_no VARCHAR(10); 
DECLARE student_admission_no CURSOR FOR SELECT admission_no FROM student_subject_score_position WHERE term_id=t_id AND subject_id=somo_id ORDER BY average DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
OPEN student_admission_no;  
get_admission: LOOP FETCH student_admission_no INTO adm_no; 
IF v_finished = 1 THEN 
 LEAVE get_admission;
END IF;

SET i = i + 1;
UPDATE student_subject_score_position SET subject_rank=i WHERE admission_no=adm_no AND term_id=t_id AND subject_id=somo_id;

END LOOP get_admission;
CLOSE student_admission_no;
END


DROP TRIGGER IF EXISTS after_update_on_staff_status_trigger//
CREATE TRIGGER after_update_on_staff_status_trigger
BEFORE UPDATE ON staff_id
FOR EACH ROW
BEGIN DECLARE x INT DEFAULT 0; 
IF NEW.status = "inactive" THEN  
SELECT COUNT(*) FROM admin_group_permission WHERE permission_id=(SELECT permission_id FROM permission WHERE description="Deactivate Account") AND admin_group_id=(SELECT admin_group_id FROM staff_admin_group WHERE staff_id=@user_id) INTO x; 
IF x > 0 THEN 
DELETE FROM role WHERE staff_id=NEW.staff_id; 
UPDATE class_stream SET teacher_id="" WHERE teacher_id=NEW.staff_id; 
UPDATE department SET staff_id="" WHERE staff_id=NEW.staff_id; 
UPDATE dormitory SET teacher_id="" WHERE teacher_id=NEW.staff_id; 
INSERT INTO audit_staff_tbl (staff_id, change_by, change_type, change_time, source) VALUES(NEW.staff_id, @user_id, "EDIT", NOW(), @ip_address); 
ELSE SIGNAL SQLSTATE '45000' SET message_text = "You do not have the privilege to deactivate accounts"; 
END IF; 
END IF; 
IF (OLD.status = "inactive" && NEW.status = "active") THEN 
SELECT COUNT(*) FROM admin_group_permission WHERE permission_id=(SELECT permission_id FROM permission WHERE description="Activate Account") AND admin_group_id=(SELECT admin_group_id FROM staff_admin_group WHERE staff_id=@user_id) INTO x;  
IF x = 0 THEN SIGNAL SQLSTATE '45000' set message_text = "You do not have the privilege to activate accounts"; 
ELSE 
INSERT INTO audit_staff_tbl (staff_id, change_by, change_type, change_time, source) VALUES(NEW.staff_id, @user_id, "EDIT", NOW(), @ip_address); 
END IF; 
END IF; 
IF (OLD.status = NEW.status) THEN 
INSERT INTO audit_staff_tbl (staff_id, change_by, change_type, change_time, source) VALUES(NEW.staff_id, @user_id, "EDIT", NOW(), @ip_address); 
END IF; 
END







BEGIN 
DECLARE v_finished INTEGER DEFAULT 0; 
DECLARE tid varchar(10) DEFAULT "";
DECLARE e_name varchar(100) DEFAULT "";
DEClARE teacher_cursor CURSOR FOR SELECT staff_id FROM teacher; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
SELECT exam_name FROM exam_type WHERE id=NEW.id INTO e_name;
OPEN teacher_cursor;  
get_teacher_id: LOOP FETCH teacher_cursor INTO tid;  
IF v_finished = 1 THEN 
LEAVE get_teacher_id; 
END IF; 
REPLACE INTO notification (staff_id, msg_id, msg, time_) 
VALUES(tid, 9, concat("Current exams: ", e_name), NOW());  
END LOOP get_teacher_id; 
CLOSE teacher_cursor; 
END


CREATE PROCEDURE send_notification_to_tod_after_comment_procedure(IN tarehe DATE)
BEGIN
DECLARE v_finished INTEGER DEFAULT 0; 
DECLARE tid varchar(10) DEFAULT "";
DEClARE teacher_cursor CURSOR FOR SELECT teacher_id FROM teacher_duty_roaster WHERE did=(SELECT id FROM duty_roaster WHERE start_date <= tarehe AND end_date >= tarehe);
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 

OPEN teacher_cursor;  
get_teacher_id: LOOP FETCH teacher_cursor INTO tid;  
IF v_finished = 1 THEN 
LEAVE get_teacher_id; 
END IF;

REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(tid, 9, "Comment have been added in one of your report", NOW());

END LOOP get_teacher_id; 
CLOSE teacher_cursor; 
END






BEGIN 
DECLARE c_name VARCHAR(20);  
SELECT concat(class_name, " ", stream) FROM class_level JOIN class_stream USING(class_id) 
WHERE class_stream_id=NEW.class_stream_id INTO c_name; 
IF(OLD.teacher_id != NEW.teacher_id) THEN 
DELETE FROM staff_role WHERE staff_id IN(OLD.teacher_id, NEW.teacher_id) AND role_type_id=4; 
IF (NEW.teacher_id != "" && OLD.teacher_id != NEW.teacher_id) THEN 
UPDATE class_teacher_history 
SET end_date=NOW() 
WHERE class_stream_id=OLD.class_stream_id AND 
end_date IS NULL; 
INSERT INTO class_teacher_history (class_stream_id, teacher_id, start_date) 
VALUES(OLD.class_stream_id, NEW.teacher_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(4, NEW.teacher_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) 
VALUES(NEW.teacher_id, 1, concat("You have been assigned as class teacher of: ", c_name), NOW()); 
END IF; 
END IF; 
END





































DROP PROCEDURE assign_class_teacher_procedure//
CREATE PROCEDURE assign_class_teacher_procedure(IN csid VARCHAR(10), IN t_id VARCHAR(10))
BEGIN 
DECLARE c_name VARCHAR(20) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT teacher_id FROM class_stream WHERE class_stream_id=csid INTO old_tid;
IF old_tid != "" THEN 
SELECT concat(class_name, " ", stream) FROM class_level JOIN class_stream USING(class_id) WHERE class_stream_id=csid INTO c_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=4; 
UPDATE class_teacher_history SET end_date=NOW() WHERE (class_stream_id=csid AND end_date IS NULL) || (teacher_id=t_id AND end_date IS NULL);
INSERT INTO class_teacher_history (class_stream_id, teacher_id, start_date) VALUES(csid, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(4, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 1, concat("You have been assigned as class teacher of: ", c_name), NOW()); 
ELSE
SELECT concat(class_name, " ", stream) FROM class_level JOIN class_stream USING(class_id) WHERE class_stream_id=csid INTO c_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=4; 
UPDATE class_teacher_history SET end_date=NOW() WHERE teacher_id=t_id AND end_date IS NULL;
INSERT INTO class_teacher_history (class_stream_id, teacher_id, start_date) VALUES(csid, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(4, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 1, concat("You have been assigned as class teacher of: ", c_name), NOW()); 
END IF;
END
//


DROP PROCEDURE assign_dorm_master_procedure//
CREATE PROCEDURE assign_dorm_master_procedure(IN dm_id VARCHAR(10), IN t_id VARCHAR(10))
BEGIN 
DECLARE d_name VARCHAR(100) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT teacher_id FROM dormitory WHERE dorm_id=dm_id INTO old_tid;
IF old_tid != "" THEN 
SELECT dorm_name FROM dormitory WHERE dorm_id=dm_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=5; 
UPDATE dorm_master_history SET end_date=NOW() WHERE (dorm_id=dm_id AND end_date IS NULL) || (teacher_id=t_id AND end_date IS NULL);
INSERT INTO dorm_master_history (dorm_id, teacher_id, start_date) VALUES(dm_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(5, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 5, concat("You have been assigned as class teacher of: ", d_name), NOW()); 
ELSE
SELECT dorm_name FROM dormitory WHERE dorm_id=dm_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=5; 
UPDATE dorm_master_history SET end_date=NOW() WHERE teacher_id=t_id AND end_date IS NULL;
INSERT INTO dorm_master_history (dorm_id, teacher_id, start_date) VALUES(dm_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(5, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 5, concat("You have been assigned as dormitory master of: ", d_name), NOW()); 
END IF;
END
//




DROP PROCEDURE assign_hod_procedure//
CREATE PROCEDURE assign_hod_procedure(IN hod_id VARCHAR(10), IN t_id VARCHAR(10))
BEGIN 
DECLARE d_name VARCHAR(100) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT staff_id FROM department WHERE dept_id=hod_id INTO old_tid;
IF old_tid != "" THEN 
SELECT dept_name FROM department WHERE dept_id=hod_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=3; 
UPDATE hod_history SET end_date=NOW() WHERE (dept_id=hod_id AND end_date IS NULL) || (staff_id=t_id AND end_date IS NULL);
INSERT INTO hod_history (dept_id, staff_id, start_date) VALUES(hod_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(3, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 3, concat("You have been assigned as class teacher of: ", d_name), NOW()); 
ELSE
SELECT dept_name FROM department WHERE dept_id=hod_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=3; 
UPDATE hod_history SET end_date=NOW() WHERE staff_id=t_id AND end_date IS NULL;
INSERT INTO hod_history (dept_id, staff_id, start_date) VALUES(hod_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(3, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 3, concat("You have been assigned as HOD of: ", d_name), NOW()); 
END IF;
END
//