-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2018 at 05:16 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `after_shifting_class_stream_procedure`(IN `adm_no` VARCHAR(10), IN `csid` VARCHAR(10))
BEGIN 
DECLARE a INT DEFAULT 0;  DECLARE lvl VARCHAR(10);
SELECT COUNT(*) FROM class_stream_subject WHERE class_stream_id=csid INTO a;
SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id=csid) INTO lvl;


IF (lvl = "A'Level" && a < 3 || lvl = "O'Level" && a < 7) THEN
SIGNAL SQLSTATE '45000' set message_text = "This class does not have any subjects at the moment";
ELSE
DELETE FROM student_subject WHERE admission_no=adm_no;
INSERT INTO student_subject SELECT adm_no, subject_id, "active", class_stream_subject.class_stream_id FROM student JOIN class_stream_subject WHERE class_stream_subject.class_stream_id =csid AND class_stream_subject.subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="compulsory") AND admission_no=adm_no;
END IF; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `after_update_class_stream_subject_procedure`(IN `csid` VARCHAR(10))
BEGIN INSERT INTO student_subject SELECT admission_no, subject_id, "active", class_stream_subject.class_stream_id FROM student JOIN class_stream_subject WHERE class_stream_subject.class_stream_id =csid  AND class_stream_subject.subject_id IN(SELECT subject_id FROM subject WHERE subject_choice="compulsory") AND admission_no IN(SELECT admission_no FROM student WHERE class_stream_id=csid); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_class_teacher_procedure`(IN `csid` VARCHAR(10), IN `t_id` VARCHAR(10))
BEGIN 
DECLARE c_name VARCHAR(20) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT teacher_id FROM class_stream WHERE class_stream_id=csid INTO old_tid;
IF old_tid != "" THEN 
SELECT concat(class_name, " ", stream) FROM class_level JOIN class_stream USING(class_id) WHERE class_stream_id=csid INTO c_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=4; 
UPDATE class_teacher_history SET end_date=NOW() WHERE (class_stream_id=csid AND end_date IS NULL) || (teacher_id=t_id AND end_date IS NULL);
INSERT INTO class_teacher_history (class_stream_id, teacher_id, start_date) VALUES(csid, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(4, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 1, concat("You have been assigned as class teacher of: ", c_name), NOW()); 
ELSE
SELECT concat(class_name, " ", stream) FROM class_level JOIN class_stream USING(class_id) WHERE class_stream_id=csid INTO c_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=4; 
UPDATE class_teacher_history SET end_date=NOW() WHERE teacher_id=t_id AND end_date IS NULL;
INSERT INTO class_teacher_history (class_stream_id, teacher_id, start_date) VALUES(csid, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(4, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 1, concat("You have been assigned as class teacher of: ", c_name), NOW()); 
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_dorm_master_procedure`(IN `dm_id` VARCHAR(10), IN `t_id` VARCHAR(10))
BEGIN 
DECLARE d_name VARCHAR(100) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT teacher_id FROM dormitory WHERE dorm_id=dm_id INTO old_tid;
IF old_tid != "" THEN 
SELECT dorm_name FROM dormitory WHERE dorm_id=dm_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=5; 
UPDATE dorm_master_history SET end_date=NOW() WHERE (dorm_id=dm_id AND end_date IS NULL) || (teacher_id=t_id AND end_date IS NULL);
INSERT INTO dorm_master_history (dorm_id, teacher_id, start_date) VALUES(dm_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(5, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 5, concat("You have been assigned as class teacher of: ", d_name), NOW()); 
ELSE
SELECT dorm_name FROM dormitory WHERE dorm_id=dm_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=5; 
UPDATE dorm_master_history SET end_date=NOW() WHERE teacher_id=t_id AND end_date IS NULL;
INSERT INTO dorm_master_history (dorm_id, teacher_id, start_date) VALUES(dm_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(5, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 5, concat("You have been assigned as dormitory master of: ", d_name), NOW()); 
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_hod_procedure`(IN `hod_id` VARCHAR(10), IN `t_id` VARCHAR(10))
BEGIN 
DECLARE d_name VARCHAR(100) DEFAULT "";  
DECLARE old_tid VARCHAR(10) DEFAULT ""; 
SELECT staff_id FROM department WHERE dept_id=hod_id INTO old_tid;
IF old_tid != "" THEN 
SELECT dept_name FROM department WHERE dept_id=hod_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=3; 
UPDATE hod_history SET end_date=NOW() WHERE (dept_id=hod_id AND end_date IS NULL) || (staff_id=t_id AND end_date IS NULL);
INSERT INTO hod_history (dept_id, staff_id, start_date) VALUES(hod_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(3, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 3, concat("You have been assigned as class teacher of: ", d_name), NOW()); 
ELSE
SELECT dept_name FROM department WHERE dept_id=hod_id INTO d_name; 
DELETE FROM staff_role WHERE staff_id IN(old_tid, t_id) AND role_type_id=3; 
UPDATE hod_history SET end_date=NOW() WHERE staff_id=t_id AND end_date IS NULL;
INSERT INTO hod_history (dept_id, staff_id, start_date) VALUES(hod_id, t_id, NOW()); 
INSERT INTO staff_role(role_type_id, staff_id) VALUES(3, t_id); 
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(t_id, 3, concat("You have been assigned as HOD of: ", d_name), NOW()); 
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `calculate_student_rank_procedure`(IN `cid` VARCHAR(10) CHARSET utf8, IN `t_id` INT, IN `csid` VARCHAR(10) CHARSET utf8)
    NO SQL
BEGIN 
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE i INT DEFAULT 0; 
DECLARE temp_rank INT DEFAULT 1; 
DECLARE adm_no VARCHAR(10); 
DECLARE avg FLOAT(5,2) DEFAULT 0.00; 
DECLARE temp_avg FLOAT(5,2) DEFAULT 0.00; 
DECLARE student_admission_no CURSOR FOR 
SELECT admission_no FROM student_assessment 
WHERE term_id=t_id AND class_id=cid ORDER BY average DESC; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;  
OPEN student_admission_no;   
get_admission: 
LOOP FETCH student_admission_no INTO adm_no;  
IF v_finished = 1 THEN   
LEAVE get_admission; 
END IF;  
SET i = i + 1; 
SELECT average FROM student_assessment 
WHERE admission_no=adm_no AND term_id=t_id AND class_id=cid INTO avg;

IF avg <> temp_avg THEN

UPDATE student_assessment 
SET class_rank=i WHERE admission_no=adm_no AND term_id=t_id AND class_id=cid AND class_stream_id=csid;
SET temp_rank = i; 

ELSE
UPDATE student_assessment 
SET class_rank=temp_rank WHERE admission_no=adm_no AND term_id=t_id AND class_id=cid AND class_stream_id=csid;

END IF;
SET temp_avg = avg;


END LOOP get_admission; 
CLOSE student_admission_no; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `calculate_subject_rank_procedure`(IN `somo_id` VARCHAR(10) CHARSET utf8, IN `t_id` INT, IN `csid` VARCHAR(10) CHARSET utf8, IN `cid` VARCHAR(10) CHARSET utf8)
BEGIN 
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE i INT DEFAULT 0; 
DECLARE temp_rank INT DEFAULT 1; 
DECLARE adm_no VARCHAR(10); 
DECLARE avg FLOAT(5,2) DEFAULT 0.00; 
DECLARE temp_avg FLOAT(5,2) DEFAULT 0.00; 
DECLARE student_admission_no CURSOR FOR 
SELECT admission_no FROM student_subject_score_position 
WHERE term_id=t_id AND subject_id=somo_id AND class_id=cid ORDER BY average DESC; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;  
OPEN student_admission_no;   
get_admission: 
LOOP FETCH student_admission_no INTO adm_no;  
IF v_finished = 1 THEN   
LEAVE get_admission; 
END IF;  
SET i = i + 1; 
SELECT average FROM student_subject_score_position 
WHERE admission_no=adm_no AND term_id=t_id AND subject_id=somo_id AND class_id=cid INTO avg;

IF avg <> temp_avg THEN

UPDATE student_subject_score_position 
SET subject_rank=i WHERE admission_no=adm_no AND term_id=t_id AND subject_id=somo_id AND class_stream_id=csid;
SET temp_rank = i; 

ELSE
UPDATE student_subject_score_position 
SET subject_rank=temp_rank WHERE admission_no=adm_no AND term_id=t_id AND subject_id=somo_id AND class_stream_id=csid;

END IF;
SET temp_avg = avg;


END LOOP get_admission; 
CLOSE student_admission_no; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `copy_to_students_enrollment_procedure`(IN `x` VARCHAR(10))
BEGIN REPLACE INTO enrollment SELECT admission_no, student.class_id, class_stream_id, academic_year FROM student  JOIN class_level ON(student.class_id = class_level.class_id) WHERE class_level.level=x AND status="active"; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `enable_exam_type_notification`(IN `etype_id` ENUM("E01","E02","E03","E04"))
BEGIN  DECLARE v_finished INTEGER DEFAULT 0;  DECLARE tid varchar(10) DEFAULT ""; DECLARE e_name varchar(100) DEFAULT ""; DEClARE teacher_cursor CURSOR FOR SELECT staff_id FROM teacher;  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;  SELECT exam_name FROM exam_type WHERE id=etype_id INTO e_name; OPEN teacher_cursor;   get_teacher_id: LOOP FETCH teacher_cursor INTO tid;   IF v_finished = 1 THEN  LEAVE get_teacher_id;  END IF;  REPLACE INTO notification (staff_id, msg_id, msg, time_)  VALUES(tid, 9, concat("Current exams: ", e_name), NOW());   END LOOP get_teacher_id;  CLOSE teacher_cursor;  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `extend_teaching_assignment_procedure`(IN `department_id` INT)
BEGIN DECLARE v_finished INTEGER DEFAULT 0; DECLARE csid varchar(10) DEFAULT ""; DEClARE class_stream_cursor CURSOR FOR SELECT class_stream_id FROM teaching_assignment WHERE dept_id=department_id; DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; OPEN class_stream_cursor;  get_class_stream_id: LOOP FETCH class_stream_cursor INTO csid;  IF v_finished = 1 THEN   LEAVE get_class_stream_id; END IF;  UPDATE teaching_assignment SET term_id=(SELECT term_id FROM term WHERE is_current="yes" AND aid=(SELECT id FROM academic_year WHERE class_level=(SELECT level FROM class_level WHERE class_id=(SELECT class_id FROM class_stream WHERE class_stream_id=csid)) AND status="current_academic_year")) WHERE dept_id=department_id AND class_stream_id=csid;  END LOOP get_class_stream_id; CLOSE class_stream_cursor; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_class_stream_id_procedure`(IN `sub_id` VARCHAR(10), IN `cid` VARCHAR(10))
BEGIN SELECT class_stream_id FROM class_stream_subject WHERE subject_id=sub_id and class_stream_id IN(SELECT class_stream_id FROM class_stream WHERE class_id=cid);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_into_academic_year_procedure`(IN `s_date` DATE, IN `e_date` DATE, IN `yr` VARCHAR(10), IN `level` VARCHAR(10))
BEGIN


DECLARE current_year VARCHAR(10);
DECLARE end_term_date DATE;
DECLARE tmp_end_term_date DATE;
DECLARE tmp_e_date DATE;
DECLARE o_lvl_aid INT;
DECLARE a_lvl_aid INT;
DECLARE count_cay INT;
DECLARE a_level VARCHAR(10) DEFAULT "A'Level";
DECLARE o_level VARCHAR(10) DEFAULT "O'Level";


UPDATE academic_year SET status="past_academic_year" WHERE class_level=level;
INSERT INTO academic_year (start_date, end_date, year, class_level) VALUES(s_date, e_date, yr, level);

SELECT DATE_ADD(s_date, INTERVAL 6 MONTH) INTO tmp_end_term_date;
SELECT DATE_ADD(tmp_end_term_date, INTERVAL -1 DAY) INTO end_term_date;

IF level = o_level THEN
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=o_level INTO o_lvl_aid;
UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=o_level) || aid IS NULL;
INSERT INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "first_term", o_lvl_aid, "yes");

UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=a_level) || aid IS NULL;
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=a_level INTO a_lvl_aid;

IF a_lvl_aid <> "" THEN
REPLACE INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "second_term", a_lvl_aid, "yes");
END IF;

UPDATE exam_type SET academic_year=o_lvl_aid;
END IF;

IF level = a_level THEN
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=a_level INTO a_lvl_aid;
UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=a_level) || aid IS NULL;
INSERT INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "first_term", a_lvl_aid, "yes");

UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=o_level) || aid IS NULL;
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=o_level INTO o_lvl_aid;

IF o_lvl_aid <> "" THEN
REPLACE INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "second_term", o_lvl_aid, "yes");
END IF;

UPDATE exam_type SET a_level_academic_year=a_lvl_aid;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_into_announcement_status_procedure`(IN `g_id` INT, IN `msg` LONGTEXT, IN `user_id` VARCHAR(10), IN `header` VARCHAR(255))
BEGIN insert into announcement (group_id, heading, msg, posted_by) values(g_id, header, msg, user_id); INSERT INTO announcement_status SELECT staff_id, group_id, (SELECT last_insert_id()), "unread" FROM staff_group WHERE group_id=g_id; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_into_student_subject_after_updating_stream_procedure`(IN `sid` VARCHAR(10), IN `sub_id` VARCHAR(10), IN `cid` VARCHAR(10))
BEGIN
DELETE FROM student_subject WHERE admission_no=sid;
INSERT INTO student_subject(admission_no, subject_id, class_stream_id)  select sid, subject_id, cid from subject join class_stream_subject using(subject_id) join class_stream using(class_stream_id) where subject_choice="compulsory" AND class_stream_id=cid UNION select sid, subject_id, cid FROM subject WHERE subject_id=sub_id AND subject_choice="optional";
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `my_classes_procedure`(IN `tid` VARCHAR(10))
BEGIN
select teaching_assignment.teacher_id, teaching_assignment.subject_id, subject_name, class_name, stream, start_date, end_date from teaching_assignment join subject using(subject_id) join class_stream using(class_stream_id)  join class_level on(class_level.class_id = class_stream.class_id) where teaching_assignment.teacher_id=tid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `password_change_reminder_procedure`()
BEGIN DECLARE user_id VARCHAR(10); DECLARE has_changed enum("T", "F"); DECLARE exit_loop BOOLEAN; DECLARE audit_password_change_cursor CURSOR FOR SELECT staff_id, user_changed FROM audit_password_reset_tbl WHERE user_changed="F"; DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE; OPEN audit_password_change_cursor; theLoop: LOOP FETCH audit_password_change_cursor INTO user_id, has_changed; IF exit_loop THEN CLOSE audit_password_change_cursor; LEAVE theLoop; END IF; IF has_changed <> "T" THEN REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(user_id, 7, "You are still using the default password, make sure you change it to protect your account", NOW()); ELSE ITERATE theLoop; END IF; END LOOP theLoop; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `promote_academic_year_procedure`(IN `x` VARCHAR(10))
BEGIN UPDATE student SET academic_year=(SELECT id from academic_year WHERE status="current_academic_year" AND class_level=x) WHERE class_id IN(SELECT class_id FROM class_level where class_level.level=x) AND status="active"; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `promote_class_id_procedure`(IN `x` VARCHAR(10), IN `cid` VARCHAR(10), IN `promote_cid` VARCHAR(10))
BEGIN IF cid="C01" THEN  delete from student_subject where admission_no IN(select admission_no from student join class_level using(class_id) where student.class_id="C01" and class_level.level=x AND status="active");  END IF; UPDATE student SET class_id=promote_cid WHERE class_id=(SELECT class_id FROM class_level WHERE class_level.level=x AND class_level.class_id=cid) AND status="active"; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `promote_class_id_procedure_new`(IN `class_lvl` VARCHAR(10))
BEGIN 
DECLARE v_finished INTEGER DEFAULT 0; DECLARE adm_no varchar(10) DEFAULT ""; DECLARE current_class_id varchar(10) DEFAULT ""; 
DECLARE admission_no_cursor CURSOR FOR SELECT admission_no FROM student WHERE class_id IN(SELECT class_id FROM class_level WHERE class_level.level=class_lvl) AND status="active"; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
OPEN admission_no_cursor;  
get_next_admission_no: LOOP FETCH admission_no_cursor INTO adm_no; 
IF v_finished = 1 THEN 
 LEAVE get_next_admission_no;
END IF;

SELECT class_id FROM student WHERE admission_no=adm_no INTO current_class_id;
IF current_class_id="C01" THEN  
DELETE FROM student_subject WHERE admission_no=adm_no; 
END IF; 
UPDATE student SET class_id=(SELECT next_class_id FROM class_level WHERE class_id=current_class_id) WHERE admission_no=adm_no AND status="active";

END LOOP get_next_admission_no;
CLOSE admission_no_cursor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `promote_class_stream_id_procedure`(IN `x` VARCHAR(10), IN `cid` VARCHAR(10), IN `stream_id` VARCHAR(10), IN `promote_to` VARCHAR(10))
BEGIN  IF cid="C01" THEN UPDATE student SET class_stream_id="" WHERE class_id IN(select class_id FROM class_level WHERE class_level.class_id="C01" AND class_level.level=x) AND status="active";  ELSEIF cid != "C01" THEN UPDATE student SET class_stream_id=promote_to WHERE class_stream_id=(SELECT class_stream_id FROM class_stream join class_level USING(class_id) WHERE class_level.level=x AND class_stream_id=stream_id) AND status="active";  END IF; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `promote_class_stream_id_procedure_new`(IN `class_lvl` VARCHAR(10))
BEGIN 
DECLARE v_finished INTEGER DEFAULT 0; DECLARE adm_no varchar(10) DEFAULT ""; DECLARE current_class_id varchar(10) DEFAULT ""; DECLARE current_class_stream_id varchar(10) DEFAULT ""; 
DECLARE admission_no_cursor CURSOR FOR SELECT admission_no FROM student WHERE class_id IN(SELECT class_id FROM class_level WHERE class_level.level=class_lvl) AND status="active"; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
OPEN admission_no_cursor;  
get_next_admission_no: LOOP FETCH admission_no_cursor INTO adm_no; 
IF v_finished = 1 THEN 
 LEAVE get_next_admission_no;
END IF;

SELECT class_id FROM student WHERE admission_no=adm_no INTO current_class_id;
SELECT class_stream_id FROM student WHERE admission_no=adm_no INTO current_class_stream_id;

IF current_class_id="C01" THEN  
UPDATE student SET class_stream_id=NULL WHERE admission_no=adm_no; 
ELSEIF current_class_id != "C01" THEN 
UPDATE student SET class_stream_id=(SELECT next_class_stream_id FROM class_stream WHERE class_stream_id=current_class_stream_id) WHERE admission_no=adm_no AND status="active";  
END IF;

END LOOP get_next_admission_no;
CLOSE admission_no_cursor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `select_class_stream_subjects`(IN `sid` VARCHAR(10), IN `cid` VARCHAR(10))
begin
INSERT INTO student_subject(admission_no, subject_id, class_stream_id)  select sid, subject_id, cid from subject join class_stream_subject using(subject_id) join class_stream using(class_stream_id) where subject_choice="compulsory" AND class_stream_id=cid;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `send_notification_to_tod_after_comment_procedure`(IN `tarehe` DATE)
BEGIN DECLARE v_finished INTEGER DEFAULT 0;  DECLARE tid varchar(10) DEFAULT ""; DEClARE teacher_cursor CURSOR FOR SELECT teacher_id FROM teacher_duty_roaster WHERE did=(SELECT id FROM duty_roaster WHERE start_date <= tarehe AND end_date >= tarehe); DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;   OPEN teacher_cursor;   get_teacher_id: LOOP FETCH teacher_cursor INTO tid;   IF v_finished = 1 THEN  LEAVE get_teacher_id;  END IF;  REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(tid, 9, "The TOD report has been commented", NOW());  END LOOP get_teacher_id;  CLOSE teacher_cursor;  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `test_automation`()
BEGIN

DECLARE current_year VARCHAR(10);
DECLARE s_date DATE;
DECLARE e_date DATE;
DECLARE end_term_date DATE;
DECLARE tmp_end_term_date DATE;
DECLARE tmp_e_date DATE;
DECLARE o_lvl_aid INT;
DECLARE o_level VARCHAR(10) DEFAULT "O'Level";
DECLARE a_lvl_aid INT;
DECLARE a_level VARCHAR(10) DEFAULT "A'Level";


SELECT concat((SELECT year((SELECT current_date()))),"-",(SELECT YEAR((SELECT DATE_ADD(current_date(), INTERVAL 1 YEAR))))) INTO current_year;
SELECT current_date() INTO s_date; 
SELECT DATE_ADD(current_date(), INTERVAL 1 YEAR) INTO tmp_e_date;
SELECT DATE_ADD(current_date(), INTERVAL 6 MONTH) INTO tmp_end_term_date;

SELECT DATE_ADD(tmp_e_date, INTERVAL -1 DAY) INTO e_date;
SELECT DATE_ADD(tmp_end_term_date, INTERVAL -1 DAY) INTO end_term_date;

UPDATE academic_year SET status="past_academic_year" WHERE class_level=a_level;
UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=a_level);
INSERT INTO academic_year (start_date, end_date, year, class_level) VALUES(s_date, e_date, current_year, a_level);
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=a_level INTO a_lvl_aid;
INSERT INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "first_term", a_lvl_aid, "yes");

UPDATE term SET is_current="no" WHERE aid IN(SELECT id FROM academic_year WHERE class_level=o_level);
SELECT id FROM academic_year WHERE status="current_academic_year" AND class_level=o_level INTO o_lvl_aid;
INSERT INTO term (begin_date, end_date, term_name, aid, is_current) VALUES(s_date, end_term_date, "second_term", o_lvl_aid, "yes");



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_duty_roaster_procedure`(IN `dr_id` INT)
BEGIN 
DECLARE c_date DATE; 
DECLARE s_date DATE; 
DECLARE e_date DATE; 


DECLARE v_finished INTEGER DEFAULT 0; 
DECLARE tod_id varchar(10) DEFAULT ""; 
DEClARE staff_cursor CURSOR FOR SELECT teacher_id FROM teacher_duty_roaster where did=dr_id; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 

SELECT current_date() INTO c_date; 
SELECT start_date FROM duty_roaster WHERE id=dr_id INTO s_date; 
SELECT end_date FROM duty_roaster WHERE id=dr_id INTO e_date; 

IF (s_date <= c_date && c_date <= e_date) THEN 
UPDATE duty_roaster SET is_current="F";
UPDATE duty_roaster SET is_current="T" WHERE id=dr_id;
DELETE FROM staff_role WHERE role_type_id=9; 
OPEN staff_cursor;  get_staff_id: 
LOOP FETCH staff_cursor INTO tod_id; 
IF v_finished = 1 THEN 
 LEAVE get_staff_id;
END IF;

INSERT INTO staff_role (role_type_id, staff_id) VALUES(9, tod_id);
REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(tod_id, 9, concat("Your are the teacher on duty, FROM: ", "<b>", s_date, "</b>", "TO: ", "<b>", e_date, "</b>"), NOW());

END LOOP get_staff_id;
CLOSE staff_cursor;


ELSE
SIGNAL SQLSTATE '45000' SET message_text='Unable to set the current duty roaster, the current date must lie between the duty roaster start and end dates';
END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `academic_year`
--

CREATE TABLE IF NOT EXISTS `academic_year` (
`id` int(5) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `year` varchar(10) NOT NULL,
  `class_level` enum('A''Level','O''Level') DEFAULT NULL,
  `status` enum('current_academic_year','past_academic_year') NOT NULL DEFAULT 'current_academic_year'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`id`, `start_date`, `end_date`, `year`, `class_level`, `status`) VALUES
(1, '2018-01-08', '2018-12-07', '2018', 'O''Level', 'current_academic_year'),
(3, '2018-07-01', '2019-06-23', '2018-2019', 'A''Level', 'current_academic_year');

--
-- Triggers `academic_year`
--
DELIMITER //
CREATE TRIGGER `before_adding_academic_year_trigger` BEFORE INSERT ON `academic_year`
 FOR EACH ROW BEGIN  DECLARE current_year VARCHAR(4);  DECLARE a_level_academic_year VARCHAR(10);  DECLARE current_month VARCHAR(20);  SELECT YEAR((select current_date())) INTO current_year;  SELECT concat((SELECT year((SELECT current_date()))),"-",(SELECT YEAR((SELECT DATE_ADD(current_date(), INTERVAL 1 YEAR))))) INTO a_level_academic_year;  SELECT MONTHNAME((select current_date())) INTO current_month;  IF (NEW.class_level = "O'Level") THEN  IF !(NEW.year = current_year && current_month = "January") THEN  SIGNAL SQLSTATE '45000' set message_text = "The current year do not correspond with the O'LEVEL academic year you are trying to create. Check your date settings";  END IF;  END IF;  IF (NEW.class_level = "A'Level") THEN  IF !(NEW.year = a_level_academic_year && current_month = "July") THEN  SIGNAL SQLSTATE '45000' set message_text = "The current year do not correspond with the A'LEVEL academic year you are trying to create. Check your date settings";  END IF;  END IF;  END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `academic_year_term_view`
--
CREATE TABLE IF NOT EXISTS `academic_year_term_view` (
`term_id` int(11)
,`is_current` enum('yes','no')
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`term_name` enum('first_term','second_term')
,`term_begin_date` date
,`term_end_date` date
);
-- --------------------------------------------------------

--
-- Table structure for table `accomodation_history`
--

CREATE TABLE IF NOT EXISTS `accomodation_history` (
  `admission_no` varchar(10) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `account_no` varchar(30) NOT NULL,
  `available_amount` bigint(20) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `month` varchar(10) NOT NULL,
  `action` enum('none','debit','credit') NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `accountant`
--

CREATE TABLE IF NOT EXISTS `accountant` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `active_students_view`
--
CREATE TABLE IF NOT EXISTS `active_students_view` (
`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE IF NOT EXISTS `activity_log` (
`id` bigint(20) NOT NULL,
  `activity` varchar(50) NOT NULL,
  `tableName` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `source` varchar(30) NOT NULL,
  `destination` varchar(10) DEFAULT NULL,
  `user_id` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `activity`, `tableName`, `time`, `source`, `destination`, `user_id`) VALUES
(1, 'VIEW USER ACTIVITY LOG', 'LOG', '2018-05-31 09:06:24', '::1', '', '1'),
(2, 'ADD QUALIFICATION', 'QUALIFICATION', '2018-05-31 09:08:30', '::1', '', '1'),
(3, 'REMOVE QUALIFICATION', 'QUALIFICATION', '2018-05-31 09:08:37', '::1', '', '1'),
(4, 'ADD QUALIFICATION', 'QUALIFICATION', '2018-05-31 09:09:40', '::1', '', '1'),
(5, 'REMOVE QUALIFICATION', 'QUALIFICATION', '2018-05-31 09:09:54', '::1', '', '1'),
(6, 'VIEW', 'STAFFS', '2018-05-31 09:10:06', '::1', '', '1'),
(7, 'UPDATE', 'staff', '2018-05-31 09:10:12', '::1', '', '1'),
(8, 'VIEW', 'STAFFS', '2018-05-31 09:10:12', '::1', '', '1'),
(9, 'UPDATE STAFF', 'STAFF', '2018-05-31 09:14:03', '::1', '', '1'),
(10, 'UPDATE STAFF', 'STAFF', '2018-05-31 09:14:11', '::1', '', '1'),
(11, 'UPDATE STAFF', 'STAFF', '2018-05-31 09:14:22', '::1', '', '1'),
(12, 'INSERT', 'ADD TOD', '2018-05-31 09:15:02', '::1', '', '1'),
(13, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:09', '::1', '', '1'),
(14, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:17', '::1', '', '1'),
(15, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:21', '::1', '', '1'),
(16, 'DELETE', 'DUTY ROASTER', '2018-05-31 09:15:24', '::1', '', '1'),
(17, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:24', '::1', '', '1'),
(18, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:29', '::1', '', '1'),
(19, 'INSERT', 'ADD TOD', '2018-05-31 09:15:46', '::1', '', '1'),
(20, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:15:51', '::1', '', '1'),
(21, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:16:09', '::1', '', '1'),
(22, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:16:57', '::1', '', '1'),
(23, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:17:56', '::1', '', '1'),
(24, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:18:02', '::1', '', '1'),
(25, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:18:27', '::1', '', '1'),
(26, 'INSERT', 'ADD TOD', '2018-05-01 09:18:59', '::1', '', '1'),
(27, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:19:05', '::1', '', '1'),
(28, 'INSERT', 'ADD TOD', '2018-05-01 09:19:17', '::1', '', '1'),
(29, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:19:22', '::1', '', '1'),
(30, 'SET CURRENT TODs', 'DUTY ROASTER', '2018-05-01 09:19:27', '::1', '', '1'),
(31, 'SELECT', 'DUTY ROASTER', '2018-05-01 09:19:27', '::1', '', '1'),
(32, 'DELETE', 'DUTY ROASTER', '2018-05-31 09:20:06', '::1', '', '1'),
(33, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:20:07', '::1', '', '1'),
(34, 'DELETE', 'DUTY ROASTER', '2018-05-31 09:20:09', '::1', '', '1'),
(35, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:20:09', '::1', '', '1'),
(36, 'INSERT', 'ADD TOD', '2018-05-31 09:20:28', '::1', '', '1'),
(37, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:20:33', '::1', '', '1'),
(38, 'SET CURRENT TODs', 'DUTY ROASTER', '2018-05-31 09:20:39', '::1', '', '1'),
(39, 'SELECT', 'DUTY ROASTER', '2018-05-31 09:20:40', '::1', '', '1'),
(40, 'VIEW', 'STAFFS', '2018-05-31 09:20:42', '::1', '', '1'),
(41, 'VIEW', 'ROLES', '2018-05-31 09:21:34', '::1', '', '1'),
(42, 'VIEW', 'ROLES', '2018-05-31 09:21:39', '::1', '', '1'),
(43, 'VIEW', 'ROLES', '2018-05-31 09:21:46', '::1', '', '1'),
(44, 'VIEW', 'DEPARTMENTS', '2018-05-31 09:21:53', '::1', '', '1'),
(45, 'VIEW', 'ROLES', '2018-05-31 09:21:55', '::1', '', '1'),
(46, 'VIEW LOG', 'LOG', '2018-05-31 09:22:10', '::1', '', 'STAFF0002'),
(47, 'VIEW', 'TEACHING_ASSIGNMENT', '2018-05-31 09:22:13', '::1', '', 'STAFF0002'),
(48, 'VIEW', 'DEPARTMENT_STAFFS', '2018-05-31 09:22:22', '::1', '', 'STAFF0002'),
(49, 'VIEW', 'NOTIFICATION', '2018-05-31 09:22:34', '::1', '', 'STAFF0002'),
(50, 'VIEW', 'NOTIFICATION', '2018-05-31 09:22:39', '::1', '', 'STAFF0002'),
(51, 'VIEW', 'NOTIFICATION', '2018-05-31 09:22:41', '::1', '', 'STAFF0002'),
(52, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:25:59', '::1', '', '1'),
(53, 'ADD', 'SELECTED STUDENT', '2018-05-31 09:27:49', '::1', '', '1'),
(54, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:27:50', '::1', '', '1'),
(55, 'SET REPORTED', 'SELECTED STUDENTS', '2018-05-31 09:27:56', '::1', '', '1'),
(56, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:28:11', '::1', '', '1'),
(57, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:28:12', '::1', '', '1'),
(58, 'SET REPORTED', 'SELECTED STUDENTS', '2018-05-31 09:28:20', '::1', '', '1'),
(59, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:28:21', '::1', '', '1'),
(60, 'UPDATE', 'SELECTED STUDENT', '2018-05-31 09:28:34', '::1', '', '1'),
(61, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:28:34', '::1', '', '1'),
(62, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:30:00', '::1', '', '1'),
(63, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:35:26', '::1', '', '1'),
(64, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:35:42', '::1', '', '1'),
(65, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:36:04', '::1', '', '1'),
(66, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:36:17', '::1', '', '1'),
(67, 'VIEW', 'DISABLED STUDENTS', '2018-05-31 09:36:37', '::1', '', '1'),
(68, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:36:52', '::1', '', '1'),
(69, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:39:41', '::1', '', '1'),
(70, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:40:12', '::1', '', '1'),
(71, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:40:58', '::1', '', '1'),
(72, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:41:01', '::1', '', '1'),
(73, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:43:45', '::1', '', '1'),
(74, 'VIEW', 'TRANSFERRED IN STUDENT', '2018-05-31 09:46:08', '::1', '', '1'),
(75, 'VIEW', 'TRANSFERRED OUT STUDENT', '2018-05-31 09:54:06', '::1', '', '1'),
(76, 'VIEW', 'SUSPENDED STUDENTS', '2018-05-31 09:54:12', '::1', '', '1'),
(77, 'VIEW', 'SUSPENDED STUDENTS', '2018-05-31 09:57:00', '::1', '', '1'),
(78, 'VIEW', 'EXPELLED STUDENTS', '2018-05-31 09:57:33', '::1', '', '1'),
(79, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-31 09:57:57', '::1', '', '1'),
(80, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-31 09:58:09', '::1', '', '1'),
(81, 'UPDATE', 'SELECTED STUDENT', '2018-05-31 09:59:02', '::1', '', '1'),
(82, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:59:02', '::1', '', '1'),
(83, 'UPDATE', 'SELECTED STUDENT', '2018-05-31 09:59:11', '::1', '', '1'),
(84, 'VIEW', 'SELECTED STUDENTS', '2018-05-31 09:59:11', '::1', '', '1'),
(85, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:00:44', '::1', '', '1'),
(86, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:00:51', '::1', '', '1'),
(87, 'VIEW', 'TRANSFERRED OUT STUDENT', '2018-05-30 22:01:49', '::1', '', '1'),
(88, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:05:12', '::1', '', '1'),
(89, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:05:40', '::1', '', '1'),
(90, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:07:01', '::1', '', '1'),
(91, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:07:09', '::1', '', '1'),
(92, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:07:17', '::1', '', '1'),
(93, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:10:36', '::1', '', '1'),
(94, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:10:41', '::1', '', '1'),
(95, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:11:54', '::1', '', '1'),
(96, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:11:56', '::1', '', '1'),
(97, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:22', '::1', '', '1'),
(98, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:25', '::1', '', '1'),
(99, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:27', '::1', '', '1'),
(100, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:28', '::1', '', '1'),
(101, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:30', '::1', '', '1'),
(102, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:32', '::1', '', '1'),
(103, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:13:34', '::1', '', '1'),
(104, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:13:40', '::1', '', '1'),
(105, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:20:14', '::1', '', '1'),
(106, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:21:21', '::1', '', '1'),
(107, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:21:32', '::1', '', '1'),
(108, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:21:47', '::1', '', '1'),
(109, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:23:03', '::1', '', '1'),
(110, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:23:24', '::1', '', '1'),
(111, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:23:31', '::1', '', '1'),
(112, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:23:50', '::1', '', '1'),
(113, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:23:57', '::1', '', '1'),
(114, 'VIEW', 'UNREPORTED STUDENTS', '2018-05-30 22:24:04', '::1', '', '1'),
(115, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:24:57', '::1', '', '1'),
(116, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:24:58', '::1', '', '1'),
(117, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:25:13', '::1', '', '1'),
(118, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:25:14', '::1', '', '1'),
(119, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:25:15', '::1', '', '1'),
(120, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:25:48', '::1', '', '1'),
(121, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:25:49', '::1', '', '1'),
(122, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:26:47', '::1', '', '1'),
(123, 'VIEW', 'SELECTED STUDENTS', '2018-05-30 22:26:48', '::1', '', '1'),
(124, 'VIEW', 'MATOKEO', '2018-05-30 22:57:37', '::1', '', '1'),
(125, 'VIEW', 'EXAM_TYPES', '2018-05-30 22:57:37', '::1', '', '1'),
(126, 'VIEW', 'STAFFS', '2018-05-30 22:57:46', '::1', '', '1'),
(127, 'VIEW', 'STAFFS', '2018-05-30 22:57:50', '::1', '', '1'),
(128, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 22:58:22', '::1', '', '1'),
(129, 'VIEW', 'STAFFS', '2018-05-30 22:58:24', '::1', '', '1'),
(130, 'VIEW', 'STAFFS', '2018-05-30 23:02:41', '::1', '', '1'),
(131, 'VIEW', 'STAFFS', '2018-05-30 23:07:15', '::1', '', '1'),
(132, 'VIEW', 'STAFFS', '2018-05-30 23:15:21', '::1', '', '1'),
(133, 'VIEW', 'STAFFS', '2018-05-30 23:15:25', '::1', '', '1'),
(134, 'VIEW', 'STAFFS', '2018-05-30 23:15:29', '::1', '', '1'),
(135, 'VIEW', 'STAFFS', '2018-05-30 23:21:14', '::1', '', '1'),
(136, 'VIEW', 'STAFFS', '2018-05-30 23:21:43', '::1', '', '1'),
(137, 'VIEW', 'STAFFS', '2018-05-30 23:21:50', '::1', '', '1'),
(138, 'VIEW', 'STAFFS', '2018-05-30 23:22:10', '::1', '', '1'),
(139, 'VIEW', 'STAFFS', '2018-05-30 23:27:26', '::1', '', '1'),
(140, 'VIEW', 'STAFFS', '2018-05-30 23:29:50', '::1', '', '1'),
(141, 'VIEW', 'STAFFS', '2018-05-30 23:30:23', '::1', '', '1'),
(142, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:30:26', '::1', '', '1'),
(143, 'VIEW', 'STAFFS', '2018-05-30 23:30:29', '::1', '', '1'),
(144, 'VIEW', 'STAFFS', '2018-05-30 23:31:21', '::1', '', '1'),
(145, 'VIEW', 'STAFFS', '2018-05-30 23:31:26', '::1', '', '1'),
(146, 'VIEW', 'STAFFS', '2018-05-30 23:31:46', '::1', '', '1'),
(147, 'VIEW', 'STAFFS', '2018-05-30 23:31:51', '::1', '', '1'),
(148, 'VIEW', 'STAFFS', '2018-05-30 23:32:34', '::1', '', '1'),
(149, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:33:29', '::1', '', '1'),
(150, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:33:36', '::1', '', '1'),
(151, 'VIEW', 'STAFFS', '2018-05-30 23:39:15', '::1', '', '1'),
(152, 'VIEW', 'STAFFS', '2018-05-30 23:39:31', '::1', '', '1'),
(153, 'VIEW', 'STAFFS', '2018-05-30 23:39:41', '::1', '', '1'),
(154, 'VIEW', 'STAFFS', '2018-05-30 23:40:23', '::1', '', '1'),
(155, 'UPDATE STAFF', 'STAFF', '2018-05-30 23:40:28', '::1', '', '1'),
(156, 'VIEW', 'STAFFS', '2018-05-30 23:40:48', '::1', '', '1'),
(157, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:40:50', '::1', '', '1'),
(158, 'VIEW', 'STAFFS', '2018-05-30 23:40:52', '::1', '', '1'),
(159, 'VIEW', 'STAFFS', '2018-05-30 23:42:00', '::1', '', '1'),
(160, 'VIEW', 'STAFFS', '2018-05-30 23:42:07', '::1', '', '1'),
(161, 'VIEW', 'STAFFS', '2018-05-30 23:42:41', '::1', '', '1'),
(162, 'VIEW', 'STAFFS', '2018-05-30 23:43:03', '::1', '', '1'),
(163, 'VIEW', 'STAFFS', '2018-05-30 23:43:16', '::1', '', '1'),
(164, 'VIEW', 'SUSPENDED STUDENTS', '2018-05-30 23:51:54', '::1', '', '1'),
(165, 'VIEW', 'STAFFS', '2018-05-30 23:52:02', '::1', '', '1'),
(166, 'UPDATE STAFF', 'STAFF', '2018-05-30 23:52:08', '::1', '', '1'),
(167, 'VIEW', 'STAFFS', '2018-05-30 23:52:16', '::1', '', '1'),
(168, 'VIEW', 'STAFFS', '2018-05-30 23:53:47', '::1', '', '1'),
(169, 'VIEW', 'STAFFS', '2018-05-30 23:53:59', '::1', '', '1'),
(170, 'UPDATE', 'staff', '2018-05-30 23:54:01', '::1', '', '1'),
(171, 'VIEW', 'STAFFS', '2018-05-30 23:54:02', '::1', '', '1'),
(172, 'UPDATE', 'staff', '2018-05-30 23:54:06', '::1', '', '1'),
(173, 'VIEW', 'STAFFS', '2018-05-30 23:54:06', '::1', '', '1'),
(174, 'UPDATE', 'staff', '2018-05-30 23:54:12', '::1', '', '1'),
(175, 'VIEW', 'STAFFS', '2018-05-30 23:54:12', '::1', '', '1'),
(176, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:54:15', '::1', '', '1'),
(177, 'VIEW OWN PROFILE', 'STAFF', '2018-05-30 23:55:04', '::1', '', '1'),
(178, 'VIEW', 'STAFF"S ATTENDANCE', '2018-05-30 23:55:14', '::1', '', '1'),
(179, 'ADD', 'STAFF ATTENDANCE', '2018-05-31 00:06:38', '::1', '', '1'),
(180, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:06:54', '::1', '', '1'),
(181, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:08:03', '::1', '', '1'),
(182, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:08:15', '::1', '', '1'),
(183, 'UPDATE', 'STAFF ATTENDANCE', '2018-05-31 00:08:39', '::1', '', '1'),
(184, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:08:39', '::1', '', '1'),
(185, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:09:00', '::1', '', '1'),
(186, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:09:03', '::1', '', '1'),
(187, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:09:05', '::1', '', '1'),
(188, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:09:10', '::1', '', '1'),
(189, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:09:21', '::1', '', '1'),
(190, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:09:25', '::1', '', '1'),
(191, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:12:01', '::1', '', '1'),
(192, 'ADD', 'STAFF ATTENDANCE', '2018-05-31 00:12:17', '::1', '', '1'),
(193, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:12:25', '::1', '', '1'),
(194, 'UPDATE', 'STAFF ATTENDANCE', '2018-05-31 00:12:32', '::1', '', '1'),
(195, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:12:32', '::1', '', '1'),
(196, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:12:34', '::1', '', '1'),
(197, 'ADD', 'STAFF ATTENDANCE', '2018-05-31 00:12:58', '::1', '', '1'),
(198, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:05', '::1', '', '1'),
(199, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:13:07', '::1', '', '1'),
(200, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:13:18', '::1', '', '1'),
(201, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:13:23', '::1', '', '1'),
(202, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:24', '::1', '', '1'),
(203, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:29', '::1', '', '1'),
(204, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:32', '::1', '', '1'),
(205, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:37', '::1', '', '1'),
(206, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:40', '::1', '', '1'),
(207, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:44', '::1', '', '1'),
(208, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:13:49', '::1', '', '1'),
(209, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:13:59', '::1', '', '1'),
(210, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:04', '::1', '', '1'),
(211, 'UPDATE', 'STAFF ATTENDANCE', '2018-05-31 00:14:07', '::1', '', '1'),
(212, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:08', '::1', '', '1'),
(213, 'VIEW', 'STAFFS MONTHLY ATTENDANCE', '2018-05-31 00:14:09', '::1', '', '1'),
(214, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:13', '::1', '', '1'),
(215, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:18', '::1', '', '1'),
(216, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:21', '::1', '', '1'),
(217, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:14:24', '::1', '', '1'),
(218, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:15:44', '::1', '', '1'),
(219, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:15:58', '::1', '', '1'),
(220, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:16:19', '::1', '', '1'),
(221, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:16:29', '::1', '', '1'),
(222, 'VIEW', 'STAFFS DAILY ATTENDANCE', '2018-05-31 00:16:38', '::1', '', '1'),
(223, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:17:02', '::1', '', '1'),
(224, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:20:01', '::1', '', '1'),
(225, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:21:04', '::1', '', '1'),
(226, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:21:22', '::1', '', '1'),
(227, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:22:53', '::1', '', '1'),
(228, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:23:10', '::1', '', '1'),
(229, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:23:18', '::1', '', '1'),
(230, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:23:22', '::1', '', '1'),
(231, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:23:27', '::1', '', '1'),
(232, 'VIEW', 'DEPARTMENT', '2018-05-31 00:24:26', '::1', '', '1'),
(233, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:24:28', '::1', '', '1'),
(234, 'INSERT', 'DEPARTMENT', '2018-05-31 00:24:42', '::1', '', '1'),
(235, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:24:42', '::1', '', '1'),
(236, 'VIEW', 'DEPARTMENT', '2018-05-31 00:24:45', '::1', '', '1'),
(237, 'VIEW', 'DEPARTMENT', '2018-05-31 00:24:48', '::1', '', '1'),
(238, 'UPDATE', 'DEPARTMENT', '2018-05-31 00:24:48', '::1', '', '1'),
(239, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:24:49', '::1', '', '1'),
(240, 'VIEW', 'DEPARTMENT', '2018-05-31 00:24:51', '::1', '', '1'),
(241, 'VIEW', 'DEPARTMENT', '2018-05-31 00:24:54', '::1', '', '1'),
(242, 'UPDATE', 'DEPARTMENT', '2018-05-31 00:24:54', '::1', '', '1'),
(243, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:24:55', '::1', '', '1'),
(244, 'VIEW', 'DEPARTMENT', '2018-05-31 00:25:03', '::1', '', '1'),
(245, 'VIEW', 'DEPARTMENT', '2018-05-31 00:25:07', '::1', '', '1'),
(246, 'UPDATE', 'DEPARTMENT', '2018-05-31 00:25:08', '::1', '', '1'),
(247, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:25:08', '::1', '', '1'),
(248, 'DELETE', 'DEPARTMENT', '2018-05-31 00:25:12', '::1', '', '1'),
(249, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:25:12', '::1', '', '1'),
(250, 'VIEW', 'TRANSFERRED OUT STUDENT', '2018-05-31 00:25:18', '::1', '', '1'),
(251, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:25:22', '::1', '', '1'),
(252, 'VIEW', 'DEPARTMENT', '2018-05-31 00:25:38', '::1', '', '1'),
(253, 'VIEW', 'DEPARTMENT', '2018-05-31 00:25:39', '::1', '', '1'),
(254, 'VIEW', 'DEPARTMENT', '2018-05-31 00:26:31', '::1', '', '1'),
(255, 'VIEW', 'DEPARTMENT', '2018-05-31 00:26:31', '::1', '', '1'),
(256, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:26:33', '::1', '', '1'),
(257, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:26:56', '::1', '', '1'),
(258, 'VIEW', 'DEPARTMENT_STAFFS', '2018-05-31 00:26:58', '::1', '', '1'),
(259, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:27:02', '::1', '', '1'),
(260, 'VIEW', 'DEPARTMENT_STAFFS', '2018-05-31 00:27:13', '::1', '', '1'),
(261, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:27:18', '::1', '', '1'),
(262, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:28:49', '::1', '', '1'),
(263, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:28:55', '::1', '', '1'),
(264, 'VIEW', 'TEACHING_ASSIGNMENT', '2018-05-31 00:30:57', '::1', '', '1'),
(265, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:30:59', '::1', '', '1'),
(266, 'VIEW', 'TEACHING_ASSIGNMENT', '2018-05-31 00:33:35', '::1', '', '1'),
(267, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:34:29', '::1', '', '1'),
(268, 'VIEW', 'TEACHING_ASSIGNMENT', '2018-05-31 00:34:31', '::1', '', '1'),
(269, 'VIEW', 'TEACHING_ASSIGNMENT', '2018-05-31 00:34:37', '::1', '', '1'),
(270, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:35:06', '::1', '', '1'),
(271, 'VIEW LOG', 'LOG', '2018-05-31 00:35:08', '::1', '', '1'),
(272, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:44:45', '::1', '', '1'),
(273, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:45:24', '::1', '', '1'),
(274, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:45:40', '::1', '', '1'),
(275, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:45:49', '::1', '', '1'),
(276, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:46:26', '::1', '', '1'),
(277, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:46:58', '::1', '', '1'),
(278, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:48:06', '::1', '', '1'),
(279, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:48:22', '::1', '', '1'),
(280, 'VIEW', 'HODs HISTORY', '2018-05-31 00:48:28', '::1', '', '1'),
(281, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:48:36', '::1', '', '1'),
(282, 'VIEW', 'STAFFS', '2018-05-31 00:49:30', '::1', '', '1'),
(283, 'VIEW', 'DEPARTMENTS', '2018-05-31 00:49:39', '::1', '', '1'),
(284, 'VIEW', 'STAFFS', '2018-05-31 00:49:43', '::1', '', '1'),
(285, 'VIEW', 'ROLES', '2018-05-31 00:55:07', '::1', '', '1'),
(286, 'VIEW', 'ROLES', '2018-05-31 00:55:11', '::1', '', '1'),
(287, 'VIEW', 'ROLES', '2018-05-31 00:55:15', '::1', '', '1'),
(288, 'VIEW', 'ROLES', '2018-05-31 00:55:19', '::1', '', '1'),
(289, 'VIEW', 'ROLES', '2018-05-31 00:56:01', '::1', '', '1'),
(290, 'UPDATE', 'ROLE_PERMISSION', '2018-05-31 00:56:07', '::1', '', '1'),
(291, 'REGISTER STAFF', 'STAFF', '2018-05-31 01:14:39', '::1', '', '1'),
(292, 'VIEW', 'STAFFS', '2018-05-31 01:14:51', '::1', '', '1'),
(293, 'UPDATE STAFF', 'STAFF', '2018-05-31 01:14:57', '::1', '', '1'),
(294, 'VIEW', 'STAFFS', '2018-05-31 01:15:04', '::1', '', '1'),
(295, 'UPDATE STAFF', 'STAFF', '2018-05-31 01:17:23', '::1', '', '1'),
(296, 'VIEW', 'STAFFS', '2018-05-31 01:17:59', '::1', '', '1'),
(297, 'VIEW', 'STAFFS', '2018-05-31 01:18:28', '::1', '', '1'),
(298, 'UPDATE STAFF', 'STAFF', '2018-05-31 01:18:32', '::1', '', '1'),
(299, 'VIEW', 'CLASS_STREAMS', '2018-05-31 01:46:42', '::1', '', '1'),
(300, 'VIEW', 'STAFFS', '2018-05-31 02:09:39', '::1', '', '1'),
(301, 'VIEW', 'ROLES', '2018-05-31 02:11:57', '::1', '', '1'),
(302, 'VIEW', 'ROLES', '2018-05-31 02:12:04', '::1', '', '1'),
(303, 'VIEW', 'ROLES', '2018-05-31 02:12:25', '::1', '', '1'),
(304, 'VIEW', 'CLASS_STREAMS', '2018-05-31 02:12:29', '::1', '', '1'),
(305, 'VIEW', 'EXAM_TYPES', '2018-05-31 02:12:50', '::1', '', '1'),
(306, 'ENABLE', 'EXAM_TYPE', '2018-05-31 02:12:54', '::1', '', '1'),
(307, 'VIEW', 'EXAM_TYPES', '2018-05-31 02:12:54', '::1', '', '1'),
(308, 'VIEW', 'TRANSFERRED IN STUDENT', '2018-05-31 02:12:58', '::1', '', '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `activity_log_view`
--
CREATE TABLE IF NOT EXISTS `activity_log_view` (
`id` bigint(20)
,`activity` varchar(50)
,`tableName` varchar(50)
,`time` timestamp
,`source` varchar(30)
,`destination` varchar(10)
,`user_id` varchar(10)
,`username` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(1) unsigned NOT NULL,
  `user_role` enum('Admin') DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `last_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_role`, `username`, `password`, `last_log`) VALUES
(1, 'Admin', 'Administrator', 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 01:27:45');

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_announcements_view`
--
CREATE TABLE IF NOT EXISTS `all_announcements_view` (
`group_id` int(11)
,`announcement_id` int(11)
,`heading` varchar(255)
,`msg` longtext
,`time` timestamp
,`posted_by` varchar(10)
,`status` enum('show','hide')
,`group_name` varchar(100)
,`description` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `all_class_streams_view`
--
CREATE TABLE IF NOT EXISTS `all_class_streams_view` (
`teacher_id` varchar(10)
,`teacher_names` varchar(61)
,`id` int(5)
,`year` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`level` enum('O''Level','A''Level')
,`enrolled` bigint(21)
,`capacity` int(3)
,`description` varchar(255)
,`id_display` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `all_results_view`
--
CREATE TABLE IF NOT EXISTS `all_results_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`class_name` varchar(30)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`term_id` int(11)
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`aid` int(5)
,`term_name` enum('first_term','second_term')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `all_teachers_view`
--
CREATE TABLE IF NOT EXISTS `all_teachers_view` (
`staff_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`user_role` varchar(30)
,`staff_type` char(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE IF NOT EXISTS `announcement` (
`announcement_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `msg` longtext NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `posted_by` varchar(10) NOT NULL,
  `status` enum('show','hide') NOT NULL DEFAULT 'hide'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announcement_id`, `group_id`, `heading`, `msg`, `time`, `posted_by`, `status`) VALUES
(3, 1, 'Yah: Kikao', 'Manaombwa kuwahi kwenye kikao kitakachoanza saa 3 asubuhi', '2018-05-22 09:05:47', 'STAFF0005', 'show'),
(4, 1, 'Kikao', 'kikao', '2018-05-22 09:05:28', 'STAFF0005', 'show'),
(5, 1, 'kikao', 'kikao', '2018-05-22 09:05:21', 'STAFF0005', 'show'),
(6, 1, 'helow', 'kikao', '2018-05-22 09:05:39', 'STAFF0005', 'show'),
(7, 1, 'Oyooooo', 'Kama kawaa			wazeiya', '2018-05-31 06:54:30', '1', 'show');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_status`
--

CREATE TABLE IF NOT EXISTS `announcement_status` (
  `staff_id` varchar(10) NOT NULL,
  `group_id` int(11) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `is_read` enum('read','unread') NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement_status`
--

INSERT INTO `announcement_status` (`staff_id`, `group_id`, `announcement_id`, `is_read`) VALUES
('STAFF0001', 1, 3, 'unread'),
('STAFF0001', 1, 4, 'unread'),
('STAFF0001', 1, 5, 'unread'),
('STAFF0001', 1, 6, 'unread'),
('STAFF0001', 1, 7, 'unread'),
('STAFF0002', 1, 3, 'unread'),
('STAFF0002', 1, 4, 'unread'),
('STAFF0002', 1, 5, 'unread'),
('STAFF0002', 1, 6, 'unread'),
('STAFF0002', 1, 7, 'unread'),
('STAFF0003', 1, 3, 'unread'),
('STAFF0003', 1, 4, 'unread'),
('STAFF0003', 1, 5, 'unread'),
('STAFF0003', 1, 6, 'unread'),
('STAFF0003', 1, 7, 'unread'),
('STAFF0005', 1, 3, 'read'),
('STAFF0005', 1, 4, 'read'),
('STAFF0005', 1, 5, 'read'),
('STAFF0005', 1, 6, 'read'),
('STAFF0005', 1, 7, 'read');

-- --------------------------------------------------------

--
-- Stand-in structure for view `assigned_user_roles`
--
CREATE TABLE IF NOT EXISTS `assigned_user_roles` (
`role_name` varchar(30)
,`description` mediumtext
,`role_type_id` int(2)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`active` enum('on','off')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `assign_hod_class_teacher_view`
--
CREATE TABLE IF NOT EXISTS `assign_hod_class_teacher_view` (
`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
`att_id` int(2) NOT NULL,
  `att_type` char(1) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`att_id`, `att_type`, `description`) VALUES
(1, 'A', 'Absent'),
(2, 'P', 'Present'),
(3, 'S', 'Sick'),
(4, 'T', 'Permitted'),
(5, 'H', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `audit_password_reset_tbl`
--

CREATE TABLE IF NOT EXISTS `audit_password_reset_tbl` (
  `staff_id` varchar(10) NOT NULL,
  `reset_by` int(1) unsigned NOT NULL,
  `reset_to` varchar(50) NOT NULL,
  `reset_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_changed` enum('T','F') NOT NULL DEFAULT 'F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_password_reset_tbl`
--

INSERT INTO `audit_password_reset_tbl` (`staff_id`, `reset_by`, `reset_to`, `reset_time`, `user_changed`) VALUES
('STAFF0001', 1, '62196791f73898b3c1c3fb7f75071f0f8189820c', '2018-05-31 09:52:59', 'T'),
('STAFF0002', 1, 'ef547badb8b0801d06a93155cc052341c749d1c0', '2018-05-31 10:21:22', 'T'),
('STAFF0003', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 08:59:35', 'F'),
('STAFF0004', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 09:27:22', 'F'),
('STAFF0005', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 09:23:18', 'F'),
('STAFF0008', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 08:02:02', 'F'),
('STAFF1111', 1, 'a96af5cb3ae3b0e034f1c597133307de25068490', '2018-05-31 09:25:52', 'F'),
('STAFF2222', 1, 'b3faf601cfde55566462119aa92869f4b8e4d829', '2018-05-31 14:14:57', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `audit_register_student_tbl`
--

CREATE TABLE IF NOT EXISTS `audit_register_student_tbl` (
`id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `change_type` enum('NEW','EDIT','DELETE') NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `audit_staff_attendance`
--

CREATE TABLE IF NOT EXISTS `audit_staff_attendance` (
`id` int(11) NOT NULL,
  `sar_id` int(11) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `att_id` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_staff_attendance_view`
--
CREATE TABLE IF NOT EXISTS `audit_staff_attendance_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`date_` date
,`day` varchar(9)
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` varchar(6)
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
,`added_changed_by` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `audit_student_attendance`
--

CREATE TABLE IF NOT EXISTS `audit_student_attendance` (
`id` int(11) NOT NULL,
  `sar_id` int(11) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `att_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_student_attendance_view`
--
CREATE TABLE IF NOT EXISTS `audit_student_attendance_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`admission_no` varchar(10)
,`date_` date
,`day` varchar(9)
,`csid` varchar(10)
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` varchar(6)
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
,`class_name` varchar(30)
,`added_changed_by` varchar(61)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `audit_student_results_view`
--
CREATE TABLE IF NOT EXISTS `audit_student_results_view` (
`admission_no` varchar(10)
,`student_names` varchar(41)
,`subject_name` varchar(50)
,`exam_name` varchar(30)
,`marks` float(5,2)
,`class_name` varchar(30)
,`e_date` date
,`term_name` varchar(11)
,`year` varchar(10)
,`change_type` varchar(6)
,`ip_address` varchar(255)
,`change_time` timestamp
,`added_changed_by` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `audit_student_subject_assessment`
--

CREATE TABLE IF NOT EXISTS `audit_student_subject_assessment` (
`id` bigint(20) NOT NULL,
  `ssa_id` bigint(20) NOT NULL,
  `change_type` enum('ADD','UPDATE','REMOVE') NOT NULL,
  `change_by` varchar(10) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `marks` float(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
`author_id` int(3) unsigned NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `available_books_view`
--
CREATE TABLE IF NOT EXISTS `available_books_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`BOOK_TITLE` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `a_level_grade_system_tbl`
--

CREATE TABLE IF NOT EXISTS `a_level_grade_system_tbl` (
`id` int(3) NOT NULL,
  `start_mark` float(5,2) NOT NULL,
  `end_mark` float(5,2) NOT NULL,
  `grade` enum('A','B+','B','C','D','E','S','F') NOT NULL,
  `points` int(1) NOT NULL,
  `remarks` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `a_level_grade_system_tbl`
--

INSERT INTO `a_level_grade_system_tbl` (`id`, `start_mark`, `end_mark`, `grade`, `points`, `remarks`) VALUES
(1, 0.10, 30.00, 'F', 7, 'Fail'),
(2, 30.10, 40.00, 'S', 6, 'Satisfactory'),
(3, 40.10, 50.00, 'E', 5, 'Pass'),
(4, 50.10, 60.00, 'D', 4, 'Fair'),
(5, 60.01, 70.00, 'C', 3, 'Good'),
(6, 80.01, 100.00, 'A', 1, 'Excellent'),
(7, 70.01, 80.00, 'B', 2, 'Very Good');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `isbn` varchar(17) NOT NULL,
  `ID` varchar(50) NOT NULL DEFAULT '',
  `LAST_EDIT` date DEFAULT NULL,
  `STATUS` enum('normal','lost','bad') DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`isbn`, `ID`, `LAST_EDIT`, `STATUS`) VALUES
('04-002-5', '1', '2018-05-23', 'normal'),
('04-002-5', '10', '2018-05-23', 'normal'),
('04-002-5', '11', '2018-05-23', 'normal'),
('04-002-5', '12', '2018-05-23', 'normal'),
('04-002-5', '13', '2018-05-23', 'normal'),
('04-002-5', '14', '2018-05-23', 'normal'),
('04-002-5', '15', '2018-05-23', 'normal'),
('04-002-5', '16', '2018-05-23', 'normal'),
('04-002-5', '17', '2018-05-23', 'normal'),
('04-002-5', '18', '2018-05-23', 'normal'),
('04-002-5', '19', '2018-05-23', 'normal'),
('04-002-5', '2', '2018-05-23', 'normal'),
('04-002-5', '20', '2018-05-23', 'normal'),
('04-002-5', '21', '2018-05-23', 'normal'),
('04-002-5', '22', '2018-05-23', 'normal'),
('04-002-5', '23', '2018-05-23', 'normal'),
('04-002-5', '24', '2018-05-23', 'normal'),
('04-002-5', '25', '2018-05-23', 'normal'),
('04-002-5', '26', '2018-05-23', 'normal'),
('04-002-5', '27', '2018-05-23', 'normal'),
('04-002-5', '28', '2018-05-23', 'normal'),
('04-002-5', '29', '2018-05-23', 'normal'),
('04-002-5', '3', '2018-05-23', 'normal'),
('04-002-5', '30', '2018-05-23', 'normal'),
('04-002-5', '31', '2018-05-23', 'normal'),
('04-002-5', '32', '2018-05-23', 'normal'),
('04-002-5', '33', '2018-05-23', 'normal'),
('04-002-5', '34', '2018-05-23', 'normal'),
('04-002-5', '35', '2018-05-23', 'normal'),
('04-002-5', '36', '2018-05-23', 'normal'),
('04-002-5', '37', '2018-05-23', 'normal'),
('04-002-5', '38', '2018-05-23', 'normal'),
('04-002-5', '39', '2018-05-23', 'normal'),
('04-002-5', '4', '2018-05-23', 'normal'),
('04-002-5', '40', '2018-05-23', 'normal'),
('04-002-5', '41', '2018-05-23', 'normal'),
('04-002-5', '42', '2018-05-23', 'normal'),
('04-002-5', '43', '2018-05-23', 'normal'),
('04-002-5', '44', '2018-05-23', 'normal'),
('04-002-5', '45', '2018-05-23', 'normal'),
('04-002-5', '46', '2018-05-23', 'normal'),
('04-002-5', '47', '2018-05-23', 'normal'),
('04-002-5', '48', '2018-05-23', 'normal'),
('04-002-5', '49', '2018-05-23', 'normal'),
('04-002-5', '5', '2018-05-23', 'normal'),
('04-002-5', '50', '2018-05-23', 'normal'),
('04-002-5', '51', '2018-05-23', 'normal'),
('04-002-5', '52', '2018-05-23', 'normal'),
('04-002-5', '53', '2018-05-23', 'normal'),
('04-002-5', '54', '2018-05-23', 'normal'),
('04-002-5', '55', '2018-05-23', 'normal'),
('04-002-5', '56', '2018-05-23', 'normal'),
('04-002-5', '57', '2018-05-23', 'normal'),
('04-002-5', '58', '2018-05-23', 'normal'),
('04-002-5', '59', '2018-05-23', 'normal'),
('04-002-5', '6', '2018-05-23', 'normal'),
('04-002-5', '60', '2018-05-23', 'normal'),
('04-002-5', '61', '2018-05-23', 'normal'),
('04-002-5', '62', '2018-05-23', 'normal'),
('04-002-5', '63', '2018-05-23', 'normal'),
('04-002-5', '64', '2018-05-23', 'normal'),
('04-002-5', '65', '2018-05-23', 'normal'),
('04-002-5', '66', '2018-05-23', 'normal'),
('04-002-5', '67', '2018-05-23', 'normal'),
('04-002-5', '68', '2018-05-23', 'normal'),
('04-002-5', '69', '2018-05-23', 'normal'),
('04-002-5', '7', '2018-05-23', 'normal'),
('04-002-5', '70', '2018-05-23', 'normal'),
('04-002-5', '71', '2018-05-23', 'normal'),
('04-002-5', '72', '2018-05-23', 'normal'),
('04-002-5', '73', '2018-05-23', 'normal'),
('04-002-5', '74', '2018-05-23', 'normal'),
('04-002-5', '75', '2018-05-23', 'normal'),
('04-002-5', '76', '2018-05-23', 'normal'),
('04-002-5', '77', '2018-05-23', 'normal'),
('04-002-5', '78', '2018-05-23', 'normal'),
('04-002-5', '79', '2018-05-23', 'normal'),
('04-002-5', '8', '2018-05-23', 'normal'),
('04-002-5', '80', '2018-05-23', 'normal'),
('04-002-5', '81', '2018-05-23', 'normal'),
('04-002-5', '82', '2018-05-23', 'normal'),
('04-002-5', '83', '2018-05-23', 'normal'),
('04-002-5', '84', '2018-05-23', 'normal'),
('04-002-5', '9', '2018-05-23', 'normal'),
('61-057-2', '1', '2018-05-23', 'normal'),
('61-057-2', '10', '2018-05-23', 'normal'),
('61-057-2', '11', '2018-05-23', 'normal'),
('61-057-2', '12', '2018-05-23', 'normal'),
('61-057-2', '13', '2018-05-23', 'normal'),
('61-057-2', '14', '2018-05-23', 'normal'),
('61-057-2', '15', '2018-05-23', 'normal'),
('61-057-2', '16', '2018-05-23', 'normal'),
('61-057-2', '17', '2018-05-23', 'normal'),
('61-057-2', '18', '2018-05-23', 'normal'),
('61-057-2', '19', '2018-05-23', 'normal'),
('61-057-2', '2', '2018-05-23', 'normal'),
('61-057-2', '20', '2018-05-23', 'normal'),
('61-057-2', '21', '2018-05-23', 'normal'),
('61-057-2', '22', '2018-05-23', 'normal'),
('61-057-2', '23', '2018-05-23', 'normal'),
('61-057-2', '24', '2018-05-23', 'normal'),
('61-057-2', '25', '2018-05-23', 'normal'),
('61-057-2', '26', '2018-05-23', 'normal'),
('61-057-2', '27', '2018-05-23', 'normal'),
('61-057-2', '28', '2018-05-23', 'normal'),
('61-057-2', '29', '2018-05-23', 'normal'),
('61-057-2', '3', '2018-05-23', 'normal'),
('61-057-2', '30', '2018-05-23', 'normal'),
('61-057-2', '31', '2018-05-23', 'normal'),
('61-057-2', '32', '2018-05-23', 'normal'),
('61-057-2', '33', '2018-05-23', 'normal'),
('61-057-2', '34', '2018-05-23', 'normal'),
('61-057-2', '35', '2018-05-23', 'normal'),
('61-057-2', '36', '2018-05-23', 'normal'),
('61-057-2', '37', '2018-05-23', 'normal'),
('61-057-2', '38', '2018-05-23', 'normal'),
('61-057-2', '39', '2018-05-23', 'normal'),
('61-057-2', '4', '2018-05-23', 'normal'),
('61-057-2', '40', '2018-05-23', 'normal'),
('61-057-2', '41', '2018-05-23', 'normal'),
('61-057-2', '42', '2018-05-23', 'normal'),
('61-057-2', '43', '2018-05-23', 'normal'),
('61-057-2', '44', '2018-05-23', 'normal'),
('61-057-2', '45', '2018-05-23', 'normal'),
('61-057-2', '46', '2018-05-23', 'normal'),
('61-057-2', '47', '2018-05-23', 'normal'),
('61-057-2', '48', '2018-05-23', 'normal'),
('61-057-2', '49', '2018-05-23', 'normal'),
('61-057-2', '5', '2018-05-23', 'normal'),
('61-057-2', '50', '2018-05-23', 'normal'),
('61-057-2', '51', '2018-05-23', 'normal'),
('61-057-2', '52', '2018-05-23', 'normal'),
('61-057-2', '6', '2018-05-23', 'normal'),
('61-057-2', '7', '2018-05-23', 'normal'),
('61-057-2', '8', '2018-05-23', 'normal'),
('61-057-2', '9', '2018-05-23', 'normal'),
('61-457-2', '1', '2018-05-23', 'normal'),
('61-457-2', '10', '2018-05-23', 'normal'),
('61-457-2', '100', '2018-05-23', 'normal'),
('61-457-2', '101', '2018-05-23', 'normal'),
('61-457-2', '102', '2018-05-23', 'normal'),
('61-457-2', '103', '2018-05-23', 'normal'),
('61-457-2', '104', '2018-05-23', 'normal'),
('61-457-2', '105', '2018-05-23', 'normal'),
('61-457-2', '106', '2018-05-23', 'normal'),
('61-457-2', '107', '2018-05-23', 'normal'),
('61-457-2', '108', '2018-05-23', 'normal'),
('61-457-2', '109', '2018-05-23', 'normal'),
('61-457-2', '11', '2018-05-23', 'normal'),
('61-457-2', '110', '2018-05-23', 'normal'),
('61-457-2', '111', '2018-05-23', 'normal'),
('61-457-2', '112', '2018-05-23', 'normal'),
('61-457-2', '113', '2018-05-23', 'normal'),
('61-457-2', '114', '2018-05-23', 'normal'),
('61-457-2', '115', '2018-05-23', 'normal'),
('61-457-2', '116', '2018-05-23', 'normal'),
('61-457-2', '117', '2018-05-23', 'normal'),
('61-457-2', '118', '2018-05-23', 'normal'),
('61-457-2', '119', '2018-05-23', 'normal'),
('61-457-2', '12', '2018-05-23', 'normal'),
('61-457-2', '120', '2018-05-23', 'normal'),
('61-457-2', '13', '2018-05-23', 'normal'),
('61-457-2', '14', '2018-05-23', 'normal'),
('61-457-2', '15', '2018-05-23', 'normal'),
('61-457-2', '16', '2018-05-23', 'normal'),
('61-457-2', '17', '2018-05-23', 'normal'),
('61-457-2', '18', '2018-05-23', 'normal'),
('61-457-2', '19', '2018-05-23', 'normal'),
('61-457-2', '2', '2018-05-23', 'normal'),
('61-457-2', '20', '2018-05-23', 'normal'),
('61-457-2', '21', '2018-05-23', 'normal'),
('61-457-2', '22', '2018-05-23', 'normal'),
('61-457-2', '23', '2018-05-23', 'normal'),
('61-457-2', '24', '2018-05-23', 'normal'),
('61-457-2', '25', '2018-05-23', 'normal'),
('61-457-2', '26', '2018-05-23', 'normal'),
('61-457-2', '27', '2018-05-23', 'normal'),
('61-457-2', '28', '2018-05-23', 'normal'),
('61-457-2', '29', '2018-05-23', 'normal'),
('61-457-2', '3', '2018-05-23', 'normal'),
('61-457-2', '30', '2018-05-23', 'normal'),
('61-457-2', '31', '2018-05-23', 'normal'),
('61-457-2', '32', '2018-05-23', 'normal'),
('61-457-2', '33', '2018-05-23', 'normal'),
('61-457-2', '34', '2018-05-23', 'normal'),
('61-457-2', '35', '2018-05-23', 'normal'),
('61-457-2', '36', '2018-05-23', 'normal'),
('61-457-2', '37', '2018-05-23', 'normal'),
('61-457-2', '38', '2018-05-23', 'normal'),
('61-457-2', '39', '2018-05-23', 'normal'),
('61-457-2', '4', '2018-05-23', 'normal'),
('61-457-2', '40', '2018-05-23', 'normal'),
('61-457-2', '41', '2018-05-23', 'normal'),
('61-457-2', '42', '2018-05-23', 'normal'),
('61-457-2', '43', '2018-05-23', 'normal'),
('61-457-2', '44', '2018-05-23', 'normal'),
('61-457-2', '45', '2018-05-23', 'normal'),
('61-457-2', '46', '2018-05-23', 'normal'),
('61-457-2', '47', '2018-05-23', 'normal'),
('61-457-2', '48', '2018-05-23', 'normal'),
('61-457-2', '49', '2018-05-23', 'normal'),
('61-457-2', '5', '2018-05-23', 'normal'),
('61-457-2', '50', '2018-05-23', 'normal'),
('61-457-2', '51', '2018-05-23', 'normal'),
('61-457-2', '52', '2018-05-23', 'normal'),
('61-457-2', '53', '2018-05-23', 'normal'),
('61-457-2', '54', '2018-05-23', 'normal'),
('61-457-2', '55', '2018-05-23', 'normal'),
('61-457-2', '56', '2018-05-23', 'normal'),
('61-457-2', '57', '2018-05-23', 'normal'),
('61-457-2', '58', '2018-05-23', 'normal'),
('61-457-2', '59', '2018-05-23', 'normal'),
('61-457-2', '6', '2018-05-23', 'normal'),
('61-457-2', '60', '2018-05-23', 'normal'),
('61-457-2', '61', '2018-05-23', 'normal'),
('61-457-2', '62', '2018-05-23', 'normal'),
('61-457-2', '63', '2018-05-23', 'normal'),
('61-457-2', '64', '2018-05-23', 'normal'),
('61-457-2', '65', '2018-05-23', 'normal'),
('61-457-2', '66', '2018-05-23', 'normal'),
('61-457-2', '67', '2018-05-23', 'normal'),
('61-457-2', '68', '2018-05-23', 'normal'),
('61-457-2', '69', '2018-05-23', 'normal'),
('61-457-2', '7', '2018-05-23', 'normal'),
('61-457-2', '70', '2018-05-23', 'normal'),
('61-457-2', '71', '2018-05-23', 'normal'),
('61-457-2', '72', '2018-05-23', 'normal'),
('61-457-2', '73', '2018-05-23', 'normal'),
('61-457-2', '74', '2018-05-23', 'normal'),
('61-457-2', '75', '2018-05-23', 'normal'),
('61-457-2', '76', '2018-05-23', 'normal'),
('61-457-2', '77', '2018-05-23', 'normal'),
('61-457-2', '78', '2018-05-23', 'normal'),
('61-457-2', '79', '2018-05-23', 'normal'),
('61-457-2', '8', '2018-05-23', 'normal'),
('61-457-2', '80', '2018-05-23', 'normal'),
('61-457-2', '81', '2018-05-23', 'normal'),
('61-457-2', '82', '2018-05-23', 'normal'),
('61-457-2', '83', '2018-05-23', 'normal'),
('61-457-2', '84', '2018-05-23', 'normal'),
('61-457-2', '85', '2018-05-23', 'normal'),
('61-457-2', '86', '2018-05-23', 'normal'),
('61-457-2', '87', '2018-05-23', 'normal'),
('61-457-2', '88', '2018-05-23', 'normal'),
('61-457-2', '89', '2018-05-23', 'normal'),
('61-457-2', '9', '2018-05-23', 'normal'),
('61-457-2', '90', '2018-05-23', 'normal'),
('61-457-2', '91', '2018-05-23', 'normal'),
('61-457-2', '92', '2018-05-23', 'normal'),
('61-457-2', '93', '2018-05-23', 'normal'),
('61-457-2', '94', '2018-05-23', 'normal'),
('61-457-2', '95', '2018-05-23', 'normal'),
('61-457-2', '96', '2018-05-23', 'normal'),
('61-457-2', '97', '2018-05-23', 'normal'),
('61-457-2', '98', '2018-05-23', 'normal'),
('61-457-2', '99', '2018-05-23', 'normal'),
('61-458-9', '1', '2018-05-23', 'normal'),
('61-458-9', '10', '2018-05-23', 'normal'),
('61-458-9', '11', '2018-05-23', 'normal'),
('61-458-9', '12', '2018-05-23', 'normal'),
('61-458-9', '13', '2018-05-23', 'normal'),
('61-458-9', '14', '2018-05-23', 'normal'),
('61-458-9', '15', '2018-05-23', 'normal'),
('61-458-9', '16', '2018-05-23', 'normal'),
('61-458-9', '17', '2018-05-23', 'normal'),
('61-458-9', '18', '2018-05-23', 'normal'),
('61-458-9', '19', '2018-05-23', 'normal'),
('61-458-9', '2', '2018-05-23', 'normal'),
('61-458-9', '20', '2018-05-23', 'normal'),
('61-458-9', '21', '2018-05-23', 'normal'),
('61-458-9', '22', '2018-05-23', 'normal'),
('61-458-9', '23', '2018-05-23', 'normal'),
('61-458-9', '24', '2018-05-23', 'normal'),
('61-458-9', '25', '2018-05-23', 'normal'),
('61-458-9', '26', '2018-05-23', 'normal'),
('61-458-9', '27', '2018-05-23', 'normal'),
('61-458-9', '28', '2018-05-23', 'normal'),
('61-458-9', '29', '2018-05-23', 'normal'),
('61-458-9', '3', '2018-05-23', 'normal'),
('61-458-9', '30', '2018-05-23', 'normal'),
('61-458-9', '31', '2018-05-23', 'normal'),
('61-458-9', '32', '2018-05-23', 'normal'),
('61-458-9', '33', '2018-05-23', 'normal'),
('61-458-9', '34', '2018-05-23', 'normal'),
('61-458-9', '35', '2018-05-23', 'normal'),
('61-458-9', '36', '2018-05-23', 'normal'),
('61-458-9', '37', '2018-05-23', 'normal'),
('61-458-9', '38', '2018-05-23', 'normal'),
('61-458-9', '39', '2018-05-23', 'normal'),
('61-458-9', '4', '2018-05-23', 'normal'),
('61-458-9', '40', '2018-05-23', 'normal'),
('61-458-9', '41', '2018-05-23', 'normal'),
('61-458-9', '42', '2018-05-23', 'normal'),
('61-458-9', '43', '2018-05-23', 'normal'),
('61-458-9', '44', '2018-05-23', 'normal'),
('61-458-9', '45', '2018-05-23', 'normal'),
('61-458-9', '46', '2018-05-23', 'normal'),
('61-458-9', '47', '2018-05-23', 'normal'),
('61-458-9', '48', '2018-05-23', 'normal'),
('61-458-9', '49', '2018-05-23', 'normal'),
('61-458-9', '5', '2018-05-23', 'normal'),
('61-458-9', '50', '2018-05-23', 'normal'),
('61-458-9', '51', '2018-05-23', 'normal'),
('61-458-9', '52', '2018-05-23', 'normal'),
('61-458-9', '53', '2018-05-23', 'normal'),
('61-458-9', '54', '2018-05-23', 'normal'),
('61-458-9', '55', '2018-05-23', 'normal'),
('61-458-9', '56', '2018-05-23', 'normal'),
('61-458-9', '57', '2018-05-23', 'normal'),
('61-458-9', '58', '2018-05-23', 'normal'),
('61-458-9', '59', '2018-05-23', 'normal'),
('61-458-9', '6', '2018-05-23', 'normal'),
('61-458-9', '60', '2018-05-23', 'normal'),
('61-458-9', '61', '2018-05-23', 'normal'),
('61-458-9', '7', '2018-05-23', 'normal'),
('61-458-9', '8', '2018-05-23', 'normal'),
('61-458-9', '9', '2018-05-23', 'normal'),
('61-462-6', '1', '2018-05-23', 'normal'),
('61-462-6', '10', '2018-05-23', 'bad'),
('61-462-6', '11', '2018-05-23', 'bad'),
('61-462-6', '12', '2018-05-23', 'bad'),
('61-462-6', '13', '2018-05-23', 'normal'),
('61-462-6', '14', '2018-05-23', 'normal'),
('61-462-6', '15', '2018-05-23', 'normal'),
('61-462-6', '16', '2018-05-23', 'normal'),
('61-462-6', '17', '2018-05-23', 'normal'),
('61-462-6', '18', '2018-05-23', 'normal'),
('61-462-6', '19', '2018-05-23', 'normal'),
('61-462-6', '2', '2018-05-23', 'normal'),
('61-462-6', '20', '2018-05-23', 'normal'),
('61-462-6', '21', '2018-05-23', 'normal'),
('61-462-6', '22', '2018-05-23', 'normal'),
('61-462-6', '23', '2018-05-23', 'normal'),
('61-462-6', '24', '2018-05-23', 'normal'),
('61-462-6', '25', '2018-05-23', 'normal'),
('61-462-6', '26', '2018-05-23', 'normal'),
('61-462-6', '27', '2018-05-23', 'normal'),
('61-462-6', '28', '2018-05-23', 'normal'),
('61-462-6', '29', '2018-05-23', 'normal'),
('61-462-6', '3', '2018-05-23', 'normal'),
('61-462-6', '30', '2018-05-23', 'normal'),
('61-462-6', '31', '2018-05-23', 'normal'),
('61-462-6', '32', '2018-05-23', 'normal'),
('61-462-6', '33', '2018-05-23', 'normal'),
('61-462-6', '34', '2018-05-23', 'normal'),
('61-462-6', '35', '2018-05-23', 'normal'),
('61-462-6', '36', '2018-05-23', 'normal'),
('61-462-6', '37', '2018-05-23', 'normal'),
('61-462-6', '38', '2018-05-23', 'normal'),
('61-462-6', '39', '2018-05-23', 'normal'),
('61-462-6', '4', '2018-05-23', 'normal'),
('61-462-6', '40', '2018-05-23', 'normal'),
('61-462-6', '41', '2018-05-23', 'normal'),
('61-462-6', '42', '2018-05-23', 'normal'),
('61-462-6', '43', '2018-05-23', 'normal'),
('61-462-6', '44', '2018-05-23', 'normal'),
('61-462-6', '45', '2018-05-23', 'normal'),
('61-462-6', '46', '2018-05-23', 'normal'),
('61-462-6', '47', '2018-05-23', 'normal'),
('61-462-6', '48', '2018-05-23', 'normal'),
('61-462-6', '49', '2018-05-23', 'normal'),
('61-462-6', '5', '2018-05-23', 'normal'),
('61-462-6', '50', '2018-05-23', 'normal'),
('61-462-6', '6', '2018-05-23', 'normal'),
('61-462-6', '7', '2018-05-23', 'normal'),
('61-462-6', '8', '2018-05-23', 'normal'),
('61-462-6', '9', '2018-05-23', 'normal'),
('61-463-3', '1', '2018-05-23', 'normal'),
('61-463-3', '10', '2018-05-23', 'normal'),
('61-463-3', '11', '2018-05-23', 'normal'),
('61-463-3', '12', '2018-05-23', 'normal'),
('61-463-3', '13', '2018-05-23', 'normal'),
('61-463-3', '14', '2018-05-23', 'normal'),
('61-463-3', '15', '2018-05-23', 'normal'),
('61-463-3', '16', '2018-05-23', 'normal'),
('61-463-3', '17', '2018-05-23', 'normal'),
('61-463-3', '18', '2018-05-23', 'normal'),
('61-463-3', '19', '2018-05-23', 'normal'),
('61-463-3', '2', '2018-05-23', 'normal'),
('61-463-3', '20', '2018-05-23', 'normal'),
('61-463-3', '21', '2018-05-23', 'normal'),
('61-463-3', '22', '2018-05-23', 'normal'),
('61-463-3', '23', '2018-05-23', 'normal'),
('61-463-3', '24', '2018-05-23', 'normal'),
('61-463-3', '25', '2018-05-23', 'normal'),
('61-463-3', '26', '2018-05-23', 'normal'),
('61-463-3', '27', '2018-05-23', 'normal'),
('61-463-3', '28', '2018-05-23', 'normal'),
('61-463-3', '29', '2018-05-23', 'normal'),
('61-463-3', '3', '2018-05-23', 'normal'),
('61-463-3', '30', '2018-05-23', 'normal'),
('61-463-3', '31', '2018-05-23', 'normal'),
('61-463-3', '32', '2018-05-23', 'normal'),
('61-463-3', '33', '2018-05-23', 'normal'),
('61-463-3', '34', '2018-05-23', 'normal'),
('61-463-3', '35', '2018-05-23', 'normal'),
('61-463-3', '36', '2018-05-23', 'normal'),
('61-463-3', '37', '2018-05-23', 'normal'),
('61-463-3', '38', '2018-05-23', 'normal'),
('61-463-3', '39', '2018-05-23', 'normal'),
('61-463-3', '4', '2018-05-23', 'normal'),
('61-463-3', '40', '2018-05-23', 'normal'),
('61-463-3', '5', '2018-05-23', 'normal'),
('61-463-3', '6', '2018-05-23', 'normal'),
('61-463-3', '7', '2018-05-23', 'normal'),
('61-463-3', '8', '2018-05-23', 'normal'),
('61-463-3', '9', '2018-05-23', 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `books_borrowing_limit`
--

CREATE TABLE IF NOT EXISTS `books_borrowing_limit` (
  `borrowing_limit` int(3) NOT NULL,
  `b_type` varchar(10) NOT NULL,
`blid` smallint(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books_borrowing_limit`
--

INSERT INTO `books_borrowing_limit` (`borrowing_limit`, `b_type`, `blid`) VALUES
(2, 'STUDENT', 1),
(3, 'STAFF', 2),
(20, 'DEPARTMENT', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `book_copy_report`
--
CREATE TABLE IF NOT EXISTS `book_copy_report` (
`ISBN` varchar(17)
,`CODE` varchar(50)
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
);
-- --------------------------------------------------------

--
-- Table structure for table `book_loan_info`
--

CREATE TABLE IF NOT EXISTS `book_loan_info` (
`TRANSACTION_ID` int(11) NOT NULL,
  `ID` varchar(50) DEFAULT NULL,
  `ISBN` varchar(17) DEFAULT NULL,
  `LIBRARIAN_ID` varchar(10) DEFAULT NULL,
  `DATE_BORROWED` date NOT NULL,
  `RETURN_DATE` date NOT NULL,
  `DATE_DUE_BACK` date DEFAULT NULL,
  `DATE_OVERDUE` date DEFAULT NULL,
  `status_before` enum('good','bad') DEFAULT 'good',
  `status_after` enum('good','bad') DEFAULT 'good',
  `BORROWER_ID` varchar(10) NOT NULL,
  `BORROWER_TYPE` varchar(100) DEFAULT NULL,
  `LAST_EDIT` date DEFAULT NULL,
  `FLAG` enum('borrowed','lost','returned') DEFAULT 'borrowed'
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_loan_info`
--

INSERT INTO `book_loan_info` (`TRANSACTION_ID`, `ID`, `ISBN`, `LIBRARIAN_ID`, `DATE_BORROWED`, `RETURN_DATE`, `DATE_DUE_BACK`, `DATE_OVERDUE`, `status_before`, `status_after`, `BORROWER_ID`, `BORROWER_TYPE`, `LAST_EDIT`, `FLAG`) VALUES
(29, '1', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(30, '10', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(31, '11', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(32, '12', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(33, '13', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(34, '14', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(35, '15', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(36, '16', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(37, '17', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(38, '18', '61-462-6', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '4', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(39, '2', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(40, '20', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(41, '21', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(42, '22', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(43, '23', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(44, '24', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(45, '25', '61-462-6', NULL, '2018-05-23', '2018-05-26', NULL, NULL, 'good', 'good', '3', 'DEPARTMENT', '2018-05-23', 'borrowed'),
(46, '37', '61-463-3', NULL, '2018-05-23', '2018-05-30', NULL, NULL, 'good', 'good', '1000000', 'STUDENT', '2018-05-23', 'borrowed');

-- --------------------------------------------------------

--
-- Table structure for table `book_type`
--

CREATE TABLE IF NOT EXISTS `book_type` (
  `ISBN` varchar(17) NOT NULL,
  `BOOK_TITLE` varchar(100) NOT NULL,
  `AUTHOR_NAME` varchar(100) NOT NULL,
  `EDITION` varchar(15) NOT NULL,
  `CLASS_ID` varchar(10) DEFAULT NULL,
  `A_ID` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `LAST_EDIT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_type`
--

INSERT INTO `book_type` (`ISBN`, `BOOK_TITLE`, `AUTHOR_NAME`, `EDITION`, `CLASS_ID`, `A_ID`, `year`, `LAST_EDIT`) VALUES
('04-002-5', 'MATHEMATICS FOR SECONDARY SCHOOLS FORM TWO', 'SCSU & MoEVT-ZANZIBAR,Rev,Ed,2010', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-057-2', 'ENGLISH FOR SECONDARY SCHOOLS FORM ONE', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-457-2', 'PHYSICS FOR SECONDARY SCHOOLS FORM SIX', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-458-9', 'ENGLISH FOR SECONDARY SCHOOLS FORM TWO', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-462-6', 'BIOLOGY FOR SECONDARY SCHOOLS FORM FIVE', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23'),
('61-463-3', 'BIOLOGY FOR SECONDARY SCHOOLS FORM SIX', 'TANZANIA INSTITUTE OF EDUCATION', '1', '--select--', NULL, 0000, '2018-05-23');

-- --------------------------------------------------------

--
-- Stand-in structure for view `book_type_report`
--
CREATE TABLE IF NOT EXISTS `book_type_report` (
`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`LAST_EDIT` date
,`COPIES` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowable_view`
--
CREATE TABLE IF NOT EXISTS `borrowable_view` (
`ID` varchar(50)
,`ISBN` varchar(17)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowed_report`
--
CREATE TABLE IF NOT EXISTS `borrowed_report` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`EDITION` varchar(15)
,`BORROWER_DESCRIPTION` varchar(61)
,`BORROWER_ID` varchar(11)
,`BORROWER_TYPE` varchar(100)
,`STATUS_BEFORE` enum('good','bad')
,`FLAG` enum('borrowed','lost','returned')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowed_report_view`
--
CREATE TABLE IF NOT EXISTS `borrowed_report_view` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BORROWER_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`NAMES` varchar(61)
,`FLAG` enum('borrowed','lost','returned')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowers`
--
CREATE TABLE IF NOT EXISTS `borrowers` (
`borrower_id` varchar(11)
,`borrower_description` varchar(61)
,`b_type` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `borrower_type`
--

CREATE TABLE IF NOT EXISTS `borrower_type` (
`ID` int(11) NOT NULL,
  `TYPE` varchar(30) NOT NULL,
  `SYMBOL` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `borrowing_report`
--
CREATE TABLE IF NOT EXISTS `borrowing_report` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`DATE_DUE_BACK` date
,`DATE_OVERDUE` date
,`status_before` enum('good','bad')
,`status_after` enum('good','bad')
,`BORROWER_ID` varchar(10)
,`BORROWER_TYPE` varchar(100)
,`LAST_EDIT` date
,`FLAG` enum('borrowed','lost','returned')
,`NAMES` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `break_time`
--

CREATE TABLE IF NOT EXISTS `break_time` (
`bid` int(2) NOT NULL,
  `after_period` int(2) NOT NULL,
  `description` varchar(100) NOT NULL,
  `duration` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `break_time`
--

INSERT INTO `break_time` (`bid`, `after_period`, `description`, `duration`) VALUES
(2, 8, 'Lunch Break', 20),
(3, 5, 'Breakfast Break', 20);

-- --------------------------------------------------------

--
-- Stand-in structure for view `calculate_a_level_units_view`
--
CREATE TABLE IF NOT EXISTS `calculate_a_level_units_view` (
`term_id` int(11)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`points` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `calculate_o_level_units_view`
--
CREATE TABLE IF NOT EXISTS `calculate_o_level_units_view` (
`term_id` int(11)
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`average` float(5,2)
,`terminal` float(5,2)
,`subject_id` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`grade` varchar(3)
,`unit_point` int(2) unsigned
);
-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('fdtnhgev0rsjfhmn8mfbf0k6u52f6d3p', '::1', 1527779789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313532373737393738393b);

-- --------------------------------------------------------

--
-- Stand-in structure for view `classes_assigned_view`
--
CREATE TABLE IF NOT EXISTS `classes_assigned_view` (
`class_stream_id` varchar(10)
,`teacher_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`start_date` date
,`end_date` date
);
-- --------------------------------------------------------

--
-- Table structure for table `class_asset`
--

CREATE TABLE IF NOT EXISTS `class_asset` (
`id` int(11) NOT NULL,
  `asset_no` varchar(50) NOT NULL,
  `asset_name` varchar(20) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `status` mediumtext NOT NULL,
  `description` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_asset_view`
--
CREATE TABLE IF NOT EXISTS `class_asset_view` (
`id` int(11)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`asset_name` varchar(20)
,`asset_no` varchar(50)
,`status` mediumtext
,`description` varchar(25)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `class_journal_view`
--
CREATE TABLE IF NOT EXISTS `class_journal_view` (
`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`timetable_class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);
-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE IF NOT EXISTS `class_level` (
  `class_id` varchar(10) NOT NULL,
  `class_name` varchar(30) NOT NULL,
  `level` enum('O''Level','A''Level') NOT NULL,
  `next_class_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` (`class_id`, `class_name`, `level`, `next_class_id`) VALUES
('C01', 'Form One', 'O''Level', 'C02'),
('C02', 'Form Two', 'O''Level', 'C03'),
('C05', 'Form Five', 'A''Level', 'C06'),
('C06', 'Form Six', 'A''Level', 'C07');

-- --------------------------------------------------------

--
-- Table structure for table `class_stream`
--

CREATE TABLE IF NOT EXISTS `class_stream` (
  `class_stream_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `stream` varchar(5) NOT NULL,
  `stream_id` int(2) NOT NULL,
  `teacher_id` varchar(10) DEFAULT NULL,
  `admission_no` varchar(10) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `capacity` int(3) DEFAULT NULL,
  `number_of_subjects` int(2) NOT NULL,
  `id_display` varchar(30) DEFAULT NULL,
  `next_class_stream_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `class_stream_subject`
--

CREATE TABLE IF NOT EXISTS `class_stream_subject` (
  `class_stream_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `class_stream_subject`
--
DELIMITER //
CREATE TRIGGER `after_update_on_class_stream_subject` BEFORE UPDATE ON `class_stream_subject`
 FOR EACH ROW BEGIN
UPDATE student_subject SET class_stream_id="", subject_id="" WHERE class_stream_id=OLD.class_stream_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_stream_subject_display_view`
--
CREATE TABLE IF NOT EXISTS `class_stream_subject_display_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`subject_id` varchar(10)
,`stream` varchar(5)
,`class_subjects` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `class_stream_subject_view`
--
CREATE TABLE IF NOT EXISTS `class_stream_subject_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`subject_choice` enum('optional','compulsory')
,`subject_category` enum('T','N')
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `class_subject_teachers_view`
--
CREATE TABLE IF NOT EXISTS `class_subject_teachers_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`names` varchar(61)
,`subject_name` varchar(50)
,`class_stream_id` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `class_teacher_history`
--

CREATE TABLE IF NOT EXISTS `class_teacher_history` (
`id` int(10) unsigned NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `completed_students_view`
--
CREATE TABLE IF NOT EXISTS `completed_students_view` (
`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`id` int(11)
,`index_no` varchar(30)
,`admission_no` varchar(10)
,`division` varchar(2)
,`points` int(2)
,`kashachukua_cheti` enum('ndiyo','hapana')
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`academic_year_id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `compute_division`
--

CREATE TABLE IF NOT EXISTS `compute_division` (
`id` int(1) NOT NULL,
  `level` enum('O','A') NOT NULL,
  `nos` int(2) NOT NULL,
  `penalty` enum('yes','no') NOT NULL DEFAULT 'no',
  `penalty_divs` varchar(50) DEFAULT NULL,
  `assignedDiv` varchar(10) DEFAULT NULL,
  `penalty_grade` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `compute_division`
--

INSERT INTO `compute_division` (`id`, `level`, `nos`, `penalty`, `penalty_divs`, `assignedDiv`, `penalty_grade`) VALUES
(24, 'O', 7, 'yes', 'I,II', 'III', 'F'),
(25, 'A', 3, 'no', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `copy_duty_roaster`
--

CREATE TABLE IF NOT EXISTS `copy_duty_roaster` (
  `teacher_id` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `count_borrowed_per_person_view`
--
CREATE TABLE IF NOT EXISTS `count_borrowed_per_person_view` (
`copies` bigint(21)
,`names` varchar(61)
,`borrower_type` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `current_term_academic_year_view`
--
CREATE TABLE IF NOT EXISTS `current_term_academic_year_view` (
`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`class_level` enum('A''Level','O''Level')
,`id` int(5)
,`year` varchar(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_attendance_report_view`
--
CREATE TABLE IF NOT EXISTS `daily_attendance_report_view` (
`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`att_type` char(1)
,`nos` bigint(21)
,`date_` date
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_school_attendance_view`
--
CREATE TABLE IF NOT EXISTS `daily_school_attendance_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`att_type` char(1)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`date_` date
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_student_attendance_view`
--
CREATE TABLE IF NOT EXISTS `daily_student_attendance_view` (
`admission_no` varchar(10)
,`names` varchar(41)
,`att_type` char(1)
,`description` varchar(50)
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`date_` date
,`class_stream_id` varchar(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_student_roll_call_view`
--
CREATE TABLE IF NOT EXISTS `daily_student_roll_call_view` (
`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`att_type` char(1)
,`description` varchar(50)
,`date_` date
,`dorm_id` int(3)
,`att_id` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`dept_id` int(3) unsigned NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `dept_loc` varchar(50) DEFAULT NULL,
  `staff_id` varchar(10) DEFAULT NULL,
  `dept_type` enum('subject_department','non_subject_department') NOT NULL DEFAULT 'subject_department',
  `b_type` enum('DEPARTMENT') DEFAULT 'DEPARTMENT'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `department_class_stream_subject_view`
--
CREATE TABLE IF NOT EXISTS `department_class_stream_subject_view` (
`class_name` varchar(30)
,`subject_name` varchar(50)
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`class_stream_id` varchar(10)
,`class_id` varchar(10)
,`stream` varchar(5)
,`stream_id` int(2)
,`teacher_id` varchar(10)
,`admission_no` varchar(10)
,`description` varchar(255)
,`capacity` int(3)
,`id_display` varchar(30)
,`subject_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`staff_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `department_staff_view`
--
CREATE TABLE IF NOT EXISTS `department_staff_view` (
`staff_id` varchar(10)
,`staff_names` varchar(92)
,`dept_id` int(3) unsigned
,`gender` varchar(6)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `department_view`
--
CREATE TABLE IF NOT EXISTS `department_view` (
`staff_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`dept_type` enum('subject_department','non_subject_department')
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `disabled_students`
--
CREATE TABLE IF NOT EXISTS `disabled_students` (
`disabled` enum('yes','no')
,`statasi` enum('active','inactive')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`student_names` varchar(41)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`class_stream_id` varchar(10)
,`stream` varchar(5)
,`id` int(5)
,`status` enum('current_academic_year','past_academic_year')
,`class_level` enum('A''Level','O''Level')
,`year` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `discipline`
--

CREATE TABLE IF NOT EXISTS `discipline` (
`id` int(10) unsigned NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `description` mediumtext NOT NULL,
  `decision` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discipline`
--

INSERT INTO `discipline` (`id`, `admission_no`, `description`, `decision`, `date`) VALUES
(1, '1000000', 'katorokashuleni usiku nakwenda kuangalia mpirA', 'KURUDI NYUMBANI KWA WIKI MOJA', '2018-05-24'),
(2, '1234567', 'Kakutwa town bila uniform', 'SUSPENDED FOR 7 Days', '2018-05-24'),
(3, '1000345', 'Mlevi				Sanaaaa					fkhfkhfkf', 'EXPELLED FROM STUDIES FFFFFF', '2018-05-31'),
(4, '9999999', 'Kalevi		Hakaa', 'EXPELLED FROM STUDIES HHH', '2018-06-07'),
(5, '1000001', 'Oyooo', 'wtrh', '2018-05-30'),
(6, '1000003', 'Holla', 'EXPELLED FROM STUDIES', '2018-05-30'),
(7, '5552231', 'Kakutwa town', 'SUSPENDED FOR 13 Days', '2018-05-30'),
(8, '5552231', 'Holaaa', 'SUSPENDED FOR 8 Days', '2018-05-31');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
`id` int(3) NOT NULL,
  `level` enum('O','A') NOT NULL,
  `starting_points` int(2) NOT NULL,
  `ending_points` int(2) NOT NULL,
  `div_name` enum('I','II','III','IV','0') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `level`, `starting_points`, `ending_points`, `div_name`) VALUES
(1, '', 1, 1, ''),
(2, 'O', 1, 17, 'I'),
(3, 'O', 18, 21, 'II'),
(4, 'O', 22, 26, 'III'),
(5, 'O', 27, 33, 'IV'),
(6, 'O', 34, 35, '0'),
(7, 'A', 3, 9, 'I'),
(14, 'A', 10, 12, 'II'),
(9, 'A', 13, 16, 'III'),
(10, 'A', 17, 19, 'IV'),
(13, 'A', 20, 21, '0');

--
-- Triggers `division`
--
DELIMITER //
CREATE TRIGGER `before_delete_division_trigger` BEFORE DELETE ON `division`
 FOR EACH ROW BEGIN DECLARE x INT DEFAULT 0; SELECT id FROM division WHERE id=OLD.id INTO x; IF x = 1 THEN SIGNAL SQLSTATE '45000' SET message_text="Permission Denied, Cannot delete this value"; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `division_bk`
--

CREATE TABLE IF NOT EXISTS `division_bk` (
  `id` int(3) NOT NULL DEFAULT '0',
  `level` enum('O','A') NOT NULL,
  `starting_points` int(2) NOT NULL,
  `ending_points` int(2) NOT NULL,
  `div_name` enum('I','II','III','IV','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division_bk`
--

INSERT INTO `division_bk` (`id`, `level`, `starting_points`, `ending_points`, `div_name`) VALUES
(1, '', 1, 1, ''),
(2, 'O', 1, 17, 'I'),
(3, 'O', 18, 21, 'II'),
(4, 'O', 22, 26, 'III'),
(5, 'O', 27, 33, 'IV'),
(6, 'O', 34, 35, '0'),
(7, 'A', 3, 9, 'I'),
(8, 'A', 10, 12, 'II'),
(9, 'A', 13, 16, 'III'),
(10, 'A', 17, 19, 'IV'),
(11, 'A', 20, 21, '0');

-- --------------------------------------------------------

--
-- Table structure for table `dormitory`
--

CREATE TABLE IF NOT EXISTS `dormitory` (
`dorm_id` int(3) NOT NULL,
  `dorm_name` varchar(30) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `capacity` int(11) NOT NULL,
  `admission_no` varchar(10) DEFAULT NULL,
  `teacher_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dorm_asset`
--

CREATE TABLE IF NOT EXISTS `dorm_asset` (
`id` int(11) NOT NULL,
  `asset_no` varchar(50) NOT NULL,
  `asset_name` varchar(20) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `status` mediumtext NOT NULL,
  `description` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dorm_asset_view`
--
CREATE TABLE IF NOT EXISTS `dorm_asset_view` (
`id` int(11)
,`dorm_id` int(3)
,`dorm_name` varchar(30)
,`asset_name` varchar(20)
,`asset_no` varchar(50)
,`status` mediumtext
,`description` varchar(25)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `dorm_capacity_view`
--
CREATE TABLE IF NOT EXISTS `dorm_capacity_view` (
`dorm_id` int(3)
,`no_of_students` bigint(21)
,`dorm_name` varchar(30)
,`capacity` int(11)
,`remaining` bigint(22)
);
-- --------------------------------------------------------

--
-- Table structure for table `dorm_master_history`
--

CREATE TABLE IF NOT EXISTS `dorm_master_history` (
`id` int(10) unsigned NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `duty_roaster`
--

CREATE TABLE IF NOT EXISTS `duty_roaster` (
`id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_current` enum('T','F') NOT NULL DEFAULT 'F'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `duty_roaster`
--

INSERT INTO `duty_roaster` (`id`, `start_date`, `end_date`, `is_current`) VALUES
(1, '2017-12-18', '2017-12-24', 'F'),
(4, '2017-12-25', '2017-12-31', 'F'),
(5, '2018-01-01', '2018-01-07', 'F'),
(7, '2018-03-19', '2018-03-25', 'F'),
(8, '2018-01-22', '2018-01-28', 'F'),
(9, '2018-02-05', '2018-02-11', 'F'),
(12, '2016-08-01', '2016-08-07', 'F'),
(17, '2019-01-07', '2019-01-13', 'F'),
(23, '2021-01-25', '2021-01-31', 'F'),
(26, '2018-05-28', '2018-06-03', 'T');

--
-- Triggers `duty_roaster`
--
DELIMITER //
CREATE TRIGGER `before_delete_duty_roaster` BEFORE DELETE ON `duty_roaster`
 FOR EACH ROW BEGIN DECLARE is_cur ENUM('T','F') DEFAULT 'F'; SELECT is_current FROM duty_roaster WHERE id=OLD.id INTO is_cur; IF is_cur = 'T' THEN SIGNAL SQLSTATE '45000' SET message_text='Cannot delete this record while currently set as on duty'; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `edit_teaching_attendance_view`
--
CREATE TABLE IF NOT EXISTS `edit_teaching_attendance_view` (
`subTopicID` int(11)
,`subtopic_name` varchar(255)
,`ta_id` bigint(20)
,`no_of_absentees` int(2)
,`status` enum('taught','untaught','none')
,`date` date
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `edit_teaching_attendance_view_mpya`
--
CREATE TABLE IF NOT EXISTS `edit_teaching_attendance_view_mpya` (
`subTopicID` int(11)
,`subtopic_name` varchar(255)
,`ta_id` bigint(20)
,`no_of_absentees` int(2)
,`status` enum('taught','untaught','none')
,`date` date
,`subject_id` varchar(10)
,`t_id` int(5) unsigned
,`class_stream_id` varchar(10)
,`assignment_id` int(11)
,`period_no` int(2)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`subject_name` varchar(50)
,`subject_teacher` varchar(62)
);
-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

CREATE TABLE IF NOT EXISTS `enrollment` (
  `admission_no` varchar(10) NOT NULL DEFAULT '',
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL DEFAULT '',
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exam_type`
--

CREATE TABLE IF NOT EXISTS `exam_type` (
  `id` varchar(10) NOT NULL,
  `exam_name` varchar(30) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `academic_year` int(5) DEFAULT NULL,
  `a_level_academic_year` int(5) DEFAULT NULL,
  `add_score_enabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exam_type`
--

INSERT INTO `exam_type` (`id`, `exam_name`, `start_date`, `end_date`, `academic_year`, `a_level_academic_year`, `add_score_enabled`) VALUES
('E01', 'First Monthly Examinations', '2018-04-11', '2018-04-16', NULL, 3, 1),
('E02', 'Midterm Examinations', '2018-08-31', '2018-09-04', NULL, 3, 0),
('E03', 'Second Monthly Examinations', '2018-10-28', '2018-10-30', NULL, 3, 0),
('E04', 'End of term Examinations', '2018-12-01', '2018-12-10', NULL, 3, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `expelled_students_view`
--
CREATE TABLE IF NOT EXISTS `expelled_students_view` (
`admission_no` varchar(10)
,`date` date
,`academic_year` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`dob` date
,`tribe_id` int(5)
,`religion_id` int(5)
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`dorm_id` int(3)
,`status` enum('active','inactive')
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `expired_book_view`
--
CREATE TABLE IF NOT EXISTS `expired_book_view` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`BORROWER_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`TIME_LEFT` bigint(21)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`EDITION` varchar(15)
,`NAMES` varchar(61)
,`FLAG` enum('borrowed','lost','returned')
);
-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE IF NOT EXISTS `fee` (
  `fee_id` varchar(10) NOT NULL,
  `fee_name` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fee_details`
--

CREATE TABLE IF NOT EXISTS `fee_details` (
`id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `fee_id` varchar(10) NOT NULL,
  `date_paid` date NOT NULL,
  `amount_paid` int(11) NOT NULL,
  `amount_remaining` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fee_details`
--

INSERT INTO `fee_details` (`id`, `admission_no`, `fee_id`, `date_paid`, `amount_paid`, `amount_remaining`) VALUES
(1, 'STD0081', 'F1', '2016-07-20', 20000, 0),
(2, 'STD0081', 'F2', '2016-07-20', 10000, 0),
(3, 'STD0081', 'F3', '2016-07-20', 20000, 0),
(4, 'STD0081', 'F4', '2016-07-20', 15000, 0),
(5, 'STD0081', 'F5', '2016-07-20', 5000, 0),
(6, 'STD0081', 'F6', '2016-07-20', 3000, 0),
(7, 'STD0081', 'F7', '2016-07-20', 10000, 0),
(8, 'STD0082', 'F1', '2016-07-21', 20000, 0),
(9, 'STD0082', 'F2', '2016-07-21', 10000, 0),
(10, 'STD0082', 'F3', '2016-07-21', 20000, 0),
(11, 'STD0082', 'F4', '2016-07-21', 10000, 0),
(12, 'STD0082', 'F5', '2016-07-21', 5000, 0),
(13, 'STD0082', 'F6', '2016-07-21', 3000, 0),
(14, 'STD0082', 'F7', '2016-07-21', 5000, 0),
(15, 'STD0083', 'F1', '2016-07-21', 20000, 0),
(16, 'STD0083', 'F2', '2016-07-21', 10000, 0),
(17, 'STD0083', 'F3', '2016-07-21', 20000, 0),
(18, 'STD0083', 'F4', '2016-07-21', 15000, 0),
(19, 'STD0083', 'F5', '2016-07-21', 5000, 0),
(20, 'STD0083', 'F6', '2016-07-21', 3000, 0),
(21, 'STD0083', 'F7', '2016-07-21', 10000, 0),
(22, 'STD0084', 'F1', '2016-07-10', 20000, 0),
(23, 'STD0084', 'F2', '2016-07-10', 10000, 0),
(24, 'STD0084', 'F3', '2016-07-10', 20000, 0),
(25, 'STD0084', 'F4', '2016-07-10', 15000, 0),
(26, 'STD0084', 'F5', '2016-07-10', 5000, 0),
(27, 'STD0084', 'F6', '2016-07-10', 3000, 0),
(28, 'STD0084', 'F7', '2016-07-10', 10000, 0),
(29, 'STD0085', 'F1', '2016-07-10', 20000, 0),
(30, 'STD0085', 'F2', '2016-07-10', 10000, 0),
(31, 'STD0085', 'F3', '2016-07-10', 20000, 0),
(32, 'STD0085', 'F4', '2016-07-10', 15000, 0),
(33, 'STD0085', 'F5', '2016-07-10', 5000, 0),
(34, 'STD0085', 'F6', '2016-07-10', 3000, 0),
(35, 'STD0085', 'F7', '2016-07-10', 10000, 0),
(36, 'STD0086', 'F1', '2016-07-11', 20000, 0),
(37, 'STD0086', 'F2', '2016-07-11', 10000, 0),
(38, 'STD0086', 'F3', '2016-07-11', 20000, 0),
(39, 'STD0086', 'F4', '2016-07-11', 15000, 0),
(40, 'STD0086', 'F5', '2016-07-11', 5000, 0),
(41, 'STD0086', 'F6', '2016-07-11', 0, 0),
(42, 'STD0086', 'F7', '2016-07-11', 0, 0),
(43, 'STD0087', 'F1', '2016-07-17', 20000, 0),
(44, 'STD0087', 'F2', '2016-07-17', 10000, 0),
(45, 'STD0087', 'F3', '2016-07-17', 20000, 0),
(46, 'STD0087', 'F4', '2016-07-17', 15000, 0),
(47, 'STD0087', 'F5', '2016-07-17', 2000, 0),
(48, 'STD0087', 'F6', '2016-07-17', 0, 0),
(49, 'STD0087', 'F7', '2016-07-17', 0, 0),
(50, 'STD0088', 'F1', '2016-07-17', 20000, 0),
(51, 'STD0088', 'F2', '2016-07-17', 10000, 0),
(52, 'STD0088', 'F3', '2016-07-17', 20000, 0),
(53, 'STD0088', 'F4', '2016-07-17', 15000, 0),
(54, 'STD0088', 'F5', '2016-07-17', 5000, 0),
(55, 'STD0088', 'F6', '2016-07-17', 3000, 0),
(56, 'STD0088', 'F7', '2016-07-17', 10000, 0),
(57, 'STD0089', 'F1', '2016-07-17', 20000, 0),
(58, 'STD0089', 'F2', '2016-07-17', 10000, 0),
(59, 'STD0089', 'F3', '2016-07-17', 20000, 0),
(60, 'STD0089', 'F4', '2016-07-17', 15000, 0),
(61, 'STD0089', 'F5', '2016-07-17', 5000, 0),
(62, 'STD0089', 'F6', '2016-07-17', 3000, 0),
(63, 'STD0089', 'F7', '2016-07-17', 10000, 0),
(64, 'STD0090', 'F1', '2016-07-18', 20000, 0),
(65, 'STD0090', 'F2', '2016-07-18', 10000, 0),
(66, 'STD0090', 'F3', '2016-07-18', 20000, 0),
(67, 'STD0090', 'F4', '2016-07-18', 0, 0),
(68, 'STD0090', 'F5', '2016-07-18', 0, 0),
(69, 'STD0090', 'F6', '2016-07-18', 0, 0),
(70, 'STD0090', 'F7', '2016-07-18', 0, 0),
(71, 'STD0091', 'F1', '2015-07-08', 20000, 0),
(72, 'STD0091', 'F2', '2015-07-08', 10000, 0),
(73, 'STD0091', 'F3', '2015-07-08', 20000, 0),
(74, 'STD0091', 'F4', '2015-07-08', 15000, 0),
(75, 'STD0091', 'F5', '2015-07-08', 5000, 0),
(76, 'STD0091', 'F6', '2015-07-08', 3000, 0),
(77, 'STD0091', 'F7', '2015-07-08', 10000, 0),
(78, 'STD0091', 'F8', '2015-07-08', 10000, 0),
(79, 'STD0091', 'F9', '2015-07-08', 25000, 0),
(80, 'STD0092', 'F1', '2015-07-08', 20000, 0),
(81, 'STD0092', 'F2', '2015-07-08', 10000, 0),
(82, 'STD0092', 'F3', '2015-07-08', 20000, 0),
(83, 'STD0092', 'F4', '2015-07-08', 15000, 0),
(84, 'STD0092', 'F5', '2015-07-08', 5000, 0),
(85, 'STD0092', 'F6', '2015-07-08', 3000, 0),
(86, 'STD0092', 'F7', '2015-07-08', 10000, 0),
(87, 'STD0092', 'F8', '2015-07-08', 10000, 0),
(88, 'STD0092', 'F9', '2015-07-08', 25000, 0),
(89, 'STD0093', 'F1', '2015-07-08', 20000, 0),
(90, 'STD0093', 'F2', '2015-07-08', 10000, 0),
(91, 'STD0093', 'F3', '2015-07-08', 20000, 0),
(92, 'STD0093', 'F4', '2015-07-08', 15000, 0),
(93, 'STD0093', 'F5', '2015-07-08', 5000, 0),
(94, 'STD0093', 'F6', '2015-07-08', 3000, 0),
(95, 'STD0093', 'F7', '2015-07-08', 0, 0),
(96, 'STD0093', 'F8', '2015-07-08', 0, 0),
(97, 'STD0093', 'F9', '2015-07-08', 0, 0),
(98, 'STD0094', 'F1', '2015-07-09', 20000, 0),
(99, 'STD0094', 'F2', '2015-07-09', 10000, 0),
(100, 'STD0094', 'F3', '2015-07-09', 20000, 0),
(101, 'STD0094', 'F4', '2015-07-09', 15000, 0),
(102, 'STD0094', 'F5', '2015-07-09', 5000, 0),
(103, 'STD0094', 'F6', '2015-07-09', 3000, 0),
(104, 'STD0094', 'F7', '2015-07-09', 10000, 0),
(105, 'STD0094', 'F8', '2015-07-09', 10000, 0),
(106, 'STD0094', 'F9', '2015-07-09', 25000, 0),
(107, 'STD0095', 'F1', '2015-07-09', 20000, 0),
(108, 'STD0095', 'F2', '2015-07-09', 10000, 0),
(109, 'STD0095', 'F3', '2015-07-09', 20000, 0),
(110, 'STD0095', 'F4', '2015-07-09', 15000, 0),
(111, 'STD0095', 'F5', '2015-07-09', 5000, 0),
(112, 'STD0095', 'F6', '2015-07-09', 3000, 0),
(113, 'STD0095', 'F7', '2015-07-09', 10000, 0),
(114, 'STD0095', 'F8', '2015-07-09', 10000, 0),
(115, 'STD0095', 'F9', '2015-07-09', 25000, 0),
(116, 'STD0096', 'F1', '2015-07-09', 20000, 0),
(117, 'STD0096', 'F2', '2015-07-09', 10000, 0),
(118, 'STD0096', 'F3', '2015-07-09', 20000, 0),
(119, 'STD0096', 'F4', '2015-07-09', 15000, 0),
(120, 'STD0096', 'F5', '2015-07-09', 5000, 0),
(121, 'STD0096', 'F6', '2015-07-09', 3000, 0),
(122, 'STD0096', 'F7', '2015-07-09', 10000, 0),
(123, 'STD0096', 'F8', '2015-07-09', 10000, 0),
(124, 'STD0096', 'F9', '2015-07-09', 25000, 0),
(125, 'STD0097', 'F1', '2015-07-09', 20000, 0),
(126, 'STD0097', 'F2', '2015-07-09', 10000, 0),
(127, 'STD0097', 'F3', '2015-07-09', 0, 0),
(128, 'STD0097', 'F4', '2015-07-09', 0, 0),
(129, 'STD0097', 'F5', '2015-07-09', 0, 0),
(130, 'STD0097', 'F6', '2015-07-09', 0, 0),
(131, 'STD0097', 'F7', '2015-07-09', 0, 0),
(132, 'STD0097', 'F8', '2015-07-09', 10000, 0),
(133, 'STD0097', 'F9', '2015-07-09', 25000, 0),
(134, 'STD0098', 'F1', '2015-07-09', 20000, 0),
(135, 'STD0098', 'F2', '2015-07-09', 10000, 0),
(136, 'STD0098', 'F3', '2015-07-09', 20000, 0),
(137, 'STD0098', 'F4', '2015-07-09', 15000, 0),
(138, 'STD0098', 'F5', '2015-07-09', 5000, 0),
(139, 'STD0098', 'F6', '2015-07-09', 3000, 0),
(140, 'STD0098', 'F7', '2015-07-09', 10000, 0),
(141, 'STD0098', 'F8', '2015-07-09', 10000, 0),
(142, 'STD0098', 'F9', '2015-07-09', 25000, 0),
(143, 'STD0099', 'F1', '2015-07-09', 20000, 0),
(144, 'STD0099', 'F2', '2015-07-09', 10000, 0),
(145, 'STD0099', 'F3', '2015-07-09', 20000, 0),
(146, 'STD0099', 'F4', '2015-07-09', 15000, 0),
(147, 'STD0099', 'F5', '2015-07-09', 5000, 0),
(148, 'STD0099', 'F6', '2015-07-09', 0, 0),
(149, 'STD0099', 'F7', '2015-07-09', 0, 0),
(150, 'STD0099', 'F8', '2015-07-09', 10000, 0),
(151, 'STD0099', 'F9', '2015-07-09', 25000, 0),
(152, 'STD0100', 'F1', '2015-07-09', 20000, 0),
(153, 'STD0100', 'F2', '2015-07-09', 10000, 0),
(154, 'STD0100', 'F3', '2015-07-09', 20000, 0),
(155, 'STD0100', 'F4', '2015-07-09', 15000, 0),
(156, 'STD0100', 'F5', '2015-07-09', 0, 0),
(157, 'STD0100', 'F6', '2015-07-09', 0, 0),
(158, 'STD0100', 'F7', '2015-07-09', 0, 0),
(159, 'STD0100', 'F8', '2015-07-09', 10000, 0),
(160, 'STD0100', 'F9', '2015-07-09', 25000, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `filtered_teaching_assignment`
--
CREATE TABLE IF NOT EXISTS `filtered_teaching_assignment` (
`assignment_id` int(11)
,`teacher_id` varchar(10)
,`subject_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`term_id` int(11)
,`filter_flag` tinyint(1)
,`dept_id` int(3) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `first_join_staff_attendance_view`
--
CREATE TABLE IF NOT EXISTS `first_join_staff_attendance_view` (
`firstname` varchar(30)
,`lastname` varchar(30)
,`staff_id` varchar(10)
,`date_` date
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`at` char(1)
,`att_desc` varchar(50)
,`change_type` enum('ADD','UPDATE','REMOVE')
,`change_by` varchar(10)
,`ip_address` varchar(255)
,`change_time` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table `group_type`
--

CREATE TABLE IF NOT EXISTS `group_type` (
`group_id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_type`
--

INSERT INTO `group_type` (`group_id`, `group_name`, `description`) VALUES
(1, 'Walimu Wa Darasa', 'Wasimamizi wa shughuli zote za darasa lake');

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE IF NOT EXISTS `guardian` (
  `admission_no` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `rel_type` enum('Father','Mother','Guardian') NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `p_box` int(5) NOT NULL,
  `p_region` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hod_history`
--

CREATE TABLE IF NOT EXISTS `hod_history` (
`id` int(10) unsigned NOT NULL,
  `dept_id` int(3) unsigned NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inactive_staff`
--

CREATE TABLE IF NOT EXISTS `inactive_staff` (
  `staff_id` varchar(10) NOT NULL,
  `deactivated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
`s_no` int(11) NOT NULL,
  `source` varchar(50) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `received_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE IF NOT EXISTS `librarian` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lock_account_tbl`
--

CREATE TABLE IF NOT EXISTS `lock_account_tbl` (
  `username` varchar(13) NOT NULL,
  `account_locked` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `count_failed_attempt` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `lost_book_view`
--
CREATE TABLE IF NOT EXISTS `lost_book_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`LAST_EDIT` date
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
`module_id` int(3) unsigned NOT NULL,
  `module_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `module_name`) VALUES
(1, 'Admission'),
(2, 'Manage Finance'),
(3, 'Staff Management'),
(4, 'Student Management'),
(5, 'Library Management'),
(6, 'Accomodation'),
(7, 'Administration'),
(8, 'Exams'),
(9, 'Reports'),
(10, 'Inventory'),
(11, 'Department Management'),
(12, 'Class Management'),
(13, 'Timetable'),
(14, 'Announcements'),
(15, 'My Assigned Classes'),
(16, 'Completed Students'),
(17, 'TOD Management'),
(18, 'Discipline Management'),
(19, 'My Account'),
(20, 'My Department'),
(21, 'My Dormitory'),
(22, 'My Class'),
(23, 'TOD'),
(24, 'Results'),
(25, 'Role'),
(26, 'Class Results'),
(27, 'Subject Management'),
(28, 'Academic Year'),
(29, 'Admin Settings'),
(30, 'Notifications');

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE IF NOT EXISTS `month` (
  `month_id` varchar(2) NOT NULL,
  `month_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`month_id`, `month_name`) VALUES
('01', 'January'),
('02', 'February'),
('03', 'March'),
('04', 'April'),
('05', 'May'),
('06', 'June'),
('07', 'July'),
('08', 'August'),
('09', 'September'),
('10', 'October'),
('11', 'November'),
('12', 'December');

-- --------------------------------------------------------

--
-- Stand-in structure for view `my_announcements_view`
--
CREATE TABLE IF NOT EXISTS `my_announcements_view` (
`gid` int(11)
,`announcement_id` int(11)
,`group_id` int(11)
,`heading` varchar(255)
,`msg` longtext
,`time` timestamp
,`posted_by` varchar(10)
,`status` varchar(4)
,`staff_id` varchar(10)
,`is_read` varchar(6)
,`username` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
`notification_id` int(11) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `msg_id` int(2) NOT NULL,
  `msg` mediumtext NOT NULL,
  `time_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notification_status` enum('read','unread') DEFAULT 'unread'
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `out_book`
--
CREATE TABLE IF NOT EXISTS `out_book` (
`TRANSACTION_ID` int(11)
,`ID` varchar(50)
,`ISBN` varchar(17)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`RETURN_DATE` date
,`DATE_DUE_BACK` date
,`DATE_OVERDUE` date
,`STATUS_BEFORE` enum('good','bad')
,`STATUS_AFTER` enum('good','bad')
,`BORROWER_ID` varchar(10)
,`BORROWER_TYPE` varchar(100)
,`LAST_EDIT` date
,`FLAG` enum('borrowed','lost','returned')
);
-- --------------------------------------------------------

--
-- Table structure for table `o_level_grade_system_tbl`
--

CREATE TABLE IF NOT EXISTS `o_level_grade_system_tbl` (
`id` int(3) NOT NULL,
  `start_mark` float(5,2) NOT NULL,
  `end_mark` float(5,2) NOT NULL,
  `grade` enum('A','B+','B','C','D','F') NOT NULL,
  `unit_point` int(2) unsigned NOT NULL,
  `remarks` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o_level_grade_system_tbl`
--

INSERT INTO `o_level_grade_system_tbl` (`id`, `start_mark`, `end_mark`, `grade`, `unit_point`, `remarks`) VALUES
(10, 0.00, 30.00, 'F', 6, 'Fail'),
(11, 30.01, 45.00, 'D', 5, 'Pass'),
(12, 45.01, 60.00, 'C', 4, 'Credit'),
(13, 60.01, 70.00, 'B', 3, 'Good'),
(14, 70.01, 80.00, 'B+', 2, 'Very Good'),
(15, 80.01, 100.00, 'A', 1, 'Excellent');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tbl`
--

CREATE TABLE IF NOT EXISTS `password_reset_tbl` (
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `password_reset_tbl`
--

INSERT INTO `password_reset_tbl` (`password`) VALUES
('a96af5cb3ae3b0e034f1c597133307de25068490');

-- --------------------------------------------------------

--
-- Table structure for table `payee`
--

CREATE TABLE IF NOT EXISTS `payee` (
`s_no` int(11) NOT NULL,
  `cheque_no` varchar(20) NOT NULL,
  `amount_paid` bigint(20) NOT NULL,
  `payee_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `approved` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE IF NOT EXISTS `period` (
  `period_no` int(2) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `duration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`period_no`, `start_time`, `end_time`, `duration`) VALUES
(1, '08:00:00', '08:40:00', 40),
(2, '08:40:00', '09:20:00', 40),
(3, '09:20:00', '10:00:00', 40),
(4, '10:00:00', '10:40:00', 40),
(5, '10:40:00', '11:20:00', 40),
(6, '11:40:00', '12:20:00', 40),
(7, '12:20:00', '13:00:00', 40),
(8, '13:00:00', '13:20:00', 20),
(9, '13:40:00', '14:20:00', 40),
(10, '14:20:00', '15:00:00', 40);

--
-- Triggers `period`
--
DELIMITER //
CREATE TRIGGER `before_insert_on_period` BEFORE INSERT ON `period`
 FOR EACH ROW BEGIN DECLARE min_start_time time; DECLARE max_end_time time; SELECT MIN(start_time) FROM period INTO min_start_time; SELECT MAX(end_time) FROM period INTO max_end_time; IF (min_start_time <= NEW.start_time && max_end_time >= NEW.start_time) THEN SIGNAL SQLSTATE '45000' SET message_text="Invalid input, the time you are trying to enter overlaps with the available period"; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
`permission_id` int(5) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  `common_to` enum('class_teachers','hods','dormitory_masters','all','admin','other') NOT NULL DEFAULT 'other',
  `module_id` int(3) unsigned NOT NULL,
  `permission_type` enum('root_child_node','root_node','child_node') NOT NULL,
  `root_node_id` int(5) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `description`, `common_to`, `module_id`, `permission_type`, `root_node_id`) VALUES
(1, 'Manage Staff', 'other', 3, 'child_node', 75),
(3, 'Activate-Deactivate Staff Account', 'admin', 3, 'child_node', 175),
(4, 'Register Student', 'other', 4, 'child_node', 5),
(5, 'View Registered Students', 'other', 4, 'root_node', 5),
(6, 'Promote Students', 'other', 4, 'root_node', 6),
(11, 'Manage Department', 'other', 11, 'child_node', 17),
(12, 'View Subject Teaching Logs', 'other', 11, 'child_node', 17),
(13, 'Manage Teaching Assignments', 'other', 11, 'child_node', 17),
(15, 'Add HOD', 'other', 11, 'child_node', 17),
(17, 'View Departments', 'other', 11, 'root_node', 17),
(18, 'Add Academic Year', 'admin', 28, 'child_node', 19),
(19, 'View Academic Years', 'other', 28, 'root_node', 19),
(20, 'Update Academic Year', 'admin', 28, 'child_node', 19),
(21, 'Add New Term', 'admin', 28, 'child_node', 19),
(22, 'Update Term', 'admin', 28, 'child_node', 19),
(23, 'Set Current Term', 'admin', 28, 'child_node', 19),
(24, 'View TODs', 'other', 17, 'root_node', 24),
(25, 'Manage TODs', 'other', 17, 'child_node', 24),
(27, 'Manage Dormitory', 'other', 6, 'child_node', 43),
(30, 'Manage O''Level Grade System', 'other', 8, 'child_node', 34),
(31, 'Manage A''Level Grade System', 'other', 8, 'child_node', 35),
(34, 'View O''Level Grade System', 'other', 8, 'root_node', 34),
(35, 'View A''Level Grade System', 'other', 8, 'root_node', 35),
(36, 'Manage Subjects', 'other', 27, 'child_node', 39),
(39, 'View Subjects', 'other', 27, 'root_node', 39),
(40, 'View Class Stream Subjects', 'other', 27, 'root_node', 40),
(42, 'View Own Department Staffs', 'hods', 20, 'root_node', 42),
(43, 'View Dormitories', 'other', 6, 'root_node', 43),
(44, 'Manage Dormitory Assets', 'other', 6, 'child_node', 135),
(45, 'Manage Class', 'other', 12, 'root_node', 45),
(48, 'View TOD Report', 'other', 17, 'root_node', 48),
(49, 'Manage TOD Reports', 'other', 17, 'child_node', 48),
(50, 'View Classes', 'other', 12, 'root_node', 50),
(52, 'Remove Stream', 'other', 12, 'child_node', 55),
(54, 'Update Stream', 'other', 12, 'child_node', 55),
(55, 'View Streams', 'other', 12, 'root_node', 55),
(58, 'View Exam Types', 'other', 8, 'root_node', 58),
(59, 'Update Exam Type', 'other', 8, 'child_node', 58),
(61, 'Enable/Disable Exam Types', 'other', 8, 'child_node', 58),
(62, 'Change Own Password', 'all', 19, 'root_node', 62),
(63, 'View Class Attendance', 'other', 17, 'root_node', 63),
(64, 'View School Attendance', 'other', 7, 'root_node', 64),
(65, 'Manage Score & Results Of Assigned Subjects', 'all', 8, 'root_node', 65),
(66, 'Add Score Of Own Subject', 'all', 8, 'child_node', 65),
(67, 'Manage Selected Students', 'other', 4, 'child_node', 83),
(68, 'View Own Department Classes', 'hods', 20, 'root_node', 68),
(69, 'Assign Teacher To Subject', 'hods', 20, 'child_node', 100),
(70, 'Remove Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(71, 'Explore School Attendance', 'other', 7, 'child_node', 64),
(73, 'Manage Timetable', 'other', 13, 'child_node', 80),
(75, 'View Staffs', 'other', 3, 'root_node', 75),
(77, 'Add Own Qualification', 'all', 19, 'root_node', 77),
(78, 'View Own Profile', 'all', 19, 'root_node', 78),
(80, 'View School Timetable', 'other', 13, 'root_node', 80),
(81, 'Update Own Profile', 'all', 19, 'child_node', 78),
(82, 'Update Score Of Own Subject', 'all', 8, 'child_node', 65),
(83, 'View Selected Students', 'other', 4, 'root_node', 83),
(84, 'View Transferred IN Students', 'other', 4, 'root_node', 84),
(85, 'View Transferred OUT Students', 'other', 4, 'root_node', 85),
(86, 'View Suspended Students', 'other', 4, 'root_node', 86),
(88, 'View Enrolled Students', 'other', 4, 'root_node', 88),
(89, 'View Disabled Students', 'other', 4, 'root_node', 89),
(90, 'Suspend Student', 'other', 4, 'child_node', 5),
(91, 'View Class Streams', 'other', 12, 'root_node', 91),
(92, 'View Students In Dormitory', 'other', 6, 'root_child_node', 43),
(93, 'Transfer Student', 'other', 4, 'child_node', 5),
(95, 'Manage Groups', 'admin', 29, 'root_node', 95),
(99, 'View Staffs Details', 'other', 3, 'child_node', 75),
(100, 'View Assinged Teachers In Own Department', 'hods', 20, 'root_node', 100),
(101, 'Update Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(103, 'View Teaching Log In Own Department', 'hods', 20, 'child_node', 68),
(104, 'View Activity Log', 'admin', 29, 'root_node', 104),
(105, 'View Staff Activity Log', 'admin', 3, 'child_node', 99),
(106, 'View Audit Trail', 'admin', 29, 'root_node', 106),
(107, 'Edit Student Results', 'other', 16, 'child_node', 179),
(108, 'Add Completion Details', 'other', 16, 'child_node', 174),
(109, 'View Own Class Results', 'class_teachers', 24, 'root_node', 109),
(111, 'Add Class Attedance', 'class_teachers', 22, 'child_node', 112),
(112, 'View Own Class Attedance', 'class_teachers', 22, 'root_node', 112),
(113, 'View Students In Own Class', 'class_teachers', 22, 'root_node', 113),
(114, 'View Assigned Teachers In Own Class', 'class_teachers', 22, 'root_node', 114),
(115, 'View Own Class Timetable', 'class_teachers', 22, 'root_node', 115),
(116, 'Add Class Journal Record', 'class_teachers', 22, 'child_node', 118),
(117, 'Update Class Journal', 'class_teachers', 22, 'child_node', 118),
(118, 'View Own Class Journal Report', 'class_teachers', 22, 'root_node', 118),
(119, 'Add Class Monitor', 'class_teachers', 22, 'child_node', 113),
(120, 'View Students In Own Dormitory', 'dormitory_masters', 21, 'root_node', 120),
(121, 'Add Dormitory Leader', 'dormitory_masters', 21, 'child_node', 120),
(122, 'View Assets In Own Dormitory', 'dormitory_masters', 21, 'root_node', 122),
(125, 'Update Student', 'other', 4, 'child_node', 5),
(127, 'Manage Own Posted Announcements', 'all', 14, 'root_node', 127),
(128, 'View Announcements', 'all', 14, 'root_node', 128),
(131, 'View Own Roles', 'all', 25, 'root_node', 131),
(132, 'View Assigned Classes', 'all', 15, 'root_node', 132),
(133, 'Add Dormitory Master', 'other', 6, 'child_node', 43),
(134, 'View Roll Call', 'other', 6, 'child_node', 43),
(135, 'View Assets In Dormitory', 'other', 6, 'root_child_node', 43),
(137, 'Reset Password', 'admin', 3, 'root_node', 137),
(138, 'Manage Staffs Attendance', 'other', 3, 'child_node', 165),
(142, 'Manage Class Subjects', 'other', 27, 'child_node', 40),
(146, 'Add Roll Call', 'dormitory_masters', 21, 'child_node', 147),
(147, 'View Roll Call In Own Dormitory', 'dormitory_masters', 21, 'root_node', 147),
(150, 'Add Student To Dormitory', 'other', 6, 'child_node', 92),
(154, 'Manage Roles', 'admin', 25, 'root_node', 154),
(155, 'Manage Division', 'other', 8, 'child_node', 158),
(158, 'View Divisions', 'other', 8, 'root_node', 158),
(161, 'Restore Suspended Student', 'other', 4, 'child_node', 86),
(165, 'View Staff Attendance', 'other', 3, 'root_node', 165),
(169, 'Manage Staff Qualifications', 'other', 3, 'child_node', 99),
(170, 'View Students In Own Department', 'hods', 20, 'child_node', 68),
(172, 'View Personal Timetable', 'all', 13, 'root_node', 172),
(174, 'Manage Alumni', 'other', 16, 'root_node', 174),
(175, 'Manage Staff Accounts', 'admin', 3, 'root_node', 175),
(176, 'View Own Log', 'all', 15, 'child_node', 189),
(177, 'Manage Teaching Log', 'all', 15, 'child_node', 189),
(179, 'View School Results', 'other', 24, 'root_node', 179),
(180, 'Manage Indiscipline Records', 'other', 18, 'child_node', 181),
(181, 'View Indiscipline Records', 'other', 18, 'root_node', 181),
(182, 'View All Teachers', 'other', 7, 'root_node', 182),
(183, 'Manage Periods', 'other', 7, 'child_node', 184),
(184, 'View Periods', 'other', 7, 'root_node', 184),
(185, 'Perform Database Backup', 'all', 29, 'root_node', 185),
(186, 'Change Avatar', 'all', 19, 'child_node', 78),
(189, 'View Topics', 'other', 15, 'root_child_node', 132),
(190, 'Manage Topics', 'other', 15, 'child_node', 189),
(193, 'View Subtopics', 'other', 15, 'root_child_node', 189),
(194, 'Manage Subtopics', 'other', 15, 'child_node', 193),
(197, 'Assign Class Teacher', 'other', 12, 'child_node', 91),
(198, 'View Students In Class', 'other', 12, 'root_child_node', 91),
(199, 'View Notifications', 'all', 30, 'root_node', 199),
(200, 'Transfer Student To Another Dormitory', 'other', 6, 'child_node', 92),
(201, 'View Class Journal Report', 'other', 12, 'child_node', 91),
(202, 'Manage Disabled Students', 'other', 4, 'child_node', 89),
(203, 'Restore Transferred Students', 'other', 4, 'child_node', 85),
(205, 'Assign Option Subject', 'other', 4, 'child_node', 5),
(206, 'Shift Student To Another Stream', 'other', 12, 'child_node', 198),
(207, 'Add Subjects To Teacher', 'other', 7, 'child_node', 182),
(208, 'View Subjects Teachers', 'other', 27, 'child_node', 39),
(211, 'Upload Student''s Photo', 'all', 4, 'child_node', 162),
(212, 'Manage Class Assets', 'other', 12, 'child_node', 91),
(215, 'View Assets In Class', 'other', 12, 'child_node', 91),
(216, 'View Assigned Teachers In Class', 'other', 12, 'child_node', 91),
(217, 'View Teacher Details', 'other', 7, 'child_node', 182),
(218, 'View Staff Role', 'admin', 7, 'child_node', 75);

-- --------------------------------------------------------

--
-- Table structure for table `permission_bk`
--

CREATE TABLE IF NOT EXISTS `permission_bk` (
  `permission_id` int(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `common_to` enum('class_teachers','hods','dormitory_masters','all','other') CHARACTER SET utf8 NOT NULL DEFAULT 'other',
  `module_id` int(3) unsigned NOT NULL,
  `permission_type` enum('root_child_node','root_node','child_node') CHARACTER SET utf8 NOT NULL,
  `root_node_id` int(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_bk`
--

INSERT INTO `permission_bk` (`permission_id`, `description`, `common_to`, `module_id`, `permission_type`, `root_node_id`) VALUES
(1, 'Add New Staff', 'other', 3, 'child_node', 175),
(2, 'Update Staff', 'other', 3, 'child_node', 175),
(3, 'Activate Staff Account', 'other', 3, 'child_node', 175),
(4, 'Register Student', 'other', 4, 'child_node', 5),
(5, 'View Registered Students', 'other', 4, 'root_node', 5),
(6, 'Promote Students', 'other', 4, 'root_node', 6),
(11, 'Add New Department', 'other', 11, 'child_node', 17),
(12, 'Update Department', 'other', 11, 'child_node', 17),
(13, 'Remove Department', 'other', 11, 'child_node', 17),
(14, 'View Department Members', 'other', 11, 'child_node', 17),
(15, 'Add HOD', 'other', 11, 'child_node', 17),
(16, 'View Subjects Offered By Department', 'other', 11, 'child_node', 17),
(17, 'View Departments', 'other', 11, 'root_node', 17),
(18, 'Add Academic Year', 'other', 28, 'child_node', 19),
(19, 'View Academic Years', 'other', 28, 'root_node', 19),
(20, 'Update Academic Year', 'other', 28, 'child_node', 19),
(21, 'Add New Term', 'other', 28, 'child_node', 19),
(22, 'Update Term', 'other', 28, 'child_node', 19),
(23, 'Set Current Term', 'other', 28, 'child_node', 19),
(24, 'View TODs', 'other', 17, 'root_node', 24),
(25, 'Add New TOD', 'other', 17, 'child_node', 24),
(26, 'Update TOD', 'other', 17, 'child_node', 24),
(27, 'Add New Dormitory', 'other', 6, 'child_node', 43),
(28, 'Update Dormitory', 'other', 6, 'child_node', 43),
(29, 'Remove Dormitory', 'other', 6, 'child_node', 43),
(30, 'Add O''Level Grade System', 'other', 8, 'child_node', 34),
(31, 'Add A''Level Grade System', 'other', 8, 'child_node', 35),
(32, 'Update A''Level Grade System', 'other', 8, 'child_node', 35),
(33, 'Update O''Level Grade System', 'other', 8, 'child_node', 34),
(34, 'View O''Level Grade System', 'other', 8, 'root_node', 34),
(35, 'View A''Level Grade System', 'other', 8, 'root_node', 35),
(36, 'Add New Subject', 'other', 27, 'child_node', 39),
(37, 'Update Subject', 'other', 27, 'child_node', 39),
(38, 'Remove Subject', 'other', 27, 'child_node', 39),
(39, 'View Subjects', 'other', 27, 'root_node', 39),
(40, 'View Class Stream Subjects', 'other', 27, 'root_node', 40),
(41, 'Remove TOD', 'other', 17, 'child_node', 24),
(42, 'View Own Department Staffs', 'hods', 20, 'root_node', 42),
(43, 'View Dormitories', 'other', 6, 'root_node', 43),
(44, 'Add Asset In Dormitory', 'other', 6, 'child_node', 135),
(45, 'Add New Class', 'other', 12, 'root_node', 45),
(46, 'Update Class', 'other', 12, 'child_node', 50),
(47, 'Set Current TOD', 'other', 17, 'child_node', 24),
(48, 'View TOD Report', 'other', 17, 'root_child_node', 72),
(49, 'Add Comment TOD Report', 'other', 17, 'child_node', 48),
(50, 'View Classes', 'other', 12, 'root_node', 50),
(51, 'Remove Class', 'other', 12, 'child_node', 50),
(52, 'Remove Stream', 'other', 12, 'child_node', 55),
(53, 'Add New Stream', 'other', 12, 'child_node', 55),
(54, 'Update Stream', 'other', 12, 'child_node', 55),
(55, 'View Streams', 'other', 12, 'root_node', 55),
(56, 'Remove A''Level Grade System', 'other', 8, 'child_node', 35),
(57, 'Remove O''Level Grade System', 'other', 8, 'child_node', 34),
(58, 'View Exam Types', 'other', 8, 'root_node', 58),
(59, 'Update Exam Type', 'other', 8, 'child_node', 58),
(61, 'Enable/Disable Exam Types', 'other', 8, 'child_node', 58),
(62, 'Change Own Password', 'all', 19, 'root_node', 62),
(63, 'View Class Attendance', 'other', 17, 'root_node', 63),
(64, 'View School Attendance', 'other', 7, 'root_node', 64),
(65, 'Manage Score & Results Of Assigned Subjects', 'all', 8, 'root_node', 65),
(66, 'Add Score Of Own Subject', 'all', 8, 'child_node', 65),
(67, 'Add Selected Student', 'other', 4, 'child_node', 83),
(68, 'View Own Department Classes', 'hods', 20, 'root_node', 68),
(69, 'Assign Teacher To Subject', 'hods', 20, 'child_node', 100),
(70, 'Remove Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(71, 'Explore School Attendance', 'other', 7, 'child_node', 64),
(72, 'TOD Routine', 'other', 17, 'root_node', 72),
(73, 'Add New Timetable', 'other', 13, 'child_node', 80),
(74, 'Deactivate Staff Account', 'other', 3, 'child_node', 175),
(75, 'View Staffs', 'other', 3, 'root_node', 75),
(77, 'Add Own Qualification', 'all', 19, 'root_node', 77),
(78, 'View Own Profile', 'all', 19, 'root_node', 78),
(79, 'Update Timetable', 'other', 13, 'child_node', 80),
(80, 'View School Timetable', 'other', 13, 'root_node', 80),
(81, 'Update Own Profile', 'all', 19, 'child_node', 78),
(82, 'Update Score Of Own Subject', 'all', 8, 'child_node', 65),
(83, 'View Selected Students', 'other', 4, 'root_node', 83),
(84, 'View Transferred IN Students', 'other', 4, 'root_node', 84),
(85, 'View Transferred OUT Students', 'other', 4, 'root_node', 85),
(86, 'View Suspended Students', 'other', 4, 'root_node', 86),
(87, 'View Unreported Students', 'other', 4, 'root_node', 87),
(88, 'View Enrolled Students', 'other', 4, 'root_node', 88),
(89, 'View Disabled Students', 'other', 4, 'root_node', 89),
(90, 'Suspend Student', 'other', 4, 'child_node', 5),
(91, 'View Class Streams', 'other', 12, 'root_node', 91),
(92, 'View Students In Dormitory', 'other', 6, 'root_child_node', 43),
(93, 'Transfer Student', 'other', 4, 'child_node', 5),
(95, 'Manage Groups', 'other', 29, 'root_node', 95),
(99, 'View Staffs Details', 'other', 3, 'root_child_node', 175),
(100, 'View Assinged Teachers In Own Department', 'hods', 20, 'root_node', 100),
(101, 'Update Teaching Assignment In Own Department', 'hods', 20, 'child_node', 100),
(103, 'View Teaching Log In Own Department', 'hods', 20, 'child_node', 68),
(104, 'View Activity Log', 'other', 29, 'root_node', 104),
(105, 'View Staff Activity Log', 'other', 3, 'child_node', 99),
(106, 'View Audit Trail', 'other', 29, 'root_node', 106),
(107, 'Edit Student Results', 'other', 16, 'child_node', 179),
(108, 'Add Completion Details', 'other', 16, 'child_node', 174),
(109, 'View Own Class Results', 'class_teachers', 24, 'root_node', 109),
(111, 'Add Class Attedance', 'class_teachers', 22, 'child_node', 112),
(112, 'View Own Class Attedance', 'class_teachers', 22, 'root_node', 112),
(113, 'View Students In Own Class', 'class_teachers', 22, 'root_node', 113),
(114, 'View Assigned Teachers In Own Class', 'class_teachers', 22, 'root_node', 114),
(115, 'View Own Class Timetable', 'class_teachers', 22, 'root_node', 115),
(116, 'Add Class Journal Record', 'class_teachers', 22, 'child_node', 118),
(117, 'Update Class Journal', 'class_teachers', 22, 'child_node', 118),
(118, 'View Own Class Journal Report', 'class_teachers', 22, 'root_node', 118),
(119, 'Add Class Monitor', 'class_teachers', 22, 'child_node', 113),
(120, 'View Students In Own Dormitory', 'dormitory_masters', 21, 'root_node', 120),
(121, 'Add Dormitory Leader', 'dormitory_masters', 21, 'child_node', 120),
(122, 'View Assets In Own Dormitory', 'dormitory_masters', 21, 'root_node', 122),
(125, 'Update Student', 'other', 4, 'child_node', 5),
(126, 'Post New Announcement', 'all', 14, 'child_node', 127),
(127, 'View Own Posted Announcements', 'all', 14, 'root_node', 127),
(128, 'View Announcements', 'all', 14, 'root_node', 128),
(129, 'Update Announcement', 'all', 14, 'child_node', 127),
(131, 'View Own Roles', 'all', 25, 'root_node', 131),
(132, 'View Assigned Classes', 'all', 15, 'root_node', 132),
(133, 'Add Dormitory Master', 'other', 6, 'child_node', 43),
(134, 'View Roll Call', 'other', 6, 'child_node', 43),
(135, 'View Assets In Dormitory', 'other', 6, 'root_child_node', 43),
(137, 'Reset Password', 'other', 3, 'root_node', 137),
(138, 'Add Staff Attendance', 'other', 3, 'child_node', 165),
(139, 'Update Class Stream', 'other', 12, 'child_node', 91),
(140, 'Add New Class Stream', 'other', 12, 'child_node', 91),
(141, 'Remove Class Stream', 'other', 12, 'child_node', 91),
(142, 'Add Class Stream Subject', 'other', 27, 'child_node', 40),
(143, 'Update Class Stream Subject', 'other', 27, 'child_node', 40),
(144, 'Remove Class Stream Subject', 'other', 27, 'child_node', 40),
(146, 'Add Roll Call', 'dormitory_masters', 21, 'child_node', 147),
(147, 'View Roll Call In Own Dormitory', 'dormitory_masters', 21, 'root_node', 147),
(148, 'Remove Asset From Dormitory', 'other', 6, 'child_node', 135),
(149, 'Update Asset In Dormitory', 'other', 6, 'child_node', 135),
(150, 'Add Student To Dormitory', 'other', 6, 'child_node', 92),
(151, 'Edit Period', 'other', 7, 'child_node', 184),
(154, 'Manage Roles', 'other', 25, 'root_node', 154),
(155, 'Add Division', 'other', 8, 'child_node', 158),
(156, 'Update Division', 'other', 8, 'child_node', 158),
(157, 'Remove Division', 'other', 8, 'child_node', 158),
(158, 'View Divisions', 'other', 8, 'root_node', 158),
(161, 'Restore Suspended Student', 'other', 4, 'child_node', 86),
(162, 'View Student Profile', 'all', 4, '', 5),
(163, 'Mark Student As Reported', 'other', 4, 'child_node', 83),
(164, 'Mark Student As Unreported', 'other', 4, 'child_node', 83),
(165, 'View Staff Attendance', 'other', 3, 'root_node', 165),
(169, 'View Staff Qualification', 'other', 3, 'child_node', 99),
(170, 'View Students In Own Department', 'hods', 20, 'child_node', 68),
(172, 'View Personal Timetable', 'all', 13, 'root_node', 172),
(174, 'View All Completed Students', 'other', 16, 'root_node', 174),
(175, 'Manage Staff Accounts', 'other', 3, 'root_node', 175),
(176, 'View Own Log', 'all', 15, 'child_node', 189),
(177, 'Add Teaching Log', 'all', 15, 'child_node', 189),
(179, 'View School Results', 'other', 24, 'root_node', 179),
(180, 'Add Indiscipline Record', 'other', 18, 'child_node', 181),
(181, 'View Indiscipline Records', 'other', 18, 'root_node', 181),
(182, 'View All Teachers', 'other', 7, 'root_node', 182),
(183, 'Add New Period', 'other', 7, 'child_node', 184),
(184, 'View Periods', 'other', 7, 'root_node', 184),
(185, 'Perform Database Backup', 'all', 29, 'root_node', 185),
(186, 'Change Avatar', 'all', 19, 'child_node', 78),
(188, 'Erase Timetable', 'other', 13, 'child_node', 80),
(189, 'View Topics', 'other', 15, 'root_child_node', 132),
(190, 'Add Topic', 'other', 15, 'child_node', 189),
(191, 'Update Topic', 'other', 15, 'child_node', 189),
(192, 'Remove Topic', 'other', 15, 'child_node', 189),
(193, 'View Subtopics', 'other', 15, 'root_child_node', 189),
(194, 'Add Subtopic', 'other', 15, 'child_node', 193),
(195, 'Update Subtopic', 'other', 15, 'child_node', 193),
(196, 'Remove Subtopic', 'other', 15, 'child_node', 193),
(197, 'Assign Class Teacher', 'other', 12, 'child_node', 91),
(198, 'View Students In Class', 'other', 12, 'root_child_node', 91),
(199, 'View Notifications', 'all', 30, 'root_node', 199),
(200, 'Transfer Student To Another Dormitory', 'other', 6, 'child_node', 92),
(201, 'View Class Journal Report', 'other', 12, 'child_node', 91),
(202, 'Add Staff Qualification', 'other', 3, 'child_node', 99),
(205, 'Assign Option Subject', 'other', 4, 'child_node', 5),
(206, 'Shift Student To Another Stream', 'other', 12, 'child_node', 198),
(207, 'Add Subjects To Teacher', 'other', 7, 'child_node', 182),
(208, 'View Subjects Teachers', 'other', 27, 'child_node', 39),
(209, 'Edit Selected Students', 'other', 4, 'child_node', 83),
(210, 'Update Staff Attendance', 'all', 3, 'child_node', 165),
(211, 'Upload Student''s Photo', 'all', 4, 'child_node', 162),
(212, 'Add Asset In Class', 'other', 12, 'child_node', 91),
(213, 'Update Asset In Class', 'other', 12, 'child_node', 91),
(214, 'Remove Asset From Class', 'other', 12, 'child_node', 91),
(215, 'View Assets In Class', 'other', 12, 'child_node', 91);

-- --------------------------------------------------------

--
-- Table structure for table `promote_student_audit_table`
--

CREATE TABLE IF NOT EXISTS `promote_student_audit_table` (
`id` int(4) NOT NULL,
  `promotion_level` varchar(10) NOT NULL,
  `promoted_by` varchar(10) NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `promote_student_audit_table`
--
DELIMITER //
CREATE TRIGGER `promote_students_trigger` BEFORE INSERT ON `promote_student_audit_table`
 FOR EACH ROW BEGIN 
DECLARE role VARCHAR(20); 
SELECT user_role FROM admin WHERE id=1 INTO role; 
IF(role = "admin") THEN 
CALL promote_class_stream_id_procedure_new(NEW.promotion_level); 
CALL promote_class_id_procedure_new(NEW.promotion_level);  
CALL promote_academic_year_procedure(NEW.promotion_level); 
CALL copy_to_students_enrollment_procedure(NEW.promotion_level); 
ELSE 
SIGNAL SQLSTATE '45000' set message_text = "You do not have the privilege to promote students"; 
END IF; 
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE IF NOT EXISTS `qualification` (
  `certification` varchar(30) NOT NULL,
  `description` mediumtext,
  `university` varchar(100) NOT NULL,
  `completion_date` year(4) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
`q_id` int(11) NOT NULL,
  `certified` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
`region_id` int(2) NOT NULL,
  `region_name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`region_id`, `region_name`) VALUES
(1, 'Arusha'),
(2, 'Dar-es-salaam'),
(3, 'Dodoma'),
(4, 'Geita'),
(5, 'Iringa'),
(6, 'Kagera'),
(7, 'Katavi'),
(8, 'Kigoma'),
(9, 'Kilimanjaro'),
(10, 'Manyara'),
(11, 'Mara'),
(12, 'Mbeya'),
(13, 'Morogoro'),
(14, 'Mtwara'),
(15, 'Mwanza'),
(16, 'Pwani'),
(17, 'Rukwa'),
(18, 'Ruvuma'),
(19, 'Shinyanga'),
(20, 'Simiyu'),
(21, 'Singida'),
(22, 'Tabora'),
(23, 'Tanga'),
(24, 'Tunduma');

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE IF NOT EXISTS `religion` (
`religion_id` int(5) NOT NULL,
  `religion_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`religion_id`, `religion_name`) VALUES
(1, 'Christian'),
(2, 'Muslim');

-- --------------------------------------------------------

--
-- Stand-in structure for view `remained_book_view`
--
CREATE TABLE IF NOT EXISTS `remained_book_view` (
`isbn` varchar(17)
,`ID` varchar(50)
,`LAST_EDIT` date
,`STATUS` enum('normal','lost','bad')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `reminder`
--
CREATE TABLE IF NOT EXISTS `reminder` (
`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`ID` varchar(50)
,`ISBN` varchar(17)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `remove_borrowed_view`
--
CREATE TABLE IF NOT EXISTS `remove_borrowed_view` (
`ID` varchar(50)
,`ISBN` varchar(17)
,`STATUS` enum('normal','lost','bad')
,`LAST_EDIT` date
,`AUTHOR_NAME` varchar(100)
,`BOOK_TITLE` varchar(100)
,`EDITION` varchar(15)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `returned_book_view`
--
CREATE TABLE IF NOT EXISTS `returned_book_view` (
`ISBN` varchar(17)
,`ID` varchar(50)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`LIBRARIAN_ID` varchar(10)
,`DATE_BORROWED` date
,`EXPECT_RETURN` date
,`RETURN_DATE` date
,`STATUS_AFTER` enum('good','bad')
,`BORROWER_ID` varchar(11)
,`BORROWER_DESCRIPTION` varchar(61)
,`BORROWER_TYPE` varchar(100)
,`FLAG` enum('borrowed','lost','returned')
);
-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE IF NOT EXISTS `role_module` (
  `role_type_id` int(2) NOT NULL,
  `module_id` int(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`role_type_id`, `module_id`) VALUES
(5, 3),
(10, 4),
(11, 4),
(9, 7),
(3, 8),
(4, 8),
(10, 8),
(3, 13),
(4, 13),
(10, 13),
(4, 14),
(10, 14),
(3, 15),
(4, 15),
(10, 15),
(4, 19),
(10, 19),
(3, 20),
(5, 21),
(4, 22),
(4, 24),
(4, 25),
(10, 25),
(3, 30),
(4, 30),
(10, 30);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
  `role_type_id` int(2) NOT NULL,
  `permission_id` int(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_type_id`, `permission_id`) VALUES
(11, 4),
(10, 5),
(11, 5),
(3, 42),
(4, 62),
(10, 62),
(9, 64),
(3, 65),
(4, 65),
(10, 65),
(3, 66),
(4, 66),
(10, 66),
(3, 68),
(3, 69),
(9, 71),
(5, 75),
(4, 77),
(10, 77),
(4, 78),
(10, 78),
(10, 80),
(4, 81),
(10, 81),
(4, 82),
(10, 82),
(3, 100),
(3, 101),
(3, 103),
(4, 109),
(4, 111),
(4, 112),
(4, 113),
(4, 114),
(4, 115),
(4, 116),
(4, 117),
(4, 118),
(4, 119),
(5, 120),
(5, 121),
(5, 122),
(4, 127),
(10, 127),
(4, 128),
(10, 128),
(4, 131),
(10, 131),
(3, 132),
(4, 132),
(10, 132),
(5, 146),
(5, 147),
(3, 170),
(3, 172),
(4, 172),
(10, 172),
(3, 177),
(3, 189),
(10, 189),
(10, 190),
(10, 193),
(10, 194),
(3, 199),
(4, 199),
(10, 199);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission_dummy_table`
--

CREATE TABLE IF NOT EXISTS `role_permission_dummy_table` (
  `role_type_id` int(2) DEFAULT NULL,
  `permission_id` int(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission_dummy_table`
--

INSERT INTO `role_permission_dummy_table` (`role_type_id`, `permission_id`) VALUES
(9, 64),
(9, 71),
(4, 66),
(4, 65),
(4, 82),
(4, 172),
(4, 127),
(4, 128),
(4, 132),
(4, 81),
(4, 78),
(4, 77),
(4, 62),
(4, 131),
(4, 199),
(4, 119),
(4, 118),
(4, 117),
(4, 116),
(4, 115),
(4, 114),
(4, 113),
(4, 112),
(4, 111),
(4, 109),
(11, 4),
(11, 5),
(10, 66),
(10, 65),
(10, 82),
(10, 172),
(10, 127),
(10, 128),
(10, 132),
(10, 81),
(10, 78),
(10, 77),
(10, 62),
(10, 131),
(10, 199),
(10, 5),
(10, 80),
(10, 189),
(10, 190),
(10, 193),
(10, 194),
(5, 75),
(5, 120),
(5, 121),
(5, 122),
(5, 146),
(5, 147),
(3, 65),
(3, 66),
(3, 172),
(3, 177),
(3, 132),
(3, 199),
(3, 189),
(3, 42),
(3, 68),
(3, 69),
(3, 100),
(3, 101),
(3, 103),
(3, 170);

--
-- Triggers `role_permission_dummy_table`
--
DELIMITER //
CREATE TRIGGER `before_insert_in_role_permission_trigger` BEFORE INSERT ON `role_permission_dummy_table`
 FOR EACH ROW BEGIN 
DECLARE rnid INT DEFAULT 0;
DECLARE p_type VARCHAR(30);
DECLARE rnid2 INT DEFAULT 0;

SELECT root_node_id FROM permission WHERE permission_id=NEW.permission_id INTO rnid; 
SELECT permission_type FROM permission WHERE permission_id=NEW.permission_id INTO p_type;

IF p_type = "child_node" THEN 
REPLACE INTO role_permission VALUES(NEW.role_type_id, NEW.permission_id);
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid);

SELECT root_node_id FROM permission WHERE permission_id=rnid INTO rnid2; 
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid2);
ELSE
REPLACE INTO role_permission VALUES(NEW.role_type_id, NEW.permission_id);
REPLACE INTO role_permission VALUES(NEW.role_type_id, rnid);
END IF;

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role_type`
--

CREATE TABLE IF NOT EXISTS `role_type` (
`role_type_id` int(2) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  `description` mediumtext NOT NULL,
  `active` enum('on','off') DEFAULT 'on'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_type`
--

INSERT INTO `role_type` (`role_type_id`, `role_name`, `description`, `active`) VALUES
(3, 'Head Of Department', 'Responsible for department', 'on'),
(4, 'Class Teacher', 'Responsible for class', 'on'),
(5, 'Dormitory Master', 'Responsible for dormitory', 'on'),
(9, 'Teacher On Duty', 'Resposible for all activities this week', 'on'),
(10, 'Teacher', 'Responsible for teaching', 'on'),
(11, 'Registrer', 'registering new students', 'on');

--
-- Triggers `role_type`
--
DELIMITER //
CREATE TRIGGER `before_delete_role_type` BEFORE DELETE ON `role_type`
 FOR EACH ROW BEGIN IF (OLD.role_type_id = 3 || OLD.role_type_id = 4 || OLD.role_type_id = 5 || OLD.role_type_id = 9) THEN SIGNAL SQLSTATE '45000' SET message_text="Permission Denied. Cannot delete this role"; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `secretary`
--

CREATE TABLE IF NOT EXISTS `secretary` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `selected_students`
--

CREATE TABLE IF NOT EXISTS `selected_students` (
  `examination_no` varchar(20) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date NOT NULL,
  `former_school` varchar(255) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `ward` varchar(30) NOT NULL,
  `orphan` enum('yes','no') DEFAULT NULL,
  `g_firstname` varchar(30) DEFAULT NULL,
  `g_lastname` varchar(30) DEFAULT NULL,
  `disability` enum('yes','no') NOT NULL DEFAULT 'no',
  `region` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `tel_no` varchar(15) NOT NULL,
  `box` varchar(10) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `form` enum('I','V') NOT NULL,
  `reported` enum('yes','no') NOT NULL DEFAULT 'no',
  `academic_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selected_students`
--

INSERT INTO `selected_students` (`examination_no`, `firstname`, `lastname`, `dob`, `former_school`, `gender`, `nationality`, `ward`, `orphan`, `g_firstname`, `g_lastname`, `disability`, `region`, `district`, `tel_no`, `box`, `email`, `form`, `reported`, `academic_year`) VALUES
('S123-123', 'Haruna', 'Bakaru', '2018-05-08', 'Nkurumahia Secondary School', 'Male', 'Tanzanian', 'Ilemela', 'no', 'Grace', 'Makei', 'no', 'Kilimanjaro', 'Ilemela', '+255717808090', '123', '', 'V', 'no', 3);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date NOT NULL,
  `marital_status` enum('Married','Single','Divorced','Widowed') NOT NULL,
  `gender` varchar(6) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone_no` varchar(13) NOT NULL,
  `staff_image` varchar(255) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `staff_type` char(1) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `user_role` varchar(30) NOT NULL,
  `last_log` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `b_type` enum('STAFF') DEFAULT 'STAFF'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `staff`
--
DELIMITER //
CREATE TRIGGER `before_update_on_staff_password_trigger` BEFORE UPDATE ON `staff`
 FOR EACH ROW BEGIN 
IF (NEW.password = (SELECT password FROM password_reset_tbl) && @user_id = (SELECT id FROM admin)) THEN 
REPLACE INTO audit_password_reset_tbl (staff_id, reset_by, reset_to, reset_time) VALUES(NEW.staff_id, @user_id, (SELECT password FROM password_reset_tbl), NOW()); REPLACE INTO notification (staff_id, msg_id, msg, time_) VALUES(NEW.staff_id, 8, "Your password has been reset, make sure you change the default password", NOW()); 
END IF;  
IF NEW.password <> 'a96af5cb3ae3b0e034f1c597133307de25068490' THEN UPDATE audit_password_reset_tbl SET user_changed="T", reset_to=NEW.password WHERE staff_id=NEW.staff_id; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_activity_log_view`
--
CREATE TABLE IF NOT EXISTS `staff_activity_log_view` (
`id` bigint(20)
,`activity` varchar(50)
,`tableName` varchar(50)
,`time` timestamp
,`source` varchar(30)
,`destination` varchar(10)
,`user_id` varchar(10)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`user_role` varchar(30)
,`last_log` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table `staff_attendance_record`
--

CREATE TABLE IF NOT EXISTS `staff_attendance_record` (
`id` int(11) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `att_id` int(2) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `date_` date NOT NULL,
  `reason` mediumtext,
  `aid` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Triggers `staff_attendance_record`
--
DELIMITER //
CREATE TRIGGER `after_insert_staff_att_trigger` AFTER INSERT ON `staff_attendance_record`
 FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM staff_attendance_record WHERE staff_id=NEW.staff_id AND date_=NEW.date_ AND aid=NEW.aid INTO att_record_id; INSERT INTO audit_staff_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "ADD", @user_id, @ip_address, NOW(), NEW.att_id); END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `after_update_staff_att_trigger` AFTER UPDATE ON `staff_attendance_record`
 FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM staff_attendance_record WHERE staff_id=NEW.staff_id AND date_=NEW.date_ AND aid=NEW.aid INTO att_record_id; INSERT INTO audit_staff_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.att_id); END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_staff_attendance` BEFORE DELETE ON `staff_attendance_record`
 FOR EACH ROW BEGIN END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_attendance_view`
--
CREATE TABLE IF NOT EXISTS `staff_attendance_view` (
`staff_id` varchar(10)
,`names` varchar(61)
,`gender` varchar(6)
,`att_type` char(1)
,`description` varchar(50)
,`day` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`date_` date
);
-- --------------------------------------------------------

--
-- Table structure for table `staff_department`
--

CREATE TABLE IF NOT EXISTS `staff_department` (
  `staff_id` varchar(10) NOT NULL,
  `dept_id` int(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `staff_department_dummy_table`
--

CREATE TABLE IF NOT EXISTS `staff_department_dummy_table` (
  `staff_id` varchar(10) NOT NULL,
  `dept_id` int(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_department_dummy_table`
--

INSERT INTO `staff_department_dummy_table` (`staff_id`, `dept_id`) VALUES
('STAFF0001', 1),
('STAFF0002', 4),
('STAFF0003', 3),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0005', 3),
('STAFF0001', 1),
('STAFF0008', 1),
('STAFF0008', 4),
('STAFF0008', 3),
('STAFF1111', 4),
('STAFF2222', 1),
('STAFF0008', 6),
('STAFF0008', 6),
('STAFF0008', 5),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0005', 7),
('STAFF0005', 3),
('STAFF0003', 6),
('STAFF0003', 3),
('STAFF0003', 3),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0002', 4),
('STAFF0002', 1),
('STAFF0002', 4),
('STAFF0002', 1),
('STAFF1111', 1),
('STAFF1111', 4),
('STAFF1111', 1),
('STAFF1111', 4),
('STAFF0003', 3),
('STAFF0003', 3),
('STAFF0003', 7),
('STAFF0005', 4),
('STAFF0005', 5),
('STAFF0005', 4),
('STAFF0005', 1),
('STAFF0004', 1),
('STAFF0004', 4),
('STAFF0002', 9);

--
-- Triggers `staff_department_dummy_table`
--
DELIMITER //
CREATE TRIGGER `before_insert_on_staff_department_trigger` BEFORE INSERT ON `staff_department_dummy_table`
 FOR EACH ROW BEGIN 
REPLACE INTO staff_department VALUES(NEW.staff_id, NEW.dept_id);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_group`
--

CREATE TABLE IF NOT EXISTS `staff_group` (
  `staff_id` varchar(10) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_group_members_view`
--
CREATE TABLE IF NOT EXISTS `staff_group_members_view` (
`group_id` int(11)
,`group_name` varchar(100)
,`group_members` text
);
-- --------------------------------------------------------

--
-- Table structure for table `staff_nok`
--

CREATE TABLE IF NOT EXISTS `staff_nok` (
  `staff_id` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `relation_type` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone_no` varchar(13) NOT NULL,
  `p_box` int(10) DEFAULT NULL,
  `p_region` varchar(30) DEFAULT NULL,
  `current_box` int(10) DEFAULT NULL,
  `current_region` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_plus_nok_view`
--
CREATE TABLE IF NOT EXISTS `staff_plus_nok_view` (
`user_role` varchar(30)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`last_log` timestamp
,`nok_fn` varchar(30)
,`nok_ln` varchar(30)
,`nok_gender` varchar(6)
,`relation_type` varchar(30)
,`nok_email` varchar(50)
,`nok_phone_no` varchar(13)
,`nok_box` int(10)
,`nok_region` varchar(30)
,`current_box` int(10)
,`current_region` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_qualification_view`
--
CREATE TABLE IF NOT EXISTS `staff_qualification_view` (
`q_id` int(11)
,`certified` enum('yes','no')
,`user_role` varchar(30)
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`status` enum('active','inactive')
,`last_log` timestamp
,`certification` varchar(30)
,`completion_date` year(4)
,`description` mediumtext
,`university` varchar(100)
,`nok_fn` varchar(30)
,`nok_ln` varchar(30)
,`nok_gender` varchar(6)
,`relation_type` varchar(30)
,`nok_email` varchar(50)
,`nok_phone_no` varchar(13)
,`nok_box` int(10)
,`nok_region` varchar(30)
,`current_box` int(10)
,`current_region` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `staff_role`
--

CREATE TABLE IF NOT EXISTS `staff_role` (
  `role_type_id` int(2) NOT NULL,
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_role_permission_view`
--
CREATE TABLE IF NOT EXISTS `staff_role_permission_view` (
`staff_id` varchar(10)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`phone_no` varchar(13)
,`status` enum('active','inactive')
,`permission_id` int(5) unsigned
,`description` varchar(255)
,`role_type_id` int(2)
,`role_name` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `stream`
--

CREATE TABLE IF NOT EXISTS `stream` (
`stream_id` int(2) NOT NULL,
  `stream_name` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stream`
--

INSERT INTO `stream` (`stream_id`, `stream_name`) VALUES
(1, 'A'),
(3, 'B'),
(4, 'C'),
(11, 'E1'),
(9, 'PCB'),
(12, 'PGM');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `admission_no` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `academic_year` int(5) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `class_id` varchar(10) DEFAULT NULL,
  `class_stream_id` varchar(10) DEFAULT NULL,
  `dob` date NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `tribe_id` int(5) NOT NULL,
  `religion_id` int(5) NOT NULL,
  `home_address` varchar(50) DEFAULT NULL,
  `region` varchar(50) NOT NULL,
  `former_school` varchar(100) NOT NULL,
  `dorm_id` int(3) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `stte` enum('selected','transferred_in') NOT NULL DEFAULT 'selected',
  `stte_out` enum('transferred_out','expelled','completed','suspended','') DEFAULT NULL,
  `disabled` enum('yes','no') NOT NULL DEFAULT 'no',
  `disability` enum('Deaf','Mute','Blind','Physical Disability') DEFAULT NULL,
  `b_type` enum('STUDENT') DEFAULT 'STUDENT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `student`
--
DELIMITER //
CREATE TRIGGER `before_insert_into_student_trigger` BEFORE INSERT ON `student`
 FOR EACH ROW BEGIN DECLARE a INT DEFAULT 0;  DECLARE lvl VARCHAR(10); DECLARE nos INT DEFAULT 0;
SELECT COUNT(*) FROM class_stream_subject WHERE class_stream_id=NEW.class_stream_id INTO a;
SELECT number_of_subjects FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO nos;

IF (a != 0 && a = nos) THEN
INSERT INTO audit_register_student_tbl (admission_no, change_by, class_stream_id, change_type, time, source) VALUES(NEW.admission_no, @user_id, NEW.class_stream_id, "NEW", NOW(), @ip_address);
ELSE
SIGNAL SQLSTATE '45000' set message_text = "This class does not have any subjects at the moment";  
END IF; 
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `insert_student_subject_trigger` AFTER INSERT ON `student`
 FOR EACH ROW BEGIN INSERT INTO enrollment (admission_no, class_id, class_stream_id, academic_year) VALUES(NEW.admission_no, NEW.class_id, NEW.class_stream_id, (SELECT id FROM academic_year where class_level=(SELECT level FROM class_level WHERE class_id=NEW.class_id) AND status="current_academic_year")); CALL select_class_stream_subjects(NEW.admission_no, NEW.class_stream_id); INSERT INTO accomodation_history(admission_no, dorm_id, start_date) VALUES(NEW.admission_no, NEW.dorm_id, NOW()); END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `students_by_class_and_subject_view`
--
CREATE TABLE IF NOT EXISTS `students_by_class_and_subject_view` (
`level` enum('O''Level','A''Level')
,`admission_no` varchar(10)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`names` varchar(41)
,`class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `students_in_dormitory_view`
--
CREATE TABLE IF NOT EXISTS `students_in_dormitory_view` (
`class_name` varchar(30)
,`stream` varchar(5)
,`admission_no` varchar(10)
,`names` varchar(41)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`status` enum('active','inactive')
,`dorm_id` int(3)
);
-- --------------------------------------------------------

--
-- Table structure for table `student_assessment`
--

CREATE TABLE IF NOT EXISTS `student_assessment` (
  `admission_no` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `average` float(5,2) DEFAULT '0.00',
  `grade` varchar(3) DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `class_rank` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance_record`
--

CREATE TABLE IF NOT EXISTS `student_attendance_record` (
`id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `att_id` int(2) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `date_` date NOT NULL,
  `month` enum('January','February','March','April','May','June','July','August','September','October','November','December') DEFAULT NULL,
  `term_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `student_attendance_record`
--
DELIMITER //
CREATE TRIGGER `after_insert_student_attendance` AFTER INSERT ON `student_attendance_record`
 FOR EACH ROW BEGIN
DECLARE att_record_id INT(2) DEFAULT 0;
SELECT id FROM student_attendance_record WHERE admission_no=NEW.admission_no AND date_=NEW.date_ AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO att_record_id;
INSERT INTO audit_student_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "ADD", @user_id, @ip_address, NOW(), NEW.att_id);
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `after_update_student_attendance` AFTER UPDATE ON `student_attendance_record`
 FOR EACH ROW BEGIN DECLARE att_record_id INT(2) DEFAULT 0; SELECT id FROM student_attendance_record WHERE admission_no=NEW.admission_no AND date_=NEW.date_ AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO att_record_id; INSERT INTO audit_student_attendance (sar_id, change_type, change_by, ip_address, change_time, att_id) VALUES(att_record_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.att_id); END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_student_attendance` BEFORE DELETE ON `student_attendance_record`
 FOR EACH ROW BEGIN  END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_enrollment_academic_year_view`
--
CREATE TABLE IF NOT EXISTS `student_enrollment_academic_year_view` (
`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`is_current` enum('yes','no')
,`aid` int(5)
,`id` int(5)
,`start_date` date
,`end_date` date
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`status` enum('current_academic_year','past_academic_year')
,`admission_no` varchar(10)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`academic_year` int(5)
,`class_name` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `student_guardian_view`
--
CREATE TABLE IF NOT EXISTS `student_guardian_view` (
`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`disabled` enum('yes','no')
,`disability` enum('Deaf','Mute','Blind','Physical Disability')
,`stte` enum('selected','transferred_in')
,`tribe_id` int(5)
,`religion_id` int(5)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`student_firstname` varchar(20)
,`student_lastname` varchar(20)
,`g_firstname` varchar(30)
,`g_lastname` varchar(30)
,`g_middlename` varchar(30)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`date` date
,`dob` date
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`status` enum('active','inactive')
,`guardian_names` varchar(92)
,`gender` enum('Male','Female')
,`rel_type` enum('Father','Mother','Guardian')
,`email` varchar(50)
,`occupation` varchar(50)
,`phone_no` varchar(30)
,`p_box` int(5)
,`p_region` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `student_progressive_results_view`
--
CREATE TABLE IF NOT EXISTS `student_progressive_results_view` (
`subject_rank` int(3)
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`terminal` float(5,2)
,`average` float(5,2)
,`grade` varchar(3)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `student_result_view`
--
CREATE TABLE IF NOT EXISTS `student_result_view` (
`subject_rank` int(3)
,`year` varchar(10)
,`class_level` enum('A''Level','O''Level')
,`term_id` int(11)
,`term_name` enum('first_term','second_term')
,`admission_no` varchar(10)
,`monthly_one` float(5,2)
,`midterm` float(5,2)
,`monthly_two` float(5,2)
,`terminal` float(5,2)
,`average` float(5,2)
,`grade` varchar(3)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`academic_year` int(5)
,`aid` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Table structure for table `student_roll_call_record`
--

CREATE TABLE IF NOT EXISTS `student_roll_call_record` (
`id` int(11) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `dorm_id` int(3) NOT NULL,
  `att_id` int(2) NOT NULL,
  `date_` date NOT NULL,
  `month` enum('January','February','March','April','May','June','July','August','September','October','November','December') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE IF NOT EXISTS `student_subject` (
  `admission_no` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_subject_assessment`
--

CREATE TABLE IF NOT EXISTS `student_subject_assessment` (
`ssa_id` bigint(20) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `etype_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `e_date` date NOT NULL,
  `marks` float(5,2) NOT NULL DEFAULT '0.00',
  `term_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `student_subject_assessment`
--
DELIMITER //
CREATE TRIGGER `after_insert_on_student_subject_assessment_trigger` AFTER INSERT ON `student_subject_assessment`
 FOR EACH ROW BEGIN DECLARE check_exist INT DEFAULT 0;
DECLARE darasa_id VARCHAR(10);
DECLARE exam_type VARCHAR(10);
DECLARE avg FLOAT(5, 2);
DECLARE overall_avg FLOAT(5, 2);
DECLARE grd VARCHAR(2);
DECLARE overall_grade VARCHAR(2);
DECLARE lvl VARCHAR(10);
DECLARE e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE sum_non_e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE get_ssa_id BIGINT(20) DEFAULT 0;

SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id INTO check_exist;

SELECT SUM(marks) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id INTO e4_marks;
SELECT SUM(marks)/COUNT(etype_id) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO sum_non_e4_marks;
IF e4_marks IS NULL THEN
SET e4_marks = 0;
END IF;
IF sum_non_e4_marks IS NULL THEN
SET sum_non_e4_marks = 0;
END IF;

IF check_exist = 0 THEN
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;

SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF NEW.etype_id = "E01" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_one, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E02" THEN
INSERT INTO student_subject_score_position (admission_no, midterm, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E03" THEN
INSERT INTO student_subject_score_position (admission_no, monthly_two, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
IF NEW.etype_id = "E04" THEN
INSERT INTO student_subject_score_position (admission_no, terminal, subject_id, class_id, class_stream_id, term_id, average, grade) VALUES (NEW.admission_no, NEW.marks, NEW.subject_id, darasa_id, NEW.class_stream_id, NEW.term_id, avg, grd);
END IF;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
REPLACE INTO student_assessment (admission_no, class_id, class_stream_id, average, grade, term_id) VALUES (NEW.admission_no, darasa_id, NEW.class_stream_id, overall_avg, overall_grade, NEW.term_id);
END IF;
IF check_exist > 0 THEN
IF NEW.etype_id = "E01" THEN
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E02" THEN
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E03" THEN
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E04" THEN
UPDATE student_subject_score_position SET terminal=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
CALL calculate_subject_rank_procedure(NEW.subject_id, NEW.term_id, NEW.class_stream_id);
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

SELECT ssa_id FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id=NEW.etype_id AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO get_ssa_id;
INSERT INTO audit_student_subject_assessment (ssa_id, change_type, change_by, ip_address, change_time, marks) VALUES(get_ssa_id, "ADD", @user_id, @ip_address, NOW(), NEW.marks);
END IF;
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `after_update_on_student_subject_assessment_trigger` AFTER UPDATE ON `student_subject_assessment`
 FOR EACH ROW BEGIN DECLARE check_exist INT DEFAULT 0;
DECLARE darasa_id VARCHAR(10);
DECLARE exam_type VARCHAR(10);
DECLARE avg FLOAT(5, 2);
DECLARE overall_avg FLOAT(5, 2);
DECLARE grd VARCHAR(2);
DECLARE overall_grade VARCHAR(2);
DECLARE lvl VARCHAR(10);
DECLARE e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE sum_non_e4_marks FLOAT(5, 2) DEFAULT 0;
DECLARE get_ssa_id BIGINT(20) DEFAULT 0;

SELECT COUNT(*) FROM student_subject_score_position WHERE subject_id=NEW.subject_id AND admission_no=NEW.admission_no AND term_id=NEW.term_id INTO check_exist;

SELECT SUM(marks) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id="E04" AND term_id=NEW.term_id INTO e4_marks;
SELECT SUM(marks)/COUNT(etype_id) FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id IN("E01", "E02", "E03") AND term_id=NEW.term_id INTO sum_non_e4_marks;
IF e4_marks IS NULL THEN
SET e4_marks = 0;
END IF;
IF sum_non_e4_marks IS NULL THEN
SET sum_non_e4_marks = 0;
END IF;

IF NEW.etype_id = "E01" THEN
UPDATE student_subject_score_position SET monthly_one=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E02" THEN
UPDATE student_subject_score_position SET midterm=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E03" THEN
UPDATE student_subject_score_position SET monthly_two=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
IF NEW.etype_id = "E04" THEN
UPDATE student_subject_score_position SET terminal=NEW.marks WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
END IF;
SELECT ROUND((e4_marks + sum_non_e4_marks)/2, 2) INTO avg;
SELECT class_id FROM class_stream WHERE class_stream_id=NEW.class_stream_id INTO darasa_id;
SELECT level FROM class_level WHERE class_id=darasa_id INTO lvl;

IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= avg AND end_mark >= avg INTO grd; END IF;
UPDATE student_subject_score_position SET average=avg, grade=grd WHERE subject_id=NEW.subject_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
SELECT ROUND(SUM(average)/COUNT(*), 2) FROM student_subject_score_position WHERE admission_no=NEW.admission_no AND term_id=NEW.term_id INTO overall_avg;
IF lvl = "O'Level" THEN SELECT grade FROM o_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
IF lvl = "A'Level" THEN SELECT grade FROM a_level_grade_system_tbl WHERE start_mark <= overall_avg AND end_mark >= overall_avg INTO overall_grade; END IF;
UPDATE student_assessment SET average=overall_avg, grade=overall_grade WHERE class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id AND admission_no=NEW.admission_no;
CALL calculate_student_rank_procedure(darasa_id, NEW.term_id, NEW.class_stream_id);

SELECT ssa_id FROM student_subject_assessment WHERE admission_no=NEW.admission_no AND subject_id=NEW.subject_id AND etype_id=NEW.etype_id AND class_stream_id=NEW.class_stream_id AND term_id=NEW.term_id INTO get_ssa_id;
INSERT INTO audit_student_subject_assessment (ssa_id, change_type, change_by, ip_address, change_time, marks) VALUES(get_ssa_id, "UPDATE", @user_id, @ip_address, NOW(), NEW.marks);
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_results_trigger` BEFORE DELETE ON `student_subject_assessment`
 FOR EACH ROW BEGIN END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_insert_results_trigger` BEFORE INSERT ON `student_subject_assessment`
 FOR EACH ROW BEGIN DECLARE min_mark FLOAT(5,2) DEFAULT 0.00; DECLARE max_mark FLOAT(5,2) DEFAULT 100.00; IF(NEW.marks < min_mark || NEW.marks > max_mark) THEN SIGNAL SQLSTATE '45000' SET message_text="Error: Marks should NOT be less that 0.00n OR greater than 100"; END IF; END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_results_trigger` BEFORE UPDATE ON `student_subject_assessment`
 FOR EACH ROW BEGIN DECLARE min_mark FLOAT(5,2) DEFAULT 0.00; DECLARE max_mark FLOAT(5,2) DEFAULT 100.00; IF(NEW.marks < min_mark || NEW.marks > max_mark) THEN SIGNAL SQLSTATE '45000' SET message_text="Error: Marks should NOT be less that 0.00n OR greater than 100"; END IF; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `student_subject_score_position`
--

CREATE TABLE IF NOT EXISTS `student_subject_score_position` (
  `admission_no` varchar(10) NOT NULL,
  `monthly_one` float(5,2) DEFAULT '0.00',
  `midterm` float(5,2) DEFAULT '0.00',
  `monthly_two` float(5,2) DEFAULT '0.00',
  `average` float(5,2) DEFAULT '0.00',
  `terminal` float(5,2) DEFAULT '0.00',
  `subject_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `grade` varchar(3) DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `subject_rank` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `student_transfer_in_view`
--
CREATE TABLE IF NOT EXISTS `student_transfer_in_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`stte` enum('selected','transferred_in')
,`status` enum('active','inactive')
,`form` varchar(10)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`school` varchar(255)
,`class_name` varchar(30)
,`date_of_transfer` date
,`transfer_letter` enum('Available','NOT Available')
,`self_form_receipt` enum('Received','NOT Received')
,`transfer_status` enum('IN','OUT')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `student_transfer_out_view`
--
CREATE TABLE IF NOT EXISTS `student_transfer_out_view` (
`firstname` varchar(20)
,`lastname` varchar(20)
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`status` enum('active','inactive')
,`form` varchar(10)
,`admission_no` varchar(10)
,`student_names` varchar(41)
,`school` varchar(255)
,`class_name` varchar(30)
,`date_of_transfer` date
,`transfer_letter` enum('Available','NOT Available')
,`self_form_receipt` enum('Received','NOT Received')
,`transfer_status` enum('IN','OUT')
);
-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `subject_id` varchar(10) NOT NULL,
  `subject_name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `dept_id` int(3) unsigned DEFAULT NULL,
  `subject_category` enum('T','N') NOT NULL,
  `subject_choice` enum('optional','compulsory') NOT NULL DEFAULT 'compulsory',
  `studied_by` enum('O','A','BOTH') NOT NULL,
  `subject_type` enum('Core','Core_With_Penalty','Supplement') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `subjects_subtopic_view`
--
CREATE TABLE IF NOT EXISTS `subjects_subtopic_view` (
`class_stream_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`subtopic_id` int(11)
,`subtopic_name` varchar(255)
,`topic_name` varchar(100)
,`topic_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `subject_department_view`
--
CREATE TABLE IF NOT EXISTS `subject_department_view` (
`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`staff_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`subject_type` enum('Core','Core_With_Penalty','Supplement')
);
-- --------------------------------------------------------

--
-- Table structure for table `subject_teacher`
--

CREATE TABLE IF NOT EXISTS `subject_teacher` (
  `subject_id` varchar(10) NOT NULL,
  `teacher_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `subject_teacher_view`
--
CREATE TABLE IF NOT EXISTS `subject_teacher_view` (
`staff_id` varchar(10)
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`user_role` varchar(30)
,`staff_type` char(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `sub_topic`
--

CREATE TABLE IF NOT EXISTS `sub_topic` (
`subtopic_id` int(11) NOT NULL,
  `subtopic_name` varchar(255) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `super_admin_module`
--

CREATE TABLE IF NOT EXISTS `super_admin_module` (
  `id` int(1) unsigned NOT NULL,
  `module_id` int(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin_module`
--

INSERT INTO `super_admin_module` (`id`, `module_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin_permission`
--

CREATE TABLE IF NOT EXISTS `super_admin_permission` (
  `id` int(1) unsigned NOT NULL,
  `permission_id` int(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin_permission`
--

INSERT INTO `super_admin_permission` (`id`, `permission_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 11),
(1, 12),
(1, 13),
(1, 15),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 27),
(1, 30),
(1, 31),
(1, 34),
(1, 35),
(1, 36),
(1, 39),
(1, 40),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 48),
(1, 49),
(1, 50),
(1, 55),
(1, 58),
(1, 59),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 73),
(1, 75),
(1, 77),
(1, 78),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 95),
(1, 99),
(1, 100),
(1, 101),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(1, 109),
(1, 111),
(1, 112),
(1, 113),
(1, 114),
(1, 115),
(1, 116),
(1, 117),
(1, 118),
(1, 119),
(1, 120),
(1, 121),
(1, 122),
(1, 125),
(1, 127),
(1, 128),
(1, 131),
(1, 132),
(1, 133),
(1, 134),
(1, 135),
(1, 137),
(1, 138),
(1, 142),
(1, 146),
(1, 147),
(1, 150),
(1, 154),
(1, 155),
(1, 158),
(1, 161),
(1, 165),
(1, 169),
(1, 170),
(1, 172),
(1, 174),
(1, 175),
(1, 176),
(1, 177),
(1, 179),
(1, 180),
(1, 181),
(1, 182),
(1, 183),
(1, 184),
(1, 185),
(1, 186),
(1, 189),
(1, 190),
(1, 193),
(1, 194),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(1, 201),
(1, 202),
(1, 203),
(1, 205),
(1, 206),
(1, 207),
(1, 208),
(1, 211),
(1, 212),
(1, 215),
(1, 216),
(1, 217),
(1, 218);

-- --------------------------------------------------------

--
-- Table structure for table `suspended_students`
--

CREATE TABLE IF NOT EXISTS `suspended_students` (
`id` int(5) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `suspension_reason` mediumtext NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `suspended_students_view`
--
CREATE TABLE IF NOT EXISTS `suspended_students_view` (
`admission_no` varchar(10)
,`date` date
,`academic_year` int(5)
,`firstname` varchar(20)
,`lastname` varchar(20)
,`class_id` varchar(10)
,`class_stream_id` varchar(10)
,`dob` date
,`tribe_id` int(5)
,`religion_id` int(5)
,`home_address` varchar(50)
,`region` varchar(50)
,`former_school` varchar(100)
,`dorm_id` int(3)
,`status` enum('active','inactive')
,`stte_out` enum('transferred_out','expelled','completed','suspended','')
,`class_name` varchar(30)
,`stream` varchar(5)
);
-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `staff_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `teachers_timetable_view`
--
CREATE TABLE IF NOT EXISTS `teachers_timetable_view` (
`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`subject_name` varchar(50)
,`class_name` varchar(30)
,`stream` varchar(5)
,`teacher_id` varchar(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `teacher_duty_roaster`
--

CREATE TABLE IF NOT EXISTS `teacher_duty_roaster` (
  `teacher_id` varchar(10) NOT NULL DEFAULT '',
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teaching_assignment`
--

CREATE TABLE IF NOT EXISTS `teaching_assignment` (
`assignment_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `term_id` int(11) NOT NULL,
  `filter_flag` tinyint(1) NOT NULL DEFAULT '0',
  `dept_id` int(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `teaching_assignment_department_view`
--
CREATE TABLE IF NOT EXISTS `teaching_assignment_department_view` (
`subject_name` varchar(50)
,`term_id` int(11)
,`class_id` varchar(10)
,`class_name` varchar(30)
,`stream` varchar(5)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`gender` varchar(6)
,`staff_id` varchar(10)
,`dept_id` int(3) unsigned
,`dept_name` varchar(50)
,`dept_loc` varchar(50)
,`hod_id` varchar(10)
,`dept_type` enum('subject_department','non_subject_department')
,`assignment_id` int(11)
,`teacher_id` varchar(10)
,`subject_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`filter_flag` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `teaching_attendance`
--

CREATE TABLE IF NOT EXISTS `teaching_attendance` (
`ta_id` bigint(20) NOT NULL,
  `t_id` int(5) unsigned DEFAULT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `status` enum('taught','untaught','none') NOT NULL,
  `subTopicID` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `no_of_absentees` int(2) NOT NULL,
  `period_no` int(2) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teaching_log`
--

CREATE TABLE IF NOT EXISTS `teaching_log` (
`log_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `dept_id` int(3) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `term_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `approved` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teaching_log_view`
--

CREATE TABLE IF NOT EXISTS `teaching_log_view` (
  `subject_id` varchar(10) DEFAULT NULL,
  `subject_name` varchar(50) DEFAULT NULL,
  `subject_category` enum('T','N') DEFAULT NULL,
  `subject_choice` enum('optional','compulsory') DEFAULT NULL,
  `dept_id` int(3) unsigned DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `topic_name` varchar(100) DEFAULT NULL,
  `log_id` int(11) DEFAULT NULL,
  `subtopic_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `subtopic_name` varchar(255) DEFAULT NULL,
  `class_stream_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `term`
--

CREATE TABLE IF NOT EXISTS `term` (
`term_id` int(11) NOT NULL,
  `aid` int(5) NOT NULL,
  `term_name` enum('first_term','second_term') NOT NULL DEFAULT 'first_term',
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_current` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `term`
--

INSERT INTO `term` (`term_id`, `aid`, `term_name`, `begin_date`, `end_date`, `is_current`) VALUES
(1, 1, 'first_term', '2018-01-08', '2018-06-08', 'no'),
(2, 3, 'first_term', '2018-07-01', '2018-12-31', 'yes'),
(3, 1, 'second_term', '2018-07-01', '2018-12-31', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `timetable`
--

CREATE TABLE IF NOT EXISTS `timetable` (
`t_id` int(5) unsigned NOT NULL,
  `period_no` int(2) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL,
  `weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timetable`
--

INSERT INTO `timetable` (`t_id`, `period_no`, `assignment_id`, `teacher_id`, `class_stream_id`, `weekday`) VALUES
(51, 1, 12, 'STAFF0002', 'C05PCB', 'Monday'),
(52, 2, 12, 'STAFF0002', 'C05PCB', 'Monday'),
(53, 3, 11, 'STAFF0008', 'C05PCB', 'Monday'),
(54, 4, 11, 'STAFF0008', 'C05PCB', 'Monday'),
(55, 5, 13, 'STAFF0002', 'C05PCB', 'Monday'),
(56, 6, -1, 'T-C05PCB', 'C05PCB', 'Monday'),
(57, 7, 13, 'STAFF0002', 'C05PCB', 'Monday'),
(58, 8, 10, 'STAFF0008', 'C05PCB', 'Monday'),
(59, 9, 10, 'STAFF0008', 'C05PCB', 'Monday'),
(60, 10, -1, 'T-C05PCB', 'C05PCB', 'Monday'),
(61, 1, 10, 'STAFF0008', 'C05PCB', 'Tuesday'),
(62, 2, 10, 'STAFF0008', 'C05PCB', 'Tuesday'),
(63, 3, 12, 'STAFF0002', 'C05PCB', 'Tuesday'),
(64, 4, 12, 'STAFF0002', 'C05PCB', 'Tuesday'),
(65, 5, 14, 'STAFF0005', 'C05PCB', 'Tuesday'),
(66, 6, 14, 'STAFF0005', 'C05PCB', 'Tuesday'),
(67, 7, 11, 'STAFF0008', 'C05PCB', 'Tuesday'),
(68, 8, 11, 'STAFF0008', 'C05PCB', 'Tuesday'),
(69, 9, -1, 'T-C05PCB', 'C05PCB', 'Tuesday'),
(70, 10, -1, 'T-C05PCB', 'C05PCB', 'Tuesday'),
(71, 1, 11, 'STAFF0008', 'C05PCB', 'Wednesday'),
(72, 2, 11, 'STAFF0008', 'C05PCB', 'Wednesday'),
(73, 3, 14, 'STAFF0005', 'C05PCB', 'Wednesday'),
(74, 4, 14, 'STAFF0005', 'C05PCB', 'Wednesday'),
(75, 5, 10, 'STAFF0008', 'C05PCB', 'Wednesday'),
(76, 6, 10, 'STAFF0008', 'C05PCB', 'Wednesday'),
(77, 7, 12, 'STAFF0002', 'C05PCB', 'Wednesday'),
(78, 8, 12, 'STAFF0002', 'C05PCB', 'Wednesday'),
(79, 9, 13, 'STAFF0002', 'C05PCB', 'Wednesday'),
(80, 10, 13, 'STAFF0002', 'C05PCB', 'Wednesday'),
(81, 1, 10, 'STAFF0008', 'C05PCB', 'Thursday'),
(82, 2, 10, 'STAFF0008', 'C05PCB', 'Thursday'),
(83, 3, -1, 'T-C05PCB', 'C05PCB', 'Thursday'),
(84, 4, 12, 'STAFF0002', 'C05PCB', 'Thursday'),
(85, 5, 14, 'STAFF0005', 'C05PCB', 'Thursday'),
(86, 6, 14, 'STAFF0005', 'C05PCB', 'Thursday'),
(87, 7, 10, 'STAFF0008', 'C05PCB', 'Thursday'),
(88, 8, 11, 'STAFF0008', 'C05PCB', 'Thursday'),
(89, 9, 11, 'STAFF0008', 'C05PCB', 'Thursday'),
(90, 10, 12, 'STAFF0002', 'C05PCB', 'Thursday'),
(91, 1, -1, 'T-C05PCB', 'C05PCB', 'Friday'),
(92, 2, -1, 'T-C05PCB', 'C05PCB', 'Friday'),
(93, 3, 12, 'STAFF0002', 'C05PCB', 'Friday'),
(94, 4, 12, 'STAFF0002', 'C05PCB', 'Friday'),
(95, 5, 10, 'STAFF0008', 'C05PCB', 'Friday'),
(96, 6, 10, 'STAFF0008', 'C05PCB', 'Friday'),
(97, 7, -1, 'T-C05PCB', 'C05PCB', 'Friday'),
(98, 8, 14, 'STAFF0005', 'C05PCB', 'Friday'),
(99, 9, 11, 'STAFF0008', 'C05PCB', 'Friday'),
(100, 10, -1, 'T-C05PCB', 'C05PCB', 'Friday');

-- --------------------------------------------------------

--
-- Stand-in structure for view `timetable_view`
--
CREATE TABLE IF NOT EXISTS `timetable_view` (
`teacher_id` varchar(10)
,`class_stream_id` varchar(10)
,`csid` varchar(28)
,`start_date` date
,`end_date` date
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`assignment_id` int(11)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`t_id` int(5) unsigned
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`duration` int(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `title_sum_up_view`
--
CREATE TABLE IF NOT EXISTS `title_sum_up_view` (
`ISBN` varchar(17)
,`BOOK_TITLE` varchar(100)
,`AUTHOR_NAME` varchar(100)
,`COPIES` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `tod_daily_routine_tbl`
--

CREATE TABLE IF NOT EXISTS `tod_daily_routine_tbl` (
  `date_` date NOT NULL,
  `wake_up_time` varchar(20) NOT NULL,
  `parade_time` varchar(20) NOT NULL,
  `start_classes_time` varchar(20) NOT NULL,
  `breakfast` varchar(100) DEFAULT NULL,
  `lunch` varchar(100) DEFAULT NULL,
  `dinner` varchar(100) DEFAULT NULL,
  `events` longtext,
  `security` longtext,
  `comments` longtext,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
`topic_id` int(11) NOT NULL,
  `topic_name` varchar(100) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `class_stream_id` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topic_id`, `topic_name`, `subject_id`, `class_stream_id`) VALUES
(1, 'FASIHI SIMULIZI', 'SUB03', 'C01A'),
(3, 'MOTION IN A STAIGHT LINE', 'SUB02', 'C01A'),
(4, 'QUADRATIC EQUATION', 'SUB01', 'C01A'),
(5, 'MOTION IN A STRAIGHT LINE', 'SUB02', 'C01A'),
(6, 'logarism', 'SUB01', 'C01A'),
(7, 'ALGEBRA', 'SUB01', 'C01A'),
(8, 'Gas welding equipment', 'SUB01', 'C01A'),
(9, 'public finance', 'SUB01', 'C01A'),
(11, 'National income', 'SUB01', 'C01A'),
(12, 'Java', 'SUB07', 'C05PCB'),
(13, 'Database', 'SUB07', 'C05PCB'),
(14, 'Security', 'SUB07', 'C05PCB'),
(15, 'Music', 'SUB07', 'C05PCB'),
(17, 'Dancing', 'SUB07', 'C05PCB');

-- --------------------------------------------------------

--
-- Table structure for table `transferred_students`
--

CREATE TABLE IF NOT EXISTS `transferred_students` (
`id` int(5) NOT NULL,
  `admission_no` varchar(10) NOT NULL,
  `transfer_status` enum('IN','OUT') NOT NULL,
  `school` varchar(255) NOT NULL,
  `form` varchar(10) NOT NULL,
  `date_of_transfer` date NOT NULL,
  `transfer_letter` enum('Available','NOT Available') NOT NULL DEFAULT 'Available',
  `self_form_receipt` enum('Received','NOT Received') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transferred_students`
--

INSERT INTO `transferred_students` (`id`, `admission_no`, `transfer_status`, `school`, `form`, `date_of_transfer`, `transfer_letter`, `self_form_receipt`) VALUES
(3, '5552121', 'IN', 'Respect Secondary School', 'C05', '2018-05-30', 'Available', 'Received'),
(11, '1000003', 'IN', 'Katoro primary school', 'C01', '2018-05-30', 'Available', 'Received'),
(16, '5552231', 'IN', 'Nkurumah Secondary School', 'C05', '2018-05-30', 'Available', 'Received'),
(23, '6743321', 'IN', 'Aljazeera Secondary School', 'C05', '2018-05-31', 'Available', 'Received'),
(24, '1000012', 'OUT', 'Nyakabungo Sec', 'C05', '2018-05-31', 'Available', 'Received'),
(25, '1000000', 'IN', 'Kibaigwa primary school', 'C01', '2018-05-31', 'Available', 'Received'),
(27, '5552231', 'OUT', 'Nyakahoja Secondary School', 'C05', '2018-05-31', 'Available', 'Received');

-- --------------------------------------------------------

--
-- Table structure for table `tribe`
--

CREATE TABLE IF NOT EXISTS `tribe` (
  `tribe_name` varchar(100) NOT NULL,
`tribe_id` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tribe`
--

INSERT INTO `tribe` (`tribe_name`, `tribe_id`) VALUES
('Akiek', 1),
('Akie Northern Tanzania', 2),
('Arusha', 3),
('Assa', 4),
('Barabaig', 5),
('Balouch Coastal Tanzania', 6),
('Bembe', 7),
('Bena', 8),
('Bende', 9),
('Bondei', 10),
('Bungu', 11),
('Burunge', 12),
('Chaga', 13),
('Datooga', 14),
('Dhaiso', 15),
('Digo', 16),
('Doe', 17),
('Fipa', 18),
('Gogo', 19),
('Goa Coastal Tanzania', 20),
('Gorowa', 21),
('Gujirati Coastal Tanzania', 22),
('Gweno', 23),
('Ha', 24),
('Hadza', 26),
('Hangaza', 27),
('Haya', 28),
('Hehe', 29),
('Holoholo people', 30),
('Ikizu', 31),
('Ikoma', 32),
('Iraqw', 33),
('Isanzu', 34),
('Jiji', 35),
('Jita', 36),
('Kabwa', 37),
('Kagura', 38),
('Kaguru', 39),
('Kahe', 40),
('Kami', 41),
('Kamba Northern Tanzania', 42),
('Kara (also called Regi)', 43),
('Kerewe', 44),
('Kikuyu', 45),
('Kimbu', 46),
('Kinga', 47),
('Kisankasa', 48),
('Kisi', 49),
('Konongo', 50),
('Kuria', 51),
('Kutu', 52),
('Kwâ€™adza', 53),
('Kwavi', 54),
('Kwaya', 55),
('Kwere', 56),
('Kwifa', 57),
('Lagwa', 58),
('Lambya', 59),
('Luguru', 60),
('Luo', 61),
('Maasai', 62),
('Machinga', 63),
('Magoma', 64),
('Mbulu (in Arusha)', 65),
('Makonde', 66),
('Makua', 67),
('Makwe', 68),
('Malila', 69),
('Mambwe', 70),
('Manyema', 71),
('Manda', 72),
('Mahara', 73),
('Mediak', 74),
('Matengo', 75),
('Matumbi', 76),
('Maviha', 77),
('Mbugwe', 78),
('Mbunga', 79),
('Mbugu', 80),
('Mosiro', 82),
('Mpoto', 83),
('Msur Zanzibar', 84),
('Mwanga', 85),
('Mwera', 86),
('Ndali', 87),
('Ndamba', 88),
('Ndendeule', 89),
('Ndengereko', 90),
('Ndonde', 91),
('Ngas Northern Tanzania', 93),
('Ngasa', 94),
('Ngindo', 95),
('Ngoni', 96),
('Ngulu', 97),
('Ngazija (Zanzibar island)', 98),
('Ngurimi', 99),
('Ngwele', 100),
('Nilamba', 101),
('Nindi', 102),
('Nyakyusa', 103),
('Nyasa people in Mbeya', 104),
('Nyambo', 105),
('Nyamwanga', 106),
('Nyamwezi', 107),
('Nyanyembe', 108),
('Nyaturu', 109),
('Nyiha', 110),
('Nyiramba', 111),
('Omotik', 112),
('Okiek people', 113),
('Pangwa', 114),
('Pare', 115),
('Pimbwe', 116),
('Pogolo', 117),
('Rangi', 118),
('Rufiji', 119),
('Rungi', 120),
('Rungu', 121),
('Rungwa', 122),
('Rwa', 123),
('Safwa', 124),
('Sagara', 125),
('Sandawe', 126),
('Sangu', 127),
('Segeju', 128),
('Swengwear', 129),
('Shambaa', 130),
('Shirazi', 131),
('Shubi', 132),
('Sizaki', 133),
('Suba', 134),
('Sukuma', 135),
('Sumbwa', 136),
('Sungu Tanga', 137),
('Swahili', 138),
('Temi', 139),
('Tongwe', 140),
('Twa Western Tanzania', 141),
('Tutsi Western Tanzania', 142),
('Tumbuka', 143),
('Vidunda', 144),
('Vinza', 145),
('Wanda', 146),
('Washihiri Zanzibar', 147),
('Wanji', 149),
('Wangarenaro Arusha', 150),
('Ware', 151),
('Yao', 153),
('Zanaki', 154),
('Zaramo', 155),
('Zigula', 156),
('Zinza', 157),
('Zyoba', 158);

-- --------------------------------------------------------

--
-- Stand-in structure for view `used_to_edit_timetable_view`
--
CREATE TABLE IF NOT EXISTS `used_to_edit_timetable_view` (
`teacher_id` varchar(10)
,`class_stream_id` varchar(10)
,`start_date` date
,`end_date` date
,`staff_id` varchar(10)
,`firstname` varchar(30)
,`middlename` varchar(30)
,`lastname` varchar(30)
,`dob` date
,`marital_status` enum('Married','Single','Divorced','Widowed')
,`gender` varchar(6)
,`email` varchar(50)
,`phone_no` varchar(13)
,`password` varchar(40)
,`staff_type` char(1)
,`assignment_id` int(11)
,`weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday')
,`t_id` int(5) unsigned
,`subject_id` varchar(10)
,`subject_name` varchar(50)
,`description` varchar(255)
,`dept_id` int(3) unsigned
,`subject_category` enum('T','N')
,`subject_choice` enum('optional','compulsory')
,`period_no` int(2)
,`start_time` time
,`end_time` time
,`duration` int(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `users`
--
CREATE TABLE IF NOT EXISTS `users` (
`id` varchar(11)
,`names` varchar(61)
);
-- --------------------------------------------------------

--
-- Table structure for table `waliomaliza_tbl`
--

CREATE TABLE IF NOT EXISTS `waliomaliza_tbl` (
`id` int(11) NOT NULL,
  `index_no` varchar(30) DEFAULT NULL,
  `slip_no` varchar(30) DEFAULT NULL,
  `admission_no` varchar(10) NOT NULL,
  `division` varchar(2) DEFAULT NULL,
  `points` int(2) DEFAULT NULL,
  `kashachukua_cheti` enum('ndiyo','hapana') DEFAULT 'hapana',
  `tarehe_ya_kuchukua` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `completion_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `academic_year_term_view`
--
DROP TABLE IF EXISTS `academic_year_term_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `academic_year_term_view` AS select `term`.`term_id` AS `term_id`,`term`.`is_current` AS `is_current`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`term`.`term_name` AS `term_name`,`term`.`begin_date` AS `term_begin_date`,`term`.`end_date` AS `term_end_date` from (`academic_year` join `term` on((`academic_year`.`id` = `term`.`aid`))) where (`academic_year`.`status` = 'current_academic_year');

-- --------------------------------------------------------

--
-- Structure for view `active_students_view`
--
DROP TABLE IF EXISTS `active_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `active_students_view` AS select `student`.`stte_out` AS `stte_out`,`student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from (((`student` join `class_level` on((`student`.`class_id` = `class_level`.`class_id`))) left join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `academic_year` on((`student`.`academic_year` = `academic_year`.`id`))) where ((`student`.`status` = 'active') and ((`student`.`stte` = 'transferred_in') or (`student`.`stte` = 'selected')));

-- --------------------------------------------------------

--
-- Structure for view `activity_log_view`
--
DROP TABLE IF EXISTS `activity_log_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `activity_log_view` AS select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `username` from (`activity_log` join `staff` on((`staff`.`staff_id` = `activity_log`.`user_id`))) union select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,`admin`.`username` AS `username` from (`activity_log` join `admin` on((`admin`.`id` = `activity_log`.`user_id`))) order by `time` desc;

-- --------------------------------------------------------

--
-- Structure for view `all_announcements_view`
--
DROP TABLE IF EXISTS `all_announcements_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_announcements_view` AS select `announcement`.`group_id` AS `group_id`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`group_type`.`group_name` AS `group_name`,`group_type`.`description` AS `description` from (`announcement` join `group_type` on((`announcement`.`group_id` = `group_type`.`group_id`)));

-- --------------------------------------------------------

--
-- Structure for view `all_class_streams_view`
--
DROP TABLE IF EXISTS `all_class_streams_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_class_streams_view` AS select `class_stream`.`teacher_id` AS `teacher_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `teacher_names`,`academic_year`.`id` AS `id`,`academic_year`.`year` AS `year`,`class_level`.`class_id` AS `class_id`,`class_stream`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`class_level`.`level` AS `level`,(select count(`student`.`admission_no`) from `student` where ((`student`.`status` = 'active') and (`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) AS `enrolled`,`class_stream`.`capacity` AS `capacity`,`class_stream`.`description` AS `description`,`class_stream`.`id_display` AS `id_display` from ((((`class_level` join `class_stream` on((`class_level`.`class_id` = `class_stream`.`class_id`))) left join `student` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `academic_year` on((`class_level`.`level` = `academic_year`.`class_level`))) left join `staff` on((`staff`.`staff_id` = `class_stream`.`teacher_id`))) where (`academic_year`.`status` = 'current_academic_year') group by `class_level`.`class_name`,`class_stream`.`stream` order by `class_stream`.`class_id`,`class_stream`.`class_stream_id`;

-- --------------------------------------------------------

--
-- Structure for view `all_results_view`
--
DROP TABLE IF EXISTS `all_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_results_view` AS select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`class_level`.`class_name` AS `class_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`term_id` AS `term_id`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`term`.`aid` AS `aid`,`term`.`term_name` AS `term_name` from ((((`term` join `student_subject_score_position` on((`term`.`term_id` = `student_subject_score_position`.`term_id`))) join `academic_year` on((`academic_year`.`id` = `term`.`aid`))) join `student` on((`student`.`admission_no` = `student_subject_score_position`.`admission_no`))) join `class_level` on((`class_level`.`class_id` = `student_subject_score_position`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `all_teachers_view`
--
DROP TABLE IF EXISTS `all_teachers_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_teachers_view` AS select `staff`.`staff_id` AS `staff_id`,`subject_teacher`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`user_role` AS `user_role`,`staff`.`staff_type` AS `staff_type` from (`staff` left join (`subject` join `subject_teacher` on((`subject_teacher`.`subject_id` = `subject`.`subject_id`))) on((`subject_teacher`.`teacher_id` = `staff`.`staff_id`))) where (`staff`.`staff_type` = 'T');

-- --------------------------------------------------------

--
-- Structure for view `assigned_user_roles`
--
DROP TABLE IF EXISTS `assigned_user_roles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assigned_user_roles` AS select `role_type`.`role_name` AS `role_name`,`role_type`.`description` AS `description`,`staff_role`.`role_type_id` AS `role_type_id`,`staff_role`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`role_type`.`active` AS `active` from ((`staff_role` join `role_type` on((`staff_role`.`role_type_id` = `role_type`.`role_type_id`))) join `staff` on((`staff`.`staff_id` = `staff_role`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `assign_hod_class_teacher_view`
--
DROP TABLE IF EXISTS `assign_hod_class_teacher_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assign_hod_class_teacher_view` AS select `staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type` from (`staff` join `teacher` on((`staff`.`staff_id` = `teacher`.`staff_id`))) where (`staff`.`status` = 'active');

-- --------------------------------------------------------

--
-- Structure for view `audit_staff_attendance_view`
--
DROP TABLE IF EXISTS `audit_staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_staff_attendance_view` AS select `first_join_staff_attendance_view`.`firstname` AS `firstname`,`first_join_staff_attendance_view`.`lastname` AS `lastname`,`first_join_staff_attendance_view`.`staff_id` AS `staff_id`,`first_join_staff_attendance_view`.`date_` AS `date_`,`first_join_staff_attendance_view`.`day` AS `day`,`first_join_staff_attendance_view`.`at` AS `at`,`first_join_staff_attendance_view`.`att_desc` AS `att_desc`,`first_join_staff_attendance_view`.`change_type` AS `change_type`,`first_join_staff_attendance_view`.`change_by` AS `change_by`,`first_join_staff_attendance_view`.`ip_address` AS `ip_address`,`first_join_staff_attendance_view`.`change_time` AS `change_time`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from (`staff` join `first_join_staff_attendance_view` on((`staff`.`staff_id` = convert(`first_join_staff_attendance_view`.`change_by` using utf8)))) union select `first_join_staff_attendance_view`.`firstname` AS `firstname`,`first_join_staff_attendance_view`.`lastname` AS `lastname`,`first_join_staff_attendance_view`.`staff_id` AS `staff_id`,`first_join_staff_attendance_view`.`date_` AS `date_`,`first_join_staff_attendance_view`.`day` AS `day`,`first_join_staff_attendance_view`.`at` AS `at`,`first_join_staff_attendance_view`.`att_desc` AS `att_desc`,`first_join_staff_attendance_view`.`change_type` AS `change_type`,`first_join_staff_attendance_view`.`change_by` AS `change_by`,`first_join_staff_attendance_view`.`ip_address` AS `ip_address`,`first_join_staff_attendance_view`.`change_time` AS `change_time`,`admin`.`username` AS `username` from (`admin` join `first_join_staff_attendance_view` on((`admin`.`id` = `first_join_staff_attendance_view`.`change_by`))) order by `staff_id`,`change_time` desc;

-- --------------------------------------------------------

--
-- Structure for view `audit_student_attendance_view`
--
DROP TABLE IF EXISTS `audit_student_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_student_attendance_view` AS select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_attendance_record`.`admission_no` AS `admission_no`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`class_stream_id` AS `csid`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_student_attendance`.`change_type` AS `change_type`,`audit_student_attendance`.`change_by` AS `change_by`,`audit_student_attendance`.`ip_address` AS `ip_address`,`audit_student_attendance`.`change_time` AS `change_time`,`class_level`.`class_name` AS `class_name`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from ((((((`student_attendance_record` join `student` on((`student_attendance_record`.`admission_no` = `student`.`admission_no`))) join `audit_student_attendance` on((`student_attendance_record`.`id` = `audit_student_attendance`.`sar_id`))) join `attendance` on((`audit_student_attendance`.`att_id` = `attendance`.`att_id`))) join `class_stream` on((`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_stream`.`class_id` = `class_level`.`class_id`))) join `staff` on((`audit_student_attendance`.`change_by` = `staff`.`staff_id`))) union select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_attendance_record`.`admission_no` AS `admission_no`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`class_stream_id` AS `csid`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_student_attendance`.`change_type` AS `change_type`,`audit_student_attendance`.`change_by` AS `change_by`,`audit_student_attendance`.`ip_address` AS `ip_address`,`audit_student_attendance`.`change_time` AS `change_time`,`class_level`.`class_name` AS `class_name`,`admin`.`username` AS `added_changed_by` from ((((((`student_attendance_record` join `student` on((`student_attendance_record`.`admission_no` = `student`.`admission_no`))) join `audit_student_attendance` on((`student_attendance_record`.`id` = `audit_student_attendance`.`sar_id`))) join `attendance` on((`audit_student_attendance`.`att_id` = `attendance`.`att_id`))) join `class_stream` on((`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_stream`.`class_id` = `class_level`.`class_id`))) join `admin` on((`audit_student_attendance`.`change_by` = `admin`.`id`))) order by `admission_no`,`change_time` desc;

-- --------------------------------------------------------

--
-- Structure for view `audit_student_results_view`
--
DROP TABLE IF EXISTS `audit_student_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `audit_student_results_view` AS select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`subject`.`subject_name` AS `subject_name`,`exam_type`.`exam_name` AS `exam_name`,`audit_student_subject_assessment`.`marks` AS `marks`,`class_level`.`class_name` AS `class_name`,`student_subject_assessment`.`e_date` AS `e_date`,`term`.`term_name` AS `term_name`,`academic_year`.`year` AS `year`,`audit_student_subject_assessment`.`change_type` AS `change_type`,`audit_student_subject_assessment`.`ip_address` AS `ip_address`,`audit_student_subject_assessment`.`change_time` AS `change_time`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `added_changed_by` from (((((((((`student` join `student_subject_assessment` on((`student`.`admission_no` = `student_subject_assessment`.`admission_no`))) join `audit_student_subject_assessment` on((`student_subject_assessment`.`ssa_id` = `audit_student_subject_assessment`.`ssa_id`))) join `subject` on((`student_subject_assessment`.`subject_id` = `subject`.`subject_id`))) join `exam_type` on((`student_subject_assessment`.`etype_id` = `exam_type`.`id`))) join `class_stream` on((`student_subject_assessment`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`))) join `term` on((`student_subject_assessment`.`term_id` = `term`.`term_id`))) join `academic_year` on((`academic_year`.`id` = `term`.`aid`))) join `staff` on((`staff`.`staff_id` = `audit_student_subject_assessment`.`change_by`))) union select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`subject`.`subject_name` AS `subject_name`,`exam_type`.`exam_name` AS `exam_name`,`audit_student_subject_assessment`.`marks` AS `marks`,`class_level`.`class_name` AS `class_name`,`student_subject_assessment`.`e_date` AS `e_date`,`term`.`term_name` AS `term_name`,`academic_year`.`year` AS `year`,`audit_student_subject_assessment`.`change_type` AS `change_type`,`audit_student_subject_assessment`.`ip_address` AS `ip_address`,`audit_student_subject_assessment`.`change_time` AS `change_time`,`admin`.`username` AS `added_changed_by` from (((((((((`student` join `student_subject_assessment` on((`student`.`admission_no` = `student_subject_assessment`.`admission_no`))) join `audit_student_subject_assessment` on((`student_subject_assessment`.`ssa_id` = `audit_student_subject_assessment`.`ssa_id`))) join `subject` on((`student_subject_assessment`.`subject_id` = `subject`.`subject_id`))) join `exam_type` on((`student_subject_assessment`.`etype_id` = `exam_type`.`id`))) join `class_stream` on((`student_subject_assessment`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`))) join `term` on((`student_subject_assessment`.`term_id` = `term`.`term_id`))) join `academic_year` on((`academic_year`.`id` = `term`.`aid`))) join `admin` on((`admin`.`id` = `audit_student_subject_assessment`.`change_by`))) order by `student_names`,`change_time` desc;

-- --------------------------------------------------------

--
-- Structure for view `available_books_view`
--
DROP TABLE IF EXISTS `available_books_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `available_books_view` AS select distinct `borrowable_view`.`ISBN` AS `ISBN`,`borrowable_view`.`ID` AS `ID`,`borrowable_view`.`BOOK_TITLE` AS `BOOK_TITLE` from `borrowable_view` where (not((`borrowable_view`.`ISBN`,`borrowable_view`.`ID`) in (select `lost_book_view`.`ISBN`,`lost_book_view`.`ID` from `lost_book_view`)));

-- --------------------------------------------------------

--
-- Structure for view `book_copy_report`
--
DROP TABLE IF EXISTS `book_copy_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `book_copy_report` AS select `book_type`.`ISBN` AS `ISBN`,`book`.`ID` AS `CODE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`EDITION` AS `EDITION`,`book`.`STATUS` AS `STATUS`,`book`.`LAST_EDIT` AS `LAST_EDIT` from (`book` join `book_type`) where (`book_type`.`ISBN` = `book`.`isbn`) order by `book`.`isbn`,`book`.`ID`;

-- --------------------------------------------------------

--
-- Structure for view `book_type_report`
--
DROP TABLE IF EXISTS `book_type_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `book_type_report` AS select `book_type`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`EDITION` AS `EDITION`,`book_type`.`LAST_EDIT` AS `LAST_EDIT`,(select count(`book`.`ID`) from `book` where (`book`.`isbn` = `book_type`.`ISBN`)) AS `COPIES` from `book_type` order by `book_type`.`LAST_EDIT` desc;

-- --------------------------------------------------------

--
-- Structure for view `borrowable_view`
--
DROP TABLE IF EXISTS `borrowable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowable_view` AS select `remove_borrowed_view`.`ID` AS `ID`,`remove_borrowed_view`.`ISBN` AS `ISBN`,`remove_borrowed_view`.`STATUS` AS `STATUS`,`remove_borrowed_view`.`LAST_EDIT` AS `LAST_EDIT`,`remove_borrowed_view`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`remove_borrowed_view`.`BOOK_TITLE` AS `BOOK_TITLE`,`remove_borrowed_view`.`EDITION` AS `EDITION` from `remove_borrowed_view` where (not((`remove_borrowed_view`.`ID`,`remove_borrowed_view`.`ISBN`) in (select `book`.`ID`,`book`.`isbn` from `book` where (`book`.`STATUS` = 'lost'))));

-- --------------------------------------------------------

--
-- Structure for view `borrowed_report`
--
DROP TABLE IF EXISTS `borrowed_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowed_report` AS select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,timestampdiff(DAY,now(),`book_loan_info`.`RETURN_DATE`) AS `TIME_LEFT`,`book_type`.`EDITION` AS `EDITION`,`borrowers`.`borrower_description` AS `BORROWER_DESCRIPTION`,`borrowers`.`borrower_id` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`status_before` AS `STATUS_BEFORE`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `borrowers`) where ((`book_type`.`ISBN` = `book_loan_info`.`ISBN`) and (`book_loan_info`.`BORROWER_ID` = convert(`borrowers`.`borrower_id` using utf8)) and (`book_loan_info`.`FLAG` = 'BORROWED')) order by timestampdiff(DAY,now(),`book_loan_info`.`RETURN_DATE`);

-- --------------------------------------------------------

--
-- Structure for view `borrowed_report_view`
--
DROP TABLE IF EXISTS `borrowed_report_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowed_report_view` AS select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,timestampdiff(DAY,now(),`book_loan_info`.`RETURN_DATE`) AS `TIME_LEFT`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`EDITION` AS `EDITION`,`users`.`names` AS `NAMES`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `users`) where ((`book_loan_info`.`ISBN` = `book_type`.`ISBN`) and (`book_loan_info`.`BORROWER_ID` = convert(`users`.`id` using utf8)) and (`book_loan_info`.`FLAG` = 'borrowed')) order by timestampdiff(DAY,now(),`book_loan_info`.`RETURN_DATE`);

-- --------------------------------------------------------

--
-- Structure for view `borrowers`
--
DROP TABLE IF EXISTS `borrowers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowers` AS select `student`.`admission_no` AS `borrower_id`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `borrower_description`,`student`.`b_type` AS `b_type` from `student` where ((`student`.`status` = 'active') and ((`student`.`stte_out` <> 'suspended') or isnull(`student`.`stte_out`))) union select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `concat(firstname," ",lastname)`,`staff`.`b_type` AS `b_type` from `staff` where (`staff`.`status` = 'active') union select `department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`b_type` AS `b_type` from `department`;

-- --------------------------------------------------------

--
-- Structure for view `borrowing_report`
--
DROP TABLE IF EXISTS `borrowing_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `borrowing_report` AS select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`DATE_DUE_BACK` AS `DATE_DUE_BACK`,`book_loan_info`.`DATE_OVERDUE` AS `DATE_OVERDUE`,`book_loan_info`.`status_before` AS `status_before`,`book_loan_info`.`status_after` AS `status_after`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`LAST_EDIT` AS `LAST_EDIT`,`book_loan_info`.`FLAG` AS `FLAG`,`users`.`names` AS `NAMES` from (`book_loan_info` join `users` on((convert(`users`.`id` using utf8) = `book_loan_info`.`BORROWER_ID`))) order by `book_loan_info`.`LAST_EDIT`;

-- --------------------------------------------------------

--
-- Structure for view `calculate_a_level_units_view`
--
DROP TABLE IF EXISTS `calculate_a_level_units_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `calculate_a_level_units_view` AS select `student_subject_score_position`.`term_id` AS `term_id`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`a_level_grade_system_tbl`.`points` AS `points` from (`student_subject_score_position` join `a_level_grade_system_tbl` on((`a_level_grade_system_tbl`.`grade` = `student_subject_score_position`.`grade`))) where (not(`student_subject_score_position`.`subject_id` in (select `subject`.`subject_id` from `subject` where (`subject`.`subject_type` = 'Supplement')))) order by `a_level_grade_system_tbl`.`grade`;

-- --------------------------------------------------------

--
-- Structure for view `calculate_o_level_units_view`
--
DROP TABLE IF EXISTS `calculate_o_level_units_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `calculate_o_level_units_view` AS select `student_subject_score_position`.`term_id` AS `term_id`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`subject_id` AS `subject_id`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`student_subject_score_position`.`grade` AS `grade`,`o_level_grade_system_tbl`.`unit_point` AS `unit_point` from (`student_subject_score_position` join `o_level_grade_system_tbl` on((`o_level_grade_system_tbl`.`grade` = `student_subject_score_position`.`grade`))) order by `o_level_grade_system_tbl`.`grade`;

-- --------------------------------------------------------

--
-- Structure for view `classes_assigned_view`
--
DROP TABLE IF EXISTS `classes_assigned_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `classes_assigned_view` AS select distinct `class_stream`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date` from ((`teaching_assignment` join `class_stream` on((`class_stream`.`class_stream_id` = `teaching_assignment`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `class_asset_view`
--
DROP TABLE IF EXISTS `class_asset_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_asset_view` AS select `class_asset`.`id` AS `id`,`class_asset`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_asset`.`asset_name` AS `asset_name`,`class_asset`.`asset_no` AS `asset_no`,`class_asset`.`status` AS `status`,`class_asset`.`description` AS `description` from ((`class_asset` join `class_stream` on((`class_asset`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `class_journal_view`
--
DROP TABLE IF EXISTS `class_journal_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_journal_view` AS select `subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`timetable`.`class_stream_id` AS `timetable_class_stream_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from ((`timetable` left join (((`teaching_assignment` join `teacher` on((`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`))) join `staff` on((`teacher`.`staff_id` = `staff`.`staff_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) on((`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`))) join `period` on((`timetable`.`period_no` = `period`.`period_no`)));

-- --------------------------------------------------------

--
-- Structure for view `class_stream_subject_display_view`
--
DROP TABLE IF EXISTS `class_stream_subject_display_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_stream_subject_display_view` AS select `class_stream_subject_view`.`class_id` AS `class_id`,`class_stream_subject_view`.`class_stream_id` AS `class_stream_id`,`class_stream_subject_view`.`class_name` AS `class_name`,`class_stream_subject_view`.`subject_id` AS `subject_id`,`class_stream_subject_view`.`stream` AS `stream`,group_concat(' ',`class_stream_subject_view`.`subject_name` separator ',') AS `class_subjects` from `class_stream_subject_view` group by `class_stream_subject_view`.`class_stream_id`;

-- --------------------------------------------------------

--
-- Structure for view `class_stream_subject_view`
--
DROP TABLE IF EXISTS `class_stream_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_stream_subject_view` AS select `class_level`.`class_id` AS `class_id`,`class_stream_subject`.`class_stream_id` AS `class_stream_id`,`class_stream_subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`subject_choice` AS `subject_choice`,`subject`.`subject_category` AS `subject_category`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from (((`class_stream_subject` join `class_stream` on((`class_stream_subject`.`class_stream_id` = convert(`class_stream`.`class_stream_id` using utf8)))) join `subject` on((`class_stream_subject`.`subject_id` = convert(`subject`.`subject_id` using utf8)))) join `class_level` on((`class_stream`.`class_id` = `class_level`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `class_subject_teachers_view`
--
DROP TABLE IF EXISTS `class_subject_teachers_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_subject_teachers_view` AS select `staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names`,`subject`.`subject_name` AS `subject_name`,`teaching_assignment`.`class_stream_id` AS `class_stream_id` from (((`teaching_assignment` join `teacher` on((`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`))) join `staff` on((`teacher`.`staff_id` = `staff`.`staff_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`)));

-- --------------------------------------------------------

--
-- Structure for view `completed_students_view`
--
DROP TABLE IF EXISTS `completed_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `completed_students_view` AS select `student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`waliomaliza_tbl`.`id` AS `id`,`waliomaliza_tbl`.`index_no` AS `index_no`,`waliomaliza_tbl`.`admission_no` AS `admission_no`,`waliomaliza_tbl`.`division` AS `division`,`waliomaliza_tbl`.`points` AS `points`,`waliomaliza_tbl`.`kashachukua_cheti` AS `kashachukua_cheti`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `academic_year_id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from ((((`student` join `class_level` on((`student`.`class_id` = `class_level`.`class_id`))) left join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `academic_year` on((`student`.`academic_year` = `academic_year`.`id`))) join `waliomaliza_tbl` on((`waliomaliza_tbl`.`admission_no` = `student`.`admission_no`))) where ((`student`.`stte_out` = 'completed') and (`student`.`status` = 'inactive') and ((`student`.`stte` = 'transferred_in') or (`student`.`stte` = 'selected')));

-- --------------------------------------------------------

--
-- Structure for view `count_borrowed_per_person_view`
--
DROP TABLE IF EXISTS `count_borrowed_per_person_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `count_borrowed_per_person_view` AS select count(`borrowing_report`.`BORROWER_ID`) AS `copies`,`borrowing_report`.`NAMES` AS `names`,`borrowing_report`.`BORROWER_TYPE` AS `borrower_type` from `borrowing_report` where (`borrowing_report`.`FLAG` = 'borrowed') group by `borrowing_report`.`BORROWER_ID`;

-- --------------------------------------------------------

--
-- Structure for view `current_term_academic_year_view`
--
DROP TABLE IF EXISTS `current_term_academic_year_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `current_term_academic_year_view` AS select `term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`id` AS `id`,`academic_year`.`year` AS `year` from (`term` join `academic_year` on((`academic_year`.`id` = `term`.`aid`))) where ((`term`.`is_current` = 'yes') and (`academic_year`.`status` = 'current_academic_year'));

-- --------------------------------------------------------

--
-- Structure for view `daily_attendance_report_view`
--
DROP TABLE IF EXISTS `daily_attendance_report_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_attendance_report_view` AS select `class_stream`.`class_id` AS `class_id`,`student_attendance_record`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`attendance`.`att_type` AS `att_type`,count(0) AS `nos`,`student_attendance_record`.`date_` AS `date_` from (((`student_attendance_record` join `attendance` on((`student_attendance_record`.`att_id` = `attendance`.`att_id`))) join `class_stream` on((`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_stream`.`class_id` = `class_level`.`class_id`))) group by `student_attendance_record`.`date_`,`student_attendance_record`.`class_stream_id`,`attendance`.`att_type`;

-- --------------------------------------------------------

--
-- Structure for view `daily_school_attendance_view`
--
DROP TABLE IF EXISTS `daily_school_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_school_attendance_view` AS select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`attendance`.`att_type` AS `att_type`,`class_level`.`class_id` AS `class_id`,`class_stream`.`class_stream_id` AS `class_stream_id`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`day` AS `day` from ((((`student_attendance_record` join `student` on((`student_attendance_record`.`admission_no` = `student`.`admission_no`))) join `class_stream` on((`student_attendance_record`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `attendance` on((`student_attendance_record`.`att_id` = `attendance`.`att_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `daily_student_attendance_view`
--
DROP TABLE IF EXISTS `daily_student_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_student_attendance_view` AS select `student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`student_attendance_record`.`day` AS `day`,`student_attendance_record`.`date_` AS `date_`,`student_attendance_record`.`class_stream_id` AS `class_stream_id` from ((`student` join `student_attendance_record` on((`student`.`admission_no` = `student_attendance_record`.`admission_no`))) join `attendance` on((`student_attendance_record`.`att_id` = `attendance`.`att_id`)));

-- --------------------------------------------------------

--
-- Structure for view `daily_student_roll_call_view`
--
DROP TABLE IF EXISTS `daily_student_roll_call_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_student_roll_call_view` AS select `student_roll_call_record`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`student_roll_call_record`.`date_` AS `date_`,`student_roll_call_record`.`dorm_id` AS `dorm_id`,`student_roll_call_record`.`att_id` AS `att_id` from ((`student` join `student_roll_call_record` on((`student`.`admission_no` = `student_roll_call_record`.`admission_no`))) join `attendance` on((`student_roll_call_record`.`att_id` = `attendance`.`att_id`)));

-- --------------------------------------------------------

--
-- Structure for view `department_class_stream_subject_view`
--
DROP TABLE IF EXISTS `department_class_stream_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_class_stream_subject_view` AS select `class_level`.`class_name` AS `class_name`,`subject`.`subject_name` AS `subject_name`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`class_stream`.`class_stream_id` AS `class_stream_id`,`class_stream`.`class_id` AS `class_id`,`class_stream`.`stream` AS `stream`,`class_stream`.`stream_id` AS `stream_id`,`class_stream`.`teacher_id` AS `teacher_id`,`class_stream`.`admission_no` AS `admission_no`,`class_stream`.`description` AS `description`,`class_stream`.`capacity` AS `capacity`,`class_stream`.`id_display` AS `id_display`,`class_stream_subject`.`subject_id` AS `subject_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `staff_id`,`department`.`dept_type` AS `dept_type` from ((((`class_stream_subject` join `subject` on((`class_stream_subject`.`subject_id` = convert(`subject`.`subject_id` using utf8)))) join `class_stream` on((`class_stream_subject`.`class_stream_id` = convert(`class_stream`.`class_stream_id` using utf8)))) join `department` on((`subject`.`dept_id` = `department`.`dept_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `department_staff_view`
--
DROP TABLE IF EXISTS `department_staff_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_staff_view` AS select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`middlename`,' ',`staff`.`lastname`) AS `staff_names`,`subject`.`dept_id` AS `dept_id`,`staff`.`gender` AS `gender`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name` from (((`subject_teacher` join `subject` on((`subject`.`subject_id` = `subject_teacher`.`subject_id`))) join `teacher` on((`subject_teacher`.`teacher_id` = `teacher`.`staff_id`))) join `staff` on((`staff`.`staff_id` = `teacher`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `department_view`
--
DROP TABLE IF EXISTS `department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `department_view` AS select `department`.`staff_id` AS `staff_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`dept_type` AS `dept_type`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type` from (`department` left join `staff` on((`department`.`staff_id` = `staff`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `disabled_students`
--
DROP TABLE IF EXISTS `disabled_students`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disabled_students` AS select `student`.`disabled` AS `disabled`,`student`.`status` AS `statasi`,`student`.`disability` AS `disability`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`student`.`class_stream_id` AS `class_stream_id`,`class_stream`.`stream` AS `stream`,`academic_year`.`id` AS `id`,`academic_year`.`status` AS `status`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`year` AS `year` from (((`student` join `class_level` on((`student`.`class_id` = `class_level`.`class_id`))) left join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `academic_year` on((`student`.`academic_year` = `academic_year`.`id`))) where ((`student`.`disabled` = 'yes') and (`student`.`status` = 'active') and ((`student`.`stte` = 'transferred_in') or (`student`.`stte` = 'selected')));

-- --------------------------------------------------------

--
-- Structure for view `dorm_asset_view`
--
DROP TABLE IF EXISTS `dorm_asset_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dorm_asset_view` AS select `dorm_asset`.`id` AS `id`,`dorm_asset`.`dorm_id` AS `dorm_id`,`dormitory`.`dorm_name` AS `dorm_name`,`dorm_asset`.`asset_name` AS `asset_name`,`dorm_asset`.`asset_no` AS `asset_no`,`dorm_asset`.`status` AS `status`,`dorm_asset`.`description` AS `description` from (`dorm_asset` join `dormitory` on((`dorm_asset`.`dorm_id` = `dormitory`.`dorm_id`)));

-- --------------------------------------------------------

--
-- Structure for view `dorm_capacity_view`
--
DROP TABLE IF EXISTS `dorm_capacity_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dorm_capacity_view` AS select `dormitory`.`dorm_id` AS `dorm_id`,(select count(`student`.`admission_no`) from `student` where ((`student`.`status` = 'active') and (`student`.`dorm_id` = `dormitory`.`dorm_id`))) AS `no_of_students`,`dormitory`.`dorm_name` AS `dorm_name`,`dormitory`.`capacity` AS `capacity`,(`dormitory`.`capacity` - (select count(`student`.`admission_no`) from `student` where ((`student`.`status` = 'active') and (`student`.`dorm_id` = `dormitory`.`dorm_id`)))) AS `remaining` from (`dormitory` left join `student` on((`dormitory`.`dorm_id` = `student`.`dorm_id`))) group by `dormitory`.`dorm_id`;

-- --------------------------------------------------------

--
-- Structure for view `edit_teaching_attendance_view`
--
DROP TABLE IF EXISTS `edit_teaching_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `edit_teaching_attendance_view` AS select `teaching_attendance`.`subTopicID` AS `subTopicID`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`teaching_attendance`.`ta_id` AS `ta_id`,`teaching_attendance`.`no_of_absentees` AS `no_of_absentees`,`teaching_attendance`.`status` AS `status`,`teaching_attendance`.`date` AS `date`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from (((((((`teaching_assignment` join `teacher` on((`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`))) join `staff` on((`teacher`.`staff_id` = `staff`.`staff_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) join `timetable` on((`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`))) join `period` on((`timetable`.`period_no` = `period`.`period_no`))) join `teaching_attendance` on((`teaching_attendance`.`t_id` = `timetable`.`t_id`))) join `sub_topic` on((`teaching_attendance`.`subTopicID` = `sub_topic`.`subtopic_id`)));

-- --------------------------------------------------------

--
-- Structure for view `edit_teaching_attendance_view_mpya`
--
DROP TABLE IF EXISTS `edit_teaching_attendance_view_mpya`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `edit_teaching_attendance_view_mpya` AS select `teaching_attendance`.`subTopicID` AS `subTopicID`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`teaching_attendance`.`ta_id` AS `ta_id`,`teaching_attendance`.`no_of_absentees` AS `no_of_absentees`,`teaching_attendance`.`status` AS `status`,`teaching_attendance`.`date` AS `date`,`teaching_assignment`.`subject_id` AS `subject_id`,`timetable`.`t_id` AS `t_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`timetable`.`period_no` AS `period_no`,`timetable`.`weekday` AS `weekday`,`subject`.`subject_name` AS `subject_name`,concat(`staff`.`firstname`,'  ',`staff`.`lastname`) AS `subject_teacher` from ((`teaching_attendance` left join (((((`teaching_assignment` join `teacher` on((`teaching_assignment`.`teacher_id` = `teacher`.`staff_id`))) join `staff` on((`teacher`.`staff_id` = `staff`.`staff_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) join `timetable` on((`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`))) join `period` on((`timetable`.`period_no` = `period`.`period_no`))) on((`teaching_attendance`.`t_id` = `timetable`.`t_id`))) left join `sub_topic` on((`teaching_attendance`.`subTopicID` = `sub_topic`.`subtopic_id`)));

-- --------------------------------------------------------

--
-- Structure for view `expelled_students_view`
--
DROP TABLE IF EXISTS `expelled_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expelled_students_view` AS select `student`.`admission_no` AS `admission_no`,`student`.`date` AS `date`,`student`.`academic_year` AS `academic_year`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`dob` AS `dob`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`dorm_id` AS `dorm_id`,`student`.`status` AS `status`,`student`.`stte_out` AS `stte_out`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((`student` join `class_level` on((`student`.`class_id` = `class_level`.`class_id`))) join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) where ((`student`.`stte_out` = 'expelled') and (`student`.`status` = 'inactive'));

-- --------------------------------------------------------

--
-- Structure for view `expired_book_view`
--
DROP TABLE IF EXISTS `expired_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expired_book_view` AS select `borrowed_report_view`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`borrowed_report_view`.`ID` AS `ID`,`borrowed_report_view`.`ISBN` AS `ISBN`,`borrowed_report_view`.`BORROWER_ID` AS `BORROWER_ID`,`borrowed_report_view`.`DATE_BORROWED` AS `DATE_BORROWED`,`borrowed_report_view`.`RETURN_DATE` AS `RETURN_DATE`,`borrowed_report_view`.`TIME_LEFT` AS `TIME_LEFT`,`borrowed_report_view`.`BOOK_TITLE` AS `BOOK_TITLE`,`borrowed_report_view`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`borrowed_report_view`.`EDITION` AS `EDITION`,`borrowed_report_view`.`NAMES` AS `NAMES`,`borrowed_report_view`.`FLAG` AS `FLAG` from `borrowed_report_view` where (`borrowed_report_view`.`TIME_LEFT` < 0) order by `borrowed_report_view`.`TIME_LEFT`;

-- --------------------------------------------------------

--
-- Structure for view `filtered_teaching_assignment`
--
DROP TABLE IF EXISTS `filtered_teaching_assignment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `filtered_teaching_assignment` AS select `teaching_assignment`.`assignment_id` AS `assignment_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`teaching_assignment`.`term_id` AS `term_id`,`teaching_assignment`.`filter_flag` AS `filter_flag`,`teaching_assignment`.`dept_id` AS `dept_id` from `teaching_assignment` where ((`teaching_assignment`.`term_id` = '2') and (`teaching_assignment`.`filter_flag` = 1));

-- --------------------------------------------------------

--
-- Structure for view `first_join_staff_attendance_view`
--
DROP TABLE IF EXISTS `first_join_staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `first_join_staff_attendance_view` AS select `staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff_attendance_record`.`staff_id` AS `staff_id`,`staff_attendance_record`.`date_` AS `date_`,`staff_attendance_record`.`day` AS `day`,`attendance`.`att_type` AS `at`,`attendance`.`description` AS `att_desc`,`audit_staff_attendance`.`change_type` AS `change_type`,`audit_staff_attendance`.`change_by` AS `change_by`,`audit_staff_attendance`.`ip_address` AS `ip_address`,`audit_staff_attendance`.`change_time` AS `change_time` from (((`staff_attendance_record` join `staff` on((`staff_attendance_record`.`staff_id` = `staff`.`staff_id`))) join `audit_staff_attendance` on((`staff_attendance_record`.`id` = `audit_staff_attendance`.`sar_id`))) join `attendance` on((`audit_staff_attendance`.`att_id` = `attendance`.`att_id`))) order by `staff_attendance_record`.`staff_id`,`audit_staff_attendance`.`change_time` desc;

-- --------------------------------------------------------

--
-- Structure for view `lost_book_view`
--
DROP TABLE IF EXISTS `lost_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lost_book_view` AS select distinct `book`.`isbn` AS `ISBN`,`book`.`ID` AS `ID`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME` from (`book` join `book_type`) where ((`book`.`isbn` = `book_type`.`ISBN`) and (`book`.`STATUS` = 'LOST'));

-- --------------------------------------------------------

--
-- Structure for view `my_announcements_view`
--
DROP TABLE IF EXISTS `my_announcements_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `my_announcements_view` AS select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `username` from ((`announcement` join `announcement_status` on((`announcement`.`announcement_id` = `announcement_status`.`announcement_id`))) join `staff` on((`staff`.`staff_id` = `announcement`.`posted_by`))) where (`announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where (`staff_group`.`staff_id` = 'STAFF011')) and (`announcement_status`.`staff_id` = 'STAFF011')) union select `announcement`.`group_id` AS `gid`,`announcement`.`announcement_id` AS `announcement_id`,`announcement`.`group_id` AS `group_id`,`announcement`.`heading` AS `heading`,`announcement`.`msg` AS `msg`,`announcement`.`time` AS `time`,`announcement`.`posted_by` AS `posted_by`,`announcement`.`status` AS `status`,`announcement_status`.`staff_id` AS `staff_id`,`announcement_status`.`is_read` AS `is_read`,`admin`.`username` AS `username` from ((`announcement` join `announcement_status` on((`announcement`.`announcement_id` = `announcement_status`.`announcement_id`))) join `admin` on((`admin`.`id` = `announcement`.`posted_by`))) where (`announcement`.`group_id` in (select `staff_group`.`group_id` from `staff_group` where (`staff_group`.`staff_id` = 'STAFF011')) and (`announcement_status`.`staff_id` = 'STAFF011'));

-- --------------------------------------------------------

--
-- Structure for view `out_book`
--
DROP TABLE IF EXISTS `out_book`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `out_book` AS select `book_loan_info`.`TRANSACTION_ID` AS `TRANSACTION_ID`,`book_loan_info`.`ID` AS `ID`,`book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`DATE_DUE_BACK` AS `DATE_DUE_BACK`,`book_loan_info`.`DATE_OVERDUE` AS `DATE_OVERDUE`,`book_loan_info`.`status_before` AS `STATUS_BEFORE`,`book_loan_info`.`status_after` AS `STATUS_AFTER`,`book_loan_info`.`BORROWER_ID` AS `BORROWER_ID`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`LAST_EDIT` AS `LAST_EDIT`,`book_loan_info`.`FLAG` AS `FLAG` from `book_loan_info` where (`book_loan_info`.`FLAG` = 'borrowed');

-- --------------------------------------------------------

--
-- Structure for view `remained_book_view`
--
DROP TABLE IF EXISTS `remained_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `remained_book_view` AS select `book`.`isbn` AS `isbn`,`book`.`ID` AS `ID`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book`.`STATUS` AS `STATUS` from `book` where (not((`book`.`ID`,`book`.`isbn`) in (select `book`.`ID`,`book`.`isbn` from `book` where (`book`.`STATUS` = 'lost'))));

-- --------------------------------------------------------

--
-- Structure for view `reminder`
--
DROP TABLE IF EXISTS `reminder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reminder` AS select `book`.`STATUS` AS `STATUS`,`book`.`LAST_EDIT` AS `LAST_EDIT`,`book`.`ID` AS `ID`,`book`.`isbn` AS `ISBN` from `book` where (not((`book`.`ID`,`book`.`isbn`) in (select `borrowed_report`.`ID`,`borrowed_report`.`ISBN` from `borrowed_report`)));

-- --------------------------------------------------------

--
-- Structure for view `remove_borrowed_view`
--
DROP TABLE IF EXISTS `remove_borrowed_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `remove_borrowed_view` AS select `reminder`.`ID` AS `ID`,`reminder`.`ISBN` AS `ISBN`,`reminder`.`STATUS` AS `STATUS`,`reminder`.`LAST_EDIT` AS `LAST_EDIT`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`EDITION` AS `EDITION` from (`book_type` join `reminder`) where (`reminder`.`ISBN` = `book_type`.`ISBN`) order by `reminder`.`LAST_EDIT` desc;

-- --------------------------------------------------------

--
-- Structure for view `returned_book_view`
--
DROP TABLE IF EXISTS `returned_book_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `returned_book_view` AS select `book_loan_info`.`ISBN` AS `ISBN`,`book_loan_info`.`ID` AS `ID`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,`book_loan_info`.`LIBRARIAN_ID` AS `LIBRARIAN_ID`,`book_loan_info`.`DATE_BORROWED` AS `DATE_BORROWED`,`book_loan_info`.`RETURN_DATE` AS `EXPECT_RETURN`,`book_loan_info`.`RETURN_DATE` AS `RETURN_DATE`,`book_loan_info`.`status_after` AS `STATUS_AFTER`,`borrowers`.`borrower_id` AS `BORROWER_ID`,`borrowers`.`borrower_description` AS `BORROWER_DESCRIPTION`,`book_loan_info`.`BORROWER_TYPE` AS `BORROWER_TYPE`,`book_loan_info`.`FLAG` AS `FLAG` from ((`book_type` join `book_loan_info`) join `borrowers`) where ((`book_loan_info`.`ISBN` = `book_type`.`ISBN`) and (`book_loan_info`.`BORROWER_ID` = `borrowers`.`borrower_id`)) order by `book_loan_info`.`DATE_DUE_BACK` desc;

-- --------------------------------------------------------

--
-- Structure for view `staff_activity_log_view`
--
DROP TABLE IF EXISTS `staff_activity_log_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_activity_log_view` AS select `activity_log`.`id` AS `id`,`activity_log`.`activity` AS `activity`,`activity_log`.`tableName` AS `tableName`,`activity_log`.`time` AS `time`,`activity_log`.`source` AS `source`,`activity_log`.`destination` AS `destination`,`activity_log`.`user_id` AS `user_id`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`user_role` AS `user_role`,`staff`.`last_log` AS `last_log` from (`activity_log` join `staff` on((`staff`.`staff_id` = `activity_log`.`user_id`)));

-- --------------------------------------------------------

--
-- Structure for view `staff_attendance_view`
--
DROP TABLE IF EXISTS `staff_attendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_attendance_view` AS select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names`,`staff`.`gender` AS `gender`,`attendance`.`att_type` AS `att_type`,`attendance`.`description` AS `description`,`staff_attendance_record`.`day` AS `day`,`staff_attendance_record`.`date_` AS `date_` from ((`staff` join `staff_attendance_record` on((`staff`.`staff_id` = `staff_attendance_record`.`staff_id`))) join `attendance` on((`staff_attendance_record`.`att_id` = `attendance`.`att_id`)));

-- --------------------------------------------------------

--
-- Structure for view `staff_group_members_view`
--
DROP TABLE IF EXISTS `staff_group_members_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_group_members_view` AS select `staff_group`.`group_id` AS `group_id`,`group_type`.`group_name` AS `group_name`,group_concat(concat(`staff`.`firstname`,' ',`staff`.`lastname`) separator ',  ') AS `group_members` from ((`staff_group` join `staff` on((`staff_group`.`staff_id` = `staff`.`staff_id`))) join `group_type` on((`group_type`.`group_id` = `staff_group`.`group_id`))) group by `staff_group`.`group_id`;

-- --------------------------------------------------------

--
-- Structure for view `staff_plus_nok_view`
--
DROP TABLE IF EXISTS `staff_plus_nok_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_plus_nok_view` AS select `staff`.`user_role` AS `user_role`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`last_log` AS `last_log`,`staff_nok`.`firstname` AS `nok_fn`,`staff_nok`.`lastname` AS `nok_ln`,`staff_nok`.`gender` AS `nok_gender`,`staff_nok`.`relation_type` AS `relation_type`,`staff_nok`.`email` AS `nok_email`,`staff_nok`.`phone_no` AS `nok_phone_no`,`staff_nok`.`p_box` AS `nok_box`,`staff_nok`.`p_region` AS `nok_region`,`staff_nok`.`current_box` AS `current_box`,`staff_nok`.`current_region` AS `current_region` from (`staff` left join `staff_nok` on((`staff_nok`.`staff_id` = `staff`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `staff_qualification_view`
--
DROP TABLE IF EXISTS `staff_qualification_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_qualification_view` AS select `qualification`.`q_id` AS `q_id`,`qualification`.`certified` AS `certified`,`staff`.`user_role` AS `user_role`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`staff`.`status` AS `status`,`staff`.`last_log` AS `last_log`,`qualification`.`certification` AS `certification`,`qualification`.`completion_date` AS `completion_date`,`qualification`.`description` AS `description`,`qualification`.`university` AS `university`,`staff_nok`.`firstname` AS `nok_fn`,`staff_nok`.`lastname` AS `nok_ln`,`staff_nok`.`gender` AS `nok_gender`,`staff_nok`.`relation_type` AS `relation_type`,`staff_nok`.`email` AS `nok_email`,`staff_nok`.`phone_no` AS `nok_phone_no`,`staff_nok`.`p_box` AS `nok_box`,`staff_nok`.`p_region` AS `nok_region`,`staff_nok`.`current_box` AS `current_box`,`staff_nok`.`current_region` AS `current_region` from ((`staff` left join `qualification` on((`staff`.`staff_id` = `qualification`.`staff_id`))) left join `staff_nok` on((`staff_nok`.`staff_id` = `staff`.`staff_id`))) where (`staff`.`status` = 'active') order by `qualification`.`completion_date` desc;

-- --------------------------------------------------------

--
-- Structure for view `staff_role_permission_view`
--
DROP TABLE IF EXISTS `staff_role_permission_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_role_permission_view` AS select `staff_role`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`phone_no` AS `phone_no`,`staff`.`status` AS `status`,`permission`.`permission_id` AS `permission_id`,`permission`.`description` AS `description`,`role_permission`.`role_type_id` AS `role_type_id`,`role_type`.`role_name` AS `role_name` from ((((`role_permission` join `permission` on((`permission`.`permission_id` = `role_permission`.`permission_id`))) join `role_type` on((`role_type`.`role_type_id` = `role_permission`.`role_type_id`))) join `staff_role` on((`role_type`.`role_type_id` = `staff_role`.`role_type_id`))) join `staff` on((`staff_role`.`staff_id` = `staff`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `students_by_class_and_subject_view`
--
DROP TABLE IF EXISTS `students_by_class_and_subject_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students_by_class_and_subject_view` AS select `class_level`.`level` AS `level`,`student`.`admission_no` AS `admission_no`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`class_stream`.`class_stream_id` AS `class_stream_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((`student` join `student_subject` on((`student`.`admission_no` = `student_subject`.`admission_no`))) join `subject` on((`student_subject`.`subject_id` = `subject`.`subject_id`))) join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`))) where (`student`.`status` = 'active');

-- --------------------------------------------------------

--
-- Structure for view `students_in_dormitory_view`
--
DROP TABLE IF EXISTS `students_in_dormitory_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students_in_dormitory_view` AS select `class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`status` AS `status`,`dormitory`.`dorm_id` AS `dorm_id` from (((`student` left join `dormitory` on((`student`.`dorm_id` = `dormitory`.`dorm_id`))) left join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `student`.`class_id`))) where (`student`.`status` = 'active') order by `class_level`.`class_id`;

-- --------------------------------------------------------

--
-- Structure for view `student_enrollment_academic_year_view`
--
DROP TABLE IF EXISTS `student_enrollment_academic_year_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_enrollment_academic_year_view` AS select `term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`term`.`is_current` AS `is_current`,`term`.`aid` AS `aid`,`academic_year`.`id` AS `id`,`academic_year`.`start_date` AS `start_date`,`academic_year`.`end_date` AS `end_date`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`academic_year`.`status` AS `status`,`enrollment`.`admission_no` AS `admission_no`,`enrollment`.`class_id` AS `class_id`,`enrollment`.`class_stream_id` AS `class_stream_id`,`enrollment`.`academic_year` AS `academic_year`,`class_level`.`class_name` AS `class_name` from (((`academic_year` join `term` on((`academic_year`.`id` = `term`.`aid`))) join `enrollment` on((`academic_year`.`id` = `enrollment`.`academic_year`))) join `class_level` on((`enrollment`.`class_id` = `class_level`.`class_id`))) order by `enrollment`.`class_id`,`term`.`term_name`;

-- --------------------------------------------------------

--
-- Structure for view `student_guardian_view`
--
DROP TABLE IF EXISTS `student_guardian_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_guardian_view` AS select `student`.`stte_out` AS `stte_out`,`student`.`disabled` AS `disabled`,`student`.`disability` AS `disability`,`student`.`stte` AS `stte`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`firstname` AS `student_firstname`,`student`.`lastname` AS `student_lastname`,`guardian`.`firstname` AS `g_firstname`,`guardian`.`lastname` AS `g_lastname`,`guardian`.`middlename` AS `g_middlename`,`student`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`student`.`date` AS `date`,`student`.`dob` AS `dob`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`status` AS `status`,concat(`guardian`.`firstname`,' ',`guardian`.`middlename`,' ',`guardian`.`lastname`) AS `guardian_names`,`guardian`.`gender` AS `gender`,`guardian`.`rel_type` AS `rel_type`,`guardian`.`email` AS `email`,`guardian`.`occupation` AS `occupation`,`guardian`.`phone_no` AS `phone_no`,`guardian`.`p_box` AS `p_box`,`guardian`.`p_region` AS `p_region` from (`student` left join `guardian` on((`student`.`admission_no` = `guardian`.`admission_no`)));

-- --------------------------------------------------------

--
-- Structure for view `student_progressive_results_view`
--
DROP TABLE IF EXISTS `student_progressive_results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_progressive_results_view` AS select `student_subject_score_position`.`subject_rank` AS `subject_rank`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((((`student_subject_score_position` join `student` on((`student_subject_score_position`.`admission_no` = `student`.`admission_no`))) join `class_level` on((`student_subject_score_position`.`class_id` = `class_level`.`class_id`))) join `class_stream` on((`class_stream`.`class_stream_id` = `student_subject_score_position`.`class_stream_id`))) join `term` on((`student_subject_score_position`.`term_id` = `term`.`term_id`))) join `subject` on((`student_subject_score_position`.`subject_id` = `subject`.`subject_id`))) join `academic_year` on((`academic_year`.`id` = `term`.`aid`)));

-- --------------------------------------------------------

--
-- Structure for view `student_result_view`
--
DROP TABLE IF EXISTS `student_result_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_result_view` AS select `student_subject_score_position`.`subject_rank` AS `subject_rank`,`academic_year`.`year` AS `year`,`academic_year`.`class_level` AS `class_level`,`term`.`term_id` AS `term_id`,`term`.`term_name` AS `term_name`,`student_subject_score_position`.`admission_no` AS `admission_no`,`student_subject_score_position`.`monthly_one` AS `monthly_one`,`student_subject_score_position`.`midterm` AS `midterm`,`student_subject_score_position`.`monthly_two` AS `monthly_two`,`student_subject_score_position`.`terminal` AS `terminal`,`student_subject_score_position`.`average` AS `average`,`student_subject_score_position`.`grade` AS `grade`,`student_subject_score_position`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`student`.`academic_year` AS `academic_year`,`term`.`aid` AS `aid`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student_subject_score_position`.`class_id` AS `class_id`,`student_subject_score_position`.`class_stream_id` AS `class_stream_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((((((`student_subject_score_position` join `student` on((`student_subject_score_position`.`admission_no` = `student`.`admission_no`))) join `class_level` on((`student_subject_score_position`.`class_id` = `class_level`.`class_id`))) join `class_stream` on((`class_stream`.`class_stream_id` = `student_subject_score_position`.`class_stream_id`))) join `term` on((`student_subject_score_position`.`term_id` = `term`.`term_id`))) join `subject` on((`student_subject_score_position`.`subject_id` = `subject`.`subject_id`))) join `academic_year` on((`academic_year`.`id` = `term`.`aid`)));

-- --------------------------------------------------------

--
-- Structure for view `student_transfer_in_view`
--
DROP TABLE IF EXISTS `student_transfer_in_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_transfer_in_view` AS select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`stte` AS `stte`,`student`.`status` AS `status`,`transferred_students`.`form` AS `form`,`transferred_students`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`transferred_students`.`school` AS `school`,`class_level`.`class_name` AS `class_name`,`transferred_students`.`date_of_transfer` AS `date_of_transfer`,`transferred_students`.`transfer_letter` AS `transfer_letter`,`transferred_students`.`self_form_receipt` AS `self_form_receipt`,`transferred_students`.`transfer_status` AS `transfer_status` from ((`transferred_students` join `student` on((`transferred_students`.`admission_no` = `student`.`admission_no`))) join `class_level` on((`class_level`.`class_id` = `transferred_students`.`form`))) where (`transferred_students`.`transfer_status` = 'IN');

-- --------------------------------------------------------

--
-- Structure for view `student_transfer_out_view`
--
DROP TABLE IF EXISTS `student_transfer_out_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_transfer_out_view` AS select `student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`stte_out` AS `stte_out`,`student`.`status` AS `status`,`transferred_students`.`form` AS `form`,`transferred_students`.`admission_no` AS `admission_no`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `student_names`,`transferred_students`.`school` AS `school`,`class_level`.`class_name` AS `class_name`,`transferred_students`.`date_of_transfer` AS `date_of_transfer`,`transferred_students`.`transfer_letter` AS `transfer_letter`,`transferred_students`.`self_form_receipt` AS `self_form_receipt`,`transferred_students`.`transfer_status` AS `transfer_status` from ((`transferred_students` join `student` on((`transferred_students`.`admission_no` = `student`.`admission_no`))) join `class_level` on((`class_level`.`class_id` = `transferred_students`.`form`))) where (`transferred_students`.`transfer_status` = 'OUT');

-- --------------------------------------------------------

--
-- Structure for view `subjects_subtopic_view`
--
DROP TABLE IF EXISTS `subjects_subtopic_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subjects_subtopic_view` AS select `sub_topic`.`class_stream_id` AS `class_stream_id`,`sub_topic`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`sub_topic`.`subtopic_id` AS `subtopic_id`,`sub_topic`.`subtopic_name` AS `subtopic_name`,`topic`.`topic_name` AS `topic_name`,`sub_topic`.`topic_id` AS `topic_id` from ((`sub_topic` join `topic` on((`sub_topic`.`topic_id` = `topic`.`topic_id`))) join `subject` on((`subject`.`subject_id` = `sub_topic`.`subject_id`)));

-- --------------------------------------------------------

--
-- Structure for view `subject_department_view`
--
DROP TABLE IF EXISTS `subject_department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subject_department_view` AS select `department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `staff_id`,`department`.`dept_type` AS `dept_type`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`subject`.`subject_type` AS `subject_type` from (`subject` join `department` on((`subject`.`dept_id` = `department`.`dept_id`)));

-- --------------------------------------------------------

--
-- Structure for view `subject_teacher_view`
--
DROP TABLE IF EXISTS `subject_teacher_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subject_teacher_view` AS select `staff`.`staff_id` AS `staff_id`,`subject_teacher`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`user_role` AS `user_role`,`staff`.`staff_type` AS `staff_type` from ((`subject` join `subject_teacher` on((`subject_teacher`.`subject_id` = `subject`.`subject_id`))) join `staff` on((`subject_teacher`.`teacher_id` = `staff`.`staff_id`)));

-- --------------------------------------------------------

--
-- Structure for view `suspended_students_view`
--
DROP TABLE IF EXISTS `suspended_students_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `suspended_students_view` AS select `student`.`admission_no` AS `admission_no`,`student`.`date` AS `date`,`student`.`academic_year` AS `academic_year`,`student`.`firstname` AS `firstname`,`student`.`lastname` AS `lastname`,`student`.`class_id` AS `class_id`,`student`.`class_stream_id` AS `class_stream_id`,`student`.`dob` AS `dob`,`student`.`tribe_id` AS `tribe_id`,`student`.`religion_id` AS `religion_id`,`student`.`home_address` AS `home_address`,`student`.`region` AS `region`,`student`.`former_school` AS `former_school`,`student`.`dorm_id` AS `dorm_id`,`student`.`status` AS `status`,`student`.`stte_out` AS `stte_out`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream` from ((`student` join `class_level` on((`student`.`class_id` = `class_level`.`class_id`))) join `class_stream` on((`student`.`class_stream_id` = `class_stream`.`class_stream_id`))) where ((`student`.`stte_out` = 'suspended') and (`student`.`status` = 'active'));

-- --------------------------------------------------------

--
-- Structure for view `teachers_timetable_view`
--
DROP TABLE IF EXISTS `teachers_timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teachers_timetable_view` AS select `timetable`.`weekday` AS `weekday`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`subject`.`subject_name` AS `subject_name`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`teaching_assignment`.`teacher_id` AS `teacher_id` from (((((`timetable` join `teaching_assignment` on((`timetable`.`assignment_id` = `teaching_assignment`.`assignment_id`))) join `period` on((`timetable`.`period_no` = `period`.`period_no`))) join `subject` on((`subject`.`subject_id` = `teaching_assignment`.`subject_id`))) join `class_stream` on((`teaching_assignment`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_level`.`class_id` = `class_stream`.`class_id`)));

-- --------------------------------------------------------

--
-- Structure for view `teaching_assignment_department_view`
--
DROP TABLE IF EXISTS `teaching_assignment_department_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teaching_assignment_department_view` AS select `subject`.`subject_name` AS `subject_name`,`teaching_assignment`.`term_id` AS `term_id`,`class_level`.`class_id` AS `class_id`,`class_level`.`class_name` AS `class_name`,`class_stream`.`stream` AS `stream`,`staff`.`firstname` AS `firstname`,`staff`.`lastname` AS `lastname`,`staff`.`gender` AS `gender`,`staff`.`staff_id` AS `staff_id`,`department`.`dept_id` AS `dept_id`,`department`.`dept_name` AS `dept_name`,`department`.`dept_loc` AS `dept_loc`,`department`.`staff_id` AS `hod_id`,`department`.`dept_type` AS `dept_type`,`teaching_assignment`.`assignment_id` AS `assignment_id`,`teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`subject_id` AS `subject_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`teaching_assignment`.`filter_flag` AS `filter_flag` from ((((((`teaching_assignment` join `staff` on((`teaching_assignment`.`teacher_id` = `staff`.`staff_id`))) join `staff_department` on((`staff`.`staff_id` = `staff_department`.`staff_id`))) join `department` on((`staff_department`.`dept_id` = `department`.`dept_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) join `class_stream` on((`teaching_assignment`.`class_stream_id` = `class_stream`.`class_stream_id`))) join `class_level` on((`class_stream`.`class_id` = `class_level`.`class_id`))) where `teaching_assignment`.`term_id` in (select `term`.`term_id` from `term` where (`term`.`is_current` = 'yes'));

-- --------------------------------------------------------

--
-- Structure for view `timetable_view`
--
DROP TABLE IF EXISTS `timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `timetable_view` AS select `teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,substr(`teaching_assignment`.`class_stream_id`,3) AS `csid`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`timetable`.`assignment_id` AS `assignment_id`,`timetable`.`weekday` AS `weekday`,`timetable`.`t_id` AS `t_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`period`.`duration` AS `duration` from ((((`timetable` join `period` on((`timetable`.`period_no` = `period`.`period_no`))) join `teaching_assignment` on((`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) join `staff` on((`staff`.`staff_id` = `teaching_assignment`.`teacher_id`)));

-- --------------------------------------------------------

--
-- Structure for view `title_sum_up_view`
--
DROP TABLE IF EXISTS `title_sum_up_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `title_sum_up_view` AS select distinct `book_type`.`ISBN` AS `ISBN`,`book_type`.`BOOK_TITLE` AS `BOOK_TITLE`,`book_type`.`AUTHOR_NAME` AS `AUTHOR_NAME`,(select count(0) from `book` where (`book_type`.`ISBN` = `book`.`isbn`)) AS `COPIES` from `book_type`;

-- --------------------------------------------------------

--
-- Structure for view `used_to_edit_timetable_view`
--
DROP TABLE IF EXISTS `used_to_edit_timetable_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `used_to_edit_timetable_view` AS select `teaching_assignment`.`teacher_id` AS `teacher_id`,`teaching_assignment`.`class_stream_id` AS `class_stream_id`,`teaching_assignment`.`start_date` AS `start_date`,`teaching_assignment`.`end_date` AS `end_date`,`staff`.`staff_id` AS `staff_id`,`staff`.`firstname` AS `firstname`,`staff`.`middlename` AS `middlename`,`staff`.`lastname` AS `lastname`,`staff`.`dob` AS `dob`,`staff`.`marital_status` AS `marital_status`,`staff`.`gender` AS `gender`,`staff`.`email` AS `email`,`staff`.`phone_no` AS `phone_no`,`staff`.`password` AS `password`,`staff`.`staff_type` AS `staff_type`,`timetable`.`assignment_id` AS `assignment_id`,`timetable`.`weekday` AS `weekday`,`timetable`.`t_id` AS `t_id`,`subject`.`subject_id` AS `subject_id`,`subject`.`subject_name` AS `subject_name`,`subject`.`description` AS `description`,`subject`.`dept_id` AS `dept_id`,`subject`.`subject_category` AS `subject_category`,`subject`.`subject_choice` AS `subject_choice`,`period`.`period_no` AS `period_no`,`period`.`start_time` AS `start_time`,`period`.`end_time` AS `end_time`,`period`.`duration` AS `duration` from ((((`timetable` join `period` on((`timetable`.`period_no` = `period`.`period_no`))) join `teaching_assignment` on((`teaching_assignment`.`assignment_id` = `timetable`.`assignment_id`))) join `subject` on((`teaching_assignment`.`subject_id` = `subject`.`subject_id`))) join `staff` on((`staff`.`staff_id` = `teaching_assignment`.`teacher_id`)));

-- --------------------------------------------------------

--
-- Structure for view `users`
--
DROP TABLE IF EXISTS `users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users` AS select `student`.`admission_no` AS `id`,concat(`student`.`firstname`,' ',`student`.`lastname`) AS `names` from `student` union select `department`.`dept_id` AS `id`,`department`.`dept_name` AS `names` from `department` union select `staff`.`staff_id` AS `staff_id`,concat(`staff`.`firstname`,' ',`staff`.`lastname`) AS `names` from `staff`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year`
--
ALTER TABLE `academic_year`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `year` (`year`);

--
-- Indexes for table `accomodation_history`
--
ALTER TABLE `accomodation_history`
 ADD KEY `fk_ah_dorm` (`dorm_id`), ADD KEY `fk_ah_admission_no` (`admission_no`);

--
-- Indexes for table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`account_no`);

--
-- Indexes for table `accountant`
--
ALTER TABLE `accountant`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
 ADD PRIMARY KEY (`announcement_id`), ADD KEY `fk_group_announcement` (`group_id`);

--
-- Indexes for table `announcement_status`
--
ALTER TABLE `announcement_status`
 ADD UNIQUE KEY `staff_id` (`staff_id`,`group_id`,`announcement_id`), ADD KEY `fk_announcement` (`announcement_id`), ADD KEY `fk_group_announce` (`group_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
 ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `audit_password_reset_tbl`
--
ALTER TABLE `audit_password_reset_tbl`
 ADD PRIMARY KEY (`staff_id`), ADD KEY `fk_reset_by` (`reset_by`);

--
-- Indexes for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_audit_csid` (`class_stream_id`);

--
-- Indexes for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_audit_staff_att` (`att_id`), ADD KEY `fk_audit_staff_att1` (`sar_id`);

--
-- Indexes for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_att_type` (`att_id`), ADD KEY `fk_audit_student_attendance` (`sar_id`);

--
-- Indexes for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_audit_results` (`ssa_id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
 ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `a_level_grade_system_tbl`
--
ALTER TABLE `a_level_grade_system_tbl`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `grade` (`grade`), ADD UNIQUE KEY `grade_2` (`grade`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`isbn`,`ID`);

--
-- Indexes for table `books_borrowing_limit`
--
ALTER TABLE `books_borrowing_limit`
 ADD PRIMARY KEY (`blid`), ADD UNIQUE KEY `b_type` (`b_type`);

--
-- Indexes for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
 ADD PRIMARY KEY (`TRANSACTION_ID`), ADD KEY `fk_book` (`ISBN`,`ID`), ADD KEY `fk_library` (`LIBRARIAN_ID`);

--
-- Indexes for table `book_type`
--
ALTER TABLE `book_type`
 ADD PRIMARY KEY (`ISBN`), ADD KEY `CLASS_ID` (`CLASS_ID`), ADD KEY `FK_booktype_aid` (`A_ID`);

--
-- Indexes for table `borrower_type`
--
ALTER TABLE `borrower_type`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `break_time`
--
ALTER TABLE `break_time`
 ADD PRIMARY KEY (`bid`), ADD KEY `fk_brk` (`after_period`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`id`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `class_asset`
--
ALTER TABLE `class_asset`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `asset_no` (`asset_no`), ADD KEY `fk_class_asset` (`class_stream_id`);

--
-- Indexes for table `class_level`
--
ALTER TABLE `class_level`
 ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `class_stream`
--
ALTER TABLE `class_stream`
 ADD PRIMARY KEY (`class_stream_id`), ADD UNIQUE KEY `class_id` (`class_id`,`stream`,`stream_id`), ADD KEY `fk_cteacher` (`teacher_id`), ADD KEY `fk_monitor` (`admission_no`), ADD KEY `fk_str` (`stream_id`), ADD KEY `fk_stream_name` (`stream`);

--
-- Indexes for table `class_stream_subject`
--
ALTER TABLE `class_stream_subject`
 ADD PRIMARY KEY (`class_stream_id`,`subject_id`), ADD KEY `fk_css_subject` (`subject_id`);

--
-- Indexes for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_cth_tid` (`teacher_id`), ADD KEY `fk_cth_csid` (`class_stream_id`);

--
-- Indexes for table `compute_division`
--
ALTER TABLE `compute_division`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `level` (`level`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`dept_id`), ADD UNIQUE KEY `dept_name` (`dept_name`), ADD KEY `fk_hods` (`staff_id`);

--
-- Indexes for table `discipline`
--
ALTER TABLE `discipline`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `level` (`level`,`starting_points`,`ending_points`,`div_name`), ADD UNIQUE KEY `level_2` (`level`,`div_name`);

--
-- Indexes for table `dormitory`
--
ALTER TABLE `dormitory`
 ADD PRIMARY KEY (`dorm_id`), ADD KEY `fk_d_ms` (`teacher_id`), ADD KEY `fk_dp` (`admission_no`);

--
-- Indexes for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `asset_no` (`asset_no`), ADD KEY `fk_dorm_asset` (`dorm_id`);

--
-- Indexes for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_dmh_dorm` (`dorm_id`), ADD KEY `fk_dmh_tid` (`teacher_id`);

--
-- Indexes for table `duty_roaster`
--
ALTER TABLE `duty_roaster`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `start_date` (`start_date`), ADD UNIQUE KEY `end_date` (`end_date`);

--
-- Indexes for table `enrollment`
--
ALTER TABLE `enrollment`
 ADD PRIMARY KEY (`admission_no`,`class_id`,`class_stream_id`,`academic_year`), ADD KEY `fk_year_enrollment` (`academic_year`), ADD KEY `fk_ecid` (`class_id`);

--
-- Indexes for table `exam_type`
--
ALTER TABLE `exam_type`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_academic_year` (`academic_year`), ADD KEY `fk_et_alvl` (`a_level_academic_year`);

--
-- Indexes for table `fee`
--
ALTER TABLE `fee`
 ADD PRIMARY KEY (`fee_id`);

--
-- Indexes for table `fee_details`
--
ALTER TABLE `fee_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_type`
--
ALTER TABLE `group_type`
 ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `guardian`
--
ALTER TABLE `guardian`
 ADD PRIMARY KEY (`admission_no`,`firstname`,`lastname`), ADD UNIQUE KEY `phone_no` (`phone_no`);

--
-- Indexes for table `hod_history`
--
ALTER TABLE `hod_history`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hodh_staff` (`staff_id`), ADD KEY `fk_hodh_department` (`dept_id`);

--
-- Indexes for table `inactive_staff`
--
ALTER TABLE `inactive_staff`
 ADD PRIMARY KEY (`staff_id`,`deactivated_date`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
 ADD PRIMARY KEY (`s_no`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `lock_account_tbl`
--
ALTER TABLE `lock_account_tbl`
 ADD KEY `fk_lock_account` (`username`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
 ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
 ADD PRIMARY KEY (`month_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
 ADD PRIMARY KEY (`notification_id`), ADD UNIQUE KEY `staff_id` (`staff_id`,`msg_id`);

--
-- Indexes for table `o_level_grade_system_tbl`
--
ALTER TABLE `o_level_grade_system_tbl`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `grade` (`grade`), ADD UNIQUE KEY `grade_2` (`grade`);

--
-- Indexes for table `payee`
--
ALTER TABLE `payee`
 ADD PRIMARY KEY (`s_no`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
 ADD PRIMARY KEY (`period_no`), ADD UNIQUE KEY `start_time` (`start_time`), ADD UNIQUE KEY `end_time` (`end_time`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
 ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_pr_aid` (`academic_year`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
 ADD PRIMARY KEY (`q_id`), ADD KEY `fk_qualification` (`staff_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
 ADD PRIMARY KEY (`region_id`), ADD UNIQUE KEY `region_name` (`region_name`);

--
-- Indexes for table `religion`
--
ALTER TABLE `religion`
 ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
 ADD PRIMARY KEY (`role_type_id`,`module_id`), ADD KEY `fk_mod_id` (`module_id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
 ADD PRIMARY KEY (`role_type_id`,`permission_id`), ADD KEY `fk_rp_p` (`permission_id`);

--
-- Indexes for table `role_permission_dummy_table`
--
ALTER TABLE `role_permission_dummy_table`
 ADD KEY `fk_rpdt_role` (`role_type_id`), ADD KEY `fk_rpdt_p` (`permission_id`);

--
-- Indexes for table `role_type`
--
ALTER TABLE `role_type`
 ADD PRIMARY KEY (`role_type_id`);

--
-- Indexes for table `secretary`
--
ALTER TABLE `secretary`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `selected_students`
--
ALTER TABLE `selected_students`
 ADD PRIMARY KEY (`examination_no`), ADD UNIQUE KEY `tel_no` (`tel_no`), ADD UNIQUE KEY `email` (`email`), ADD KEY `fk_ss_aid` (`academic_year`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
 ADD PRIMARY KEY (`staff_id`), ADD UNIQUE KEY `phone_no` (`phone_no`);

--
-- Indexes for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `staff_id` (`staff_id`,`date_`), ADD KEY `fk_staff_att_rec` (`att_id`), ADD KEY `fk_staff_att_aid` (`aid`);

--
-- Indexes for table `staff_department`
--
ALTER TABLE `staff_department`
 ADD PRIMARY KEY (`staff_id`,`dept_id`), ADD KEY `fk_department_staff` (`dept_id`);

--
-- Indexes for table `staff_group`
--
ALTER TABLE `staff_group`
 ADD PRIMARY KEY (`staff_id`,`group_id`), ADD KEY `fk_group_id` (`group_id`);

--
-- Indexes for table `staff_nok`
--
ALTER TABLE `staff_nok`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `staff_role`
--
ALTER TABLE `staff_role`
 ADD PRIMARY KEY (`role_type_id`,`staff_id`), ADD KEY `fk_st_role` (`staff_id`);

--
-- Indexes for table `stream`
--
ALTER TABLE `stream`
 ADD PRIMARY KEY (`stream_id`), ADD UNIQUE KEY `stream_name` (`stream_name`), ADD UNIQUE KEY `stream_name_2` (`stream_name`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`admission_no`), ADD UNIQUE KEY `image` (`image`), ADD KEY `fk_student_class_id` (`class_id`), ADD KEY `fk_student_class_stream_id` (`class_stream_id`), ADD KEY `fk_student_dormitory_id` (`dorm_id`), ADD KEY `fk_student_academic_year_id` (`academic_year`), ADD KEY `fk_tr` (`tribe_id`), ADD KEY `fk_rel` (`religion_id`);

--
-- Indexes for table `student_assessment`
--
ALTER TABLE `student_assessment`
 ADD PRIMARY KEY (`admission_no`,`class_id`,`class_stream_id`,`term_id`), ADD KEY `fk_c` (`class_id`), ADD KEY `fk_cs` (`class_stream_id`), ADD KEY `fk_t` (`term_id`);

--
-- Indexes for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admission_no` (`admission_no`,`class_stream_id`,`date_`), ADD KEY `fk_att_csid` (`class_stream_id`), ADD KEY `fk_std_att_rec` (`att_id`), ADD KEY `fk_term_att` (`term_id`);

--
-- Indexes for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admission_no` (`admission_no`,`dorm_id`,`date_`), ADD KEY `fk_atid` (`att_id`), ADD KEY `fk_dorm_roll_call` (`dorm_id`);

--
-- Indexes for table `student_subject`
--
ALTER TABLE `student_subject`
 ADD PRIMARY KEY (`admission_no`,`subject_id`), ADD KEY `fk_student_subject` (`admission_no`), ADD KEY `class_stream_id` (`class_stream_id`,`subject_id`);

--
-- Indexes for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
 ADD PRIMARY KEY (`ssa_id`), ADD UNIQUE KEY `admission_no` (`admission_no`,`subject_id`,`etype_id`,`class_stream_id`,`term_id`), ADD KEY `fk_ssa_cs` (`class_stream_id`), ADD KEY `fk_ssa_s` (`subject_id`), ADD KEY `fk_etype` (`etype_id`), ADD KEY `fk_tssa` (`term_id`);

--
-- Indexes for table `student_subject_score_position`
--
ALTER TABLE `student_subject_score_position`
 ADD PRIMARY KEY (`admission_no`,`subject_id`,`class_stream_id`,`term_id`), ADD KEY `fk_tssp` (`term_id`), ADD KEY `fk_cssssp` (`class_stream_id`), ADD KEY `fk_csssp` (`class_id`), ADD KEY `fk_tssub` (`subject_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`subject_id`), ADD UNIQUE KEY `subject_name` (`subject_name`), ADD KEY `fk_subject_department` (`dept_id`);

--
-- Indexes for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
 ADD PRIMARY KEY (`subject_id`,`teacher_id`), ADD KEY `fk_subject_teacher` (`teacher_id`);

--
-- Indexes for table `sub_topic`
--
ALTER TABLE `sub_topic`
 ADD PRIMARY KEY (`subtopic_id`), ADD KEY `fk_subtopic_subject` (`subject_id`), ADD KEY `fk_subtopic_csid` (`class_stream_id`), ADD KEY `fk_subtopic_topic` (`topic_id`);

--
-- Indexes for table `super_admin_module`
--
ALTER TABLE `super_admin_module`
 ADD PRIMARY KEY (`id`,`module_id`), ADD KEY `fk_am` (`module_id`);

--
-- Indexes for table `super_admin_permission`
--
ALTER TABLE `super_admin_permission`
 ADD PRIMARY KEY (`id`,`permission_id`), ADD KEY `fk_admin_permission` (`permission_id`);

--
-- Indexes for table `suspended_students`
--
ALTER TABLE `suspended_students`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_suspension` (`admission_no`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `teacher_duty_roaster`
--
ALTER TABLE `teacher_duty_roaster`
 ADD PRIMARY KEY (`teacher_id`,`did`), ADD KEY `fk_tod_roaster` (`did`);

--
-- Indexes for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
 ADD PRIMARY KEY (`assignment_id`), ADD UNIQUE KEY `teacher_id` (`teacher_id`,`subject_id`,`class_stream_id`), ADD UNIQUE KEY `subject_id` (`subject_id`,`class_stream_id`), ADD KEY `fk_ta_csid` (`class_stream_id`), ADD KEY `fk_ta_term` (`term_id`), ADD KEY `fk_ta_department` (`dept_id`);

--
-- Indexes for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
 ADD PRIMARY KEY (`ta_id`), ADD UNIQUE KEY `unique_ta_record` (`period_no`,`date`,`class_stream_id`), ADD KEY `fk_journal_subtopic` (`subTopicID`), ADD KEY `fk_tatt_csid` (`class_stream_id`);

--
-- Indexes for table `teaching_log`
--
ALTER TABLE `teaching_log`
 ADD PRIMARY KEY (`log_id`), ADD UNIQUE KEY `subject_id` (`subject_id`,`topic_id`,`class_stream_id`,`term_id`), ADD KEY `fk_tlog_csid` (`class_stream_id`), ADD KEY `fk_tlog_term` (`term_id`), ADD KEY `fk_tlog_teacher` (`teacher_id`), ADD KEY `fk_tlog_topic` (`topic_id`), ADD KEY `fk_log_dept` (`dept_id`);

--
-- Indexes for table `term`
--
ALTER TABLE `term`
 ADD PRIMARY KEY (`term_id`), ADD KEY `fk_aid` (`aid`);

--
-- Indexes for table `timetable`
--
ALTER TABLE `timetable`
 ADD PRIMARY KEY (`t_id`), ADD UNIQUE KEY `check_timetable_unique` (`period_no`,`teacher_id`,`weekday`), ADD KEY `fk_tt_csid` (`class_stream_id`);

--
-- Indexes for table `tod_daily_routine_tbl`
--
ALTER TABLE `tod_daily_routine_tbl`
 ADD PRIMARY KEY (`date_`), ADD KEY `fk_duty_roaster` (`did`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
 ADD PRIMARY KEY (`topic_id`), ADD KEY `fk_topic_subject` (`subject_id`), ADD KEY `fk_topic_csid` (`class_stream_id`);

--
-- Indexes for table `transferred_students`
--
ALTER TABLE `transferred_students`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_transfer_out` (`admission_no`);

--
-- Indexes for table `tribe`
--
ALTER TABLE `tribe`
 ADD PRIMARY KEY (`tribe_id`);

--
-- Indexes for table `waliomaliza_tbl`
--
ALTER TABLE `waliomaliza_tbl`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ly` (`completion_year`), ADD KEY `fk_completed_students` (`admission_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year`
--
ALTER TABLE `academic_year`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=309;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
MODIFY `att_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
MODIFY `author_id` int(3) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `a_level_grade_system_tbl`
--
ALTER TABLE `a_level_grade_system_tbl`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `books_borrowing_limit`
--
ALTER TABLE `books_borrowing_limit`
MODIFY `blid` smallint(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
MODIFY `TRANSACTION_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `borrower_type`
--
ALTER TABLE `borrower_type`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `break_time`
--
ALTER TABLE `break_time`
MODIFY `bid` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `class_asset`
--
ALTER TABLE `class_asset`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `compute_division`
--
ALTER TABLE `compute_division`
MODIFY `id` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `dept_id` int(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `discipline`
--
ALTER TABLE `discipline`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `dormitory`
--
ALTER TABLE `dormitory`
MODIFY `dorm_id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `duty_roaster`
--
ALTER TABLE `duty_roaster`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `fee_details`
--
ALTER TABLE `fee_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `group_type`
--
ALTER TABLE `group_type`
MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hod_history`
--
ALTER TABLE `hod_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
MODIFY `s_no` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
MODIFY `module_id` int(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `o_level_grade_system_tbl`
--
ALTER TABLE `o_level_grade_system_tbl`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `payee`
--
ALTER TABLE `payee`
MODIFY `s_no` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
MODIFY `permission_id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
MODIFY `region_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `religion`
--
ALTER TABLE `religion`
MODIFY `religion_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_type`
--
ALTER TABLE `role_type`
MODIFY `role_type_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `stream`
--
ALTER TABLE `stream`
MODIFY `stream_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
MODIFY `ssa_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_topic`
--
ALTER TABLE `sub_topic`
MODIFY `subtopic_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suspended_students`
--
ALTER TABLE `suspended_students`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
MODIFY `assignment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
MODIFY `ta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teaching_log`
--
ALTER TABLE `teaching_log`
MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `term`
--
ALTER TABLE `term`
MODIFY `term_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `timetable`
--
ALTER TABLE `timetable`
MODIFY `t_id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `transferred_students`
--
ALTER TABLE `transferred_students`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tribe`
--
ALTER TABLE `tribe`
MODIFY `tribe_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `waliomaliza_tbl`
--
ALTER TABLE `waliomaliza_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `accomodation_history`
--
ALTER TABLE `accomodation_history`
ADD CONSTRAINT `fk_ah_admission_no` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ah_dorm` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE;

--
-- Constraints for table `accountant`
--
ALTER TABLE `accountant`
ADD CONSTRAINT `fk_accountant` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
ADD CONSTRAINT `fk_group_announcement` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`);

--
-- Constraints for table `announcement_status`
--
ALTER TABLE `announcement_status`
ADD CONSTRAINT `fk_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`announcement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_group_announce` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `audit_password_reset_tbl`
--
ALTER TABLE `audit_password_reset_tbl`
ADD CONSTRAINT `fk_reset_by` FOREIGN KEY (`reset_by`) REFERENCES `admin` (`id`);

--
-- Constraints for table `audit_register_student_tbl`
--
ALTER TABLE `audit_register_student_tbl`
ADD CONSTRAINT `fk_audit_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE;

--
-- Constraints for table `audit_staff_attendance`
--
ALTER TABLE `audit_staff_attendance`
ADD CONSTRAINT `fk_audit_staff_att` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
ADD CONSTRAINT `fk_audit_staff_att1` FOREIGN KEY (`sar_id`) REFERENCES `staff_attendance_record` (`id`);

--
-- Constraints for table `audit_student_attendance`
--
ALTER TABLE `audit_student_attendance`
ADD CONSTRAINT `fk_att_type` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
ADD CONSTRAINT `fk_audit_student_attendance` FOREIGN KEY (`sar_id`) REFERENCES `student_attendance_record` (`id`);

--
-- Constraints for table `audit_student_subject_assessment`
--
ALTER TABLE `audit_student_subject_assessment`
ADD CONSTRAINT `fk_audit_results` FOREIGN KEY (`ssa_id`) REFERENCES `student_subject_assessment` (`ssa_id`);

--
-- Constraints for table `book`
--
ALTER TABLE `book`
ADD CONSTRAINT `fk_book_type` FOREIGN KEY (`isbn`) REFERENCES `book_type` (`ISBN`);

--
-- Constraints for table `book_loan_info`
--
ALTER TABLE `book_loan_info`
ADD CONSTRAINT `fk_book` FOREIGN KEY (`ISBN`, `ID`) REFERENCES `book` (`isbn`, `ID`),
ADD CONSTRAINT `fk_library` FOREIGN KEY (`LIBRARIAN_ID`) REFERENCES `librarian` (`staff_id`);

--
-- Constraints for table `break_time`
--
ALTER TABLE `break_time`
ADD CONSTRAINT `fk_brk` FOREIGN KEY (`after_period`) REFERENCES `period` (`period_no`);

--
-- Constraints for table `class_asset`
--
ALTER TABLE `class_asset`
ADD CONSTRAINT `fk_class_asset` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE;

--
-- Constraints for table `class_stream`
--
ALTER TABLE `class_stream`
ADD CONSTRAINT `fk_cteacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_monitor` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_str` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stream_name` FOREIGN KEY (`stream`) REFERENCES `stream` (`stream_name`) ON UPDATE CASCADE;

--
-- Constraints for table `class_stream_subject`
--
ALTER TABLE `class_stream_subject`
ADD CONSTRAINT `fk_css_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_css_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `class_teacher_history`
--
ALTER TABLE `class_teacher_history`
ADD CONSTRAINT `fk_cth_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cth_tid` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `department`
--
ALTER TABLE `department`
ADD CONSTRAINT `fk_hods` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `dormitory`
--
ALTER TABLE `dormitory`
ADD CONSTRAINT `fk_d_ms` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_dp` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE;

--
-- Constraints for table `dorm_asset`
--
ALTER TABLE `dorm_asset`
ADD CONSTRAINT `fk_dorm_asset` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE;

--
-- Constraints for table `dorm_master_history`
--
ALTER TABLE `dorm_master_history`
ADD CONSTRAINT `fk_dmh_dorm` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_dmh_tid` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `enrollment`
--
ALTER TABLE `enrollment`
ADD CONSTRAINT `fk_ecid` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`),
ADD CONSTRAINT `fk_student_enrollment` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
ADD CONSTRAINT `fk_year_enrollment` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`);

--
-- Constraints for table `exam_type`
--
ALTER TABLE `exam_type`
ADD CONSTRAINT `fk_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_et_alvl` FOREIGN KEY (`a_level_academic_year`) REFERENCES `academic_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `guardian`
--
ALTER TABLE `guardian`
ADD CONSTRAINT `fk_std_guardian` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`);

--
-- Constraints for table `hod_history`
--
ALTER TABLE `hod_history`
ADD CONSTRAINT `fk_hodh_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`),
ADD CONSTRAINT `fk_hodh_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `inactive_staff`
--
ALTER TABLE `inactive_staff`
ADD CONSTRAINT `fk_inactive_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `librarian`
--
ALTER TABLE `librarian`
ADD CONSTRAINT `fk_librarian` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lock_account_tbl`
--
ALTER TABLE `lock_account_tbl`
ADD CONSTRAINT `fk_lock_account` FOREIGN KEY (`username`) REFERENCES `staff` (`phone_no`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
ADD CONSTRAINT `fk_staff_notification` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `promote_student_audit_table`
--
ALTER TABLE `promote_student_audit_table`
ADD CONSTRAINT `fk_pr_aid` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `qualification`
--
ALTER TABLE `qualification`
ADD CONSTRAINT `fk_qualification` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
ADD CONSTRAINT `fk_mod_id` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_rl_mod` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
ADD CONSTRAINT `fk_rp_p` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_rp_role` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permission_dummy_table`
--
ALTER TABLE `role_permission_dummy_table`
ADD CONSTRAINT `fk_rpdt_p` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_rpdt_role` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `secretary`
--
ALTER TABLE `secretary`
ADD CONSTRAINT `fk_secretary` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `selected_students`
--
ALTER TABLE `selected_students`
ADD CONSTRAINT `fk_ss_aid` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`);

--
-- Constraints for table `staff_attendance_record`
--
ALTER TABLE `staff_attendance_record`
ADD CONSTRAINT `fk_staff_att` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
ADD CONSTRAINT `fk_staff_att_aid` FOREIGN KEY (`aid`) REFERENCES `academic_year` (`id`),
ADD CONSTRAINT `fk_staff_att_rec` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`);

--
-- Constraints for table `staff_department`
--
ALTER TABLE `staff_department`
ADD CONSTRAINT `fk_department_staff` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_staff_department` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_group`
--
ALTER TABLE `staff_group`
ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `group_type` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_staff_group` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_nok`
--
ALTER TABLE `staff_nok`
ADD CONSTRAINT `fk_staff_nok` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff_role`
--
ALTER TABLE `staff_role`
ADD CONSTRAINT `fk_rtid` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`role_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_st_role` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
ADD CONSTRAINT `fk_clid` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`),
ADD CONSTRAINT `fk_rel` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`religion_id`),
ADD CONSTRAINT `fk_std_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_student_academic_year_id` FOREIGN KEY (`academic_year`) REFERENCES `academic_year` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_student_dormitory_id` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tr` FOREIGN KEY (`tribe_id`) REFERENCES `tribe` (`tribe_id`);

--
-- Constraints for table `student_assessment`
--
ALTER TABLE `student_assessment`
ADD CONSTRAINT `fk_c` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cs` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_sa` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_t` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_attendance_record`
--
ALTER TABLE `student_attendance_record`
ADD CONSTRAINT `fk_adm_no_std_att` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
ADD CONSTRAINT `fk_att_adm_no` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
ADD CONSTRAINT `fk_att_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`),
ADD CONSTRAINT `fk_std_att_rec` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
ADD CONSTRAINT `fk_term_att` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`);

--
-- Constraints for table `student_roll_call_record`
--
ALTER TABLE `student_roll_call_record`
ADD CONSTRAINT `fk_adm_no_roll_call` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`),
ADD CONSTRAINT `fk_atid` FOREIGN KEY (`att_id`) REFERENCES `attendance` (`att_id`),
ADD CONSTRAINT `fk_dorm_roll_call` FOREIGN KEY (`dorm_id`) REFERENCES `dormitory` (`dorm_id`);

--
-- Constraints for table `student_subject`
--
ALTER TABLE `student_subject`
ADD CONSTRAINT `fk_student_subject` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_usumbufu` FOREIGN KEY (`class_stream_id`, `subject_id`) REFERENCES `class_stream_subject` (`class_stream_id`, `subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_subject_assessment`
--
ALTER TABLE `student_subject_assessment`
ADD CONSTRAINT `fk_etype` FOREIGN KEY (`etype_id`) REFERENCES `exam_type` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ssa` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ssa_cs` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ssa_s` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tssa` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_subject_score_position`
--
ALTER TABLE `student_subject_score_position`
ADD CONSTRAINT `fk_csssp` FOREIGN KEY (`class_id`) REFERENCES `class_level` (`class_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cssssp` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_sssp` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tssp` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tssub` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE;

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
ADD CONSTRAINT `fk_subject_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`);

--
-- Constraints for table `subject_teacher`
--
ALTER TABLE `subject_teacher`
ADD CONSTRAINT `fk_subject_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_teacher_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_topic`
--
ALTER TABLE `sub_topic`
ADD CONSTRAINT `fk_subtopic_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_subtopic_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_subtopic_topic` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `super_admin_module`
--
ALTER TABLE `super_admin_module`
ADD CONSTRAINT `fk_am` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`),
ADD CONSTRAINT `fk_am_id` FOREIGN KEY (`id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `super_admin_permission`
--
ALTER TABLE `super_admin_permission`
ADD CONSTRAINT `fk_admin_id` FOREIGN KEY (`id`) REFERENCES `admin` (`id`),
ADD CONSTRAINT `fk_admin_permission` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suspended_students`
--
ALTER TABLE `suspended_students`
ADD CONSTRAINT `fk_suspension` FOREIGN KEY (`admission_no`) REFERENCES `student` (`admission_no`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
ADD CONSTRAINT `fk_teacher` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher_duty_roaster`
--
ALTER TABLE `teacher_duty_roaster`
ADD CONSTRAINT `fk_teacher_duty` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tod_roaster` FOREIGN KEY (`did`) REFERENCES `duty_roaster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_assignment`
--
ALTER TABLE `teaching_assignment`
ADD CONSTRAINT `fk_ta_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ta_department` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ta_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ta_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`staff_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ta_term` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_attendance`
--
ALTER TABLE `teaching_attendance`
ADD CONSTRAINT `fk_journal_subtopic` FOREIGN KEY (`subTopicID`) REFERENCES `sub_topic` (`subtopic_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ta_period_no` FOREIGN KEY (`period_no`) REFERENCES `period` (`period_no`),
ADD CONSTRAINT `fk_tatt_csid` FOREIGN KEY (`class_stream_id`) REFERENCES `class_stream` (`class_stream_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_log`
--
ALTER TABLE `teaching_log`
ADD CONSTRAINT `fk_log_dept` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tod_daily_routine_tbl`
--
ALTER TABLE `tod_daily_routine_tbl`
ADD CONSTRAINT `fk_duty_roaster` FOREIGN KEY (`did`) REFERENCES `teacher_duty_roaster` (`did`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
